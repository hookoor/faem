{{/* vim: set filetype=mustache: */}}
{{/*
Expand the name of the chart.
*/}}
{{- define "faem-backend.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "faem-backend.fullname" -}}
{{- if .Values.fullnameOverride -}}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- $name := default .Chart.Name .Values.nameOverride -}}
{{- if contains $name .Release.Name -}}
{{- .Release.Name | trunc 63 | trimSuffix "-" -}}
{{- else -}}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" -}}
{{- end -}}
{{- end -}}
{{- end -}}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "faem-backend.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{/*
Common labels
*/}}
{{- define "faem-backend.labels" -}}
app.kubernetes.io/name: {{ include "faem-backend.name" . }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end -}}

{{/* Client */}}

{{- define "faem-backend.fullname-client" -}}
{{ include "faem-backend.fullname" . }}-client
{{- end -}}

{{- define "faem-backend.labels-client" -}}
app.kubernetes.io/name: client
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-client" -}}
app.kubernetes.io/name: client
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* CRM */}}

{{- define "faem-backend.fullname-crm" -}}
{{ include "faem-backend.fullname" . }}-crm
{{- end -}}

{{- define "faem-backend.labels-crm" -}}
app.kubernetes.io/name: crm
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-crm" -}}
app.kubernetes.io/name: crm
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* Driver */}}

{{- define "faem-backend.fullname-driver" -}}
{{ include "faem-backend.fullname" . }}-driver
{{- end -}}

{{- define "faem-backend.labels-driver" -}}
app.kubernetes.io/name: driver
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-driver" -}}
app.kubernetes.io/name: driver
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* Voip */}}

{{- define "faem-backend.fullname-voip" -}}
{{ include "faem-backend.fullname" . }}-voip
{{- end -}}

{{- define "faem-backend.labels-voip" -}}
app.kubernetes.io/name: voip
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-voip" -}}
app.kubernetes.io/name: voip
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* CHAT */}}

{{- define "faem-backend.fullname-chat" -}}
{{ include "faem-backend.fullname" . }}-chat
{{- end -}}

{{- define "faem-backend.labels-chat" -}}
app.kubernetes.io/name: chat
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-chat" -}}
app.kubernetes.io/name: chat
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* BILLING */}}

{{- define "faem-backend.fullname-billing" -}}
{{ include "faem-backend.fullname" . }}-billing
{{- end -}}

{{- define "faem-backend.labels-billing" -}}
app.kubernetes.io/name: billing
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-billing" -}}
app.kubernetes.io/name: billing
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* ANALYTIC */}}

{{- define "faem-backend.fullname-analytic" -}}
{{ include "faem-backend.fullname" . }}-analytic
{{- end -}}

{{- define "faem-backend.labels-analytic" -}}
app.kubernetes.io/name: analytic
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-analytic" -}}
app.kubernetes.io/name: analytic
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* SCHEDULER */}}

{{- define "faem-backend.fullname-scheduler" -}}
{{ include "faem-backend.fullname" . }}-scheduler
{{- end -}}

{{- define "faem-backend.labels-scheduler" -}}
app.kubernetes.io/name: scheduler
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-scheduler" -}}
app.kubernetes.io/name: scheduler
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* BONUSES */}}

{{- define "faem-backend.fullname-bonuses" -}}
{{ include "faem-backend.fullname" . }}-bonuses
{{- end -}}

{{- define "faem-backend.labels-bonuses" -}}
app.kubernetes.io/name: bonuses
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-bonuses" -}}
app.kubernetes.io/name: bonuses
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* MSGBOT */}}

{{- define "faem-backend.fullname-msgbot" -}}
{{ include "faem-backend.fullname" . }}-msgbot
{{- end -}}

{{- define "faem-backend.labels-msgbot" -}}
app.kubernetes.io/name: msgbot
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-msgbot" -}}
app.kubernetes.io/name: msgbot
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}

{{/* NOTIFIER */}}

{{- define "faem-backend.fullname-notifier" -}}
{{ include "faem-backend.fullname" . }}-notifier
{{- end -}}

{{- define "faem-backend.labels-notifier" -}}
app.kubernetes.io/name: notifier
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
helm.sh/chart: {{ include "faem-backend.chart" . }}
{{- end -}}

{{- define "faem-backend.selector-notifier" -}}
app.kubernetes.io/name: notifier
app.kubernetes.io/instance: {{ .Release.Name }}
app.kubernetes.io/part-of: {{ .Chart.Name }}
{{- end -}}
