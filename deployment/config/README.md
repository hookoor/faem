# Deployment Configuration

This directory holds custom configuration files for services, suitable for local development.

See [config](../config) directory for default configuration files and documentation.
