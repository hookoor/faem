
UPDATE drv_apps SET promotion = promotion - 'scoring_boost_time_by_photocontrol_passed';

UPDATE drv_distribution_rules SET rule_expr = REPLACE(rule_expr, ' + 100*PhotocontrolPassed', '')
WHERE rule_type = 'driver' AND rule_expr LIKE '% + 100*PhotocontrolPassed%';
