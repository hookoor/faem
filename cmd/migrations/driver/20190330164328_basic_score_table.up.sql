CREATE TABLE public.drv_rules
(
    id SERIAL PRIMARY KEY,
    name TEXT,
    comment TEXT,
    priority INT,
    expr TEXT,
    rule TEXT
)
WITH (
    OIDS = FALSE
);

ALTER TABLE drv_apps ADD COLUMN tag TEXT [];