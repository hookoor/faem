create index if not exists driver_uuid_ids ON drv_offers (driver_uuid);
create index if not exists refresh_tok_idx ON drv_jwt_sessions (refresh_token)