ALTER TABLE IF EXISTS drv_orders
    DROP COLUMN IF EXISTS own_delivery,
    DROP COLUMN IF EXISTS estimated_delivery_time;
