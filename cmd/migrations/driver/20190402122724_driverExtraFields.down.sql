ALTER TABLE drv_apps 
  DROP COLUMN name,
  DROP COLUMN status,
  DROP COLUMN comment,
  DROP COLUMN car,
  DROP COLUMN karma,
  DROP COLUMN color,
  DROP COLUMN reg_number;