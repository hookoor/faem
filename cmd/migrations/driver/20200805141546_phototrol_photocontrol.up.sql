

CREATE TABLE IF NOT EXISTS public.drv_phototrol_snapshot_sketch
(
    uuid            TEXT    PRIMARY KEY         UNIQUE     DEFAULT uuid_generate_v4(),
    name            TEXT,
    title           TEXT,
    description     TEXT,
	  image_mask      TEXT,

    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted         BOOLEAN                     NOT NULL    DEFAULT FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_phototrol_snapshot_sketch TO driver_apiservice;
-- GRANT SELECT, USAGE ON SEQUENCE public.drv_phototrol_snapshot_sketch_uuid_seq TO driver_apiservice;

INSERT INTO drv_phototrol_snapshot_sketch (uuid, name, title, description )
    VALUES  
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb01','front', 'Передняя сторона машины', 'Сфотографируйте переднюю сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb02','back', 'Заддняя сторона машины', 'Сфотографируйте заднюю сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb03','left', 'Левая сторона машины', 'Сфотографируйте левую сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb04','right', 'Правая сторона машины', 'Сфотографируйте правую сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb05','front_left', 'Передняя+левая сторона машины', 'Сфотографируйте переднюю+левую сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb06','front_right', 'Передняя+правая сторона машины', 'Сфотографируйте переднюю+правую сторону автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb07','salon', 'Салон', 'Сфотографируйте салон автомобиля'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb08','trunk', 'Багажник', 'Сфотографируйте багажник автомобиля'),
        
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb11','driver_license_front', 'Водительские права спереди', 'Сфотографируйте переднюю сторону водительских прав'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb12','driver_license_back', 'Водительские права сзади', 'Сфотографируйте обратную сторону водительских прав'),
        
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb21','technical_certificate_front', 'Техпаспорт спереди', 'Сфотографируйте лицевую сторону технического паспорта'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb22','technical_certificate_back', 'Техпаспорт сзади', 'Сфотографируйте обратную сторону технического паспорта'),
        
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb31','insurance_policy', 'Cтраховой полис', 'Сфотографируйте лицевую сторону полиса обязательного медицинского страхования'),
       
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb41','passport_personal_data', 'Личные паспортные данные', 'Сфотографируйте страницу с личными данными из паспорта гражданина РФ'),
        ('65ae78f9-1161-45a9-b7f7-dd8dcefdeb42','passport_reg_data', 'Информация о прописке', 'Сфотографируйте страницу с информацией о прописке из паспорта гражданина РФ')
    ON CONFLICT (uuid) DO NOTHING;

-- ===============================================
-- ===============================================
-- ===============================================


CREATE TABLE IF NOT EXISTS public.drv_phototrol_photocontrol_sketches
(
    uuid                TEXT    PRIMARY KEY         UNIQUE     DEFAULT uuid_generate_v4(),
    name                TEXT,
    title               TEXT,
    description         TEXT,
    snapshot_sketches   JSONB,
    start_activate_time BIGINT,
    start_time_validity BIGINT,
    start_warning_time  BIGINT,
    settings            JSONB,

    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted         BOOLEAN                     NOT NULL    DEFAULT FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_phototrol_photocontrol_sketches TO driver_apiservice;
--GRANT SELECT, USAGE ON SEQUENCE public.drv_phototrol_photocontrol_sketches_uuid_seq TO driver_apiservice;


INSERT INTO drv_phototrol_photocontrol_sketches (uuid, name, title, description, snapshot_sketches, start_activate_time, start_time_validity, start_warning_time)
    VALUES  
        -- Фотоосмотр при регистрации
        ('65ae78f9-1161-45a9-b7f7-dd8dcefded01','registration', 'Фотоосмотр при регистрации', '', '
[
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb05",
    "name": "front_left",
    "title": "Передняя+левая сторона машины",
    "description": "Сфотографируйте переднюю+левую сторону автомобиля"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb06",
    "name": "front_right",
    "title": "Передняя+правая сторона машины",
    "description": "Сфотографируйте переднюю+правую сторону автомобиля"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb11",
    "name": "driver_license_front",
    "title": "Водительские права спереди",
    "description": "Сфотографируйте переднюю сторону водительских прав"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb12",
    "name": "driver_license_back",
    "title": "Водительские права сзади",
    "description": "Сфотографируйте обратную сторону водительских прав"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb21",
    "name": "technical_certificate_front",
    "title": "Техпаспорт спереди",
    "description": "Сфотографируйте лицевую сторону технического паспорта"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb22",
    "name": "technical_certificate_back",
    "title": "Техпаспорт сзади",
    "description": "Сфотографируйте обратную сторону технического паспорта"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb31",
    "name": "insurance_policy",
    "title": "Cтраховой полис",
    "description": "Сфотографируйте лицевую сторону полиса обязательного медицинского страхования"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb41",
    "name": "passport_personal_data",
    "title": "Личные паспортные данные",
    "description": "Сфотографируйте страницу с личными данными из паспорта гражданина РФ"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb42",
    "name": "passport_reg_data",
    "title": "Информация о прописке",
    "description": "Сфотографируйте страницу с информацией о прописке из паспорта гражданина РФ"
  }
]',
0,86400,86400),
        -- Плановый фотоосмотр
        ('65ae78f9-1161-45a9-b7f7-dd8dcefded02','scheduled_photo_inspection', 'Плановый фотоосмотр', '', '
[
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb05",
    "name": "front_left",
    "title": "Передняя+левая сторона машины",
    "description": "Сфотографируйте переднюю+левую сторону автомобиля"
  },
  {
    "uuid": "65ae78f9-1161-45a9-b7f7-dd8dcefdeb06",
    "name": "front_right",
    "title": "Передняя+правая сторона машины",
    "description": "Сфотографируйте переднюю+правую сторону автомобиля"
  }
]',
0,86400,86400)
    ON CONFLICT (uuid) DO NOTHING;

-- Фотоосмотр по требованию администратора
-- Фотоосмотр при изменении опций

-- ===============================================
-- ===============================================
-- ===============================================

CREATE TABLE IF NOT EXISTS public.drv_phototrol_photocontrol_tickets
(
    uuid                TEXT                    PRIMARY KEY    UNIQUE,
    name                TEXT,
    creator             JSONB,
    driver              JSONB,
    title               TEXT,
    description         TEXT,
    comment             TEXT,
    snapshot_sketches   JSONB,
    status              TEXT                    NOT NULL,
    activate_time       BIGINT,
    time_validity       BIGINT,
    warning_time        BIGINT,
    postponement        JSONB,
    settings            JSONB,
    origin_settings     JSONB,
    stages              JSONB,
    created_at_unix     BIGINT,

    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted         BOOLEAN                     NOT NULL    DEFAULT FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_phototrol_photocontrol_tickets TO driver_apiservice;
-- GRANT SELECT, USAGE ON SEQUENCE public.drv_phototrol_photocontrol_tickets_id_seq TO driver_apiservice;

-- ===============================================
-- ===============================================
-- ===============================================
