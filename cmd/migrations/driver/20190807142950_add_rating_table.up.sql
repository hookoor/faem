CREATE TABLE public.drv_order_rating
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    driver_uuid text,
      order_uuid text,
        comment text,
          value integer,
           client_uuid text
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_order_rating TO driver_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.drv_order_rating_id_seq TO driver_apiservice;
