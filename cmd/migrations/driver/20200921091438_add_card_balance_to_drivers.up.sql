ALTER TABLE IF EXISTS drv_apps
    ADD COLUMN IF NOT EXISTS card_balance double precision;
