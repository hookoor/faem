ALTER TABLE IF EXISTS drv_offers
    DROP COLUMN IF EXISTS rules;

ALTER TABLE IF EXISTS drv_offers
    DROP COLUMN IF EXISTS rules_items;

ALTER TABLE IF EXISTS drv_offers
    ADD COLUMN IF NOT EXISTS counter_rejected_drivers_uuid JSONB;