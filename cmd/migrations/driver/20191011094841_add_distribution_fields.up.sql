ALTER TABLE IF EXISTS drv_apps
    ADD COLUMN IF NOT EXISTS activity INTEGER NOT NULL DEFAULT 0;

ALTER TABLE IF EXISTS drv_apps
    ALTER COLUMN karma TYPE REAL;

ALTER TABLE IF EXISTS drv_apps
    ADD COLUMN IF NOT EXISTS promotion JSONB;

ALTER TABLE IF EXISTS drv_apps
    ADD COLUMN IF NOT EXISTS blacklist TEXT[];

ALTER TABLE IF EXISTS drv_offers
    ADD COLUMN IF NOT EXISTS distribution_meta JSONB;

ALTER TABLE IF EXISTS drv_offers
    ADD COLUMN IF NOT EXISTS driver_distribution_rule TEXT NOT NULL DEFAULT '';

ALTER TABLE IF EXISTS drv_offers
    ADD COLUMN IF NOT EXISTS client_distribution_rule TEXT NOT NULL DEFAULT '';

ALTER TABLE IF EXISTS drv_orders
    ADD COLUMN IF NOT EXISTS promotion JSONB;

CREATE TABLE IF NOT EXISTS drv_distribution_rules
(
    id         TEXT PRIMARY KEY,
    rule_type  TEXT                     NOT NULL DEFAULT '', -- 'driver' and 'client' are allowed
    rule_expr  TEXT                     NOT NULL DEFAULT '',
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

CREATE INDEX IF NOT EXISTS idx__drv_distribution_rules__created_at ON drv_distribution_rules (created_at DESC);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE drv_distribution_rules TO driver_apiservice;

COMMENT ON TABLE drv_rules IS 'deprecated, use drv_distribution_rules instead';
COMMENT ON TABLE drv_rule_items IS 'deprecated, use drv_distribution_rules instead';
COMMENT ON COLUMN drv_offers.rules IS 'deprecated, use driver_distribution_rule or client_distribution_rule instead';
COMMENT ON COLUMN drv_offers.rules_items IS 'deprecated, use driver_distribution_rule or client_distribution_rule instead';

CREATE TABLE IF NOT EXISTS drv_activity_history
(
    id              TEXT PRIMARY KEY,
    driver_uuid     TEXT                     NOT NULL,
    offer_uuid      TEXT                     NOT NULL DEFAULT '',
    event           TEXT                     NOT NULL DEFAULT '',
    activity_change INTEGER                  NOT NULL DEFAULT 0,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT now()
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE drv_activity_history TO driver_apiservice;
