create extension if not exists "uuid-ossp";
Alter table if exists drv_offer_states
drop column if exists id,
add column if not exists uuid text not null default uuid_generate_v4() primary key;