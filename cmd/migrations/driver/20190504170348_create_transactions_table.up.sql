CREATE TABLE public.drv_transactions
(
    id SERIAL PRIMARY KEY,
    event text,
    operation text,
    sum int,
    driver_uuid text,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);