CREATE TABLE public.drv_app_versions
(
    id SERIAL PRIMARY KEY,
    version text,
    created_at timestamp with time zone
)
WITH (
    OIDS = FALSE
);