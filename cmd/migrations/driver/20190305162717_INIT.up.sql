CREATE TABLE public.drv_states
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    driver_uuid text COLLATE pg_catalog."default",
    state text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    started_at timestamp with time zone
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_apps
(
    id SERIAL PRIMARY KEY,
    uuid text COLLATE pg_catalog."default" UNIQUE,
    device_id text COLLATE pg_catalog."default",
    phone text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    deleted boolean DEFAULT false,
    orders_queue text COLLATE pg_catalog."default",
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_locations
(
    id SERIAL PRIMARY KEY,
    driver_id bigint,
    driver_uuid text COLLATE pg_catalog."default",
    latitude double precision,
    longitude double precision,
    satelites int,
    "timestamp" bigint,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_offers
(
    id SERIAL PRIMARY KEY,
    valid_till timestamp with time zone,
    accepted_at timestamp with time zone,
    cancelled_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT now(),
    uuid text COLLATE pg_catalog."default",
    driver_uuid text COLLATE pg_catalog."default",
    order_uuid text COLLATE pg_catalog."default",
    offer_type text COLLATE pg_catalog."default",
    response_time bigint,
    driver_id bigint,
    comment text COLLATE pg_catalog."default",
    trip_time bigint
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_offer_states
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    order_uuid text COLLATE pg_catalog."default" NOT NULL,
    driver_uuid text COLLATE pg_catalog."default",
    offer_uuid text COLLATE pg_catalog."default",
    state text COLLATE pg_catalog."default" NOT NULL,
    start_state timestamp with time zone NOT NULL
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_orders
(
    id SERIAL PRIMARY KEY,
    uuid text COLLATE pg_catalog."default" NOT NULL,
    comment text COLLATE pg_catalog."default",
    routes jsonb,
    features jsonb,
    tariff jsonb,
    service jsonb,
    owner jsonb,
    client jsonb
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_jwt_sessions
(
    id SERIAL PRIMARY KEY,
    driver_id bigint,
    driver_uuid text COLLATE pg_catalog."default",
    refresh_token text COLLATE pg_catalog."default",
    refresh_token_used timestamp with time zone,
    refresh_token_expired timestamp with time zone,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_reg_sessions
(
    ssid text COLLATE pg_catalog."default" NOT NULL,
    device_id text COLLATE pg_catalog."default",
    phone text COLLATE pg_catalog."default",
    code bigint,
    expiration timestamp with time zone,
    used_at timestamp with time zone,
    created_at timestamp with time zone DEFAULT now(),
    CONSTRAINT register_sessions_pkey PRIMARY KEY (ssid)
)
WITH (
    OIDS = FALSE
);