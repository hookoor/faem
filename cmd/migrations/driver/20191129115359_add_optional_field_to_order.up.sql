alter table if exists drv_orders
ADD COLUMN if not exists is_optional boolean NOT NULL DEFAULT false;
