ALTER TABLE drv_offers
DROP COLUMN rules,
DROP COLUMN rules_items;

ALTER TABLE drv_orders
DROP COLUMN cancel_time;