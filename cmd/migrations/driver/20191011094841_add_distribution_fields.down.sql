DROP TABLE IF EXISTS drv_activity_history;

COMMENT ON COLUMN drv_offers.rules_items IS NULL;
COMMENT ON COLUMN drv_offers.rules IS NULL;
COMMENT ON TABLE drv_rule_items IS NULL;
COMMENT ON TABLE drv_rules IS NULL;

DROP INDEX IF EXISTS idx__drv_distribution_rules__created_at;

DROP TABLE IF EXISTS drv_distribution_rules;

ALTER TABLE IF EXISTS drv_orders
    DROP COLUMN IF EXISTS promotion;

ALTER TABLE IF EXISTS drv_offers
    DROP COLUMN IF EXISTS driver_distribution_rule;

ALTER TABLE IF EXISTS drv_offers
    DROP COLUMN IF EXISTS client_distribution_rule;

ALTER TABLE IF EXISTS drv_offers
    DROP COLUMN IF EXISTS distribution_meta;

ALTER TABLE IF EXISTS drv_apps
    DROP COLUMN IF EXISTS blacklist;

ALTER TABLE IF EXISTS drv_apps
    DROP COLUMN IF EXISTS promotion;

ALTER TABLE IF EXISTS drv_apps
    ALTER COLUMN karma TYPE INTEGER;

ALTER TABLE IF EXISTS drv_apps
    DROP COLUMN IF EXISTS activity;
