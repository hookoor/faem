alter table if exists drv_apps 
add column if not exists password text;
alter table if exists drv_reg_sessions 
add column if not exists password text;