
ALTER TABLE IF EXISTS drv_apps ADD COLUMN IF NOT EXISTS counter_order_switch BOOLEAN  NOT NULL  DEFAULT FALSE;

ALTER TABLE IF EXISTS drv_orders ADD COLUMN IF NOT EXISTS counter_order_marker BOOLEAN;
ALTER TABLE IF EXISTS drv_orders ALTER COLUMN counter_order_marker SET DEFAULT false;

UPDATE drv_apps SET promotion = jsonb_set(COALESCE(promotion, '{}'), '{have_order}', 'false');

UPDATE drv_distribution_rules SET rule_expr = rule_expr || ' - 100*HaveOrder'
WHERE rule_type = 'driver';
