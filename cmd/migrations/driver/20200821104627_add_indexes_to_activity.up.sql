CREATE INDEX IF NOT EXISTS idx__drv_activity_history__driver_uuid ON drv_activity_history (driver_uuid, created_at DESC);
