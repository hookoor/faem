CREATE TABLE public.drv_firebase_tokens
(
    id SERIAL PRIMARY KEY,
    updated_at timestamp with time zone DEFAULT now(),
    driver_uuid text COLLATE pg_catalog."default" UNIQUE,
  token text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_firebase_tokens TO driver_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.drv_firebase_tokens_id_seq TO driver_apiservice;
