alter table if exists drv_offers
add column if not exists accepted_from_offline boolean not null default false;