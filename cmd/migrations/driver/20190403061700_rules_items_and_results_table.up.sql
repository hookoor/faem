CREATE TABLE public.drv_rule_items
(
    id SERIAL PRIMARY KEY,
    name text,
    if_expr text,
    eval_expr text,
    rule_id int,
    priority int,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.drv_score_result
(
    id SERIAL PRIMARY KEY,
    driver_uuid text,
    offer_uuid text,
    order_uuid text,
    score int,
    comment text,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);