ALTER TABLE if exists drv_apps
DROP COLUMN if exists alias;

ALTER TABLE if exists drv_apps
ADD COLUMN if not exists alias integer;