
ALTER TABLE IF EXISTS drv_apps DROP COLUMN IF EXISTS counter_order_switch;

ALTER TABLE IF EXISTS drv_offers DROP COLUMN IF EXISTS counter_order_marker;

UPDATE drv_apps SET promotion = promotion - 'have_order';

UPDATE drv_distribution_rules SET rule_expr = REPLACE(rule_expr, ' - 100*HaveOrder', '')
WHERE rule_type = 'driver' AND rule_expr LIKE '% - 100*HaveOrder%';
