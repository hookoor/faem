ALTER TABLE IF EXISTS public.drv_orders
    DROP COLUMN IF EXISTS driver_rating;

ALTER TABLE IF EXISTS public.drv_orders
    DROP COLUMN IF EXISTS client_rating;
