alter table if exists drv_apps 
    drop column if exists owner_uuid;
    
alter table if exists drv_orders 
    drop column if exists owner_uuid;