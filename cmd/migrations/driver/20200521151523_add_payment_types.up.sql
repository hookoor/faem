alter table if exists drv_apps 
add column if not exists payment_types text[] not null default '{"cash"}';