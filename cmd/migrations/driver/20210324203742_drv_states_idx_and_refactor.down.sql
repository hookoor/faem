DROP INDEX IF EXISTS idx_driver_uuid_n_created_at;

ALTER TABLE IF EXISTS drv_states
    ADD COLUMN IF NOT EXISTS started_at timestamp with time zone;

UPDATE drv_states
    SET started_at = created_at;