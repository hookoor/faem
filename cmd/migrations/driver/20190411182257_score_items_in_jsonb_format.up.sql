ALTER TABLE drv_offers
ADD COLUMN rules text,
ADD COLUMN rules_items jsonb;

ALTER TABLE drv_orders
ADD COLUMN cancel_time timestamp with time zone;