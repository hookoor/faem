alter table if exists drv_orders
add column if not exists next_autocall_time timestamp with time zone DEFAULT now();