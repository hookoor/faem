CREATE TABLE if not exists public.drv_alerts (
  uuid text PRIMARY KEY, message text, 
  created_at timestamp with time zone not null DEFAULT now(), 
  ack boolean not null DEFAULT false, 
  driver_uuid text NOT NULL, tag text, 
  order_uuid text
)
WITH (
    OIDS = FALSE
);
CREATE INDEX IF NOT EXISTS idx__drv_alerts__driver_uuid ON drv_alerts (driver_uuid, created_at DESC);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_alerts TO driver_apiservice;
