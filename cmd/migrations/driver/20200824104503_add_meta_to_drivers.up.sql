ALTER TABLE IF EXISTS drv_apps
    ADD COLUMN IF NOT EXISTS meta JSONB;

CREATE INDEX IF NOT EXISTS idx__drv_apps__meta_blocked_until ON drv_apps (CAST(meta ->> 'blocked_until' AS BIGINT));
CREATE INDEX IF NOT EXISTS idx__drv_apps__meta_unblocked_at ON drv_apps (CAST(meta ->> 'unblocked_at' AS BIGINT));
