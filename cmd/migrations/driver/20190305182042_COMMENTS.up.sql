COMMENT ON TABLE public.drv_states IS 'Статусы водителей';
COMMENT ON TABLE public.drv_apps IS 'Приложения, а точнее комбинация телефон/id устройства';
COMMENT ON TABLE public.drv_locations IS 'Координаты водителей каждые N секунд';
COMMENT ON TABLE public.drv_offers IS 'Предложения для водителей, которые генерируются из заказа и предлагаются свободным машинам ';
COMMENT ON TABLE public.drv_offer_states IS 'Статусы предложений для водителей';
COMMENT ON TABLE public.drv_orders IS 'Заказы пришедшие из CRM храним в этой таблице';
COMMENT ON TABLE public.drv_jwt_sessions IS 'Сессии JWT токенов';
COMMENT ON TABLE public.drv_reg_sessions IS 'Сессии одноразовых паролей';