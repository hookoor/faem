CREATE TABLE drv_last_distance (
    id SERIAL PRIMARY KEY,
    driver_uuid text,
    distance_to_target integer,
    step integer,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
); 

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_last_distance TO driver_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.drv_last_distance_id_seq TO driver_apiservice;
