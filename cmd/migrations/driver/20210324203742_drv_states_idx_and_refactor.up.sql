CREATE INDEX IF NOT EXISTS idx_driver_uuid_n_created_at ON drv_states USING btree (driver_uuid, created_at DESC);

ALTER TABLE IF EXISTS drv_states
    DROP COLUMN IF EXISTS started_at;