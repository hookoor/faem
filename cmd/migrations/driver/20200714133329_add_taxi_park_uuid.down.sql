alter table if exists drv_orders 
    drop column if exists taxi_park_uuid;

alter table if exists drv_apps
     drop column if exists taxi_park_uuid;