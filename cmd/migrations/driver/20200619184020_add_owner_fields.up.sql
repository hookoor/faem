alter table if exists drv_apps 
    add column if not exists owner_uuid text;
    
alter table if exists drv_orders 
    add column if not exists owner_uuid text;