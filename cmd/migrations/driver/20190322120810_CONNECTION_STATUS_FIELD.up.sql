CREATE TABLE public.drv_conn_states
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    driver_uuid text COLLATE pg_catalog."default",
    conn_state text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    started_at timestamp with time zone
)
WITH (
    OIDS = FALSE
);

COMMENT ON TABLE public.drv_conn_states IS 'Состояния подключения к сети, offline, no_gps и online';