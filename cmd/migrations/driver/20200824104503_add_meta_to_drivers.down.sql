DROP INDEX IF EXISTS idx__drv_apps__meta_unblocked_at;
DROP INDEX IF EXISTS idx__drv_apps__meta_blocked_until;

ALTER TABLE IF EXISTS drv_apps
    DROP COLUMN IF EXISTS meta;
