CREATE TABLE IF NOT EXISTS drv_photocontrols
(
    control_id      TEXT                     NOT NULL,
    driver_id       TEXT                     NOT NULL,
    control_type    TEXT                     NOT NULL,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    approved boolean     NOT NULL DEFAULT false,
    photos jsonb,

    UNIQUE (control_id)
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_photocontrols TO driver_apiservice;