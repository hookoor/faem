CREATE TABLE public.drv_config
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
      operator_phone text not null
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.drv_config TO driver_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.drv_config_id_seq TO driver_apiservice;
