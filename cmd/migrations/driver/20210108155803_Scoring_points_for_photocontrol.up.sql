
UPDATE drv_apps SET promotion = jsonb_set(COALESCE(promotion, '{}'), '{scoring_boost_time_by_photocontrol_passed}', '0');

UPDATE drv_distribution_rules SET rule_expr = rule_expr || ' + 100*PhotocontrolPassed'
WHERE rule_type = 'driver';
