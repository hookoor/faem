CREATE OR REPLACE VIEW reports AS
SELECT tr.id,
       tr.transfer_type,
       tr.description,
       e.entry_type,
       e.amount,
       e.balance,
       a.account_type,
       a.user_type,
       a.phone,
       a.meta  as account_meta,
       tr.meta as transfer_meta,
       tr.created_at,
       a.user_uuid
FROM transfers AS tr
         INNER JOIN transactions AS tx ON tx.id = tr.transaction_ids[1] -- since we still using the first one
         INNER JOIN entries AS e ON tx.debit_id = e.id OR tx.credit_id = e.id
         INNER JOIN accounts AS a on e.account_id = a.id;

GRANT SELECT ON reports TO billing_apiservice;
GRANT SELECT ON reports TO billing_reporter;
