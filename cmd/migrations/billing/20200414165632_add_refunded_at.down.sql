ALTER TABLE IF EXISTS bank_transfers
    DROP COLUMN IF EXISTS refunded_at;
