DROP INDEX IF EXISTS idx__accounts__meta;

ALTER TABLE IF EXISTS accounts
    DROP COLUMN IF EXISTS meta;
