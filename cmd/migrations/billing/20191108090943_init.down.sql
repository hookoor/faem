DROP TABLE IF EXISTS transfers;

DROP TABLE IF EXISTS transactions;

DROP TRIGGER IF EXISTS trigger_entries_calculate_balance ON entries;
DROP FUNCTION IF EXISTS entries_calculate_balance;
DROP TABLE IF EXISTS entries;

DROP TABLE IF EXISTS accounts;
