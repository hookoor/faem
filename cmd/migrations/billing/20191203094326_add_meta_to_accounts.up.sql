ALTER TABLE IF EXISTS accounts
    ADD COLUMN IF NOT EXISTS meta JSONB;

CREATE INDEX IF NOT EXISTS idx__accounts__meta ON accounts USING GIN (meta);
