DROP INDEX IF EXISTS idx__transfers__meta;

ALTER TABLE IF EXISTS transfers
    DROP COLUMN IF EXISTS meta;
