CREATE INDEX IF NOT EXISTS idx__transfers__created_at ON transfers
    USING btree (created_at);

CREATE INDEX IF NOT EXISTS idx__transfers__transaction_id ON transfers
    USING btree ((transaction_ids[1]));

CREATE INDEX IF NOT EXISTS idx__transactions__created_at ON transactions
    USING btree (created_at);

CREATE OR REPLACE VIEW reports_v2 AS
SELECT
    tr.id,
    tr.transfer_type,
    tr.description,
    e.entry_type,
    e.amount,
    e.balance,
    a.account_type,
    a.user_type,
    a.phone,
    a.meta AS account_meta,
    tr.meta AS transfer_meta,
    tr.created_at AS tr_created_at,
    tx.created_at AS tx_created_at,
    a.user_uuid
FROM transfers tr
    JOIN transactions tx ON tx.id = tr.transaction_ids[1]
    JOIN entries e ON tx.debit_id = e.id OR tx.credit_id = e.id
    JOIN accounts a ON e.account_id = a.id;

GRANT SELECT ON reports_v2 TO billing_apiservice;
GRANT SELECT ON reports_v2 TO analytic_apiservice;