ALTER TABLE IF EXISTS transfers
    ADD COLUMN IF NOT EXISTS meta JSONB;

CREATE INDEX IF NOT EXISTS idx__transfers__meta ON transfers USING GIN (meta);
