CREATE TABLE IF NOT EXISTS logs
(
    id         BIGSERIAL PRIMARY KEY,
    event      TEXT                     NOT NULL DEFAULT '',
    data       JSONB,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE logs TO billing_apiservice;
GRANT SELECT, USAGE ON SEQUENCE logs_id_seq TO billing_apiservice;
