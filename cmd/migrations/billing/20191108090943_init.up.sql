CREATE TABLE IF NOT EXISTS accounts
(
    id           TEXT PRIMARY KEY,
    account_type TEXT                     NOT NULL DEFAULT '', -- cash or card
    user_uuid    TEXT                     NOT NULL DEFAULT '',
    user_type    TEXT                     NOT NULL DEFAULT '', -- client, driver, gateway, owner
    phone        TEXT                     NOT NULL DEFAULT '',
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at   TIMESTAMP WITH TIME ZONE,

    UNIQUE (account_type, user_uuid, user_type, phone)
);

CREATE INDEX IF NOT EXISTS idx__accounts__user_uuid ON accounts (user_uuid, user_type, created_at DESC);
CREATE INDEX IF NOT EXISTS idx__accounts__phone ON accounts (phone, user_type, created_at DESC);
CREATE INDEX IF NOT EXISTS idx__accounts__deleted_at ON accounts (deleted_at);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE accounts TO billing_apiservice;

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE TABLE IF NOT EXISTS entries
(
    id         TEXT PRIMARY KEY,
    entry_type TEXT                     NOT NULL DEFAULT '', -- credit or debit
    account_id TEXT                     NOT NULL REFERENCES accounts (id),
    amount     NUMERIC(8, 2)            NOT NULL DEFAULT 0,
    balance    NUMERIC(11, 2)           NOT NULL DEFAULT 0,
    force      BOOLEAN                  NOT NULL DEFAULT false,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP WITH TIME ZONE
);

CREATE INDEX IF NOT EXISTS idx__entries__account_id ON entries (account_id, created_at DESC);
CREATE INDEX IF NOT EXISTS idx__entries__deleted_at ON entries (account_id, deleted_at);

CREATE OR REPLACE FUNCTION entries_calculate_balance()
    RETURNS trigger AS
$BODY$
BEGIN
    NEW.balance := NEW.amount + (
        SELECT COALESCE(
                       (
                           CASE
                               WHEN NEW.force THEN
                                   NULL
                               ELSE (
                                   SELECT balance
                                   FROM entries
                                   WHERE account_id = NEW.account_id
                                   ORDER BY created_at DESC
                                   LIMIT 1
                               )
                               END
                           ),
                       0
                   )
    );
    RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

DROP TRIGGER IF EXISTS trigger_entries_calculate_balance ON entries;
CREATE TRIGGER trigger_entries_calculate_balance
    BEFORE INSERT
    ON entries
    FOR EACH ROW
EXECUTE PROCEDURE entries_calculate_balance();

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE entries TO billing_apiservice;

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE TABLE IF NOT EXISTS transactions
(
    id               TEXT PRIMARY KEY,
    credit_id        TEXT                     NOT NULL REFERENCES entries (id),
    debit_id         TEXT                     NOT NULL REFERENCES entries (id),
    transaction_type TEXT                     NOT NULL DEFAULT '', -- fee or transaction
    created_at       TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at       TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at       TIMESTAMP WITH TIME ZONE
);

CREATE INDEX IF NOT EXISTS idx__transactions__credit_id ON transactions (credit_id);
CREATE INDEX IF NOT EXISTS idx__transactions__debit_id ON transactions (debit_id);
CREATE INDEX IF NOT EXISTS idx__transactions__deleted_at ON transactions (deleted_at);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE transactions TO billing_apiservice;

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE TABLE IF NOT EXISTS transfers
(
    id              TEXT PRIMARY KEY,
    transfer_type   TEXT                     NOT NULL DEFAULT '', -- payment, refund, topup
    amount          NUMERIC(8, 2)            NOT NULL DEFAULT 0,
    transaction_ids TEXT[],
    description     TEXT                     NOT NULL DEFAULT '',
    external_id     TEXT,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at      TIMESTAMP WITH TIME ZONE
);

CREATE INDEX IF NOT EXISTS idx__transfers__transaction_ids ON transfers USING GIN (transaction_ids);
CREATE UNIQUE INDEX IF NOT EXISTS uix__transfers__external_id ON transfers (external_id);
CREATE INDEX IF NOT EXISTS idx__transfers__deleted_at ON transfers (deleted_at);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE transfers TO billing_apiservice;
