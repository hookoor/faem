CREATE TABLE IF NOT EXISTS bank_cards
(
    id            TEXT PRIMARY KEY,
    user_uuid     TEXT                     NOT NULL,
    card_type     TEXT                     NOT NULL, -- VISA, Master Card, etc.
    card_suffix   TEXT                     NOT NULL, -- last 4 digits of a card
    payment_token TEXT                              DEFAULT '',
    data          JSONB,
    created_at    TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    deleted_at    TIMESTAMP WITH TIME ZONE
);

CREATE INDEX IF NOT EXISTS idx__bank_cards__user_uuid ON bank_cards (user_uuid);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bank_cards TO billing_apiservice;

-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

CREATE TABLE IF NOT EXISTS bank_transfers
(
    id             TEXT PRIMARY KEY,
    user_uuid      TEXT                     NOT NULL,
    transaction_id TEXT                     NOT NULL,
    payment_req    TEXT                     NOT NULL DEFAULT '', -- for 3DS
    acs_url        TEXT                     NOT NULL DEFAULT '', -- for 3DS
    amount         NUMERIC(8, 2)            NOT NULL DEFAULT 0,
    subject_uuid   TEXT                     NOT NULL DEFAULT '',
    prepaid_at     TIMESTAMP WITH TIME ZONE,
    confirmed_at   TIMESTAMP WITH TIME ZONE,
    data           JSONB,
    created_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx__bank_transfers__user_and_subject_uuid ON bank_transfers (user_uuid, subject_uuid, prepaid_at);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bank_transfers TO billing_apiservice;
