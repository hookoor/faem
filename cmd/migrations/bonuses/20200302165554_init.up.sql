CREATE TABLE if not exists bon_write_off
(
    phone              text PRIMARY KEY,
    switcher_uuid           text,
    switcher_type          text,
    condition    boolean not null default false,
    updated_at TIMESTAMP WITH time zone
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bon_write_off TO  bonuses_role;