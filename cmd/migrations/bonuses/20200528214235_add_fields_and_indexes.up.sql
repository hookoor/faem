create extension if not exists "uuid-ossp";
alter table if exists bon_promocodes_activations
    add column if not exists uuid text not null default uuid_generate_v4() primary key,
    add column if not exists bonuses_accrued integer,
    add column if not exists used integer,
    add column if not exists available integer,
    add column if not exists burned integer, 
    add column if not exists comment text, 
    add column if not exists type text, 
    add column if not exists source text, 
    add column if not exists uuid text,
    add column if not exists active boolean not null default true,
    add column if not exists life_time TIMESTAMP WITH time zone not null default current_timestamp + interval '1 year';
    
create index if not exists client_phone_idx ON bon_promocodes_activations (client_phone,created_at);
create index if not exists active_idx ON bon_promocodes_activations (active);
create index if not exists code_uuid_idx ON bon_promocodes_activations (code_uuid);

alter table if exists bon_promocodes
    add column if not exists bonuses_burn_time TIMESTAMP WITH time zone not null default current_timestamp + interval '1 year';
    