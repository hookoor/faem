alter table if exists bon_promocodes_activations
    add column if not exists rule_name text, 
    add column if not exists rule_uuid text;