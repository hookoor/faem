CREATE TABLE if not exists bon_promotions
(
    uuid          text    primary key,
	if_expr        text,
	name          text,
	active        boolean   not null default true,
	time_period    bigint,
	accrual_expr   text,
	creator_uuid   text,
	created_at     TIMESTAMP WITH time zone not null default CURRENT_TIMESTAMP,
	deleted        boolean   not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bon_promotions TO  bonuses_role;