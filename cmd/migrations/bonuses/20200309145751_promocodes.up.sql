CREATE TABLE if not exists bon_promocodes
(
    uuid              text PRIMARY KEY,
    comment           text,
    code          text,
    bonuses_count integer not null,
    client_phone    text,
    created_at TIMESTAMP WITH time zone not null default CURRENT_TIMESTAMP,
    creator_data jsonb,
    activation_count integer not null default 0,
    expiration TIMESTAMP WITH time zone not null default CURRENT_TIMESTAMP,
    activation_limit integer not null default 0, 
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bon_promocodes TO  bonuses_role;

CREATE TABLE if not exists bon_promocodes_activations
(
    code_uuid              text,
    code           text, 
    client_phone    text not null,
    created_at TIMESTAMP WITH time zone not null default CURRENT_TIMESTAMP
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE bon_promocodes_activations TO  bonuses_role;
