alter table if exists bon_promocodes_activations
    drop column if exists uuid,
    drop column if exists bonuses_accrued,
    drop column if exists used,
    drop column if exists available,     
    drop column if exists burned,     
    drop column if exists uuid,
    drop column if exists active, 
    drop column if exists source, 
    drop column if exists comment, 
    drop column if exists type, 
    drop column if exists life_time;
    
drop index if exists client_phone_idx;
drop index if exists active_idx;
drop index if exists code_uuid_idx;

alter table if exists bon_promocodes
    drop column if exists bonuses_burn_time;
    