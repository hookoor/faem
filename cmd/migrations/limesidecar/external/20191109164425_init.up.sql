CREATE OR REPLACE FUNCTION main_oper_notify_faem() RETURNS TRIGGER
    LANGUAGE plpgsql
AS
$$
BEGIN

    IF TG_OP = 'UPDATE' AND NEW.state = 'EndCancel' THEN
        NOTIFY limesidecar_cancel;
        RETURN NEW;
    END IF;

    IF TG_OP = 'INSERT' THEN
        NOTIFY limesidecar_create;
    END IF;

    RETURN NEW;

END;
$$;
ALTER FUNCTION main_oper_notify_faem() OWNER TO postgres;

DROP TRIGGER IF EXISTS tg_main_oper_notify_faem ON main_oper;
CREATE TRIGGER tg_main_oper_notify_faem
    AFTER INSERT OR UPDATE
    ON main_oper
    FOR EACH ROW
EXECUTE PROCEDURE main_oper_notify_faem();
