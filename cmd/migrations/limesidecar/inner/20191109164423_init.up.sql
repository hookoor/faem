CREATE TABLE IF NOT EXISTS mappings
(
    id             TEXT PRIMARY KEY,
    lime_object_id BIGINT                   NOT NULL,
    faem_object_id TEXT                     NOT NULL,
    object_type    TEXT                     NOT NULL,
    state          TEXT                     NOT NULL DEFAULT '',
    created_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at     TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,

    UNIQUE (faem_object_id, object_type)
);
