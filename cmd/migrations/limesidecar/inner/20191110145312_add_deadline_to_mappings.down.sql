DROP INDEX IF EXISTS idx__mappings__deadline_at;
DROP INDEX IF EXISTS idx__mappings__created_at;
DROP INDEX IF EXISTS idx__mappings__state;

ALTER TABLE IF EXISTS mappings
    ALTER COLUMN faem_object_id SET NOT NULL;

ALTER TABLE IF EXISTS mappings
    ALTER COLUMN object_type SET NOT NULL;

ALTER TABLE IF EXISTS mappings
    DROP COLUMN IF EXISTS deadline_at;
