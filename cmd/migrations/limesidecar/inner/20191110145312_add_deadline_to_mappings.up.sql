ALTER TABLE IF EXISTS mappings
    ADD COLUMN IF NOT EXISTS deadline_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT 'epoch';

ALTER TABLE IF EXISTS mappings
    ALTER COLUMN object_type DROP NOT NULL;

ALTER TABLE IF EXISTS mappings
    ALTER COLUMN faem_object_id DROP NOT NULL;

CREATE INDEX IF NOT EXISTS idx__mappings__state ON mappings (state);
CREATE INDEX IF NOT EXISTS idx__mappings__created_at ON mappings (created_at);
CREATE INDEX IF NOT EXISTS idx__mappings__deadline_at ON mappings (deadline_at);
