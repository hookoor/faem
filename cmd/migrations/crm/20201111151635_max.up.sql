alter table if exists crm_driver_tariff 
    add column if not exists max_complete_commission integer,
    add column if not exists max_period_commission integer,
    add column if not exists max_rejection_commission integer;