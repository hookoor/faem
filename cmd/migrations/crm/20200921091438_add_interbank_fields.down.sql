ALTER TABLE IF EXISTS crm_drivers
    DROP COLUMN IF EXISTS card_balance;

ALTER TABLE IF EXISTS crm_taxi_parks
    DROP COLUMN IF EXISTS card_balance;

ALTER TABLE IF EXISTS crm_taxi_parks
    DROP COLUMN IF EXISTS representative;
