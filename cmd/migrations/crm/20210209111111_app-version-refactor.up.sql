DELETE FROM crm_config WHERE "key" = 'min_client_app_version';
DELETE FROM crm_config WHERE "key" = 'min_driver_app_version';

INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'min_app_version',
'{
  "client": [
    {
      "os": "Android",
      "version_name": "0.0.3"
    },
    {
      "os": "iOS",
      "version_name": "0.0.3"
    }
  ],
  "driver": [
    {
      "os": "Android",
      "version_name": "0.0.3"
    }
  ]
}',
'Минимальные версии клиентского и водительского приложений')
ON CONFLICT(key) DO NOTHING;
