

CREATE TABLE IF NOT EXISTS crm_features_sets
(
    id          SERIAL                      PRIMARY KEY,
    name        TEXT                        NOT NULL,

    created_at  TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT now(),
    updated_at  TIMESTAMP WITH TIME ZONE,
    deleted_at  TIMESTAMP WITH TIME ZONE
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_features_sets TO crm_apiservice;


ALTER TABLE IF EXISTS crm_regions ADD COLUMN IF NOT EXISTS features_set_id INT;


ALTER TABLE IF EXISTS crm_features ADD UNIQUE (uuid);


CREATE TABLE IF NOT EXISTS crm_features_set_by_region
(
    "id"                    SERIAL      PRIMARY KEY,
    "set_id"                INT         NOT NULL  REFERENCES crm_features_sets(id),
    "feature_uuid"          TEXT        NOT NULL  REFERENCES crm_features(uuid),
    "enable_for_driver"     BOOLEAN     NOT NULL  DEFAULT FALSE,
    "visible_for_driver"    BOOLEAN     NOT NULL  DEFAULT FALSE,
    "enable_for_client"     BOOLEAN     NOT NULL  DEFAULT FALSE,
    "visible_for_client"    BOOLEAN     NOT NULL  DEFAULT FALSE,
    "display_priority"      SERIAL      NOT NULL,
    UNIQUE (set_id, feature_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_features_set_by_region TO crm_apiservice;
grant all on sequence crm_features_set_by_region_id_seq to crm_apiservice;
grant all on sequence crm_features_set_by_region_display_priority_seq to crm_apiservice;


CREATE TABLE IF NOT EXISTS crm_zzz_m2m_features_set_by_service
(
    "id"                    SERIAL      PRIMARY KEY,
    "set_id"                INT         NOT NULL  REFERENCES crm_features_sets(id),
    "service_uuid"          TEXT        NOT NULL  REFERENCES crm_services(uuid),
    "feature_uuid"          TEXT        NOT NULL  REFERENCES crm_features(uuid),
    UNIQUE (set_id, service_uuid, feature_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_zzz_m2m_features_set_by_service TO crm_apiservice;
grant all on sequence crm_zzz_m2m_features_set_by_service_id_seq to crm_apiservice;



ALTER TABLE IF EXISTS crm_set_to_service ADD COLUMN IF NOT EXISTS enable_for_driver BOOLEAN DEFAULT FALSE;
ALTER TABLE IF EXISTS crm_set_to_service ADD COLUMN IF NOT EXISTS visible_for_driver BOOLEAN DEFAULT FALSE;


-- для возможности делать реляции
ALTER TABLE IF EXISTS crm_drivers ADD UNIQUE (uuid);



CREATE TABLE IF NOT EXISTS crm_zzz_m2m_active_services_for_drivers
(
    id            SERIAL  PRIMARY KEY,
    driver_uuid   TEXT    NOT NULL   REFERENCES crm_drivers(uuid),
    service_uuid  TEXT    NOT NULL   REFERENCES crm_services(uuid),
    UNIQUE (driver_uuid, service_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_zzz_m2m_active_services_for_drivers TO crm_apiservice;
grant all on sequence crm_zzz_m2m_active_services_for_drivers_id_seq to crm_apiservice;

CREATE INDEX IF NOT EXISTS index__crm_zzz_m2m_active_services_for_drivers ON crm_zzz_m2m_active_services_for_drivers (driver_uuid);




CREATE TABLE IF NOT EXISTS crm_zzz_m2m_active_features_for_drivers
(
    id            SERIAL  PRIMARY KEY,
    driver_uuid   TEXT    NOT NULL   REFERENCES crm_drivers(uuid),
    feature_uuid  TEXT    NOT NULL   REFERENCES crm_features(uuid),
    UNIQUE (driver_uuid, feature_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_zzz_m2m_active_features_for_drivers TO crm_apiservice;
grant all on sequence crm_zzz_m2m_active_features_for_drivers_id_seq to crm_apiservice;

CREATE INDEX IF NOT EXISTS index__crm_zzz_m2m_active_features_for_drivers ON crm_zzz_m2m_active_features_for_drivers (driver_uuid)
