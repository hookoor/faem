alter table if exists crm_products
add column if not exists available boolean;

alter table if exists crm_stores
add column if not exists available boolean;