
-- +====================================+
-- | Создание стандартного набора услуг |
-- +====================================+

INSERT INTO crm_services_sets (id, name) VALUES (1, 'Стандартный набор услуг')
ON CONFLICT DO NOTHING;

INSERT INTO crm_set_to_service (set_id, service_uuid, active)
    SELECT 1, uuid, true
    FROM crm_services
    WHERE deleted IS NOT true
ON CONFLICT DO NOTHING;

---

-- +=========================+
-- | Создание дефолт региона |
-- +=========================+

INSERT INTO crm_regions (id, name, services_set_id, origin) VALUES
    (1, 'Владикавказ', 1, '{"lat": 43.032341, "long": 44.668935}')
ON CONFLICT DO NOTHING;

--

-- +======================================+
-- | Заполнение таблиц дефолтным регионом |
-- +======================================+

UPDATE crm_driver_groups
	SET  region_id=1
	WHERE region_id IS null;

UPDATE crm_driver_tariff
	SET  region_id=1
	WHERE region_id IS null;

UPDATE crm_drivers
	SET  region_id=1
	WHERE region_id IS null;
	
UPDATE crm_driver_tariff
	SET  "default"=false
	WHERE "default" IS null;

UPDATE crm_taxi_parks
	SET  region_id=1
	WHERE region_id IS null;

UPDATE crm_users
	SET  region_ids='{1}'
	WHERE region_ids IS null;

UPDATE crm_driver_news
	SET  region_ids='{1}'
	WHERE region_ids IS null;

UPDATE crm_driver_news
	SET taxi_parks_uuid=(SELECT array_agg(tp."uuid") FROM crm_taxi_parks tp WHERE tp.region_id = 1)
	WHERE taxi_parks_uuid is null;

UPDATE crm_tariff_surcharges
	SET  region_id=1
	WHERE region_id IS null;

UPDATE crm_tariff_rules
    SET region_id = 1
    WHERE region_id IS null;

UPDATE crm_tariff_prices
    SET region_id = 1
    WHERE region_id IS null;

UPDATE crm_tickets 
SET taxi_parks_uuid=(SELECT array_agg(uuid) 
                     FROM crm_taxi_parks 
                     WHERE name='Стандартный таксопарк') 
WHERE taxi_parks_uuid is null

--

-- +=========================================================+
-- | Переливаем телефонные линии в таблицу источников заказа |
-- +=========================================================+

INSERT INTO crm_sources_of_orders
("uuid", "name", "comment", default_service_uuid, created_at, updated_at, source_type, taxi_park_uuid)
(SELECT "uuid", "name", "comment", service_uuid, created_at, updated_at, 'phoneline', (SELECT tp.uuid
                                                                                    FROM crm_taxi_parks tp
                                                                                    WHERE pl.uuid = ANY (tp.phonelines_uuid) AND tp.deleted IS NOT TRUE
                                                                                    ORDER BY tp.created_at DESC
                                                                                    LIMIT 1
                                                                                    ) as taxi_park_uuid
FROM crm_phonelines pl
WHERE deleted IS NOT true)
ON CONFLICT DO NOTHING;
--
-- +=================================+
-- | Создание источников для мобилок |
-- +=================================+

WITH def_ser_uuid as(
    SELECT pl.service_uuid,sof.taxi_park_uuid
    FROM crm_phonelines as pl
    JOIN crm_sources_of_orders as sof ON sof.uuid = pl.uuid
    WHERE "default" AND NOT deleted
    LIMIT 1
)

INSERT INTO crm_sources_of_orders (name, default_service_uuid, taxi_park_uuid, region_id, source_type) 
VALUES ('Faem.Taxi (iOS)', (SELECT service_uuid FROM def_ser_uuid), (SELECT taxi_park_uuid FROM def_ser_uuid), 1, 'api'),
       ('Faem.Taxi (Android)', (SELECT service_uuid FROM def_ser_uuid), (SELECT taxi_park_uuid FROM def_ser_uuid), 1, 'api');

--

-- +================================================+
-- | Маппинг источников для мобилок к хэдеру Source |
-- +================================================+

WITH def_sof_uuid as(
    SELECT (SELECT "uuid"
			FROM crm_sources_of_orders
			WHERE "name" ILIKE '%android%'
			ORDER BY created_at DESC
			LIMIT 1) as android_sof_uuid,
			(SELECT "uuid"
			FROM crm_sources_of_orders
			WHERE "name" ILIKE '%ios%'
			ORDER BY created_at DESC
			LIMIT 1) as ios_sof_uuid
)

INSERT INTO crm_sources_of_orders_forwarding (source_key, source_of_orders_uuid) 
VALUES ('android_client_app_1', (SELECT android_sof_uuid FROM def_sof_uuid)),
       ('ios_client_app_1', (SELECT ios_sof_uuid FROM def_sof_uuid))
ON CONFLICT DO NOTHING;

---

-- +===========================================================+
-- | Инициализация правил распределения таксопарков и регионов |
-- +===========================================================+

CREATE OR REPLACE FUNCTION pg_temp.insert_distribution_rule(is_region BOOLEAN) RETURNS INT
    LANGUAGE plpgsql
AS $body$
DECLARE distribution_rule_id INT;
    DECLARE _template JSONB;
    DECLARE _expr TEXT;
    DECLARE _params JSONB;
BEGIN
    _expr = '1000*Booster + 100*Activity + 10*Karma + 30*Distance + 1*GroupWeight + (Waiting < 0.5 ? Waiting*30 : Waiting*60) + (''card'' in PaymentTypes ? 1000 : 0) - 100*HaveOrder + 100*PhotocontrolPassed';
    _template = '{"booster":"1000","activity":"100","karma":"10","distance":"30","group_weight":"1","waiting":{"if":"0.5","then":"30","else":"60"},"payment_types":{"if":"card","then":"1000","else":"0"},"have_order":"100","photocontrol_passed":"100"}';
    _params = '{"rounds":[250,1567,1568,1569,1570],"out_of_town_multiplier":2}';
    IF is_region THEN
        _expr = _expr || ' + 1*TaxiParkWeight';
        _template = _template || '{"taxipark_weight":"1"}'::jsonb;
        _params = _params || '{"allow_neutral_taxi_parks":true}'::jsonb;
    ELSE
        _params = _params || '{"smart_attempts_limit":21}'::jsonb;
    END IF;

    INSERT INTO crm_distribution_rules(expr, variables, params) VALUES (_expr, _template, _params)
    RETURNING id INTO distribution_rule_id;

    RETURN distribution_rule_id;
END
$body$;

UPDATE crm_regions
SET distribution_rule_id = pg_temp.insert_distribution_rule(true)
WHERE distribution_rule_id IS NULL AND deleted_at IS NULL;

UPDATE crm_taxi_parks
SET distribution_rule_id = pg_temp.insert_distribution_rule(false)
WHERE distribution_rule_id IS NULL AND deleted IS NOT true;


-- +=========================================+
-- | Инициализация правил повышенного спроса |
-- +=========================================+

CREATE OR REPLACE FUNCTION pg_temp.insert_surge_rule() RETURNS INT
    LANGUAGE plpgsql
AS $body$
DECLARE surge_rule_id INT;
    DECLARE _surge_table JSONB;
BEGIN
    _surge_table = '[{"delta": 2, "surge": 22, "_is_flat": false, "surge_crm": 19, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 55, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 1, "surge": 18, "_is_flat": false, "surge_crm": 15, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 30, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 1.5, "surge": 20, "_is_flat": false, "surge_crm": 17, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 30, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 0, "surge": 0, "_is_flat": false, "surge_crm": 0, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 30, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 2.3, "surge": 24, "_is_flat": false, "surge_crm": 21, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 59, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 2.5, "surge": 25, "_is_flat": false, "surge_crm": 23, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 59, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 2.8, "surge": 28, "_is_flat": false, "surge_crm": 26, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 60, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 3, "surge": 29, "_is_flat": false, "surge_crm": 28, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 60, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 3.3, "surge": 60, "_is_flat": false, "surge_crm": 30, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 77, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 3.5, "surge": 65, "_is_flat": false, "surge_crm": 31, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 78, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 3.8, "surge": 70, "_is_flat": false, "surge_crm": 32, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 79, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 4, "surge": 77, "_is_flat": false, "surge_crm": 40, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 80, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 4.3, "surge": 78, "_is_flat": false, "surge_crm": 42, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 81, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 4.5, "surge": 79, "_is_flat": false, "surge_crm": 72, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 82, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 4.8, "surge": 81, "_is_flat": false, "surge_crm": 73, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 83, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 5, "surge": 82, "_is_flat": false, "surge_crm": 74, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 84, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 5.3, "surge": 83, "_is_flat": false, "surge_crm": 75, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 85, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 5.5, "surge": 84, "_is_flat": false, "surge_crm": 76, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 86, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 5.8, "surge": 85, "_is_flat": false, "surge_crm": 77, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 87, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 6, "surge": 87, "_is_flat": false, "surge_crm": 78, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 97, "_cur_order_count": 0, "_cur_driver_count": 0}, {"delta": 6.3, "surge": 90, "_is_flat": false, "surge_crm": 79, "_cur_delta": 0, "surge_type": "percent", "max_price_delta": 100, "_cur_order_count": 0, "_cur_driver_count": 0}]';

    INSERT INTO crm_surge_rules(surge_table) VALUES (_surge_table)
    RETURNING id INTO surge_rule_id;

    RETURN surge_rule_id;
END
$body$;

UPDATE crm_set_to_service
SET surge_rule_id = pg_temp.insert_surge_rule()
WHERE surge_rule_id IS NULL;


-- +======================================+
-- | Создание стандартного набора адресов |
-- +======================================+

INSERT INTO crm_address_sets(id, name) VALUES (1, 'Стандартный набор адресов')
ON CONFLICT DO NOTHING;

INSERT INTO crm_set_to_address(set_id, address_id, weight, city_name) values (1, 0, 1500, 'Владикавказ')
ON CONFLICT DO NOTHING;

UPDATE crm_regions
SET addresses_set_id = 1
WHERE id = 1;



-- Накатили вне миграций

-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
-- UPDATE crm_orders SET region_id = 1 WHERE uuid IN (SELECT uuid FROM public.crm_orders WHERE region_id IS NULL ORDER BY created_at DESC LIMIT 1000);
 