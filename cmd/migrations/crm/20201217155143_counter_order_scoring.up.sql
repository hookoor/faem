
ALTER TABLE IF EXISTS crm_drivers ADD COLUMN IF NOT EXISTS counter_order_switch BOOLEAN  NOT NULL  DEFAULT FALSE;

ALTER TABLE IF EXISTS crm_orders ADD COLUMN IF NOT EXISTS counter_order_marker BOOLEAN;
ALTER TABLE IF EXISTS crm_orders ALTER COLUMN counter_order_marker SET DEFAULT false;

UPDATE crm_drivers SET promotion = jsonb_set(COALESCE(promotion, '{}'), '{have_order}', 'false');
