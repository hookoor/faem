CREATE TABLE IF not exists public.crm_tariff_surcharges
(
    uuid text NOT NULL,
    name text,
    if_expr text NOT NULL,
    sur_expr text NOT NULL,
    deleted boolean  NOT NULL DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    priority integer NOT NULL DEFAULT 0
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_tariff_surcharges TO crm_apiservice;
