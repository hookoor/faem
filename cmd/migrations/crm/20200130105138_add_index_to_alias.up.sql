
CREATE INDEX IF NOT EXISTS idx__crm_orders__payload_driver_aliases ON crm_orders ((driver->>'alias'));
CREATE INDEX IF NOT EXISTS idx__crm_events__payload_operator  ON public.crm_events USING btree ((payload ->> 'operator'::text));
 
   