create extension if not exists "uuid-ossp";
create table if not exists crm_store_categories (
    uuid text primary key default uuid_generate_v4(),
    name text not null,
    url text not null unique,
    priority int default 0,
    description text default '',
    deleted boolean not null default false,
    created_at timestamptz not null default now()
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_store_categories TO crm_apiservice;