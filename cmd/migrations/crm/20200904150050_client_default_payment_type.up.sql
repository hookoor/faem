
ALTER TABLE IF EXISTS crm_customers ADD COLUMN IF NOT EXISTS default_payment_type TEXT DEFAULT 'cash';

ALTER TABLE IF EXISTS crm_customers ADD COLUMN IF NOT EXISTS referral_program_data JSONB;

INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'referral_system_params',
'{
  "donor_travel_bonuses_count": 2,
  "add_for_donor": 20,
  "add_for_recipient": 50,
  "activation_limit": 30,
  "bonuses_for_recipient_travel_count_limit": 10,
  "referral_url_template": "https://faem.ru/share?ref=%s"
}',
'настройка параметров реферальной системы')
ON CONFLICT(key) DO NOTHING;
