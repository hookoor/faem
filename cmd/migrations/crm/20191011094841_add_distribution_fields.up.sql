ALTER TABLE IF EXISTS crm_drivers
    ADD COLUMN IF NOT EXISTS activity INTEGER NOT NULL DEFAULT 0;

ALTER TABLE IF EXISTS crm_drivers
    ALTER COLUMN karma TYPE REAL;

ALTER TABLE IF EXISTS crm_drivers
    ADD COLUMN IF NOT EXISTS promotion JSONB;

ALTER TABLE IF EXISTS crm_drivers
    ADD COLUMN IF NOT EXISTS blacklist TEXT[];

ALTER TABLE IF EXISTS crm_customers
    ADD COLUMN IF NOT EXISTS activity INTEGER NOT NULL DEFAULT 0;

ALTER TABLE IF EXISTS crm_customers
    ALTER COLUMN karma TYPE REAL;

ALTER TABLE IF EXISTS crm_customers
    ADD COLUMN IF NOT EXISTS promotion JSONB;

ALTER TABLE IF EXISTS crm_customers
    ADD COLUMN IF NOT EXISTS blacklist TEXT[];

ALTER TABLE IF EXISTS crm_orders
    ADD COLUMN IF NOT EXISTS promotion JSONB;

CREATE TABLE IF NOT EXISTS crm_activity_history
(
    id              TEXT PRIMARY KEY,
    user_type       TEXT                     NOT NULL,
    user_uuid       TEXT                     NOT NULL,
    order_uuid      TEXT                     NOT NULL DEFAULT '',
    offer_uuid      TEXT                     NOT NULL DEFAULT '',
    event           TEXT                     NOT NULL DEFAULT '',
    activity_change INTEGER                  NOT NULL DEFAULT 0,
    created_at      TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_activity_history TO crm_apiservice;
