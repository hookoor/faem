CREATE TABLE public.crm_drv_locations
(
    id SERIAL PRIMARY KEY,
    driver_id bigint,
    driver_uuid text COLLATE pg_catalog."default" UNIQUE,
    latitude double precision,
    longitude double precision,
    satelites int,
    "timestamp" bigint,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);
