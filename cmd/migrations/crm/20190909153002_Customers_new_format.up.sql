ALTER TABLE crm_customers ADD COLUMN device_id text;
ALTER TABLE crm_customers ADD COLUMN telegram_id text;
ALTER TABLE crm_customers ADD COLUMN broker_queue text;
ALTER TABLE crm_customers ADD CONSTRAINT UC_crm_customers UNIQUE (main_phone);


ALTER TABLE crm_customers ALTER COLUMN phones TYPE text; 
ALTER TABLE crm_customers ALTER COLUMN phones TYPE text[] USING phones::text[];
