
ALTER TABLE IF EXISTS crm_drivers DROP COLUMN IF EXISTS counter_order_switch;

ALTER TABLE IF EXISTS crm_orders DROP COLUMN IF EXISTS counter_order_marker;

UPDATE crm_drivers SET promotion = promotion - 'have_order';
