ALTER TABLE IF EXISTS public.crm_tariff_prices
   ADD COLUMN IF NOT EXISTS deleted_at timestamp with time zone;

UPDATE public.crm_tariff_prices
SET deleted_at = CURRENT_TIMESTAMP 
WHERE deleted IS TRUE;

ALTER TABLE IF EXISTS public.crm_tariff_prices
   DROP COLUMN IF EXISTS deleted;


--

ALTER TABLE IF EXISTS public.crm_tariff_prices
   ADD COLUMN IF NOT EXISTS "default" boolean DEFAULT FALSE;

UPDATE public.crm_tariff_prices
SET "default" = TRUE
WHERE rule_id = 0;

UPDATE crm_tariff_prices
    SET field_type = 'taximeter'
    WHERE field_type = 'taximetr';

UPDATE crm_tariff_prices
    SET field_type = 'country_taximeter'
    WHERE field_type = 'country_taximetr';

--

ALTER TABLE public.crm_tariff_prices
	RENAME COLUMN taximetr TO taximeter;

--

 ALTER TABLE public.crm_tariff_prices 
    DROP CONSTRAINT IF EXISTS rule_id_and_field_type_pair_unq;
 ALTER TABLE public.crm_tariff_prices 
    ADD CONSTRAINT rule_id_and_field_type_pair_unq UNIQUE(rule_id,field_type,region_id,deleted_at);