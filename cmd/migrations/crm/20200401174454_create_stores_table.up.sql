create table if not exists crm_stores
(
    uuid            TEXT                        PRIMARY KEY NOT NULL    DEFAULT uuid_generate_v4(),
    comment         TEXT,
    name         TEXT,
    image         TEXT,
    type TEXT,
    work_schedule jsonb,
    product_categories text[],
    destination_points_uuid text[],
    order_preparation_time_second int,
   	created_at  TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_stores TO crm_apiservice;

create table if not exists crm_products
(
    uuid            TEXT                        PRIMARY KEY NOT NULL    DEFAULT uuid_generate_v4(),
    comment         TEXT,
    name         TEXT,
    image         TEXT,
    store_uuid         TEXT,
    category TEXT,
    price integer,
    variants jsonb,
    toppings jsonb,
   	created_at  TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_products TO crm_apiservice;

alter table if exists crm_services 
add column if not exists product_delivery boolean;

alter table if exists crm_orders
add column if not exists products_data jsonb;

alter table if exists crm_users
add column if not exists store_uuid text;