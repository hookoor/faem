CREATE TABLE IF NOT EXISTS crm_config (
    id         TEXT PRIMARY KEY,
    key        TEXT        NOT NULL,
    value      JSONB,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX IF NOT EXISTS uix__crm_config__key ON crm_config (key);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_config TO crm_apiservice;
