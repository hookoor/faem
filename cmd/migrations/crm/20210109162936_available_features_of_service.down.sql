
ALTER TABLE IF EXISTS crm_services DROP COLUMN IF EXISTS available_features;

ALTER TABLE IF EXISTS crm_services DROP COLUMN IF EXISTS visibility;
