create extension if not exists "uuid-ossp";
ALTER TABLE IF EXISTS  crm_tariff_prices
    add column if not exists uuid text not null default uuid_generate_v4();

ALTER TABLE IF EXISTS public.crm_tariff_prices ADD CONSTRAINT crm_tariff_prices_uuid_key UNIQUE (uuid);