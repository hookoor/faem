create extension if not exists "uuid-ossp";
alter table if exists crm_penalties_for_zones
add column if not exists uuid text    NOT NULL    DEFAULT uuid_generate_v4(),
add column if not exists deleted boolean not null default false;