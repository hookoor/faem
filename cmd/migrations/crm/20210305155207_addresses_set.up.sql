CREATE TABLE IF NOT EXISTS "crm_address_sets" (
	"id" serial NOT NULL UNIQUE,
	"name" TEXT NOT NULL,

	"created_at"      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
	"updated_at"      TIMESTAMP WITH TIME ZONE,
	"deleted_at"      TIMESTAMP WITH TIME ZONE,

	CONSTRAINT "address_sets_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_address_sets TO crm_apiservice;


ALTER TABLE IF EXISTS crm_regions ADD COLUMN IF NOT EXISTS addresses_set_id INT; 


CREATE TABLE IF NOT EXISTS "crm_set_to_address" (
    "id" serial PRIMARY KEY,
	"set_id" integer NOT NULL,
	"address_id" integer NOT NULL,
	"weight" integer NOT NULL  DEFAULT 0
) WITH (
  OIDS=FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_set_to_address TO crm_apiservice;


-- временное сопоставление по городам. Переделать на idшники улиц
ALTER TABLE IF EXISTS crm_set_to_address ADD COLUMN IF NOT EXISTS city_name TEXT; 

