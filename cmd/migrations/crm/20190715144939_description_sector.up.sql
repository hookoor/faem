CREATE TABLE crm_descr_sector (
    id SERIAL PRIMARY KEY,
    uuid text,
    type text,
    parent_category text,
    name text,
    body text,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    deleted boolean DEFAULT false
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_descr_sector TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_descr_sector_id_seq TO crm_apiservice;
