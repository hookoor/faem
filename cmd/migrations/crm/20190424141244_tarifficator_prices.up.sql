CREATE TABLE public.crm_tariff_prices
(
    id SERIAL PRIMARY KEY,
    rule_id int,
    zone_id int[],
    priority int,
    field_type text,
    taximetr jsonb,
    features jsonb,
    penalties jsonb,
    services jsonb
)
WITH (
    OIDS = FALSE
);
DROP TABLE crm_areas_tariffs;