ALTER TABLE IF EXISTS crm_push_mailing
    DROP COLUMN IF EXISTS targets_taxiparks;

ALTER TABLE IF EXISTS crm_push_mailing
    DROP COLUMN IF EXISTS targets_regions;