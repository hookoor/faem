 alter table if exists crm_public_place
 add column if not exists uuid text COLLATE pg_catalog."default",
 add column if not exists deleted boolean not null default false;

 create extension if not exists "uuid-ossp";
 update crm_public_place set uuid = uuid_generate_v4();

 CREATE TABLE if not exists public.crm_destination_points
(
    point_type text COLLATE pg_catalog."default",
    uuid text COLLATE pg_catalog."default" NOT NULL,
    unrestricted_value text COLLATE pg_catalog."default",
    value text COLLATE pg_catalog."default",
    country text COLLATE pg_catalog."default",
    region text COLLATE pg_catalog."default",
    region_type text COLLATE pg_catalog."default",
    type text COLLATE pg_catalog."default",
    city text COLLATE pg_catalog."default",
    category text COLLATE pg_catalog."default",
    city_type text COLLATE pg_catalog."default",
    street text COLLATE pg_catalog."default",
    street_type text COLLATE pg_catalog."default",
    street_with_type text COLLATE pg_catalog."default",
    house text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    out_of_town boolean,
    deleted boolean not null default false,
    house_type text COLLATE pg_catalog."default",
    accuracy_level bigint,
    radius bigint,
    lat real,   
    lon real,
    CONSTRAINT crm_destination_points_pkey PRIMARY KEY (uuid)
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_destination_points TO crm_apiservice;