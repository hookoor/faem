ALTER TABLE crm_district ADD COLUMN area polygon;
ALTER TABLE crm_district ADD COLUMN tags text[];
DROP TABLE crm_polygons;