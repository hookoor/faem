ALTER TABLE IF EXISTS crm_driver_tariff
    DROP COLUMN IF EXISTS period_price;

ALTER TABLE IF EXISTS crm_driver_tariff
    DROP COLUMN IF EXISTS period;

ALTER TABLE IF EXISTS crm_driver_tariff
    DROP COLUMN IF EXISTS tariff_type;

ALTER TABLE IF EXISTS crm_driver_tariff
    DROP COLUMN IF EXISTS is_secret;

ALTER TABLE IF EXISTS crm_driver_tariff
    ALTER COLUMN rej_expr DROP DEFAULT;

ALTER TABLE IF EXISTS crm_driver_tariff
    ALTER COLUMN comm_expr DROP DEFAULT;
