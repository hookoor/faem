CREATE TABLE if not exists public.crm_push_mailing
(
    id SERIAL,
    uuid text not null,
    type text not null,
    title text,
    message text,
    creator_uuid text,
    targets_uuids text[],
    created_at timestamp with time zone not null DEFAULT now(),
    CONSTRAINT crm_push_mailing_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
) TABLESPACE pg_default;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_push_mailing TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_push_mailing_id_seq TO crm_apiservice;
