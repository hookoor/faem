alter table if exists crm_blacklist
add column if not exists ignore bool not null default false,
add column if not exists order_id int;