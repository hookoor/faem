alter table if exists crm_drivers 
add column if not exists payment_types text[] not null default '{"cash"}';