CREATE EXTENSION IF NOT EXISTS pg_trgm;

CREATE INDEX IF NOT EXISTS idx__crm_events__payload_caller_id ON crm_events USING GIN ((payload ->> 'caller_id') gin_trgm_ops);
