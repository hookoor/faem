CREATE TABLE if not exists public.crm_quick_messages
(
    id SERIAL PRIMARY KEY,
    uuid text NOT NULL unique,      
	message text NOT NULL,
    receiver text NOT NULL,
    created_at timestamp with time zone not null DEFAULT now(),
    deleted boolean DEFAULT false
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_quick_messages TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_quick_messages_id_seq TO crm_apiservice;
