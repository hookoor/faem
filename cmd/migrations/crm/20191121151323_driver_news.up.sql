CREATE TABLE IF NOT EXISTS public.crm_driver_news
(
    uuid            TEXT                        NOT NULL    UNIQUE,
    title           TEXT,
    news            TEXT,
    tag             TEXT[],

    id              SERIAL                      PRIMARY KEY,
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted         BOOLEAN                     NOT NULL    DEFAULT FALSE
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_driver_news TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_driver_news_id_seq TO crm_apiservice;