alter table if exists crm_orders
DROP COLUMN if exists reported_appointment;
alter table if exists crm_orders
DROP COLUMN if exists reported_arrival;
