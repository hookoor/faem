ALTER TABLE public.crm_driver_news 
	DROP CONSTRAINT IF EXISTS pk_uuid_driver_news;
	
ALTER TABLE public.crm_driver_news
	ADD COLUMN IF NOT EXISTS "id" SERIAL PRIMARY KEY;
	
ALTER TABLE public.crm_driver_news
	ADD COLUMN IF NOT EXISTS created_time bigint;
    
ALTER TABLE public.crm_driver_news
	ADD COLUMN IF NOT EXISTS deleted boolean NOT NULL DEFAULT false;

ALTER TABLE public.crm_driver_news
	DROP COLUMN IF EXISTS deleted_at;

ALTER TABLE IF EXISTS public.crm_driver_news
    DROP COLUMN IF EXISTS region_ids;
