CREATE INDEX if not exists idx_order_uuid ON crm_events USING GIN(order_uuid);
CREATE INDEX if not exists idx_driver_uuid ON crm_events USING GIN(driver_uuid);
CREATE INDEX if not exists idx_operator_uuid ON crm_events USING GIN(operator_uuid);
CREATE INDEX if not exists idx_ticket_uuid ON crm_events (ticket_uuid);
CREATE INDEX if not exists idx_event_time ON crm_events (event_time desc);
CREATE INDEX if not exists idx_client_phone ON crm_orders ((client ->> 'phone'),created_at desc);
CREATE INDEX if not exists idx_start_user_uuid ON crm_orders (start_user_uuid,order_state,created_at desc);