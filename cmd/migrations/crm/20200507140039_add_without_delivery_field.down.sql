
alter table if exists crm_orders
drop column if exists without_delivery;

alter table if exists crm_stores
drop column if exists own_delivery;