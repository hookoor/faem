alter table if exists crm_orders 
    add column if not exists delivered_to_driver boolean not null default false;