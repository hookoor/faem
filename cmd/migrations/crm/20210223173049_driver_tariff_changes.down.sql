

ALTER TABLE IF EXISTS crm_driver_tariff DROP COLUMN IF EXISTS region_id;
ALTER TABLE IF EXISTS crm_driver_tariff DROP COLUMN IF EXISTS taxi_park_uuid;

ALTER TABLE IF EXISTS crm_driver_groups DROP COLUMN IF EXISTS available_driver_tariffs_uuid;

