alter table if exists crm_tariff_rules 
    drop column if  exists uuid,
    drop column if  exists deleted,
    drop column if  exists created_at,
    drop column if  exists variables;