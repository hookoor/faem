
UPDATE crm_services SET image = images_set
WHERE images_set IS NOT NULL;

ALTER TABLE IF EXISTS crm_services DROP COLUMN IF EXISTS images_set;
