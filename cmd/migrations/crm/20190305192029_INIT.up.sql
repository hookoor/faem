CREATE TABLE public.crm_buldings
(
    id SERIAL PRIMARY KEY,
    idx_street integer NOT NULL,
    house character varying COLLATE pg_catalog."default" NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    deleted boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    street_uuid text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);
COMMENT ON TABLE public.crm_buldings
    IS 'Импортированные координаты зданий';

COMMENT ON COLUMN public.crm_buldings.id
    IS 'Код';

COMMENT ON COLUMN public.crm_buldings.idx_street
    IS 'Улица';

CREATE TABLE public.crm_city
(
    id integer NOT NULL,
    name text COLLATE pg_catalog."default",
    kladr_code text COLLATE pg_catalog."default",
    defaultcity boolean,
    zagorod boolean,
    latitude double precision,
    longitude double precision,
    rayonid integer,
    districtid integer,
    deleted boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    CONSTRAINT crm_city_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
COMMENT ON TABLE public.crm_city
    IS 'Тут наверно города';


CREATE TABLE public.crm_customers
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    karma bigint,
    main_phone text COLLATE pg_catalog."default",
    phones jsonb,
    comment text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);
COMMENT ON TABLE public.crm_city
    IS 'Любимые клиенты';

CREATE TABLE public.crm_district
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    "order" bigint,
    region jsonb,
    region_id bigint
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_drivers
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    phone text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    status text COLLATE pg_catalog."default",
    car text COLLATE pg_catalog."default",
    karma bigint,
    color text COLLATE pg_catalog."default",
    reg_number text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_features
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    price bigint
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_orders
(
    uuid text COLLATE pg_catalog."default" NOT NULL,
    comment text COLLATE pg_catalog."default",
    routes jsonb,
    features jsonb,
    driver jsonb,
    tariff jsonb,
    service jsonb,
    owner jsonb,
    client jsonb,
    id int,
    cl_uuid text COLLATE pg_catalog."default",
    sr_uuid text COLLATE pg_catalog."default",
    callback_phone text COLLATE pg_catalog."default",
    features_uuids jsonb,
    start_user_uuid text COLLATE pg_catalog."default",
    last_user_uuid text COLLATE pg_catalog."default",
    finish_user_uuid text COLLATE pg_catalog."default",
    appointment_time timestamp with time zone,
    complete_time timestamp with time zone,
    pickup_time timestamp with time zone,
    ow_uuid text COLLATE pg_catalog."default",
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    deleted boolean DEFAULT false,
    order_state text COLLATE pg_catalog."default",
    CONSTRAINT crm_orders_pkey PRIMARY KEY (uuid)
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_orderstates
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    order_uuid text COLLATE pg_catalog."default" NOT NULL,
    driver_uuid text COLLATE pg_catalog."default",
    offer_uuid text COLLATE pg_catalog."default",
    state text COLLATE pg_catalog."default" NOT NULL,
    start_state timestamp with time zone NOT NULL
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_owners
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_services
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    price_coeff real,
    freight boolean,
    comment text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_sessions
(
    id SERIAL PRIMARY KEY,
    user_id bigint,
    refresh_token text COLLATE pg_catalog."default",
    session_end timestamp with time zone,
    refresh_token_used timestamp with time zone,
    refresh_token_expired timestamp with time zone,
    created_at timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_streets
(
    id SERIAL PRIMARY KEY,
    ulica character varying COLLATE pg_catalog."default" NOT NULL,
    rayonid integer,
    comment character varying COLLATE pg_catalog."default",
    deleted boolean NOT NULL DEFAULT false,
    tolist boolean NOT NULL DEFAULT false,
    name character varying COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    idx_streetaliens integer,
    canreject boolean NOT NULL DEFAULT false,
    out_of_town boolean NOT NULL DEFAULT false,
    specnotice boolean NOT NULL DEFAULT false,
    ulicacategoryid integer,
    cityid integer NOT NULL,
    displaypriority integer NOT NULL DEFAULT 0,
    kladr_code text COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    fts text COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    latitude double precision,
    longitude double precision,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    district_uuid text COLLATE pg_catalog."default",
    city_uuid text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_users
(
    id SERIAL PRIMARY KEY,
    deleted boolean DEFAULT false,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    uuid text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    login text COLLATE pg_catalog."default",
    password text COLLATE pg_catalog."default",
    role text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_driver_states
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    driver_uuid text COLLATE pg_catalog."default",
    state text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    started_at timestamp with time zone
)
WITH (
    OIDS = FALSE
);