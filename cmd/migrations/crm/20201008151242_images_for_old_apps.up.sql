

ALTER TABLE IF EXISTS crm_services ADD COLUMN IF NOT EXISTS images_set JSONB;

-- переносит записи из колонки с типом text в колонку c jsonb если это возможно
-- pg_temp - создать временную функцию, используя схему pg_temp.
-- EXCEPTION - что делать если преобразование не удалось
-- invalid_text_representation - ошибка преобразования
CREATE OR REPLACE FUNCTION pg_temp.text_to_jsonb_with_exception(txt text) RETURNS jsonb AS $$
BEGIN
	RETURN txt::jsonb;
EXCEPTION
  WHEN invalid_text_representation THEN
    RETURN NULL;
END $$ LANGUAGE plpgsql;


UPDATE crm_services SET images_set = pg_temp.text_to_jsonb_with_exception(image);

UPDATE crm_services SET image = images_set ->> 'full'
WHERE images_set ->> 'full' IS NOT NULL
AND images_set IS NOT NULL;

UPDATE crm_services SET image = images_set ->> 'full_format'
WHERE images_set ->> 'full_format' IS NOT NULL
AND images_set IS NOT NULL;
