alter table if exists crm_zones 
add column if not exists distance_coeff double precision not null default 1;

CREATE INDEX IF NOT EXISTS idx__crm_orders_deleted  ON public.crm_orders USING btree (deleted);