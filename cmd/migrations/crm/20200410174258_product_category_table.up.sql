create table if not exists crm_product_categories
(
    uuid            TEXT                        PRIMARY KEY NOT NULL    DEFAULT uuid_generate_v4(),
    store_uuid         TEXT,
    category TEXT,
    store_type TEXT,
   	created_at  TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_product_categories TO crm_apiservice;
