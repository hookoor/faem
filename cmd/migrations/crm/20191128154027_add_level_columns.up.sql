alter table if exists crm_services
ADD COLUMN if not exists level integer;

alter table if exists crm_drivers
ADD COLUMN if not exists max_service_level integer;
