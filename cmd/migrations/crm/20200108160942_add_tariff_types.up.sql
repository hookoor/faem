ALTER TABLE IF EXISTS crm_driver_tariff
    ALTER COLUMN comm_expr SET DEFAULT '';

ALTER TABLE IF EXISTS crm_driver_tariff
    ALTER COLUMN rej_expr SET DEFAULT '';

ALTER TABLE IF EXISTS crm_driver_tariff
    ADD COLUMN IF NOT EXISTS is_secret BOOLEAN NOT NULL DEFAULT false;

ALTER TABLE IF EXISTS crm_driver_tariff
    ADD COLUMN IF NOT EXISTS tariff_type TEXT NOT NULL DEFAULT 'percent'; -- 'percent' or 'period'

ALTER TABLE IF EXISTS crm_driver_tariff
    ADD COLUMN IF NOT EXISTS period REAL NOT NULL DEFAULT 0;

ALTER TABLE IF EXISTS crm_driver_tariff
    ADD COLUMN IF NOT EXISTS period_price NUMERIC(8, 2) NOT NULL DEFAULT 0;
