alter table if exists crm_orders
ADD COLUMN if not exists is_optional boolean NOT NULL DEFAULT false;
