CREATE TABLE crm_events (
    id SERIAL PRIMARY KEY,
    driver_uuid text[],
    client_uuid text[],
    order_uuid text[],
    event text,
    event_trans text,
    value text,
    payload jsonb,
    reason text,
    publisher text,
    event_time timestamp with time zone DEFAULT now(),
    comment text
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_events TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_events_id_seq TO crm_apiservice;
