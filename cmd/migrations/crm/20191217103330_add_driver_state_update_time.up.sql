alter table if exists crm_drivers
ADD COLUMN if not exists status_update_time timestamp with time zone not null DEFAULT now();
