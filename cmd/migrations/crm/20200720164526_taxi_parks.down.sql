alter table if exists crm_drivers 
    drop column if exists taxi_park_uuid;

drop index if exists driver_taxi_park_idx;

alter table if exists crm_users
    drop column if exists taxi_parks_uuid;

ALTER TABLE if exists crm_orders 
    RENAME COLUMN owner_uuid to ow_uuid;

ALTER TABLE if exists crm_orders 
    DROP COLUMN if exists taxi_park_uuid;

ALTER TABLE if exists crm_phonelines
    drop column if exists asterisk_hello;
ALTER TABLE if exists crm_phonelines
    RENAME TO crm_owners;
    
drop table if exists crm_taxi_parks;
drop index if exists order_taxi_park_uuid_idx;
drop index if exists idx__order_store_uuid__timestamp;
