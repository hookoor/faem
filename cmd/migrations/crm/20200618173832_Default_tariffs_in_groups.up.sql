
ALTER TABLE IF EXISTS crm_driver_groups ADD COLUMN IF NOT EXISTS default_tariff_uuid TEXT;
ALTER TABLE IF EXISTS crm_driver_groups ADD COLUMN IF NOT EXISTS default_tariff_offline_uuid TEXT;
