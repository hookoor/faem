CREATE TABLE IF not exists public.crm_tickets
(
    uuid text NOT NULL,
    title text,
    description text,
    source_type text,
    order_data jsonb,
    driver_data jsonb,
    client_data jsonb,
    comments jsonb,
    operator_data jsonb,
    status text,
    created_at timestamp with time zone DEFAULT now()
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_tickets TO crm_apiservice;
