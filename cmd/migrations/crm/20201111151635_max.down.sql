alter table if exists crm_driver_tariff 
    drop column if exists max_complete_commission,
    drop column if exists max_period_commission,
    drop column if exists max_rejection_commission;