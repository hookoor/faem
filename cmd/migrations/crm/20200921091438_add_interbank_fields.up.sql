ALTER TABLE IF EXISTS crm_drivers
    ADD COLUMN IF NOT EXISTS card_balance double precision;

ALTER TABLE IF EXISTS crm_taxi_parks
    ADD COLUMN IF NOT EXISTS card_balance double precision;

ALTER TABLE IF EXISTS crm_taxi_parks
    ADD COLUMN IF NOT EXISTS representative JSONB;
