CREATE TABLE public.crm_increased_fare
(
    id serial,
    steps jsonb NOT NULL,
    created_at timestamp with time zone DEFAULT now()
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_increased_fare TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_increased_fare_id_seq TO crm_apiservice;
