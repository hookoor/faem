
ALTER TABLE IF EXISTS crm_customers DROP COLUMN IF EXISTS default_payment_type;

ALTER TABLE IF EXISTS crm_customers DROP COLUMN IF EXISTS referral_program_data;


DELETE FROM crm_config WHERE key = 'referral_system_params'
