INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'min_driver_app_version',
'{
    "minimal_version": "0.2.285"
}',
'минимальная версия драйверского приложения')
ON CONFLICT(key) DO NOTHING;