ALTER TABLE crm_district DROP COLUMN area;
ALTER TABLE crm_district DROP COLUMN tags;

CREATE TABLE public.crm_polygons
(
    id SERIAL PRIMARY KEY,
    name text,
    tags text[],
    priority int,
    area polygon
)
WITH (
    OIDS = FALSE
);