create extension if not exists "uuid-ossp";
create extension if not exists "postgis";
CREATE TABLE IF NOT EXISTS public.crm_zones
(
    id              SERIAL                      PRIMARY KEY,
    uuid            TEXT                        NOT NULL    DEFAULT uuid_generate_v4(),
    name            TEXT,
    comment         TEXT,
    level           INT                         NOT NULL    DEFAULT -1,
    region          TEXT,
    tags            TEXT[],
    area            GEOMETRY,
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now()
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_zones TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_zones_id_seq TO crm_apiservice;


CREATE TABLE IF NOT EXISTS public.crm_penalties_for_zones
(
    id SERIAL PRIMARY KEY,
    caption text,
    rule_id int,
    priority int,
    start_zone_uuid text,
    finish_zone_uuid text,
    penalty double precision
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_penalties_for_zones TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_penalties_for_zones_id_seq TO crm_apiservice;
