DROP TABLE IF EXISTS crm_sources_of_orders_forwarding;

ALTER TABLE IF EXISTS crm_taxi_parks DROP COLUMN IF EXISTS region_id;
ALTER TABLE IF EXISTS crm_orders DROP COLUMN IF EXISTS source_of_orders_uuid;
ALTER TABLE IF EXISTS crm_orders DROP COLUMN IF EXISTS region_id;

DROP TABLE IF EXISTS crm_sources_of_orders;