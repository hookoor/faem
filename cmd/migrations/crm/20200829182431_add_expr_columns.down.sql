alter table if exists crm_taxi_parks
    drop column if exists complete_expr,
    drop column if exists rejection_expr;