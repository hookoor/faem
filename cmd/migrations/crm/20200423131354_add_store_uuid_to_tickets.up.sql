alter table if exists crm_tickets
add column if not exists store_data jsonb;

alter table if exists crm_orders
add column if not exists send_sms boolean;