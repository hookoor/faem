ALTER TABLE crm_features ADD COLUMN tag text[];
ALTER TABLE crm_drivers ADD COLUMN available_services jsonb;
ALTER TABLE crm_drivers ADD COLUMN available_features jsonb;

