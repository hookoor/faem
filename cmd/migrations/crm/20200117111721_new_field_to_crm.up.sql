alter table if exists crm_orders
add column if not exists state_transfer_time TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP;