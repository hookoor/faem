CREATE TABLE if not exists public.crm_driver_current_app_version
(
    id SERIAL PRIMARY KEY,
    driver_uuid text NOT NULL unique,      
	version text NOT NULL,
    created_at timestamp with time zone not null DEFAULT now(),
    updated_at timestamp with time zone,
    deleted boolean DEFAULT false
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_driver_current_app_version TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_driver_current_app_version_id_seq TO crm_apiservice;
