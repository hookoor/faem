alter table if exists crm_public_places
drop column if exists uuid,
drop column if exists deleted;

drop table if exists crm_destination_points;