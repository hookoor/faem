alter table if exists crm_stores
    drop column if exists need_to_enter;
alter table if exists crm_products
    ALTER COLUMN weight type double precision USING NULLIF(weight, '')::double precision;