ALTER TABLE public.crm_driver_news
	DROP COLUMN IF EXISTS created_time;

ALTER TABLE public.crm_driver_news
	DROP COLUMN IF EXISTS deleted;

ALTER TABLE public.crm_driver_news
	ADD COLUMN IF NOT EXISTS deleted_at timestamp with time zone;
	
ALTER TABLE public.crm_driver_news
	DROP COLUMN IF EXISTS "id";
	
ALTER TABLE public.crm_driver_news 
	DROP CONSTRAINT IF EXISTS pk_uuid_driver_news;
ALTER TABLE public.crm_driver_news 
	ADD CONSTRAINT pk_uuid_driver_news PRIMARY KEY ("uuid");

ALTER TABLE IF EXISTS crm_driver_news
    ADD COLUMN IF NOT EXISTS region_ids int[];
