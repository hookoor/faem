CREATE INDEX if not exists idx_orders_created_at ON crm_orders (created_at);
CREATE INDEX if not exists idx_order_created_at_des ON crm_orders (created_at desc);
CREATE INDEX if not exists idx_order_order_state ON crm_orders (order_state);