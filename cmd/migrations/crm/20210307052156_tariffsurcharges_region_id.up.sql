ALTER TABLE IF EXISTS crm_tariff_surcharges
    ADD COLUMN IF NOT EXISTS region_id int REFERENCES crm_regions(id);
	

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO crm_user;
