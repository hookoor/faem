
DROP TABLE IF EXISTS public.crm_driver_groups;

-- //

CREATE TABLE IF NOT EXISTS public.crm_driver_groups
(
    id                  SERIAL                                  PRIMARY KEY,
    uuid                TEXT                        NOT NULL    UNIQUE,
    name                TEXT,
    description         TEXT,
	type                TEXT                        NOT NULL    DEFAULT 'common',
    expr                TEXT                        NOT NULL    DEFAULT 'false',
    tag                 TEXT,
	belonging_drivers   TEXT[],
    priority            INT,
	payload             JSONB,
    created_at          TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at          TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted             BOOLEAN                     NOT NULL    DEFAULT FALSE
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_driver_groups TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_driver_groups_id_seq TO crm_apiservice;

-- //

ALTER TABLE IF EXISTS crm_drivers DROP COLUMN IF EXISTS driver_group;
