ALTER TABLE IF EXISTS crm_push_mailing
    ADD COLUMN IF NOT EXISTS payload jsonb;