
DROP TABLE IF EXISTS public.crm_zones;

DROP TABLE IF EXISTS public.crm_penalties_for_zones;

DROP extension if exists "uuid-ossp";
