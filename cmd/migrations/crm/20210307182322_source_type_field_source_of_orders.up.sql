ALTER TABLE IF EXISTS crm_sources_of_orders
    ADD COLUMN IF NOT EXISTS source_type text NOT NULL DEFAULT '';
	
ALTER TABLE IF EXISTS crm_sources_of_orders
    ALTER COLUMN source_type DROP DEFAULT;
