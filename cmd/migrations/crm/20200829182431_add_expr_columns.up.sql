alter table if exists crm_taxi_parks
    add column if not exists complete_expr text,
    add column if not exists rejection_expr text;