
CREATE INDEX IF NOT EXISTS idx__crm_driver_states__driver_uuid_and_created_at ON crm_driver_states (driver_uuid, created_at DESC)
