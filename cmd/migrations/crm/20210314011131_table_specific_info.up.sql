create table if not exists public.crm_specific_info
(
    id serial primary key,
    source_id text unique not null references crm_sources_of_orders(uuid),
    user_agreement text default '',
    tarifs text default '',
    confidential_policy text default '',
    insurance_info text default '',
    company_color text default '',
    company_logo text default '',
    company_name text default '',
    created_at timestamp with time zone not null default now()
);

GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public to crm_apiservice;
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_specific_info TO crm_apiservice;