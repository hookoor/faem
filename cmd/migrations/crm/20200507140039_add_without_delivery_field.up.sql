alter table if exists crm_orders
add column if not exists without_delivery boolean;

alter table if exists crm_stores
add column if not exists own_delivery boolean;
