CREATE EXTENSION IF NOT EXISTS postgis;

ALTER TABLE public.crm_district ADD COLUMN polygon geometry;
ALTER TABLE public.crm_district DROP COLUMN area;


DROP TABLE public.crm_buldings;
DROP TABLE public.crm_city;


CREATE TABLE public.crm_addresses
(
    city text COLLATE pg_catalog."default",
    street text COLLATE pg_catalog."default",
    house text COLLATE pg_catalog."default",
    full_address text COLLATE pg_catalog."default",
    lat double precision,
    "long" double precision
)
WITH (
    OIDS = FALSE
);


CREATE TABLE public.crm_public_place
(
    id integer,
    full_name text COLLATE pg_catalog."default",
    category text COLLATE pg_catalog."default",
    street text COLLATE pg_catalog."default",
    building text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    special_order text COLLATE pg_catalog."default",
    out_of_town text COLLATE pg_catalog."default",
    latitude double precision,
    longitude double precision,
    hide_address text COLLATE pg_catalog."default",
    region text COLLATE pg_catalog."default",
    synonym text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    priority integer
)
WITH (
    OIDS = FALSE
);