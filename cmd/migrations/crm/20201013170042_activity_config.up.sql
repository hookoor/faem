INSERT INTO public.crm_config (id, key, value, created_at, updated_at, description)
VALUES ('8cb4a467-86b7-472d-b7c1-39e8a5e25cda',
        'activity_values',
        '{
            "new_driver": {
                "initial_activity": 100,
                "penalty_free_period": 48
            },
            "rush_hours": [
                {
                    "end": {
                        "hour": 9,
                        "minute": 10,
                        "second": 0
                    },
                    "start": {
                        "hour": 7,
                        "minute": 0,
                        "second": 0
                    }
                },
                {
                    "end": {
                        "hour": 19,
                        "minute": 0,
                        "second": 0
                    },
                    "start": {
                        "hour": 16,
                        "minute": 30,
                        "second": 0
                    }
                }
            ],
            "offer_rejected": {
                "default_penalty": 1,
                "distance_penalties": [
                    {
                        "penalty": 4,
                        "distance": 200
                    },
                    {
                        "penalty": 3,
                        "distance": 700
                    },
                    {
                        "penalty": 2,
                        "distance": 1200
                    }
                ],
                "multi_reject_limit": 3,
                "multi_reject_malus": 5,
                "multi_reject_period": 24
            },
            "order_finished": {
                "default_reward": 2,
                "far_order_reward": 4,
                "rush_hours_bonus": 4,
                "far_order_distance": 800
            },
            "order_cancelled": {
                "default_penalty": 7,
                "multi_cancel_limit": 3,
                "multi_cancel_period": 24,
                "multi_cancel_penalty": 21
            },
            "too_long_offline": {
                "days_threshold": 90,
                "default_penalty": 4,
                "activity_lower_limit": 60
            },
            "max_driver_activity": 100,
            "min_driver_activity": 0
        }',
        '2020-09-22 09:51:52.116936', '2020-09-22 09:51:52.116936', 'значения активностей')
ON CONFLICT(key) DO NOTHING;
