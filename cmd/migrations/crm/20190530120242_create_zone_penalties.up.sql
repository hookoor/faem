CREATE TABLE public.crm_zone_penalties
(
    id SERIAL PRIMARY KEY,
    caption text,
    rule_id int,
    priority int,
    start_zone_id int,
    finish_zone_id int,
    penalty double precision
)
WITH (
    OIDS = FALSE
);

