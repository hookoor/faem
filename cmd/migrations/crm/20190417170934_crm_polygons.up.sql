CREATE TABLE public.crm_polygons
(
    id SERIAL PRIMARY KEY,
    name text,
    tags text[],
    priority int,
    area polygon
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.crm_areas_tariffs
(
    id SERIAL PRIMARY KEY,
    from_area int,
    to_area int,
    price int
)
WITH (
    OIDS = FALSE
);