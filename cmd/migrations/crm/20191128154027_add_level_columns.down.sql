alter table if exists crm_services
DROP COLUMN if exists level;

alter table if exists crm_drivers
DROP COLUMN if exists max_service_level;
