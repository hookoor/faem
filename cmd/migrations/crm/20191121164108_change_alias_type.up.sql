ALTER TABLE if exists crm_drivers
DROP COLUMN if exists alias;

ALTER TABLE if exists crm_drivers
ADD COLUMN if not exists alias integer;