alter table if exists crm_products
drop column if exists available;

alter table if exists crm_stores
drop column if exists available;