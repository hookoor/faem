DROP TABLE crm_tariff_prices;

CREATE TABLE public.crm_areas_tariffs
(
    id SERIAL PRIMARY KEY,
    from_area int,
    to_area int,
    price int
)
WITH (
    OIDS = FALSE
);