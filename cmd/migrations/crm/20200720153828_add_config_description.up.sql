
ALTER TABLE IF EXISTS crm_config ADD COLUMN IF NOT EXISTS description TEXT;

INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'forbidden_driver_apps',
'{
  "apps": [
  ]
}',
'запрещенные водительские приложения')
ON CONFLICT(key) DO NOTHING;
