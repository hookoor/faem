ALTER TABLE IF EXISTS public.crm_orders
    DROP COLUMN IF EXISTS driver_rating;

ALTER TABLE IF EXISTS public.crm_orders
    DROP COLUMN IF EXISTS client_rating;
