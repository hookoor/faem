

ALTER TABLE IF EXISTS crm_driver_tariff ADD COLUMN IF NOT EXISTS region_id INT;
ALTER TABLE IF EXISTS crm_driver_tariff ADD COLUMN IF NOT EXISTS taxi_park_uuid TEXT;

ALTER TABLE IF EXISTS crm_driver_groups ADD COLUMN IF NOT EXISTS available_driver_tariffs_uuid TEXT[];

