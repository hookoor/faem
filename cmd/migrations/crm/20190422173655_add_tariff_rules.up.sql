CREATE TABLE public.crm_tariff_rules
(
    id SERIAL PRIMARY KEY,
    name text,
    comment text,
    priority int,
    expr text
)
WITH (
    OIDS = FALSE
);