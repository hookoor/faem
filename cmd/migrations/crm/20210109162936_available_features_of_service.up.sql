
ALTER TABLE IF EXISTS crm_services ADD COLUMN IF NOT EXISTS available_features TEXT[];

ALTER TABLE IF EXISTS crm_services ADD COLUMN IF NOT EXISTS visibility JSONB DEFAULT '{
    "for_operator": true,
    "for_driver": true,
    "for_client": true
}';
