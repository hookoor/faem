ALTER TABLE IF EXISTS crm_orders DROP COLUMN IF EXISTS reason_of_cancellation;
ALTER TABLE IF EXISTS crm_orders DROP COLUMN IF EXISTS reason_of_cancellation_title;
