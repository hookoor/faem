

ALTER TABLE IF EXISTS crm_regions DROP COLUMN IF EXISTS features_set_id;

DROP TABLE IF EXISTS crm_features_set_by_region;

DROP TABLE IF EXISTS crm_features_set_by_service;

-- crm_features_sets дожна удаляться после 
-- crm_features_set_by_region и crm_features_set_by_service из за реляций
DROP TABLE IF EXISTS crm_features_sets;


ALTER TABLE IF EXISTS crm_set_to_service DROP COLUMN IF EXISTS enable_for_driver;
ALTER TABLE IF EXISTS crm_set_to_service DROP COLUMN IF EXISTS visible_for_driver;


DROP TABLE IF EXISTS crm_zzz_m2m_active_services_for_drivers;

DROP TABLE IF EXISTS crm_zzz_m2m_active_features_for_drivers;
