alter table if exists crm_services add column if not exists max_bonus_payment_percent integer;
CREATE INDEX if not exists idx__crm_orders__unrestricted_value_2
    ON public.crm_orders USING gin
    ((routes -> 1 ->> 'unrestricted_value'::text) COLLATE pg_catalog."default" gin_trgm_ops)
    TABLESPACE pg_default;
CREATE INDEX  if not exists idx__crm_orders__unrestricted_value
    ON public.crm_orders USING gin
    ((routes -> 0 ->> 'unrestricted_value'::text) COLLATE pg_catalog."default" gin_trgm_ops)
    TABLESPACE pg_default;