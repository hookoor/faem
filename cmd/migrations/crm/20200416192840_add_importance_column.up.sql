alter table if exists crm_orders 
add column if not exists importance_reasons text[];

alter table if exists crm_products
add column if not exists weight integer;