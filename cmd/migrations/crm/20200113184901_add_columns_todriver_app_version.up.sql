alter table if exists crm_driver_current_app_version
add column if not exists device_model text,     
add column if not exists android_version text;