alter table if exists crm_drivers 
    add column if not exists taxi_park_uuid text;

create index if not exists driver_taxi_park_idx ON crm_drivers (taxi_park_uuid,created_at desc);

alter table if exists crm_users
    add column if not exists taxi_parks_uuid text[];

ALTER TABLE if exists crm_orders
    add COLUMN if not exists taxi_park_uuid text;

ALTER TABLE if exists crm_orders 
    RENAME COLUMN ow_uuid to owner_uuid;

ALTER TABLE if exists crm_owners
    add column if not exists asterisk_hello text;
ALTER TABLE if exists crm_owners
    RENAME TO crm_phonelines;

create index if not exists order_taxi_park_uuid_idx ON crm_orders (taxi_park_uuid,created_at desc);

CREATE INDEX IF NOT EXISTS idx__order_store_uuid__timestamp ON crm_orders((products_data -> 'store' ->> 'uuid'), created_at desc);

create table if not exists crm_taxi_parks
(
    uuid            TEXT                        PRIMARY KEY NOT NULL    DEFAULT uuid_generate_v4(),
    comment         TEXT,
    name         TEXT,
    region_uuid text,
    phonelines_uuid text[],
   	created_at  TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    creator_data jsonb,
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_taxi_parks TO crm_apiservice;

INSERT INTO crm_taxi_parks 
    ("uuid", "name", "comment", "region_uuid", "phonelines_uuid", "creator_data", "created_at", "deleted") 
VALUES 
    (uuid_generate_v4(), 'Стандартный таксопарк', DEFAULT, DEFAULT,array((select uuid from crm_phonelines)), DEFAULT, DEFAULT, DEFAULT)
