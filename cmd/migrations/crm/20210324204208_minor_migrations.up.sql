ALTER TABLE IF EXISTS crm_sources_of_orders_forwarding
  ADD COLUMN IF NOT EXISTS comment TEXT;

COMMENT ON COLUMN public.crm_sources_of_orders_forwarding.comment
IS 'Описание';