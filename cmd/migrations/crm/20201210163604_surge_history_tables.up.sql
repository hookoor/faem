CREATE TABLE IF NOT EXISTS crm_surge_zones
(
    uuid            TEXT                        PRIMARY KEY     DEFAULT uuid_generate_v4(),
    polygon         JSONB,
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL        DEFAULT now()
) WITH ( OIDS = FALSE );
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_surge_zones TO crm_apiservice;

CREATE TABLE IF NOT EXISTS crm_surge_timestamps
(
    uuid        TEXT                        PRIMARY KEY     DEFAULT uuid_generate_v4(),
    created_at  TIMESTAMP WITH TIME ZONE    NOT NULL        DEFAULT now()

) WITH ( OIDS = FALSE );
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_surge_timestamps TO crm_apiservice;

CREATE TABLE IF NOT EXISTS crm_surge_history
(
    uuid            TEXT                        PRIMARY KEY     DEFAULT uuid_generate_v4(),
    zone_uuid       TEXT                        NOT NULL,
    service_uuid    TEXT                        NOT NULL,
    surge_data      JSONB,
    timestamp_uuid  TEXT                        NOT NULL,
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL        DEFAULT now()
) WITH ( OIDS = FALSE );
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_surge_history TO crm_apiservice;
