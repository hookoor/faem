ALTER TABLE crm_customers 
DROP COLUMN IF EXISTS device_id,
DROP COLUMN IF EXISTS telegram_id,
DROP COLUMN IF EXISTS broker_queue,

DROP CONSTRAINT IF EXISTS UC_crm_customers;

ALTER TABLE crm_customers ALTER COLUMN phones TYPE text;
ALTER TABLE crm_customers ALTER COLUMN phones TYPE jsonb USING phones::jsonb;
