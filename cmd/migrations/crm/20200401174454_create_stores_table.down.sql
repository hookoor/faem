drop table if exists crm_stores;

drop table if exists crm_products;

alter table if exists crm_services 
drop column if exists product_delivery;

alter table if exists crm_orders
drop column if exists products_data;

alter table if exists crm_users
drop column if exists store_uuid;