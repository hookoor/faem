ALTER TABLE IF EXISTS crm_orders ADD COLUMN IF NOT EXISTS reason_of_cancellation text;
ALTER TABLE IF EXISTS crm_orders ADD COLUMN IF NOT EXISTS reason_of_cancellation_title text;
