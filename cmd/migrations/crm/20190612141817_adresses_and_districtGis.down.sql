DROP TABLE IF EXISTS public.crm_addresses;
DROP TABLE IF EXISTS public.crm_public_place;
ALTER TABLE public.crm_district DROP COLUMN IF EXISTS polygon;
ALTER TABLE public.crm_district ADD COLUMN area polygon;

CREATE TABLE public.crm_buldings
(
    id SERIAL PRIMARY KEY,
    idx_street integer NOT NULL,
    house character varying COLLATE pg_catalog."default" NOT NULL,
    latitude double precision NOT NULL,
    longitude double precision NOT NULL,
    name character varying COLLATE pg_catalog."default" NOT NULL DEFAULT ''::character varying,
    deleted boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    street_uuid text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);
COMMENT ON TABLE public.crm_buldings
    IS 'Импортированные координаты зданий';

COMMENT ON COLUMN public.crm_buldings.id
    IS 'Код';

COMMENT ON COLUMN public.crm_buldings.idx_street
    IS 'Улица';

CREATE TABLE public.crm_city
(
    id integer NOT NULL,
    name text COLLATE pg_catalog."default",
    kladr_code text COLLATE pg_catalog."default",
    defaultcity boolean,
    zagorod boolean,
    latitude double precision,
    longitude double precision,
    rayonid integer,
    districtid integer,
    deleted boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    CONSTRAINT crm_city_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
);
COMMENT ON TABLE public.crm_city
    IS 'Тут наверно города';


DROP EXTENSION IF EXISTS postgis;
