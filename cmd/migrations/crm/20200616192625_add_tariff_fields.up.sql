create extension if not exists "uuid-ossp";
alter table if exists crm_tariff_rules 
    add column if not exists uuid text not null default uuid_generate_v4(),
    add column if not exists deleted boolean not null default false,
    add column if not exists created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP,
    add column if not exists variables jsonb;