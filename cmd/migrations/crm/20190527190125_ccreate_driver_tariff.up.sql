CREATE TABLE public.crm_driver_tariff
(
    uuid text COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    created_at timestamp with time zone NOT NULL,
    comm_expr text COLLATE pg_catalog."default" NOT NULL,
    rej_expr text COLLATE pg_catalog."default" NOT NULL,
    comment text COLLATE pg_catalog."default",
 deleted boolean,
    CONSTRAINT crm_driver_tariff_pkey PRIMARY KEY (uuid)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;


ALTER TABLE public.crm_drivers
    add column tariff jsonb; 