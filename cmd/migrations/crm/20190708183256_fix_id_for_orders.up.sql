CREATE ROLE admin WITH CREATEDB CREATEROLE;

CREATE SEQUENCE public.crm_orders_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER SEQUENCE public.crm_orders_id_seq
    OWNER TO admin;

GRANT SELECT, USAGE ON SEQUENCE public.crm_orders_id_seq TO crm_apiservice;

GRANT ALL ON SEQUENCE public.crm_orders_id_seq TO admin;

ALTER TABLE crm_orders drop column id;

ALTER TABLE crm_orders add column id integer NOT NULL DEFAULT nextval('crm_orders_id_seq'::regclass);

