CREATE TABLE IF NOT EXISTS crm_sources_of_orders
(
    uuid           		 	TEXT		PRIMARY KEY     DEFAULT uuid_generate_v4(),
	name 					TEXT,
	comment					TEXT,
	default_service_uuid	TEXT,
    taxi_park_uuid          TEXT        NOT NULL REFERENCES crm_taxi_parks(uuid),
    region_id               INT         NOT NULL REFERENCES crm_regions(id),
	
    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE,
    deleted_at      TIMESTAMP WITH TIME ZONE         
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_sources_of_orders TO crm_apiservice;

ALTER TABLE IF EXISTS crm_orders ADD COLUMN IF NOT EXISTS region_id INT REFERENCES crm_regions(id);
ALTER TABLE IF EXISTS crm_orders ADD COLUMN IF NOT EXISTS source_of_orders_uuid TEXT REFERENCES crm_sources_of_orders(uuid);
ALTER TABLE IF EXISTS crm_taxi_parks ADD COLUMN IF NOT EXISTS region_id INT REFERENCES crm_regions(id);


CREATE TABLE IF NOT EXISTS crm_sources_of_orders_forwarding
(
    source_key              TEXT PRIMARY KEY,
    source_of_orders_uuid   TEXT NOT NULL REFERENCES crm_sources_of_orders(uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_sources_of_orders_forwarding TO crm_apiservice;
