INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'min_client_app_version',
'{
    "minimal_version": "0.0.3"
}',
'минимальная версия клиентского приложения')
ON CONFLICT(key) DO NOTHING;