ALTER TABLE IF EXISTS crm_drivers ADD COLUMN IF NOT EXISTS payment_type TEXT;
ALTER TABLE IF EXISTS crm_drivers ADD COLUMN IF NOT EXISTS get_order_radius DOUBLE PRECISION;	
