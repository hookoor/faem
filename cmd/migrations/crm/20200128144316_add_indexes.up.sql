CREATE INDEX IF NOT EXISTS idx__crm_orders__payload_caller_id ON crm_orders USING GIN ((client ->> 'main_phone') gin_trgm_ops);
