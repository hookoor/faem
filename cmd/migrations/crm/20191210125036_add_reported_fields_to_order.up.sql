alter table if exists crm_orders
ADD COLUMN if not exists reported_appointment boolean NOT NULL DEFAULT false;

alter table if exists crm_orders
ADD COLUMN if not exists reported_arrival boolean NOT NULL DEFAULT false;
