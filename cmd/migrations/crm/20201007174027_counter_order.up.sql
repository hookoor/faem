
ALTER TABLE IF EXISTS crm_drivers ADD COLUMN IF NOT EXISTS have_counter_order BOOLEAN DEFAULT false;


INSERT INTO crm_config (id,key,value,description)
VALUES(uuid_generate_v4(),'distribution_params',
'{
  "counter_order_acception_radius": 1000
}',
'настройка параметров распределения')
ON CONFLICT(key) DO NOTHING;
