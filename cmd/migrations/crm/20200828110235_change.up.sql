alter table if exists crm_stores
    add column if not exists need_to_enter boolean not null default true;
alter table if exists crm_products
    ALTER COLUMN weight type text;