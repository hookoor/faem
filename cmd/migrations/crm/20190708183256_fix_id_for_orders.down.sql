ALTER TABLE crm_orders drop column id;
ALTER TABLE crm_orders add column id int;
DROP SEQUENCE crm_orders_id_seq;

DROP ROLE IF EXISTS admin;
