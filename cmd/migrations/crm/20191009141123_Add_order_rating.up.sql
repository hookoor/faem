ALTER TABLE IF EXISTS public.crm_orders
    ADD COLUMN IF NOT EXISTS driver_rating jsonb;

ALTER TABLE IF EXISTS public.crm_orders
    ADD COLUMN IF NOT EXISTS client_rating jsonb;
