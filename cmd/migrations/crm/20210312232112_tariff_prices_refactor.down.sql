ALTER TABLE IF EXISTS public.crm_tariff_prices
   ADD COLUMN IF NOT EXISTS deleted bool DEFAULT FALSE;

UPDATE public.crm_tariff_prices
SET deleted = TRUE 
WHERE deleted_at IS NOT NULL;

ALTER TABLE IF EXISTS public.crm_tariff_prices
   DROP COLUMN IF EXISTS deleted_at;

--

ALTER TABLE IF EXISTS public.crm_tariff_prices
   DROP COLUMN IF EXISTS "default";

--

ALTER TABLE public.crm_tariff_prices
	RENAME COLUMN taximeter TO taximetr;

UPDATE crm_tariff_prices
    SET field_type = 'taximetr'
    WHERE field_type = 'taximeter';

UPDATE crm_tariff_prices
    SET field_type = 'country_taximetr'
    WHERE field_type = 'country_taximeter';
--

 ALTER TABLE public.crm_tariff_prices 
    DROP CONSTRAINT IF EXISTS rule_id_and_field_type_pair_unq;