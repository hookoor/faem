
CREATE TABLE IF NOT EXISTS public.crm_car_brand_reference
(
    id          SERIAL                      PRIMARY KEY,
    brands      JSONB                       NOT NULL,
	colors      JSONB                       NOT NULL,
	created_at  TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now()  
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_car_brand_reference TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.crm_car_brand_reference_id_seq TO crm_apiservice;
