ALTER TABLE IF EXISTS crm_push_mailing
    ADD COLUMN IF NOT EXISTS targets_taxiparks TEXT[];

ALTER TABLE IF EXISTS crm_push_mailing
    ADD COLUMN IF NOT EXISTS targets_regions INT[];