create table if not exists crm_prices_agregator
(
    uuid        text primary key         not null default uuid_generate_v4(),
    address_a   text                     not null,
    address_b   text                     not null,
    length      int                      not null,
    faem        int,
    faem_client int,
    maxim       int,
    yandex      int,
    deleted     bool                     not null default false,
    created_at  timestamp with time zone not null default date_trunc('second', current_timestamp)
);
grant insert, select, update, delete on table public.crm_prices_agregator to crm_apiservice;