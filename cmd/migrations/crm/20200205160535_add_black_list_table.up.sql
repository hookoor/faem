create extension if not exists "uuid-ossp";
create table if not exists crm_blacklist
(
    uuid            TEXT                        PRIMARY KEY NOT NULL    DEFAULT uuid_generate_v4(),
    comment         TEXT,
    phone         TEXT,
    creator_type          TEXT,
    creator_uuid          TEXT,
    creator_name        TEXT,
    created_at_unix      bigint,
    deleted boolean not null default false
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_blacklist TO crm_apiservice;
