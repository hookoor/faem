ALTER TABLE IF EXISTS crm_driver_states
    ADD COLUMN IF NOT EXISTS started_at timestamp with time zone;

UPDATE crm_driver_states
    SET started_at = created_at;
