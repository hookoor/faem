--------------
--- Услуги ---
--------------
ALTER TABLE IF EXISTS crm_services
    ADD UNIQUE (uuid);

CREATE TABLE IF NOT EXISTS crm_services_sets
(
    id          SERIAL                      PRIMARY KEY,
    name        TEXT                        NOT NULL,

    created_at  TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT now(),
    updated_at  TIMESTAMP WITH TIME ZONE,
    deleted_at  TIMESTAMP WITH TIME ZONE
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_services_sets TO crm_apiservice;

CREATE TABLE IF NOT EXISTS crm_set_to_service
(
    id            SERIAL      PRIMARY KEY,
    set_id        INT         NOT NULL REFERENCES crm_services_sets(id),
    service_uuid  TEXT        NOT NULL REFERENCES crm_services(uuid),
    active        BOOLEAN     NOT NULL DEFAULT FALSE,
    UNIQUE (set_id, service_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_set_to_service TO crm_apiservice;
CREATE INDEX IF NOT EXISTS idx__crm_set_to_service__set_id ON crm_set_to_service(set_id);

---------------------------
--- Водительские тарифы ---
---------------------------
CREATE TABLE IF NOT EXISTS crm_driver_tariff_sets
(
    id          SERIAL                      PRIMARY KEY,
    name        TEXT                        NOT NULL,

    created_at  TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT now(),
    updated_at  TIMESTAMP WITH TIME ZONE,
    deleted_at  TIMESTAMP WITH TIME ZONE
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_driver_tariff_sets TO crm_apiservice;

CREATE TABLE IF NOT EXISTS crm_set_to_driver_tariff
(
    id                  SERIAL      PRIMARY KEY,
    set_id              INT         NOT NULL REFERENCES crm_driver_tariff_sets(id),
    driver_tariff_uuid  TEXT        NOT NULL REFERENCES crm_driver_tariff(uuid),
    active              BOOLEAN     NOT NULL DEFAULT FALSE,
    UNIQUE (set_id, driver_tariff_uuid)
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_set_to_driver_tariff TO crm_apiservice;

CREATE INDEX IF NOT EXISTS idx__crm_set_to_driver_tariff__set_id ON crm_set_to_driver_tariff(set_id);

---------------
--- Регионы ---
---------------
CREATE TABLE IF NOT EXISTS crm_regions
(
    id                      SERIAL                      PRIMARY KEY,
    name                    TEXT                        NOT NULL DEFAULT '',
    services_set_id         INT                         NOT NULL REFERENCES crm_services_sets(id),
    origin                  JSONB                       NOT NULL,
    created_at              TIMESTAMP WITH TIME ZONE    NOT NULL DEFAULT now(),
    updated_at              TIMESTAMP WITH TIME ZONE,
    deleted_at              TIMESTAMP WITH TIME ZONE
) WITH (OIDS=FALSE);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE crm_regions TO crm_apiservice;