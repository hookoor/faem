alter table if exists crm_taxi_parks add column if not exists friendly_uuid text[];
alter table if exists crm_taxi_parks add column if not exists unwanted_uuid text[];