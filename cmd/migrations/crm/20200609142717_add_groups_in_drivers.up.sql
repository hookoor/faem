
DROP TABLE IF EXISTS public.crm_driver_groups;

-- //

CREATE TABLE IF NOT EXISTS public.crm_driver_groups
(
    uuid                 TEXT                   NOT NULL    UNIQUE  PRIMARY KEY,
    name                 TEXT,
    description          TEXT,
    belonging_drivers    JSONB,
    distribution_weight  INT,
    services_uuid        TEXT[],
    tags                 TEXT[],

    created_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT now(),
    updated_at      TIMESTAMP WITH TIME ZONE    NOT NULL    DEFAULT CURRENT_TIMESTAMP,
    deleted         BOOLEAN                     NOT NULL    DEFAULT FALSE
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.crm_driver_groups TO crm_apiservice;

-- //
INSERT INTO crm_driver_groups (uuid, name, description, distribution_weight, tags)
VALUES ('48e3255e-8a43-437c-8e63-6fcdbfa6bb15', 'defaul_driver_group','defaul_driver_group',0, '{defaul}');
-- //

ALTER TABLE IF EXISTS crm_drivers ADD COLUMN IF NOT EXISTS driver_group JSONB;
