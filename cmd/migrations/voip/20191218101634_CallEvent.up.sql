CREATE TABLE IF NOT EXISTS aster_events
(
    id SERIAL PRIMARY KEY,
    call_id TEXT NOT NULL DEFAULT '', -- call unique ID
    caller_id TEXT NOT NULL DEFAULT '', -- caller number
    status TEXT NOT NULL DEFAULT 'unknown', -- call status
    uid TEXT NOT NULL DEFAULT '', -- uid for autocall action
    call_timestamp TIMESTAMP WITH TIME ZONE, -- call time
    order_uuid TEXT NOT NULL DEFAULT '', -- order UUID
    operator TEXT NOT NULL DEFAULT '', -- operator
    record TEXT NOT NULL DEFAULT '', -- url to record
    dial_status TEXT NOT NULL DEFAULT '', -- call status
    channel TEXT NOT NULL DEFAULT '', -- event channel
    exten TEXT NOT NULL DEFAULT '', -- asterisk extension
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx__aster_events__caller_id ON aster_events (caller_id);
CREATE INDEX IF NOT EXISTS idx__aster_events__call_id ON aster_events (call_id);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE aster_events TO voip_apiservice;