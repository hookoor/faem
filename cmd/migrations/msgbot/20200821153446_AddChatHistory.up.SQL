-- Таблица для хранения истории чатов
CREATE TABLE IF NOT EXISTS chat_history
(
    source TEXT, -- ID источник заказа
    sender TEXT, -- отправить
    reciever TEXT, -- получатель
    order_uuid TEXT, -- идетификатор заказа
    state TEXT, -- статус заказа
    type TEXT, -- тип сообщения
    client_uuid TEXT, -- тип сообщения
    text TEXT, -- само сообщени
    client_msg_id TEXT, -- id клиента
    chat_id TEXT, -- id чата
    payload jsonb, -- доп инфа
    msg_id TEXT, -- id сообщения
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE chat_history TO msgbot_role;
