
ALTER TABLE IF EXISTS client_orders ADD COLUMN IF NOT EXISTS counter_order_marker BOOLEAN;
ALTER TABLE IF EXISTS client_orders ALTER COLUMN counter_order_marker SET DEFAULT false;
