alter table if exists client_services 
add column if not exists product_delivery boolean;

alter table if exists client_orders
add column if not exists products_data jsonb;