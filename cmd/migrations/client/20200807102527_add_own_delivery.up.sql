ALTER TABLE IF EXISTS client_orders
    ADD COLUMN IF NOT EXISTS own_delivery BOOLEAN,
    ADD COLUMN IF NOT EXISTS estimated_delivery_time TIMESTAMPTZ;
