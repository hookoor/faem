
ALTER TABLE IF EXISTS client_apps ADD COLUMN IF NOT EXISTS referral_program_data JSONB;

UPDATE client_apps SET referral_program_data = CONCAT('{
    "enable": true,
    "referral_code": "',uuid_generate_v4(),'",
    "parent_uuid": null,
    "referral_parent_code": null,
    "referral_parent_phone": null,
    "activation_count": 0,
    "recipients_travel_count": 0
}')::jsonb
WHERE referral_program_data IS NULL;

-- настроить и применить скрипт FillCrmClientReferralData() 
-- "gitlab.com/faemproject/backend/faem/pkg/structures/scripts"
