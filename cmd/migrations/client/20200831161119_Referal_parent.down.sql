
ALTER TABLE IF EXISTS client_apps DROP COLUMN IF EXISTS referral_program_data;
ALTER TABLE IF EXISTS crm_customers DROP COLUMN IF EXISTS referral_program_data;
