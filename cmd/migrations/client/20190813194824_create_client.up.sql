-- Table: public.client_city

CREATE TABLE public.client_city
(
    id serial,
    name text COLLATE pg_catalog."default",
    kladr_code text COLLATE pg_catalog."default",
    defaultcity boolean,
    zagorod boolean,
    latitude double precision,
    longitude double precision,
    rayonid integer,
    districtid integer,
    deleted boolean,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    CONSTRAINT client_city_pkey PRIMARY KEY (id)
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_city TO client_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.client_city_id_seq TO client_apiservice;
