alter table if exists client_orders
ADD COLUMN if not exists is_optional boolean NOT NULL DEFAULT false;
