CREATE TABLE IF NOT EXISTS client_app_versions (
    device_id TEXT PRIMARY KEY,
    os_name TEXT NOT NULL,
    "version" TEXT NOT NULL,
    last_activity timestamp with time zone
);

