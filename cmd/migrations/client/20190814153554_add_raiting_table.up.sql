CREATE TABLE client_order_rating
(
    id SERIAL PRIMARY KEY NOT NULL,  
    order_uuid text,
	driver_uuid text,
	client_uuid text,
    comment text,
    value integer,
	created_at timestamp with time zone DEFAULT now()	
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_order_rating TO client_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.client_order_rating_id_seq TO client_apiservice;
