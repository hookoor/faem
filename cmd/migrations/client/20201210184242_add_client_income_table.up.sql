CREATE TABLE IF NOT EXISTS public.client_income
(
    id          serial PRIMARY KEY,
    client_uuid text                     NOT NULL,
    count       int                      NOT NULL,
    total_count int                      NOT NULL,
    created     timestamp WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_income TO client_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.client_income_id_seq TO client_apiservice;