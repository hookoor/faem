CREATE TABLE public.client_orderstates
(
    id SERIAL PRIMARY KEY,
    created_at timestamp with time zone DEFAULT now(),
    order_uuid text COLLATE pg_catalog."default" NOT NULL,
    driver_uuid text COLLATE pg_catalog."default",
    offer_uuid text COLLATE pg_catalog."default",
    state text COLLATE pg_catalog."default" NOT NULL,
    comment text COLLATE pg_catalog."default", 
    start_state timestamp with time zone NOT NULL
);