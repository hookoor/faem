CREATE TABLE public.client_addresses
(
    city text COLLATE pg_catalog."default",
    street text COLLATE pg_catalog."default",
    house text COLLATE pg_catalog."default",
    full_address text COLLATE pg_catalog."default",
    lat double precision,
    "long" double precision
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.client_addresses
    OWNER to admin;

GRANT ALL ON TABLE public.client_addresses TO admin;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_addresses TO client_apiservice;


CREATE TABLE public.client_public_place
(
    id integer,
    full_name text COLLATE pg_catalog."default",
    category text COLLATE pg_catalog."default",
    street text COLLATE pg_catalog."default",
    building text COLLATE pg_catalog."default",
    comment text COLLATE pg_catalog."default",
    special_order text COLLATE pg_catalog."default",
    out_of_town text COLLATE pg_catalog."default",
    latitude double precision,
    longitude double precision,
    hide_address text COLLATE pg_catalog."default",
    region text COLLATE pg_catalog."default",
    synonym text COLLATE pg_catalog."default",
    name text COLLATE pg_catalog."default",
    priority integer
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.client_public_place
    OWNER to admin;

GRANT ALL ON TABLE public.client_public_place TO admin;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_public_place TO client_apiservice;


CREATE TABLE public.client_streets
(
    id integer NOT NULL,
    ulica character varying COLLATE pg_catalog."default" NOT NULL,
    rayonid integer,
    comment character varying COLLATE pg_catalog."default",
    deleted boolean NOT NULL DEFAULT false,
    tolist boolean NOT NULL DEFAULT false,
    name character varying COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    idx_streetaliens integer,
    canreject boolean NOT NULL DEFAULT false,
    out_of_town boolean NOT NULL DEFAULT false,
    specnotice boolean NOT NULL DEFAULT false,
    ulicacategoryid integer,
    cityid integer NOT NULL,
    displaypriority integer NOT NULL DEFAULT 0,
    kladr_code text COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    fts text COLLATE pg_catalog."default" NOT NULL DEFAULT ''::text,
    latitude double precision,
    longitude double precision,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    uuid text COLLATE pg_catalog."default",
    district_uuid text COLLATE pg_catalog."default",
    city_uuid text COLLATE pg_catalog."default",
    CONSTRAINT client_streets_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.client_streets
    OWNER to admin;

GRANT ALL ON TABLE public.client_streets TO admin;

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_streets TO client_apiservice;