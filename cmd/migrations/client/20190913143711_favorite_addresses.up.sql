CREATE TABLE public.client_favorite_addresses
(
    id SERIAL PRIMARY KEY,
    uuid text NOT NULL,      
	name text,
	description text,
	client_uuid text NOT NULL,
	address jsonb,
    created_at timestamp with time zone DEFAULT now(),
    updated_at timestamp with time zone,
    deleted boolean DEFAULT false
)
WITH (
    OIDS = FALSE
)

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_favorite_addresses TO client_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.client_favorite_addresses_id_seq TO client_apiservice;
