ALTER TABLE IF EXISTS public.client_orders
    ADD COLUMN IF NOT EXISTS visibility boolean;
