CREATE TABLE public.client_firebase_tokens
(
    id SERIAL PRIMARY KEY,
    updated_at timestamp with time zone DEFAULT now(),
    client_uuid text COLLATE pg_catalog."default" UNIQUE,
  token text COLLATE pg_catalog."default"
)
WITH (
    OIDS = FALSE
);
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.client_firebase_tokens TO client_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.client_firebase_tokens_id_seq TO client_apiservice;
