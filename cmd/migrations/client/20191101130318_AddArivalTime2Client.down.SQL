ALTER TABLE IF EXISTS public.client_orders
    DROP COLUMN IF EXISTS arrival_time;

ALTER TABLE IF EXISTS public.client_orders
    DROP COLUMN IF EXISTS callback_phone;
