CREATE INDEX IF NOT EXISTS idx__client_order_states__order_uuid ON client_orderstates (order_uuid , created_at DESC);
CREATE INDEX IF NOT EXISTS idx__client_order_states__driver_uuid ON client_orderstates (driver_uuid , created_at DESC);

