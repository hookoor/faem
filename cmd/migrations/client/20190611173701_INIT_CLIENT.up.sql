CREATE TABLE public.client_apps
(
    id              SERIAL PRIMARY KEY,
    uuid            text UNIQUE,
    name            text,
    karma           bigint,
    main_phone      text,
    phones          text[],
    comment         text,
    device_id       text,
    deleted         boolean,
    broker_queue    text,
    created_at      timestamp with time zone DEFAULT now(),
    updated_at      timestamp with time zone
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_features
(
    id          SERIAL PRIMARY KEY,
    deleted     boolean,
    created_at  timestamp with time zone DEFAULT now(),
    updated_at  timestamp with time zone,
    uuid        text,
    name        text,
    comment     text,
    price       double precision
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_jwt_sessions
(
    id                      SERIAL PRIMARY KEY,
    client_id               bigint,
    client_uuid             text,
    refresh_token           text,
    refresh_token_used      timestamp with time zone,
    refresh_token_expired   timestamp with time zone,
    created_at              timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_orders
(
    id                  SERIAL PRIMARY KEY,
    uuid                text,
    comment             text,
    routes              jsonb,
    features            jsonb,
    tariff              jsonb,
    service             jsonb,
    owner               jsonb,
    client              jsonb,
    order_start         timestamp with time zone,
    cancel_time         timestamp with time zone,
    cl_uuid             text,
    sr_uuid             text,
    callback_phone      text,
    features_uuids      jsonb,
    start_user_uuid     text,
    driver              jsonb,
    appointment_time    timestamp with time zone,
    complete_time       timestamp with time zone,
    pickup_time         timestamp with time zone,
    created_at          timestamp with time zone DEFAULT now(),
    updated_at          timestamp with time zone,
    deleted             boolean,
    order_state         text
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_reg_sessions
(
    uuid        text PRIMARY KEY,
    device_id   text,
    phone       text,
    code        bigint,
    expiration  timestamp with time zone,
    used_at     timestamp with time zone,
    created_at  timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_services
(
    deleted     boolean,
    id          SERIAL PRIMARY KEY,
    created_at  timestamp with time zone DEFAULT now(),
    updated_at  timestamp with time zone,
    uuid        text,
    name        text,
    price_coeff double precision,
    freight     boolean,
    comment     text,
    tag         text[]
)
WITH (
    OIDS = FALSE
);

CREATE TABLE public.client_sessions
(
    id                      SERIAL PRIMARY KEY,
    user_id                 bigint,
    refresh_token           text,
    session_end             timestamp with time zone,
    refresh_token_used      timestamp with time zone,
    refresh_token_expired   timestamp with time zone,
    created_at              timestamp with time zone DEFAULT now()
)
WITH (
    OIDS = FALSE
);