CREATE TABLE if not exists chat_messages
(
    id              SERIAL PRIMARY KEY,
    uuid            text UNIQUE,
    message            text,
    sender           text,
    receiver           text,
    order_uuid           text,
    driver_uuid      text,
    client_uuid          text,
    ack         boolean DEFAULT FALSE,
    created_at TIMESTAMP WITH time zone
)
WITH (
    OIDS = FALSE
);

GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE chat_messages TO  chat_apiservice;
GRANT SELECT, USAGE ON SEQUENCE chat_messages_id_seq TO chat_apiservice;