package cloudstorage

import (
	"context"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"io"
	"mime/multipart"
	"path"
	"strings"

	"cloud.google.com/go/storage"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

const (
	folderFormat = "2006/01/02"
	publicURL    = "https://storage.googleapis.com/%s/%s"

	maxUploadSize = 3e6 // 3MB
)

type Config struct {
	ProjectID  string
	BucketName string
}

var validMimeTypes = map[string]struct{}{
	"image/jpeg": struct{}{},
	"image/png":  struct{}{},
}

// Upload -
type Upload struct {
	FolderName string // "2006/01/02"
	Filename   string
	Filesize   int64
	Filetype   string
	File       multipart.File
}

func (u *Upload) Validate() error {
	if u.Filesize > maxUploadSize {
		return errors.Errorf("file is too large, maximum size is: %v bytes", maxUploadSize)
	}

	if _, found := validMimeTypes[u.Filetype]; !found {
		return errors.New("invalid file type provided")
	}
	return nil
}

func (c Config) Validate() error {
	if c.ProjectID == "" {
		return errors.New("empty cloud storage project id provided")
	}
	if c.BucketName == "" {
		return errors.New("empty cloud storage bucket name provided")
	}
	return nil
}

type GCStorage struct {
	Config Config
	Bucket *storage.BucketHandle
}

func NewGCStorage(ctx context.Context, config Config) (*GCStorage, error) {
	// Validate config first
	if err := config.Validate(); err != nil {
		return nil, err
	}

	// Create a client for google cloud storage
	client, err := storage.NewClient(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "failed to create a storage client")
	}

	bucket := client.Bucket(config.BucketName)

	// _, err = bucket.Attrs(ctx)
	// if err != nil {
	// 	return nil, errors.Wrapf(err, "bucket \"%s\"", config.BucketName)
	// }

	return &GCStorage{
		Config: config,
		Bucket: bucket,
	}, nil
}

// UploadPhoto -
func (s *GCStorage) UploadPhoto(ctx context.Context, upload *Upload) (string, error) {
	if err := upload.Validate(); err != nil {
		return "", err
	}

	var fullname string
	if upload.FolderName != "" {
		fullname = fmt.Sprintf("%s/%s", upload.FolderName, upload.Filename)
	} else {
		fullname = upload.Filename
	}

	// // Generate a random filename, retaining existing extension
	// filename := uuid.Must(uuid.NewV4()).String() + path.Ext(upload.Filename)
	// foldername := time.Now().Format(folderFormat)
	// fullname := fmt.Sprintf("%s/%s", foldername, filename)

	w := s.Bucket.Object(fullname).NewWriter(ctx)

	// storage.AllUsers gives public read access to anyone
	w.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	w.ContentType = upload.Filetype

	if _, err := io.Copy(w, upload.File); err != nil {
		return "", errpath.Err(err)
	}
	if err := w.Close(); err != nil {
		return "", errpath.Err(err)
	}
	// w.Close() - дает ошибку при непонятныцх абстоятельствах.
	// {"PhotocontrolUUID":"7b4e6c1b-6b19-40ae-af53-58c9e7ae2956","event":"PassingPhotocontrolTicket",
	//"level":"error","msg":"(PassingPhotocontrolTicket|609) -\u003e (UploadPhoto|128) -\u003e
	// googleapi: Error 404: Not Found, notFound","reason":"UploadPhoto","time":"2020-10-12T16:15:55Z"}

	return fmt.Sprintf(publicURL, s.Config.BucketName, fullname), nil
}

// UploadImage -
type UploadImage struct {
	FolderName string // "2006/01/02"
	Filename   string
	Filesize   int64
	Filetype   string
	Image      image.Image
}

func (u *UploadImage) validate() error {
	if u.Filesize > maxUploadSize {
		return errors.Errorf("file is too large, maximum size is: %v bytes", maxUploadSize)
	}

	if _, found := validMimeTypes[u.Filetype]; !found {
		return errors.New("invalid file type provided")
	}
	return nil
}

// UploadImage -
func (s *GCStorage) UploadImage(ctx context.Context, upload *UploadImage) (string, error) {
	if err := upload.validate(); err != nil {
		return "", errpath.Err(err)
	}

	var fullname string
	if upload.FolderName != "" {
		fullname = fmt.Sprintf("%s/%s", upload.FolderName, upload.Filename)
	} else {
		fullname = upload.Filename
	}

	w := s.Bucket.Object(fullname).NewWriter(ctx)

	// storage.AllUsers gives public read access to anyone
	w.ACL = []storage.ACLRule{{Entity: storage.AllUsers, Role: storage.RoleReader}}
	w.ContentType = upload.Filetype

	if upload.Filetype == "image/jpeg" {
		err := jpeg.Encode(w, upload.Image, nil)
		if err != nil {
			return "", errpath.Err(err)
		}
	}
	if upload.Filetype == "image/png" {
		err := png.Encode(w, upload.Image)
		if err != nil {
			return "", errpath.Err(err)
		}
	}

	if err := w.Close(); err != nil {
		return "", errpath.Err(err)
	}
	return fmt.Sprintf(publicURL, s.Config.BucketName, fullname), nil
}

// ReassignPicture -
type ReassignPicture struct {
	Name string
	Size tool.PictureSize
}

// DifragPic - в GCStorage везде где встретит файл originPictureName там же сделает файлы reassignPictures. prefix - фильтр по каким папка пройтись
func (s *GCStorage) DifragPic(ctx context.Context, originPictureName string, reassignPictures []ReassignPicture, prefix string) error {
	items := s.Bucket.Objects(ctx, &storage.Query{Prefix: prefix, Versions: false})

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":             "DifragPictures",
		"originPictureName": originPictureName,
		"prefix":            prefix,
	})

	var pn int = 1
	for pn > 0 {
		item, err := items.Next()
		if err != nil {
			errpath.Err(err)
		}
		pn = items.PageInfo().Remaining()

		dir := path.Dir(item.Name)
		base := path.Base(item.Name)
		ext := path.Ext(item.Name)

		if strings.Split(base, ".")[0] == strings.Split(originPictureName, ".")[0] {

			r, err := s.Bucket.Object(item.Name).NewReader(ctx)
			if err != nil {
				errpath.Err(err)
			}

			var img image.Image
			if ext == ".jpg" {
				img, err = jpeg.Decode(r)
				if err != nil {
					errpath.Err(err)
				}
			}
			if ext == ".png" {
				img, err = png.Decode(r)
				if err != nil {
					errpath.Err(err)
				}
			}

			log.Infoln("difrag for:", item.Name)

			for _, el := range reassignPictures {
				cutImg, err := tool.ResizeAndCutPicture(img, el.Size)
				if err != nil {
					errpath.Err(err)
				}
				w := s.Bucket.Object(dir + "/" + el.Name + ext).NewWriter(ctx)
				defer w.Close()

				if ext == ".jpg" {
					err = jpeg.Encode(w, cutImg, nil)
					if err != nil {
						errpath.Err(err)
					}
				}
				if ext == ".png" {
					err = png.Encode(w, cutImg)
					if err != nil {
						errpath.Err(err)
					}
				}
			}

		}
	}
	log.Infoln("difrag successfully")

	return nil
}
