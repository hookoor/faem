package gis

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	// GeometryCollectionEmptyCode - пустая геометрия
	GeometryCollectionEmptyCode string = "0107000020E610000000000000"
	// GeometryCollectionEmpty - пустая геометрия
	GeometryCollectionEmpty string = "GEOMETRYCOLLECTION EMPTY"
	// SRID -
	SRID int = 4326

	// Типы WKT представление геометрии -----

	// Point - точка POINT(0 0)
	Point string = "POINT"
	// Linestring - LINESTRING(0 0,1 1,1 2)
	Linestring string = "LINESTRING"
	// Polygon - POLYGON(0 0,4 0,4 4,0 4,0 0)
	Polygon string = "POLYGON"
	// MultiPoint - MULTIPOINT(0 0,1 2)
	MultiPoint string = "MULTIPOINT"
	// MultiLinestring - MULTILINESTRING((0 0,1 1,1 2),(2 3,3 2,5 4))
	MultiLinestring string = "MULTILINESTRING"
	// MultiPolygon - MULTIPOLYGON(((0 0,4 0,4 4,0 4,0 0),(1 1,2 1,2 2,1 2,1 1)), ((-1 -1,-1 -2,-2 -2,-2 -1,-1 -1)))
	MultiPolygon string = "MULTIPOLYGON"
	// GeometryCollection - GEOMETRYCOLLECTION(POINT(2 3),LINESTRING((2 3,3 4))) (лучше не юзать)
	GeometryCollection string = "GEOMETRYCOLLECTION"
)

// Coordinates - структура кооринаты (замена PureCoordinates на всякий случай)
type Coordinates struct {
	Long float64 `json:"long"`
	Lat  float64 `json:"lat"`
}

// Gis -
type Gis struct {
	db         *pg.DB      // подключение базы
	table      interface{} // структура-таблица
	tableName  string      // название таблицы в которой хранятся геометрии
	columnName string      // имя колонки в которой хранятся геометрии
}

// Geometry - аналог postgis геометрии
type Geometry string

// New - конструктор требует (подключение к базе, объект таблицу, имя колонки с геометрией)
func New(db *pg.DB, table interface{}, columnName string) (Gis, error) {
	if db == nil {
		return Gis{}, errors.Errorf("data base connection is empty")
	}
	if columnName == "" {
		return Gis{}, errors.Errorf("column name is empty")
	}

	tableName, err := getTableName(table)
	if err != nil {
		return Gis{}, err
	}

	return Gis{
		db:         db,
		table:      table,
		tableName:  tableName,
		columnName: columnName,
	}, nil
}

// -----------------------------------------------------------------------------------------------
// Методы для таблицы у которой данные в колонке типа геометрия являются полигоны

// ZonesBelongingToThePoint - возвращает все зоны в которые попала точка.
// Передать адрес объекта-модели(объект-таблица) в качестве параметра "result" дабы в него записался результат работы функции
func (gis Gis) ZonesBelongingToThePoint(point structures.PureCoordinates, result interface{}) error {

	pointString := fmt.Sprintf("[%f, %f]", point.Long, point.Lat)
	queryString := fmt.Sprintf(`SELECT *
		FROM %s
		WHERE  ST_Contains(%s.%s, ST_GeomFromGeoJSON('{
			"type":"Point",
			"crs":{"type":"name","properties":{"name":"EPSG:%v"}},
			"coordinates":%s}'))`,
		gis.tableName, gis.tableName, gis.columnName, SRID, pointString)
	_, err := gis.db.Query(result, queryString)
	if err != nil {
		return fmt.Errorf("(ZonesBelongingToThePoint)query error: %s", err)
	}
	return nil
}

// AllPointsBelongingToAllZonesResultItems - добавка для основной структуры-таблицы для получения вхождений
type AllPointsBelongingToAllZonesResultItems struct {
	// tableName struct{} `pg:",discard_unknown_columns"`
	Intr string `json:"intr" sql:"intr"` // точки принадлежащие пространству
}

// AllPointsBelongingToAllZones - возвращает все точки принадлежащие всем зонам
// !!! Важно: перед вызовом создать объект типа (вложенный тип-таблица + AllPointsBelongingToAllZonesResultItems)
// [
// result := []struct {
// 	Model
// 	gis.AllPointsBelongingToAllZonesResultItems
// }{}
// ].
// Передать адрес объекта в качестве параметра дабы в него записался результат работы функции
func (gis Gis) AllPointsBelongingToAllZones(points []structures.PureCoordinates, result interface{}) error {
	// MULTIPOINT(44.650515 43.035879,44.695323 43.027396,44.696545 43.025247)
	var multipointString string = "'MULTIPOINT("
	for i, point := range points {
		multipointString += fmt.Sprintf("%f %f", point.Long, point.Lat)
		if i != len(points)-1 {
			multipointString += ","
		}
	}
	multipointString += ")'"

	queryString := fmt.Sprintf(`SELECT * FROM (
			SELECT *, ST_AsText(ST_Intersection(%s, ST_GeomFromText(%s,%v))) AS intr
			FROM %s) s
		WHERE intr <> '%s'`,
		gis.columnName, multipointString, SRID, gis.tableName, GeometryCollectionEmpty)

	_, err := gis.db.Query(result, queryString)
	if err != nil {
		return fmt.Errorf("(AllPointsBelongingToAllZones)query error: %s", err)
	}

	return nil
}

// MetersInZonesResultItems - добавка для основной структуры-таблицы для результата работы функции MetersInZones.
type MetersInZonesResultItems struct {
	Intr     Geometry `json:"intr" sql:"intr"`           // маршруты принадлежащие пространству
	IntrText string   `json:"intr_text" sql:"intr_text"` // маршруты принадлежащие пространству в формате WKT (MULTILINESTRING((44.656251 43.034326,44.6551018510072 43.0359507258564),(44.6561632623973 43.0363460104275,44.657307 43.034715)))
	Distance float64  `json:"distance" sql:"distance"`   // расстояние в метрах
}

// MetersInZones - принимает точки маршрута, возвращает количество пройденных метров в зонах
// !!! Важно: перед вызовом создать объект типа (вложенный тип-таблица + MetersInZonesResultItems)
// [
// result := []struct {
// 	Model
// 	gis.MetersInZonesResultItems
// }{}
// ].
// Передать адрес объекта в качестве параметра дабы в него записался результат работы функции
func (gis Gis) MetersInZones(road []structures.PureCoordinates, result interface{}) error {
	// 0.00000000000001 желательно делать так
	// 0.000001 Sprintf("%f",f) позволяет делать только так
	for i := 0; i < len(road)-1; i++ {
		for j := i + 1; j < len(road); j++ {
			if road[i] == road[j] {
				road[j].Long += 0.000001
				road[j].Lat += 0.000001
			}
		}
	}

	roadString := toGoejson(road)

	queryString := fmt.Sprintf(`SELECT * FROM (
			SELECT * ,ST_AsText(intr) as intr_text, ST_Length(intr,true) as distance
			FROM (SELECT *, ST_Intersection(%s, ST_GeomFromGeoJSON('{
					   "type":"LineString", 
					   "crs":{"type":"name","properties":{"name":"EPSG:%v"}}, 																		  
					   "coordinates": %s
					   }')) as intr
				FROM %s) none1) none2
		WHERE (distance <> 0)`,
		gis.columnName, SRID, roadString, gis.tableName)

	_, err := gis.db.Query(result, queryString)
	if err != nil {
		return fmt.Errorf("(MetersInZones)query error: %s", err)
	}

	return nil
}

// =========================
// =========================
// =========================

// RoadLength - расчет длины дороги по координатам в метрах
func (gis Gis) RoadLength(road []structures.PureCoordinates) (float64, error) {
	var result float64
	roadString := toGoejson(road)

	queryString := fmt.Sprintf(`SELECT ST_Length(ST_GeomFromGeoJSON('{
			"type":"LineString",
			"crs":{"type":"name","properties":{"name":"EPSG:%v"}},
			"coordinates": %s
		}'),true) `,
		SRID, roadString)

	_, err := gis.db.Query(&result, queryString)
	if err != nil {
		return 0, fmt.Errorf("(RoadLength)query error: %s", err)
	}

	return result, nil
}

// =========================
// =========================
// =========================

// GeomrtryWKT - WKT представление геометрии.
type GeomrtryWKT struct {
	GeomrtryType string
	Points       [][]structures.PureCoordinates // [номер из мультигеометрии, eсли не мульти то 0][точки]
}

// WKTParse - Парсинг WKT представление геометрии (LINESTRING(0 0,1 1,1 2)).
func WKTParse(wkt string) (GeomrtryWKT, error) {
	var result GeomrtryWKT
	var typee string = ""
	var points string = ""
	for i, char := range wkt {
		if char == '(' {
			typee = wkt[:i]
			points = wkt[i:]
			break
		}
	}

	if typee == Point ||
		typee == Linestring ||
		typee == Polygon ||
		typee == MultiPoint ||
		typee == MultiLinestring ||
		typee == MultiPolygon ||
		typee == GeometryCollection {
		result.GeomrtryType = typee
		if typee == MultiLinestring ||
			typee == MultiPolygon {
			points = points[1 : len(points)-1]
		}
		if typee == MultiPoint {
			var help string = ""
			for _, char := range points {
				if char == ',' {
					help += ")("
					continue
				}
				help += string(char)
			}
			points = help
		}
	} else {
		return GeomrtryWKT{}, fmt.Errorf("(WKTParse)invalid parametr format")
	}

	var recFlag int = 0
	var chars []rune
	for _, char := range points {
		if char == ')' {
			recFlag--
			var coordText [][]string
			for _, st := range strings.Split(string(chars), ",") {
				coordText = append(coordText, strings.Split(string(st), " "))
			}

			var coord structures.PureCoordinates
			var coords = []structures.PureCoordinates{}
			for _, cords := range coordText {
				var longLat bool = false // Long/Lat = false/true
				for _, cor := range cords {
					if !longLat {
						c, err := strconv.ParseFloat(cor, 64)
						if err != nil {
							return GeomrtryWKT{}, fmt.Errorf("(WKTParse)invalid coordinate format")
						}
						coord.Long = c
						longLat = true
						continue
					}
					if longLat {
						c, err := strconv.ParseFloat(cor, 64)
						if err != nil {
							return GeomrtryWKT{}, fmt.Errorf("(WKTParse)invalid coordinate format")
						}
						coord.Lat = c
						longLat = false
					}
					coords = append(coords, coord)
				}
			}
			result.Points = append(result.Points, coords)
			chars = []rune{}
		}
		if recFlag > 0 {
			chars = append(chars, char)
		}
		if char == '(' {
			recFlag++
		}
	}
	if recFlag != 0 {
		return GeomrtryWKT{}, fmt.Errorf("(WKTParse)invalid coordinate format - parentheses")
	}
	return result, nil
}

// =========================
// =========================
// =========================

// множества:
//          ________
//     ____/___     \
//    /   |  2 \  3  |
//   |  1  \____|___/
//    \________/

// Intersection - Возвращает геометрию, которая представляет собой множество точек пересечения заданных геометрий. TODO: (затестить)
// Пересечение A^B (область 2)
func (gis Gis) Intersection(fgeom, sgeom Geometry) (Geometry, error) {
	var res Geometry
	queryString := fmt.Sprintf(`SELECT ST_Intersection(%s, %s) as res FROM %s`, fgeom, sgeom, gis.tableName)

	_, err := gis.db.Query(&res, queryString)
	if err != nil {
		return "nil", fmt.Errorf("(Intersection) -> %s", err)
	}
	return res, nil
}

// Union - возвращает геометрию, которая представляет собой множество точек объединения геометрий. TODO: (затестить)
// Объединение AvB (область 1,2,3)
func (gis Gis) Union(fgeom, sgeom Geometry) (Geometry, error) {
	var res Geometry
	queryString := fmt.Sprintf(`SELECT ST_Union(%s, %s) as res FROM %s`, fgeom, sgeom, gis.tableName)

	_, err := gis.db.Query(&res, queryString)
	if err != nil {
		return "nil", fmt.Errorf("(Union) -> %s", err)
	}
	return res, nil
}

// Difference - Возвращает геометрию, которая представляет собой множество точек геометрии A не пересекающихся с геометрией B. TODO: (затестить)
// Разностью A/B (область 1)
func (gis Gis) Difference(fgeom, sgeom Geometry) (Geometry, error) {
	var res Geometry
	queryString := fmt.Sprintf(`SELECT ST_Difference(%s, %s) as res FROM %s`, fgeom, sgeom, gis.tableName)

	_, err := gis.db.Query(&res, queryString)
	if err != nil {
		return "nil", fmt.Errorf("(Difference) -> %s", err)
	}
	return res, nil
}

// =========================
// =========================
// =========================
//  Кладбище sql
/*
-- принадлежность точки к полигону
SELECT *
FROM postgis_polygon
WHERE  ST_Contains(postgis_polygon.polygon, ST_GeomFromGeoJSON('{
		"type":"Point",
		"crs":{"type":"name","properties":{"name":"EPSG:4326"}},
		"coordinates":[44.666095, 43.024052]}')
)


-- дает отрезок принадлежащий полигону
SELECT ST_AsText(ST_Intersection(a.polygon, ST_GeomFromGeoJSON('{
				   "type":"LineString",
				   "crs":{"type":"name","properties":{"name":"EPSG:4326"}},
				    "coordinates": [ [ 44.655471, 43.037294 ], [ 44.657318, 43.034701 ], [ 44.658334, 43.035083 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ] ]
				   }')))
FROM
	(SELECT *
	FROM postgis_polygon
	WHERE  ST_Contains(postgis_polygon.polygon, ST_GeomFromGeoJSON('{
			"type":"Point",
			"crs":{"type":"name","properties":{"name":"EPSG:4326"}},
			"coordinates":[44.650515, 43.035879]}')
	))a


-- дает список отрезоков принадлежащих полигонам
SELECT * FROM(
	SELECT id, name, polygon, ST_AsText(ST_Intersection(polygon, ST_GeomFromGeoJSON('{
				   "type":"LineString",
				   "crs":{"type":"name","properties":{"name":"EPSG:4326"}},
				    "coordinates": [ [ 44.655471, 43.037294 ], [ 44.657318, 43.034701 ], [ 44.658334, 43.035083 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ], [ 44.656405, 43.037763 ] ]
				   }'))) as insect
	FROM postgis_polygon) b
WHERE (b.insect <> 'GEOMETRYCOLLECTION EMPTY')


-- рсчет длины линии() (должно быть 515+-5(точно  517.3))
-- можно убрать ST_Transform
SELECT ST_Length(ST_Transform(
		ST_GeomFromGeoJSON('{
				   "type":"LineString",
					"crs":{"type":"name","properties":{"name":"EPSG:4326"}},
				    "coordinates": [  [ 44.656251, 43.034326 ], [ 44.655067, 43.036 ], [ 44.656124, 43.036402 ], [ 44.657307, 43.034715 ] ]
				   }'), 4326
),true);


-- дает список отрезоков принадлежащих полигонам и высчитывает их длину в метрах
-- '0107000020E610000000000000' = 'GEOMETRYCOLLECTION EMPTY'
SELECT * ,ST_AsText(insect) as insect_text, ST_Length(insect,true) as dist
FROM (SELECT id, name, polygon, ST_Intersection(polygon, ST_GeomFromGeoJSON('{
				   "type":"LineString",
				   "crs":{"type":"name","properties":{"name":"EPSG:4326"}},
				   "coordinates": [ [ 44.656251, 43.034326 ], [ 44.655067, 43.036 ], [ 44.656124, 43.036402 ], [ 44.657307, 43.034715 ] ]
				   }')) as insect
	FROM postgis_polygon) b
WHERE (b.insect <> '0107000020E610000000000000')
-- "coordinates": [ [ 44.656251, 43.034326 ], [ 44.655067, 43.036 ], [ 44.656124, 43.036402 ], [ 44.657307, 43.034715 ] ]
--  "coordinates": [ [ 44.650515, 43.035879 ], [ 44.652149, 43.033598 ] ]
*/

func toGoejson(road []structures.PureCoordinates) string {
	var roadString string = "[ [ "
	for i, item := range road {
		roadString += fmt.Sprintf("%f, %f", item.Long, item.Lat)
		if i != len(road)-1 {
			roadString += " ], [ "
		}
	}
	roadString += " ] ]"
	return roadString
}

func getTableName(i interface{}) (string, error) {
	v := reflect.ValueOf(i)
	t := v.Type()
	f, ok := t.FieldByName("tableName")
	if ok {
		return f.Tag.Get("sql"), nil
	}
	return "", fmt.Errorf("field tableName in model not found")
}
