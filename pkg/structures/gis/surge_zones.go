package gis

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"math"
)

const (
	degreesPerMeter = 180 / (math.Pi * earthRadius)
)

var OriginPoint = structures.PureCoordinates{Lat: 43.032500, Long: 44.672125}

type Vertex [2]float64

func findVertexFromZoneIndices(zoneSize float64, row, col int) Vertex {
	lat := float64(row) * degreesPerMeter * zoneSize
	lon := float64(col) * degreesPerMeter / math.Cos(OriginPoint.Lat*math.Pi/180) * zoneSize

	lat += OriginPoint.Lat
	lon += OriginPoint.Long

	return Vertex{lon, lat}
}

func CalcSurgeZonePolygon(zoneSize float64, row, col int) []Vertex {
	var polygon []Vertex
	for i := 0; i < 2; i++ {
		for j := 0; j < 2; j++ {
			v := findVertexFromZoneIndices(zoneSize, row+i, col+j)
			polygon = append(polygon, v)
		}
	}
	// fix order of points
	polygon[2], polygon[3] = polygon[3], polygon[2]

	// connect last point with first
	polygon = append(polygon, polygon[0])

	return polygon
}
