package structures

import (
	"fmt"
)

type SurgeData struct {
	FreeDriverCount int `json:"free_driver_count"`
	FreeOrderCount  int `json:"free_order_count"`
}

type SurgeDataV2 struct {
	Drivers map[string]*SurgeDriverInfo `json:"drivers_by_statuses,omitempty"`
	Orders  map[string]*SurgeOrderInfo  `json:"orders_by_states,omitempty"`
}

type SurgeDriverInfo struct {
	DriverCount int `json:"driver_count"`
}

type SurgeOrderInfo struct {
	OrderStartCount int `json:"order_start_count"`
	OrderEndCount   int `json:"order_end_count"`
}

// MakeSurgeZoneUUID simply creates UUID for SurgeZone in this format:
//	{zoneSize}-{row}-{col}
func MakeSurgeZoneUUID(zoneSize float64, row, col int) string {
	size := int(zoneSize)
	return fmt.Sprintf("%d-%d-%d", size, row, col)
}
