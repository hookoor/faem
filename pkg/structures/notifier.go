package structures

import (
	"encoding/json"
)

// SeparationSymbolForChannel -
const SeparationSymbolForChannel = "/"

type Notification struct {
	Channel string `json:"channel"`
	Payload []byte `json:"payload"`
}

func NewNotification(channel string, payload interface{}) (Notification, error) {
	body, err := json.Marshal(payload)
	if err != nil {
		return Notification{}, err
	}
	return Notification{
		Channel: channel,
		Payload: body,
	}, nil
}

// NotificationDesirablePayload -
type NotificationDesirablePayload struct {
	Tag     string      `json:"tag"`
	Title   string      `json:"title"`
	Message string      `json:"message"`
	Payload interface{} `json:"payload"`
}
