package structures

import "time"

// DBConfigKey -
type DBConfigKey string

const ( // имена ключей конфигов
	DBConfigSurge                DBConfigKey = "surge"
	DBConfigForbiddenDriverApps  DBConfigKey = "forbidden_driver_apps"
	DBConfigMinAppVersion        DBConfigKey = "min_app_version"
	DBConfigBlocking             DBConfigKey = "blocking"
	DBConfigActivity             DBConfigKey = "activity"
	DBConfigReferralSystemParams DBConfigKey = "referral_system_params"
	DBConfigDistributionParams   DBConfigKey = "distribution_params"
	DBConfigGodModeParams        DBConfigKey = "god_mode_params"
	DBConfigInsuranceParams      DBConfigKey = "insurance"
)

const (
	MinVerDriverApp DBConfigKey = "driver"
	MinVerClientApp DBConfigKey = "client"
	MinVerAndroidOS DBConfigKey = "Android"
	MinVeriOS       DBConfigKey = "iOS"
)

const (
	// ReferralMessage - сообщение при начислении бонусов по реферальной системе
	ReferralMessage string = "Бонусы по реферальной системе"

	// Сообщения при начислении бонусов после первой поездки
	ReferralMessageFirstRide string = "Бонусы за первую поездку приглашенного пользователя"
)

// ReferralSystemParams - параметры реферальной системы (referral_system_params)
// донор - тот кто пригласил
// реципиент - тот кого пригласили
type ReferralSystemParams struct {
	// DonorTravelBonusesCount - количество начисляемых бонусов донору за поездку реципиента
	DonorTravelBonusesCount int `json:"donor_travel_bonuses_count"`
	// AddForDonor - количество начисляемых бонусов донору при регистрации реципиента
	AddForDonor int `json:"add_for_donor"`
	// AddForRecipient - количество начисляемых бонусов реципиенту за регистрацию по ссылке донора
	AddForRecipient int `json:"add_for_recipient"`
	// ActivationLimit - сколько человек можно пригласить по реферальной ссылке
	ActivationLimit int `json:"activation_limit"`
	// BonusesForRecipientTravelCountLimit - за сколько поездок реципиентов получит бонусы донор
	BonusesForRecipientTravelCountLimit int `json:"bonuses_for_recipient_travel_count_limit"`
	// ReferralURLTemplate - шаблон реферальной ссылки с меткой для подстановки кода
	// meta : (%s - метка для вставки в url)
	ReferralURLTemplate string `json:"referral_url_template"`
}

// --------
// --------
// --------

// DistributionParams - (distribution_params)
type DistributionParams struct {
	// CounterOrderAcceptionRadius -
	CounterOrderAcceptionRadius  int `json:"counter_order_acception_radius"`
	CounterAssignIntervalSeconds int `json:"counter_assign_interval_seconds"`
}

// --------
// --------
// --------

// RefreshGodModeParamsPeriod -
const RefreshGodModeParamsPeriod time.Duration = time.Second * 60

// GodMode -
type GodMode struct {
	WriteoffOfActivity struct { // переключалка списания активности с водителей
		Description string `json:"description"`
		Value       bool   `json:"value"`
	} `json:"writeoff_of_activity"`
	ValidDistanceToGetTariff struct { // максимальное расстояние для рачета тарифа
		Description string  `json:"description"`
		Value       float64 `json:"value"` // value in meters
	} `json:"valid_distance_to_get_tariff"`
	EnableToggleAcceptanceOfCounterOrders struct { // разрешить водителям переключать прием встречных заказов
		Description string `json:"description"`
		Value       bool   `json:"value"`
	} `json:"enable_toggle_acceptance_of_counter_orders"`
	AppsVerificationCode struct {
		Description string `json:"description"`
		Value       int    `json:"value"`
	} `json:"apps_verification_code"`
	OSRM struct {
		Address string `json:"address"`
	} `json:"osrm"`
}

// Init -
func (gm *GodMode) Init() {
	gm.WriteoffOfActivity.Value = true
	gm.ValidDistanceToGetTariff.Value = 200000
	gm.EnableToggleAcceptanceOfCounterOrders.Value = true
	gm.AppsVerificationCode.Value = 1415
}

// --------
// --------
// --------

type InsuranceParams struct {
	// Enabled - включена ли страховка
	Enabled bool `json:"enabled"`
	// Cost - цена страховки, которую доплачивает КЛИЕНТ за каждый заказ
	Cost int `json:"cost"`
	// Messages - строки для фронта
	Messages struct {
		// InfoBox - сообщение во всплывающем окошке
		InfoBox string `json:"info_box"`
		// InsuranceCost - строка для фронт-онли итема тарифа типа "поездка застрахована клиентом"
		InsuranceCost string `json:"insurance_cost"`
		// TotalPriceWithInsurance - строка для фронт-онли итема тарифа типа "стоимость поездки (с учетом страхования) составит"
		TotalPriceWithInsurance string `json:"total_price_with_insurance"`
	} `json:"messages"`
}

// --------
// --------
// --------
