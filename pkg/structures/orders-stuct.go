package structures

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
)

type DestinationPointTypes string

const (
	AddressType     DestinationPointTypes = "address"
	PublicPlaceType DestinationPointTypes = "public_place"
	CityType        DestinationPointTypes = "city"
	StreetType      DestinationPointTypes = "street"

	// типы таксометра по которому будет расчет (таксиметр по времени, по расстоянию, ...)
	CalcWithDist       TariffCalcType = "withdist"
	CalcWithTime       TariffCalcType = "withtime"
	CalcWithFix        TariffCalcType = "withfix"
	OrderDurationCoeff float32        = 1.20 //пока мы не учитываем траффик, будем умножать время поездки на это. Боже, надеюсь это временно
)

// type Route struct {
// 	FullAddress string    `json:"full_address"`
// 	City        Cities    `json:"city"`
// 	Street      Streets   `json:"street"`
// 	Building    Buildings `json:"building"`
// 	CityName    string    `json:"city_name"`
// 	StreetName  string    `json:"street_name"`
// 	Region      string    `json:"region"`
// 	District    string    `json:"district"`
// 	Radius      int       `json:"radius"`
// 	Lat         float32   `json:"lat"`
// 	Lon         float32   `json:"lon"`
// }

// Route - точка назначения в заказе
type Route struct {
	UUID              string                `json:"uuid" `
	PointType         DestinationPointTypes `json:"point_type"`
	UnrestrictedValue string                `json:"unrestricted_value"  description:"полный адрес"`
	ValueForSearch    string                `json:"-" sql:"-"`
	Value             string                `json:"value"  description:"короткий адрес"`
	Country           string                `json:"country"  description:"Страна"`
	Region            string                `json:"region"  description:"Регион"`
	RegionType        string                `json:"region_type"  description:"Тип региона"`
	Type              string                `json:"type"  description:"Тип точки"`
	City              string                `json:"city"  description:"Город"`
	Category          string                `json:"category,omitempty" `
	CityType          string                `json:"city_type"  description:"Тип города"`
	Street            string                `json:"street"  description:"Улица"`
	StreetType        string                `json:"street_type"  description:"Тип улицы"`
	StreetWithType    string                `json:"street_with_type"  description:"Улица с типом"`
	House             string                `json:"house"  description:"Дом"`
	FrontDoor         int                   `json:"front_door" sql:"-"`
	Comment           string                `json:"comment"`
	OutOfTown         bool                  `json:"out_of_town"  description:"Загород ли"`
	HouseType         string                `json:"house_type"  description:"Тип дома"`
	AccuracyLevel     int                   `json:"accuracy_level"  description:"Уровень детализации адреса"`
	Radius            int                   `json:"radius"  description:"Радиус"`
	Lat               float32               `json:"lat"  description:"Широта"`
	Lon               float32               `json:"lon"  description:"Долгота"`
}

type DriverCoordinates struct {
	Coordinates
	CreatedAt time.Time `json:"created_at"`
}

//OrderWithDataForClient - кроме заказа содержит разные полезные данные
type OrderWithDataForClient struct {
	State                string    `json:"state"`
	UUID                 string    `json:"uuid"`
	StateTransferTime    time.Time `json:"state_transfer_time"`
	DriverArrivalTime    int64     `json:"driver_arrival_time"`
	RemainingWaitingTime int64     `json:"remaining_waiting_time"`
	DriverUUID           string    `json:"driver_uuid"`
}

// CoordinatesWithAdditionalData - структура кооринат c доп. даннными
type CoordinatesWithAdditionalData struct {
	Coordinates
	DriverState      string  `json:"driver_state"`
	DriverStateTrans string  `json:"driver_state_trans"`
	Car              string  `json:"car"`
	Alias            int     `json:"alias"`
	Distance         float64 `json:"distance"`
}

// Coordinates - структура кооринаты
type Coordinates struct {
	Lat        float64 `json:"lat"`
	Long       float64 `json:"long"`
	DriverUUID string  `json:"driver_uuid,omitempty"`
}

// ImagesSet - ссылки на загруженные изображения разных форматов
type ImagesSet struct {
	FullFormat   string `json:"full_format,omitempty"`
	SmallFormat  string `json:"small_format,omitempty"`
	MediumFormat string `json:"medium_format,omitempty"`
	HighFormat   string `json:"high_format,omitempty"`
}

// TariffCalcType -
type TariffCalcType string

// Tariff хранит информацию о тарифе (стоимости заказа)
// Не является справочником! Тариф обрабатывается отдельным сервисом
type Tariff struct {
	Name                              string          `json:"name"`
	TotalPrice                        int             `json:"total_price"`
	FixedPrice                        int             `json:"fixed_price"`
	ProductsPrice                     int             `json:"products_price"`
	GuaranteedDriverIncome            int             `json:"guaranteed_driver_income"`
	GuaranteedDriverIncomeForDelivery int             `json:"guaranteed_driver_income_for_delivery"`
	SupplementToGuaranteedIncome      int             `json:"supplement_to_guaranteed_income"` //то, сколько мы доплатил водиле до гарант. дохода
	TariffCalcType                    TariffCalcType  `json:"tariff_calc_type"`                // тип таксометра по которому будет расчет (таксиметр по времени, по расстоянию, ...)
	OrderTripTime                     int64           `json:"order_trip_time"`                 // время выполнения заказа с момента on_place (расчет когда order_payment)
	OrderCompleateDist                float64         `json:"order_compleate_dist"`            // расстояние выполненного заказа (для заказа с 1 роутом)
	OrderStartTime                    int64           `json:"order_start_time"`                // время когда был переведен в on_place
	OrderMinPaymentWithTime           int             `json:"min_payment_with_time"`           // минимальная оплата для заказа по времени
	Currency                          string          `json:"currency"`
	PaymentType                       string          `json:"payment_type"`
	MaxBonusPayment                   int             `json:"max_bonus_payment"`
	BonusPayment                      int             `json:"bonus_payment"`
	Items                             []TariffItem    `json:"items"`
	WaitingBoarding                   map[int]float64 `json:"waiting_boarding"` // стомиость ожидания посадки
	WaitingPoint                      map[int]float64 `json:"waiting_point"`    // стомиость ожидания точки
	TimeTaximeter                     map[int]float64 `json:"time_taximeter"`   // таксиметр по времени
	WaitingPrice                      int             `json:"waiting_price"`    // общая стоимость ожидания
	Surge                             *SurgeItem      `json:"surge"`            // повышенный спрос
	InsuranceCost                     int             `json:"insurance_cost"`
	// Unpaid                            bool            `json:"unpaid"`  для всяких акций типа бесплатной поездки в Беслан
	PreCalculated string `json:"precalculated"` // рассчитанное значение тарифа, которое будет использовано при создании заказа
}

const (
	TariffItemTypeDefault   TariffItemType = "default"
	TariffItemTypeInsurance TariffItemType = "insurance"
)

type TariffItemType string

// TariffItem структура элемента тарифа
type TariffItem struct {
	Name  string         `json:"name"`
	Price int            `json:"price"`
	Type  TariffItemType `json:"type"`
}

// Service структура услуги
type Service struct {
	UUID                   string  `json:"uuid"`
	Name                   string  `json:"name"`
	PriceCoeff             float32 `json:"price_coefficient"`
	Freight                *bool   `json:"freight"`
	ProductDelivery        bool    `json:"product_delivery"`
	Comment                string  `json:"comment"`
	MaxBonusPaymentPercent *int    `json:"max_bonus_payment_percent"`
	//Image поле со ссылкой на иконку сервиса
	Image     string    `json:"image" sql:"image"`
	ImagesSet ImagesSet `json:"images_set" sql:"images_set"`
	Tag       []string  `json:"tag" sql:",type:text[]"`
}

func (sr *Service) GetMaxBonusPayment() int {
	if sr == nil || sr.MaxBonusPaymentPercent == nil {
		return 0
	}
	return *sr.MaxBonusPaymentPercent
}

type FeatureSlice []Feature

// IsZero is used by go-pg to determine if a value has been set
func (fs FeatureSlice) IsZero() bool {
	return fs == nil
}

type OrderPaymentType string

const (
	OrderPaymentTypeCash  OrderPaymentType = "cash" // default
	OrderPaymentTypeCard  OrderPaymentType = "card"
	OrderPaymentTypeOnine OrderPaymentType = "online_transfer"

	OrderPaymentTypeCashRus OrderPaymentType = "Наличные"
	// OrderPaymentTypeCardRus OrderPaymentType = "Карта"
)

var OrderPaymentTypes map[OrderPaymentType]struct{}

func init() {
	OrderPaymentTypes = map[OrderPaymentType]struct{}{
		OrderPaymentTypeCash:  {},
		OrderPaymentTypeCard:  {},
		OrderPaymentTypeOnine: {},
	}
}

// Order базовая структура заказа
type Order struct {
	UUID         string       `json:"uuid" sql:",pk"`
	Comment      *string      `json:"comment"`
	Routes       []Route      `json:"routes"`
	RouteWayData RouteWayData `json:"route_way_data" sql:"route_way_data"`
	Features     FeatureSlice `json:"features"`
	Tariff       Tariff       `json:"tariff"`

	FixedPrice int     `json:"fixed_price" sql:"-"`
	Service    Service `json:"service"`
	// IncreasedFare - прибавка к основной стоимости заказа клиентом (слайдер в мобильном приложении)
	IncreasedFare          float32              `json:"increased_fare"`
	Driver                 Driver               `json:"driver"`
	Client                 Client               `json:"client"`
	Source                 string               `json:"source"`
	ProductsInput          []ProductInputData   `json:"products_input,omitempty" sql:"-"`
	DriverRating           OrderRatingUnit      `json:"driver_rating"`
	ClientRating           OrderRatingUnit      `json:"client_rating"`
	IsOptional             *bool                `json:"is_optional,omitempty"`
	WithoutDelivery        bool                 `json:"without_delivery"`
	OwnDelivery            bool                 `json:"own_delivery"`
	OrderStart             time.Time            `json:"order_start"`
	CancelTime             time.Time            `json:"cancel_time"`
	CreatedAt              time.Time            `json:"created_at"`
	DistributionByTaxiPark *bool                `json:"distribution_by_taxi_park"`
	Promotion              OrderPromotion       `json:"promotion"`
	ArrivalTime            int64                `json:"arrival_time"`
	ProductsData           *ProductsDataInOrder `json:"products_data"`
	CallbackPhone          string               `json:"callback_phone"`
	PaymentType            string               `json:"payment_type"`
	PaymentMeta            OrderPaymentMetadata `json:"payment_meta"`
	EstimatedDeliveryTime  time.Time            `json:"estimated_delivery_time"`

	SourceOfOrders SourceOfOrders `json:"source_of_orders" sql:"owner"`
	TaxiParkShortFields
}

func (or *Order) IsDistributedByTaxiPark() bool {
	if or == nil || or.DistributionByTaxiPark == nil {
		return false
	}
	return *or.DistributionByTaxiPark
}
func (or *Order) GetTaxiParkUUID() string {
	if or == nil || or.TaxiParkUUID == nil {
		return ""
	}
	return *or.TaxiParkUUID
}
func (or *Order) GetTaxiParksUUID() []string {
	if or == nil || or.TaxiParkUUID == nil {
		return nil
	}
	return []string{*or.TaxiParkUUID}
}

// Deprecated: use Tariff.SetTotalPriceForDriver
// SetTotalPriceForDriver ставит общую цену тарифа равной гарант. доходу, если надо
func (or *Order) SetTotalPriceForDriver() {
	if or == nil {
		return
	}
	if or.Tariff.TotalPrice < or.Tariff.GuaranteedDriverIncome+or.Tariff.InsuranceCost {
		or.Tariff.TotalPrice = or.Tariff.GuaranteedDriverIncome + or.Tariff.InsuranceCost
	}
}

func (or *Order) SetTaxiParkData(tp ...TaxiPark) {
	if or == nil || len(tp) == 0 {
		return
	}
	or.TaxiParkData = tp[0]
}
func (or *Order) FromClientApp() bool {
	return or.Source == constants.OrderSourceAndroid ||
		or.Source == constants.OrderSourceIOS
}

func (or *Order) DeliveryRequired() bool {
	return !(or.WithoutDelivery || or.OwnDelivery)
}

func (or *Order) GetProductsData() ProductsDataInOrder {
	if or == nil || or.ProductsData == nil {
		return ProductsDataInOrder{}
	}
	return *or.ProductsData
}

func (or *Order) SurgeAllowed(allowOutOfTown bool) bool {
	if or.Service.ProductDelivery {
		return false
	}
	if !allowOutOfTown && len(or.Routes) > 1 && or.Routes[0].OutOfTown {
		return false
	}
	return true
}

//OrderWithClientHistory for bonuses
type OrderWithClientHistory struct {
	Order
	ClientOrderHistory []time.Time `json:"history"`
	StartZones         []string    `json:"start_zones"`
	FinishZones        []string    `json:"finish_zones"`
}

//OrderUUIDWithMinTime for bonuses
type OrderUUIDWithMinTime struct {
	OrderUUID string    `json:"order_uuid"`
	MinTime   time.Time `json:"min_time"`
}

func (or *Order) GetComment() string {
	if or == nil || or.Comment == nil {
		return ""
	}
	return *or.Comment
}
func (or *Order) ItIsOptional() bool {
	if or.IsOptional == nil {
		return false
	}
	return *or.IsOptional
}

func (or *Order) GetClientPhone() string {
	if or == nil {
		return ""
	}
	if or.CallbackPhone != "" {
		return or.CallbackPhone
	}
	return or.Client.MainPhone
}

func (or *Order) GetPaymentType() string {
	if or == nil || or.PaymentType == "" {
		return constants.OrderPaymentTypeCash // default payment type
	}
	return or.PaymentType
}

type OrderRatingUnit struct {
	Value   int    `json:"value"`
	Comment string `json:"comment"`
}

// RouteWayData - (для преобразования в JSON)
type RouteWayData struct {
	Geometry                RouteToDriverJson   `json:"routes"`
	RouteFromDriverToClient RouteToDriverJson   `json:"route_from_driver_to_client"`
	Routes                  []RouteToDriverJson `json:"steps,omitempty"`
}
type RouteToDriverJson struct {
	Way    GeometryToDriverJson   `json:"geometry"`
	Type   string                 `json:"type"` // "Feature"
	Proper PropertiesToDriverJson `json:"properties"`
}
type GeometryToDriverJson struct {
	Coor [][]float64 `json:"coordinates"`
	Type string      `json:"type"` // "LineString"
}
type PropertiesToDriverJson struct {
	Duration int     `json:"duration"`
	Distance float64 `json:"distance"`
}

type DistributionData struct {
	Rule       string       `json:"rule"`
	ScoreTable ScoreResults `json:"score_results"`
}

// SetTotalPriceForDriver ставит общую цену тарифа равной гарант. доходу, если надо
func (t *Tariff) SetTotalPriceForDriver(driver Driver, guaranteedActivityLimit int) {
	if t == nil {
		return
	}
	if t.TotalPrice < t.GuaranteedDriverIncome+t.InsuranceCost {
		if driver.CanReceiveGuaranteedIncome(guaranteedActivityLimit) {
			t.TotalPrice = t.GuaranteedDriverIncome + t.InsuranceCost
		} else {
			t.GuaranteedDriverIncome = 0
			t.SupplementToGuaranteedIncome = 0
		}
	}
}

type ScoreResults struct {
	DriverUUID string  `json:"driver_uuid"`
	OrderUUID  string  `json:"order_uuid"`
	OfferUUID  string  `json:"offer_uuid"`
	Score      float64 `json:"score"`
	Comment    string  `json:"comment"`
}

// // OrderCRM структура для использования внутри CRM
// // поля OrderFromPhone и CallbackPhone специально дублируются и в заказе
// // теоретически клиент может сделать заказ с другого номера и для кого-то
// type OrderCRM struct {
// 	Order
// 	OrderState      State     `json:"order_state"`
// 	OrderFromPhone  string    `json:"order_from_phone"`
// 	CallbackPhone   string    `json:"callback_phone"`
// 	OwnerID         int       `json:"owner"`
// 	Taxi            Taxi      `json:"taxi"`
// 	StartUserID     int       `json:"start_user_id"`
// 	LastUserID      int       `json:"last_user_id"`
// 	FinishUserID    int       `json:"finish_user_id"`
// 	AppointmentTime time.Time `json:"appointment_time"`
// 	CompleteTime    time.Time `json:"complete_time"`
// 	PickupTime      time.Time `json:"pickup_time"`
// }

// OrderRating оценки заказа
type OrderRating struct {
	ClientUUID string `json:"client_uuid"` // FIXME: Stores the client phone for now in "driver" service
	DriverUUID string `json:"driver_uuid"`
	OrderUUID  string `json:"order_uuid"`
	Value      int    `json:"value"`
	Comment    string `json:"comment"`
}

type UserKarma struct {
	UserUUID string  `json:"user_uuid"`
	Karma    float64 `json:"karma"`
}

type UserBlacklist struct {
	UserUUID  string   `json:"user_uuid"`
	Blacklist []string `json:"blacklist"`
}

type OrderReDistributionData struct {
	OrderUUID         string    `json:"order_uuid"`
	OperatorUUID      string    `json:"operator_uuid"`
	CurrentDriverUUID string    `json:"current_driver_uuid"`
	DistData          time.Time `json:"dist_time"`
}
type UnmarkOrderAsImportant struct {
	OrderUUID    string `json:"order_uuid"`
	OperatorUUID string `json:"operator_uuid"`
}

const OrderSourceKey = "Source"

// Deprecated: Use variables.OrderSources instead
//var OrderSource = map[string]string{
//	"CRM":      "crm",
//	"Lime":     "lime",
//	"Delivery": "deliver_partners",
//	"Android1": "android_client_app_1",
//	"Android2": "android_client_app_2",
//	// "Android3": "android_client_app_3",
//	"IOS1": "ios_client_app_1",
//	"IOS2": "ios_client_app_2",
//	// "IOS3":     "ios_client_app_3",
//	"EdaFaem":        "eda.faem.ru",
//	"PartnerIOS":     "partner_ios",
//	"PartnerWebSite": "partner_web_site",
//	"PartnerAndroid": "partner_android",
//}

type OrderPromotion struct {
	IsVIP    bool `json:"is_vip"`
	IsUnpaid bool `json:"is_unpaid"`
}

type (
	OrderAdditionalPaymentMetadata struct {
		ReceiptURL string `json:"receipt_url"`
		QrCodeURL  string `json:"qr_code_url"`
	}

	OrderPaymentMetadata struct {
		IsPrepaid  bool   `json:"_is_prepaid"`
		ReceiptURL string `json:"receipt_url"`
		QrCodeURL  string `json:"qr_code_url"`

		AdditionalData []OrderAdditionalPaymentMetadata `json:"additional_data"` // if there are multiple receipts per an order
	}
)

func (pm OrderPaymentMetadata) HasPrimaryData() bool {
	return pm.ReceiptURL != ""
}

// TypeActionOnOrder -
type TypeActionOnOrder string

const (
	// ActionOnOrderCancelOrder -
	ActionOnOrderCancelOrder TypeActionOnOrder = "cancel order"
	// ActionOnOrderSetOrderImportant -
	ActionOnOrderSetOrderImportant TypeActionOnOrder = "set order important"
)

// ActionOnOrder -
type ActionOnOrder struct {
	OrderUUID string
	Action    TypeActionOnOrder
}
