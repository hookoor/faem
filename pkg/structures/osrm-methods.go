package structures

// OSRM - описание
type OSRM struct {
	Host string
}

// OSRMData - структура для анмаршалинга ответа от OSRM
type OSRMData struct {
	Routes []struct {
		Geometry struct {
			Coor [][]float64 `json:"coordinates"` // набор координат из которых состоит маршрут
			Type string      `json:"type"`
		} `json:"geometry"`
		Legs []struct {
			Summary  string  `json:"summary"`
			Weight   float64 `json:"weight"`
			Duration float64 `json:"duration"` // время в секундах
			// Steps    []string `json:"steps"`
			Distance float64 `json:"distance"` // расстояние в метрах
		} `json:"legs"`
		WeightName string  `json:"weight_name"`
		Weight     float64 `json:"weight"`
		Duration   float64 `json:"duration"` // время в секундах всего маршрута
		Distance   float64 `json:"distance"` // расстояние в метрах всего маршрута
	} `json:"routes"`

	Waypoints []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"waypoints"`

	Code    string `json:"code"`
	Message string `json:"message"`
}
