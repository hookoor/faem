package structures

import (
	"math"

	"github.com/pkg/errors"
)

const BillingSkipUpdate = math.MinInt32

type BillingAccountType string

const (
	AccountTypeCash  BillingAccountType = "cash"
	AccountTypeCard  BillingAccountType = "card"
	AccountTypeBonus BillingAccountType = "bonus"
)

func (t BillingAccountType) Validate() error {
	if t == AccountTypeCash || t == AccountTypeCard || t == AccountTypeBonus {
		return nil
	}
	return errors.New("unknown account type")
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type BillingUserType string

const (
	UserTypeClient   BillingUserType = "client"
	UserTypeTaxiPark BillingUserType = "taxi_park"
	UserTypeDriver   BillingUserType = "driver"
	UserTypeGateway  BillingUserType = "gateway" // inner usage
	UserTypeOwner    BillingUserType = "owner"   // inner usage
)

func (t BillingUserType) Validate() error {
	if t == UserTypeClient || t == UserTypeDriver || t == UserTypeTaxiPark {
		return nil
	}
	return errors.Errorf("wrong user type: %s", t)
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type BillingTransferType string

const (
	TransferTypeForceSet BillingTransferType = "force"
	TransferTypePayment  BillingTransferType = "payment"
	TransferTypeTopUp    BillingTransferType = "topup"
	TransferTypeWithdraw BillingTransferType = "withdraw"
)

func (t BillingTransferType) Validate() error {
	if t == TransferTypeForceSet || t == TransferTypePayment || t == TransferTypeWithdraw || t == TransferTypeTopUp {
		return nil
	}
	return errors.Errorf("unknown transfer type: %s", t)
}

func (t BillingTransferType) RequiresPayee() bool {
	return t == TransferTypeForceSet || t == TransferTypePayment || t == TransferTypeTopUp
}

func (t BillingTransferType) RequiresPayer() bool {
	return t == TransferTypePayment || t == TransferTypeWithdraw
}

func (t BillingTransferType) GetTranslatedNameFor(accountType BillingAccountType) string {
	result := "неизвестная"
	switch t {
	case TransferTypeForceSet:
		result = "установка администратором"
	case TransferTypePayment:
		result = "перевод"
	case TransferTypeTopUp:
		result = "пополнение"
	case TransferTypeWithdraw:
		result = "списание"
	}

	result += ", счет: "
	switch accountType {
	case AccountTypeBonus:
		result += "баланс"
	case AccountTypeCard:
		result += "кошелек"
	default:
		result += "неизвестный"
	}
	return result
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type TransferGatewayKey string

const (
	// Default keys
	// TransferKeyOrder      TransferGatewayKey = "order"
	// TransferKeyWaiting    TransferGatewayKey = "waiting"

	TransferKeyPrepayDiff TransferGatewayKey = "prepay_diff"
	TransferKeyTip        TransferGatewayKey = "tip"
)

type TariffMetadata struct {
	UUID       string           `json:"uuid"`
	Name       string           `json:"name"`
	TariffType DriverTariffType `json:"tariff_type"`
}

type TaxiParkMetadata struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type TransferMetadata struct {
	OwnerName                string           `json:"owner_name"`
	OfferUUID                string           `json:"offer_uuid"`
	OrderUUID                string           `json:"order_uuid"`
	DriverUUID               string           `json:"driver_uuid"`
	ClientID                 string           `json:"client_id"`
	ClientPhone              string           `json:"client_phone"`
	OperatorUUID             string           `json:"operator_uuid"`
	DriverTariff             TariffMetadata   `json:"driver_tariff"`
	DriverTaxiPark           TaxiParkMetadata `json:"driver_taxi_park"`
	Prepaid                  bool             `json:"prepaid"`
	CurrentDriverBalance     *float64         `json:"current_driver_balance"`
	CurrentDriverCardBalance *float64         `json:"current_driver_card_balance"`
	FeeAmount                float64          `json:"fee_amount"` // must be set in billing service
}

type TransferFee struct {
	AccountType BillingAccountType `json:"account_type"`
	PayerUUID   string             `json:"payer_uuid"`
	PayerType   BillingUserType    `json:"payer_type"`
	Amount      float64            `json:"amount"`
}

type NewTransfer struct {
	IdempotencyKey   string              `json:"idempotency_key"`
	TransferType     BillingTransferType `json:"transfer_type"`
	PayerUUID        string              `json:"payer_uuid"`
	PayerType        BillingUserType     `json:"payer_type"`
	PayerAccountType BillingAccountType  `json:"payer_account_type"`
	PayeeUUID        string              `json:"payee_uuid"`
	PayeeType        BillingUserType     `json:"payee_type"`
	PayeeAccountType BillingAccountType  `json:"payee_account_type"`
	Amount           float64             `json:"amount"`
	Description      string              `json:"description"`
	Fee              TransferFee         `json:"fee"`
	Meta             TransferMetadata    `json:"meta"`
	GatewayKey       TransferGatewayKey  `json:"gateway_key"`
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type CompletedTransfer struct {
	ID               string              `json:"id"`
	TransferType     BillingTransferType `json:"transfer_type"`
	PayerUUID        string              `json:"payer_uuid"`
	PayerType        BillingUserType     `json:"payer_type"`
	PayerAccountType BillingAccountType  `json:"payer_account_type"`
	PayerBalance     float64             `json:"payer_balance"`
	PayeeUUID        string              `json:"payee_uuid"`
	PayeeType        BillingUserType     `json:"payee_type"`
	PayeeAccountType BillingAccountType  `json:"payee_account_type"`
	PayeeBalance     float64             `json:"payee_balance"`
	Amount           float64             `json:"amount"`
	Description      string              `json:"description"`
	Meta             TransferMetadata    `json:"meta"`
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type PrepayOrderMeta struct {
	OfferUUID string `json:"offer_uuid"`
}

type PrepayOrder struct {
	IdempotencyKey string          `json:"idempotency_key"`
	PayerUUID      string          `json:"payer_uuid"`
	SubjectUUID    string          `json:"subject_uuid"`
	Amount         float64         `json:"amount"`
	PayeeUUID      string          `json:"payee_uuid"`
	Meta           PrepayOrderMeta `json:"meta"`
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type ClientBalancesArgs struct {
	Phones []string `json:"phones"`
}

type DriverBalancesArgs struct {
	UserUUIDs []string `json:"user_uuids"`
}
type TaxiParksBalancesArgs struct {
	UUIDs []string `json:"uuids"`
}
type BillingAccountInfo struct {
	Balance float64 `json:"balance"`
}

type BillingAccountsInfo struct {
	Accounts map[BillingAccountType]BillingAccountInfo `json:"accounts"`
}

type BalancesInfo struct {
	UserAccounts map[string]BillingAccountsInfo `json:"user_accounts"`
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type ReceiptGatewayKey string

const (
	// Default keys
	// ReceiptKeyOrder ReceiptGatewayKey = "order"

	ReceiptKeyTip ReceiptGatewayKey = "tip"
)

type ReceiptData struct {
	Amount     float64           `json:"amount"`
	OrderUUID  string            `json:"order_uuid"`
	ClientUUID string            `json:"client_uuid"`
	Label      string            `json:"label"`
	GatewayKey ReceiptGatewayKey `json:"gateway_key"`
}

type ReceiptHook struct {
	OrderUUID string `json:"order_uuid"`
	Url       string `json:"url"`
	QrCodeUrl string `json:"qr_code_url"`
}

// RoundUp rounds to 2 decimal places
func RoundUp(x float64) float64 {
	return math.Round(x*100) / 100
}
