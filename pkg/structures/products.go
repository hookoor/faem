package structures

import (
	"fmt"
)

type (
	// StoreTypes string
	Store struct {
		UUID                       string             `json:"uuid"`
		Name                       string             `json:"name"`
		Phone                      string             `json:"phone"`
		StaffPhones                StringSlice        `json:"staff_phones" sql:",type:text[]"`
		Comment                    string             `json:"comment"`
		NeedToEnter                *bool              `json:"need_to_enter"` // нужно ли заходить в заведение
		OwnDelivery                *bool              `json:"own_delivery"`  // есть ли у заведения собственная доставка
		Image                      string             `json:"image"`         // картинка
		Available                  *bool              `json:"available"`     // может ли заведение принимать заказы
		Visible                    *bool              `json:"visible"`
		SerialNumber               int                `json:"serial_number"` // для управления порядком отображения заведений
		WorkSchedule               []WorkScheduleItem `json:"work_schedule"`
		Type                       string             `json:"type"`
		ProductCategories          []ProductCategory  `json:"product_category" sql:",type:text[]"`
		OrderPreparationTimeSecond *int               `json:"order_preparation_time_second"`
		AverageCheck               int                `json:"average_check"`
		PaymentTypes               []string           `json:"payment_types" sql:",type:text[]"`
		Categories                 []string           `json:"categories" sql:",type:text[]"`
	}
	WorkScheduleItem struct {
		WeekDay       int  `json:"week_day"`
		DayOff        bool `json:"day_off"`
		WorkBeginning int  `json:"work_beginning"`
		WorkEnding    int  `json:"work_ending"`
	}
	Product struct {
		ProductCommonData
		Category ProductCategory  `json:"category"`
		Variants []ProductVariant `json:"variants"`
	}
	ProductCommonData struct {
		UUID          string    `json:"uuid"`
		Weight        string    `json:"weight"`
		WeightMeasure string    `json:"weight_measure"`
		Name          string    `json:"name"`
		Comment       string    `json:"comment"`
		Available     *bool     `json:"available"`
		Visible       *bool     `json:"visible"`
		Price         int       `json:"price"`
		Image         string    `json:"image"`
		StoreUUID     string    `json:"store_uuid"`
		Toppings      []Topping `json:"toppings"`
	}
	ProductVariant struct {
		UUID     string `json:"uuid"`
		Name     string `json:"name"`
		Standard bool   `json:"standard"`
		Price    int    `json:"price"`
		Comment  string `json:"comment"`
	}
	ProductInputData struct {
		UUID         string   `json:"uuid"`
		VariantUUID  string   `json:"variant_uuid"`
		Number       int      `json:"number"`
		ToppingsUUID []string `json:"toppings_uuid"`
	}
	ProductsDataInOrder struct {
		StoreData          Store              `json:"store"`
		PreparationTime    int                `json:"preparation_time"`
		ConfirmationTime   int64              `json:"confirmation_time"`
		Buyout             bool               `json:"buyout"`
		OrderNumberInStore string             `json:"order_number_in_store"`
		Pruducts           []ProductReadyData `json:"products"`
	}
	ProductReadyData struct {
		ProductCommonData
		Number          int            `json:"number"`
		SelectedVariant ProductVariant `json:"selected_variant"`
	}
	Topping struct {
		UUID    string `json:"uuid"`
		Name    string `json:"name"`
		Price   int    `json:"price"`
		Comment string `json:"comment"`
	}
	ProductCategory string
)

// const (
// 	Shop       StoreTypes = "shop"
// 	Pharmacy   StoreTypes = "pharmacy"
// 	Restaurant StoreTypes = "restaurant"
// )

func (it *WorkScheduleItem) IsWorkRoundTheClock() bool {
	return !it.DayOff && it.WorkBeginning == 0 && it.WorkEnding == 0
}
func (st *Store) IsNeedToEnter() bool {
	if st == nil || st.NeedToEnter == nil {
		return false
	}
	return *st.NeedToEnter
}

func (pcd *ProductCommonData) IsAvailable() bool {
	if pcd == nil || pcd.Available == nil {
		return false
	}
	return *pcd.Available
}

func (pcd *ProductCommonData) IsVisible() bool {
	if pcd == nil || pcd.Visible == nil {
		return false
	}
	return *pcd.Visible
}

func (st *Store) IsAvailable() bool {
	if st == nil || st.Available == nil {
		return false
	}
	return *st.Available
}

func (st *Store) IsVisible() bool {
	if st == nil || st.Visible == nil {
		return false
	}
	return *st.Visible
}

func (st *Store) HasOwnDelivery() bool {
	if st == nil || st.OwnDelivery == nil {
		return false
	}
	return *st.OwnDelivery
}

func (st *Store) GetWorkScheduleForDay(day int) (WorkScheduleItem, error) {
	for _, item := range st.WorkSchedule {
		if item.WeekDay == day {
			return item, nil
		}
	}
	return WorkScheduleItem{}, fmt.Errorf("cant find needed week day in store work shedule")
}

// GetOrderPreparationTimeSecond -
func (st *Store) GetOrderPreparationTimeSecond() int {
	if st == nil || st.OrderPreparationTimeSecond == nil {
		return 0
	}
	return *st.OrderPreparationTimeSecond
}
