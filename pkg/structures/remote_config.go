package structures

import (
	"strconv"
	"time"
)

type SurgeType string

const (
	SurgeTypeAbsolute SurgeType = "absolute"
	SurgeTypePercent  SurgeType = "percent"
)

type SurgeItem struct {
	Delta          float64   `json:"delta"`
	Surge          float64   `json:"surge"`
	SurgeCrm       float64   `json:"surge_crm"`
	SurgeType      SurgeType `json:"surge_type"`
	MaxPriceDelta  float64   `json:"max_price_delta"`
	CurDelta       float64   `json:"_cur_delta"`        // for logging
	CurOrderCount  int       `json:"_cur_order_count"`  // for logging
	CurDriverCount int       `json:"_cur_driver_count"` // for logging
	IsFlat         bool      `json:"_is_flat"`          // for logging
}

func NewSurgeItem(values map[string]interface{}) *SurgeItem {
	if values == nil {
		values = make(map[string]interface{})
	}

	iDelta, found := values["delta"]
	if !found {
		return nil
	}
	delta, casted := iDelta.(float64)
	if !casted {
		return nil
	}

	iSurge, found := values["surge"]
	if !found {
		return nil
	}
	surge, casted := iSurge.(float64)
	if !casted {
		return nil
	}

	crmSurgeType, found := values["surge_crm"]
	if !found {
		return nil
	}
	crmSurge, casted := crmSurgeType.(float64)
	if !casted {
		return nil
	}

	iSurgeType, found := values["surge_type"]
	if !found {
		return nil
	}
	surgeType, casted := iSurgeType.(string)
	if !casted {
		return nil
	}

	iMaxPriceDelta, found := values["max_price_delta"]
	if !found {
		return nil
	}
	maxPriceDelta, casted := iMaxPriceDelta.(float64)
	if !casted {
		return nil
	}

	return &SurgeItem{
		Delta:         delta,
		Surge:         surge,
		SurgeCrm:      crmSurge,
		SurgeType:     SurgeType(surgeType),
		MaxPriceDelta: maxPriceDelta,
	}
}

func (s *SurgeItem) HasCorrection() bool {
	if s == nil {
		return false
	}
	return s.Surge != 0
}

func (s *SurgeItem) CalcCorrection(value float64, source string) float64 {
	if s == nil {
		return value
	}

	var newValue float64
	if s.SurgeType == SurgeTypeAbsolute && source == "crm" { // order is from crm
		newValue = value + s.SurgeCrm
	} else if s.SurgeType == SurgeTypeAbsolute {
		newValue = value + s.Surge
	} else if source == "crm" { // percent type AND order is from crm
		coeff := 1 + (s.SurgeCrm / 100)
		newValue = value * coeff
	} else { // percent type
		coeff := 1 + (s.Surge / 100)
		newValue = value * coeff
	}

	if newValue > value+s.MaxPriceDelta {
		newValue = value + s.MaxPriceDelta
	}
	return newValue
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

const (
	DefaultActivityRate = 1.
)

type ActivityPriceItem struct {
	Rate float64 `json:"rate"`
}

func NewActivityPriceItem(values map[string]interface{}) *ActivityPriceItem {
	if values == nil {
		values = make(map[string]interface{})
	}

	rate := DefaultActivityRate
	if iRate, found := values["rate"]; found {
		if val, casted := iRate.(float64); casted {
			rate = val
		}
	}

	return &ActivityPriceItem{Rate: rate}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

const (
	DefaultBlockDriverActivity                 = -1
	DefaultGuaranteedDriverIncomeActivityLimit = -1
)

type BlockingConfig struct {
	BlockDriverActivity                 int
	GuaranteedDriverIncomeActivityLimit int
	RejectionTable                      map[int]time.Duration
}

func NewBlockingConfig(values map[string]interface{}) *BlockingConfig {
	if values == nil {
		values = make(map[string]interface{})
	}

	blockDriverActivity := DefaultBlockDriverActivity
	if iBlockDriverActivity, found := values["block_driver_activity"]; found {
		if val, casted := iBlockDriverActivity.(float64); casted {
			blockDriverActivity = int(val)
		}
	}

	guaranteedDriverIncomeActivityLimit := DefaultGuaranteedDriverIncomeActivityLimit
	if iGuaranteedDriverIncomeActivityLimit, found := values["guaranteed_driver_income_activity_limit"]; found {
		if val, casted := iGuaranteedDriverIncomeActivityLimit.(float64); casted {
			guaranteedDriverIncomeActivityLimit = int(val)
		}
	}

	rejectionTable := make(map[int]time.Duration)
	if iRejectionTable, found := values["rejection_table"]; found {
		if val, casted := iRejectionTable.(map[string]interface{}); casted {
			for keyRejections, iBlockSeconds := range val {
				rejections, err := strconv.ParseInt(keyRejections, 10, 64)
				if err != nil {
					continue
				}
				if blockSeconds, casted := iBlockSeconds.(float64); casted {
					rejectionTable[int(rejections)] = time.Second * time.Duration(blockSeconds)
				}
			}
		}
	}

	return &BlockingConfig{
		BlockDriverActivity:                 blockDriverActivity,
		GuaranteedDriverIncomeActivityLimit: guaranteedDriverIncomeActivityLimit,
		RejectionTable:                      rejectionTable,
	}
}
