package structures

import (
	"fmt"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
)

// MemberFromJWT - возвращает uuid и название мембера
func MemberFromJWT(c echo.Context) (string, ChatMembers, error) {
	user := c.Get("user")
	userTok, check := user.(*jwt.Token)
	if !check {
		return "", "", fmt.Errorf("invalid user field in context")
	}
	claims := userTok.Claims.(jwt.MapClaims)
	clientUUID, check := claims["client_uuid"]
	if check {
		return clientUUID.(string), ClientMember, nil
	}
	driverUUID, check := claims["driver_uuid"]
	if check {
		return driverUUID.(string), DriverMember, nil
	}
	operUUID, check := claims["user_uuid"]
	if check {
		return operUUID.(string), UserCRMMember, nil
	}
	return "", "", fmt.Errorf("invalid uuid field in context")
}
