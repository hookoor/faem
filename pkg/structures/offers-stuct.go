package structures

import (
	"time"
)

const (
	approximateIncreaseInCarArrivalTime         float32 = 1.3
	WaitingOnPointPrefix, WaitingOnTheWayPrefix         = "Ожидание на точке", "Ожидание в пути"
	minArrivalTimeSeconds                               = 45
)

// Offer предложение для водителя
// Response time это Unixtimestamp до которого нужно ответить на оффер
// OfferTime - предлагаемое (рассчетное) время для водителя
// !!!!! Offer СУЩЕСТВУЕТ ТОЛЬКО НА УРОВНЕ
type (
	Offer struct {
		UUID                       string        `json:"uuid"`
		DriverUUID                 string        `json:"driver_uuid"`
		OrderUUID                  string        `json:"order_uuid"`
		OfferType                  string        `json:"offer_type"`
		ResponseTime               int64         `json:"response_time"`
		DriverID                   int           `json:"driver_id"`
		Comment                    string        `json:"comment"`
		AcceptedFromOffline        bool          `json:"accepted_from_offline" sql:"accepted_from_offline,notnull"`
		TripTime                   int64         `json:"trip_time"`
		Order                      Order         `json:"order"`
		TaximetrData               OfferTaximetr `json:"-"`
		RejectedDriversUUID        []string      `json:"-"`
		CounterRejectedDriversUUID []string      `json:"-"`
	}

	// OfferNoOrder структура создана специально для того что бы на Android отправлять
	// без вложенного ордера в оффере
	OfferNoOrder struct {
		UUID         string `json:"uuid"`
		DriverUUID   string `json:"driver_uuid"`
		OrderUUID    string `json:"order_uuid"`
		OfferType    string `json:"offer_type"`
		ResponseTime int64  `json:"response_time"`
		DriverID     int    `json:"driver_id"`
		Comment      string `json:"comment"`
		TripTime     int64  `json:"trip_time"`
		IsFree       bool   `json:"is_free"`
		Activity     struct {
			Accept int `json:"accept"`
			Reject int `json:"reject"`
		} `json:"activity,omitempty"`
		RouteToClient RouteToDriverJson `json:"route_to_client,omitempty"`
		ArrivalTime   int64             `json:"arrival_time"` // virtual field, represents time.Now() + RouteToClient.Duration
	}

	// OfferTaximetr это оффер вместе с данными таксометра для получения водителем
	OfferTaximetr struct {
		OfferData OfferNoOrder `json:"offer"`
		Order     Order        `json:"order"`
		TaxData   TaximetrData `json:"taximeter"`
	}
	// TaximetrData godoc
	TaximetrData struct {
		TaximeterWait     []TaximeterWait `json:"wait"`
		TaximeterDistance []TaximeterItem `json:"distance"`
		TaximeterTime     []TaximeterItem `json:"time"`
	}
	// TaximeterWait godoc
	TaximeterWait struct {
		FreeTime int `json:"free_time"`
		Interval int `json:"interval"` // ( (time/interval(60) - дабы получить минуты))
		RateTime int `json:"rate_time"`
	}
	// TaximeterItem godoc
	TaximeterItem struct {
		Section  int `json:"section"`
		Interval int `json:"interval"`
		Rate     int `json:"rate"`
	}
	OfferStates struct {
		OrderUUID    string    `sql:",notnull" json:"order_uuid"`
		DriverUUID   string    `json:"driver_uuid"`
		OperatorUUID string    `sql:"-" json:"operator_uuid,omitempty"` //for cancelled event
		ArrivalTime  int64     `sql:"-" json:"arrival_time,omitempty"`
		FreeTime     int       `sql:"-" json:"free_time,omitempty"` //оставшееся время ожидания. Заполнено только когда статус waiting или on_place
		OfferUUID    string    `json:"offer_uuid"`
		State        string    `sql:",notnull" json:"state"`
		Comment      string    `json:"comment"`
		StartState   time.Time `sql:",notnull" json:"start_state"`
		// Флаг для обработки специальных случаев в ивентах CRM, например когда заказ взят из свободных, назначен как встречный и т.п.
		Flags OfferStateFlag `sql:"-" json:"flags,omitempty"`
	}
	ClientLocation struct {
		ClienUUID  string  `json:"client_uuid"`
		OrderUUID  string  `json:"order_uuid"`
		Lat        float32 `json:"lat"`
		Lon        float32 `json:"lon"`
		Visibility bool    `json:"visibility"`
	}
	// OrderDeliveredMessage for events
	OrderDeliveredMessage struct {
		DriverUUID string `json:"driver_uuid"`
		OrderUUID  string `json:"order_uuid"`
	}
	NewConferenceData struct {
		ClientPhone string `json:"client_phone"`
		DriverPhone string `json:"driver_phone"`
	}
)

type OfferStateFlag uint8

const (
	OfferStateFlagFreeOrder OfferStateFlag = 1 << iota
	OfferStateFlagCounterOrder
)

func (os OfferStateFlag) Has(flag OfferStateFlag) bool {
	return os&flag > 0
}

func (o *OfferNoOrder) ClearDriverAssignedFields() {
	o.IsFree = false
	o.Activity.Accept, o.Activity.Reject = 0, 0
	o.RouteToClient = RouteToDriverJson{}
	o.ArrivalTime = 0
}

func (o *OfferNoOrder) CalcAndSetArrivalTime() {
	if o.ArrivalTime != 0 {
		return
	}
	// Try to load arrival time from the guessed value calculated by OSRM
	arrivalTime := int64(float32(o.RouteToClient.Proper.Duration) * approximateIncreaseInCarArrivalTime)
	if arrivalTime < minArrivalTimeSeconds {
		arrivalTime = minArrivalTimeSeconds
	}
	o.ArrivalTime = time.Now().Unix() + arrivalTime
}
