package structures

import (
	"fmt"
	"net/http"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/sirupsen/logrus"
)

const CRMPort = 1324
const BonusesPort = 1327
const DriverPort = 1323
const ClientPort = 1325

const AllowedDistance = 2000

const (
	ErrorLevel   = "error"
	WarningLevel = "warning"
	//этот uuid будет в токене voip сервиса
	VoipUUIDForToken          string = "voip_uuid"
	OnplaceAutoCall                  = "AutoCall-OnPlace"
	OnTheWayAutoCall                 = "AutoCall-OnTheWay"
	LongDistibNotificAutoCall        = "long_dist"
	BasicAutoCall                    = "AutoCall"

	//CALL STATUSES
	VOIPCallSatusANSWER = "ANSWER"
	VOIPCallSatusBUSY   = "BUSY"
)

var (
	ResponseStructOk = ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	}
)

// ResponseStruct REST response structs
type ResponseStruct struct {
	Code int    `json:"code"`
	Msg  string `json:"message"`
}
type ErrorWithLevel struct {
	Error error
	Level string
}

func (errWL *ErrorWithLevel) Print(log *logrus.Entry) {
	if errWL == nil || errWL.Error == nil || log == nil {
		return
	}
	if errWL.Level == WarningLevel {
		log.Warning(errWL.Error)
	}
	if errWL.Level == ErrorLevel {
		log.Error(errWL.Error)
	}
}
func Error(err error) *ErrorWithLevel {
	if err == nil {
		return nil
	}
	return &ErrorWithLevel{
		Error: err,
		Level: ErrorLevel,
	}
}
func Errorf(format string, a ...interface{}) *ErrorWithLevel {
	return &ErrorWithLevel{
		Error: fmt.Errorf(format, a...),
		Level: ErrorLevel,
	}
}
func Warningf(format string, a ...interface{}) *ErrorWithLevel {
	return &ErrorWithLevel{
		Error: fmt.Errorf(format, a...),
		Level: WarningLevel,
	}
}
func Warning(err error) *ErrorWithLevel {
	if err == nil {
		return nil
	}
	return &ErrorWithLevel{
		Error: err,
		Level: WarningLevel,
	}
}

// DataWithMarker - обертка с маркером для структуры
type DataWithMarker struct {
	Marker   string `json:"marker"`
	Features []Feature
	Services []Service
}

type NewSMSData struct {
	Text  string
	Phone string
}

// PureCoordinates - исключительно структура кооринаты
type PureCoordinates struct {
	Lat  float64 `json:"lat"`
	Long float64 `json:"long"`
}

type (
	// OrderStateWithToken order state with recipient token
	OrderStateWithToken struct {
		ClientUUID          string `json:"client_uuid"` // для centrifugo
		Token               string `json:"token"`
		ProductDeliveryData OrderDeliveryData
		OrdState            OfferStates `json:"order_states"`
	}
	OrderDeliveryData struct {
		IsProductsDelivery bool `json:"is_products_delivery"`
		WithoutDelivery    bool `json:"without_delivery"`
		OwnDelivery        bool `json:"own_delivery"`
	}
	// DriverLocWithToken driver location with recipient token
	DriverLocWithToken struct {
		ClientUUID string      `json:"client_uuid"` // для centrifugo
		Token      string      `json:"token"`
		DriverLoc  Coordinates `json:"driver_loc"`
	}
	// DriverLocWithToken driver location with recipient token
	InitDataStruct struct {
		ClientUUID  string `json:"client_uuid"` // для centrifugo
		Token       string `json:"token"`
		NotifyTitle string `json:"notify_title"`
		NotifyBody  string `json:"notify_body"`
	}
)

//type AutoCallType string

const DriverRegType string = "driver_reg"
const RemindDriverPassType string = "driver_remind_pass"

//const AboutLongDistribution AutoCallType = "long_dist"
const CLientNotification string = "notification"

// AutoOutCallRequest rest request to synth and call
type AutoOutCallRequest struct {
	Text       string    `json:"text"`              // текст для синтеза
	Emotion    string    `json:"emotion,omitempty"` // эмоция - [good, evil, neutral]
	Type       string    `json:"type"`              // тип - [driver_reg]
	Voice      string    `json:"voice,omitempty"`   // голос [alyss, jane, oksana, omazh], необязательные
	Phone      string    `json:"phone"`             // номер телефона
	CreatedAt  time.Time `json:"created_at"`        // время создания
	OrderUUID  string    `json:"order_uuid"`        // UUID заказа
	OrderState string    `json:"order_state"`       // статус заказа для которого актулен звонок
}

//type TelephonyCall struct {
//	Status     string `json:"status" query:"status"`
//	CallerID   string `json:"callerid" query:"callerid"`
//	Exten      string `json:"exten" query:"exten"`
//	Uid        string `json:"uid" query:"uid"`
//	OrderUUID  string `json:"order_uuid" query:"order_uuid"`
//	Date       string `json:"date" query:"date"`
//	ID         uint64 `json:"id" query:"id"`
//	Operator   string `json:"operator" query:"operator"`
//	Record     string `json:"rec" query:"rec"`
//	DialStatus string `json:"dial-status" query:"dial-status"`
//}

type TelephonyCall struct {
	Status    string    `json:"status"`     // статус звонка
	Record    string    `json:"rec"`        //ссылка на запись
	CallerID  string    `json:"caller_id"`  //номер звонившего
	CallID    string    `json:"call_id"`    //id звонка
	StartTime time.Time `json:"start_time"` //время завершения звонка
	EndTime   time.Time `json:"end_time"`   //время завершения звонка
	Exten     string    `json:"exten"`      //номер на который звонили
	Operator  string    `json:"operator"`   //номер последнего оператора
	Uid       string    `json:"uid"`        //идетификатор авто-звонка
	OrderUUID string    `json:"order_uuid"` //UUID заказа
	Type      string    `json:"type"`       //Тип звонка [Autocall|Incoming|Outgoing]
}

type OrderUUIDWithStateTransferState struct {
	UUID           string    `json:"uuid"`
	StateTransTime time.Time `json:"state_trans_time"`
	State          string    `json:"state"`
	DriverUUID     string    `json:"driver_uuid"`
}

type DeletedObject struct {
	UUID string `json:"uuid"`
}

type OrdersDataByStatesArgs struct {
	States   []string `json:"states"`
	RegionID int      `json:"region_id,omitempty"`
}

func SortByUnique(old []string) (new []string) {
	alreadyAdded := func(str string) bool {
		for _, item := range new {
			if item == str {
				return true
			}
		}
		return false
	}
	for _, item := range old {
		if alreadyAdded(item) {
			continue
		}
		new = append(new, item)
	}
	return
}
func StringInArray(str string, arr []string) bool {
	for _, val := range arr {
		if val == str {
			return true
		}
	}
	return false
}

func IntInArray(in int, arr []int) bool {
	for _, val := range arr {
		if val == in {
			return true
		}
	}
	return false
}

// ClientTokenClaim содержание самого клиентского токена
type ClientTokenClaim struct {
	ClientUUID  string `json:"client_uuid"`
	DeviceID    string `json:"device_id"`
	Phone       string `json:"phone"`
	RegTime     string `json:"reg_time"`
	ServiceName string `json:"service_name"`
	jwt.StandardClaims
}

// SmartphoneApp - содержит данные по мобильному приложению
type SmartphoneApp struct {
	Name        string `json:"name"`
	AppID       string `json:"app_id"`
	Description string `json:"description"`
}
