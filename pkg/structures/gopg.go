package structures

// StringSlice - для записи пустого значения при UpdateNotNull
type StringSlice []string

// IsZero is used by go-pg to determine if a value has been set
func (ss StringSlice) IsZero() bool {
	return ss == nil
}
