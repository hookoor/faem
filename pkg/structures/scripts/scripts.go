package scripts

import (
	"encoding/json"
	"fmt"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

// FillCrmClientReferralData - Передает данные реферальной системы клиентов из таблицы crm_customers в client_apps
// meta: используется при миграции 20200831161119_Referral_parent.up
func FillCrmClientReferralData() error {
	var err error
	var dbCRM *pg.DB
	var dbClient *pg.DB

	// connect - создает соединение с базой
	connect := func(addr, user, passwd, base string) (*pg.DB, error) {
		conn := pg.Connect(&pg.Options{
			Addr:     addr,
			User:     user,
			Password: passwd,
			Database: base,
		})

		return conn, nil
	}

	dbCRM, err = connect(
		"localhost:5432",
		"superuser",
		"superuser",
		"mycrm",
	)
	defer dbCRM.Close()
	if err != nil {
		return errpath.Err(err)
	}

	dbClient, err = connect(
		"localhost:5432",
		"superuser",
		"superuser",
		"myclient",
	)
	defer dbClient.Close()
	if err != nil {
		return errpath.Err(err)
	}

	// ReferralProgramData -
	type ReferralProgramData struct {
		Enable              bool   `json:"enable"`
		ReferralCode        string `json:"referral_code"`
		ParentUUID          string `json:"parent_uuid"`
		ReferralParentUUID  string `json:"referral_parent_code"`
		ReferralParentPhone string `json:"referral_parent_phone"`
		ActivationCount     int    `json:"activation_count"`
	}

	// var queryString string

	type Client struct {
		tableName           struct{}            `sql:"client_apps"`
		MainPhone           string              `json:"main_phone"`
		ReferralProgramData ReferralProgramData `json:"referral_program_data"`
	}

	var Clients []Client

	err = dbClient.Model(&Clients).
		Column("main_phone", "referral_program_data").
		Select()
	if err != nil {
		return errpath.Err(err)
	}

	{ // --------------------------
		var queryString string

		queryString += `
		CREATE TEMPORARY TABLE temp (
			main_phone text,
			referral_program_data jsonb
		);
		`

		queryString += "insert into temp values\n"
		for i, sp := range Clients {
			jsn, err := json.Marshal(sp.ReferralProgramData)
			if i+1 == len(Clients) {

				if err != nil {
					return errpath.Err(err)
				}

				queryString += fmt.Sprintf("('%s','%s');\n", sp.MainPhone, string(jsn))
				break
			}
			queryString += fmt.Sprintf("('%s','%s'),\n", sp.MainPhone, string(jsn))
		}

		queryString += `
			UPDATE crm_customers as st SET referral_program_data = (select referral_program_data from temp where temp.main_phone = st.main_phone)
			where (select referral_program_data from temp where temp.main_phone = st.main_phone) is not null;`
		queryString += `
		drop table temp;`

		_, err = dbCRM.Query(nil, queryString)
		if err != nil {
			return errpath.Err(err)
		}

	}

	fmt.Println("Count of clients", len(Clients))

	return nil
}
