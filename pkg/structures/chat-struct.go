package structures

import "time"

type (
	ChatMembers  string
	MailingTypes string
)

const (
	MailingAllClientsType     MailingTypes = "mailing_all_clients"
	MailingForFewClientType   MailingTypes = "mailing_few_client"
	MailingAllDriversType     MailingTypes = "mailing_all_drivers"
	MailingRegionalDrivers    MailingTypes = "mailing_regional_drivers"
	MailingDriversGroupType   MailingTypes = "mailing_drivers_group"
	MailingInformDriverType   MailingTypes = "mailing_driver_inform"
	MailingOnlineDriversType  MailingTypes = "mailing_drivers_online"
	MailingOfflineDriversType MailingTypes = "mailing_drivers_offline"
	MailingOwnerDriversType   MailingTypes = "mailing_drivers_by_owner"

	UserCRMMember ChatMembers = "operator"
	DriverMember  ChatMembers = "driver"
	ClientMember  ChatMembers = "client"

	TelegramBotMember ChatMembers = "telegram_bot"

	MsgBotService              ChatMembers = "msg_bot_service"        //источник заказов
	MsgBotTelegramUserSender   ChatMembers = "tlgrm_user_sender"      //отправитель telegram пользователь
	MsgBotTelegramUserReciever ChatMembers = "tlgrm_user_reciever"    //отправитель telegram пользователь
	MsgBotWhatsAppUserSender   ChatMembers = "whatsapp_user_sender"   //отправитель whatsapp пользователь
	MsgBotWhatsAppUserReciever ChatMembers = "whatsapp_user_reciever" //отправитель whatsapp пользователь
	MsgBotSender               ChatMembers = "bot_sender"             //получатель бот
	MsgBotReciever             ChatMembers = "bot_reciever"           //получатель бот
	MsgBotSupportReciever      ChatMembers = "support_reciever"       //получатель служба поддержки
	MsgBotSupportSender        ChatMembers = "support_sender"         //отправитель служба поддержки

)

func AllChatMembers() []ChatMembers {
	return []ChatMembers{
		UserCRMMember,
		ClientMember,
		DriverMember,
	}
}
func AllMailingTypes() []MailingTypes {
	return []MailingTypes{
		MailingAllClientsType,
		MailingAllDriversType,
		MailingOfflineDriversType,
		MailingDriversGroupType,
		MailingInformDriverType,
		MailingOnlineDriversType,
		MailingOwnerDriversType,
	}
}

// ChatMessagesMarkedAsRead структура для информирования о том что какие то сообщения прочитаны
type ChatMessagesMarkedAsRead struct {
	ReaderType   ChatMembers `json:"reader_type"`
	SenderType   ChatMembers `json:"sender_type"`
	ReaderUUID   string      `json:"reader_uuid"`
	OrderUUID    string      `json:"order_uuid"`
	MessagesUUID []string    `json:"messages_uuid"`
}

type ChatMessages struct {
	UUID          string      `json:"uuid"`
	Message       string      `json:"message"`
	Ack           bool        `json:"ack"`
	OrderUUID     string      `json:"order_uuid"`
	Sender        ChatMembers `json:"from"`
	Receiver      ChatMembers `json:"to"`
	CreatedAtUnix int64       `json:"created_at" sql:"-"`
	DriverUUID    string      `json:"driver_uuid"`
	ClientUUID    string      `json:"client_uuid"`
}
type PushMailing struct {
	Type             MailingTypes `json:"type"`
	TargetsUUIDs     []string     `json:"targets_uuid" sql:"targets_uuids,type:text[]"`
	TargetsPhones    []string     `json:"targets_phones" sql:"-"`
	TargetsRegions   []int        `json:"targets_regions" sql:",type:int[]"`
	TargetsTaxiparks []string     `json:"targets_taxiparks" sql:",type:text[]"`
	Title            string       `json:"title"`
	Message          string       `json:"message"`
	Payload          *PushPayload `json:"payload,omitempty"`
}

type PushPayload struct {
	URL string `json:"message_url"`
}

type MessageFromBot struct {
	Source       string    `json:"source"`        // источник сообщшения
	ClientMsgID  string    `json:"client_msg_id"` // id пользователю внутри мессенджера
	UserLogin    string    `json:"user_login"`    // login пользователю внутри мессенджера
	ChatMsgID    string    `json:"chat_msg_id"`   // id chat-a внутри мессенджера
	MsgID        string    `json:"msg_id"`        // id сообщения
	Text         string    `json:"text"`          //message text or payload data
	Receiver     string    `json:"receiver"`      //получатель
	Sender       string    `json:"sender"`        //отправитель
	CreatedAt    time.Time `json:"created_at"`
	OrderUUID    string    `json:"order_uuid"`
	ClientUUID   string    `json:"client_uuid"`
	MsgUUID      string    `json:"msg_uuid"`
	CreatedAtMsg time.Time `json:"created_at_msg"` // время создания в мессенджере
}

type OrdersUUIDForUnreadMessages struct {
	OrderUUIDs []string `json:"order_uuids"`
}
