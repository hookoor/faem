package tool

import (
	"image"
	"math"
	"reflect"

	"github.com/nfnt/resize"
	"github.com/oliamb/cutter"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

// RoundTo - округляет число x до prec знаков после запятой
func RoundTo(x float64, prec int) float64 {
	pow := math.Pow(10, float64(prec))

	return math.Round(x*pow) / pow
}

// Divide -
func Divide(numerator, denominator float64) float64 {
	if numerator == 0 || denominator == 0 {
		return 0
	}
	return numerator / denominator
}

// ResizeAndCutPicture - пропорционально сжимает затем обрезает изображение до нужного размера
func ResizeAndCutPicture(img image.Image, neededSize PictureSize) (image.Image, error) {
	var resizeImg image.Image

	widthForResize := float64(img.Bounds().Dx()) / float64(neededSize.Width)
	heightForResize := float64(img.Bounds().Dy()) / float64(neededSize.Height)

	if widthForResize < heightForResize {
		resizeImg = resize.Resize(uint(neededSize.Width), 0, img, resize.Lanczos3)
	} else {
		resizeImg = resize.Resize(0, uint(neededSize.Height), img, resize.Lanczos3)
	}

	cutingImg, err := cutter.Crop(resizeImg, cutter.Config{
		Width:  neededSize.Width,
		Height: neededSize.Height,
		Mode:   cutter.Centered,
	})
	if err != nil {
		return nil, errpath.Err(err)
	}

	return cutingImg, nil
}

// IsNil - проверка интерфейса на nil значение
func IsNil(v interface{}) bool {
	return v == nil || (reflect.ValueOf(v).Kind() == reflect.Ptr && reflect.ValueOf(v).IsNil())
}

// SliceIntersection -
// from: https://stackoverflow.com/questions/52120488/what-is-the-most-efficient-way-to-get-the-intersection-and-exclusions-from-two-a
func SliceIntersection(a, b []int) (inAAndB []int, inAWithoutB []int, inBWithoutA []int) {
	m := make(map[int]uint8)
	for _, k := range a {
		m[k] |= (1 << 0)
	}
	for _, k := range b {
		m[k] |= (1 << 1)
	}

	for k, v := range m {
		a := v&(1<<0) != 0
		b := v&(1<<1) != 0
		switch {
		case a && b:
			inAAndB = append(inAAndB, k)
		case a && !b:
			inAWithoutB = append(inAWithoutB, k)
		case !a && b:
			inBWithoutA = append(inBWithoutA, k)
		}
	}

	return
}
