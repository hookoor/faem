package tool

import (
	"time"
)

// Объекты общего назначения

const (
	// Pi -
	Pi float64 = 3.14
)

// Constant - constant
type Constant string

// ConstantF - constant
type ConstantF func() string

// URL - url адрес
type URL string

// Coordinates -
type Coordinates struct {
	Long float64 `json:"long"`
	Lat  float64 `json:"lat"`
}

// PictureSize -
type PictureSize struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// DataBaseConfigParams - модель для хранения конфиг-параметров в базе
type DataBaseConfigParams struct {
	UUID        string    `sql:"uuid"`
	Key         string    `sql:"key"`         // уникальное имя конфига
	Value       string    `sql:"value"`       // параметры в json формате
	description string    `sql:"description"` // описание конфига
	CreatedAt   time.Time `sql:"created_at"`
	UpdatedAt   time.Time `sql:"updated_at"`
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
