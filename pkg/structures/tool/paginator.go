package tool

import (
	"reflect"

	"github.com/go-pg/pg/urlvalues"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

// Paginator -
type Paginator struct {
	Page  int             `json:"page"`  // номер страницы
	Limit int             `json:"limit"` // сколько должно быть записей в странице
	Pager urlvalues.Pager `json:"-"`
}

// Fasten - уствновить значения пагинотора перед использованием
func (p *Paginator) Fasten() error {
	if p.Page == 0 || p.Limit == 0 {
		return errpath.Errorf("empty pager params")
	}
	p.Pager.Limit = p.Limit
	p.Pager.SetPage(p.Page)
	return nil
}

type paginationList []interface{}

// PaginatorResult -
type PaginatorResult struct {
	List         paginationList `json:"list"`
	RecordsCount int            `json:"records_count"` // сколько записей существует вообще (может быть больше чем в списке)
}

// GetPaginatorResult -
func GetPaginatorResult(records interface{}, allRecordsCount int) (*PaginatorResult, error) {
	var pres PaginatorResult

	err := pres.List.fill(records)
	if err != nil {
		return nil, errpath.Err(err)
	}
	if len(pres.List) > allRecordsCount {
		return nil, errpath.Errorf("выводимое число елементов не должно превышать общего количества записей")
	}
	pres.RecordsCount = allRecordsCount

	return &pres, nil
}

// Fill - заполняет список элементами
func (pl *paginationList) fill(elements interface{}) error {
	kind := reflect.TypeOf(elements).Kind()
	if kind == reflect.Ptr {
		elements = reflect.ValueOf(elements).Elem().Interface()
	}

	switch reflect.TypeOf(elements).Kind() {
	case reflect.Slice:
		v := reflect.ValueOf(elements)
		for i := 0; i < v.Len(); i++ {
			*pl = append(*pl, v.Index(i).Interface())
		}

	default:
		return errpath.Errorf("elements is not a slice")
	}

	return nil
}

// ------------------------------------------------- Example
/*

type filterExample struct {
	UUID *string `json:"uuid"`
	Name *string `json:"name"`

	AfteCreatedAt   *int64 `json:"after_created_at"`
	BeforeCreatedAt *int64 `json:"before_created_at"`

	Paginator
}

type dbItem struct {
	UUID      string    `json:"uuid"`
	Name      string    `json:"name"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updateds_at"`
	DeletedAt time.Time `json:"deleted_at"`
}

func getListExample(ctx context.Context, filter filterExample) (*PaginatorResult, error) {
	var records []dbItem

	err := filter.Fasten()
	if err != nil {
		fmt.Println(err)
	}

	var db *pg.DB // заглушка
	query := db.ModelContext(ctx, &records).
		Where("deleted_at is not null").
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.UUID != nil {
				q.Where("uuid = ?", *filter.UUID)
			}
			if filter.Name != nil {
				q.Where("name = ?", *filter.Name)
			}

			if filter.AfteCreatedAt != nil {
				q.Where("created_at >= ?", time.Unix(*filter.AfteCreatedAt, 0))
			}
			if filter.BeforeCreatedAt != nil {
				q.Where("created_at <= ?", time.Unix(*filter.BeforeCreatedAt, 0))
			}
			return q, nil
		}).
		Order("created_at DESC")

	count, err := query.Count()
	if err != nil {
		return nil, errpath.Err(err)
	}
	if count != 0 {
		err = query.
			Apply(filter.Pager.Pagination).
			Select()
		if err != nil {
			return nil, errpath.Err(err)
		}
	}

	pRes, err := GetPaginatorResult(records, count)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return pRes, nil
}

*/

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
