package postgre

import (
	"fmt"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"go.uber.org/atomic"
)

const funcCallerNumber int = 10

// DBQueryTraceHook -
type DBQueryTraceHook struct {
	enableCounter *atomic.Int32
}

// BeforeQuery -
func (db *DBQueryTraceHook) BeforeQuery(q *pg.QueryEvent) {
	if db.enableCounterVal() > 0 {
		query, err := q.FormattedQuery()
		if err != nil {
			fmt.Printf("Error: %s\n", err)
		}
		fmt.Printf(errpath.InfofWithFuncCaller(funcCallerNumber, "\x1b[35m %s \x1b[0m\n\n", query))

		db.enableCounterDec()
	}
}

// AfterQuery -
func (db *DBQueryTraceHook) AfterQuery(q *pg.QueryEvent) {}

// DebugQueryHook - активирует логирование SQL запроса
type DebugQueryHook interface {
	// StartTrace - начать логировать SQL
	StartTrace()
	enableCounterInc()
	enableCounterDec()
	enableCounterVal() int
}

// StartTrace - начать логировать SQL
func (db *DBQueryTraceHook) StartTrace() { db.enableCounterInc() }

func (db *DBQueryTraceHook) enableCounterInc()     { db.enableCounter.Inc() }
func (db *DBQueryTraceHook) enableCounterDec()     { db.enableCounter.Dec() }
func (db *DBQueryTraceHook) enableCounterVal() int { return int(db.enableCounter.Load()) }

// InitDebugSQLQueryHook -
func InitDebugSQLQueryHook(conn *pg.DB) *DBQueryTraceHook {
	hook := DBQueryTraceHook{
		enableCounter: atomic.NewInt32(0),
	}
	conn.AddQueryHook(&hook)
	return &hook
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Int -
type Int int

// IsZero -
func (i Int) IsZero() bool { return false }
