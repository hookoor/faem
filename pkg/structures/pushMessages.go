package structures

var PushInsurance = PushMailing{
	Title:   "Информация",
	Message: "Ваша поездка застрахована, счастливого пути!",
	Payload: &PushPayload{"https://faem.ru/legal/insurance"},
}
