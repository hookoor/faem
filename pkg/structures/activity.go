package structures

import (
	"time"
)

type ActivityHistoryItem struct {
	UserUUID       string `json:"user_uuid"`
	OfferUUID      string `json:"offer_uuid"`
	OrderUUID      string `json:"order_uuid"`
	Event          string `json:"event"`
	ActivityChange int    `json:"activity_change"`
	Activity       int    `json:"activity"`
}

type (
	ActivityConfig struct {
		// минимальное значение активности водителей
		MinDriverActivity int `json:"min_driver_activity"`

		// максимальное значение активности водителей
		MaxDriverActivity int `json:"max_driver_activity"`

		// часы пик
		RushHours RushHours `json:"rush_hours"`

		// настройки связанные с активностью нового водителя
		NewDriver struct {
			InitialActivity   int `json:"initial_activity"`    // стартовая активность после регистрации
			PenaltyFreePeriod int `json:"penalty_free_period"` // льготный период после регистрации
		} `json:"new_driver"`

		// настройки связанные с наградой за завершение заказа
		OrderFinished struct {
			// стандартная награда за выполнение заказа
			DefaultReward int `json:"default_reward"`

			// награда за дальний заказ
			FarOrderReward int `json:"far_order_reward"`

			// расстояние с которого заказ считается дальним
			FarOrderDistance int `json:"far_order_distance"`

			// бонус за выполнение заказа в час пик
			RushHoursBonus int `json:"rush_hours_bonus"`
		} `json:"order_finished"`

		// настройки связанные со штрафом за отмену заказа
		OrderCancelled struct {
			// стандартный штраф за отмену заказа
			DefaultPenalty int `json:"default_penalty"`

			// штраф за большое число отмен
			MultiCancelPenalty int `json:"multi_cancel_penalty"`

			// период в течение которого учитываются отмены заказов
			MultiCancelPeriod int `json:"multi_cancel_period"`

			// порог числа отмен для увеличенного
			MultiCancelLimit int `json:"multi_cancel_limit"`
		} `json:"order_cancelled"`

		// настройки связанные со штрафом за отказ от предложенного заказа
		OfferRejected struct {
			// штрафы за отказ в зависимости от расстояния
			DistancePenalties []DistancePenalty `json:"distance_penalties"`

			// стандартный штраф (если расстояние больше последнего в таблице штрафов от расстояния)
			DefaultPenalty int `json:"default_penalty"`

			// доп. штраф за большое число отказов (не учитывает когда расстояние больше последнего в таблице)
			MultiRejectMalus int `json:"multi_reject_malus"`

			// период за который считаются отказы для доп. штрафа за большое число отказов
			MultiRejectPeriod int `json:"multi_reject_period"`

			// порог числа отказов для доп. штрафа за большое число отказов
			MultiRejectLimit int `json:"multi_reject_limit"`
		} `json:"offer_rejected"`

		// настройки связанные с длительным оффлайном
		TooLongOffline struct {
			// стандартный ежедневный штраф за длительный оффлайн
			DefaultPenalty int `json:"default_penalty"`

			// число дней с которого оффлайн считается длительным
			DaysThreshold int `json:"days_threshold"`

			// минимальное значение активности, до которого может снижать этот штраф
			ActivityLowerLimit int `json:"activity_lower_limit"`
		} `json:"too_long_offline"`
	}

	RushMoment struct {
		Hour   int `json:"hour"`
		Minute int `json:"minute"`
		Second int `json:"second"`
	}

	RushHour struct {
		Start RushMoment `json:"start"`
		End   RushMoment `json:"end"`
	}

	DistancePenalty struct {
		Distance float64 `json:"distance"`
		Penalty  int     `json:"penalty"`
	}
)
type RushHours []RushHour

func (rs RushHours) Include(moment time.Time, timeZone string) bool {
	location, err := time.LoadLocation(timeZone)
	if err != nil {
		location = time.Local
	}
	const year, month, day = 2006, 1, 2
	for _, r := range rs {
		// Normalize then compare
		startMoment := time.Date(year, month, day, r.Start.Hour, r.Start.Minute, r.Start.Second, 0, location)
		endMoment := time.Date(year, month, day, r.End.Hour, r.End.Minute, r.End.Second, 0, location)
		moment = time.Date(year, month, day, moment.Hour(), moment.Minute(), moment.Second(), 0, time.UTC)
		if moment.After(startMoment) && moment.Before(endMoment) {
			return true
		}
	}
	return false
}
