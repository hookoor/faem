package structures

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/localtime"
)

// SourceOfOrders - источник заказов (линия заказов)
// rus: является источником заказов(заказы из любого места должны быть назначены к линии(даже из приложения))
// meta: является заменой телефонной линии
type SourceOfOrders struct {
	UUID          string `json:"uuid" pg:",pk"`
	Name          string `json:"name" required_on:"create"`
	SourceType    string `json:"source_type" required_on:"create"`
	Comment       string `json:"comment"`
	AsteriskHello string `json:"asterisk_hello"` // файл приветствия для астериска
	// DefaultServiceUUID - при создании заказа если явно не задана услуга, то задается услуга по умолчанию
	DefaultServiceUUID string `json:"default_service_uuid" required_on:"create"`
	TaxiParkUUID       string `json:"taxi_park_uuid"`
	RegionID           int    `json:"region_id"` // WARNING: заполняется в бд триггером tg_crm_sources_of_orders_on
}

type TaxiParkRepresentative struct {
	Name string `json:"name"`
	INN  string `json:"inn"`
}

// TaxiPark -
type TaxiPark struct {
	UUID                   string `json:"uuid"`
	Name                   string `json:"name"`
	Comment                string `json:"comment"`
	DefaultDriverGroupUUID string `json:"default_driver_group_uuid"`
	// FriendlyUUID - friendly taxi parks uuid
	// rus: дружественные таксопарки нужны для распеделения
	// (если не удалось распределить заказ среди машин своего таксопарка, то распределения пойдет среди дружественных таксопарков)
	FriendlyUUID StringSlice `json:"friendly_uuid,omitempty" sql:",type:text[]"`
	// UnwantedUUID - unwanted taxi parks uuid
	// rus: нежелательные таксопарки нужны для распеделения
	// (если не удалось распределить заказ среди машин своего таксопарка, то заказ не должен попасть в распределение нежелательные таксопарков)
	UnwantedUUID       StringSlice             `json:"unwanted_uuid,omitempty" sql:",type:text[]"`
	Representative     *TaxiParkRepresentative `json:"representative,omitempty"`
	RegionID           int                     `json:"region_id,omitempty"`            // id региона к которому принадлежит таксопарк
	DistributionWeight int                     `json:"distribution_weight,omitempty"`  // вес таксопарка в глобальном распределении
	DistributionRuleID int                     `json:"distribution_rule_id,omitempty"` // id правила распределения
}

func (st TicketStatuses) IsFiniteState() bool {
	return st == ResolvedStatus || st == NotResolvedStatus
}

type (
	TicketStatuses string
)

const (
	NewStatus         TicketStatuses = "new"
	InWorkStatus      TicketStatuses = "in_work"
	ResolvedStatus    TicketStatuses = "resolved"
	NotResolvedStatus TicketStatuses = "not_resolved"
)

//BillingData структура,к которой будут применятся выражения для биллинга
type BillingData struct {
	DriverBalance            float64 `json:"driver_balance"`
	OfferAcceptedFromOffline bool    `json:"offer_accepted_from_offline"`
	OrderPrice               int     `json:"order_price"`
	DriverCompensation       int     `json:"driver_compensation"`
	ActualOrderPrice         *int    `json:"actual_order_price"`
	WaitingPrice             int     `json:"waiting_price"`
	InsuranceCost            int     `json:"insurance_cost"`
}

func (b *BillingData) Clone() *BillingData {
	result := *b // get a shallow copy
	// Deep clone the pointers
	if b.ActualOrderPrice != nil {
		result.ActualOrderPrice = new(int)
		*result.ActualOrderPrice = *b.ActualOrderPrice
	}
	return &result
}

// Sub value from the order price and actual order price
func (b *BillingData) SubOrderPrice(orderPrice int) {
	if b == nil {
		return
	}

	b.OrderPrice -= orderPrice
	if b.OrderPrice < 0 { // max of 0 and value
		b.OrderPrice = 0
	}
	// Change actual order price also
	if b.ActualOrderPrice != nil {
		*b.ActualOrderPrice -= orderPrice
		if *b.ActualOrderPrice < 0 { // max of 0 and value
			*b.ActualOrderPrice = 0
		}
	}
}

// Decrease current order price value to the new one
func (b *BillingData) DecreaseOrderPriceTo(orderPrice int) {
	if b == nil {
		return
	}

	if b.OrderPrice > orderPrice {
		b.OrderPrice = orderPrice
	}
	// Change actual order price also
	if b.ActualOrderPrice != nil && *b.ActualOrderPrice > orderPrice {
		*b.ActualOrderPrice = orderPrice
	}
}

func (b *BillingData) GetActualOrderPrice() int {
	if b == nil {
		return 0
	}

	if b.ActualOrderPrice == nil {
		return b.OrderPrice + b.InsuranceCost // total price includes insurance cost
	}
	return *b.ActualOrderPrice
}

type TicketComment struct {
	SenderType    ChatMembers `json:"sender_type"`
	SenderUUID    string      `json:"sender_uuid"`
	CreatedAtUnix int64       `json:"created_at_unix"`
	SenderName    string      `json:"sender_name"`
	Message       string      `json:"message"`
}

type Ticket struct {
	UUID         string         `json:"uuid"`
	Title        string         `json:"title"`
	Description  string         `json:"description"`
	Status       TicketStatuses `json:"status"`
	SourceType   ChatMembers    `json:"source_type"` // who created the ticket
	OperatorData struct {
		Name string `json:"name"`
		UUID string `json:"uuid"`
	} `json:"operator_data"`
	StoreData struct {
		Name string `json:"name"`
		UUID string `json:"uuid"`
	} `json:"store_data"`
	DriverData struct {
		Name  string `json:"name"`
		UUID  string `json:"uuid"`
		Alias int    `json:"alias"`
	} `json:"driver_data"`
	ClientData struct {
		Phone string `json:"phone"`
		UUID  string `json:"uuid"`
	} `json:"client_data"`
	OrderData struct {
		ID   int    `json:"id"`
		UUID string `json:"uuid"`
	} `json:"order_data"`
	Comments      []TicketComment `json:"comments"`
	TaxiParksUUID []string        `json:"taxi_parks_uuid"  sql:",type:text[]"`
}

type DriverTariffType string

const (
	DriverTariffPercent DriverTariffType = "percent" // default
	DriverTariffPeriod  DriverTariffType = "period"
)

func (d DriverTariffType) Validate() error {
	if d == DriverTariffPercent || d == DriverTariffPeriod {
		return nil
	}
	return fmt.Errorf("некорректный тип тарифа: %s", d)
}

// DriverTariff - тарифы для работы водителей с Faem
type DriverTariff struct {
	UUID              string             `json:"uuid"`
	Offline           bool               `json:"offline"`
	DriversGroupsUUID []string           `json:"drivers_groups_uuid" pg:",zeroable" sql:",type:text[]"`
	Default           bool               `json:"default" description:"true только у одного стандартного тарифа"`
	IsSecret          bool               `json:"is_secret"`
	TariffType        DriverTariffType   `json:"tariff_type"`
	Name              string             `json:"name" description:"Название тарифа"`
	Comment           string             `json:"comment" description:"Комментарий"`
	Color             string             `json:"color" sql:"color"`
	Markups           TariffMarkupsArray `json:"markups,omitempty"`
	// For percent type
	RejectionExp  string `json:"rej_exp" sql:"rej_expr" description:"Выражение для регулирования отказов"`
	CommissionExp string `json:"comm_exp" sql:"comm_expr" description:"Выражение для регулирования комиссии"`
	// For period type
	Period       float64 `json:"period"`
	PeriodPrice  float64 `json:"period_price"`
	RegionID     int     `json:"region_id"`
	TaxiParkUUID string  `json:"taxi_park_uuid"`
}

type TariffMarkup struct {
	TaxiParkUUID     string      `json:"taxi_park_uuid"`
	RejectionExpr    string      `json:"rejection_expr"`
	CompleteExpr     string      `json:"complete_expr"`
	ForPeriodTariffs float64     `json:"for_period_tariffs"`
	CompleteVars     interface{} `json:"complete_vars"`
	RejectionVars    interface{} `json:"rejection_vars"`
	Title            string      `json:"title"`
	Description      string      `json:"description"`
}

func (drT DriverTariff) GetMarkupByTaxiParkUUID(tpUUID string) (TariffMarkup, bool) {
	for _, mar := range drT.Markups {
		if mar.TaxiParkUUID == tpUUID {
			return mar, true
		}
	}
	return TariffMarkup{}, false
}

type TariffMarkupsArray []TariffMarkup

func (arr TariffMarkupsArray) IsZero() bool {
	return arr == nil
}

type ManyMarkupsBody struct {
	TariffsUUID []string `json:"tariffs_uuid"`
}

// Client клиенты
type Client struct {
	UUID                string              `json:"uuid"`
	Name                string              `json:"name"`
	Karma               float64             `json:"karma"`
	MainPhone           string              `json:"main_phone"`
	Blocked             bool                `json:"blocked"`
	Phones              []string            `json:"phones" sql:",type:text[]"`
	DeviceID            string              `json:"device_id" description:"Login" required:"true"`
	TelegramID          string              `json:"telegram_id"`
	Comment             string              `json:"comment"`
	Activity            int                 `json:"activity" sql:",notnull"`
	DefaultPaymentType  string              `json:"default_payment_type" sql:"default_payment_type"`
	Promotion           ClientPromotion     `json:"promotion"`
	ReferralProgramData ReferralProgramData `json:"referral_program_data"`
	Blacklist           []string            `json:"blacklist" sql:",array"`
}

// ReferralProgramData -
type ReferralProgramData struct {
	Enable                bool   `json:"enable"`                  // работает ли реферальная программа для этого клиента
	ReferralCode          string `json:"referral_code"`           // код для активации новым клиентом
	ParentUUID            string `json:"parent_uuid"`             // uuid клиента который предоставил реферальную ссылку
	ReferralParentUUID    string `json:"referral_parent_code"`    // referral_code клиента который предоставил реферальную ссылку
	ReferralParentPhone   string `json:"referral_parent_phone"`   // телефон клиента который предоставил реферальную ссылку
	ActivationCount       int    `json:"activation_count"`        // количество активных реферальных аккаунтов (сколько людей зарегистрировалось по твоей ссылке)
	RecipientsTravelCount int    `json:"recipients_travel_count"` // количество поездок реципиентов
	IsNewcomer            bool   `json:"is_newcomer"`             // новый ли пользователь (без заказов)
}

type TaxiParkShortFields struct {
	TaxiParkData TaxiPark `json:"taxi_park_data,omitempty" sql:"-"`
	TaxiParkUUID *string  `json:"taxi_park_uuid"`
}

// Users юзеры
type Users struct {
	UUID              string      `json:"uuid"`
	Name              string      `json:"name"`
	StoreUUID         *string     `json:"store_uuid"`
	Active            *bool       `json:"active"`
	Login             string      `json:"login"`
	Password          string      `json:"password"`
	LoginTelephony    string      `json:"login_telephony"`
	PasswordTelephony string      `json:"password_telephony"`
	Role              string      `json:"role"`
	Comment           string      `json:"comment"`
	TaxiParksData     []TaxiPark  `json:"taxi_parks_data,omitempty" sql:"-"`
	TaxiParksUUID     StringSlice `json:"taxi_parks_uuid"  sql:",type:text[]"`
	RegionIDs         []int       `json:"region_ids"  sql:"region_ids,type:integer[]"`
}

func (us *Users) SetTaxiParkData(tp ...TaxiPark) {
	if us == nil {
		return
	}
	for i := range tp {
		us.TaxiParksData = append(us.TaxiParksData, tp[i])
	}
}

func (us *Users) GetTaxiParksUUID() []string {
	if us == nil {
		return nil
	}
	return us.TaxiParksUUID
}

func (us *Users) GetStoreUUID() string {
	if us == nil || us.StoreUUID == nil {
		return ""
	}
	return *us.StoreUUID
}

// Customer переименовал в Client так будет правильней
// Будет проще если телефон будет массивом
// Пенести в CRM:
// BlockedOperatorSessionID int       `json:"blocked_operator_session_id"`
// Blocked                  bool      `json:"blocked" description:"Заблокирован" sql:"default:false"`
// Deleted                  bool      `json:"-" sql:"default:false"`
// BlockDate                time.Time `json:"block_date"`
// BlockEnddate             time.Time `json:"block_enddate"`
// CreatedAt                time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
// UpdatedAt                time.Time `json:"updated_at" `

// District районы
type District struct {
	UUID     string `json:"uuid"`
	Name     string `json:"name" description:"Название района" required:"true"`
	Comment  string `json:"comment" description:"Комментарий"`
	Order    int    `json:"order" description:"Последовательность"`
	Region   Region `json:"-" sql:",fk:region_id"`
	RegionID int    `json:"regionID"`
	Tags     []string
	Polygon  string
}

type SelectedDriverTariff struct {
	DriverTariff
	PayedAt int64 `json:"payed_at"`
}

func (dt SelectedDriverTariff) TariffValidUntil(timeZone string) time.Time {
	if dt.TariffType != DriverTariffPeriod {
		return time.Time{}
	}

	payedAt := time.Unix(dt.PayedAt, 0)
	payedAt = payedAt.Add(time.Hour * time.Duration(dt.Period))
	if timeZone != "" {
		payedAt = localtime.TimeInZone(payedAt, timeZone)
	}
	return payedAt
}

func (dt SelectedDriverTariff) HasPayedTariff(timeZone string) (bool, time.Time) {
	if dt.TariffType != DriverTariffPeriod {
		return false, time.Time{}
	}

	now := time.Now()
	if timeZone != "" {
		now = localtime.TimeInZone(now, timeZone)
	}
	validUntil := dt.TariffValidUntil(timeZone)
	return now.Before(validUntil), validUntil
}

type DriverMeta struct {
	BlockedAt    int64 `json:"blocked_at"`
	BlockedUntil int64 `json:"blocked_until"`
	UnblockedAt  int64 `json:"unblocked_at"`
}

// Driver водитель. Пока все в одной структуре. AppID решил убрать, он нужен только в DriversApp
type Driver struct {
	UUID                   string               `json:"uuid"`
	Name                   string               `json:"name" required:"true"`
	PaymentTypes           []string             `json:"payment_types" sql:",type:text[]"`
	Phone                  string               `json:"phone" description:"Телефон" required:"true"`
	Comment                string               `json:"comment" description:"Comment"`
	Status                 string               `json:"state_name" sql:"status" description:"статус водителя"`
	Car                    string               `json:"car" description:"Машина"`
	Balance                float64              `json:"balance"`
	CardBalance            float64              `json:"card_balance"`
	MaxServiceLevel        *int                 `json:"max_service_level,omitempty"`
	Karma                  float64              `json:"karma" description:"Рейтинг"`
	Color                  string               `json:"color"`
	DrvTariff              SelectedDriverTariff `json:"tariff" sql:"tariff"`
	Tag                    []string             `json:"tag" sql:",type:text[],notnull"`
	AvailableServices      []Service            `json:"available_services" sql:",notnull"`
	AvailableFeatures      []Feature            `json:"available_features" sql:",notnull"`
	Alias                  int                  `json:"alias" description:"уникальный код водителя"`
	RegNumber              string               `json:"reg_number" description:"Номера машины"` // хранит только цифры (связанно с телефонией)
	Activity               int                  `json:"activity" sql:",notnull"`
	Promotion              DriverPromotion      `json:"promotion"`
	FullRegistrationNumber string               `json:"full_registration_number,omitempty"` // полный номер (преимущественно формата A111AA)
	Group                  DriverGroup          `json:"group" sql:"driver_group"`
	Blacklist              []string             `json:"blacklist" sql:",array"`
	Meta                   DriverMeta           `json:"meta"`
	CounterOrderSwitch     *bool                `json:"counter_order_switch" sql:"counter_order_switch"`
	TaxiParkShortFields
}

// DriverGroup - сущность группа водителей.
// meta: водитель может принадлежать только к одной группе
type DriverGroup struct {
	UUID         string `json:"uuid"`
	Name         string `json:"name" sql:"name"` // Name - название группы
	Description  string `json:"description"`     // Description - описание группы
	TaxiParkUUID string `json:"taxi_park_uuid"`  // TaxiParkUUID - к какому таксопарку принадлежит группа
	RegionID     int    `json:"region_id"`       // RegionID - к какому региону принадлежит группа WARNING: заполняется в бд триггером tg_crm_driver_groups_on
	// BelongingDrivers - водители принадлежащие группе
	// meta: водитель не может принадлежать нескольким группам
	BelongingDrivers []DriversDatForGroup `json:"belonging_drivers,omitempty"`
	// DistributionWeight - вес группы учитывающийся при распределении.
	DistributionWeight int `json:"distribution_weight"`
	// ServicesUUID - список uuid услуг (комфорт...) которые доступны водителям принадлежащим этой группе.
	// meta: оператор не может назначить услугу которой нет в списке группы (для этого есть метод GetServicesFromGroup)
	ServicesUUID []string `json:"services_uuid" sql:"services_uuid,type:text[]"`
	// PhotocontrolTemplates - uuid шаблонов-фотоконтролей на основе которых должны создасться фотоконтроли и назначиться при вступлении водителя в группу
	PhotocontrolTemplates []string `json:"photocontrol_templates" sql:",type:text[]"`
	Tags                  []string `json:"tag" sql:",type:text[]"`
	// DefaultTariffUUID - uuid тарифа по которому будет выполнен заказ при окончании срока действия текущего.
	// если у водителя закончился срок действия купленного тарифа(смена) его переключит на этот тариф
	DefaultTariffUUID string `json:"default_tariff_uuid" sql:"default_tariff_uuid"`
	// DefaultTariffOfflineUUID - uuid тарифа по которому будет выполнен заказ, если водитель принял заказ находясь оффлайн
	DefaultTariffOfflineUUID string `json:"default_tariff_offline_uuid" sql:"default_tariff_offline_uuid"`
	// AvailableDriverTariffsUUID - доступные тарифы для группы водителей
	AvailableDriverTariffsUUID []string `json:"available_driver_tariffs_uuid" sql:",type:text[]"`
}

// DriversDatForGroup -
type DriversDatForGroup struct {
	UUID string `json:"uuid,omitempty"`
	Name string `json:"name,omitempty"`
}

// UpdateDriverGroup - для обновления группы у водителей
type UpdateDriverGroup struct {
	Drivers       []DriversDatForGroup
	DriversGroups DriverGroup
}

// DriverStates - журнал статусов водителей
type DriverStates struct {
	DriverUUID string    `json:"driver_uuid"`
	State      string    `json:"state" description:"Status" required:"true"`
	Comment    string    `json:"comment" description:"Comment"`
	CreatedAt  time.Time `json:"created_at" sql:"default:now()"`
}

// Feature - фичи заказа (кресло, багажни и т.д.)
type Feature struct {
	UUID         string   `json:"uuid"`
	Name         string   `json:"name" description:"Название услуги" required:"true"`
	Comment      string   `json:"comment" description:"Комментарий"`
	Price        int      `json:"price" description:"Цена"`
	Tag          []string `json:"tag" sql:",type:text[]"`
	ServicesUUID []string `json:"services_uuid" sql:",type:text[]"`
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Region -
type Region struct {
	ID     int              `json:"id" pg:",pk"`
	Name   string           `json:"name"`
	Origin *PureCoordinates `json:"origin" sql:"origin"`

	// ServicesSet - содержит uuid-шники сервисов доступных региону
	// meta: должен быть дефолтный набор сервисов для назначения его при создании региона
	ServicesSetID  int `json:"services_set_id"`
	FeaturesSetID  int `json:"features_set_id"`
	AddressesSetID int `json:"addresses_set_id"`

	DistributionRuleID int `json:"distribution_rule_id"`
}

// ServicesSet -
type ServicesSet struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// DriverTariffsSets -
type DriverTariffsSets struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// FeatiresSet -
type FeaturesSet struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// AddressesSets -
type AddressesSets struct {
	ID                   int                   `json:"id"`
	Name                 string                `json:"name"`
	CityAddressesWeights []CityAddressesWeight `json:"city_addresses_weights"`
}

// CityAddressesWeight -
// rus: используется при автокомплите адресов. Weight это коэффициент дабы дать приоритет городам
type CityAddressesWeight struct {
	City   string `json:"city"`
	Weight int    `json:"weight"`
}

// ClientPromotion represents additional properties to promote a client in the distribution process
type ClientPromotion struct {
	Booster bool `json:"booster"`
	IsVIP   bool `json:"is_vip"`
}

// DriverPromotion represents additional properties to promote a driver in the distribution process
type DriverPromotion struct {
	Booster      bool `json:"booster"`
	RentedAuto   bool `json:"rented_auto"`
	BrandSticker bool `json:"brand_sticker"`
	// ScoringBoostTimeByPhotocontrolPassed - время действия(unix) надбавки очков распределения при прохождении фотоконтроля
	ScoringBoostTimeByPhotocontrolPassed int64 `json:"scoring_boost_time_by_photocontrol_passed" sql:"scoring_boost_time_by_photocontrol_passed"`
}

// FormulaTemplate -
type FormulaTemplate struct {
	Formula string                `json:"formula,omitempty"` // формула
	Comment string                `json:"comment"`           // описание формулы
	Vars    []FormulaTemplateVars `json:"variables"`         // переменные в формуле
	Belong  string                `json:"-"`                 // к чему принадлежит формула (к тарифу, к распределению, ... )
}

// FormulaTemplateVars -
type FormulaTemplateVars struct {
	Key         string `json:"key"`         // буква в формуле для подстановки
	Value       string `json:"value"`       // значение для подстановки в формулу
	Name        string `json:"name"`        // полное имя переменной
	Description string `json:"description"` // описание поля
}

func (d *Driver) GetTaxiParksUUID() []string {
	if d == nil || d.TaxiParkUUID == nil {
		return nil
	}
	return []string{*d.TaxiParkUUID}
}

func (d *Driver) GetTaxiParkUUID() string {
	if d == nil || d.TaxiParkUUID == nil {
		return ""
	}
	return *d.TaxiParkUUID
}

func (d *Driver) SetTaxiParkData(tp ...TaxiPark) {
	if d == nil || len(tp) == 0 {
		return
	}
	d.TaxiParkData = tp[0]
}

func (d *Driver) CanReceiveGuaranteedIncome(activityLimit int) bool {
	if d == nil {
		return true
	}
	return d.Activity >= activityLimit
}

func (d *Driver) GetTotalBalance() float64 {
	return d.Balance + d.CardBalance
}
