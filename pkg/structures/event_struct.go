package structures

import "time"

type (
	EventItem struct {
		EventTime     time.Time   `json:"event_time"`
		TicketUUID    string      `json:"ticket_uuid"`
		MessagesUUID  []string    `json:"messages_uuid" sql:",type:text[]"`
		EventTimeUnix int64       `json:"event_time_unix" sql:"event_time_unix"`
		DriverUUID    []string    `json:"driver_uuid" sql:",type:text[]"`
		OrderUUID     []string    `json:"order_uuid" sql:",type:text[]"`
		ClientUUID    []string    `json:"client_uuid" sql:",type:text[]"`
		OperatorUUID  []string    `json:"operator_uuid" sql:",type:text[]"`
		Publisher     string      `json:"publisher"`
		Event         string      `json:"event_name"`
		EventTrans    string      `json:"event_title"`
		Reason        string      `json:"reason"`
		Value         string      `json:"value"`
		Payload       interface{} `json:"payload"`
		Comment       string      `json:"comment"`
		ValueTitle    string      `json:"value_title,omitempty" sql:"-"`
	}
	LocationData struct {
		DriverID   int64     `json:"-"`
		DriverUUID string    `json:"driver_uuid" sql:",unique" `
		Latitude   float64   `json:"lat"`
		Longitude  float64   `json:"lng"`
		Satelites  int       `json:"sats"`
		Timestamp  int64     `json:"time"`
		CreatedAt  time.Time `json:"created_at" sql:"default:now()"`
	}
)
