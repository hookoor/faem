package structures

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

// TEndPoints -
type TEndPoints struct {
	Driver   struct{}
	Client   struct{}
	Notyfire struct {
		IsPresence struct {
			Method tool.ConstantF
			URL    tool.ConstantF
			Params struct {
				Client  tool.ConstantF
				Channel tool.ConstantF
			}
		}
	}
}

// EndPoints -
var EndPoints TEndPoints

func init() {
	{ // Notyfire
		{
			EndPoints.Notyfire.IsPresence.Method = func() string { return http.MethodGet }
			EndPoints.Notyfire.IsPresence.URL = func() string { return "/ispresence" }

			EndPoints.Notyfire.IsPresence.Params.Client = func() string { return "client" }
			EndPoints.Notyfire.IsPresence.Params.Channel = func() string { return "channel" }
		}
	}
	{

	}

}
