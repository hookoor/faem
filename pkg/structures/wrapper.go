package structures

import "github.com/gofrs/uuid"

func GenerateUUID() string {
	newUUID, _ := uuid.NewV4()
	return newUUID.String()
}

// WrapperDriver - добавлен uuid оператора который произвел изменения над водителем
type WrapperDriver struct {
	OperatorUUID string `json:"operator_uuid"`
	Driver
}

// WrapperOrder - добавлен uuid оператора который произвел изменения над заказом и разница между старой и новой надбавочной стоимостью
type WrapperOrder struct {
	FareDifferences float32 `json:"fare_differences,omitempty"`
	OperatorUUID    string  `json:"operator_uuid"`
	Order
}

// WrapperOperator - добавлен uuid оператора который произвел изменения над оператором
type WrapperOperator struct {
	OperatorUUID string `json:"operator_uuid"`
	Users
}
