package structures

type AppVersion struct {
	DeviceID string `json:"device_id" sql:",pk"`
	Version  string `json:"version_name"`
	OS       string `json:"os" sql:"os_name"` //sql тэг для учета версий в клиентском приложении
}
