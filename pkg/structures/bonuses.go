package structures

type (
	BonusesData struct {
		Bonuses           float64 `json:"bonuses"`
		AutomaticWriteOff bool    `json:"automatic_write_off"`
		Comment           string  `json:"comment"`
	}
)
