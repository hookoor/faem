package structures

import "time"

type (
	PhotoStatuses            string
	PhotoControlStatusesType string
)

const (
	ModerationPhotoControlState PhotoControlStatusesType = "moderation"
	ExpiredPhotoControlState    PhotoControlStatusesType = "expired"
	WarningPhotoControlState    PhotoControlStatusesType = "warning"
	OKPhotoControlState         PhotoControlStatusesType = "ok"

	ModerationStatus PhotoStatuses = "moderation"
	ApprovedStatus   PhotoStatuses = "approved"
	RejectedStatus   PhotoStatuses = "rejected"
)

type PhotoControlData struct {
	Notified            bool                     `json:"notified"`
	ControlType         string                   `json:"control_type"`
	ControlTypeTitle    string                   `json:"control_type_title"`
	Status              PhotoControlStatusesType `json:"status"`
	NextControlTime     time.Time                `json:"next_control_time"`
	NextControlTimeUnix int64                    `json:"next_control_time_unix" sql:"-"`
}
type Photo struct {
	ControlVariant string        `json:"control_variant"`
	Title          string        `json:"title"`
	ImageURL       string        `json:"image_url"`
	Status         PhotoStatuses `json:"status"`
	Comment        string        `json:"comment"`
}

type PhotoControl struct {
	DriverID    string  `json:"driver_id"`
	ControlID   string  `json:"control_id"`
	Approved    bool    `json:"approved"`
	ControlType string  `json:"control_type"`
	Photos      []Photo `json:"photos"`
}
