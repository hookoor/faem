package osrm

import (
	"math"
	"net/http"
	"strconv"

	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

// OSRM - хранит адрес сервиса
type OSRM struct {
	Host string
}

// New - (глобальный инстанс как у базы)
func New(host string) *OSRM {
	return &OSRM{host}
}

// RoutePart -
type RoutePart struct {
	Coords   []tool.Coordinates `json:"coordinates"` // координаты маршрута
	Duration float64            `json:"duration"`    // время в секундах
	Distance float64            `json:"distance"`    // расстояние в метрах
}

// RouteData -
type RouteData struct {
	Way  RoutePart   `json:"way"`  // полный путь
	Legs []RoutePart `json:"legs"` // разделение пути по промежуточным точкам
}

// TableData -
type TableData struct {
	PairsDuration []RoutePart // длина маршрута между порами координат (записанными в Coords)
}

// GetItinerary - получение данных с помощью OSRM по координатам
func (osrm OSRM) GetItinerary(coordinates []tool.Coordinates) (RouteData, error) {
	var (
		rd        RouteData
		rs        resultOSRMRouteService
		reqString string
	)

	reqString += osrm.Host
	reqString += "/route/v1/driving/"

	for i, inst := range coordinates {
		reqString += strconv.FormatFloat(inst.Long, 'f', 6, 64) + "," + strconv.FormatFloat(float64(inst.Lat), 'f', 6, 64)
		if i != len(coordinates)-1 {
			reqString += ";"
		}
	}
	reqString += "?geometries=geojson"

	err := tool.SendRequest(http.MethodGet, reqString, nil, nil, &rs)
	if err != nil {
		return rd, errpath.Err(err)
	}

	rd, err = convertingOSRMRouteService(rs)
	if err != nil {
		return rd, errpath.Err(err)
	}

	return rd, nil
}

// OSRMTableService - Вычисляет время и продолжительность маршрута между всеми парами предоставленных координат.
// Расчет от всех источников ко всем пунктам назначения (двудольный граф).
// !!! Все источники и пункты назначения должны быть в координатах первого параметра
// Если источник не указан по умолчанию будет первый элемент.
// Если пункт назначения не указан по умолчанию будут все кроме первого.
func (osrm OSRM) OSRMTableService(coordinates []tool.Coordinates, source []tool.Coordinates, destinations []tool.Coordinates) (TableData, error) {
	var ts resultOSRMTableService
	var td TableData

	{ // Validate
		if len(coordinates) < 2 {
			return td, errpath.Errorf("number of coordinates needs to be at least two (OSRMTableService)")
		}
		if len(destinations) == 0 {
			destinations = append(destinations, coordinates[0]) // first coordinate is the destination by the convention
		}
		if len(source) == 0 {
			// We have already checked for coordinates length above
			for i := 1; i < len(coordinates); i++ {
				source = append(source, coordinates[i])
			}
		}
	}

	var reqString string

	reqString += osrm.Host
	reqString += "/table/v1/driving/"

	for i, inst := range coordinates {
		reqString += strconv.FormatFloat(inst.Long, 'f', 6, 64) + "," + strconv.FormatFloat(inst.Lat, 'f', 6, 64)
		if i != len(coordinates)-1 {
			reqString += ";"
		}
	}

	var sourceIndex []int
	for _, sourceInst := range source {
		for i, coordInst := range coordinates {
			if coordInst == sourceInst {
				sourceIndex = append(sourceIndex, i)
				break
			}
		}
	}
	var destIndex []int
	for _, destInst := range destinations {
		for i, coordInst := range coordinates {
			if coordInst == destInst {
				destIndex = append(destIndex, i)
				break
			}
		}

	}

	reqString += "?sources="
	for i, inst := range sourceIndex {
		reqString += strconv.Itoa(inst)
		if i != len(sourceIndex)-1 {
			reqString += ";"
		}
	}
	dstString := "&destinations="
	for i, inst := range destIndex {
		dstString += strconv.Itoa(inst)
		if i != len(destIndex)-1 {
			dstString += ";"
		}
	}
	reqString += dstString
	reqString += "&annotations=distance,duration"

	err := tool.SendRequest(http.MethodGet, reqString, nil, nil, &ts)
	if err != nil {
		return td, errpath.Err(err)
	}

	td, err = convertingOSRMTableService(ts)
	if err != nil {
		return td, errpath.Err(err)
	}

	return td, nil
}

// MultipleTableDataInput - для расчета пути от источников (Sources) до целей (Destinations)
type MultipleTableDataInput struct {
	Source      tool.Coordinates
	Destination tool.Coordinates
}

// GetDistancesArray -
func (osrm OSRM) GetDistancesArray(sourceData []MultipleTableDataInput) (TableData, error) {

	var res TableData

	// maxURLLength - условно максимальная длина URL. Длина должна быть меньше 2000 символов. В противном случае такие ссылки не будут работать примерно у 60% пользователей интернет.
	const maxURLLength int = 2000
	const lenOSRMHost = 30                                                            // символов в хосте
	const lenOSRMParams = 70                                                          // символов в параметрах url
	const lenOfOneCoord = 20                                                          // символов в координатах
	const lenOfSrcOrTargIndex = 2                                                     // символов при указании источников и целей (0;1;)
	urlResidue := maxURLLength - lenOSRMHost - lenOSRMParams                          // остаточный объем символов для координат
	var lenOfRoadCoords = (lenOfOneCoord + lenOfSrcOrTargIndex) * len(sourceData) * 2 // объем символов из всех координат
	var requestsCount = int(math.Ceil(float64(lenOfRoadCoords / urlResidue)))         // количество запросов на OSRM на которые надо поделить все координаты
	if requestsCount == 0 {
		requestsCount = 1
	}
	var coordsStack = int(math.Ceil(float64(len(sourceData) / requestsCount))) // количество координат источника и цели (MultipleTableDataInput) в одном запросе

	type params struct {
		paramCoordinates  []tool.Coordinates
		paramSource       []tool.Coordinates
		paramDestinations []tool.Coordinates
	}
	pieceQuery := map[int]params{} // [кусoк][параметры]

	saveParamCoords := func(arr []tool.Coordinates, el tool.Coordinates) []tool.Coordinates {
		var saveMark bool = true
		for _, item := range arr {
			if item == el {
				saveMark = false
			}
		}
		if saveMark {
			arr = append(arr, el)
		}
		return arr
	}

	i := 0
	for i = 0; i < requestsCount-1; i++ {
		var roadPiece []MultipleTableDataInput
		var pr params

		roadPiece = append(roadPiece, sourceData[i*coordsStack:(i+1)*coordsStack]...)
		for _, rp := range roadPiece {
			pr.paramCoordinates = saveParamCoords(pr.paramCoordinates, rp.Source)
			pr.paramCoordinates = saveParamCoords(pr.paramCoordinates, rp.Destination)
			pr.paramSource = append(pr.paramSource, rp.Source)
			pr.paramDestinations = append(pr.paramDestinations, rp.Destination)
		}
		pieceQuery[i] = pr
	}
	var roadPiece []MultipleTableDataInput
	var pr params
	roadPiece = append(roadPiece, sourceData[i*coordsStack:]...)
	for _, rp := range roadPiece {
		pr.paramCoordinates = saveParamCoords(pr.paramCoordinates, rp.Source)
		pr.paramCoordinates = saveParamCoords(pr.paramCoordinates, rp.Destination)
		pr.paramSource = append(pr.paramSource, rp.Source)
		pr.paramDestinations = append(pr.paramDestinations, rp.Destination)
	}
	pieceQuery[i] = pr

	for _, val := range pieceQuery {
		osrmdata, err := osrm.OSRMTableService(val.paramCoordinates, val.paramSource, val.paramDestinations)
		if err != nil {
			return res, errpath.Err(err)
		}

		for i := 0; i < len(val.paramSource); i++ {
			need := i*len(val.paramDestinations) + i
			res.PairsDuration = append(res.PairsDuration, osrmdata.PairsDuration[need])
		}
	}

	for i := range res.PairsDuration {
		res.PairsDuration[i].Coords[0].Lat = sourceData[i].Source.Lat
		res.PairsDuration[i].Coords[0].Long = sourceData[i].Source.Long
		res.PairsDuration[i].Coords[1].Lat = sourceData[i].Destination.Lat
		res.PairsDuration[i].Coords[1].Long = sourceData[i].Destination.Long
	}

	return res, nil
}

// CoordinatesParse - из <[][]float64> делает <[]Coordinates>
func CoordinatesParse(cords [][]float64) []tool.Coordinates {
	result := []tool.Coordinates{}
	for _, item := range cords {
		result = append(result, tool.Coordinates{Long: item[0], Lat: item[1]})
	}
	return result
}

// ------------------

// resultOSRMRouteService - структура для анмаршалинга ответа от OSRM (route service)
type resultOSRMRouteService struct {
	Routes []struct {
		Geometry struct {
			Coor [][]float64 `json:"coordinates"` // набор координат из которых состоит маршрут
			Type string      `json:"type"`        // "LineString"
		} `json:"geometry"`
		Legs []struct {
			Summary  string   `json:"summary"`
			Weight   float64  `json:"weight"`
			Duration float64  `json:"duration"` // время в секундах
			Steps    []string `json:"steps"`
			Distance float64  `json:"distance"` // расстояние в метрах
		} `json:"legs"`
		WeightName string  `json:"weight_name"`
		Weight     float64 `json:"weight"`
		Duration   float64 `json:"duration"` // время в секундах всего маршрута
		Distance   float64 `json:"distance"` // расстояние в метрах всего маршрута
	} `json:"routes"`

	Waypoints []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"waypoints"`
	Code string `json:"code"`
}

// resultOSRMTableService - структура для анмаршалинга ответа от OSRM (table service)
type resultOSRMTableService struct {
	Code    string `json:"code"`
	Sources []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"sources"`
	Destinations []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"destinations"`
	Durations [][]float64 `json:"durations"` // [индекс источника][индекс цели] время в секундах всего маршрута
	Distances [][]float64 `json:"distances"` // [индекс источника][индекс цели] расстояние в метрах всего маршрута
}

func convertingOSRMRouteService(rs resultOSRMRouteService) (RouteData, error) {
	var rd RouteData

	if len(rs.Routes) == 0 {
		return rd, errpath.Errorf("resultOSRMRouteService is empty")
	}
	rd.Way.Coords = CoordinatesParse(rs.Routes[0].Geometry.Coor)
	rd.Way.Distance = rs.Routes[0].Distance
	rd.Way.Duration = rs.Routes[0].Duration

	for i, item := range rs.Routes[0].Legs {
		var rp RoutePart
		var record bool = false
		var lc = make([]tool.Coordinates, 0)

		for _, legCords := range rd.Way.Coords {
			if legCords.Long == rs.Waypoints[i].Location[0] && legCords.Lat == rs.Waypoints[i].Location[1] {
				record = true
			}
			if record {
				lc = append(lc, legCords)
			}
			if legCords.Long == rs.Waypoints[i+1].Location[0] && legCords.Lat == rs.Waypoints[i+1].Location[1] && record {
				record = false
				break
			}
		}

		rp.Coords = append(rp.Coords, lc...)
		rp.Distance = item.Distance
		rp.Duration = item.Duration
		rd.Legs = append(rd.Legs, rp)
	}

	return rd, nil
}

func convertingOSRMTableService(ts resultOSRMTableService) (TableData, error) {
	var td TableData

	for i := 0; i < len(ts.Sources); i++ {
		for j := 0; j < len(ts.Destinations); j++ {
			var rp RoutePart
			rp.Coords = append(rp.Coords, tool.Coordinates{Long: ts.Sources[i].Location[0], Lat: ts.Sources[i].Location[1]})
			rp.Coords = append(rp.Coords, tool.Coordinates{Long: ts.Destinations[j].Location[0], Lat: ts.Destinations[j].Location[1]})
			rp.Duration = ts.Durations[i][j]
			rp.Distance = ts.Distances[i][j]

			td.PairsDuration = append(td.PairsDuration, rp)
		}
	}

	return td, nil
}
