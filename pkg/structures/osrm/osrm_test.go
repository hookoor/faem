package osrm

// // {
// // 	"uuid": "f9606636-b6a4-4c7b-875a-fb76a815997c",
// // 	"unrestricted_value": "Цоколаева 10",
// // 	"lat": 43.0444,
// // 	"lon": 44.6344
// // }

// // {
// // 	"uuid": "8b81671d-ec17-4f70-a0c2-2101a471042b",
// // 	"unrestricted_value": "ГМИ на Тельмана, Тельмана 29",
// // 	"lat": 43.0619,
// // 	"lon": 44.6722
// // }

// // {
// // 	"uuid": "940acdcf-9ba3-4c82-9697-bcdd74af6833",
// // 	"unrestricted_value": "СОГУ - Северо-Осетинский Государственный университет , Ватутина 46",
// // 	"lat": 43.0272,
// // 	"lon": 44.6911
// // }

// // {
// // 	"uuid": "6063abc8-a9c4-49c8-aac5-46cd8688c167",
// // 	"unrestricted_value": "Юность, Хаджи Мамсурова 51",
// // 	"lat": 43.0353,
// // 	"lon": 44.6541
// // }

// // 1 = 44.6344,43.0444
// // 2 = 44.6722,43.0619
// // 3 = 44.6911,43.0272
// // 4 = 44.6541,43.0353

// sd := []osrm.MultipleTableDataInput{
// 	{ //  1 -> 3 = distances: 7457.8; "durations": 627
// 		Source: tool.Coordinates{
// 			Long: 44.6344,
// 			Lat:  43.0444,
// 		},
// 		Destination: tool.Coordinates{
// 			Long: 44.6911,
// 			Lat:  43.0272,
// 		},
// 	},
// 	{ // 2 -> 4 = distances: 4295.7; "durations": 350.8
// 		Source: tool.Coordinates{
// 			Long: 44.6722,
// 			Lat:  43.0619,
// 		},
// 		Destination: tool.Coordinates{
// 			Long: 44.6541,
// 			Lat:  43.0353,
// 		},
// 	},
// 	{ // 3 -> 2 = distances: 4584; "durations": 408.9
// 		Source: tool.Coordinates{
// 			Long: 44.6911,
// 			Lat:  43.0272,
// 		},
// 		Destination: tool.Coordinates{
// 			Long: 44.6722,
// 			Lat:  43.0619,
// 		},
// 	},
// 	{ // 4 -> 1 = distances: 3010.2; "durations": 251.8
// 		Source: tool.Coordinates{
// 			Long: 44.6541,
// 			Lat:  43.0353,
// 		},
// 		Destination: tool.Coordinates{
// 			Long: 44.6344,
// 			Lat:  43.0444,
// 		},
// 	},
// }

// host := "https://osrm.stage.faem.pro"
// tableData, err := osrm.New(host).GetDistancesArray(sd)
// if err != nil {
// 	fmt.Println(err)
// 	return
// }

// for i, input := range sd {
// 	for _, output := range tableData.PairsDuration {
// 		if (output.Coords[0] == tool.Coordinates{Lat: input.Source.Lat, Long: input.Source.Long}) &&
// 			(output.Coords[1] == tool.Coordinates{Lat: input.Destination.Lat, Long: input.Destination.Long}) {
// 			fmt.Println(fmt.Sprintf("%v = %v", i, output.Distance))
// 		}
// 	}
// }
