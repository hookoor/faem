package structures

// Zone - информация о зоне на карте
type Zone struct {
	UUID          string   `json:"uuid" sql:"default:nil"`
	Name          string   `json:"name" description:"Название зоны"`
	Comment       string   `json:"comment" description:"Комментарий"`
	DistanceCoeff float64  `json:"distance_coeff"`
	Level         int      `json:"level" sql:"default:-1"` // приоритет
	Region        string   `json:"region" description:"область к которой принадлежит зона"`
	Tags          []string `json:"tags" sql:",type:text[]" description:"группы к которм принадлежит зона"`
	Area          string   `json:"area" description:"зона"` // геометрия(полигон)
}

// ZoneWithPoints -
type ZoneWithPoints struct {
	ZoneUUID string
	ZoneName string
	Points   []PureCoordinates
}
