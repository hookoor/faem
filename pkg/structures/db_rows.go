package structures

import (
	"fmt"
	"reflect"

	"github.com/go-pg/pg/orm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

// RowSpaceType -
type RowSpaceType string

const (
	// RowSpaceRegionIDTag -
	rowSpaceRegionIDTag RowSpaceType = "region_id"
	// RowSpaceTaxiParkUUIDTag -
	rowSpaceTaxiParkUUIDTag RowSpaceType = "taxi_park_uuid"
	// RowSpaceOrdersSourceUUIDTag -
	rowSpaceOrdersSourceUUIDTag RowSpaceType = "order_source_uuid"

	// RowSpaceDeletedTag -
	rowSpaceDeletedTag RowSpaceType = "deleted"
	// RowSpaceDeletedAtTag -
	rowSpaceDeletedAtTag RowSpaceType = "deleted_at"
)

// RowSpaceData -
type RowSpaceData struct {
	RegionID, TaxiParkUUID, OrdersSourceUUID interface{}
	Deleted, DeletedAt                       *bool
}

// RowSpace -
func (model RowSpaceData) RowSpace() func(q *orm.Query) (*orm.Query, error) {
	var err error

	checkEmpty := func(value interface{}) (isEmpty bool) {
		t := reflect.ValueOf(value)
		switch t.Kind() {
		case reflect.String:
			v := value.(string)
			if v != "" {
				return false
			}
		case reflect.Int:
			v := value.(int)
			if v != 0 {
				return false
			}
		case reflect.Ptr:
			switch t.Elem().Kind() {
			case reflect.String:
				v := value.(*string)
				if *v != "" {
					return false
				}
			case reflect.Int:
				v := value.(*int)
				if *v != 0 {
					return false
				}
			}
		}
		return true
	}

	return func(q *orm.Query) (*orm.Query, error) {
		if !tool.IsNil(model.RegionID) {
			if !checkEmpty(model.RegionID) {
				q.Where(fmt.Sprintf("%s = ?", rowSpaceRegionIDTag), model.RegionID)
			}
		}
		if !tool.IsNil(model.TaxiParkUUID) {
			if !checkEmpty(model.TaxiParkUUID) {
				q.Where(fmt.Sprintf("%s = ?", rowSpaceTaxiParkUUIDTag), model.TaxiParkUUID)
			}
		}
		if !tool.IsNil(model.OrdersSourceUUID) {
			if !checkEmpty(model.OrdersSourceUUID) {
				q.Where(fmt.Sprintf("%s = ?", rowSpaceOrdersSourceUUIDTag), model.OrdersSourceUUID)
			}
		}

		if !tool.IsNil(model.Deleted) {
			q.Where(fmt.Sprintf("%s is not true", rowSpaceDeletedTag))
		}
		if !tool.IsNil(model.DeletedAt) {
			q.Where(fmt.Sprintf("%s is null", rowSpaceDeletedAtTag))
		}
		return q, err
	}
}

// ------------------------------------------------- Example
/*

type SomeStruct struct {
	tableName struct{} `sql:"some_struct"`

	RegionID     int    `sql:"region_id"`
	TaxiParkUUID string `sql:"taxi_park_uuid"`

	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
	DeletedAt time.Time `sql:"deleted_at"`
}

func AAAAA(uuid , regionID, taxiParkUUID string) {

	rowSpace := RowSpaceData{
		RegionID: regionID,
		TaxiParkUUID: taxiParkUUID,

		DeletedAt: &true,
	}

	var rows []SomeStruct
	db.Model(&rows).
		Apply(rowSpace.RowSpace()).
		Where("uuid = ?", uuid).
		Select()

	fmt.Println(rows)
}

*/

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
// Попытка реализации с помощью тэгов
// она сложна в эксплуатации и имеет множество нюансов

/*

const (
	// RowSpaceTagKey - значние должно быть идентичным названию колонки
	RowSpaceTagKey RowSpaceType = "row_space"
)

// rowData -
type rowData struct {
	regionID, taxiParkUUID, ordersSourceUUID interface{}
	deleted, deletedAt                       interface{}
}

// RowSpace -
func RowSpace(model interface{}) func(q *orm.Query) (*orm.Query, error) {
	var err error
	var data rowData

	modelVal := reflect.ValueOf(model)
	if modelVal.Kind() != reflect.Struct {
		err = errpath.Errorf("model is not a struct")
	} else {
		structExtract(modelVal, &data)
	}

	checkEmpty := func(value interface{}) (isEmpty bool) {
		switch reflect.TypeOf(value).Kind() {
		case reflect.String:
			v := value.(string)
			if v != "" {
				return false
			}
		case reflect.Int:
			v := value.(int)
			if v != 0 {
				return false
			}
		}
		return true
	}

	return func(q *orm.Query) (*orm.Query, error) {
		if data.regionID != nil {
			if !checkEmpty(data.regionID) {
				q.Where(fmt.Sprintf("%s = ?", RowSpaceRegionIDTag), data.regionID)
			}
		}
		if data.taxiParkUUID != nil {
			if !checkEmpty(data.taxiParkUUID) {
				q.Where(fmt.Sprintf("%s = ?", RowSpaceTaxiParkUUIDTag), data.taxiParkUUID)
			}
		}
		if data.ordersSourceUUID != nil {
			if !checkEmpty(data.ordersSourceUUID) {
				q.Where(fmt.Sprintf("%s = ?", RowSpaceOrdersSourceUUIDTag), data.ordersSourceUUID)
			}
		}

		if data.deleted != nil {
			q.Where(fmt.Sprintf("%s is not true", RowSpaceDeletedTag))
		}
		if data.deletedAt != nil {
			q.Where(fmt.Sprintf("%s is null", RowSpaceDeletedAtTag))
		}
		return q, err
	}
}

func structExtract(modelVal reflect.Value, data *rowData) {

	for i := 0; i < modelVal.NumField(); i++ {

		if modelVal.Type().Field(i).Type.Kind() == reflect.Struct {
			if modelVal.Field(i).CanInterface() {
				structExtract(modelVal.Field(i), data)
			}
		}

		tag, ex := modelVal.Type().Field(i).Tag.Lookup(string(RowSpaceTagKey))
		if ex {
			switch tag {
			case string(RowSpaceRegionIDTag):
				data.regionID = modelVal.Field(i).Interface()
			case string(RowSpaceTaxiParkUUIDTag):
				data.taxiParkUUID = modelVal.Field(i).Interface()
			case string(RowSpaceOrdersSourceUUIDTag):
				data.ordersSourceUUID = modelVal.Field(i).Interface()

			case string(RowSpaceDeletedTag):
				data.deleted = modelVal.Field(i).Interface()
			case string(RowSpaceDeletedAtTag):
				data.deletedAt = modelVal.Field(i).Interface()
			}
		}
	}
}
*/
// ------------------------------------------------- Example

/*

type SomeStruct struct {
	tableName struct{} `sql:"some_struct"`

	RegionID     int    `sql:"region_id" row_space:"region_id"`
	TaxiParkUUID string `sql:"taxi_park_uuid" row_space:"taxi_park_uuid"`

	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
	DeletedAt time.Time `sql:"deleted_at" row_space:"deleted_at"`
}

func AAAAA(uuid , regionID, taxiParkUUID string) {

	var rows []SomeStruct
	db.Model(&rows).
		Apply(RowSpace(SomeStruct{ RegionID: regionID,TaxiParkUUID: taxiParkUUID})).
		Where("uuid = ?", uuid).
		Select()

	fmt.Println(rows)
}

*/

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
