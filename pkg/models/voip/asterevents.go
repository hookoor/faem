package voip

import (
	"time"
)

type AsterEvent struct {
	ID            int64     `json:"id"`
	CallID        string    `pg:"call_id"`
	Exten         string    `pg:"exten"`
	CallerID      string    `pg:"caller_id"`
	Status        string    `pg:"status"`
	Uid           string    `pg:"uid"`
	CallTimestamp time.Time `pg:"call_timestamp"`
	OrderUuid     string    `pg:"order_uuid"`
	Operator      string    `pg:"operator"`
	Record        string    `pg:"record"`
	DialStatus    string    `pg:"dial_status"`
	Channel       string    `pg:"channel"`
	CreatedAt     time.Time `json:"created_at"`
	ConfID        string    `sql:"confid"`
	CRMAction     string    `sql:"crmaction"`
}
