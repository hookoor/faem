package rabbit

// ext - для внешних приложений (android, ios, фронтэнд)
// int - для внутреннего взаимодействия микросервисов
const (
	// Новые обменники
	DriverExchange   = "events.driver"
	OrderExchange    = "events.order"
	ClientExchange   = "events.client"
	OperatorExchange = "events.operator"
	EventExchange    = "events.short"

	OfferExchange         = "events.offer"
	TicketsExchange       = "events.tickets"
	FCMExchange           = "events.fcm"
	VoipExchange          = "events.voip"
	DelayedReCallExchande = "voip.delayed"
	BillingExchange       = "events.billing"
	ChatExchange          = "events.chat"
	PhotocontrolExchange  = "events.photocontrol"
	NotifierExchange      = "events.notifier"

	ChatMessagesMemberHeaderName = "member_uuid" //contains uuid of reciever if it is message about marking messages as read and uuid of sender if it is message about new chat message

	OrderUpdateByOperatorRequestTag = "update.by.operator"
	// Обменник для нового заказа из ClientApp
	rawClientExchNewOrder = "int.client.new_order"
	// Обменник для отправки в клиентское приложение опций созданных црмке
	rawCrmExchNewOptionsToClient = "int.new_options_to_client"
	rawDriverOffersExchange      = "ext.orders_offers"

	// Обменники
	// Обменник для координат машин
	// rawDriverLocationExchange = "driver.locations"
	// Статусы водителей из водительского приложения
	// rawDriverDriverStatesExchange = "int.drv.driver_states"
	// rawExchOrderUpdate = "int.order_update"
	// rawCrmExchNewOrder      = "int.orders"
	// rawDriverOrderStatesExchange = "ext.order_states"
	// rawOrderStatesExchange     = "int.order_state"
	// rawCrmDriverStatesExchange = "int.crm.driver_states"
	// Статусы заказов из CRM в водительское приложение
	// rawCrmOrderStatesExchange   = "int.driver.order_states"
	// rawUpdateDriverDataExchange = "int.update_driver"
	// rawDriverNewDriverEchange = "events.driver"

	// Очереди
	EventsOrderQueue     = "events.order"
	EventsDriverQueue    = "events.driver"
	EventsClientQueue    = "events.client"
	EventsFCMQueue       = "events.fcm"
	EventsOperatorQueue  = "events.operator"
	EventsBillingQueue   = "events.billing"
	EventsTicketsQueue   = "events.tickets"
	EventsTelephonyQueue = "events.telephony"

	BonusesOrderStateQueue     = "bon.order.state"
	BonusesEventNewClientQueue = "bon.event_new_client"
	BonusesNewTransfersQueue   = "bon.transfers.new"

	CrmNewDriverQueue            = "crm.driver.new"
	CrmNewClientQueue            = "crm.client.new"
	CrmUpdateClientQueue         = "crm.client.update"
	CrmDriverStateQueue          = "crm.driver.state"
	CrmChatMessagesQueue         = "crm.chat.message"
	CrmTicketsQueue              = "crm.tickets.queue"
	CrmDriverUpdateQueue         = "crm.driver.update"
	CrmOrderStateQueue           = "crm.order.state"
	CrmActionOnOrderQueue        = "crm.action_on_order"
	CrmNewOrderQueue             = "crm.order.new"
	CrmNewOrderFromBotQueue      = "crm.new_order_from_bot"
	CrmOrderUpdateQueue          = "crm.order.update"
	CrmNewPhotoControlQueue      = "crm.photocontrol.new"
	CrmBillingCompletedQueue     = "crm.billing.completed"
	CrmBillingPaymentTypeChanged = "crm.billing.payment_type_changed"

	DriverDriverUpdateQueue         = "driver.driver.update"
	DriverNewDriverQueue            = "driver.driver.new"
	DriverDriverDeleteQueue         = "driver.driver.delete"
	DriverChatMessageQueue          = "driver.chat.message"
	DriverTelephonyQueue            = "driver.telephony.new"
	DriverDriverStateQueue          = "driver.driver.state"
	DriverUpdatePhotoControlQueue   = "driver.photocontrol.new"
	DriverExpiredPhotocontrolsQueue = "driver.photocontrol.expired"
	DriverTicketsQueue              = "driver.tickets"
	DriverNewOrderQueue             = "driver.order.new"
	DriverOrderReDistrQueue         = "driver.order.redistr"
	DriverOrderUpdateQueue          = "driver.order.update"
	DriverOrderStateQueue           = "driver.order.state"
	DriverBillingCompletedQueue     = "driver.billing.completed"
	DriverUpdateGroupInDriversQueue = "driver.update_group_in_drivers.queue" // для обновления группы у водителей
	DriverIncomeTrackerQueue        = "driver.income_tracker"                // очередь для статистики заработка
	// очереди для ключа установления рейтинга
	CrmSetOrderRatingQueue          = "crm.order.rating"
	ClientSetOrderRatingQueue       = "client.order.rating"
	DriverSetOrderRatingQueue       = "driver.order.rating"
	DriverBillingPaymentTypeChanged = "driver.billing.payment_type_changed"

	CrmSetRouteToClientQueue    = "crm.order.route_to_client"
	ClientSetRouteToClientQueue = "client.order.route_to_client"
	//DriverSetRouteToClientQueue = "driver.order.route_to_client"

	CrmDriverActivityChangedQueue  = "crm.driver_activity_changed"
	CrmDriverKarmaChangedQueue     = "crm.driver_karma_changed"
	CrmClientActivityChangedQueue  = "crm.client_activity_changed"
	CrmClientKarmaChangedQueue     = "crm.client_karma_changed"
	CrmClientBlacklistChangedQueue = "crm.client_blacklist_changed"

	DriverClientLocationsQueue = "driver.client.locations"

	ClientNewOrChangedClientQueue   = "client.client.new_or_update"
	ClientOptionsOrder              = "client.options_to_client"
	ClientOrderStateQueue           = "client.order.state"
	ClientFCMQueue                  = "client.fcm.message"
	ClientSMSQueue                  = "client.sms.message"
	ClientTicketsQueue              = "client.tickets.data"
	ClientChatQueue                 = "client.fcm.chat"
	ClientOrderUpdateQueue          = "client.order.update"
	ClientDriverLocationQueue       = "client.driver_location"
	ClientBillingPaymentTypeChanged = "client.billing.payment_type_changed"
	ClientBillingReceiptGenerated   = "client.billing.receipt_generated"
	ClientReferralFirstRideQueue    = "client.referral_first_ride"

	LimeSidecarOrderStateQueue = "limesidecar.order.state"
	ChatOrderStateQueue        = "chat.order.states"
	ChatNewOrderQueue          = "chat.order.new"

	AnalyticEventsOrderQueue = "analytic.events.order"
	AnalyticOfferStatesQueue = "analytic.offer_states"
	AnalyticEventsQueue      = "analytic.events"
	//Bot SERVICE
	ChatNewBotOrderQueue  = "chat.order.new" // очередь для приема новых draft заказов из бот сервиса
	ChatNewBotMsgQueue    = "chat.botmsg.new"
	NewMsg2BotQueue       = "bot.newmsg.queue"
	BotOrderStatesQueue   = "bot.order_states"
	BotDriverFoundedQueue = "bot.driverFounded"
	BotOrderUpdate        = "bot.orderUpdate"
	BotNewOrders          = "bot.newOrders"

	VoipAutoCallQueue      = "voip.autocall"
	VoipAutoReCallQueue    = "voip.autoReCall"
	VoipNewOrders          = "voip.newOrders"
	VoipDriverFoundedQueue = "voip.driverFounded"
	VoipOrdersStateUpdate  = "voip.orderStateUpdate"
	VoipDriverClientQueue  = "voip.driver_to_client_call"

	BillingNewDriverQueue       = "billing.driver.new"
	BillingUpdatedDriverQueue   = "billing.driver.update"
	BillingDeletedDriverQueue   = "billing.driver.deleted"
	BillingNewClientQueue       = "billing.client.new"
	BillingUpdatedClientQueue   = "billing.client.update"
	BillingDeletedClientQueue   = "billing.client.deleted"
	BillingNewTransferQueue     = "billing.transfer.new"
	BillingPrepayOrderQueue     = "billing.order_state.change"
	BillingPenaltyClientQueue   = "billing.penalty_client"
	BillingRefundClientQueue    = "billing.refund_client"
	BillingGenerateReceiptQueue = "billing.generate_receipt"

	NotifierNewMessageQueue = "notifier.message.new"

	rawDriverLocationQueue = "drvQueue.locations"
	// rawCrmOrderUpdateQueue      = "crmQueue.order_update"
	rawClientOrderStateCrmQueue = "clientQueue.orderstate_crm"

	// Консюмеры
	CrmPhotoControlConsumer            = "crm.new_photo_control.consumer"
	CrmTicketsConsumer                 = "crm.tickets.consumer"
	CrmNewClientConsumer               = "crm.client_new.consumer"
	CrmUpdateClientConsumer            = "crm.client_update.consumer"
	CrmDriverUpdateConsumer            = "crm.drivers_update.consumer"
	CrmBillingCompletedConsumer        = "crm.billing_completed_consumer"
	DriverOrderUpdateConsumer          = "driver.order_update.consumer"
	DriverChatConsumer                 = "driver.chat.consumer"
	DriverClientLocationsConsumer      = "driver.cient_loc.consumer"
	DriverOrderReDistrConsumer         = "driver.order.redistr.consumer"
	DriverTicketsConsumer              = "driver.tickets.consumer"
	DriverPhotoControlConsumer         = "driver.update_photo_control.consumer"
	DriverExpiredPhotocontrolsConsumer = "driver.expired_photocontrol.consumer"
	DriverBillingCompletedConsumer     = "driver.billing_completed_consumer"
	DriverDriverDeletedConsumer        = "driver.driver_deleted_consumer"
	DriverIncomeTrackerConsumer        = "driver.income_tracker.consumer"
	EventsClientConsumer               = "events.client.consumer"
	EventsOperatorConsumer             = "events.operator.consumer"
	EventsDriverConsumer               = "events.driver.consumer"
	EventsFCMConsumer                  = "events.fcm.consumer"
	EventsTicketsConsumer              = "events.tickets.consumer"
	EventsOrderConsumer                = "events.order.consumer"
	EventsBillingConsumer              = "events.billing.consumer"
	ClientDriverLocationClientConsumer = "client.driver_location_consumer"
	ClientNewOrChangedClient           = "client.new_or_changed_consumer"
	ClientFCMConsumer                  = "client.new_mess_to_fcm_consumer"
	ClientSMSConsumer                  = "client.new_sms"
	ClientTicketsConsumer              = "client.new_tickets_data"
	ClientChatConsumer                 = "client.new_chat_mess_to_client"
	ClientReferralFirstRideConsumer    = "client.referral_first_ride.consumer"
	BonusesOrderStateConsumer          = "bon.order.state.consumer"
	BonusesTransferConsumer            = "bon.transfer.new.consumer"
	BonusesEventNewClientConsumer      = "bon.event_new_client.consumer"
	EventsTelephonyConsumer            = "events.telephony.consumer"
	DriverTelephonyConsumer            = "events.telephony.driver.consumer"

	CrmNewOrderFromBotConsumer         = "crm.new_order_from_bot.consumer"
	CrmSetOrderRatingConsumer          = "crm.order_rating.consumer"
	ClientSetOrderRatingConsumer       = "client.order_rating.consumer"
	DriverSetOrderRatingConsumer       = "driver.order_rating.consumer"
	DriverUpdateGroupInDriversConsumer = "driver.update_group_in_drivers.consumer" // для обновления группы у водителей

	CrmSetRouteToClientConsumer    = "crm.route_to_client.consumer"
	ClientSetRouteToClientConsumer = "client.route_to_client.consumer"

	CrmDriverActivityChangedConsumer  = "crm.driver_activity_changed.consumer"
	CrmDriverKarmaChangedConsumer     = "crm.driver_karma_changed.consumer"
	CrmClientActivityChangedConsumer  = "crm.client_activity_changed.consumer"
	CrmClientKarmaChangedConsumer     = "crm.client_karma_changed.consumer"
	CrmClientBlacklistChangedConsumer = "crm.client_blacklist_changed.consumer"
	CrmChatMessagesConsumer           = "crm.chat_mess.consumer"
	CrmActionOnOrderConsumer          = "crm.action_on_order.consumer"

	DriverBillingPaymentTypeChangedConsumer = "driver.billing_payment_type_changed_consumer"
	CrmBillingPaymentTypeChangedConsumer    = "crm.billing_payment_type_changed_consumer"
	ClientBillingPaymentTypeChangedConsumer = "client.billing_payment_type_changed_consumer"
	ClientBillingReceiptGeneratedConsumer   = "client.billing_receipt_generated_consumer"

	ChatOrderStatesConsumer = "chat.order_states"
	ChatNewOrderConsumer    = "chat.order_new"
	ChatNewBotOrderConsumer = "chat.order_from_bot"
	ChatNewBotMsgConsumer   = "chat.botnewmsg"
	NewOrderConsumer        = "bot.neworder_consumer"

	VoipAutoCallConsumer      = "voip.autocall_consumer"
	VoipAutoReCallConsumer    = "voip.autorecall_consumer"
	VoipNewOrderConsumer      = "voip.neworder_consumer"
	VoipDriverFoundedConsumer = "voip.driverfounded_consumer"
	VoipOrderUpdateConsumer   = "voip.orderStateUpdate_consumer"
	VoipDriverClientConsumer  = "voip.driver_client_consumer"

	BillingNewDriverConsumer       = "billing.new_driver_consumer"
	BillingUpdatedDriverConsumer   = "billing.updated_driver_consumer"
	BillingDeletedDriverConsumer   = "billing.deleted_driver_consumer"
	BillingNewClientConsumer       = "billing.new_client_consumer"
	BillingUpdatedClientConsumer   = "billing.updated_client_consumer"
	BillingDeletedClientConsumer   = "billing.deleted_client_consumer"
	BillingNewTransferConsumer     = "billing.new_transfer_consumer"
	BillingPrepayOrderConsumer     = "billing.changed_order_state_consumer"
	BillingPenaltyClientConsumer   = "billing.penalty_client_consumer"
	BillingRefundClientConsumer    = "billing.refund_client_consumer"
	BillingGenerateReceiptConsumer = "billing.generate_receipt_consumer"

	NotifierNewMessageConsumer = "notifier.new_message_consumer"

	rawCrmNewDriverConsumer           = "crm.new_driver"
	rawDriverLocationConsumer         = "driver.locations"
	rawDriverLocationCRMConsumer      = "crm.driver_locations"
	rawDriverNewOrderConsumer         = "driver.new_orders"
	rawDriverNewOrderStateConsumer    = "driver.order_state"
	rawCrmOrderUpdateConsumer         = "crm.order_update"
	rawDriverStateConsumer            = "driver.driverstate"
	rawClientOrderUpdateConsumer      = "client.order_update"
	rawClientOrderStateCrmConsumer    = "client.orderstate_crm"
	rawClientOrderStateDriverConsumer = "client.orderstate_driver"
	rawCrmDriverStateConsumer         = "crm.driverstate"
	rawCrmOrderStateConsumer          = "crm.orderstate"
	rawCrmNewOrderConsumer            = "crm.new_order"
	rawDriverDriverUpdateConsumer     = "driver.update_driver"

	rawCrmNewOptionsToClientConsumer = "client.new_options_to_client"

	AnalyticEventsOrderConsumer = "analytic.events.order.consumer"
	AnalyticOfferStatesConsumer = "analytic.offer_states.consumer"
	AnalyticEventsConsumer      = "analytic.events.consumer"

	BotNewMsgConsumer        = "bot.new_msg_consumer"
	BotOrderStateConsumer    = "bot.orderstate_consumer"
	BotDriverFoundedConsumer = "bot.driverfounded_consumer"
	// Keys -------------------------------
	PhotoControlKey       = "new_photo_control"
	SMSKey                = "new_sms"
	PhotoControlUpdateKey = "update_photo_control"

	NewKey                  = "new"
	UpdateKey               = "update"
	UnmarkKey               = "unmark_as_important"
	InitDataKey             = "init_data"
	UpdateOnlyCommentKey    = "update.comment"
	DeleteKey               = "delete"
	NewConferenceKey        = "driver_to_client_call"
	UpdateOrderTariffKey    = "update.tariff"
	UpdateOrderProductData  = "update.product_data"
	UpdateOrderRoutesKey    = "update.routes"
	UpdateIncreasedFareKey  = "update.increased_fare"
	ReDistributionKey       = "distribution.new" //если надо перераспределить заказ
	OrderDeliveredKey       = "delivered.key"    //order delivered to driver
	DistributionKey         = "distribution"
	DistributionNoDriverKey = "distribution_no_driver"  //когда не найдены свободные водители
	UpdateGroupInDrivers    = "update_group_in_drivers" // для обновления группы у водителей
	ReferralFirstRideKey    = "referral_first_ride"

	// TODO: возможно это надо делать по другому, усли нет убери тудушку
	OperatorCreateOrder  = "operator.create_order"
	OperatorUpdateOrder  = "operator.update_order"
	OperatorUpdateDriver = "operator.update_driver"

	OrderUpdateFromCRMKey                     = "update_from_crm" // чтобы обновленные данные заказа не уходили на driver при driver_accepted
	StateKey                                  = "state"
	UpdateOrderCommentWithLongDistributionKey = "update.comment"
	MailingKey                                = "mailing"
	ExpiredKey                                = "expired"
	LocationKey                               = "location"
	ClientLocationKey                         = "client_location"
	LoggedIn                                  = "logged_in"
	SetRatingKey                              = "set_rating"
	RouteToClient                             = "route_to_client"
	OptionsUpdateKey                          = "options.update"
	NewChatOrder                              = "start_chat_order" // специальный ключ для создания пред-заказа из чат-бота
	// voip keys
	AutoCallKey = "autocall"

	HangupAsterStatus     = "hangup"
	StartcallAsterStatus  = "startcall"
	AnswerAsterStatus     = "answer"
	IncomingAsterStatus   = "incoming"
	RingingAsterStatus    = "ringing"
	PlayendAsterStatus    = "playend"
	ConferenceAsterStatus = "conference"
	ConfHangupAsterStatus = "hangup-conf"
	IVRAsterStatus        = "ivr"

	// fcm keys
	ChatMessageToClientKey      = "chat.message.client"
	ChatMessageToDriverKey      = "chat.message.driver"
	ChatMessageToOperatorKey    = "chat.message.operator"
	ChatMessagesMarkedAsReadKey = "chat.message.read"

	ActivityChangedKey = "activity_changed"
	KarmaChangedKey    = "karma_changed"

	BlacklistChangedKey = "blacklist_changed"

	//
	ActionOnOrderKey = "action_on_order"

	// Billing routing keys
	CompletedKey             = "completed"
	PrepayKey                = "prepay"
	PaymentTypeChangedToCash = "payment_changed_to_cash"
	PenaltyClient            = "penalty_client"
	RefundClient             = "refund_client"
	GenerateReceipt          = "generate_receipt"
	ReceiptGenerated         = "receipt_generated"

	rawDrvCrmNewDriverState = "newstate"
	rawDrvCrmNewDriver      = "new_driver"

	rawLimeSidecarOrderStateConsumer = "limesidecar.order_state"

	//Chat routing keys
	NewIncomingMsgKey = "incoming"
)

var (
	DriverNewDriverExchange string

	// DriverLocationExchange string
	// DriverDriverStatesExchange string
	// CrmOrderStatesExchange     string
	// обменник для отправки статусов водителей из CRM
	// CrmDriverStatesExchange string
	// обменник для отправки новых водителей из DriverApp
	// CrmExchNewOrder string
	//ExchOrderUpdate обменник для обновленных данных о заказе
	// ExchOrderUpdate      string
	DriverOffersExchange string
	// DriverOrderStatesExchange string
	// OrderStatesExchange string
	//Обменник для обновления данных водителя
	UpdateDriverDataExchange string
	DriverLocationQueue      string
	// для изменений данных водителя с Црмки
	// DriverDriverUpdateQueue string
	//СlientOrderUpdateQueue принимает обновления заказа в client
	// ClientOrderUpdateQueue string
	//ClientOrderStateCrmQueue принимает обновления статусов заказа из crm-ки в client
	ClientOrderStateCrmQueue string
	//ClientOrderStateCrmQueue принимает обновления статусов заказа из driver в client
	// ClientOrderStateQueue string
	// DriverNewOrderQueue   string
	// обменник для заказов от клиентского приложения
	ClientExchNewOrder string
	// очередь для заказов от клиентского приложения
	// ClientNewOrderQueue string
	// очередь, из которой водительское приложение принимает статусы водителей из CRM
	// DriverDriverStateQueue string
	// // очередь, из которой CRM принимает новых водителей из DriverApp
	// CRMNewDriverQueue         string
	// CrmDriverStateQueue       string
	// CrmOrderUpdateQueue   string
	// DriverOrderStateQueue string
	// CrmOrderStateQueue        string
	DriverLocationConsumer    string
	DriverLocationCRMConsumer string
	// консюмер для статусов водителей в водительском приложении
	DriverStateConsumer string
	// ClientOrderUpdateConsumer обновления заказов
	ClientOrderUpdateConsumer string

	// CrmNewDriverConsumer консюмер для новых водителей в CRM
	CrmNewDriverConsumer   string
	DriverNewOrderConsumer string
	// ClientOrderStateCrmConsumer консюмер для новых статусов заказов из crm-ки в client
	ClientOrderStateCrmConsumer string
	// ClientOrderStateDriverConsumer консюмер для новых статусов заказов из driver в client
	ClientOrderStateDriverConsumer string
	DriverNewOrderStateConsumer    string
	CrmDriverStateConsumer         string
	//DriverDriverUpdateConsumer для обновления данных водителя на driverApp
	DriverDriverUpdateConsumer string
	DriverNewDriverConsumer    string
	CrmOrderStateConsumer      string
	CrmNewOrderConsumer        string
	CrmOrderUpdateConsumer     string
	// NewOrderKey                string
	DrvCrmNewDriverState string
	// Ключ для нового драйвера из DriverAPP в CRM
	// CrmDrvNewDriverStateKey string
	//CrmDrvNewDriverKey Ключ для нового драйвера из DriverAPP в CRM
	CrmDrvNewDriverKey string

	// Обменник для отправки в клиентское приложение опции созданного в црмке
	CrmNewOptionsToClientExchange string
	// Очереди и консьюмеры для новых опци
	ClientNewOptionsToClientQueue    string
	ClientNewOptionsToClientConsumer string

	LimeSidecarOrderStateConsumer string
)

func initRabbitVars(pref, postf string) {
	// DriverLocationExchange = pref + rawDriverLocationExchange + postf
	// DriverNewDriverExchange = pref + rawDriverNewDriverEchange + postf
	// CrmOrderStatesExchange = pref + rawCrmOrderStatesExchange + postf
	// CrmDriverStatesExchange = pref + rawCrmDriverStatesExchange + postf
	// DriverDriverStatesExchange = pref + rawDriverDriverStatesExchange + postf
	// CrmExchNewOrder = pref + rawCrmExchNewOrder + postf
	DriverOffersExchange = pref + rawDriverOffersExchange + postf
	// DriverOrderStatesExchange = pref + rawDriverOrderStatesExchange + postf
	// OrderStatesExchange = pref + rawOrderStatesExchange + postf
	DriverLocationQueue = pref + rawDriverLocationQueue + postf
	// DriverDriverStateQueue = pref + rawDriverDriverStateQueue + postf
	// DriverNewOrderQueue = pref + rawDriverNewOrderQueue + postf
	// CRMNewDriverQueue = pref + rawCrmNewDriverQueue + postf
	// DriverOrderStateQueue = pref + rawDriverOrderStateQueue + postf
	// CrmDriverStateQueue = pref + rawCrmDriverStateQueue + postf
	// CrmOrderStateQueue = pref + rawCrmOrderStateQueue + postf
	DriverLocationConsumer = pref + rawDriverLocationConsumer + postf
	DriverLocationCRMConsumer = pref + rawDriverLocationCRMConsumer + postf
	DriverNewOrderConsumer = pref + rawDriverNewOrderConsumer + postf
	DriverStateConsumer = pref + rawDriverStateConsumer + postf
	CrmNewDriverConsumer = pref + rawCrmNewDriverConsumer + postf
	DriverNewOrderStateConsumer = pref + rawDriverNewOrderStateConsumer + postf
	CrmDriverStateConsumer = pref + rawCrmDriverStateConsumer + postf
	CrmOrderStateConsumer = pref + rawCrmOrderStateConsumer + postf
	// NewOrderKey = pref + rawNewOrderKey + postf
	DrvCrmNewDriverState = pref + rawDrvCrmNewDriverState + postf
	// CrmDrvNewDriverStateKey = pref + rawCrmDrvNewDriverStateKey + postf
	CrmDrvNewDriverKey = pref + rawDrvCrmNewDriver + postf
	ClientExchNewOrder = pref + rawClientExchNewOrder + postf
	// ClientNewOrderQueue = pref + rawClientNewOrderQueue + postf
	CrmNewOrderConsumer = pref + rawCrmNewOrderConsumer + postf
	DriverDriverUpdateConsumer = pref + rawDriverDriverUpdateConsumer + postf
	// UpdateDriverDataExchange = pref + rawUpdateDriverDataExchange + postf
	// DriverDriverUpdateQueue = pref + rawDriverDriverUpdateQueue + postf
	// ExchOrderUpdate = pref + rawExchOrderUpdate + postf
	ClientOrderUpdateConsumer = pref + rawClientOrderUpdateConsumer + postf
	// ClientOrderUpdateQueue = pref + rawClientOrderUpdateQueue + postf
	ClientOrderStateCrmQueue = pref + rawClientOrderStateCrmQueue + postf
	// ClientOrderStateQueue = pref + rawClientOrderStateQueue + postf
	ClientOrderStateCrmConsumer = pref + rawClientOrderStateCrmConsumer + postf
	ClientOrderStateDriverConsumer = pref + rawClientOrderStateDriverConsumer + postf
	CrmOrderUpdateConsumer = pref + rawCrmOrderUpdateConsumer + postf
	CrmNewOptionsToClientExchange = pref + rawCrmExchNewOptionsToClient + postf
	// ClientNewOptionsToClientQueue = pref + rawCrmNewOptionsToClientQueue + postf
	ClientNewOptionsToClientConsumer = pref + rawCrmNewOptionsToClientConsumer + postf
	// CrmOrderUpdateQueue = pref + rawCrmOrderUpdateQueue + postf
	LimeSidecarOrderStateConsumer = pref + rawLimeSidecarOrderStateConsumer + postf
}
