package rabbit

import "encoding/json"

type JsonEncoder struct{}

func (j *JsonEncoder) Encode(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func (j *JsonEncoder) Decode(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

func (j *JsonEncoder) ContentType() string {
	return "application/json"
}
