package rabbit

import (
	"fmt"
	"sync"

	"github.com/pkg/errors"
	"go.uber.org/multierr"
)

// Rabbit структура для работы с rabbit
type Rabbit struct {
	Connection *Connection
	Credits    ConnCredits

	channels struct {
		m   map[string]*Channel
		mtx sync.Mutex
	}
}

// ConnCredits data for connection
type ConnCredits struct {
	User string
	URL  string
}

// New создает Rabbit инстанс
func New() (r *Rabbit) {
	return &Rabbit{}
}

// Init инициализирует подключение
func (rb *Rabbit) Init(pf ...string) error {
	var pref, post string

	switch len(pf) {
	case 1:
		pref, post = pf[0], ""
	case 2:
		pref, post = pf[0], pf[1]
	default:
		pref, post = "", ""
	}
	initRabbitVars(pref, post)

	var err error
	connString := fmt.Sprintf("amqp://%s@%s/", rb.Credits.User, rb.Credits.URL)
	rb.Connection, err = Dial(connString)
	if err != nil {
		er := fmt.Errorf("Error dialing Rabbit 0_o. %s", err)
		return er
	}

	rb.channels.m = make(map[string]*Channel)
	return nil
}

// CloseRabbit закрывает rabbitMQ соединение
func (rb *Rabbit) CloseRabbit() error {
	// Close all the channels first
	rb.channels.mtx.Lock()
	defer rb.channels.mtx.Unlock()

	var err error
	for _, ch := range rb.channels.m {
		err = multierr.Append(err, ch.Close())
	}

	// Then close the connection itself
	return multierr.Append(err, rb.Connection.Close())
}

func (rb *Rabbit) GetReceiver(name string) (*Channel, error) {
	return rb.getChannel("receiver." + name)
}

func (rb *Rabbit) GetSender(name string) (*Channel, error) {
	return rb.getChannel("sender." + name)
}

func (rb *Rabbit) getChannel(name string) (*Channel, error) {
	rb.channels.mtx.Lock()
	defer rb.channels.mtx.Unlock()

	if ch, found := rb.channels.m[name]; found {
		return ch, nil
	}

	ch, err := rb.Connection.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "failed to create a channel")
	}
	rb.channels.m[name] = ch
	return ch, nil
}
