package fcm

import (
	"github.com/NaySoftware/go-fcm"
)

type (
	FcmStruct struct {
		Tag                 string      `json:"tag"`
		NotificationMessage string      `json:"notification_message,omitempty"`
		Payload             interface{} `json:"payload"`
	}
)

const (
	OrderStateTag       = "order_state"
	InitDataTag         = "update_init_data"
	ChatMessageTag      = "chat_message"
	NewOrderToDriverKey = "new_order_to_driver"
	MailingTag          = "mailing"
	MailingURL          = "mailing_url"
	ChatMessagesReadTag = "chat_messages_read"
	DriverLocTag        = "driver_location"
)

// NewClient возвращает клиента для работы с FCM
func NewClient(serverKey string) *fcm.FcmClient {

	c := fcm.NewFcmClient(serverKey)
	// New()

	return c

}

// func New() (*firebase.App, error) {
// 	registrationToken := "dRRBOJyGwCk:APA91bGCPJ0N79tMgmlVWfzTzWiNPYNPNBA01dY3zrtEViBHo1WMyeiiyJdkIaV1nb8B5NiYqoI4KhH_vgnM16DCjTJK6FPpVV7xTintWCDp9tEJ4gk309LReAlBU2V9hX0XiEd50Azg"
// 	ctx := context.Background()
// 	order := structures.Order{}
// 	order.UUID = "ffffffffffffffff"
// 	app, err := firebase.NewApp(ctx, nil)
// 	client, err := app.Messaging(ctx)
// 	if err != nil {
// 		log.Fatalf("error getting Messaging client: %v\n", err)
// 	}
// 	jsonOrder, _ := json.Marshal(order)
// 	var objmap map[string]*json.RawMessage
// 	err = json.Unmarshal(jsonOrder, &objmap)
// 	order = structures.Order{}

// 	// See documentation on defining a message payload.
// 	message := &messaging{
// 		Data:  objmap,
// 		Token: registrationToken,
// 	}
// 	response, err := client.Send(ctx, message)
// 	if err != nil {
// 		log.Fatalln(err)
// 	}
// 	fmt.Println("Successfully sent message:", response)
// 	return app, err
// }
