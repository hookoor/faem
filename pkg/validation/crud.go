package validation

import (
	"reflect"
	"strings"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	create = "create"
	update = "update"
)

func Create(value interface{}) error {
	return validateCU(reflect.ValueOf(value), create)
}

func Update(value interface{}) error {
	return validateCU(reflect.ValueOf(value), update)
}

func validateCU(v reflect.Value, operation string) error {
	if v.Kind() == reflect.Ptr {
		return validateCU(reflect.Indirect(v), operation)
	}

	if v.Kind() != reflect.Struct {
		return errors.Errorf("cannot use type %v in func ValidateCU", v.Kind())
	}

	for i := 0; i < v.NumField(); i++ {
		if v.Field(i).Kind() == reflect.Struct && v.Field(i).Type().Name() != "Time" {
			if err := validateCU(v.Field(i), operation); err != nil {
				return err
			}
			continue
		}

		tag, ok := v.Type().Field(i).Tag.Lookup("required_on")
		if !ok {
			continue
		}

		if !structures.StringInArray(operation, strings.Split(tag, ",")) {
			continue
		}

		if reflect.Zero(v.Field(i).Type()).Interface() == v.Field(i).Interface() {
			return errors.Errorf("field %s of %s needed for %s, but is empty", v.Type().Field(i).Name, v.Type().Name(), operation)
		}
	}

	return nil
}
