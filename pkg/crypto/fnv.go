package crypto

import (
	"hash/fnv"
)

func FNV(str string) int {
	h := fnv.New32a()
	h.Write([]byte(str))
	return int(h.Sum32())
}
