package lang

import (
	"fmt"
	"log"
)

type Procedure func()
type ErrorFunction func() error

func Recover(proceed Procedure) Procedure {
	return func() {
		defer func() {
			if r := recover(); r != nil {
				log.Printf("recover from panic: %v", r)
			}
		}()

		proceed()
	}
}

func RecoverWithError(proceed ErrorFunction) (err error) {
	defer func() {
		if r := recover(); r != nil {
			var ok bool
			err, ok = r.(error)
			if !ok {
				err = fmt.Errorf("%v", r)
			}
			log.Printf("recover from panic with error: %v", err)
		}
	}()

	err = proceed()
	return
}
