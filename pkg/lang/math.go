package lang

import (
	"math"
)

func Log(x, base float64) float64 {
	return math.Log(x) / math.Log(base)
}
