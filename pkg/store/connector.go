package store

import (
	"fmt"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

func Connect(opts *pg.Options) (*pg.DB, error) {
	conn := pg.Connect(opts)

	// Test connection
	var ping int
	_, err := conn.QueryOne(pg.Scan(&ping), "SELECT 1")
	if err != nil {
		return nil, errors.Wrap(err, "failed to connect to the db")
	}

	// Log queries
	conn.AddQueryHook(dbLogger{})
	return conn, nil
}

func Addr(host string, port int) string {
	return fmt.Sprintf("%s:%v", host, port)
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	logs.Eloger.Trace(q.FormattedQuery())
}
