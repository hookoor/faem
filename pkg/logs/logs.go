package logs

import (
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	strct "gitlab.com/faemproject/backend/faem/pkg/structures"
)

// Логика ЛОГОВ

// 	"event": "set driver state" //  Само событие
// 	"driverUUID": de00a4e1-7c63-47cd-b426-23302f37cb86
// 	"offerUUID": 0738ac67-c18f-4040-8801-d5de68a5d1e3
//  "orderUUID": e5f16e62-fbdd-4444-b2d1-8f3de3ef55a5
// 	"reason": "driver request" // Причина срабатываения
// 	"value": newState // Значение

var (
	// Eloger logrus instance
	Eloger *logrus.Logger
)

func init() {
	Eloger = logrus.New()
	// default logging level
	Eloger.SetLevel(logrus.InfoLevel)
}

// SetLogFormat sets level for the logger.
func SetLogLevel(level string) error {
	lvl, err := logrus.ParseLevel(level)
	if err != nil {
		return errors.Wrap(err, "failed to parse a log level")
	}
	Eloger.SetLevel(lvl)
	return nil
}

// SetLogFormat sets format for the logger.
func SetLogFormat(format string) error {
	var formatter logrus.Formatter
	switch format {
	case "text":
		formatter = &logrus.TextFormatter{}
	case "json":
		formatter = &logrus.JSONFormatter{}
	default:
		return fmt.Errorf("unknown log format %q", format)
	}
	Eloger.SetFormatter(formatter)
	return nil
}

// OutputRestError отдает структуру с ошибкой
func OutputRestError(errorDesc string, err error, code ...int) strct.ResponseStruct {

	// Eloger.WithFields(logrus.Fields{
	// 	"error": err,
	// }).Error(errorDesc)

	var cd int
	if len(code) > 0 {
		cd = code[0]
	} else {
		cd = 400
	}

	msg := errorDesc
	if err != nil {
		msg += ". " + err.Error()
	}

	return strct.ResponseStruct{
		Code: cd,
		Msg:  msg,
	}
}

// OutputRestOK godoc
func OutputRestOK(msg string, code ...int) strct.ResponseStruct {
	var cd int
	if len(code) > 0 {
		cd = code[0]
	} else {
		cd = 200
	}
	return strct.ResponseStruct{
		Code: cd,
		Msg:  fmt.Sprintf("%s", msg),
	}
}

// OutputUserEvent godoc godoc
func OutputUserEvent(event, msg string, userID int) {
	Eloger.WithFields(logrus.Fields{
		"event":  event,
		"userID": userID,
	}).Info(msg)
}

// OutputEvent godoc godoc
func OutputEvent(event, msg string, userID int) {
	fmt.Printf("\n\n EVENT must be cutted - %s, Message - %s\n\n\n", event, msg)
	// Eloger.WithFields(logrus.Fields{
	// 	"event":  event,
	// 	"userID": userID,
	// }).Info(msg)
}

// OutputDebugEvent godoc
func OutputDebugEvent(event, msg string, userID int) {
	Eloger.WithFields(logrus.Fields{
		"event":  event,
		"userID": userID,
	}).Debug(msg)
}

// OutputDriverEvent godoc
func OutputDriverEvent(event, msg string, driver string) {
	// Eloger.WithFields(logrus.Fields{
	// 	"event":    event,
	// 	"driverID": driver,
	// }).Info(msg)
}

// OutputClientEvent godoc
func OutputClientEvent(event, msg string, client string) {
	Eloger.WithFields(logrus.Fields{
		"event":    event,
		"clientID": client,
	}).Info(msg)
}

// OutputOrderEvent godoc
func OutputOrderEvent(event, msg string, driver string) {
	Eloger.WithFields(logrus.Fields{
		"event":   event,
		"orderID": driver,
	}).Info(msg)
}

// OutputInfo godoc
func OutputInfo(msg string) {
	Eloger.Info(msg)
}

// OutputError godoc
func OutputError(msg string) {
	// Eloger.Error(msg)
}
