package osrm

import "github.com/pkg/errors"

type Error struct {
	Code    string `json:"code"`
	Message string `json:"message"`
}

func (e Error) ParseError() error {
	if e.Code == "Ok" {
		return nil
	}
	return errors.Errorf("osrm: %s - %s", e.Code, e.Message)
}
