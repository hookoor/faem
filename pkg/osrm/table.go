package osrm

import (
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"math"
	"net/http"
	"strings"
)

type TableResponse struct {
	Error
	Sources []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"sources"`
	Destinations []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"destinations"`
	Durations [][]float64 `json:"durations"` // время в секундах всего маршрута от i-го источника к j-ому назначению
	Distances [][]float64 `json:"distances"` // расстояние в метрах всего маршрута от i-го источника к j-ому назначению
}

func (r *TableResponse) GetDuration(i, j int) float64 {
	if i >= len(r.Durations) || j >= len(r.Durations[i]) {
		return math.MaxFloat32
	}
	return r.Durations[i][j]
}

func (r *TableResponse) GetDistance(i, j int) float64 {
	if i >= len(r.Distances) || j >= len(r.Distances[i]) {
		return math.MaxFloat32
	}
	return r.Distances[i][j]
}

func (o *OSRM) Table(coordinates, sources, destinations []structures.PureCoordinates) (TableResponse, error) {
	return o.table(
		convertCoordinates(coordinates),
		convertCoordinates(sources),
		convertCoordinates(destinations),
	)
}

func (o *OSRM) table(coordinates, sources, destinations []Coordinate) (TableResponse, error) {
	if len(coordinates) < 2 {
		return TableResponse{}, errors.New("expected at least 2 coordinates input")
	}

	httpClient := &http.Client{Timeout: osrmTimeout}
	urlString := o.endpoint(osrmServiceTable, osrmProfileDriving)
	urlString += CoordinatesToString(coordinates)
	// build query params
	params := "?annotations=distance,duration"
	params += "&generate_hints=false"
	if len(sources) > 0 {
		params += "&sources=" + coordinateIndicesToString(coordinates, sources)
	}
	if len(destinations) > 0 {
		params += "&destinations=" + coordinateIndicesToString(coordinates, destinations)
	}
	// append query params to url
	urlString += params

	req, err := http.NewRequest(http.MethodGet, urlString, nil)
	if err != nil {
		return TableResponse{}, err
	}

	resp, err := httpClient.Do(req)
	if err != nil {
		return TableResponse{}, err
	}
	defer resp.Body.Close()

	var result TableResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return TableResponse{}, errors.Wrap(err, "failed to decode OSRM response")
	}
	if err := result.ParseError(); err != nil {
		return TableResponse{}, err
	}

	return result, nil
}

func coordinateIndicesToString(coordinates, coordsSubSlice []Coordinate) string {
	result := make([]string, len(coordsSubSlice))
	for i, coord := range coordsSubSlice {
		index := coord.FindIndex(coordinates)
		result[i] = fmt.Sprint(index)
	}

	return strings.Join(result, ";")
}
