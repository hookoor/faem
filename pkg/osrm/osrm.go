// TODO: дорефакторить OSRM и вынести в shared
package osrm

import (
	"fmt"
	"time"
)

// Доступные сервисы OSRM
const (
	osrmServiceTable = "table"
	osrmServiceRoute = "route"
)

// Доступные профили OSRM
const (
	osrmProfileDriving = "driving"
)

const (
	osrmVersion = "v1"
	osrmTimeout = 30 * time.Second
)

type OSRM struct {
	Host string
}

func New(host string) *OSRM {
	return &OSRM{
		Host: host,
	}
}

func (o *OSRM) endpoint(service, profile string) string {
	return fmt.Sprintf("%s/%s/%s/%s/", o.Host, service, osrmVersion, profile)
}
