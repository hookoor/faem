package osrm

import (
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"strings"
)

type Coordinate struct {
	Longitude, Latitude float64
}

func (c Coordinate) String() string {
	return fmt.Sprintf("%.6f,%.6f", c.Longitude, c.Latitude)
}

func (c Coordinate) FindIndex(coordinates []Coordinate) int {
	for i := range coordinates {
		if coordinates[i] == c {
			return i
		}
	}

	return -1
}

func CoordinatesToString(coordinates []Coordinate) string {
	result := make([]string, len(coordinates))
	for i, coord := range coordinates {
		result[i] = coord.String()
	}

	return strings.Join(result, ";")
}

func convertCoordinates(coordinates []structures.PureCoordinates) []Coordinate {
	result := make([]Coordinate, len(coordinates))
	for i, coord := range coordinates {
		result[i] = Coordinate{
			coord.Long,
			coord.Lat,
		}
	}

	return result
}
