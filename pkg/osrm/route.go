package osrm

import (
	"encoding/json"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"net/http"
)

type RouteResponse struct {
	Error
	Routes []struct {
		Geometry struct {
			Coordinates [][]float64 `json:"coordinates"` // набор координат из которых состоит маршрут
			Type        string      `json:"type"`
		} `json:"geometry"`
		Legs []struct {
			Summary  string  `json:"summary"`
			Weight   float64 `json:"weight"`
			Duration float64 `json:"duration"` // время в секундах
			// Steps    []string `json:"steps"`
			Distance float64 `json:"distance"` // расстояние в метрах
		} `json:"legs"`
		WeightName string  `json:"weight_name"`
		Weight     float64 `json:"weight"`
		Duration   float64 `json:"duration"` // время в секундах всего маршрута
		Distance   float64 `json:"distance"` // расстояние в метрах всего маршрута
	} `json:"routes"`
	Waypoints []struct {
		Hint     string    `json:"hint"`
		Distance float64   `json:"distance"`
		Name     string    `json:"name"`
		Location []float64 `json:"location"`
	} `json:"waypoints"`
}

func (o *OSRM) Route(coordinates []structures.PureCoordinates) (RouteResponse, error) {
	return o.route(
		convertCoordinates(coordinates),
	)
}

func (o *OSRM) route(coordinates []Coordinate) (RouteResponse, error) {
	if len(coordinates) < 2 {
		return RouteResponse{}, errors.New("expected at least 2 coordinates input")
	}

	httpClient := &http.Client{Timeout: osrmTimeout}
	urlString := o.endpoint(osrmServiceRoute, osrmProfileDriving)
	urlString += CoordinatesToString(coordinates)
	// build query params
	params := "?geometries=geojson"
	// append query params to url
	urlString += params

	req, err := http.NewRequest(http.MethodGet, urlString, nil)
	if err != nil {
		return RouteResponse{}, err
	}

	resp, err := httpClient.Do(req)
	if err != nil {
		return RouteResponse{}, err
	}
	defer resp.Body.Close()

	var result RouteResponse
	if err = json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return RouteResponse{}, errors.Wrap(err, "failed to decode OSRM response")
	}
	if err := result.ParseError(); err != nil {
		return RouteResponse{}, err
	}

	return result, nil
}
