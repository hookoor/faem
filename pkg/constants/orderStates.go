package constants

// Статусы заказов
const (
	OrderStateCreated        = "order_created"      // Создан
	OrderStateDistributing   = "smart_distribution" // Распределяется в рамках поэтапного алгоритма
	OrderStateFree           = "finding_driver"     // Свободный заказ (водители могут сами его взять из списка)
	OrderStateOffered        = "offer_offered"      // Предложен водителю
	OrderStateAccepted       = "offer_accepted"     // Принят водителем
	OrderStateRejected       = "offer_rejected"     // Отклонен водителем
	OrderStateDriverNotFound = "driver_not_found"   // Водитель не найден (истек срок заказа)
	OrderStateCancelled      = "offer_cancelled"    // Отменен
	OrderStateStarted        = "order_start"        // Начато выполнение заказа
	OrderStateOnPlace        = "on_place"           // Водитель на месте (приехал к пассажирам)
	OrderStateOnTheWay       = "on_the_way"         // В пути (заказ выполняется)
	OrderStateOnPoint        = "on_point"           // Доехал до промежуточной точки
	OrderStateWaiting        = "waiting"            // Водитель ожидает клиента
	OrderStatePayment        = "order_payment"      // Оплата заказа
	OrderStateFinished       = "finished"           // Заказ выполнен
)

// Вроде как статусы Еды
const (
	OrderStateConfirmation = "waiting_for_confirmation"
	OrderStateCooking      = "cooking"
	OrderStateTransToStore = "transferred_to_store"
)

// Вроде как статусы Бота
const (
	// Начало чата с оператором
	OrderStateChatStart = "start_chatting"
	// Статус когда заказ в чате готов для запуска, используется когда например нет контакта.
	// После получения контакта, мы его запустим
	OrderStateChatStartOrder = "start_chatting_order"
	OrderStateChatEnd        = "end_chatting"
)

var OrderStateRU = map[string]string{
	OrderStateCreated:        "Заказ создан",
	OrderStateCooking:        "Заказ готовится",
	OrderStateDistributing:   "Распределение",
	OrderStateTransToStore:   "Передан заведению",
	OrderStateFree:           "Свобод.",
	OrderStateConfirmation:   "Ожидает подтверждения",
	OrderStateOffered:        "Предлагается",
	OrderStateAccepted:       "Принят водителем",
	OrderStateRejected:       "Отклонен водителем",
	OrderStateDriverNotFound: "Авто не найдено",
	OrderStateCancelled:      "Отменен",
	OrderStateStarted:        "Выехал к клиенту",
	OrderStateOnPlace:        "Ожидает клиента",
	OrderStateOnTheWay:       "Выполнение заказа",
	OrderStateOnPoint:        "Доехал до промежуточной точки",
	OrderStatePayment:        "Оплата заказа",
	OrderStateFinished:       "Завершен",
	OrderStateWaiting:        "Ожидает",
}

// ListOrderStates возвращает список всех существующих статусов заказов
func ListOrderStates() []string {
	return []string{
		OrderStateCreated,
		OrderStateDistributing,
		OrderStateFree,
		OrderStateOffered,
		OrderStateAccepted,
		OrderStateRejected,
		OrderStateDriverNotFound,
		OrderStateCancelled,
		OrderStateStarted,
		OrderStateOnPlace,
		OrderStateOnTheWay,
		OrderStateOnPoint,
		OrderStateWaiting,
		OrderStatePayment,
		OrderStateFinished,
		OrderStateConfirmation,
		OrderStateCooking,
		OrderStateTransToStore,
		OrderStateChatStart,
		OrderStateChatStartOrder,
		OrderStateChatEnd,
	}
}

func DistributingStates() []string {
	return []string{
		OrderStateDistributing,
		OrderStateFree,
	}
}

func ListActiveOrderStates() []string {
	return []string{
		OrderStateDistributing,
		OrderStateAccepted,
		OrderStateRejected,
		OrderStateCooking,
		OrderStateConfirmation,
		OrderStateFree,
		OrderStateCreated,
		OrderStateOnPlace,
		OrderStateStarted,
		OrderStateOffered,
		OrderStatePayment,
		OrderStateOnTheWay,
		OrderStateWaiting,
		OrderStateTransToStore,
	}
}

//InactiveOrderStatesWoChat неактивные статусы без чата
func inactiveOrderStatesWoChat() []string {
	return []string{
		OrderStateFinished,
		OrderStateDriverNotFound,
		OrderStateCancelled,
	}
}

func InactiveOrderStates(state string) bool {
	for _, st := range inactiveOrderStatesWoChat() {
		if st == state {
			return true
		}
	}
	return false
}

func ListInactiveOrderStates() []string {
	return []string{
		OrderStateFinished,
		OrderStateDriverNotFound,
		OrderStateCancelled,
		OrderStateChatStart,
		OrderStateChatStartOrder,
		OrderStateChatEnd,
	}
}

func InDistributingStates(state string) bool {
	for _, st := range DistributingStates() {
		if st == state {
			return true
		}
	}
	return false
}
func InActiveStates(state string) bool {
	for _, st := range ListActiveOrderStates() {
		if st == state {
			return true
		}
	}
	return false
}
func RunningOrderStatesList() []string {
	return []string{
		OrderStateOnTheWay,
		OrderStateWaiting,
		OrderStatePayment,
	}
}

func ReplaceStateForClientIfNeed(state string, isProdDeliveryOrder bool) string {
	if !isProdDeliveryOrder {
		return state
	}
	switch state {
	case OrderStateAccepted,
		OrderStateOnPlace,
		OrderStateStarted,
		OrderStateOffered,
		OrderStateRejected:

		return OrderStateCooking
	case OrderStateWaiting:

		return OrderStateOnTheWay
	}
	return state
}

func TranslateOrderStateForClient(state string, isProductDeliveryOrder bool) string {
	switch state {
	case OrderStateCreated,
		OrderStateDistributing,
		OrderStateFree,
		OrderStateRejected:
		return "Поиск водителя"
	case OrderStateOffered:
		return "Заказ предложен водителю"
	case OrderStateAccepted:
		return "Заказ принят водителем"
	case OrderStateDriverNotFound:
		return "Машина не найдена"
	case OrderStateCancelled:
		return "Заказ отменен"
	case OrderStateStarted:
		return "Водитель приедет через"
	case OrderStateTransToStore:
		return "Передан заведению"
	case OrderStateOnPlace:
		return "Водитель подъехал"
	case OrderStateConfirmation:
		return "Заказ в обработке"
	case OrderStateCooking:
		return "Заказ готовится"
	case OrderStateOnTheWay,
		OrderStateOnPoint:
		if isProductDeliveryOrder {
			return "Водитель везет ваш заказ"
		}
		return "Заказ выполняется"
	case OrderStateWaiting:
		return "Водитель ожидает"
	case OrderStatePayment:
		if isProductDeliveryOrder {
			return "Водитель подъехал"
		}
		return "Оплата заказа"
	case OrderStateFinished:
		return "Завершен"
	default:
		return ""
	}
}

func IsStateForPushToClient(state string) bool {
	for _, item := range StatesForPushToClient() {
		if item == state {
			return true
		}
	}
	return false
}

func StatesForPushToClient() []string {
	return []string{
		OrderStateCreated,
		OrderStateAccepted,
		OrderStateOnPlace,
		OrderStateWaiting,
		OrderStateFinished,
	}
}
