package constants

// Статусы водителей
const (
	DriverStateCreated                 = "driver_created"          // Создан
	DriverStateOnModeration            = "on_moderation"           // На модерации
	DriverStateOnModerationAfterChange = "moderation_after_change" // На модерации после изменения
	DriverStateBlocked                 = "blocked"                 // Заблокирован
	DriverStateOnline                  = "online"                  // Онлайн (доступен для заказов)
	DriverStateAvailable               = "available"               // Прошел модерацию
	DriverStateWorking                 = "working"                 // Выполняет заказ
	DriverStateConsidering             = "considering"             // Обдумывает оффер
	DriverStateOffline                 = "offline"                 // Оффлайн (не готов принимать заказы)
)

var DriverStateRU = map[string]string{
	DriverStateCreated:                 "Создан",
	DriverStateOnModerationAfterChange: "На модерации после изменения данных",
	DriverStateAvailable:               "Прошел модерацию",
	DriverStateBlocked:                 "Заблокирован",
	DriverStateOnModeration:            "На модерации",
	DriverStateOnline:                  "Доступен для заказа",
	DriverStateWorking:                 "Выполняет заказ",
	DriverStateOffline:                 "Не готов принимать заказы",
}

func ListDriverStates() []string {
	return []string{
		DriverStateCreated,
		DriverStateOnModeration,
		DriverStateOnModerationAfterChange,
		DriverStateBlocked,
		DriverStateOnline,
		DriverStateAvailable,
		DriverStateWorking,
		DriverStateConsidering,
		DriverStateOffline,
	}
}
