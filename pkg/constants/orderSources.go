package constants

const (
	OrderSourceCRM             = "crm"
	OrderSourceLime            = "lime"
	OrderSourceDelivery        = "deliver_partners"
	OrderSourceAndroid         = "android_client_app_1"
	OrderSourceIOS             = "ios_client_app_1"
	OrderSourceEdaFaem         = "eda.faem.ru"
	OrderSourcePartnerIOS      = "partner_ios"
	OrderSourcePartnerWebSite  = "partner_web_site"
	OrderSourcePartnerAndroid  = "partner_android"
	OrderSourceSuperAppIOS     = "ios_faem_superapp"
	OrderSourceSuperAppAndroid = "android_faem_superapp"
	OrderSourceTelegram        = "telegram"
	OrderSourceWhatsapp        = "whatsapp"
)

var OrderSourceRU = map[string]string{
	OrderSourceCRM:             "Оператор",
	OrderSourceLime:            "Лайм",
	OrderSourceDelivery:        "Партнерская админка",
	OrderSourceAndroid:         "Приложение на Android",
	OrderSourceIOS:             "Приложение на iOS",
	OrderSourceEdaFaem:         "Сайт агрегатора еды",
	OrderSourcePartnerIOS:      "Партнерское приложение на iOS",
	OrderSourcePartnerWebSite:  "Партнерский сайт",
	OrderSourcePartnerAndroid:  "Партнерское приложение на Android",
	OrderSourceTelegram:        "Бот Telegram",
	OrderSourceWhatsapp:        "Бот WhatsApp",
	OrderSourceSuperAppIOS:     "СуперПриложение iOS",
	OrderSourceSuperAppAndroid: "СуперПриложение Android",
}

func ListOrderSources() []string {
	return []string{
		OrderSourceCRM,
		OrderSourceLime,
		OrderSourceDelivery,
		OrderSourceAndroid,
		OrderSourceIOS,
		OrderSourceEdaFaem,
		OrderSourcePartnerIOS,
		OrderSourcePartnerWebSite,
		OrderSourcePartnerAndroid,
		OrderSourceSuperAppIOS,
		OrderSourceSuperAppAndroid,
		//OrderSourceTelegram,
		//OrderSourceWhatsapp,
	}
}
