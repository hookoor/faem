package constants

const (
	OrderPaymentTypeCash           = "cash" // default
	OrderPaymentTypeCard           = "card"
	OrderPaymentTypeOnlineTransfer = "online_transfer"

	// TODO: переделать текущую логику бесплатных (бонусных) заказов на отдельный тип оплаты
	OrderPaymentTypeUnpaid = "unpaid"
)

var OrderPaymentTypeRU = map[string]string{
	OrderPaymentTypeCash:           "Наличные",
	OrderPaymentTypeCard:           "Картой",
	OrderPaymentTypeOnlineTransfer: "Онлайн-перевод",
	OrderPaymentTypeUnpaid:         "Бонусы (не брать оплату с клиента)",
}
