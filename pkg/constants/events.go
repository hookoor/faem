package constants

const (
	EventNewOrder                 = "new_order"
	EventUnmarkOrder              = "unmark_order"
	EventNewTicket                = "new_ticket"
	EventTicketNewStatus          = "new_ticket_status"
	EventOrderState               = "order_state"
	EventOrderUpdate              = "order_update"
	EventIncreasedFare            = "inc_fare"
	EventDistribution             = "distribution"
	EventReDistribution           = "re_distribution"
	EventMessReadOne              = "mess_read_one"
	EventMessReadMany             = "mess_read_many"
	EventMessFromOperatorToDriver = "mess_oper_driver"
	EventOrderDelivered           = "order_delivered"

	EventNewDriver    = "new_driver"
	EventDriverState  = "driver_state"
	EventDriverUpdate = "driver_update"

	EventOperatorLogged = "operator_logged"
	EventNewOperator    = "new_operator"
	EventOperatorUpdate = "operator_update"

	EventTelephonyHangup    = "telephony_hangup"
	EventTelephonyStartCall = "telephony_startcall"
	EventTelephonyAnswer    = "telephony_answer"
	EventTelephonyIncoming  = "telephony_incoming"
	EventTelephonyRinging   = "telephony_ringing"
	EventTelephonyPlayEnd   = "telephony_playend"
	EventTelephony          = "telephony"

	EventNewTransfer = "new_payment_transfer"

	EventActivityChanged = "activity_changed"
)

var EventRU = map[string]string{
	EventNewOrder:                 "Заказ создан",
	EventUnmarkOrder:              "С заказа снята отметка важности",
	EventOrderState:               "Статус заказа изменен",
	EventIncreasedFare:            "Клиент обновил надбавочную стоимость",
	EventOrderUpdate:              "Заказ обновлен",
	EventNewTicket:                "Новый тикет создан",
	EventTicketNewStatus:          "Статус тикета изменен",
	EventReDistribution:           "Запрос на перераспределение заказа",
	EventDistribution:             "Данные распределения",
	EventMessReadOne:              "Оператор прочитал новое сообщение",
	EventMessReadMany:             "Оператор прочитал новые сообщения",
	EventMessFromOperatorToDriver: "Оператор написал новое сообщение водителю",
	EventOrderDelivered:           "Заказ доставлен водителю",
	EventNewDriver:                "Новый водитель",
	EventDriverState:              "Статус водителя изменен",
	EventDriverUpdate:             "Данные водителя обновлены",
	EventOperatorLogged:           "Оператор вошел в сеть",
	EventNewOperator:              "Новый оператор",
	EventOperatorUpdate:           "Данные оператора обновлены",
	EventTelephonyHangup:          "Звонок завершен",
	EventTelephonyStartCall:       "Звонок начат",
	EventTelephonyIncoming:        "Входящий звонок",
	EventTelephonyRinging:         "Звонок идет",
	EventTelephonyAnswer:          "Ответ",
	EventTelephonyPlayEnd:         "Конец воспроизведения",
	EventTelephony:                "Звонок",
	EventNewTransfer:              "Новый платежный перевод",
	EventActivityChanged:          "Изменение активности",
}
