package constants

const (
	FcmTagInitData         = "update_init_data"
	FcmTagNewOrderToDriver = "new_order_to_driver"
	FcmTagClientLoc        = "client_locations"
	FcmTagClientNotified   = "client_notified"
)
