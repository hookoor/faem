package constants

// Причины отмены заказа из CRM
const (
	CancelReasonNoDrivers       = "no_driver"
	CancelReasonSystemError     = "system_mistake"
	CancelReasonClient          = "client_canceled_order"
	CancelReasonCantReach       = "can_not_reach"
	CancelReasonExpiredConfTime = "expired_confirmation_time"
	CancelReasonDriver          = "guilt_of_driver"
)

func ListCrmCancelReasons() []string {
	return []string{
		CancelReasonNoDrivers,
		CancelReasonSystemError,
		CancelReasonClient,
		CancelReasonCantReach,
		CancelReasonExpiredConfTime,
		CancelReasonDriver,
	}
}

// Причины отмены заказа из клиентского приложения
const (
	CancelReasonOrderedByMistake = "ordered_my_mistake"
	CancelReasonDriverAsked      = "the_driver_asked_to_cancel"
	CancelReasonLongWait         = "long_wait"
	CancelReasonAnotherTaxi      = "left_in_another_taxi"
	CancelReasonOtherWay         = "the_driver_drove_the_othe_way"
)

func ListClientCancelReasons() []string {
	return []string{
		CancelReasonOrderedByMistake,
		CancelReasonDriverAsked,
		CancelReasonLongWait,
		CancelReasonAnotherTaxi,
		CancelReasonOtherWay,
	}
}

// Причины блокировки водителя
const (
	DriverBlockReasonPhotocontrol = "photocontrol_expired"
	DriverBlockReasonByAdmin      = "admin"
)

var (
	CancelReasonRU = map[string]string{
		CancelReasonNoDrivers:       "Нет водителей",
		CancelReasonSystemError:     "Системная ошибка",
		CancelReasonExpiredConfTime: "У заведения нет возможности обработать заказ",
		CancelReasonClient:          "Клиент отменил заказ",
		CancelReasonCantReach:       "Невозможно дозвониться до клиента",
		CancelReasonDriver:          "По вине водителя",

		CancelReasonOrderedByMistake: "Заказал по ошибке",
		CancelReasonDriverAsked:      "Водитель попросил отменить",
		CancelReasonLongWait:         "Большое ожидание",
		CancelReasonAnotherTaxi:      "Уехал на другом такси",
		CancelReasonOtherWay:         "Водитель уехал в другую сторону",
	}

	// TODO: думаю плохая идея хардкодить ссылки на картинки
	CancelReasonImages = map[string]string{
		CancelReasonOrderedByMistake: "https://storage.googleapis.com/faem-ios-images/cancel_reasons_icons/reas1.png",
		CancelReasonDriverAsked:      "https://storage.googleapis.com/faem-ios-images/cancel_reasons_icons/reas2.png",
		CancelReasonLongWait:         "https://storage.googleapis.com/faem-ios-images/cancel_reasons_icons/reas3.png",
		CancelReasonAnotherTaxi:      "https://storage.googleapis.com/faem-ios-images/cancel_reasons_icons/reas4.png",
		CancelReasonOtherWay:         "https://storage.googleapis.com/faem-ios-images/cancel_reasons_icons/reas5.png",
	}

	DriverBlockReasonRU = map[string]string{
		DriverBlockReasonPhotocontrol: "Необходимо пройти фотоконтроль",
		DriverBlockReasonByAdmin:      "Доступ запрещен администратором",
	}
)
