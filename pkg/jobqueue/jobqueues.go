package jobqueue

import (
	"sync"
)

type JobQueues struct {
	queues map[string]*JobQueue
	mx     sync.Mutex
}

func NewJobQueues() *JobQueues {
	return &JobQueues{queues: make(map[string]*JobQueue)}
}

func (j *JobQueues) GetJobQueue(name string, initLimit int) *JobQueue {
	j.mx.Lock()
	defer j.mx.Unlock()

	if jq, found := j.queues[name]; found {
		return jq
	}

	jq := NewJobQueue(initLimit)
	j.queues[name] = jq
	return jq
}
