// jobqueue package reduces parallelism providing an ability to execute the code sequentially
// depending on the provided job id.

package jobqueue

import (
	"sync"
)

const (
	// DefaultLimit is the default queue limit.
	DefaultLimit = 97
)

// JobQueue object.
type JobQueue struct {
	limit   int
	tickets []*sync.Mutex
}

// NewJobQueue allocates a new JobQueue.
func NewJobQueue(limit int) *JobQueue {
	if limit <= 0 {
		limit = DefaultLimit
	}

	// Allocate a worker queue instance
	jobs := JobQueue{
		limit:   limit,
		tickets: make([]*sync.Mutex, limit),
	}

	// Allocate the tickets
	for i := 0; i < jobs.limit; i++ {
		jobs.tickets[i] = &sync.Mutex{}
	}

	return &jobs
}

// Execute adds a job to the sequentially execution queue.
func (j *JobQueue) Execute(jobID int, job func() error) error {
	ticket := j.ticketByJobID(jobID)
	ticket.Lock()
	defer ticket.Unlock()

	return job() // run the job
}

// ticketByJobID returns a ticket by a provided job id.
func (j *JobQueue) ticketByJobID(id int) *sync.Mutex {
	return j.tickets[id%j.limit]
}
