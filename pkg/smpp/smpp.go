package smpp

import (
	"fmt"
	"time"

	"github.com/fiorix/go-smpp/smpp"
	"github.com/fiorix/go-smpp/smpp/pdu/pdufield"
	"github.com/fiorix/go-smpp/smpp/pdu/pdutext"
)

// Sender main package struct
type Sender struct {
	From         string
	ConnStatus   <-chan smpp.ConnStatus
	tx           smpp.Transmitter
	SenderStatus chan SenderStatus
}

// SenderStatus структура для хранения статуса отправки
type SenderStatus struct {
	State string
	Msg   string
	MsgID string
}

// NewSmsSender создает SMS Message инстанс
func NewSmsSender(host, user, pass, name string) (*Sender, error) {
	sndr := &Sender{}
	sndr.From = name
	sndr.tx = smpp.Transmitter{
		Addr:   host,
		User:   user,
		Passwd: pass,
	}
	sndr.SenderStatus = make(chan SenderStatus)
	sndr.ConnStatus = sndr.tx.Bind()

	// check initial connection status
	var status smpp.ConnStatus
	if status = <-sndr.ConnStatus; status.Error() != nil {
		return &Sender{}, status.Error()
	}

	return sndr, nil
}

// SendSMS fires smsbody to phone and return message ID
func (msg *Sender) SendSMS(phone, smsbody, from string) {

	var (
		err error
		sm  *smpp.ShortMessage
	)

	codec := pdutext.UCS2(smsbody)
	for i := 0; i < 5; i++ {
		sm, err = msg.tx.Submit(&smpp.ShortMessage{
			Src:      from,
			Dst:      phone,
			Text:     codec,
			Register: pdufield.NoDeliveryReceipt,
		})
		if err == nil {
			resp := fmt.Sprintf("SMS to <%s> sended! ID:%v", phone, sm.RespID())
			msg.SenderStatus <- SenderStatus{
				Msg:   resp,
				MsgID: sm.RespID(),
				State: "Ok",
			}
			return
		}

		resp := fmt.Sprintf("Error sending SMS to <%s> attempt: %v, error: %s, messSize:%v", phone, i, err, len(codec))
		msg.SenderStatus <- SenderStatus{
			Msg:   resp,
			State: "Warn",
		}
		time.Sleep(time.Duration(i+1) * time.Second)
	}

	resp := fmt.Sprintf("SMS to <%s> not sended. error: %s, messSize:%v", phone, err, len(codec))
	msg.SenderStatus <- SenderStatus{
		Msg:   resp,
		State: "Error",
	}
}
