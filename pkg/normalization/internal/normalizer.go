package internal

import (
	"errors"
	"math"
)

var ErrInvalidNormBounds = errors.New("normalization: min should be lower or equal to max")

type Normalizer interface {
	Normalize(value *float64) error
	UpdateBoundsIfNeed(value float64)
}

// -----------------------------------------------------------------------------

type DefaultNormalizer struct {
	min float64
	max float64
}

func NewDefaultNormalizer() *DefaultNormalizer {
	var n DefaultNormalizer

	n.min = math.MaxFloat64
	n.max = -math.MaxFloat64

	return &n
}

func (n *DefaultNormalizer) Normalize(value *float64) error {
	if n.max < n.min {
		return ErrInvalidNormBounds
	}
	if n.max == n.min {
		*value = 1
	} else {
		*value = (*value - n.min) / (n.max - n.min)
	}
	return nil
}

func (n *DefaultNormalizer) UpdateBoundsIfNeed(value float64) {
	if value < n.min {
		n.min = value
	}
	if value > n.max {
		n.max = value
	}
}

// -----------------------------------------------------------------------------

type InverseNormalizer struct {
	min float64
	max float64
}

func NewInverseNormalizer() *InverseNormalizer {
	var n InverseNormalizer

	n.min = math.MaxFloat64
	n.max = -math.MaxFloat64

	return &n
}

func (n *InverseNormalizer) Normalize(value *float64) error {
	if n.max < n.min {
		return ErrInvalidNormBounds
	}
	if n.max == n.min {
		*value = 0
	} else {
		inverseValue := invert(*value)
		inverseMin := invert(n.max)
		inverseMax := invert(n.min)
		*value = (inverseValue - inverseMin) / (inverseMax - inverseMin)
	}
	return nil
}

func invert(x float64) float64 {
	if x <= 1 {
		return 1
	}
	return 1 / x
}

func (n *InverseNormalizer) UpdateBoundsIfNeed(value float64) {
	if value < n.min {
		n.min = value
	}
	if value > n.max {
		n.max = value
	}
}
