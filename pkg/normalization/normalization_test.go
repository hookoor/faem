package normalization

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNormalize(t *testing.T) {
	type TestStruct struct {
		DefaultField float64 `normalize:"default"`
		InverseField float64 `normalize:"inverse"`
	}

	arr := []TestStruct{
		{-100, 10},
		{0, 5},
		{35, 2},
		{50, 20},
		{99, 25},
		{100, 0},
		{100, 0},
	}

	err := Normalize(&arr)
	assert.NoError(t, err)

	normArr := []TestStruct{
		{0, 0.0625},
		{0.5, 0.166666666},
		{0.675, 0.47916666},
		{0.75, 0.010416666},
		{0.995, 0},
		{1, 1},
		{1, 1},
	}

	delta := 1e-6
	for i := range arr {
		assert.InDelta(t, normArr[i].DefaultField, arr[i].DefaultField, delta)
		assert.InDelta(t, normArr[i].InverseField, arr[i].InverseField, delta)
	}
}
