package normalization

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/normalization/internal"
	"reflect"
)

const (
	typeDefault = "default"
	typeInverse = "inverse"

	tagKey = "normalize"
)

func getNormalizers(t reflect.Type) (map[string]internal.Normalizer, error) {
	if t.Kind() != reflect.Struct {
		return nil, errors.Errorf("struct expected, got '%s'", t.Kind())
	}

	result := make(map[string]internal.Normalizer)
	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		tag, ok := field.Tag.Lookup(tagKey)
		if !ok {
			continue
		}

		switch tag {
		case typeDefault:
			result[field.Name] = internal.NewDefaultNormalizer()
		case typeInverse:
			result[field.Name] = internal.NewInverseNormalizer()
		case "":
			return nil, errors.New("empty normalization type specified")
		default:
			return nil, errors.Errorf("unsupported normalization type: '%s'", tag)
		}
	}

	return result, nil
}

func Normalize(slice interface{}) error {
	s := reflect.ValueOf(slice)
	s = reflect.Indirect(s)

	if s.Kind() != reflect.Slice && s.Kind() != reflect.Array {
		return errors.New("ты че дурак")
	}

	// do nothing on zero-length slice
	if s.Len() == 0 {
		return nil
	}

	normalizers, err := getNormalizers(s.Index(0).Type())
	if err != nil {
		return err
	}

	for i := 0; i < s.Len(); i++ {
		el := s.Index(i)
		for name, norm := range normalizers {
			field := el.FieldByName(name)
			if field.Kind() != reflect.Float64 {
				return errors.New("normalization works only on float64 fields")
			}

			norm.UpdateBoundsIfNeed(field.Float())
		}
	}

	for i := 0; i < s.Len(); i++ {
		el := s.Index(i)
		for name, norm := range normalizers {
			field := el.FieldByName(name)
			val := field.Float()
			if err := norm.Normalize(&val); err != nil {
				return err
			}

			field.SetFloat(val)
		}
	}

	return nil
}
