package phoneverify

import (
	"fmt"

	"github.com/ttacon/libphonenumber"
)

// NumberVerify verifies and formats a phone number
func NumberVerify(number string) (string, error) {

	num, err := libphonenumber.Parse(number, "")
	if err != nil {
		return "", fmt.Errorf("error parce number, %s", err)
	}

	if !libphonenumber.IsValidNumber(num) {
		return "", fmt.Errorf("invalid phone number")
	}
	number = libphonenumber.Format(num, libphonenumber.E164)
	return number, nil
}
