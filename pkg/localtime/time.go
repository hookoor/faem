package localtime

import "time"

func TimeInZone(t time.Time, zone string) time.Time {
	loc, err := time.LoadLocation(zone)
	if err == nil {
		t = t.In(loc)
	}
	return t
}
