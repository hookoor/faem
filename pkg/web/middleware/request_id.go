package middleware

import (
	"context"

	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/random"
)

func RequestID() echo.MiddlewareFunc {
	return requestID
}

func requestID(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		req := c.Request()
		rid := req.Header.Get(echo.HeaderXRequestID)
		if rid == "" {
			rid = generator()
		}
		ctx := context.WithValue(req.Context(), echo.HeaderXRequestID, rid)
		c.SetRequest(req.WithContext(ctx))

		return next(c)
	}
}

func generator() string {
	return random.String(16)
}
