package web

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

const (
	HealthCheckReadEndpoint = "/health/ready"
	HealthCheckLiveEndpoint = "/health/alive"
)

func UseHealthCheck(e *echo.Echo) {
	e.GET(HealthCheckReadEndpoint, HealthCheck)
	e.GET(HealthCheckLiveEndpoint, HealthCheck)
}

func HealthCheck(c echo.Context) error {
	return c.String(http.StatusOK, "OK")
}
