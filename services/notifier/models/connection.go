package models

import (
	"time"

	"github.com/dgrijalva/jwt-go"
)

const (
	ConnectionTTL = time.Hour * 168 // a week
)

type ConnectionClaims struct {
	jwt.StandardClaims
}

func NewConnectionClaims(sub string) ConnectionClaims {
	return ConnectionClaims{
		StandardClaims: jwt.StandardClaims{
			Subject:   sub,
			ExpiresAt: time.Now().Add(ConnectionTTL).Unix(),
		},
	}
}
