package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	maxPublishedMessagesAllowed = 50
)

func (s *Subscriber) HandlePublishedMessage(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var notification structures.Notification
	if err := s.Encoder.Decode(msg.Body, &notification); err != nil {
		return errors.Wrap(err, "failed to decode a notification")
	}
	log := logs.Eloger.WithField("notification", notification)

	// Handle incoming message
	if err := s.Handler.PublishMessage(ctx, notification); err != nil {
		log.Errorf("can't publish a notification: %s", err)
		return errors.Wrap(err, "failed to publish a notification")
	}

	log.Debug("notification published successfully")
	return nil
}

func (s *Subscriber) initMessagePublished() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.NotifierNewMessageQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.NotifierExchange, // name
		"topic",                 // type
		true,                    // durable
		false,                   // auto-deleted
		false,                   // internal
		false,                   // no-wait
		nil,                     // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.NotifierNewMessageQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,              // queue name
		rabbit.NewKey,           // routing key
		rabbit.NotifierExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                        // queue
		rabbit.NotifierNewMessageConsumer, // consumer
		false,                             // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handlePublishedMessage(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handlePublishedMessage(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxPublishedMessagesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandlePublishedMessage(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to handle a new message: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
