package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "notifier"
)

type Application struct {
	Env            string
	Port           string
	LogLevel       string
	LogFormat      string
	AuthServiceUrl string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

func (a *Application) Validate() error {
	return nil
}

type Broker struct {
	UserURL     string
	UserCredits string
}

func (b *Broker) Validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

type Centrifugo struct {
	Addr     string
	ApiKey   string
	TokenKey string
}

func (c *Centrifugo) Validate() error {
	if c.Addr == "" {
		return errors.New("empty centrifugo address provided")
	}
	if c.ApiKey == "" {
		return errors.New("empty centrifugo api key provided")
	}
	if c.TokenKey == "" {
		return errors.New("empty centrifugo token key provided")
	}
	return nil
}

type Config struct {
	Application Application
	Broker      Broker
	Centrifugo  Centrifugo
}

func (c *Config) Validate() error {
	return multierr.Combine(
		c.Application.Validate(),
		c.Broker.Validate(),
		c.Centrifugo.Validate(),
	)
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

func (c *Config) Print() {
	if c.Application.IsProduction() {
		return
	}
	inspected := *c // get a copy of an actual object
	// Hide sensitive data
	inspected.Broker.UserCredits = ""
	fmt.Printf("%+v\n", inspected)
}

func setDefaults() {
	viper.SetDefault("Application.Env", "production")
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")
	viper.SetDefault("Application.Port", "8080")
	viper.SetDefault("Application.AuthServiceUrl", "http://auth-auth")

	viper.SetDefault("Broker.UserURL", "")
	viper.SetDefault("Broker.UserCredits", "")

	viper.SetDefault("Centrifugo.Addr", "")
	viper.SetDefault("Centrifugo.ApiKey", "")
	viper.SetDefault("Centrifugo.TokenKey", "")
}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}
