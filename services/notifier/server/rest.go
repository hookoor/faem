package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/core/shared/auth"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/notifier/handler"
	"log"
)

const (
	apiPrefix = "/api/v2"
)

type Rest struct {
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route(router *echo.Echo) {
	web.UseHealthCheck(router)
	// Open API group
	open := router.Group(apiPrefix)

	// Protected API group
	protected := router.Group(apiPrefix)
	jwtMiddleware, err := auth.GetJWTMiddlewareWithSupportV2(r.Handler.Config.Application.AuthServiceUrl)
	if err != nil {
		log.Fatalf("jwtMiddleware err: %v", err)
	}
	protected.Use(jwtMiddleware)

	protected.GET("/connectiontoken", r.ConnectionToken)

	open.GET(structures.EndPoints.Notyfire.IsPresence.URL(), r.IsPresence)
}
