package server

import (
	"gitlab.com/faemproject/backend/core/shared/auth"
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/notifier/helpers"
)

func (r *Rest) ConnectionToken(c echo.Context) error {
	var (
		userUUID string
		err      error
	)
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "connection token")

	// v2 auth client/driver token parsing
	if userUUID, err = helpers.UserUUIDFromJWT(c); err != nil {
		// v3 auth  client token parsing
		if userUUID, err = auth.StrKeyFromJWT(c, auth.KeyClientUUID); err != nil {
			// v3 auth  driver token parsing
			if userUUID, err = auth.StrKeyFromJWT(c, "uuid"); err != nil {
				logger.Error(err)
				res := logs.OutputRestError("cannot parse user id: ", err)
				return c.JSON(http.StatusUnauthorized, res)
			}
		}
	}

	result, err := r.Handler.ConnectionToken(userUUID)
	if err != nil {
		logger.WithField("reason", "generating a connection token").Error(err)
		res := logs.OutputRestError("Ошибка генерации токена", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, result)

}

// IsPresence -
func (r *Rest) IsPresence(c echo.Context) error {
	ctx := c.Request().Context()
	logger := logs.LoggerForContext(ctx).WithField("event", "check presence")

	client := c.QueryParam("client")
	if client == "" {
		err := errpath.Errorf("empty param 'client'")
		logger.WithField("reason", "check presence").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("пустой параметр 'client'", err))
	}
	channel := c.QueryParam("channel")
	if channel == "" {
		err := errpath.Errorf("empty param 'channel'")
		logger.WithField("reason", "check presence").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("пустой параметр 'channel'", err))
	}

	result, err := r.Handler.IsPresence(ctx, channel, client)
	if err != nil {
		err = errpath.Err(err)
		logger.WithField("reason", "check presence").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("Ошибка проверки присутсвия соединения", err))
	}
	return c.JSON(http.StatusOK, result)
}
