package helpers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

func UserUUIDFromJWT(c echo.Context) (string, error) {
	user, ok := c.Get("user").(*jwt.Token)
	if !ok {
		return "", errors.New("invalid user field")
	}

	claims, ok := user.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid claims")
	}

	clientUUID, clientFound := claims["client_uuid"]
	driverUUID, driverFound := claims["driver_uuid"]
	staffUUID, staffFound := claims["user_uuid"]
	//v3 token support
	UUID, ok := claims["uuid"]

	if !clientFound && !driverFound && !staffFound {
		return "", errors.New("no permissible user uuid is found in jwt")
	}

	if clientFound {
		userUUID, ok := clientUUID.(string)
		if !ok {
			return "", errors.New("invalid client uuid claim")
		}
		return userUUID, nil
	}

	if driverFound {
		userUUID, ok := driverUUID.(string)
		if !ok {
			return "", errors.New("invalid driver uuid claim")
		}
		return userUUID, nil
	}

	// Staff found
	if staffFound {
		userUUID, ok := staffUUID.(string)
		if !ok {
			return "", errors.New("invalid staff uuid claim")
		}
		return userUUID, nil
	}

	userUUID, ok := UUID.(string)
	if !ok {
		return "", errors.New("invalid v3_uuid claim")
	}

	return userUUID, nil
}
