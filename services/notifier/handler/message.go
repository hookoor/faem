package handler

import (
	"context"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (h *Handler) PublishMessage(ctx context.Context, message structures.Notification) error {
	return h.cent.Publish(ctx, message.Channel, message.Payload)
}
