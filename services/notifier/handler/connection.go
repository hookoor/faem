package handler

import (
	"github.com/dgrijalva/jwt-go"

	"gitlab.com/faemproject/backend/faem/services/notifier/models"
)

type ConnectionTokenResult struct {
	Token string `json:"token"`
}

func (h *Handler) ConnectionToken(userUUID string) (ConnectionTokenResult, error) {
	claims := models.NewConnectionClaims(userUUID)
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenString, err := token.SignedString([]byte(h.Config.Centrifugo.TokenKey))
	if err != nil {
		return ConnectionTokenResult{}, err
	}
	return ConnectionTokenResult{
		Token: tokenString,
	}, nil
}
