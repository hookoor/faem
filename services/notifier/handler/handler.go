package handler

import (
	"context"
	"net/http"
	"time"

	"github.com/centrifugal/gocent"

	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/notifier/config"
)

const (
	CentrifugoTimeout = 30 * time.Second
)

type Publisher interface {
}

type Handler struct {
	Pub    Publisher
	Config config.Config

	cent *gocent.Client
}

func NewHandler(pub Publisher, cfg config.Config) *Handler {
	cent := gocent.New(gocent.Config{
		Addr: cfg.Centrifugo.Addr,
		Key:  cfg.Centrifugo.ApiKey,
		HTTPClient: &http.Client{
			Timeout: CentrifugoTimeout,
		},
	})

	return &Handler{
		Pub:    pub,
		Config: cfg,

		cent: cent,
	}
}

// ------------------------
// ------------------------
// ------------------------

// IsPresence -
func (h *Handler) IsPresence(ctx context.Context, channel string, user string) (bool, error) {
	pesenceres, err := h.cent.Presence(ctx, channel)
	if err != nil {
		return false, errpath.Err(err)
	}

	for _, val := range pesenceres.Presence {
		if val.User == user {
			return true, nil
		}
	}

	return false, nil
}
