package repository

import (
	"github.com/go-pg/pg"
)

type Pg struct {
	CrmDb    *pg.DB
	VoipDb   *pg.DB
	DriverDb *pg.DB
	ClientDb *pg.DB
}
