package repository

import (
	"context"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/scheduler/models"
)

const (
	replyTimeSeconds = 600
	queryBlock       = 15000
)

func (p *Pg) CleanAsterEvents(t time.Duration) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	//
	var astEvents models.AsterEvents
	tm := time.Now().Add(-t)
	res, err := p.VoipDb.ModelContext(ctx, &astEvents).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete aster events")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanAsterAutocalls(t time.Duration) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	//
	var astAutocalls models.AsterAutoCall
	tm := time.Now().Add(-t)
	res, err := p.VoipDb.ModelContext(ctx, &astAutocalls).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete aster autocalls")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanAsterCRMOrders(t time.Duration) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	//
	var astOrders models.AsterOrders
	tm := time.Now().Add(-t)
	res, err := p.VoipDb.ModelContext(ctx, &astOrders).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete aster orders")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) GetActiveOrders(lastDate time.Time) ([]string, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()

	var activeOrders []models.OrderCRM

	err := p.CrmDb.ModelContext(ctx, &activeOrders).
		Column("uuid").
		WhereIn("order_state IN (?)", constants.ListActiveOrderStates()).
		Where("state_transfer_time < ?", lastDate).
		Where("deleted IS false").
		Select()

	if err != nil {
		return nil, errors.Wrap(err, "failed to get active orders")
	}
	var result []string
	for _, val := range activeOrders {
		result = append(result, val.UUID)
	}

	return result, nil
}

func (p *Pg) CleanDrvOfferStatesDriverDB(orders []string, t time.Time) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	var drvOffers []models.OfferStatesDrv
	res, err := p.DriverDb.ModelContext(ctx, &drvOffers).
		Where("order_uuid NOT IN (?) AND created_at < ?", orders, t).Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete driver offer states")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanDrvOfferStatesCRMDB(orders []string, t time.Time) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	var drvOffers []models.OrderStateCRM
	res, err := p.CrmDb.ModelContext(ctx, &drvOffers).
		Where("order_uuid NOT IN (?) AND created_at < ?", orders, t).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete offer states CRM")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanDrvOfferStatesClientDB(orders []string, t time.Time) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	var ordStates []models.OrderStateCApp
	res, err := p.ClientDb.ModelContext(ctx, &ordStates).
		Where("order_uuid NOT IN (?) AND created_at < ?", orders, t).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete aster events")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanDrvStatesDriverDB() (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), replyTimeSeconds*time.Second)
	defer cancel()

	q := `DELETE FROM drv_states s1 
		  WHERE s1.id NOT IN (SELECT s2.id 
							  FROM drv_states s2 
							  WHERE s2.driver_uuid = s1.driver_uuid
							  ORDER BY created_at DESC
							  LIMIT 2)`
	res, err := p.DriverDb.ExecContext(ctx, q)
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete drv states")
	}

	return res.RowsAffected(), nil
}

func (p *Pg) CleanDrvStatesCRMDB() (int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), replyTimeSeconds*time.Second)
	defer cancel()

	q := `DELETE FROM crm_driver_states s1 
		  WHERE s1.id NOT IN (SELECT s2.id 
							  FROM crm_driver_states s2 
							  WHERE s2.driver_uuid = s1.driver_uuid
							  ORDER BY created_at DESC
							  LIMIT 2)`
	res, err := p.CrmDb.ExecContext(ctx, q)
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete drv states")
	}

	return res.RowsAffected(), nil
}

func (p *Pg) VacuumDriver() error {
	_, err := p.DriverDb.Exec("VACUUM FULL")
	if err != nil {
		return errors.Wrap(err, "failed to VACUUM")
	}
	return nil
}

func (p *Pg) VacuumCRM() error {
	_, err := p.CrmDb.Exec("VACUUM FULL")
	if err != nil {
		return errors.Wrap(err, "failed to VACUUM")
	}
	return nil
}

func (p *Pg) VacuumClient() error {
	_, err := p.ClientDb.Exec("VACUUM FULL")
	if err != nil {
		return errors.Wrap(err, "failed to VACUUM")
	}
	return nil
}

func (p *Pg) VacuumVoip() error {
	_, err := p.VoipDb.Exec("VACUUM FULL")
	if err != nil {
		return errors.Wrap(err, "failed to VACUUM")
	}
	return nil
}

func (p *Pg) CleanCRMEvents(t time.Duration) (int, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()
	//
	var crmEvents models.CrmEvents
	tm := time.Now().Add(-t)

	res, err := p.CrmDb.ModelContext(ctx, &crmEvents).
		Where("event_time < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete crm events")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) GetLocationsBefore(ctx context.Context, t time.Duration) ([]models.LocationData, error) {

	var locs []models.LocationData
	var lastLoc models.LocationData

	tm := time.Now().Add(-t * time.Minute)

	//находим последний элемент который идет сразу после нужной нам даты
	err := p.DriverDb.ModelContext(ctx, &lastLoc).
		Where("created_at > ?", tm).Order("created_at ASC").Limit(1).
		Select()
	if err != nil {
		return locs, errors.Wrap(err, "failed to find last record")
	}
	var counter int
	for {
		var partLocs []models.LocationData
		err = p.DriverDb.ModelContext(ctx, &partLocs).Where("id < ?", lastLoc.ID).
			Order("id ASC").Limit(queryBlock).Offset(counter * queryBlock).Select()
		if err != nil {
			return locs, errors.Wrap(err, "failed to get locations list")
		}
		counter++
		if len(partLocs) == 0 {
			return locs, nil
		}
		locs = append(locs, partLocs...)
	}
	return locs, nil
}

//DeleteLocations delete all driver locations with id less then corresponded
func (p *Pg) DeleteLocations(ctx context.Context, id int) (int, error) {

	var locs []models.LocationData
	result, err := p.DriverDb.ModelContext(ctx, &locs).Where("id < ?", id).Delete()

	if err != nil {
		return 0, err
	}

	return result.RowsAffected(), nil
}
