package repository

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/scheduler/models"
	"time"
)

func (p *Pg) CleanDriverOffers(ctx context.Context, t time.Duration) (int, error) {

	var drvOffers models.DriverOffers
	tm := time.Now().Add(-t)

	res, err := p.DriverDb.ModelContext(ctx, &drvOffers).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete driver offers")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanDriverOrders(ctx context.Context, t time.Duration) (int, error) {
	var drvOrders models.DriverOrders
	tm := time.Now().Add(-t)

	res, err := p.DriverDb.ModelContext(ctx, &drvOrders).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete driver orders")
	}
	return res.RowsAffected(), nil
}

func (p *Pg) CleanDriverAlerts(ctx context.Context, t time.Duration) (int, error) {
	var drvAlerts models.DriverAlerts
	tm := time.Now().Add(-t)
	res, err := p.DriverDb.ModelContext(ctx, &drvAlerts).
		Where("created_at < ?", tm).
		Delete()
	if err != nil {
		return 0, errors.Wrap(err, "failed to delete driver alerts")
	}
	return res.RowsAffected(), nil
}
