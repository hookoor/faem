package repository

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/scheduler/models"
	"time"
)

//const (
//	replyTimeSeconds = 600
//)

type SchedulerState struct {
	tableName struct{} `sql:"scheduler_del"`
	ID        int      `sql:"id"`
	Table     string   `sql:"tbl"`
	RecId     int      `sql:"id_rec"`
	CreatedAt int      `sql:"create_dt"`
}

func (p *Pg) GetLastLocations(t time.Duration) ([]models.LocationData, error) {
	//lag := time.Duration(replyTimeSeconds)
	//ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	//defer cancel()
	//
	//var (
	//	schState SchedulerState
	//	locData  []models.LocationData
	//)
	//
	//err := p.DriverDb.ModelContext(ctx, &schState).Where("tbl = 'drv_locations'").Select()
	//if err != nil {
	//	return locData, errors.Wrap(err, "failed to get last record")
	//}
	//
	//tm := time.Now().Add(-t)

	return []models.LocationData{}, nil
}

func (p *Pg) GetDrvLocation(t time.Duration) ([]models.LocationData, error) {
	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()

	var locData []models.LocationData
	tm := time.Now().Add(-t)
	//TODO Здесь можно сделат так что бы данные выбирались пачками
	err := p.DriverDb.ModelContext(ctx, &locData).
		Where("created_at < ?", tm).
		Limit(100).
		Select()
	if err != nil {
		return locData, errors.Wrap(err, "failed to delete aster events")
	}
	return locData, nil
}
