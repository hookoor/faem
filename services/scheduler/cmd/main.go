package main

import (
	"flag"
	"gitlab.com/faemproject/backend/faem/services/scheduler/cloudstorage"
	"log"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/store"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/pkg/web/middleware"
	"gitlab.com/faemproject/backend/faem/services/scheduler/config"
	"gitlab.com/faemproject/backend/faem/services/scheduler/cronjob"
	"gitlab.com/faemproject/backend/faem/services/scheduler/handler"
	"gitlab.com/faemproject/backend/faem/services/scheduler/repository"
	"gitlab.com/faemproject/backend/faem/services/scheduler/server"
)

const (
	defaultConfigPath     = "config/scheduler.toml"
	maxRequestsAllowed    = 1000
	serverShutdownTimeout = 30 * time.Second
	brokerShutdownTimeout = 30 * time.Second
)

func main() {
	// Parse flags
	configPath := flag.String("config", defaultConfigPath, "configuration file path")
	flag.Parse()

	cfg, err := config.Parse(*configPath)
	if err != nil {
		log.Fatalf("failed to parse the config file: %v", err)
	}

	if err := logs.SetLogLevel(cfg.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(cfg.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	logger := logs.Eloger

	// Connect to the VOIP db and remember to close it
	voipDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Databases.Voip.Host, cfg.Databases.Voip.Port),
		User:     cfg.Databases.Voip.User,
		Password: cfg.Databases.Voip.Password,
		Database: cfg.Databases.Voip.Db,
	})

	if err != nil {
		logger.Fatalf("failed to create VOIP db instance: %v", err)
	}
	defer voipDB.Close()

	// Connect to the DRIVER db and remember to close it
	driverDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Databases.Driver.Host, cfg.Databases.Driver.Port),
		User:     cfg.Databases.Driver.User,
		Password: cfg.Databases.Driver.Password,
		Database: cfg.Databases.Driver.Db,
	})

	if err != nil {
		logger.Fatalf("failed to create Driver db instance: %v", err)
	}
	defer driverDB.Close()

	// Connect to the CRM db and remember to close it
	crmDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
		User:     cfg.Databases.CRM.User,
		Password: cfg.Databases.CRM.Password,
		Database: cfg.Databases.CRM.Db,
	})

	if err != nil {
		logger.Fatalf("failed to create CRM db instance: %v", err)
	}
	defer crmDB.Close()

	// Connect to the CRM db and remember to close it
	clientDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Databases.Client.Host, cfg.Databases.Client.Port),
		User:     cfg.Databases.Client.User,
		Password: cfg.Databases.Client.Password,
		Database: cfg.Databases.Client.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create CRM db instance: %v", err)
	}
	defer clientDB.Close()

	// Cron jobs
	var cj cronjob.CronJobber

	//Cloud storage
	cs, err := cloudstorage.InitCloudStorage(cfg.CloudStorage.BucketName)
	if err != nil {
		logger.Fatalf("failed to create a CloudStorage client: %v", err)
	}
	defer cs.Close()

	// Create a service object
	hdlr := handler.Handler{
		DB: &repository.Pg{
			CrmDb:    crmDB,
			DriverDb: driverDB,
			VoipDb:   voipDB,
			ClientDb: clientDB,
		},
		//Pub:  &pub,
		Cron: &cj,
		CS:   &cs,
	}

	//---------------------------------------------------------
	//hdlr.DrvLocationCleaner()
	//---------------------------------------------------------

	//Init CRON jobs
	err = hdlr.InitCronJobs()
	if err != nil {
		logger.Fatalf("failed to init CRON jobs: %v", err)
	}

	//// Create a subscriber
	//sub := subscriber.Subscriber{
	//	Rabbit:  rmq,
	//	Encoder: &rabbit.JsonEncoder{},
	//	Handler: &hdlr,
	//}
	//
	//if err = sub.Init(); err != nil {
	//	logger.Fatalf("failed to start the subscriber: %v", err)
	//}
	//defer sub.Wait(brokerShutdownTimeout)

	// Create a rest gateway and handle http requests
	router := web.NewRouter(
		loggerOption(logger),
		prometheusmetric,
		throttler,
	)
	rest := server.Rest{
		Router:  router,
		Handler: &hdlr,
	}
	rest.Route()

	// Start an http server and remember to shut it down
	go web.Start(router, cfg.Application.Port)
	defer web.Stop(router, serverShutdownTimeout)

	// Wait for program exit
	<-os.NotifyAboutExit()
}

func loggerOption(logger *logrus.Logger) web.Option {
	return func(e *echo.Echo) {
		e.Logger = &middleware.Logger{Logger: logger} // replace the original echo.Logger with the logrus one
		// Log the requests
		e.Use(middleware.LoggerWithSkipper(
			func(c echo.Context) bool {
				return strings.Contains(c.Request().RequestURI, "/api/v2/locations")
			}),
		)
	}
}

func throttler(e *echo.Echo) {
	e.Use(middleware.Throttle(maxRequestsAllowed))
}

func prometheusmetric(e *echo.Echo) {
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
}
