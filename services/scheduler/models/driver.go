package models

import (
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// таблица оффер статусов
type OfferStatesDrv struct {
	tableName struct{} `sql:"drv_offer_states"`
	structures.OfferStates
}

type LocationData struct {
	tableName struct{} `sql:"drv_locations"`
	ID        int      `sql:",pk"`
	structures.LocationData
	//DriverID   int       `json:"driver_id"`
	//DriverUUID string    `json:"driver_uuid"`
	//Latitude   float64   `json:"lat"`
	//Longitude  float64   `json:"lng"`
	//Satelites  int       `json:"sats"`
	//Timestamp  int64     `json:"time"`
	//CreatedAt  time.Time `json:"created_at" sql:"default:now()"`
}

func (ld *LocationData) StringArray() []string {
	return []string{
		fmt.Sprintf("%v", ld.ID),
		fmt.Sprintf("%v", ld.DriverID),
		fmt.Sprintf("%v", ld.DriverUUID),
		fmt.Sprintf("%v", ld.Latitude),
		fmt.Sprintf("%v", ld.Longitude),
		fmt.Sprintf("%v", ld.Satelites),
		fmt.Sprintf("%v", ld.Timestamp),
		fmt.Sprintf("%v", ld.CreatedAt),
	}
}
