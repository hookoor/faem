package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type OrderStateCApp struct {
	tableName struct{} `sql:"client_orderstates"`
	structures.OfferStates
}
