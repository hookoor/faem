package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/models/voip"
	"time"
)

type AsterEvents struct {
	tableName struct{} `sql:"aster_events"`
	voip.AsterEvent
}

type AsterOrders struct {
	tableName  struct{}  `sql:"crm_orders"`
	Created_at time.Time `json:"created_at"`
}

type AsterAutoCall struct {
	tableName  struct{}  `sql:"autocalls"`
	Created_at time.Time `json:"created_at"`
}
