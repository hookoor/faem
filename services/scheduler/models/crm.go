package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	driverModel "gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

type OrderCRM struct {
	tableName struct{} `sql:"crm_orders"`
	structures.Order
	StateTransferTime time.Time `json:"state_transfer_time"`
}

type OrderStateCRM struct {
	tableName struct{} `sql:"crm_orderstates"`
	structures.OfferStates
}

type CrmEvents struct {
	tableName struct{} `sql:"crm_events"  pg:",discard_unknown_columns"`
	structures.EventItem
}

type DriverOffers struct {
	tableName struct{} `sql:"drv_offers" pg:",discard_unknown_columns"`
	driverModel.OfferDrv
}

type DriverOrders struct {
	tableName struct{} `sql:"drv_orders" pg:",discard_unknown_columns"`
	driverModel.OrderDrv
}

type DriverAlerts struct {
	tableName struct{} `sql:"drv_alerts" pg:",discard_unknown_columns"`
	driverModel.AlertsDRV
}
