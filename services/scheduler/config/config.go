package config

import (
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "scheduler"
)

type Application struct {
	Env       string
	Addr      string
	Port      string
	Secret    string
	LogLevel  string
	LogFormat string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

func (a *Application) validate() error {
	//if a.Addr == "" {
	//	return errors.New("empty address provided for an http server to start on")
	//}
	//if a.Secret == "" {
	//	return errors.New("empty secret provided")
	//}
	return nil
}

type Databases struct {
	Voip   Database
	Driver Database
	CRM    Database
	Client Database
}

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

func (dd *Databases) validate() error {
	return multierr.Combine(
		errors.Wrap(dd.Voip.validate(), "voip database"),
		errors.Wrap(dd.Driver.validate(), "driver database"),
		errors.Wrap(dd.CRM.validate(), "crm database"),
		errors.Wrap(dd.Client.validate(), "crm database"),
	)
}

func (d *Database) validate() error {
	if d.Host == "" {
		return errors.New("empty db host provided")
	}
	if d.Port == 0 {
		return errors.New("empty db port provided")
	}
	if d.User == "" {
		return errors.New("empty db user provided")
	}
	if d.Password == "" {
		return errors.New("empty db password provided")
	}
	if d.Db == "" {
		return errors.New("empty db name provided")
	}
	return nil
}

type Broker struct {
	UserURL         string
	UserCredits     string
	ExchangePrefix  string
	ExchangePostfix string
}

type CloudStorage struct {
	BucketName string
}

func (b *Broker) validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

type Config struct {
	Application  Application
	Databases    Databases
	Broker       Broker
	CloudStorage CloudStorage
}

func (c *Config) validate() error {
	return c.Databases.validate()
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

// TODO: set the default values here
func setDefaults() {
	viper.SetDefault("Application.Env", "production")
	viper.SetDefault("Application.LogLevel", "trace")
	viper.SetDefault("Application.LogFormat", "text")
	viper.SetDefault("Application.Port", "8080")

	viper.SetDefault("Databases.CRM.Host", "")
	viper.SetDefault("Databases.CRM.User", "")
	viper.SetDefault("Databases.CRM.Password", "")
	viper.SetDefault("Databases.CRM.Db", "")
	viper.SetDefault("Databases.CRM.Port", 0)

	viper.SetDefault("Databases.Voip.Host", "")
	viper.SetDefault("Databases.Voip.User", "")
	viper.SetDefault("Databases.Voip.Password", "")
	viper.SetDefault("Databases.Voip.Db", "")
	viper.SetDefault("Databases.Voip.Port", 0)

	viper.SetDefault("Databases.Driver.Host", "")
	viper.SetDefault("Databases.Driver.User", "")
	viper.SetDefault("Databases.Driver.Password", "")
	viper.SetDefault("Databases.Driver.Db", "")
	viper.SetDefault("Databases.Driver.Port", 0)

	viper.SetDefault("Databases.Client.Host", "")
	viper.SetDefault("Databases.Client.User", "")
	viper.SetDefault("Databases.Client.Password", "")
	viper.SetDefault("Databases.Client.Db", "")
	viper.SetDefault("Databases.Client.Port", 0)

	viper.SetDefault("CloudStorage.BucketName", "faem-staging-data-backup")

}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}
