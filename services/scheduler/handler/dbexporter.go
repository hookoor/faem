package handler

import (
	"encoding/csv"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/scheduler/models"
	"os"
	"time"
)

const (
	//за какой период оставлять события астера
	exportDrvLocationData = 2880
)

type ExporterJobs interface {
	//Запрос на получение водительских координат за период
	GetDrvLocation(t time.Duration) ([]models.LocationData, error)
	//DeleteDrvLocations(t time.Duration) (int, error)
}

func (h *Handler) dbExporter() {
	//
	//h.exportDrvLocations()
}

func (h *Handler) exportDrvLocations() {
	dur := time.Duration(exportDrvLocationData) * time.Minute

	log := logs.Eloger.WithFields(logrus.Fields{
		"Event": "Export Driver Locations",
	})

	//получаем данные координат
	startTime := time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Action":     "Exporting location data from SQL",
	}).Info("Start cleaner...")
	drvLocations, err := h.DB.GetDrvLocation(dur)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Action":      "Exporting location data from SQL",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows founded": len(drvLocations),
		"time spend":   time.Now().Sub(startTime),
		"Action":       "Exporting location data from SQL",
	}).Info("Success!")

	//пишем в CSV
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Action":     "Write to CSV",
	}).Info("Start...")
	err = writeToCSV(drvLocations)
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"time spend": time.Now().Sub(startTime),
		"Action":     "Write to CSV",
	}).Info("Finish...")

	return
}

func writeToCSV(payload []models.LocationData) error {
	var res [][]string
	for _, val := range payload {
		res = append(res, val.StringArray())
	}
	w := csv.NewWriter(os.Stdout)
	w.WriteAll(res) // calls Flush internally
	w.Flush()
	return nil
}
