package handler

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"time"
)

//DRIVER - drv_offers + drv_orders +  drv_alerts
func (h *Handler) CleanDriversDB() {
	dur := time.Duration(driverCleanerMin) * time.Minute

	log := logs.Eloger.WithFields(logrus.Fields{
		"Database":           "DRIVER",
		"deleted last (min)": dur,
		"event":              "DRIVER - drv_offers + drv_orders + drv_alerts events",
	})

	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()

	startTime := time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
	}).Info("Start cleaner...")
	count, err := h.DB.CleanDriverOffers(ctx, dur)
	if err != nil {
		log.WithField("table", "drv_offers").Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"table":        "drv_offers",
		"time spend":   time.Now().Sub(startTime),
	}).Info("Success!")

	startTime = time.Now()
	count, err = h.DB.CleanDriverOrders(ctx, dur)
	if err != nil {
		log.WithField("table", "drv_offers").Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"table":        "drv_orders",
		"time spend":   time.Now().Sub(startTime),
	}).Info("Success!")

	startTime = time.Now()
	count, err = h.DB.CleanDriverAlerts(ctx, dur)
	if err != nil {
		log.WithField("table", "drv_alerts").Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"table":        "drv_alerts",
		"time spend":   time.Now().Sub(startTime),
	}).Info("Success!")

	return
}
