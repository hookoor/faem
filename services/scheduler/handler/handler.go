package handler

import (
	"context"
	"gitlab.com/faemproject/backend/faem/services/scheduler/cronjob"
	//"gitlab.com/faemproject/backend/faem/services/scheduler/models"
)

type Repository interface {
	CleanerJobs
	ExporterJobs
}

type Publisher interface {
	//GreeterPublisher
}

type Handler struct {
	DB   Repository
	Pub  Publisher
	Cron *cronjob.CronJobber
	CS   CloudStorage
}

type CloudStorage interface {
	WriteToStorage(ctx context.Context, byteData *[]byte, filename string) error
	Close() error
	//SaveEventToStorage(ctx context.Context, event []models.LocationData, t string) error
	//SetStopFlag()
	//SetStartFlag()
	//GetStopFlag() bool
}
