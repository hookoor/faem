package handler

import (
	"gitlab.com/faemproject/backend/faem/services/scheduler/cronjob"
)

var (
	//список задач для периодического выполнения
	//dbCleaner - чистка БД
	dbCleaner cronjob.CronJobber

	//dbExport - экспорт даных
	dbExport cronjob.CronJobber
)

func (h *Handler) InitCronJobs() error {
	initJobVars()
	err := dbCleaner.ScheduleIt(func() { h.dbCleaner() })
	if err != nil {
		return err
	}
	err = dbExport.ScheduleIt(func() { h.dbExporter() })
	if err != nil {
		return err
	}
	return nil
}

func initJobVars() {
	dbCleaner.Name = "DatabaseCleaner"
	dbCleaner.Desc = "Очистка не нужны для хранения данных"
	dbCleaner.CronExpr = "0 20 4 * *"

	dbExport.Name = "DataExporter"
	dbExport.Desc = "Экспорт данных для архивации"
	dbExport.CronExpr = "0 20 3 * *"
}
