package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/scheduler/models"
)

const (
	replyTimeSeconds = 1200

	//за какой период оставлять события астера
	delAsterEventMin = 4320
	//... водительские офферы
	delDrvOffersMin = 4320
	//... водительские офферы
	delCrmEventsMin = 30240
	//... водительские координат
	lastLocationMin = 4320
	//DRIVER - drv_offers + drv_orders +  drv_alerts
	driverCleanerMin = 4320
)

type CleanerJobs interface {
	// Чистка ивентов астера, автозвонков и оредеров
	CleanAsterCRMOrders(t time.Duration) (int, error)
	CleanAsterAutocalls(t time.Duration) (int, error)
	CleanAsterEvents(t time.Duration) (int, error)

	//Получаем список активных заказов что бы случайно их не удалилть
	GetActiveOrders(t time.Time) ([]string, error)

	// Чистка оффер стейтов из разных баз
	CleanDrvOfferStatesDriverDB(orders []string, t time.Time) (int, error)
	CleanDrvOfferStatesCRMDB(orders []string, t time.Time) (int, error)
	CleanDrvOfferStatesClientDB(orders []string, t time.Time) (int, error)

	// Чистка стейтов водителей из разных баз
	CleanDrvStatesDriverDB() (int, error)
	CleanDrvStatesCRMDB() (int, error)

	// Чистка CRM ивентов
	CleanCRMEvents(t time.Duration) (int, error)
	CleanDriverOffers(ctx context.Context, t time.Duration) (int, error)
	CleanDriverOrders(ctx context.Context, t time.Duration) (int, error)
	CleanDriverAlerts(ctx context.Context, t time.Duration) (int, error)

	//Возвращает список координат до указанной даты
	GetLocationsBefore(ctx context.Context, d time.Duration) ([]models.LocationData, error)

	//Удаление списока координат
	DeleteLocations(ctx context.Context, id int) (int, error)

	//Вакумирование
	VacuumDriver() error
	VacuumCRM() error
	VacuumClient() error
	VacuumVoip() error
}

func (h *Handler) dbCleaner() {
	// Чистим события астериска
	h.asterEventsCleaner()

	// Чистим статусы офферов
	h.drvOffersCleaner()

	// Чистим ивенты CRM
	h.crmEventsCleaner()

	// Чистим статусы водителей (driver+crm)
	h.drvStatesCleaner()

	// Чистим DRIVER - drv_offers + drv_orders +  drv_alerts
	h.CleanDriversDB()

	// Чистим координаты водителей
	h.DrvLocationCleaner()

	//проводим полный вакум
	//убираем вакумирование данных ибо нужны специальные права для этого
	//h.vacuumTables()
}

func (h *Handler) asterEventsCleaner() {
	dur := time.Duration(delAsterEventMin) * time.Minute

	log := logs.Eloger.WithFields(logrus.Fields{
		"Database":           "VOIP",
		"deleted last (min)": dur,
	})

	// Чистим ивенты астера
	startTime := time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Event":      "Deleting Aster Events",
	}).Info("Start cleaner...")
	count, err := h.DB.CleanAsterEvents(dur)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"event":       "cleaning aster events",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"time spend":   time.Now().Sub(startTime),
		"Event":        "Deleting Aster Events",
	}).Info("Success!")

	// Чистим заказы из телефонии
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Event":      "Deleting VOIP crm orders",
	}).Info("Start cleaner...")

	count, err = h.DB.CleanAsterCRMOrders(dur)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Event":       "Deleting VOIP crm orders",
		}).Error(err)
		return
	}

	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"time spend":   time.Now().Sub(startTime),
		"Event":        "Deleting VOIP crm orders",
	}).Info("Success!")

	// Чистим автоколы
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Event":      "Deleting VOIP autocalls",
	}).Info("Start cleaner...")

	count, err = h.DB.CleanAsterAutocalls(dur)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Event":       "Deleting VOIP autocalls",
		}).Error(err)
		return
	}

	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"time spend":   time.Now().Sub(startTime),
		"Event":        "Deleting VOIP autocalls",
	}).Info("Success!")
	return
}

func (h *Handler) drvOffersCleaner() {
	dur := time.Duration(delDrvOffersMin) * time.Minute
	cleanStartTime := time.Now()
	startTime := time.Now()
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":              "Driver Events Cleaner",
		"start time":         startTime,
		"deleted last (min)": delDrvOffersMin,
	})
	log.Info("Getting unactive orders list")

	// получаем активные офферы
	lastDate := time.Now().Add(-dur)
	unActiveOrders, err := h.DB.GetActiveOrders(lastDate)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"reason":      "Error getting unactive orders",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"Orders count": len(unActiveOrders),
		"time spend":   time.Now().Sub(startTime),
	}).Info("Active orders request")

	// очищаем
	startTime = time.Now()
	c, err := h.DB.CleanDrvOfferStatesDriverDB(unActiveOrders, lastDate)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"reason":      "Error deleting offers from Driver DB",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": c,
		"time spend":   time.Now().Sub(startTime),
		"database":     "Driver",
	}).Info("Success!")

	startTime = time.Now()
	c, err = h.DB.CleanDrvOfferStatesCRMDB(unActiveOrders, lastDate)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"reason":      "Error deleting offers from CRM DB",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": c,
		"time spend":   time.Now().Sub(startTime),
		"database":     "CRM",
	}).Info("Success!")

	startTime = time.Now()
	c, err = h.DB.CleanDrvOfferStatesClientDB(unActiveOrders, lastDate)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"reason":      "Error deleting offers from Client DB",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": c,
		"time spend":   time.Now().Sub(startTime),
		"database":     "Client",
	}).Info("Success!")

	log.WithFields(logrus.Fields{
		"time spend": time.Now().Sub(cleanStartTime),
		"counter":    delAsterEventMin,
	}).Info("All cleans are success!")
}

func (h *Handler) drvStatesCleaner() {
	cleanStartTime := time.Now()
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "Driver states cleaner",
		"start time": cleanStartTime,
	})
	log.Info("cleaning driver states")

	startTime := time.Now()
	c, err := h.DB.CleanDrvStatesDriverDB()
	if err != nil {
		log.WithField("reason", "Error deleting states from Driver DB").Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": c,
		"time spend":   time.Since(startTime),
		"database":     "Driver",
	}).Info("driver states from DriverDB were cleaned succesfully!")

	startTime = time.Now()
	c, err = h.DB.CleanDrvStatesCRMDB()
	if err != nil {
		log.WithField("reason", "Error deleting states from CRM DB").Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": c,
		"time spend":   time.Since(startTime),
		"database":     "CRM",
	}).Info("driver states from CRMDB were cleaned succesfully!")

	log.WithField("time spend", time.Since(cleanStartTime)).Info("All cleans succeeded!")
}

func (h *Handler) vacuumTables() {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "Vacuuming tables",
	})

	//Вакумирование драйверского
	startTime := time.Now()
	log.WithFields(logrus.Fields{
		"Table":      "Driver",
		"Start time": startTime,
	}).Info("Start VACUUMING")

	err := h.DB.VacuumDriver()
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Table":       "Driver",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"Table":      "Driver",
		"time spend": time.Now().Sub(startTime),
	}).Info("VACUUMING finished")

	//Вакумирование CRM-ки
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"Table":      "CRM",
		"Start time": startTime,
	}).Info("Start VACUUMING")

	err = h.DB.VacuumCRM()
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Table":       "CRM",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"Table":      "CRM",
		"time spend": time.Now().Sub(startTime),
	}).Info("VACUUMING finished")

	//Вакумирование Clint-ского
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"Table":      "Client",
		"Start time": startTime,
	}).Info("Start VACUUMING")

	err = h.DB.VacuumClient()
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Table":       "Client",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"Table":      "Client",
		"time spend": time.Now().Sub(startTime),
	}).Info("VACUUMING finished")

	//Вакумирование VOIP-ского
	startTime = time.Now()
	log.WithFields(logrus.Fields{
		"Table":      "VOIP",
		"Start time": startTime,
	}).Info("Start VACUUMING")

	err = h.DB.VacuumVoip()
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
			"Table":       "VOIP",
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"Table":      "VOIP",
		"time spend": time.Now().Sub(startTime),
	}).Info("VACUUMING finished")
}

//clean crm events
func (h *Handler) crmEventsCleaner() {
	dur := time.Duration(delCrmEventsMin) * time.Minute

	log := logs.Eloger.WithFields(logrus.Fields{
		"Database":           "CRM",
		"deleted last (min)": dur,
		"event":              "clean CRM events",
	})

	// Чистим ивенты астера
	startTime := time.Now()
	log.WithFields(logrus.Fields{
		"start time": startTime,
		"Event":      "Deleting CRM Events",
	}).Info("Start cleaner...")
	//count, err := h.DB.CleanAsterEvents(dur)
	count, err := h.DB.CleanCRMEvents(dur)
	if err != nil {
		log.WithFields(logrus.Fields{
			"finish time": time.Now(),
		}).Error(err)
		return
	}
	log.WithFields(logrus.Fields{
		"rows deleted": count,
		"time spend":   time.Now().Sub(startTime),
	}).Info("Success!")

	return
}

// DrvLocationCleaner сохранение и последующая чистка координат водителей
func (h *Handler) DrvLocationCleaner() {
	log := logs.Eloger.WithFields(logrus.Fields{
		"Database": "Driver",
		"event":    "clean driver locations",
	})

	lag := time.Duration(replyTimeSeconds)
	ctx, cancel := context.WithTimeout(context.Background(), lag*time.Second)
	defer cancel()

	// Получаем координаты для сохранения
	data, err := h.DB.GetLocationsBefore(ctx, lastLocationMin)
	if err != nil {
		log.Error(err)
	}

	// группировка координат по дням и затем по водителям
	days := make(map[string]map[string][]models.LocationData) // day - (driverUUID - []coords)
	for _, v := range data {
		y, m, d := v.CreatedAt.Date()
		date := fmt.Sprintf("%d-%02d-%02d", y, m, d)

		drivers, ok := days[date]
		if ok {
			coords := drivers[v.DriverUUID]
			coords = append(coords, v)
			days[date][v.DriverUUID] = coords
		} else {
			driverCoords := make(map[string][]models.LocationData)
			driverCoords[v.DriverUUID] = []models.LocationData{v}
			days[date] = driverCoords
		}
	}

	// сохранение координат в storage
	for _, drivers := range days {
		for _, coords := range drivers {
			bytes, err := json.Marshal(coords)
			if err != nil {
				log.WithFields(logrus.Fields{
					"event": "convert driver coordinates to byte[]",
				}).Error(err)
			}

			err = h.CS.WriteToStorage(ctx, &bytes, getDriverCoordsFilename(coords[0]))
			if err != nil {
				log.WithFields(logrus.Fields{
					"event": "save driver coordinates in storage",
				}).Error(err)
				return
			}
		}
	}

	//удаляем только при успешной записи в storage, чтобы не потерять данные
	deletedCount, err := h.DB.DeleteLocations(ctx, data[len(data)-1].ID)
	if err != nil {
		log.WithFields(logrus.Fields{
			"event": "deleting driver coordinates",
		}).Error(err)
		return
	}

	log.WithFields(logrus.Fields{
		"event": "deleted " + fmt.Sprint(deletedCount) + " driver coordinates",
	}).Info()
}

func getDriverCoordsFilename(locData models.LocationData) string {
	y, m, d := locData.CreatedAt.Date()
	return fmt.Sprintf("locations/%d/%02d/%02d/%s.json", y, m, d, locData.DriverUUID)
}
