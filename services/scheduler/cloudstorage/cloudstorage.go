package cloudstorage

import (
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"go.uber.org/multierr"
	"hash/crc32"
	"math/rand"
	"strings"
	"time"
)

const (
	timeOutSec = 900
)

type CloudStorage struct {
	Client *storage.Client
	Bucket *storage.BucketHandle
}

func InitCloudStorage(bucketName string) (CloudStorage, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	bucket := client.Bucket(bucketName)
	if err != nil {
		return CloudStorage{}, errors.Wrap(err, "failed to init GCP storage client")
	}
	cs := CloudStorage{
		Client: client,
		Bucket: bucket,
	}

	return cs, nil
}

func (cs *CloudStorage) Close() error {
	return multierr.Combine(
		errors.Wrap(cs.Client.Close(), "error while closing cloud client"),
	)
}

func (cs *CloudStorage) WriteToStorage(ctx context.Context, byteData *[]byte, filename string) error {
	dur := time.Second * time.Duration(timeOutSec)
	tctx, cancel := context.WithTimeout(ctx, dur)
	defer cancel()
	_, err := cs.Bucket.Object(filename).Attrs(ctx)

	//а вдруг такой файл уже есть - создаим случайный
	if err != storage.ErrObjectNotExist {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		f := strings.Split(filename, ".")
		if len(f) > 0 {
			filename = fmt.Sprintf("%s_%d.%s", f[0], r1.Intn(100), f[1])
		}
		//return nil
	}

	wr := cs.Bucket.Object(filename).NewWriter(tctx)

	wr.CRC32C = crc32.Checksum(*byteData, crc32.MakeTable(crc32.Castagnoli))
	wr.SendCRC32C = true

	if _, err := wr.Write(*byteData); err != nil {
		return errors.Wrap(err, "failed to write data to storage")
	}

	if err := wr.Close(); err != nil {
		return errors.Wrap(err, "error while closing writer to storage")
	}

	return nil
}
