package cronjob

import (
	"github.com/pkg/errors"
	"github.com/robfig/cron"
	"time"
)

const (
	curLocation = "Europe/Moscow"
)

// в рамках одного типа заданий, например чистка событий телефонии, будем создавать
// один планировщик
type CronJobber struct {
	Cron     *cron.Cron
	Name     string
	Desc     string
	CronExpr string
	Cmd      func()
}

func (cj *CronJobber) ScheduleIt(cmd func()) error {
	location, err := time.LoadLocation(curLocation)
	if err != nil {
		return errors.Wrap(err, "Can't get current location data")
	}
	cj.Cron = cron.NewWithLocation(location)
	err = cj.Cron.AddFunc(cj.CronExpr, cmd)
	if err != nil {
		return errors.Wrap(err, "Can't add cron job function")
	}
	cj.Cron.Start()
	return nil
}
