package fcmsender

import (
	fcm "github.com/NaySoftware/go-fcm"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

var (
	fcmclient *fcm.FcmClient
)

type FCMChannels string

const (
	defaultChannel = "fcm_default_channel_new"
	chatChannel    = "fcm_message_chat_channel_new"
)

//Init godoc
func Init(fclient *fcm.FcmClient) {
	fcmclient = fclient
}
func filterForUniqueness(tokens []string) []string {
	var result []string
	for _, token := range tokens {
		check := false
		for _, resToken := range result {
			if token == resToken {
				check = true
			}
			break
		}
		if !check {
			result = append(result, token)
		}
	}
	return result
}

// SendMessageToDrivers отправляет водителю с переданным driverToken сообщение с переданным payload
func SendMessageToDrivers(payload interface{}, driverTokens []string, tag string, lifetime int, titleAndBody ...string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "Send JSON to FCM",
		"tag":          tag,
		"driverTokens": driverTokens,
	})

	fcmclient.Message.Notification.Sound = ""
	fcmclient.Message.Notification.Title = ""
	fcmclient.Message.Priority = "high"
	fcmclient.Message.Notification.Body = ""
	fcmclient.Message.Notification.AndroidChannelID = defaultChannel

	switch tag {
	case fcmpkg.ChatMessageTag:
		fcmclient.Message.Notification.AndroidChannelID = chatChannel
	case constants.FcmTagNewOrderToDriver:
		fcmclient.Message.Notification.AndroidChannelID = ""
		tag = constants.FcmTagInitData
	}
	fcmstr := fcmpkg.FcmStruct{
		Tag:     tag,
		Payload: payload,
	}
	if len(titleAndBody) != 0 {
		fcmclient.Message.Notification.Sound = "default"
		fcmclient.Message.Notification.Title = titleAndBody[0]
	}
	if len(titleAndBody) > 1 {
		fcmclient.Message.Notification.Body = titleAndBody[1]
	}

	if len(driverTokens) == 0 {
		log.Error(errpath.Errorf("empty driverTokens"))
		return
	}
	driverTokens = filterForUniqueness(driverTokens)
	fcmclient.NewFcmRegIdsMsg(driverTokens, fcmstr)
	fcmclient.SetTimeToLive(lifetime)
	status, err := fcmclient.Send()
	if err != nil {
		log.WithField("reason", "fcmclient.Send").Error(errpath.Err(err))
		return
	}
	if status.Fail != 0 {
		log.Error(errpath.Errorf("error sending data to driver"))
		status.PrintResults()
		return
	}
	log.Info("Sended to FCM")

}

// SendMessageToDriverWithCentrifugo -
func SendMessageToDriverWithCentrifugo(driveruuid string, payload interface{}, driverTokens string, tag string, lifetime int, titleAndBody ...string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":         "SendMessageToDriverWithCentrifugo",
		"driver_uuid":   driveruuid,
		"driver_tokens": driverTokens,
		"tag":           tag,
	})

	if tag == constants.FcmTagNewOrderToDriver {
		tag = constants.FcmTagInitData
	}

	skiperr := func() error {
		centrifugoChannel := "driver/" + driveruuid
		centrifugoClient := driveruuid

		connExist, err := rabsender.CentrifugoPresence(centrifugoChannel, centrifugoClient)
		if err != nil {
			err = errpath.Err(err)
			log.Error(err)
			return err
		}

		var title, message string
		if len(titleAndBody) > 0 {
			title = titleAndBody[0]
		}
		if len(titleAndBody) > 1 {
			message = titleAndBody[1]
		}

		if connExist {
			pl := structures.NotificationDesirablePayload{
				Tag:     tag,
				Title:   title,
				Message: message,
				Payload: payload,
			}
			data, err := structures.NewNotification(centrifugoChannel, pl)
			if err != nil {
				err = errpath.Err(err)
				log.Error(err)
				return err
			}

			if err := rabsender.GlobalPublisher.SendByCentrifugo(data); err != nil {
				err = errpath.Err(err)
				log.Error(err)
				return err
			}

			log.Info("Send by centrifugo")

			return nil
		}

		return errpath.Errorf("Centrifugo connect not exist")
	}()

	if skiperr != nil {
		log.Warn(errpath.Err(skiperr, "sending by centrifugo failed"))

		SendMessageToDrivers(payload, []string{driverTokens}, tag, lifetime, titleAndBody...)
	}

}
