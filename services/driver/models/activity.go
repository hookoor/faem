package models

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

// TODO: move to the remote config: db/consul/redis/etc.

const (
	eventOrderFinished          = "order finished"
	eventOrderCancelled         = "order cancelled"
	eventTooManyOrdersCancelled = "too many orders cancelled"
	eventOfferRejected          = "offer rejected"
	eventTooManyOffersRejected  = "too many offers rejected"
	activityConfigEndpoint      = "/config/activity"
)

type ActivityConfig struct {
	structures.ActivityConfig
	mx sync.RWMutex
}

var currentActivityConfig = ActivityConfig{ActivityConfig: structures.ActivityConfig{}}

func GetCurrentActivityConfig() structures.ActivityConfig {
	currentActivityConfig.mx.RLock()
	defer currentActivityConfig.mx.RUnlock()
	return currentActivityConfig.ActivityConfig
}

func SetCurrentActivityConfig(config structures.ActivityConfig) {
	currentActivityConfig.mx.Lock()
	currentActivityConfig.ActivityConfig = config
	currentActivityConfig.mx.Unlock()
}

func UpdateActivityConfig(context.Context) error {
	cfg, err := GetActivityConfigFromCRM()
	if err != nil {
		return errors.Wrap(err, "failed to update activity config")
	}

	SetCurrentActivityConfig(cfg)
	return nil
}

func GetActivityConfigFromCRM() (structures.ActivityConfig, error) {
	var result structures.ActivityConfig
	if err := request(http.MethodGet, config.St.CRM.Host+activityConfigEndpoint, nil, &result); err != nil {
		return structures.ActivityConfig{}, errors.Wrap(err, "failed to get activity config from CRM")
	}
	return result, nil
}

func GetInitialDriverActivity() (int, error) {
	return GetCurrentActivityConfig().NewDriver.InitialActivity, nil
}

func GetDriverActivityForFinishedOrder() (int, error) {
	return GetCurrentActivityConfig().OrderFinished.DefaultReward, nil
}

func GetDriverFarOrderDistance() (int, error) {
	return GetCurrentActivityConfig().OrderFinished.FarOrderDistance, nil
}

func GetDriverActivityForFinishedFarOrder() (int, error) {
	return GetCurrentActivityConfig().OrderFinished.FarOrderReward, nil
}

func CanDriverGetPenalty(driver *DriversApps) bool {
	freePeriod := time.Duration(GetCurrentActivityConfig().NewDriver.PenaltyFreePeriod) * time.Hour
	if driver != nil && time.Now().Before(driver.CreatedAt.Add(freePeriod)) {
		return false
	}

	return true
}

func GetDriverPenaltyForRejectedOffer(driver *DriversApps, distance float64) (int, error) {
	if !CanDriverGetPenalty(driver) {
		return 0, nil
	}

	// Calc penalty corresponding to distance
	for _, dp := range GetCurrentActivityConfig().OfferRejected.DistancePenalties {
		if dp.Distance > distance {
			return dp.Penalty, nil
		}
	}
	return GetCurrentActivityConfig().OfferRejected.DefaultPenalty, nil
}

func GetDriverRejectedOffersLimit() (int, error) {
	return GetCurrentActivityConfig().OfferRejected.MultiRejectLimit, nil
}

func GetDriverPeriodForMultiRejectedOffers() (time.Duration, error) {
	period := time.Duration(GetCurrentActivityConfig().OfferRejected.MultiRejectPeriod)

	return period * time.Hour, nil
}

func GetDriverPenaltyForMultiRejectedOffers(driver *DriversApps, distance float64) (int, error) {
	if !CanDriverGetPenalty(driver) {
		return 0, nil
	}

	// Calc penalty corresponding to distance
	penalty := GetCurrentActivityConfig().OfferRejected.DefaultPenalty
	for _, dp := range GetCurrentActivityConfig().OfferRejected.DistancePenalties {
		if dp.Distance > distance {
			penalty = dp.Penalty
			break
		}
	}
	return GetCurrentActivityConfig().OfferRejected.MultiRejectMalus + penalty, nil
}

func GetDriverPenaltyForCancelledOrder(driver *DriversApps) (int, error) {
	if !CanDriverGetPenalty(driver) {
		return 0, nil
	}

	return GetCurrentActivityConfig().OrderCancelled.DefaultPenalty, nil
}

func GetDriverCancelledOrdersLimit() (int, error) {
	return GetCurrentActivityConfig().OrderCancelled.MultiCancelLimit, nil
}

func GetDriverPeriodForMultiCancelledOrders() (time.Duration, error) {
	period := time.Duration(GetCurrentActivityConfig().OrderCancelled.MultiCancelPeriod)

	return period * time.Hour, nil
}

func GetDriverPenaltyForMultiCancelledOrders(driver *DriversApps) (int, error) {
	if !CanDriverGetPenalty(driver) {
		return 0, nil
	}

	return GetCurrentActivityConfig().OrderCancelled.MultiCancelPenalty, nil
}

func GetRushHours() (structures.RushHours, error) {
	return GetCurrentActivityConfig().RushHours, nil
}

func GetDriverActivityBonusForRushHours() (int, error) {
	return GetCurrentActivityConfig().OrderFinished.RushHoursBonus, nil
}

type DriverActivityHistoryItem struct {
	tableName struct{} `sql:"drv_activity_history" pg:",discard_unknown_columns"`

	ID             string    `json:"id"`
	DriverUUID     string    `json:"driver_uuid"`
	OfferUUID      string    `json:"offer_uuid"`
	Event          string    `json:"event"`
	ActivityChange int       `json:"activity_change"`
	CreatedAt      time.Time `json:"-"`
	OrderUUID      string    `json:"-" sql:"-"`
}

type PossibleActivityArgs struct {
	OfferUUID        string
	DriverUUID       string
	StartState       time.Time
	IsFreeOrder      bool
	DistanceToClient float64
}

func GetPossibleActivity(args PossibleActivityArgs) (
	accept int, reject int, err error,
) {
	accept, err = getActivityForAccepting(args.OfferUUID, args.StartState, args.DistanceToClient)
	if err != nil {
		err = errors.Wrap(err, "failed to calculate activity for accepting the order")
		return
	}
	reject, _, err = getPenaltyForRejecting(args.DriverUUID, args.OfferUUID, args.IsFreeOrder, true, args.DistanceToClient)
	if err != nil {
		err = errors.Wrap(err, "failed to calculate activity for rejecting the order")
		return
	}

	// flip sign for reverse compatibility
	reject = -reject

	return
}

func ChangeDriversActivityIfNeeded(offerState structures.OfferStates) error {
	if offerState.DriverUUID == "" {
		return nil
	}

	switch offerState.State {
	case constants.OrderStateOffered:
		// Clear driver promotion after the offer been offered
		err := ClearDriverPromotionBooster(offerState.DriverUUID)
		return errors.Wrap(err, "failed to clear driver's promotion")
	case constants.OrderStateFinished:
		activity, err := getActivityForAccepting(offerState.OfferUUID, offerState.StartState, 0)
		if err != nil {
			return errors.Wrap(err, "failed to calculate activity for finished order")
		}
		_, err = AddActivityForDriver(activityItemFromOfferState(offerState, activity, eventOrderFinished))
		return err
	case constants.OrderStateRejected:
		{ // не штрафовать водителя за отказ от встречного
			// TODO: логика херовая, но будет работать в 99% случаев, нужно переделать
			current, previous, err := getCurrentAndPreviousDriverStates(offerState.DriverUUID)
			if err != nil {
				return errpath.Err(err)
			}
			if current.State == constants.DriverStateWorking || previous.State == constants.DriverStateWorking {
				return nil
			}
		}
		offer, err := GetOfferByUUID(offerState.OfferUUID)
		if err != nil {
			return errors.Wrap(err, "failed to get an offer by UUID")
		}
		penalty, event, err := getPenaltyForRejecting(
			offerState.DriverUUID,
			offerState.OfferUUID,
			offer.TaximetrData.OfferData.IsFree || offer.TaximetrData.Order.ItIsOptional(),
			false,
			offer.TaximetrData.OfferData.RouteToClient.Proper.Distance,
		)
		if err != nil {
			return errors.Wrap(err, "failed to calculate penalty for rejected offer")
		}
		_, err = AddActivityForDriver(activityItemFromOfferState(offerState, -penalty, event))
		return err
	case constants.OrderStateCancelled:
		// If an order was cancelled by the fault of a driver, penalty him
		if offerState.Comment == constants.CancelReasonDriver {
			return penaltyDriverForCancelledByDriverOrder(offerState)
		}

		// Try to detect if an order was cancelled by a client
		if offerState.Comment == constants.CancelReasonClient {
			return PromoteDriverBooster(offerState.DriverUUID)
		}
		for _, reason := range constants.ListClientCancelReasons() {
			if offerState.Comment == reason {
				return PromoteDriverBooster(offerState.DriverUUID)
			}
		}
		return nil
	}
	return nil
}

func AddActivityForDriver(activity DriverActivityHistoryItem) (int, error) {
	// There is no need to continue, if activity isn't changed
	if activity.ActivityChange == 0 {
		return 0, nil
	}

	var newActivity int
	_, err := db.Model((*DriversApps)(nil)).
		Where("uuid = ?", activity.DriverUUID).
		Set(
			"activity = LEAST(GREATEST(activity + ?, ?), ?)",
			activity.ActivityChange, GetCurrentActivityConfig().MinDriverActivity, GetCurrentActivityConfig().MaxDriverActivity,
		). // control mix and max value
		Returning("activity").
		Update(&newActivity)
	if err != nil {
		return 0, errors.Wrapf(
			err, "failed to add activity %d for a driver with uuid %s", activity.ActivityChange, activity.DriverUUID,
		)
	}

	_, err = db.Model(&activity).Insert()
	if err != nil {
		return 0, errors.Wrap(err, "failed to insert an activity history item")
	}

	// Notify listeners about driver's activity changing
	go func() {
		log := logs.Eloger.WithFields(logrus.Fields{
			"driverUUID":      activity.DriverUUID,
			"offerUUID":       activity.OfferUUID,
			"reason":          activity.Event,
			"activity_change": activity.ActivityChange,
		})
		if err := rabsender.SendJSONByAction(
			rabsender.AcDriverActivityChanged, "", crmActivityHistoryItem(activity, newActivity),
		); err != nil {
			log.WithField("event", "notifying about driver activity changing").Error(err)
			return
		}
		log.WithField("event", "activity changed").Debug("OK")
	}()

	return newActivity, nil
}

func CountDriverRejectedOffers(driverUUID string, period time.Duration) (int, error) {
	count, err := db.Model((*OfferStatesDrv)(nil)).
		Where("driver_uuid = ?", driverUUID).
		Where("start_state >= ?", time.Now().Add(-period)).
		Where("state = ?", constants.OrderStateRejected).
		Count()
	return count, err
}

func CountDriverRejectedOffersForDay(driverUUID string) (int, error) {
	location, err := time.LoadLocation(config.St.Application.TimeZone)
	if err != nil {
		location = time.Local
	}

	now := time.Now().In(location)
	dayStart := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, location)

	count, err := db.Model((*OfferStatesDrv)(nil)).
		Where("driver_uuid = ?", driverUUID).
		Where("start_state >= ?", dayStart).
		Where("state = ?", constants.OrderStateRejected).
		Count()
	return count, err
}

func activityItemFromOfferState(offerState structures.OfferStates, amount int, event string) DriverActivityHistoryItem {
	return DriverActivityHistoryItem{
		ID:             uuid.Must(uuid.NewV4()).String(),
		DriverUUID:     offerState.DriverUUID,
		OfferUUID:      offerState.OfferUUID,
		OrderUUID:      offerState.OrderUUID,
		Event:          event,
		ActivityChange: amount,
	}
}

func hasDriverRejectedLimitRound(driverUUID string, period time.Duration, limit int, predict bool) (bool, error) {
	count, err := CountDriverRejectedOffers(driverUUID, period)
	if err != nil {
		return false, errors.Wrap(err, "failed to count rejected offers in the db")
	}
	if predict {
		count++
	}
	if count < limit || limit == 0 {
		return false, nil // has not pass the single round
	}
	return count%limit == 0, nil
}

func hasDriverCancelledLimitRound(driverUUID string, period time.Duration, limit int) (bool, error) {
	count, err := db.Model((*OfferStatesDrv)(nil)).
		Where("driver_uuid = ?", driverUUID).
		Where("start_state >= ?", time.Now().Add(-period)).
		Where("state = ?", constants.OrderStateCancelled).
		Where("comment = ?", constants.CancelReasonDriver).
		Count()
	if err != nil {
		return false, errors.Wrap(err, "failed to count cancelled orders in the db")
	}
	if count < limit || limit == 0 {
		return false, nil // has not pass the single round
	}
	return count%limit == 0, nil
}

func crmActivityHistoryItem(item DriverActivityHistoryItem, newActivity int) structures.ActivityHistoryItem {
	return structures.ActivityHistoryItem{
		UserUUID:       item.DriverUUID,
		OfferUUID:      item.OfferUUID,
		OrderUUID:      item.OrderUUID,
		Event:          item.Event,
		ActivityChange: item.ActivityChange,
		Activity:       newActivity,
	}
}

func penaltyDriverForCancelledByDriverOrder(offerState structures.OfferStates) error {
	// Check if there were multiple cancellations for the specified period
	limit, err := GetDriverCancelledOrdersLimit()
	if err != nil {
		return errors.Wrap(err, "failed to get cancelled orders limit for a driver")
	}
	period, err := GetDriverPeriodForMultiCancelledOrders()
	if err != nil {
		return errors.Wrap(err, "failed to get cancellation period")
	}
	cancelledLimitRound, err := hasDriverCancelledLimitRound(offerState.DriverUUID, period, limit)
	if err != nil {
		return errors.Wrap(err, "failed to inspect if a driver has reach the cancellation round")
	}

	driver, err := GetDriverByUUID(offerState.DriverUUID)
	if err != nil {
		return errors.Wrap(err, "failed to get driver by uuid")
	}

	// Increase penalty if driver cancelled more than allowed for the specified period again
	getPenaltyFn, event := GetDriverPenaltyForCancelledOrder, eventOrderCancelled
	if cancelledLimitRound {
		getPenaltyFn, event = GetDriverPenaltyForMultiCancelledOrders, eventTooManyOrdersCancelled
	}
	penalty, err := getPenaltyFn(&driver)
	if err != nil {
		return errors.Wrap(err, "failed to get driver penalty for a cancelled order")
	}
	_, err = AddActivityForDriver(activityItemFromOfferState(offerState, -penalty, event))
	if err != nil {
		return errors.Wrap(err, "failed to change driver's activity")
	}

	// Also penalty driver by withdrawing some money
	return nil
	//return WithdrawDriverForCancelledOrder(offerState)
}

func PromoteDriverBooster(driverUUID string) error {
	return setDriverPromotionBooster(driverUUID, "true")
}

func ClearDriverPromotionBooster(driverUUID string) error {
	return setDriverPromotionBooster(driverUUID, "false")
}

func setDriverPromotionBooster(driverUUID, booster string) error {
	_, err := db.Model((*DriversApps)(nil)).
		Where("uuid = ?", driverUUID).
		Set(fmt.Sprintf("promotion = jsonb_set(COALESCE(promotion, '{}'), '{booster}', '%s')", booster)).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to change driver's promotion")
	}
	return nil
}

func getActivityForAccepting(offerUUID string, startState time.Time, distanceToClient float64) (int, error) {
	// Check if it was a far order
	farDistance, err := GetDriverFarOrderDistance()
	if err != nil {
		return 0, errors.Wrap(err, "failed to get an order far distance value")
	}

	// Try to load distance to client from the offer itself in case if it's not provided
	if distanceToClient == 0 {
		offer, err := GetOfferByUUID(offerUUID)
		if err != nil {
			return 0, errors.Wrap(err, "failed to get an offer by UUID")
		}
		distanceToClient = offer.TaximetrData.OfferData.RouteToClient.Proper.Distance
	}
	getActivityFn := GetDriverActivityForFinishedOrder
	if distanceToClient >= float64(farDistance) {
		getActivityFn = GetDriverActivityForFinishedFarOrder
	}
	activity, err := getActivityFn()
	if err != nil {
		return 0, errors.Wrap(err, "failed to get driver activity for a finished order")
	}

	// If an order have been completed during the rush hours, add the bonus
	rushHours, err := GetRushHours()
	if err != nil {
		logs.Eloger.WithField("event", "trying to check if it's a rush hour").Error(err)
	}
	var bonus int
	if rushHours.Include(startState, config.St.Application.TimeZone) {
		// Set some real value to the bonus
		bonus, err = GetDriverActivityBonusForRushHours()
		if err != nil {
			logs.Eloger.WithField("event", "trying to fetch a rush hour bonus from the db").Error(err)
		}
	}
	activity += bonus
	return activity, nil
}

func getPenaltyForRejecting(driverUUID, offerUUID string, isFreeOrder, predict bool, distance float64) (
	penalty int, event string, err error,
) {
	if isFreeOrder {
		return 0, "", nil
	}

	// Check if there were multiple rejections for the specified period
	limit, err := GetDriverRejectedOffersLimit()
	if err != nil {
		return 0, "", errors.Wrap(err, "failed to get rejected offers limit for a driver")
	}
	period, err := GetDriverPeriodForMultiRejectedOffers()
	if err != nil {
		return 0, "", errors.Wrap(err, "failed to get rejection period")
	}
	rejectedLimitRound, err := hasDriverRejectedLimitRound(driverUUID, period, limit, predict)
	if err != nil {
		return 0, "", errors.Wrap(err, "failed to inspect if a driver has reach the rejection round")
	}

	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return 0, "", errors.Wrap(err, "failed to get a driver by uuid")
	}

	// Try to load distance to client from the offer itself in case if it's not provided
	if distance == 0 {
		offer, err := GetOfferByUUID(offerUUID)
		if err != nil {
			return 0, "", errors.Wrap(err, "failed to get an offer by UUID")
		}
		distance = offer.TaximetrData.OfferData.RouteToClient.Proper.Distance
	}

	// Increase penalty if driver rejected more than allowed for the specified period again
	getPenaltyFn, event := GetDriverPenaltyForRejectedOffer, eventOfferRejected
	if rejectedLimitRound {
		getPenaltyFn, event = GetDriverPenaltyForMultiRejectedOffers, eventTooManyOffersRejected
	}
	penalty, err = getPenaltyFn(&driver, distance)
	if err != nil {
		return 0, "", errors.Wrap(err, "failed to get driver penalty for a rejected offer")
	}
	return
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

const (
	activityRateEndpoint = "/config/activity_rate"

	buyActivityEvent = "buy activity"
	msgBuyActivity   = "Покупка активности"
)

func GetActivityRate(context.Context) (structures.ActivityPriceItem, error) {
	var result structures.ActivityPriceItem
	if err := request(http.MethodGet, config.St.CRM.Host+activityRateEndpoint, nil, &result); err != nil {
		return structures.ActivityPriceItem{}, errors.Wrap(err, "failed to fetch activity rate from CRM")
	}
	return result, nil
}

func BuyActivity(ctx context.Context, driverUUID string, activity int) (int, error) {
	if activity <= 0 {
		return 0, errors.New("Значение активности должно быть положительным")
	}

	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get a driver by uuid")
	}

	if driver.Activity+activity > GetCurrentActivityConfig().MaxDriverActivity {
		return 0, errors.Errorf(
			"Ваша активность не можете превышать %d единиц. Вы можете купить максимум %d единиц активности.",
			GetCurrentActivityConfig().MaxDriverActivity, GetCurrentActivityConfig().MaxDriverActivity-driver.Activity,
		)
	}

	rate, err := GetActivityRate(ctx)
	if err != nil {
		return 0, errors.Wrap(err, "failed to get activity rate")
	}

	requiredAmount := rate.Rate * float64(activity)
	if driver.GetTotalBalance() < requiredAmount {
		return 0, errors.Errorf(
			"У вас недостаточно средств. Необходимо иметь %.2f руб, а ваш баланс равен %.2f руб",
			requiredAmount, driver.GetTotalBalance(),
		)
	}

	// Buy activity
	newActivity, err := AddActivityForDriver(DriverActivityHistoryItem{
		ID:             structures.GenerateUUID(),
		DriverUUID:     driverUUID,
		Event:          buyActivityEvent,
		ActivityChange: activity,
	})
	if err != nil {
		return 0, errors.Wrap(err, "failed to add activity for a driver")
	}

	// If all is ok, then actually pay for the activity
	go func() {
		transfer := structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     structures.TransferTypeWithdraw,
			Amount:           requiredAmount,
			Description:      msgBuyActivity,
			PayerType:        structures.UserTypeDriver,
			PayerUUID:        driverUUID,
			Meta: structures.TransferMetadata{
				CurrentDriverBalance:     &driver.Balance,
				CurrentDriverCardBalance: &driver.CardBalance,
			},
		}
		if err := rabsender.SendJSONByAction(rabsender.AcMakeDriverTransfer, "", transfer); err != nil {
			logs.LoggerForContext(ctx).WithField("event", "buying a tariff").Error(err)
			return
		}
		logs.LoggerForContext(ctx).WithField("event", "buying a tariff sent to RMQ").Debug("OK")
	}()

	// Unblock a driver
	driver.Activity = newActivity
	if err := UnblockDriverIfNeeded(ctx, &driver); err != nil {
		return 0, errors.Wrap(err, "failed to unblock a driver")
	}

	return newActivity, nil
}

const (
	activityHistoryInterval = -time.Hour * 24
)

type ActivityHistoryItem struct {
	tableName struct{} `sql:"drv_activity_history" pg:",discard_unknown_columns"`

	OrderUUID      string    `json:"order_uuid"`
	OrderID        int       `json:"order_id"`
	Event          string    `json:"event"`
	ActivityChange int       `json:"activity_change"`
	CreatedAt      time.Time `json:"-"`
	Created        int64     `json:"created_at"`
}

var translatedActivityEvents = map[string]string{
	eventOrderFinished:          "Заказ выполнен",
	eventOfferRejected:          "Отказ от заказа",
	eventTooManyOffersRejected:  "Большое количество отказов от заказа",
	eventOrderCancelled:         "Заказ отменен по вине водителя",
	eventTooManyOrdersCancelled: "Большое количество отмененных по вине водителя заказов",
	buyActivityEvent:            msgBuyActivity,
}

func ActivityHistory(ctx context.Context, driverUUID string) ([]*ActivityHistoryItem, error) {
	var result []*ActivityHistoryItem
	err := db.ModelContext(ctx, &result).
		Column("activity_history_item.*").
		ColumnExpr("ord.uuid AS order_uuid").
		ColumnExpr("ord.id AS order_id").
		Join("LEFT JOIN drv_offers off ON activity_history_item.offer_uuid = off.uuid").
		Join("LEFT JOIN drv_orders ord ON off.order_uuid = ord.uuid").
		Where("activity_history_item.driver_uuid = ?", driverUUID).
		Where("activity_history_item.created_at >= ?", time.Now().Add(activityHistoryInterval)).
		Order("activity_history_item.created_at DESC").
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch records")
	}

	// Translate events
	for _, item := range result {
		if tr, found := translatedActivityEvents[item.Event]; found {
			item.Event = tr
		}
		item.Created = item.CreatedAt.Unix()
	}
	return result, nil
}

func PenalizeInactiveDrivers(ctx context.Context) (int, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "inactive drivers penalty",
	})

	offlineConfig := GetCurrentActivityConfig().TooLongOffline

	onlineStatesExistQuery := db.ModelContext(ctx, (*DriverStatesDrv)(nil)).
		Where("driver_uuid = uuid").
		Where("created_at > now() - interval'? days'", offlineConfig.DaysThreshold).
		Where("state = ?", constants.DriverStateOnline).
		Column("*")

	var inactiveDrivers []DriversApps
	err := db.ModelContext(ctx, &inactiveDrivers).
		Where("status = ?", constants.DriverStateOffline).
		Where("activity > ?", offlineConfig.ActivityLowerLimit).
		Where("NOT EXISTS(?)", onlineStatesExistQuery).
		Column("uuid", "activity").
		Select()
	if err != nil {
		return 0, errors.Wrap(err, "failed to fetch inactive drivers")
	}

	for _, driver := range inactiveDrivers {
		penalty := offlineConfig.DefaultPenalty
		if newActivity := driver.Activity - penalty; newActivity < offlineConfig.ActivityLowerLimit {
			// не можем штрафом сделать активность ниже порога
			penalty -= offlineConfig.ActivityLowerLimit - newActivity
		}

		historyItem := DriverActivityHistoryItem{
			ID:             structures.GenerateUUID(),
			DriverUUID:     driver.UUID,
			Event:          "inactivity penalty",
			ActivityChange: -penalty,
		}

		_, err := AddActivityForDriver(historyItem)
		if err != nil {
			log.WithError(err).Warn("failed to change driver activity")
		}
	}

	return len(inactiveDrivers), nil
}
