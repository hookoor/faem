package models

import (
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
)

type AlertsDRV struct {
	tableName     struct{}  `sql:"drv_alerts"`
	UUID          string    `json:"uuid"`
	TicketUUID    string    `json:"ticket_uuid"`
	Message       string    `json:"message"`
	Ack           bool      `json:"ack"`
	OrderUUID     string    `json:"order_uuid"`
	DriverUUID    string    `json:"driver_uuid"`
	CreatedAt     time.Time `json:"-"`
	CreatedAtUnix int64     `sql:"-" json:"created_at_unix"`
	Tag           string    `json:"tag"`
}

func (alert *AlertsDRV) Save() error {
	err := alert.validate()
	if err != nil {
		return err
	}
	alert.CreatedAt = time.Now()
	alert.UUID = structures.GenerateUUID()
	_, err = db.Model(alert).
		Returning("*").
		Insert()
	return err
}

func SaveManyAlerts(alerts []AlertsDRV) error {
	for i := range alerts {
		err := alerts[i].validate()
		if err != nil {
			return err
		}
		alerts[i].UUID = structures.GenerateUUID()
	}
	_, err := db.Model(&alerts).
		Insert()
	return err
}
func MarkAlertsAsRead(driverUUID string, alertsUUID []string) error {
	err := validateAlertsForMarkingAsRead(driverUUID, alertsUUID)
	if err != nil {
		return err
	}
	_, err = db.Model(&AlertsDRV{}).
		Where("uuid in (?)", pg.In(alertsUUID)).
		Set("ack = true").
		Update()
	return err
}

func validateAlertsForMarkingAsRead(driverUUID string, alertsUUID []string) error {
	var alerts []AlertsDRV
	err := db.Model(&alerts).
		Where("driver_uuid = ?", driverUUID).
		Where("ack is not true").
		Order("created_at DESC").
		Select()
	if err != nil {
		return fmt.Errorf("error finding driver alerts,%s", err)
	}
	if len(alerts) == 0 {
		return fmt.Errorf("not read alerts for driver [%s] not found", driverUUID)
	}

	for _, alertUUID := range alertsUUID {
		check := false
		for _, alert := range alerts {
			if alertUUID == alert.UUID {
				check = true
				break
			}
		}
		if !check {
			return fmt.Errorf("alert [%s] does not belong to the driver or is already marked as read", alertUUID)
		}
	}
	return nil
}
func GetAlertsByDriverUUID(driverUUID string, pager urlvalues.Pager) ([]AlertsDRV, error) {
	var result []AlertsDRV

	passedTime, err := time.ParseDuration(config.St.Drivers.AlertsExpirationTime)
	if err != nil {
		return nil, err
	}
	err = db.Model(&result).
		Where("driver_uuid = ?", driverUUID).
		Where("created_at > ?", time.Now().Add(-passedTime)).
		Order("created_at DESC").
		Apply(pager.Pagination).
		Select()
	return result, err
}
func (alert *AlertsDRV) validate() error {
	if alert.DriverUUID == "" {
		return errors.New("empty driver uuid")
	}
	return nil
}
func FillCreatedAtUnix(alerts []AlertsDRV) {
	for i := range alerts {
		alerts[i].CreatedAtUnix = alerts[i].CreatedAt.Unix()
	}
}
