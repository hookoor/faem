package models

import (
	"fmt"
	"net/http"
	"sort"
	"strings"
	"sync"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
)

type (
	// OfferForDriver структура для отправки оффера водителю.Содержит кроме самого офера его актуальный статус
	OfferForDriver struct {
		structures.OfferTaximetr
		State struct {
			Value   string `json:"value"`
			Message string `json:"message"`
		} `json:"order_state"`
		StartTime int64 `json:"start_time"`
	}

	// дублируем структуру из locationsaver
	// TODO: вынести эту структуру в shared, так как не DRY
	locData struct {
		ID         int
		DriverUUID string  `json:"driver_uuid"`
		Latitude   float64 `json:"lat"`
		Longitude  float64 `json:"lng"`
		DriverID   int     `json:"driver_id"`
		Satelites  int     `json:"sats"`
		Timestamp  int64   `json:"time"`
		CreatedAt  time.Time
	}

	// таблица результатов скоринга
	scoreResultsDrv struct {
		tableName struct{} `sql:"drv_score_result"`
		structures.ScoreResults
		ID        int       `json:"id"`
		CreatedAt time.Time `json:"created_at"`
	}
	// FcmStruct структура, отпраляющаяся водителю в поле data
	FcmStruct struct {
		Tag     string `json:"tag"`
		Message string `json:"message"`
	}
)

const (
	minDriverWaitTime             int    = 4 //drivers, who wait less this, will not accept orders
	SortTypeByActivity            string = "activity"
	EndPointForOrdersUUIDInState  string = "/orders/uuidbystate/"
	EndPointForOrdersDataInState  string = "/orders/data/bystate/"
	EndPointForOrdersDataInStates string = "/orders/data/bystates"
	SortTypeByDistance            string = "distance"
	SortTypeByPrice               string = "price"
)

type drvRoutesAnnotations struct {
	drvUUID   string
	durations float64
	distances float64
}

//GetOfferByUUID возвращает стандартную структуру оффера по uuid
func GetOfferByUUID(uuid string) (OfferDrv, error) {
	var (
		offer OfferDrv
	)

	err := db.Model(&offer).
		Column("*").
		Where("uuid = ?", uuid).
		Limit(1).
		Select()

	if err != nil {
		return OfferDrv{}, err
	}

	return offer, nil
}

// GetAllFreeOffersUUID возвращает uuid всех свободных заказов
func GetAllFreeOffersUUID() ([]string, error) {
	return GetAllOffersUUIDByStates(constants.OrderStateFree)
}

// GetAllOrdersUUIDByStates returns map of uuid to state
func GetAllOrdersUUIDByStates(states ...string) (map[string]string, error) {
	var ordersData []structures.OrderUUIDWithStateTransferState
	url := config.St.CRM.Host + EndPointForOrdersDataInStates
	args := structures.OrdersDataByStatesArgs{States: states}
	if err := request(http.MethodPost, url, args, &ordersData); err != nil {
		return nil, errors.Wrap(err, "failed to get orders by states via rpc")
	}

	result := make(map[string]string)
	for _, order := range ordersData {
		result[order.UUID] = order.State
	}
	return result, nil
}

// GetOrdersInRegion returns map of uuid to state
func GetOrdersInRegion(regionID int, orderStates ...string) (map[string]string, error) {
	var ordersData []structures.OrderUUIDWithStateTransferState

	url := config.St.CRM.Host + EndPointForOrdersDataInStates
	args := structures.OrdersDataByStatesArgs{
		RegionID: regionID,
		States:   orderStates,
	}

	if err := tool.SendRequest(http.MethodPost, url, nil, args, &ordersData); err != nil {
		return nil, errors.Wrap(err, "failed to get orders by states via rpc")
	}

	result := make(map[string]string)
	for _, order := range ordersData {
		result[order.UUID] = order.State
	}
	return result, nil
}

func GetOrdersDataByState(state string) ([]structures.OrderUUIDWithStateTransferState, error) {
	var ordersData []structures.OrderUUIDWithStateTransferState
	url := config.St.CRM.Host + EndPointForOrdersDataInState + state
	err := request(http.MethodGet, url, nil, &ordersData)
	return ordersData, err
}

// GetAllOffersUUIDByStates возвращает uuid всех заказов с указанными статусами
func GetAllOffersUUIDByStates(states ...string) ([]string, error) {
	freeOrdersData, err := GetAllOrdersUUIDByStates(states...)
	if err != nil {
		return nil, err
	}
	if len(freeOrdersData) == 0 {
		return nil, nil
	}

	freeOrdersUUIDs := make([]string, 0, len(freeOrdersData))
	for uuid := range freeOrdersData {
		freeOrdersUUIDs = append(freeOrdersUUIDs, uuid)
	}
	freeOffersUUIDS, err := GetOffersUUIDByOrdersUUID(freeOrdersUUIDs)
	return freeOffersUUIDS, err
}

func GetOffersUUIDByOrdersUUIDMap(ordersUUID []string) (map[string]string, error) {
	var offers []OfferDrv
	result := make(map[string]string)
	if len(ordersUUID) == 0 {
		return nil, fmt.Errorf("ordersUUID is empty")
	}
	err := db.Model(&offers).
		Where("order_uuid in (?)", pg.In(ordersUUID)).
		Returning("uuid,order_uuid").
		Select()
	if err != nil {
		return nil, fmt.Errorf("error getting offers by orderUUID,%s", err)
	}
	for _, of := range offers {
		result[of.OrderUUID] = of.UUID
	}
	return result, err
}

func GetOffersUUIDByOrdersUUID(ordersUUID []string) ([]string, error) {
	var offers []OfferDrv
	var result []string
	if len(ordersUUID) == 0 {
		return nil, fmt.Errorf("ordersUUID is empty")
	}
	err := db.Model(&offers).
		Where("order_uuid in (?)", pg.In(ordersUUID)).
		Column("uuid").
		Select()
	if err != nil {
		return nil, fmt.Errorf("error getting offers by orderUUID,%s", err)
	}
	for _, of := range offers {
		result = append(result, of.UUID)
	}
	return result, err
}

//GetOffersInStates возвращает статусы всех заказов, находящихся в переданном состоянии
func GetOffersInStates(neededState string) ([]OfferStatesDrv, error) {
	var (
		offerStates []OfferStatesDrv
	)

	offerStatesQ := db.Model((*OfferStatesDrv)(nil)).
		ColumnExpr("DISTINCT ON (offer_uuid) *").
		Order("offer_uuid").
		Order("start_state DESC")

	err := db.Model().
		TableExpr("(?) s", offerStatesQ).
		Where("s.state = ?", neededState).
		Select(&offerStates)
	return offerStates, err
}

// GetRecentFreeOffers возвращает все свободные заказы
func GetRecentFreeOffers(regionID int) ([]OfferDrv, error) {
	return AllFreeOffers[regionID], nil
}

// GetAllOffersByStates возвращает все заказы с указанными статусами
func GetAllOffersByStates(recent bool, states ...string) ([]OfferDrv, error) {
	var freeOffers []OfferDrv
	freeOrdersData, err := GetAllOrdersUUIDByStates(states...)
	if err != nil {
		return freeOffers, fmt.Errorf("error getting free offer uuids: %s", err)
	}
	if len(freeOrdersData) == 0 {
		return freeOffers, nil
	}

	freeOrdersUUIDs := make([]string, 0, len(freeOrdersData))
	for uuid := range freeOrdersData {
		freeOrdersUUIDs = append(freeOrdersUUIDs, uuid)
	}
	order := "created_at"
	if recent {
		order += " DESC"
	}
	err = db.Model(&freeOffers).
		Where("order_uuid in (?)", pg.In(freeOrdersUUIDs)).
		Order(order).
		Select()
	if err != nil {
		return freeOffers, fmt.Errorf("error getting free offers,%s", err)
	}

	// Fill the state field
	for i, offer := range freeOffers {
		state := freeOrdersData[offer.OrderUUID]
		offer.DistributionMeta.State = state
		freeOffers[i] = offer
	}

	return freeOffers, err
}

// GetRegionalOffersByState возвращает все заказы с указанными статусами
func GetRegionalOffersByState(regionID int, recent bool, states ...string) ([]OfferDrv, error) {
	var freeOffers []OfferDrv
	freeOrdersData, err := GetOrdersInRegion(regionID, states...)
	if err != nil {
		return freeOffers, fmt.Errorf("error getting free offer uuids: %s", err)
	}
	if len(freeOrdersData) == 0 {
		return freeOffers, nil
	}

	freeOrdersUUIDs := make([]string, 0, len(freeOrdersData))
	for uuid := range freeOrdersData {
		freeOrdersUUIDs = append(freeOrdersUUIDs, uuid)
	}

	orderBy := "created_at"
	if recent {
		orderBy += " DESC"
	}

	query := db.Model(&freeOffers).
		Where("order_uuid in (?)", pg.In(freeOrdersUUIDs)).
		Order(orderBy)
	if err := query.Select(); err != nil {
		return freeOffers, fmt.Errorf("error getting free offers,%s", err)
	}

	// Fill the state field
	for i, offer := range freeOffers {
		state := freeOrdersData[offer.OrderUUID]
		offer.DistributionMeta.State = state
		freeOffers[i] = offer
	}

	return freeOffers, err
}

// GetFreeOfferDetails returns a free offer information with ad§ional activity and route to client data.
func GetFreeOfferDetails(driverUUID, offerUUID string) (*structures.OfferTaximetr, error) {
	if driverUUID == "" || offerUUID == "" {
		return nil, errors.New("empty driverUUID or offerUUID provided")
	}

	const event = "get free offer details"
	offer, err := getFreeOrderForDriver(driverUUID, offerUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     event,
			"offerUUID": offerUUID,
			"reason":    "failed to get a free order for a driver",
		}).Error(err)
		return nil, err
	}
	taxData := offer.TaximetrData

	// Calculate additional info for a driver
	taxData.OfferData.IsFree = true
	taxData.OfferData.RouteToClient, err = CalcRouteToClient(offer.OrderUUID, driverUUID, true)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     event,
			"offerUUID": offerUUID,
			"reason":    "failed to calc a route to a client",
		}).Error(err)
		return nil, err
	}
	taxData.OfferData.ArrivalTime = 0
	taxData.OfferData.CalcAndSetArrivalTime()
	taxData.OfferData.Activity.Accept, taxData.OfferData.Activity.Reject, err = GetPossibleActivity(
		PossibleActivityArgs{
			OfferUUID:        offerUUID,
			DriverUUID:       driverUUID,
			StartState:       time.Now(),
			IsFreeOrder:      true,
			DistanceToClient: taxData.OfferData.RouteToClient.Proper.Distance,
		},
	)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "assign offer to the driver",
			"offerUUID": offerUUID,
			"reason":    "failed to get possibly activity",
		}).Error(err)
		return nil, err
	}

	return &taxData, nil
}

func SendFreeOfferToDriverSafely(driverUUID string, offerUUID string) error {
	offerJob := jobs.GetJobQueue(JobQueueAssignOrders, JobQueueLimitOffers)
	err := offerJob.Execute(uuidHash(offerUUID), func() error {
		IncMutexFreeOffersCountMetric("mutex_freeoffers")
		defer DecMutexFreeOffersCountMetric("mutex_freeoffers")

		return SendFreeOfferToDriver(driverUUID, offerUUID)
	})
	return err
}

// SendFreeOfferToDriver отправляет оффер водиле
func SendFreeOfferToDriver(driverUUID string, offerUUID string) error {
	// Check if a driver is already working, then forbid him to take another order
	currentDriverState, err := GetCurrentDriverStateFromCRM(driverUUID)
	if err != nil {
		return err
	}

	switch currentDriverState {
	case constants.DriverStateConsidering:
		return errors.New("Вы не можете принять свободный заказ. В данный момент Вам назначается другой заказ.")
	case constants.DriverStateWorking:
		return errors.New("Вы не можете принять свободный заказ. Сперва завершите активную поездку.")
	case constants.DriverStateBlocked:
		return errors.New("Вы не можете принять свободный заказ. Ваш аккаунт заблокирован.")
	case constants.DriverStateOnModeration, constants.DriverStateOnModerationAfterChange:
		return errors.New("Вы не можете принять свободный заказ. Ваш аккаунт находится на модерации.")
	case constants.DriverStateOffline:
		return errors.New("Переключитесь в онлайн. В оффлайне принятие заказов недоступно")
	}

	offer, err := getFreeOrderForDriver(driverUUID, offerUUID)
	if err != nil {
		return err
	}

	// Check if a driver has enough balance to complete the order
	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return errors.Wrapf(err, "failed to get a driver by uuid %s", driverUUID)
	}
	driver.DrvTariff.Markups, err = GetDriverTariffMarkups(driver.DrvTariff.UUID)
	if err != nil {
		return errors.Wrapf(err, "failed to get a driver tariff markups by uuid %s", driverUUID)
	}
	canCompleteOrder, err := driver.HasEnoughBalance(&offer)
	if err != nil {
		return errors.Wrap(err, "failed to detect if a driver has enough balance to complete the order")
	}
	if !canCompleteOrder {
		return errors.New("Вы не может принять заказ: на вашем счете недостаточно средств")
	}

	err = SetOfferAcceptedFromOffline(currentDriverState == constants.DriverStateOffline, offerUUID)
	if err != nil {
		return errors.Wrap(err, "error setting offer accepted from offline")
	}
	return AssignOfferToTheDriver(offer, true, false)
}

// use NewBusyLocker()
type BusyLocker struct {
	locks map[string]bool
	mx    sync.RWMutex
}

func NewBusyLocker() *BusyLocker {
	return &BusyLocker{
		locks: make(map[string]bool),
	}
}

func (al *BusyLocker) IsOccupied(uuid string) bool {
	al.mx.RLock()
	locked := al.locks[uuid]
	al.mx.RUnlock()

	return locked
}

func (al *BusyLocker) Occupy(uuid string, duration time.Duration) {
	al.mx.Lock()
	al.locks[uuid] = true
	al.mx.Unlock()

	time.AfterFunc(duration, func() {
		al.Release(uuid)
	})

}

func (al *BusyLocker) Release(uuid string) {
	al.mx.Lock()
	delete(al.locks, uuid)
	al.mx.Unlock()
}

var assignLocker = NewBusyLocker()

// установка метки встречного заказа на ордере
func setCounterOrderMarker(orderUUID string, value bool, updatePersistent bool) error {
	return tool.SendRequest(http.MethodPost,
		config.St.CRM.Host+"/orders/counterordermarker/"+orderUUID,
		nil,
		fmt.Sprintf(`{"is_counter_order": %v, "update_persistent_marker": %v}`, value, updatePersistent),
		nil,
	)
}

// получение значения метки встречного заказа на ордере
func getCounterOrderMarker(orderUUID string) (bool, error) {
	var result bool
	err := tool.SendRequest(http.MethodGet,
		config.St.CRM.Host+"/orders/counterordermarker/"+orderUUID,
		nil,
		nil,
		&result,
	)

	return result, err
}

//AssignOfferToTheDriver отправляет оффер водителю, указанному в offer.DriverUUID
func AssignOfferToTheDriver(offer OfferDrv, isFree bool, isCounter bool) error {
	// TODO: убрать этот костыль
	// запрещаем другие назначения этому водителю на 10 сек
	if assignLocker.IsOccupied(offer.DriverUUID) {
		return errors.New("driver has assignations in process")
	}
	assignLocker.Occupy(offer.DriverUUID, 10*time.Second)

	// назначаем данные для таксометра
	var (
		taxData    structures.OfferTaximetr
		orderState OfferStatesDrv
	)

	taxData = offer.TaximetrData
	taxData.OfferData.DriverUUID = offer.DriverUUID

	currentOfferState, err := GetOfferActualState(offer.UUID)
	if err != nil {
		return errors.Wrap(err, "failed to fetch current offer state")
	}

	orderState.InitFsm()
	orderState.DriverUUID = offer.DriverUUID
	if isFree {
		orderState.Flags |= structures.OfferStateFlagFreeOrder
	}
	if isCounter {
		orderState.Flags |= structures.OfferStateFlagCounterOrder
	}
	orderState.OfferUUID = taxData.OfferData.UUID
	orderState.OrderUUID = taxData.OfferData.OrderUUID
	if err = orderState.ApplyEventWithMutex(constants.OrderStateOffered); err != nil {
		return errors.Wrap(err, "failed to apply FSM event")
	}

	if isCounter {
		err := setCounterOrderMarker(offer.OrderUUID, true, true)
		if err != nil {
			return errors.Wrap(err, "failed to mark order as counter")
		}
	}

	// Calculate additional info for a driver
	taxData.OfferData.IsFree = isFree
	taxData.OfferData.RouteToClient, err = CalcRouteToClient(taxData.OfferData.OrderUUID, taxData.OfferData.DriverUUID, isCounter)
	if err != nil {
		return errors.Wrap(err, "failed to calc route from driver to client")
	}
	taxData.OfferData.CalcAndSetArrivalTime()
	taxData.OfferData.Activity.Accept, taxData.OfferData.Activity.Reject, err = GetPossibleActivity(
		PossibleActivityArgs{
			OfferUUID:        orderState.OfferUUID,
			DriverUUID:       orderState.DriverUUID,
			StartState:       orderState.StartState,
			IsFreeOrder:      isFree || taxData.Order.ItIsOptional(),
			DistanceToClient: taxData.OfferData.RouteToClient.Proper.Distance,
		},
	)
	if err != nil {
		return errors.Wrap(err, "failed to calc possible activity")
	}

	if err = UpdateOfferTaximeter(taxData); err != nil {
		return errors.Wrap(err, "failed to update taximeter data")
	}

	// дописываем водителя в оффер
	err = AppendDriverToOffer(taxData.OfferData.UUID, taxData.OfferData.DriverUUID, isCounter)
	if err != nil {
		return errors.Wrap(err, "failed to append driver to offer")
	}

	// смена статуса водителя на 'considering', чтобы ему не приходили новые заказы
	_, err = SetDriverStatus(taxData.OfferData.DriverUUID, constants.DriverStateConsidering, "Обдумывает предложение")
	if err != nil {
		return errors.Wrap(err, "failed to change driver status")
	}

	// отправляем водителю команду обновиться (постучать на /initdata)
	err = SendOrderToDriver(taxData.OfferData.DriverUUID)
	if err != nil {
		return errors.Wrap(err, "failed to send offer to driver")
	}

	// увеличение счетчика попыток назначений заказа
	switch currentOfferState.State {
	case constants.OrderStateDistributing, constants.OrderStateFree:
		offer.DistributionMeta.SmartAttempts++
		if err = offer.UpdateOfferDistributionMeta(); err != nil {
			return errors.Wrap(err, "failed to update meta in offer")
		}
	}

	// да-да, я в курсе что сверху такое же условие
	// это наименее важная часть, поэтому ничего страшного не произойдет если вернет ошибку
	if isCounter {
		err := updateLastCounterAssignment(offer.DriverUUID)
		if err != nil {
			return errors.Wrap(err, "failed to update last_counter_assignment")
		}
	}

	return nil
}

func updateLastCounterAssignment(driverUUID string) error {
	return tool.SendRequest(
		http.MethodPut,
		fmt.Sprintf("%s/%s/driver/%s/last_counter_assignment", config.St.CRM.Host, constants.InternalGroup, driverUUID),
		nil,
		nil,
		nil,
	)
}

//AppendDriverToOffer изменяет driver_uuid оффера на указанный и добавляет его в rejected_drivers_uuid
func AppendDriverToOffer(offerUUID string, driverUUID string, isCounter bool) error {
	query := db.Model((*OfferDrv)(nil)).
		Where("uuid = ?", offerUUID).
		Set("driver_uuid = ?", driverUUID)
	if isCounter {
		query.Set("counter_rejected_drivers_uuid = COALESCE(counter_rejected_drivers_uuid, '[]') || to_jsonb(?::text)", driverUUID)
	} else {
		query.Set("rejected_drivers_uuid = COALESCE(rejected_drivers_uuid, '[]') || to_jsonb(?::text)", driverUUID)
	}
	if _, err := query.Update(); err != nil {
		return err
	}

	return nil
}

//SendOrderToDriver отправляет водителю заказ
func SendOrderToDriver(driverUUID string) error {
	token, err := GetCurrentDriverFCMToken(driverUUID)
	if err != nil {
		return fmt.Errorf("(error getting current driver fcm token,%s", err)
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(driverUUID, "", token, constants.FcmTagNewOrderToDriver, int(config.St.Drivers.ResponseTime))
	return nil
}

func getDriverDistancesToOffers(drvLoc structures.DriverCoordinates, offers []OfferDrv) (map[string]float64, error) {
	result := make(map[string]float64)

	locations := make([]structures.PureCoordinates, 0, len(offers)+1)
	locations = append(locations, structures.PureCoordinates{
		Lat:  drvLoc.Lat,
		Long: drvLoc.Long,
	})

	for _, offer := range offers {
		locations = append(locations, structures.PureCoordinates{
			Lat:  float64(offer.TaximetrData.Order.Routes[0].Lat),
			Long: float64(offer.TaximetrData.Order.Routes[0].Lon),
		})
	}

	data, err := osrm.New(config.St.OSRM.Host).Table(locations, locations[:1], locations[1:])
	if err != nil {
		return nil, err
	}

	for j, offer := range offers {
		result[offer.UUID] = data.GetDistance(0, j)
	}

	return result, nil
}

// TODO: отрефакторить и перенести в пакет distribution
//GetFreeOrdersByDriver возвращает все свободные заказы около водителя
func GetFreeOrdersByDriver(regionID int, driverUUID, driverTaxiParkUUID, service string, orderNum int, exceptDebug bool, sortType ...string) ([]structures.OfferTaximetr, error) {
	freeOffers, err := GetRecentFreeOffers(regionID)
	if err != nil {
		return nil, fmt.Errorf("error finding free offers uuid,%s", err)
	}
	// var drvLocation locData
	// err = db.Model(&drvLocation).
	// 	Column("*").
	// 	Where("driver_uuid = ?", driverUUID).
	// 	Order("timestamp DESC").
	// 	Limit(1).
	// 	Select()
	// if err != nil {
	// 	return nil, fmt.Errorf("error finding driver actual location,%s", err)
	// }
	if service != "" {
		var sortFreeOffersByService []OfferDrv
		for _, off := range freeOffers {
			if off.TaximetrData.Order.Service.Name == service {
				sortFreeOffersByService = append(sortFreeOffersByService, off)
			}
		}
		freeOffers = sortFreeOffersByService
	}
	if driverTaxiParkUUID != "" {
		err := FillTaxiParksData(freeOffers)
		if err != nil {
			return nil, fmt.Errorf("error fill taxi parks data, %s", err)
		}
		var sortFreeOffersByTaxiPark []OfferDrv
		for _, off := range freeOffers {
			skip := structures.StringInArray(driverTaxiParkUUID, off.TaximetrData.Order.TaxiParkData.UnwantedUUID)
			if !skip {
				sortFreeOffersByTaxiPark = append(sortFreeOffersByTaxiPark, off)
			}
		}
		freeOffers = sortFreeOffersByTaxiPark
	}
	if exceptDebug {
		var sortFreeOffersByDebug []OfferDrv
		for _, off := range freeOffers {
			if off.TaximetrData.Order.Service.Name != "Дебаг" {
				sortFreeOffersByDebug = append(sortFreeOffersByDebug, off)
			}
		}
		freeOffers = sortFreeOffersByDebug
	}
	if len(freeOffers) == 0 {
		return nil, nil
	}
	drvLocations, err := GetDriversLastLocations(driverUUID)
	if err != nil {
		return nil, fmt.Errorf("error finding driver actual location,%s", err)
	}
	if drvLocations == nil {
		return nil, errors.Errorf("no known last location for a driver %s", driverUUID)
	}
	offerDistance, err := getDriverDistancesToOffers(drvLocations[0], freeOffers)

	var (
		result     []structures.OfferTaximetr
		neededUUID string
	)
	offerDistanceValue := SortMapSF64(offerDistance)
	for _, dis := range offerDistanceValue {
		for indexUUID, v := range offerDistance {
			if v == dis {
				neededUUID = indexUUID
				offerDistance[indexUUID] = 0
				break
			}
		}
		for _, offer := range freeOffers {
			if offer.UUID == neededUUID {
				offer.TaximetrData.OfferData.IsFree = true
				offer.TaximetrData.OfferData.RouteToClient.Proper.Distance = dis
				offer.TaximetrData.OfferData.Activity.Accept, offer.TaximetrData.OfferData.Activity.Reject, err = GetPossibleActivity(
					PossibleActivityArgs{
						OfferUUID:        offer.UUID,
						DriverUUID:       driverUUID,
						StartState:       time.Now(),
						IsFreeOrder:      true,
						DistanceToClient: dis,
					},
				)
				result = append(result, offer.TaximetrData)
				break
			}
		}
		if len(result) >= orderNum {
			break
		}
	}
	if len(sortType) == 0 {
		return result, nil
	}
	switch sortType[0] {
	case SortTypeByActivity:
		sort.Slice(result, func(i, j int) bool { return result[i].OfferData.Activity.Accept > result[j].OfferData.Activity.Accept })
	case SortTypeByPrice:
		sort.Slice(result, func(i, j int) bool { return result[i].Order.Tariff.TotalPrice > result[j].Order.Tariff.TotalPrice })
	}
	return result, nil
}

func SortMapSF64(arg map[string]float64) []float64 {
	var valueMass []float64
	for _, value := range arg {
		valueMass = append(valueMass, value)
	}
	sort.Slice(valueMass, func(i, j int) bool { return valueMass[i] < valueMass[j] })
	return valueMass
}

// checkSuitableOfferForDriverUUID проверяет подходит ли оффер водителю
func checkSuitableOfferForDriverUUID(offer OfferDrv, driverUUID string) error {
	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return fmt.Errorf("getting driver error,%s", err)
	}
	return checkSuitableOfferForDriver(offer, driver)
}
func paymentTypeSelected(neededType string, driverTypes []string) bool {
	for _, val := range driverTypes {
		if val == neededType {
			return true
		}
	}
	return false
}

func checkSuitableOfferForDriver(offer OfferDrv, driver DriversApps) error {
	var check bool

	service := offer.TaximetrData.Order.Service
	var availableServiceUUIDs []string
	err := tool.SendRequest(
		http.MethodGet,
		fmt.Sprintf("%s/driver/%s/active_services", config.St.CRM.Host, driver.UUID),
		nil,
		nil,
		&availableServiceUUIDs,
	)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "checkSuitableOfferForDriver",
			"driver_uuid": driver.UUID,
		}).WithError(err).Errorln(errpath.Err(err))
	}
	for _, uuid := range availableServiceUUIDs {
		if uuid == service.UUID {
			check = true
			break
		}
	}
	if !check {
		return fmt.Errorf("Сожалеем, но вам недоступен сервис %s", service.Name)
	}
	if !paymentTypeSelected(offer.TaximetrData.Order.PaymentType, driver.PaymentTypes) {
		return fmt.Errorf("Данный тип оплаты вам недоступен. Вы можете выбрать его в настройках")
	}

	var availableFeatureUUIDs []string
	err = tool.SendRequest(
		http.MethodGet,
		fmt.Sprintf("%s/driver/%s/active_features", config.St.CRM.Host, driver.UUID),
		nil,
		nil,
		&availableFeatureUUIDs,
	)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "checkSuitableOfferForDriver",
			"driver_uuid": driver.UUID,
		}).WithError(err).Errorln(errpath.Err(err))
	}
	// Driver must have all the specified features
	var unavailableOptionsNames []string
	for _, feature := range offer.TaximetrData.Order.Features {
		if !structures.StringInArray(feature.UUID, availableFeatureUUIDs) {
			unavailableOptionsNames = append(unavailableOptionsNames, feature.Name)
		}
	}
	if len(unavailableOptionsNames) != 0 {
		return fmt.Errorf("Сожалеем, но вам недоступны следующие опции: %s", strings.Join(unavailableOptionsNames, ", "))
	}

	errCannotAccept := errors.New("Сожалеем, но вы не можете принимать заказы от этого клиента")
	for _, driverUUID := range offer.TaximetrData.Order.Client.Blacklist {
		if driverUUID == driver.UUID {
			return errCannotAccept
		}
	}
	// Filter banned for a driver clients
	for _, b := range driver.Blacklist {
		if offer.TaximetrData.Order.GetClientPhone() == b { // possibly, match client uuid when all the client ids will be available
			return errCannotAccept
		}
	}
	return nil
}

// PrepareOfferForDistributionByUUID запускает алгоритм распределения для перерванного по uuid оффера
func PrepareOfferForDistributionByUUID(offerUUID string) (OfferDrv, error) {
	var offerDrv OfferDrv
	if err := db.Model(&offerDrv).Where("uuid = ?", offerUUID).Select(); err != nil {
		return offerDrv, err
	}
	newState := constants.OrderStateFree

	return offerDrv, PrepareOfferForDistribution(offerDrv, newState, "")
}

// PrepareOfferForDistribution запускает алгоритм распределения для переданного оффера
func PrepareOfferForDistribution(offer OfferDrv, distributionState, comment string) error {
	currentOrderState, err := GetOfferActualState(offer.UUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "distribution algorithm",
			"reason":    "Error getting offer current state",
			"offerUUID": offer.UUID,
			"orderUUID": offer.OrderUUID,
		}).Error(err)
		return err
	}

	if currentOrderState.State == distributionState {
		// no action required
		return nil
	}
	if comment == "" {
		comment = "Перевод в распределение"
	}
	// Init state machine, set the current status and try to change state
	orderState := OfferStatesDrv{
		OfferStates: structures.OfferStates{
			OfferUUID:  offer.UUID,
			DriverUUID: offer.DriverUUID,
			OrderUUID:  offer.OrderUUID,
			Comment:    comment,
		},
	}
	orderState.InitFsm()
	orderState.FSM.SetState(currentOrderState.State)

	if err := orderState.FSM.Event(distributionState); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "distribution algorithm",
			"reason":    "Error setting SmartDistribution state",
			"offerUUID": offer.UUID,
			"orderUUID": offer.OrderUUID,
		}).Error(err)
		return err
	}

	// Clear helper fields if needed
	if offer.TaximetrData.OfferData.Activity.Accept != 0 || offer.TaximetrData.OfferData.Activity.Reject != 0 {
		offer.TaximetrData.OfferData.ClearDriverAssignedFields()
		if err := UpdateOfferTaximeter(offer.TaximetrData); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "distribution algorithm",
				"offerUUID": offer.UUID,
			}).Error(err)
			// continue intentionally
		}
		if err := setCounterOrderMarker(offer.OrderUUID, false, true); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "distribution algorithm",
				"offerUUID": offer.UUID,
			}).WithError(err).Error("failed to clear counter order marker")
		}
	}

	return nil
}

//UpdateDistTime обновляет время распределения заказа
func (offer *OfferDrv) UpdateDistTimeAndAcceptedFromOfflineField(distTime time.Time) error {
	offer.TaximetrData.Order.CancelTime = distTime
	_, err := db.Model(offer).Where("uuid=?uuid").Update()
	if err != nil {
		return fmt.Errorf("error updating offer.order cancel time,%s", err)
	}
	_, err = db.Model(&OrderDrv{}).Where("uuid=?", offer.OrderUUID).Set("cancel_time = ? , accepted_from_offline = false", distTime).Update()
	if err != nil {
		return fmt.Errorf("error updating order cancel time,%s", err)
	}
	return nil
}

//Redistribute перераспределяет оффер, снимая его с текущего водителя
func (offer *OfferDrv) Redistribute(distData time.Time, reason string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "Redistribute offer",
		"offerUUID": offer.UUID,
		"orderUUID": offer.OrderUUID,
	})

	var offState OfferStatesDrv
	offState.InitFsm()
	offState.Comment = reason
	offState.FSM.SetState(constants.OrderStateCreated)
	realState, err := GetOfferActualState(offer.UUID)
	if err != nil {
		log.WithError(err).Error("Error get current offer state")
		return
	}

	newState := constants.OrderStateDistributing
	if realState.State == newState {
		msg := fmt.Sprintf("Order is already in %s status", newState)
		log.WithFields(logrus.Fields{
			"reason": "Redistribution is not required",
		}).Info(msg)
		return
	}

	offState.OfferUUID = offer.UUID
	offState.OrderUUID = offer.OrderUUID
	err = offer.UpdateDistTimeAndAcceptedFromOfflineField(distData)
	if err != nil {
		log.WithError(err).Error("Error updating dist time")
		return
	}
	offState.Comment = "Перевод после запроса о перераспределении заказа"
	err = offState.ApplyEventWithMutex(newState, true)
	if err != nil {
		log.WithError(err).Error("Error setting new order state")
		return
	}

	// Remember to update distribution metadata also to start the distribution process again
	offer.DistributionMeta = DistributionMeta{
		ByTaxiPark: offer.DistributionMeta.ByTaxiPark,
	}
	if err = offer.UpdateOfferDistributionMeta(); err != nil {
		log.WithError(err).Error("Error updating offer's meta")
		return
	}

	if realState.DriverUUID == "" ||
		realState.State == constants.OrderStateRejected ||
		realState.State == constants.OrderStateFinished {
		return
	}
	token, err := GetCurrentDriverFCMToken(realState.DriverUUID)
	if err != nil {
		log.WithError(err).Error("Error get current driver fcm token")
		return
	}

	msg := "Заказ отправлен оператором на перераспределение"
	alert := AlertsDRV{
		OrderUUID:  offer.OrderUUID,
		DriverUUID: realState.DriverUUID,
		Tag:        constants.FcmTagInitData,
		Message:    msg,
	}
	err = alert.Save()

	isCounter, err := getCounterOrderMarker(offer.OrderUUID)
	if err != nil {
		log.WithError(err).Error("failed to fetch counter order marker value")
		return
	}

	if isCounter {
		DriverSetStatus(realState.DriverUUID, constants.DriverStateWorking, "Перевод в воркинг после снятия заказа")
	} else {
		DriverSetStatus(realState.DriverUUID, constants.DriverStateOffline, "Перевод в оффлайн после снятия заказа")
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(offer.TaximetrData.Order.Driver.UUID, msg, token, constants.FcmTagInitData, 120)

	logs.Eloger.WithFields(logrus.Fields{
		"event":     "Redistribute offer",
		"offerUUID": offer.UUID,
		"orderUUID": offer.OrderUUID,
	}).Info("Complete")
}

//RedistributeAfterNoResponse перераспределяет оффер если нет ответа с водительского приложения
func (os OfferStatesDrv) RedistributeAfterNoResponse() {
	var ofSR OfferStateRequest
	ofSR.OfferUUID = os.OfferUUID
	ofSR.Msg = "Нет ответа от водительского приложения"
	ofSR.State = constants.OrderStateRejected
	err := OfferSetState(os.DriverUUID, ofSR)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handling check response",
			"reason":     "Error set offer state",
			"offerUUID":  os.OfferUUID,
			"orderUUID":  os.OrderUUID,
			"driverUUID": os.DriverUUID,
		}).Error(errpath.Err(err))
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling check response",
		"reason":     "no response from application",
		"offerUUID":  os.OfferUUID,
		"orderUUID":  os.OrderUUID,
		"driverUUID": os.DriverUUID,
	}).Warn("auto rejecting offer")

	// _, errorWL := DriverSetStatus(os.DriverUUID, variables.DriverStateOffline, "отсутствие ответа на предложенный оффер")

	state, err := GetCurrentDriverStateWithoutConsideringFromCRM(os.DriverUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handling check response",
			"reason":     "no response from application",
			"offerUUID":  os.OfferUUID,
			"orderUUID":  os.OrderUUID,
			"driverUUID": os.DriverUUID,
		}).Error(errpath.Err(err))
		return
	}

	var errorWL *structures.ErrorWithLevel
	if state == constants.DriverStateWorking {
		_, errorWL = DriverSetStatus(os.DriverUUID, constants.DriverStateWorking, "Перевод в воркинг после снятия заказа")
	} else {
		_, errorWL = DriverSetStatus(os.DriverUUID, constants.DriverStateOffline, "Перевод в оффлайн после снятия заказа")
	}
	if errorWL != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handling check response",
			"reason":     "Error set driver state",
			"offerUUID":  os.OfferUUID,
			"orderUUID":  os.OrderUUID,
			"driverUUID": os.DriverUUID,
		}).Error(errorWL.Error)
		return
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling check response",
		"reason":     "set driver state",
		"offerUUID":  os.OfferUUID,
		"orderUUID":  os.OrderUUID,
		"driverUUID": os.DriverUUID,
	}).Info("set new driver state - offline")
	return
}

//InformDriverAboutOrderUpdate godoc
func (offer *OfferDrv) InformDriverAboutOrderUpdate(body string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "inform driver about order update",
		"offer_uuid": offer.UUID,
		"order_uuid": offer.OrderUUID,
	})

	realState, err := GetOfferActualState(offer.UUID)
	if err != nil {
		log.WithField("reason", "Error get current offer state").Error(err)
		return
	}

	if realState.DriverUUID == "" || realState.State == constants.OrderStateRejected {
		return
	}

	token, err := GetCurrentDriverFCMToken(realState.DriverUUID)
	if err != nil {
		log.WithField("reason", "Error get current driver fcm token").Error(err)
		return
	}

	msg := "Данные заказа были изменены"
	payload := msg + ": " + body

	alert := AlertsDRV{
		DriverUUID: realState.DriverUUID,
		Tag:        constants.FcmTagInitData,
		Message:    msg,
		OrderUUID:  offer.OrderUUID,
	}
	if err := alert.Save(); err != nil {
		log.WithField("reason", "error saving alert").Error(err)
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(realState.DriverUUID, payload, token, constants.FcmTagInitData, 120, msg, body)
}

// TODO:
func (ld *locData) GetLastCoordinateByDriverUUID(driverUUID string) error {
	if driverUUID == "" {
		driverUUID = ld.DriverUUID
		if driverUUID == "" {
			return fmt.Errorf("driverUUID is empty")
		}
	}

	drvLoc, err := GetDriversLastLocations(driverUUID)
	if err != nil {
		return errors.Wrap(err, "location data not found")
	} else if drvLoc == nil {
		return errors.Errorf("no known last location for a driver %s", driverUUID)
	}

	check := false
	for _, data := range drvLoc {
		if data.DriverUUID == driverUUID {
			check = true
		}
	}
	if !check {
		return errors.New("location data not found in result array")
	}

	ld.DriverUUID = driverUUID
	ld.Latitude = drvLoc[0].Lat
	ld.Longitude = drvLoc[0].Long
	ld.CreatedAt = drvLoc[0].CreatedAt

	return nil
}
