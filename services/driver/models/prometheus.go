package models

import (
	"github.com/pkg/errors"
	promethGo "github.com/prometheus/client_golang/prometheus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
)

var (
	pt prometheusData
)

type prometheusData struct {
	prometh              *prometheus.Prometheus
	drvStatesCount       *promethGo.GaugeVec
	ordStatesCount       *promethGo.GaugeVec
	mutexCount           *promethGo.GaugeVec
	mutexFreeOffersCount *promethGo.GaugeVec
}

// InitPrometheus initialize Prometheues instance
func InitPrometheus(prometh *prometheus.Prometheus) error {
	//init gloabal var
	pt.prometh = prometh

	//setting up prometheusData metrics variables
	for _, metricDef := range prometh.MetricsList {
		switch metricDef.ID {
		case "feamDrvStates":
			pt.drvStatesCount = metricDef.MetricCollector.(*promethGo.GaugeVec)
		case "feamOrdStates":
			pt.ordStatesCount = metricDef.MetricCollector.(*promethGo.GaugeVec)
		case "feamMutexCount":
			pt.mutexCount = metricDef.MetricCollector.(*promethGo.GaugeVec)
		case "feamMutexFreeOffersCount":
			pt.mutexFreeOffersCount = metricDef.MetricCollector.(*promethGo.GaugeVec)
		}
	}

	if err := getDriverStatesCount(); err != nil {
		return errors.Wrap(err, "Error getting driver states count")
	}
	if err := getOrdersStatesCount(); err != nil {
		return errors.Wrap(err, "Error getting order states count")
	}

	return nil
}

// getInitStatesValues fill structure with drivers state data
func getDriverStatesCount() error {
	for _, drvStatus := range constants.ListDriverStates() {
		drivers, err := GetDriversUUIDsAndTransitionTimeByState(drvStatus)
		if err != nil {
			return errors.Wrapf(err, "Error getting list of drivers in state: %v", drvStatus)
		}
		pt.drvStatesCount.WithLabelValues(drvStatus).Set(float64(len(drivers)))
	}
	return nil
}

// getInitStatesValues fill structure with drivers state data
func getOrdersStatesCount() error {
	for _, ordStatus := range constants.ListOrderStates() {
		switch ordStatus {
		case constants.OrderStateDriverNotFound, constants.OrderStateFinished, constants.OrderStateCancelled:
			continue
		default:
			ordCount, err := GetOrderCountByState(ordStatus)
			if err != nil {
				return errors.Wrapf(err, "Error getting count of orders in state: %v", ordStatus)
			}
			pt.ordStatesCount.WithLabelValues(ordStatus).Set(float64(ordCount))
		}
	}
	return nil
}

func BusinessMetrics() []*prometheus.Metric {
	var (
		drvStatesCnt = &prometheus.Metric{
			ID:          "feamDrvStates",
			Name:        "driver_states_count",
			Description: "How many drivers in each state",
			Type:        "gauge_vec",
			Args:        []string{"status"}}

		ordStatesCnt = &prometheus.Metric{
			ID:          "feamOrdStates",
			Name:        "orders_states_count",
			Description: "How many orders in each state",
			Type:        "gauge_vec",
			Args:        []string{"status"}}

		mutexCount = &prometheus.Metric{
			ID:          "feamMutexCount",
			Name:        "driver_mutex_count",
			Description: "How long is mutex queue",
			Type:        "gauge_vec",
			Args:        []string{"hash"}}

		mutexFreeOffersCount = &prometheus.Metric{
			ID:          "feamMutexFreeOffersCount",
			Name:        "driver_mutex_free_count",
			Description: "How long is free offers mutex queue",
			Type:        "gauge_vec",
			Args:        []string{"hash"}}
	)
	return []*prometheus.Metric{
		drvStatesCnt,
		ordStatesCnt,
		mutexCount,
		mutexFreeOffersCount,
	}
}

func IncMutexCountMetric(label string) {
	if pt.mutexCount != nil {
		pt.mutexCount.WithLabelValues(label).Inc()
	}
}

func DecMutexCountMetric(label string) {
	if pt.mutexCount != nil {
		pt.mutexCount.WithLabelValues(label).Dec()
	}
}

func IncMutexFreeOffersCountMetric(label string) {
	if pt.mutexFreeOffersCount != nil {
		pt.mutexFreeOffersCount.WithLabelValues(label).Inc()
	}
}

func DecMutexFreeOffersCountMetric(label string) {
	if pt.mutexFreeOffersCount != nil {
		pt.mutexFreeOffersCount.WithLabelValues(label).Dec()
	}
}

func IncDriverStatesCountMetric(label string) {
	if pt.drvStatesCount != nil {
		pt.drvStatesCount.WithLabelValues(label).Inc()
	}
}

func DecDriverStatesCountMetric(label string) {
	if pt.drvStatesCount != nil {
		pt.drvStatesCount.WithLabelValues(label).Dec()
	}
}
