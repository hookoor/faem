package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// TaximetrData данные для таксометра
func TaximetrData(offer structures.Offer) (structures.OfferTaximetr, error) {
	var ot structures.OfferTaximetr

	// ot.OfferData = offer
	ot.OfferData.UUID = offer.UUID
	ot.OfferData.DriverUUID = offer.DriverUUID
	ot.OfferData.OrderUUID = offer.OrderUUID
	ot.OfferData.OfferType = offer.OfferType
	ot.OfferData.ResponseTime = offer.ResponseTime
	ot.OfferData.DriverID = offer.DriverID
	ot.OfferData.Comment = offer.Comment
	ot.OfferData.TripTime = offer.TripTime
	ot.Order = offer.Order

	// АЦКАЯ ЗАПЛАТКА
	wfreeTime := 999999
	wwTime := 0

	for k, v := range ot.Order.Tariff.WaitingBoarding {

		if k != 0 && k < wfreeTime {

			wfreeTime = k
			wwTime = int(v)
		}
	}

	pfreeTime := 999999
	pwTime := 0
	for k, v := range ot.Order.Tariff.WaitingPoint {
		if k != 0 && k < pfreeTime {

			pfreeTime = k
			pwTime = int(v)
		}
	}
	var tWaits = []structures.TaximeterWait{
		structures.TaximeterWait{
			FreeTime: wfreeTime * 60,
			Interval: 60,
			RateTime: wwTime,
		},
		{
			FreeTime: pfreeTime * 60,
			Interval: 60,
			RateTime: pwTime,
		}}

	var tDistance = []structures.TaximeterItem{
		structures.TaximeterItem{
			Section:  3000,
			Interval: 300,
			Rate:     20,
		},
		{
			Section:  1500,
			Interval: 300,
			Rate:     20,
		}}

	var tTime = []structures.TaximeterItem{
		structures.TaximeterItem{
			Section:  1800,
			Interval: 2,
			Rate:     5,
		},
		{
			Section:  1000,
			Interval: 2,
			Rate:     3,
		}}

	ot.TaxData.TaximeterWait = tWaits
	ot.TaxData.TaximeterDistance = tDistance
	ot.TaxData.TaximeterTime = tTime

	// msg := fmt.Sprintf("Driver ID=%s", ot.OfferData.DriverUUID)
	// logs.OutputOrderEvent("Taximetr data generated", msg, offer.UUID)

	return ot, nil
}
