package models

import (
	"time"
)

// ConfigDRV конфиги
type ConfigDRV struct {
	tableName     struct{}  `sql:"drv_config"`
	ID            int       `json:"-"`
	CreatedAt     time.Time `json:"-"`
	OperatorPhone string    `json:"operator_phone"`
}

// GetCurrentConfig return current config settings
func GetCurrentConfig() (ConfigDRV, error) {
	var (
		conf ConfigDRV
	)
	err := db.Model(&conf).
		Order("created_at DESC").
		Limit(1).
		Select()
	return conf, err
}

// ChangeCurrentConfig changes current config settings
func ChangeCurrentConfig(conf ConfigDRV) (ConfigDRV, error) {

	_, err := db.Model(&conf).
		Insert()
	return conf, err
}
