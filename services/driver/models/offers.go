package models

// В этом пекадже у нас должно быть только бизнес логика работы с оффера, сохранение а базу и все такое

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

type (
	DistributionMeta struct {
		Round         int     `json:"round"`
		SmartAttempts int     `json:"smart_attempts"`
		ByTaxiPark    bool    `json:"by_taxi_park"` //Is there a distribution by taxi fleet
		Score         float64 `json:"score" sql:"-"`
		State         string  `json:"-" sql:"-"`
	}

	// OfferDrv используется уже внутри DRV сервиса и хранит время создания,
	// принятия/отмены оффера и прочую информацию
	OfferDrv struct {
		tableName              struct{}         `sql:"drv_offers" pg:",discard_unknown_columns"`
		ID                     int              `json:"id" sql:",pk"`
		ValidTill              time.Time        `json:"valid_till"`
		AcceptedAt             time.Time        `json:"accepted_at"`
		CancelledAt            time.Time        `json:"cancelled_at"`
		CreatedAt              time.Time        `sql:"default:now()" json:"created_at"`
		DriverDistributionRule string           `json:"-" sql:",notnull"`
		ClientDistributionRule string           `json:"-" sql:",notnull"`
		DistributionMeta       DistributionMeta `json:"-"`
		//чтобы каждый раз не генерить через несколько функций данные таксометра, решил сразу при генерации оффера
		//сохранять их в базу вместе с ним
		structures.Offer
	}
)

var AllFreeOffers = make(map[int][]OfferDrv) //AllFreeOffers contains all offer in finding_driver state. ONLI FOR READ,DO NOT CHANGE THIS VARIABLE

const (
	tripTime                            = 200
	currentOrderEndpont          string = "/orders/current/driver/"
	getManyTaxiParksEndpoint     string = "/taxiparks/many"
	unDeliveredOrdersEndpoint    string = "/orders/undelivered/"
	changeCRMOrderTariffEndpoint string = "/ordertariff/"
	getTariff                    string = "/orders/tariff"
)

func (offer *OfferDrv) UpdateOfferDistributionMeta() error {
	_, err := db.Model(offer).
		Where("uuid = ?uuid").
		Set("distribution_meta = ?distribution_meta").
		Update()
	return err
}

// InitDB init db
func InitDB(conn *pg.DB) {
	db = conn
}

// SaveOrder saving order
func SaveOrder(crmOrder structures.Order) error {
	dur, _ := time.ParseDuration(config.St.Drivers.ClientCallPeriod)
	var drvOrder OrderDrv
	drvOrder.Order = crmOrder
	drvOrder.NextAutocallTime = time.Now().Add(dur)
	// drvOrder.UUID = crmOrder.UUID
	// drvOrder.Comment = crmOrder.Comment
	// drvOrder.Routes = crmOrder.Routes
	// drvOrder.Features = crmOrder.Features
	// drvOrder.Tariff = crmOrder.Tariff
	// drvOrder.Service = crmOrder.Service
	// drvOrder.Client = crmOrder.Client
	// drvOrder.CreatedAt = crmOrder.CreatedAt
	// drvOrder.CancelTime = crmOrder.CancelTime
	_, err := db.Model(&drvOrder).
		OnConflict("(uuid) DO UPDATE").
		Insert()
	if err != nil {
		return err
	}
	return nil
}

//GetCurrentOrderState возвращает текущий статус заказа
func GetCurrentOrderState(orderUUID string) (OfferStatesDrv, error) {
	var (
		ofState OfferStatesDrv
		err     error
	)

	err = db.Model(&ofState).
		Where("order_uuid = ?", orderUUID).
		Order("start_state DESC").
		Limit(1).Select()
	if err != nil {
		return ofState, err
	}

	return ofState, nil
}

// GenerateOffer generate empty order
func GenerateOffer(or structures.Order) (OfferDrv, error) {
	var offer OfferDrv
	guid, err := uuid.NewV4()
	if err != nil {
		return offer, err
	}
	offer.UUID = guid.String()
	offer.OfferType = "regular"
	offer.ResponseTime = time.Now().Unix() + config.St.Drivers.ResponseTime
	offer.TripTime = time.Now().Add(tripTime * time.Second).Unix()
	offer.OrderUUID = or.UUID
	offer.Order = or
	if or.IsDistributedByTaxiPark() {
		offer.DistributionMeta.ByTaxiPark = true
	}
	return offer, nil
}

func SetOfferAcceptedFromOffline(value bool, offerUUID string) error {
	var offer OfferDrv
	check, err := db.Model(&offer).Where("uuid = ?", offerUUID).
		Exists()
	if err != nil {
		return err
	}
	if !check {
		return fmt.Errorf("offer not exists")
	}
	_, err = db.Model(&offer).Where("uuid = ?", offerUUID).Set("accepted_from_offline = ?", value).
		Update()
	return err
}

//FillTaxiParksData godoc
func FillTaxiParksData(offers []OfferDrv) error {
	body := struct {
		UUID []string `json:"uuid"`
	}{}
	// to make it easier to find later
	uuidMap := make(map[string][]int)

	for i := range offers {
		tpUUID := offers[i].TaximetrData.Order.GetTaxiParkUUID()
		if tpUUID == "" {
			continue
		}
		uuidMap[tpUUID] = append(uuidMap[tpUUID], i)
		body.UUID = append(body.UUID, tpUUID)
	}
	if len(body.UUID) == 0 {
		return nil
	}
	var result []structures.TaxiPark
	url := config.St.CRM.Host + getManyTaxiParksEndpoint
	err := request(http.MethodGet, url, &body, &result)
	for _, val := range result {
		for _, idx := range uuidMap[val.UUID] {
			offers[idx].TaximetrData.Order.SetTaxiParkData(val)
		}

	}
	return err
}

func FillTaxiParksInOffers(offers []OfferDrv) error {
	body := struct {
		UUID []string `json:"uuid"`
	}{}
	// to make it easier to find later
	uuidMap := make(map[string][]int)

	for i := range offers {
		tpUUID := offers[i].Order.GetTaxiParkUUID()
		if tpUUID == "" {
			continue
		}
		uuidMap[tpUUID] = append(uuidMap[tpUUID], i)
		body.UUID = append(body.UUID, tpUUID)
	}
	if len(body.UUID) == 0 {
		return nil
	}
	var result []structures.TaxiPark
	url := config.St.CRM.Host + getManyTaxiParksEndpoint
	err := request(http.MethodGet, url, &body, &result)
	for _, val := range result {
		for _, idx := range uuidMap[val.UUID] {
			offers[idx].Order.SetTaxiParkData(val)
		}

	}
	return err
}

//FillOrderForOffers заполняет поле Orders. Чтобы соринг норм работал
func FillOrderForOffers(offers []OfferDrv) ([]OfferDrv, error) {
	var (
		ordersuuids []string
		orders      []OrderDrv
		newOffers   []OfferDrv
	)
	for _, offer := range offers {
		ordersuuids = append(ordersuuids, offer.OrderUUID)
	}

	err := db.Model(&orders).
		Where("uuid in (?)", pg.In(ordersuuids)).
		Select()
	if err != nil {
		return nil, err
	}
	for _, order := range orders {
		for _, offer := range offers {
			if order.UUID == offer.OrderUUID {
				offer.Order = order.Order
				newOffers = append(newOffers, offer)
			}
		}

	}
	return newOffers, nil
}

// FillOrderInOffers заполняет поле Order у всех офферов. Нужно, чтобы скоринг норм работал
func FillOrderInOffers(offers []OfferDrv) error {
	ordersUUIDs := make([]string, len(offers))
	for i := range ordersUUIDs {
		ordersUUIDs[i] = offers[i].OrderUUID
	}

	var orders []OrderDrv
	err := db.Model(&orders).
		WhereIn("uuid in (?)", ordersUUIDs).
		Select()
	if err != nil {
		return err
	}

	for _, order := range orders {
		for i := range offers {
			if offers[i].OrderUUID == order.UUID {
				offers[i].Order = order.Order
			}
		}
	}

	return nil
}

func GetCurrentDriverOrderDataFromCRM(driverUUID string) (structures.OrderUUIDWithStateTransferState, error) {
	var result structures.OrderUUIDWithStateTransferState
	url := config.St.CRM.Host + currentOrderEndpont + driverUUID
	err := request(http.MethodGet, url, nil, &result)
	return result, err
}

//GetCurrentDriverOrder возвращает текущий заказ водилы
func GetCurrentDriverOrder(driverUUID string) (structures.OfferTaximetr, string, error) {
	var (
		// offState OfferStatesDrv
		offer OfferDrv
	)
	orData, err := GetCurrentDriverOrderDataFromCRM(driverUUID)
	if err != nil {
		return structures.OfferTaximetr{}, "", fmt.Errorf("Error finding current driver order data from CRM, %s", err)
	}

	if orData.UUID == "" {
		return structures.OfferTaximetr{}, "", nil
	}
	// //ищется актуальный статус последнего заказа водителя
	// _, err = db.Query(&offState, `SELECT osf.state, osf.start_state, osf.offer_uuid, osf.driver_uuid from
	// ((SELECT
	// 	DISTINCT ON (offer_uuid) * FROM
	// 	 drv_offer_states order by offer_uuid,start_state desc) osf
	// 				   inner join
	//   	 (SELECT * FROM
	// 		drv_offer_states where driver_uuid = ? order by start_state desc limit 1) os
	// 			on osf.offer_uuid=os.offer_uuid)`, driverUUID)
	// if err != nil {
	// 	return structures.OfferTaximetr{}, "", fmt.Errorf("Error finding current driver offer state, %s", err)
	// }

	validResponseTime := orData.StateTransTime.Unix() + config.St.Drivers.ResponseTime
	expiryСheck := time.Now().Unix() >= validResponseTime-5
	if orData.State == constants.OrderStateOffered && expiryСheck {
		return structures.OfferTaximetr{}, "", nil
	}

	if orData.State != constants.OrderStateRejected &&
		orData.State != constants.OrderStateDistributing &&
		orData.State != constants.OrderStateFree {
		offer, err = GetOfferByOrderUUID(orData.UUID)
		if err != nil {
			return structures.OfferTaximetr{}, "", fmt.Errorf("Error finding current driver offer by order UUID, %s", err)
		}
		offer.TaximetrData.OfferData.ResponseTime = orData.StateTransTime.Unix() + config.St.Drivers.ResponseTime
		offer.TaximetrData.OfferData.CalcAndSetArrivalTime()
		return offer.TaximetrData, orData.State, nil
	}
	return structures.OfferTaximetr{}, "", nil
}

// GetCounterDriverOrderDataFromCRM -
func GetCounterDriverOrderDataFromCRM(driverUUID string) (structures.OrderUUIDWithStateTransferState, error) {
	var result structures.OrderUUIDWithStateTransferState
	url := config.St.CRM.Host + "/orders/counter/driver/" + driverUUID
	err := request(http.MethodGet, url, nil, &result)
	return result, err
}

// GetCounterDriverOrder - возвращает встречный принятый заказ водилы
func GetCounterDriverOrder(driverUUID string) (structures.OfferTaximetr, string, error) {
	var (
		// offState OfferStatesDrv
		offer OfferDrv
	)
	orData, err := GetCounterDriverOrderDataFromCRM(driverUUID)
	if err != nil {
		return structures.OfferTaximetr{}, "", errpath.Err(err)
	}

	if orData.UUID == "" {
		return structures.OfferTaximetr{}, "", nil
	}

	validResponseTime := orData.StateTransTime.Unix() + config.St.Drivers.ResponseTime + 10
	expiryСheck := time.Now().Unix() >= validResponseTime-5
	if orData.State == constants.OrderStateOffered && expiryСheck {
		return structures.OfferTaximetr{}, "", nil
	}

	if orData.State != constants.OrderStateRejected &&
		orData.State != constants.OrderStateDistributing &&
		orData.State != constants.OrderStateFree {

		offer, err = GetOfferByOrderUUID(orData.UUID)
		if err != nil {
			return structures.OfferTaximetr{}, "", errpath.Err(err, "Error finding counter driver offer by order UUID")
		}
		offer.TaximetrData.OfferData.ResponseTime = orData.StateTransTime.Unix() + config.St.Drivers.ResponseTime
		offer.TaximetrData.OfferData.CalcAndSetArrivalTime()
		return offer.TaximetrData, orData.State, nil
	}
	return structures.OfferTaximetr{}, "", nil
}

// CreateOffer creates an offer
func CreateOffer(od OfferDrv) (OfferDrv, error) {
	var (
		err     error
		orState OfferStatesDrv
	)
	od.ValidTill = time.Unix(od.ResponseTime, 0)

	od.TaximetrData, _ = TaximetrData(od.Offer)

	_, err = db.Model(&od).Returning("*").Insert()
	if err != nil {
		return OfferDrv{}, fmt.Errorf("error saving offer,%s", err)
	}
	orState.OfferUUID = od.UUID
	orState.OrderUUID = od.OrderUUID
	orState.StartState, orState.CreatedAt = time.Now(), time.Now()
	orState.State = constants.OrderStateCreated
	orState.Comment = "New order in driver"
	orState.UUID = structures.GenerateUUID()
	_, err = db.Model(&orState).Returning("*").Insert()
	if err != nil {
		return OfferDrv{}, fmt.Errorf("error saving offer state,%s", err)
	}
	return od, nil
}

// UpdateOffer updating offer
func UpdateOffer(of structures.Offer) (OfferDrv, error) {
	var od OfferDrv
	od.UUID = of.UUID
	od.DriverUUID = of.DriverUUID
	od.ValidTill = time.Unix(of.ResponseTime, 0)
	od.OfferType = of.OfferType
	od.ResponseTime = of.ResponseTime
	od.OrderUUID = of.OrderUUID
	_, err := db.Model(&od).Where("uuid=?uuid").Returning("*").UpdateNotNull()
	if err != nil {
		return OfferDrv{}, err
	}
	return od, nil
}

// UpdateFromBroker обновляет заказ и привязанный к нему оффер
func (or *OrderDrv) UpdateFromBroker(routingKey string) (OfferDrv, error) {
	var (
		err   error
		offer OfferDrv
	)
	query := db.Model(or).
		Where("uuid = ?", or.UUID).
		Returning("*")
	//обновляем сам заказ
	switch routingKey {
	case rabbit.UpdateOnlyCommentKey:
		_, err = query.
			Set("comment = ?", or.Comment).
			Update()
	case rabbit.UpdateIncreasedFareKey:
		_, err = query.
			Set("increased_fare = ?increased_fare").
			Set("tariff = ?tariff").
			Update()

	case rabbit.UpdateOrderProductData:
		_, err = query.
			Set("products_data = ?", or.GetProductsData).
			Update()

	case rabbit.UpdateOrderRoutesKey:
		_, err = query.
			Set("routes = ?routes").
			Set("tariff = ?tariff").
			Set("route_way_data = ?route_way_data").
			Update()

	default:
		_, err = db.Model(or).Where("uuid = ?", or.UUID).
			UpdateNotNull()
	}
	if err != nil {
		return offer, fmt.Errorf("error updating order,%s", err)
	}
	//ищем оффер, привязанный к заказу
	err = db.Model(&offer).Where("order_uuid = ?", or.UUID).Select()
	if err != nil {
		return offer, fmt.Errorf("offer search error,%s", err)
	}
	offerUUID := offer.UUID
	originalArrivalTime := offer.TaximetrData.OfferData.ArrivalTime
	//исходя из новых данных заказа, генерим новый оффер
	newOf, err := GenerateOffer(or.Order)
	if err != nil {
		return offer, fmt.Errorf("error generate offer,%s", err)
	}
	offer.Offer = newOf.Offer

	//чтобы uuid остался старый
	offer.UUID = offerUUID
	//генерим новые данные таксиметра
	offer.TaximetrData, err = TaximetrData(offer.Offer)
	if err != nil {
		return offer, fmt.Errorf("error generate taximetr data,%s", err)
	}
	// Keep the calculated field because of the immutable initial point
	offer.TaximetrData.OfferData.ArrivalTime = originalArrivalTime
	//обновляем уже оффер
	_, err = db.Model(&offer).Where("uuid = ?", offerUUID).UpdateNotNull()
	if err != nil {
		return offer, fmt.Errorf("error updating offer,%s", err)
	}
	return offer, nil
}

// UpdateRoutesAndTariff update routes, route_way_data and tariff
func (or *OrderDrv) UpdateRoutesAndTariff() error {
	_, err := db.Model(or).
		Where("uuid = ?", or.UUID).
		Set("routes = ?routes").
		Set("tariff = ?tariff").
		Set("route_way_data = ?route_way_data").
		Update()
	if err != nil {
		return err
	}
	return nil
}

// CalcOfferWaitingTime функция возвращает время ожидания при посадке и во время
// поездки, для переданного оффера
func CalcOfferWaitingTime(offerUUID string) (structures.Tariff, error) {
	var (
		offStates []OfferStatesDrv
		//ожидание при посадке и во время пути
		brdWaite, tripWaite int

		tripTime                             int64 // время выполнения заказа с момента on_place
		paidMinutesBoarding, paidMinutesTrip float32
		tariffItem                           structures.TariffItem
		offer                                OfferDrv
		tariff                               structures.Tariff
	)
	err := db.Model(&offer).
		Where("uuid = ?", offerUUID).
		Select()
	if err != nil {
		return structures.Tariff{}, fmt.Errorf("error findind offer,%s", err)
	}
	// если это доставка товаров то ожидание не должно учитыватья
	if offer.TaximetrData.Order.Service.ProductDelivery {
		return offer.TaximetrData.Order.Tariff, nil
	}
	err = db.Model(&offStates).
		Where("offer_uuid = ?", offerUUID).
		Order("start_state DESC").
		Select()
	if err != nil {
		return structures.Tariff{}, fmt.Errorf("error findind offerstates,%s", err)
	}
	var orderStartTime int64
	for k, v := range offStates {

		if k < len(offStates)-2 {
			if v.State == constants.OrderStateOnPlace {
				brdWaite = int(offStates[k-1].StartState.Unix() - v.StartState.Unix())
				tripTime = time.Now().Unix() - v.StartState.Unix()
				// offer.TaximetrData.Order.Tariff.OrderTripTime =
				// dist = логика для расчета пойденного расстояния
				orderStartTime = time.Now().Unix()
			}
			if v.State == constants.OrderStateWaiting {
				tripWaite += int(offStates[k-1].StartState.Unix() - v.StartState.Unix())
			}
		}
	}

	if offer.TaximetrData.Order.Tariff.TariffCalcType == structures.CalcWithTime {
		tariff, err = GetTariffFromCrm(offer, tripTime, 0)
		if err != nil {
			return tariff, errpath.Err(err, "get tariff from crm")
		}
	} else {
		tariff = offer.TaximetrData.Order.Tariff
	}
	tariff.OrderStartTime = orderStartTime

	//проверяем не был ли уже пересчитан тариф
	// for _, it := range tariff.Items {
	// 	if strings.Contains(it.Name, waitingOnPointPrefix) || strings.Contains(it.Name, waitingOnTheWayPrefix) {
	// 		return tariff, nil
	// 	}
	// }

	if len(offer.TaximetrData.TaxData.TaximeterWait) < 2 {
		return structures.Tariff{}, fmt.Errorf("incorrect taximeter wait. Requires two or more elements")
	}
	tariff = removeWaitingItems(tariff)
	if offer.TaximetrData.Order.Tariff.TariffCalcType != structures.CalcWithTime {

		brdInterval := offer.TaximetrData.TaxData.TaximeterWait[0].Interval
		brdRate := offer.TaximetrData.TaxData.TaximeterWait[0].RateTime
		brdFreeTime := offer.TaximetrData.TaxData.TaximeterWait[0].FreeTime

		tripInterval := offer.TaximetrData.TaxData.TaximeterWait[1].Interval
		tripRate := offer.TaximetrData.TaxData.TaximeterWait[1].RateTime
		tripFreeTime := offer.TaximetrData.TaxData.TaximeterWait[1].FreeTime

		paidMinutesBoarding = float32((brdWaite - brdFreeTime) / brdInterval)
		paidMinutesTrip = float32((tripWaite - tripFreeTime) / tripInterval)

		if paidMinutesBoarding > 0 {
			tariffItem.Name = fmt.Sprintf("%s, %vм", structures.WaitingOnPointPrefix, paidMinutesBoarding)
			tariffItem.Price = int(paidMinutesBoarding * float32(brdRate))
			tariff.WaitingPrice += tariffItem.Price
			tariff.TotalPrice += tariffItem.Price
			tariff.Items = append(tariff.Items, tariffItem)
		}
		if paidMinutesTrip > 0 {
			tariffItem.Name = fmt.Sprintf("%s, %vм", structures.WaitingOnTheWayPrefix, paidMinutesTrip)
			tariffItem.Price = int(paidMinutesTrip * float32(tripRate))
			tariff.WaitingPrice += tariffItem.Price
			tariff.TotalPrice += tariffItem.Price
			tariff.Items = append(tariff.Items, tariffItem)
		}
		if tariff.WaitingPrice == 0 {
			return tariff, nil
		}
	}
	// tariffItem.Name = fmt.Sprintf("Доп. услуги")
	// tariffItem.Price = 0
	// tariff.TotalPrice += tariffItem.Price
	// tariff.Items = append(tariff.Items, tariffItem)

	//пересчитываем надбавку
	supp := tariff.GuaranteedDriverIncome - (tariff.TotalPrice - tariff.InsuranceCost)
	if supp < 0 {
		supp = 0
	}
	tariff.SupplementToGuaranteedIncome = supp
	err = offer.updateOrderTariff(tariff)
	if err != nil {
		return structures.Tariff{}, fmt.Errorf("Error updateOrderTariff [%s]", err)
	}

	err = rabsender.SendJSONByAction(rabsender.AcOrderDataUpdateByTariff, "", offer.TaximetrData.Order)
	if err != nil {
		return structures.Tariff{}, fmt.Errorf("Error SendJSONByAction[%s]", err)
	}

	return tariff, nil
}
func removeWaitingItems(tariff structures.Tariff) structures.Tariff {
	newTariff := tariff
	newTariff.Items = nil
	for _, it := range tariff.Items {
		if strings.Contains(it.Name, structures.WaitingOnTheWayPrefix) ||
			strings.Contains(it.Name, structures.WaitingOnPointPrefix) {
			continue
		}
		newTariff.Items = append(newTariff.Items, it)
	}
	newTariff.TotalPrice -= tariff.WaitingPrice
	newTariff.WaitingPrice = 0
	return newTariff
}

// SetOfferOnPlaceTime -
func SetOfferOnPlaceTime(offerUUID string) error {
	var (
		offer OfferDrv
	)

	err := db.Model(&offer).
		Where("uuid = ?", offerUUID).
		Select()
	if err != nil {
		return fmt.Errorf("error findind offer,%s", err)
	}

	orderStartTime := time.Now().Unix()
	offer.TaximetrData.Order.Tariff.OrderStartTime = orderStartTime

	if len(offer.TaximetrData.TaxData.TaximeterWait) < 2 {
		return errpath.Errorf("incorrect taximeter wait. Requires two or more elements")
	}

	err = offer.updateOrderTariff(offer.TaximetrData.Order.Tariff)
	if err != nil {
		return errpath.Err(err)
	}
	//закомментил потому что тариф и так менется в црмке через rest запрос
	// err = rabsender.SendJSONByAction(rabsender.AcOrderDataUpdateByTariff, "", offer.TaximetrData.Order)
	// if err != nil {
	// 	return errpath.Err(err)
	// }

	return nil
}

// GetTariffFromCrm -
func GetTariffFromCrm(offer OfferDrv, tripTime int64, dist int) (structures.Tariff, error) {
	var tariff structures.Tariff
	var body structures.Order

	body = offer.TaximetrData.Order
	body.Tariff.OrderTripTime = tripTime
	body.Tariff.OrderCompleateDist = float64(dist)

	request(http.MethodPost, config.St.CRM.Host+getTariff, body, &tariff)

	return tariff, nil
}

// UpdateOrderTariffInCRM -
func UpdateOrderTariffInCRM(orderUUID string, tariff structures.Tariff) error {
	return request(http.MethodPut, config.St.CRM.Host+changeCRMOrderTariffEndpoint+orderUUID, tariff, nil)
}

// CalcBoardingFreeTime - функция возвращает бесплатное время ожидания при посадке, для переданного оффера
func CalcBoardingFreeTime(offerUUID string) (int, error) {
	var (
		offDuration OfferStatesDrv
		offStates   []OfferStatesDrv
		brdWaite    int
		offer       OfferDrv
	)
	err := db.Model(&offer).
		Where("uuid = ?", offerUUID).
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offer,%s", err)
	}

	err = db.Model(&offDuration).
		Where("offer_uuid = ?", offerUUID).
		Where("state = ?", constants.OrderStateAccepted).
		Order("start_state DESC").
		Limit(1).
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offerstates,%s", err)
	}

	bound := offDuration.StartState
	err = db.Model(&offStates).
		Where("offer_uuid = ?", offerUUID).
		Where("start_state > ?", bound).
		Order("start_state DESC").
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offerstates,%s", err)
	}

	for i := 1; i < len(offStates); i++ {
		if offStates[i].State == constants.OrderStateOnPlace {
			brdWaite = int(offStates[i-1].CreatedAt.Unix() - offStates[i].CreatedAt.Unix())
		}
	}
	if offStates[0].State == constants.OrderStateOnPlace {
		brdWaite += int(time.Now().Unix() - offStates[0].CreatedAt.Unix())
	}

	brdFreeTime := offer.TaximetrData.TaxData.TaximeterWait[0].FreeTime
	resBrdWaite := brdFreeTime - brdWaite

	return resBrdWaite, nil
}

// CalcWaitingFreeTime - функция возвращает бесплатное время ожидания во время поездки, для переданного оффера
func CalcWaitingFreeTime(offerUUID string) (int, error) {
	var (
		offDuration OfferStatesDrv
		offStates   []OfferStatesDrv
		tripWaite   int
		offer       OfferDrv
	)
	err := db.Model(&offer).
		Where("uuid = ?", offerUUID).
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offer,%s", err)
	}

	if offer.Order.Tariff.TariffCalcType == structures.CalcWithTime {
		return 0, nil
	}

	err = db.Model(&offDuration).
		Where("offer_uuid = ?", offerUUID).
		Where("state = ?", "offer_accepted").
		Order("start_state DESC").
		Limit(1).
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offerstates,%s", err)
	}

	bound := offDuration.CreatedAt
	err = db.Model(&offStates).
		Where("offer_uuid = ?", offerUUID).
		Where("start_state > ?", bound).
		Order("start_state DESC").
		Select()
	if err != nil {
		return -1, fmt.Errorf("error findind offerstates,%s", err)
	}

	for i := 1; i < len(offStates); i++ {
		if offStates[i].State == constants.OrderStateWaiting {
			tripWaite += int(offStates[i-1].CreatedAt.Unix() - offStates[i].CreatedAt.Unix())
		}
	}
	if offStates[0].State == constants.OrderStateWaiting {
		tripWaite += int(time.Now().Unix() - offStates[0].CreatedAt.Unix())
	}

	tripFreeTime := offer.TaximetrData.TaxData.TaximeterWait[1].FreeTime
	resTripWaite := tripFreeTime - tripWaite

	return resTripWaite, nil
}

func (offer *OfferDrv) updateOrderTariff(tariff structures.Tariff) error {
	offer.Order.Tariff = tariff
	offer.TaximetrData.Order.Tariff = tariff

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	errG, ctx := errgroup.WithContext(ctx)
	updateOfferTariff := func() error {
		_, err := db.ModelContext(ctx, offer).Where("uuid = ?", offer.UUID).Update()
		if err != nil {
			return fmt.Errorf("error updating offer.order tariff,%s", err)
		}
		return nil
	}
	updateOrerTariff := func() error {
		_, err := db.ModelContext(ctx, &OrderDrv{}).Where("uuid = ?", offer.OrderUUID).Set("tariff = ?", tariff).Update()
		if err != nil {
			return fmt.Errorf("error updating order tariff,%s", err)
		}
		return nil
	}
	updateOrerTariffInCRM := func() error {
		return UpdateOrderTariffInCRM(offer.OrderUUID, tariff)
	}
	errG.Go(updateOfferTariff)
	errG.Go(updateOrerTariff)
	errG.Go(updateOrerTariffInCRM)
	if err := errG.Wait(); err != nil {
		return err
	}
	return nil
}

func GetDistanceToTarget(driverUUID string, offerUUID string, target string) (int, error) {
	var (
		err       error
		instOffer OfferDrv
		route     []structures.Coordinates
	)

	locs, err := GetDriversLastLocations(driverUUID)
	if err != nil {
		return -1, fmt.Errorf("Error GetDistanceToTarget [%s]", err)
	}
	if len(locs) < 1 {
		return -1, fmt.Errorf("can't find any driver locations by uuid %s", driverUUID)
	}
	// Take the first one, because we have provided only one driver uuid
	instDriverLoc := locData{
		Latitude:  locs[0].Lat,
		Longitude: locs[0].Long,
	}

	err = db.Model(&instOffer).
		Where("uuid = ?", offerUUID).
		Order("id DESC").
		Limit(1).
		Select()
	if err != nil {
		return -1, fmt.Errorf("Error GetDistanceToTarget [%s]", err)
	}

	route = append(route, structures.Coordinates{Lat: instDriverLoc.Latitude, Long: instDriverLoc.Longitude})
	if target == constants.OrderStateOnPlace {
		route = append(route, structures.Coordinates{Lat: float64(instOffer.TaximetrData.Order.Routes[0].Lat), Long: float64(instOffer.TaximetrData.Order.Routes[0].Lon)})
		osrm, err := GetItinerary(route)
		if err != nil {
			return -1, errpath.Err(err, "ошибка расчета дистанции")
		}
		return int(osrm.Routes[0].Distance), nil
	}

	if target == constants.OrderStatePayment {
		route = append(route, structures.Coordinates{
			Lat:  float64(instOffer.TaximetrData.Order.Routes[len(instOffer.TaximetrData.Order.Routes)-1].Lat),
			Long: float64(instOffer.TaximetrData.Order.Routes[len(instOffer.TaximetrData.Order.Routes)-1].Lon)},
		)
		osrm, err := GetItinerary(route)
		if err != nil {
			return -1, errpath.Err(err, "ошибка расчета дистанции")
		}
		return int(osrm.Routes[0].Distance), nil
	}
	return -1, fmt.Errorf("Error calculate distance [target not found]")
}

// GetItinerary - получение данных с помощью OSRM по координатам
// meta: устаревшая реализация. Используйте "gitlab.com/faemproject/backend/faem/pkg/structures/osrm"
func GetItinerary(coordinates []structures.Coordinates) (structures.OSRMData, error) {
	var (
		reqString string
		res       structures.OSRMData
	)

	client := &http.Client{}
	reqString += config.St.OSRM.Host
	reqString += "/route/v1/driving/"

	for i, inst := range coordinates {
		reqString += strconv.FormatFloat(inst.Long, 'f', 6, 64) + "," + strconv.FormatFloat(float64(inst.Lat), 'f', 6, 64)
		if i != len(coordinates)-1 {
			reqString += ";"
		}
	}

	reqString += "?geometries=geojson"

	req, err := http.NewRequest(
		"GET",
		reqString,
		nil,
	)
	if err != nil {
		fmt.Println(err)
		return structures.OSRMData{}, err
	}
	//req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return structures.OSRMData{}, err
	}
	defer resp.Body.Close()
	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(&res)
	if err != nil {
		return structures.OSRMData{}, fmt.Errorf("ошибка декодинга ответа с OSRM,%s", err)
	}
	if res.Code != "Ok" {
		return structures.OSRMData{}, errpath.Errorf("статус не ОК")
	}

	return res, nil
}

// LastSendDistance - TODO: descr
type LastDistance struct {
	tableName        struct{}  `sql:"drv_last_distance"`
	ID               int       `sql:"id"`
	DriverUUID       string    `sql:"driver_uuid"`
	DistanceToTarget int       `sql:"distance_to_target"`
	Step             int       `sql:"step"` // промежуток пути
	CreatedAt        time.Time `sql:"created_at,default:now()"`
}

func (lastCoord *LastDistance) SetDriverLastDistance() error {
	_, err := db.Model(lastCoord).Insert()
	if err != nil {
		return fmt.Errorf("error insert last coordinates to database. %s ", err)
	}
	return nil
}
func (lastCoord *LastDistance) GetDriverLastDistance(driverUUID string) (int, error) {
	err := db.Model(lastCoord).
		Where("driver_uuid = ?", driverUUID).
		Order("id DESC").
		Limit(1).
		Select()
	if err != nil {
		return -1, fmt.Errorf("Error GetDistanceToTarget [%s]", err)
	}
	return 0, nil
}
func GetOffersByOrdersUUID(ordersUUID []string) ([]OfferDrv, error) {
	if len(ordersUUID) < 1 {
		return nil, nil
	}
	var offers []OfferDrv

	err := db.Model(&offers).
		WhereIn("order_uuid in (?)", ordersUUID).
		Select()

	return offers, err
}
func GetOfferByOrderUUID(orderUUID string) (OfferDrv, error) {
	var offer OfferDrv
	err := db.Model(&offer).
		Where("order_uuid = ?", orderUUID).
		Select()
	return offer, err
}

func UpdateOfferTaximeter(tax structures.OfferTaximetr) error {
	_, err := db.Model((*OfferDrv)(nil)).
		Where("uuid = ?", tax.OfferData.UUID).
		Set("taximetr_data = ?", tax).
		Update()
	return errors.Wrap(err, "failed to update taximeter data")
}

func CalcRouteToClient(orderUUID, driverUUID string, isCounter bool) (structures.RouteToDriverJson, error) {
	order, err := OrderDrv{}.GetOrderByUUID(orderUUID)
	if err != nil {
		return structures.RouteToDriverJson{}, errors.Wrap(err, "failed to get an order by uuid")
	}

	var driverLocation locData
	if err = driverLocation.GetLastCoordinateByDriverUUID(driverUUID); err != nil {
		return structures.RouteToDriverJson{}, errors.Wrap(err, "failed to get last driver location")
	}

	coords := []structures.PureCoordinates{
		{Lat: driverLocation.Latitude, Long: driverLocation.Longitude},
	}
	if isCounter {
		// встречное назначение
		currentOffer, _, err := GetCurrentDriverOrder(driverUUID)
		if err != nil {
			return structures.RouteToDriverJson{}, errors.Wrap(err, "failed to get current order")
		}
		if len(currentOffer.Order.Routes) > 1 {
			// если у водителя есть текущий заказ и в нем есть конечная точка, то в пути между координатами водителя
			// и началом нового заказа должна быть конечная точка текущего заказа
			lastIndex := len(currentOffer.Order.Routes) - 1
			coords = append(coords, structures.PureCoordinates{
				Lat:  float64(currentOffer.Order.Routes[lastIndex].Lat),
				Long: float64(currentOffer.Order.Routes[lastIndex].Lon),
			})
		}
	}
	coords = append(coords, structures.PureCoordinates{
		Lat:  float64(order.Routes[0].Lat),
		Long: float64(order.Routes[0].Lon),
	})

	osrmData, err := osrm.New(config.St.OSRM.Host).Route(coords)
	if err != nil {
		return structures.RouteToDriverJson{}, errors.Wrap(err, "failed to get osrm data")
	}

	return structures.RouteToDriverJson{
		Way: structures.GeometryToDriverJson{
			Coor: osrmData.Routes[0].Geometry.Coordinates,
			Type: osrmData.Routes[0].Geometry.Type,
		},
		Type: "Feature",
		Proper: structures.PropertiesToDriverJson{
			Duration: int(osrmData.Routes[0].Duration),
			Distance: osrmData.Routes[0].Distance,
		},
	}, nil
}

func getFreeOrderForDriver(driverUUUID, offerUUID string) (OfferDrv, error) {
	offer, err := GetOfferByUUID(offerUUID)
	if err != nil {
		return OfferDrv{}, fmt.Errorf("error finding offer  by uuid,%s", err)
	}
	err = checkSuitableOfferForDriverUUID(offer, driverUUUID)
	if err != nil {
		return OfferDrv{}, err
	}
	offer.DriverUUID = driverUUUID
	freeOfferUUIDS, err := GetAllFreeOffersUUID()
	if err != nil {
		return OfferDrv{}, fmt.Errorf("error finding free offers uuids,%s", err)
	}

	isStillFree := false
	for _, freeOffUUID := range freeOfferUUIDS {
		if freeOffUUID == offerUUID {
			isStillFree = true
			break
		}
	}
	if !isStillFree {
		return OfferDrv{}, errors.New("Заказ неактуален - его взял ваш коллега пару секунд назад")
	}
	return offer, nil
}

func RollBackUnDeliveredOrders(orderAndDriverUUID map[string]string) error {
	if len(orderAndDriverUUID) < 1 {
		return nil
	}
	msg := "Заказ не был доставлен водителю"
	//get keys(ordersUUID) from map
	offers, err := GetOffersByOrdersUUID(GetKeys(orderAndDriverUUID))
	if err != nil {
		return fmt.Errorf("error getting offers, %s", err)
	}
	for _, of := range offers {
		err = PrepareOfferForDistribution(of, constants.OrderStateDistributing, msg)
		if err != nil {
			return fmt.Errorf("error setting offer state. OfferUUID = (%s), orderUUID = (%s), %s", of.UUID, of.OrderUUID, err)
		}
		driverUUID := orderAndDriverUUID[of.OrderUUID]

		NextDriverStatus(driverUUID, msg, StatusEventOrderUndelivered)
	}
	return nil
}

type DriverStatusEvent int

const (
	StatusEventOrderUndelivered DriverStatusEvent = iota
	StatusEventOrderFinished
	StatusEventOrderCancelled
	StatusEventOfferRejected
)

func NextDriverStatus(driverUUID, msg string, event DriverStatusEvent) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "NextDriverStatus",
		"driver_uuid": driverUUID,
	})

	currentState, err := GetCurrentDriverState(driverUUID)
	if err != nil {
		log.WithError(err).Error("failed to fetch driver state")
		return
	}

	currentOrder, err := GetCurrentDriverOrderDataFromCRM(driverUUID)
	if err != nil {
		log.WithError(err).Error("failed to fetch current order")
		return
	}
	currentOrderExists := currentOrder.UUID != ""

	counterOrder, err := GetCounterDriverOrderDataFromCRM(driverUUID)
	if err != nil {
		log.WithError(err).Error("failed to fetch counter order")
		return
	}
	counterOrderExists := counterOrder.UUID != ""

	log = log.WithField("current_state", currentState.State)
	currentStateMatchesExpected := func(expected string) bool {
		if currentState.State == expected {
			return true
		}
		log.Warn("current state doesn't match expected")
		return false
	}

	var newState string
	switch event {
	case StatusEventOrderUndelivered:
		if !currentStateMatchesExpected(constants.DriverStateConsidering) {
			return
		}
		fallthrough
	case StatusEventOrderCancelled:
		newState = constants.DriverStateOffline
		if currentOrderExists || counterOrderExists {
			// если есть активный заказ -- нельзя переводить в оффлайн
			newState = constants.DriverStateWorking
		}
	case StatusEventOrderFinished, StatusEventOfferRejected:
		newState = constants.DriverStateOnline
		if currentOrderExists || counterOrderExists {
			// если есть активный заказ -- нельзя переводить в онлайн
			newState = constants.DriverStateWorking
		}
	}

	_, errWL := DriverSetStatus(driverUUID, newState, msg)
	if errWL != nil {
		errWL.Print(log)
		return
	}
	log.WithField("new_state", newState).Info("changed driver state")
}

func GetUndeliveredOrdersFromCRM() (map[string]string, error) {
	result := make(map[string]string)
	url := config.St.CRM.Host + unDeliveredOrdersEndpoint
	err := request(http.MethodGet, url, nil, &result)
	return result, err
}
