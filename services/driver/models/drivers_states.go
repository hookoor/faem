package models

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"

	"github.com/looplab/fsm"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	BlockingActivityMarker   = "низкое значение активности"
	BlockingRejectionsMarker = "отказов на"
)

// DriverStatesDrv setting driver apps status
type DriverStatesDrv struct {
	tableName struct{} `sql:"drv_states"`

	ID int `json:"id" sql:",pk"`
	// DriverID  int       `json:"device_id" description:"Login" required:"true"`
	FSM       *fsm.FSM `sql:"-"`
	prevState string   `sql:"-"` // предыдущий статус, не экспортируемый
	structures.DriverStates
}

type DriverStateNotify struct {
	Value   string `json:"value"`
	Message string `json:"message"`
}

func driverExists(driverUUID string) bool {
	var driver DriversApps
	_, err := db.Model(&driver).Where("uuid = ?", driverUUID).Exists()
	if err != nil {
		return false
	}
	return true
}

// DriverSetStatus для проверки статуса самого водителя
func DriverSetStatus(driverUUID, state, comment string) (DriverStatesDrv, *structures.ErrorWithLevel) {

	var appState DriverStatesDrv
	appState.initFsm()
	appState.Comment = comment
	appState.DriverUUID = driverUUID
	appState.CreatedAt = time.Now()
	// Ищем текущий статус водителя
	actualState, err := GetCurrentDriverStateFromCRM(driverUUID)
	if err != nil {
		return DriverStatesDrv{}, structures.Error(fmt.Errorf("Not found previous status for driver. %s", err))
	}
	if actualState == "" {
		logs.OutputError(fmt.Sprintf("Last Driver state is empty for UUID=%v", driverUUID))
	}
	appState.prevState = actualState
	appState.FSM.SetState(actualState)

	// Проверка на блокировку или нахождение на модерации
	if (actualState == constants.DriverStateOnModeration) ||
		(actualState == constants.DriverStateBlocked) ||
		(actualState == constants.DriverStateOnModerationAfterChange) {
		return DriverStatesDrv{}, structures.Warning(fmt.Errorf("Ошибка. Вы в статусе %s", constants.DriverStateRU[actualState]))
	}

	if actualState == state {
		return appState, nil
	}
	err = appState.FSM.Event(state)
	if err != nil {
		es := fmt.Sprintf("Driver state transition failed. DriverUUID=%s, currentState=%s, newState=%s, %s", driverUUID, appState.State, state, err)
		return DriverStatesDrv{}, structures.Warning(fmt.Errorf(es))
	}
	return appState, nil
}

// SetDriverStatus для системного перевода статуса
func SetDriverStatus(driverUUID, state, comment string) (DriverStatesDrv, error) {

	if !driverExists(driverUUID) {
		return DriverStatesDrv{}, fmt.Errorf("driver not found. UUID=%s", driverUUID)
	}

	var appState DriverStatesDrv
	appState.initFsm()
	appState.Comment = comment
	appState.DriverUUID = driverUUID
	appState.CreatedAt = time.Now()
	// Ищем текущий статус водителя
	actualState, err := GetCurrentDriverStateFromCRM(driverUUID)
	if err != nil || actualState == "" {
		appState.State = constants.DriverStateCreated
	} else {
		appState.State = actualState
	}
	appState.FSM.SetState(appState.State)

	err = appState.FSM.Event(state)
	if err != nil {
		es := fmt.Sprintf("Driver state transition failed. DriverUUID=%s, currentState=%s, newState=%s, %s", driverUUID, appState.State, state, err)
		return DriverStatesDrv{}, fmt.Errorf(es)
	}
	return appState, nil
}

func (dsd *DriverStatesDrv) initFsm() {
	dsd.FSM = fsm.NewFSM(
		constants.DriverStateCreated,
		fsm.Events{
			{
				Name: constants.DriverStateAvailable,
				Src: []string{
					constants.DriverStateCreated,
					constants.DriverStateBlocked,
					constants.DriverStateAvailable,
				},
				Dst: constants.DriverStateAvailable,
			},
			{
				Name: constants.DriverStateOnModerationAfterChange,
				Src: []string{
					constants.DriverStateCreated,
					constants.DriverStateOnModeration,
					constants.DriverStateOnline,
					constants.DriverStateAvailable,
					constants.DriverStateWorking,
					constants.DriverStateBlocked,
					constants.DriverStateOffline,
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateOnModerationAfterChange,
			},
			{
				Name: constants.DriverStateOnModeration,
				Src: []string{
					constants.DriverStateCreated,
					constants.DriverStateBlocked,
					constants.DriverStateOnModeration,
				},
				Dst: constants.DriverStateOnModeration,
			},
			{
				Name: constants.DriverStateBlocked,
				Src: []string{
					constants.DriverStateCreated,
					constants.DriverStateOnModeration,
					constants.DriverStateOnline,
					constants.DriverStateAvailable,
					constants.DriverStateWorking,
					constants.DriverStateOnModerationAfterChange,
					constants.DriverStateBlocked,
					constants.DriverStateOffline,
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateBlocked,
			},
			{
				Name: constants.DriverStateOnline,
				Src: []string{
					constants.DriverStateAvailable,
					constants.DriverStateOffline,
					constants.DriverStateWorking,
					constants.DriverStateOnline,
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateOnline,
			},
			{
				Name: constants.DriverStateOffline,
				Src: []string{
					constants.DriverStateAvailable,
					constants.DriverStateOnline,
					constants.DriverStateWorking,
					constants.DriverStateAvailable,
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateOffline,
			},
			{
				Name: constants.DriverStateWorking,
				Src: []string{
					constants.DriverStateAvailable,
					constants.DriverStateOnline,  // возможно это стоит убрать если ничего не сломается
					constants.DriverStateOffline, // возможно это стоит убрать если ничего не сломается
					constants.DriverStateWorking, // возможно это стоит убрать если ничего не сломается
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateWorking,
			},
			{
				Name: constants.DriverStateConsidering,
				Src: []string{
					constants.DriverStateAvailable,
					constants.DriverStateOnline,
					constants.DriverStateOffline,
					constants.DriverStateWorking,
					constants.DriverStateConsidering,
				},
				Dst: constants.DriverStateConsidering,
			},
		},
		fsm.Callbacks{
			"enter_state": func(e *fsm.Event) {
				dsd.saveState(e)
				// Добавляем и соостветсвенно уменьшаем значение счетчиков
				if dsd.State != "" {
					IncDriverStatesCountMetric(dsd.State)
				}
			},
			"leave_state": func(e *fsm.Event) {
				cur := e.FSM.Current()
				if cur != "" {
					DecDriverStatesCountMetric(cur)
				}
			},
			fmt.Sprintf("after_%s", constants.DriverStateAvailable): func(e *fsm.Event) {
				newState := DriverStatesDrv{DriverStates: dsd.DriverStates}
				newState.Comment = "Перевод после разблокировки"
				newState.initFsm()
				newState.CreatedAt = time.Now()
				newState.FSM.SetState(constants.DriverStateAvailable)

				// If previous state was "working" then return the driver back to that status
				// previousState, err := getPreviousDriverState(dsd.DriverUUID)
				// if err != nil {
				// 	logs.Eloger.WithField("event", "after_available driver state callback").Error(err)
				// 	newState.FSM.Event(variables.DriverStateOffline) // fallback state
				// 	return
				// }

				newStateValue := constants.DriverStateOffline
				// if previousState.State == variables.DriverStateWorking {
				// 	newStateValue = variables.DriverStateWorking
				// }

				newState.FSM.Event(newStateValue)
			},
			// перенес эту строчку отсюда в контроллер, ибо он ругался на циклческие зависимости. По моему идея с разбивкой логики по разным пакетам
			// для разных сущностей была такой себе идеей
			// fmt.Sprintf("after_%s", variables.DriverStateOnline): func(e *fsm.Event) { offers.DistributeFreeOffer(dsd.DriverUUID) },
		},
	)
}

func (dsd *DriverStatesDrv) suitableForBlockByControl() bool {
	if dsd.State == constants.DriverStateBlocked ||
		dsd.State == constants.DriverStateOnModerationAfterChange ||
		dsd.State == constants.DriverStateOnModeration {
		return false
	}

	return true
}
func (dsd *DriverStatesDrv) saveState(e *fsm.Event) {
	dsd.State = e.Dst
	_, err := db.Model(dsd).Insert()
	if err != nil {
		msg := fmt.Sprintf("Error saving driver state. DriverUUID=%s, New state=%s. %s", dsd.DriverUUID, e.Dst, err)
		logs.OutputError(msg)
	}

	// // Отправляем в rabbit новый статус
	bds := dsd.basicDriverState()
	key := fmt.Sprintf("%s.%s", rabbit.StateKey, e.Dst)
	err = rabsender.SendJSONByAction(rabsender.AcChangeDriverState, key, bds)
	if err != nil {
		msg := fmt.Sprintf("Error sending driver state to rabbit. DriverUUID=%s, New state=%s. %s", dsd.DriverUUID, e.Dst, err)
		logs.OutputError(msg)
	}
}

// SaveDriverStateFromCRM godoc
func SaveDriverStateFromCRM(drvState structures.DriverStates) (DriverStatesDrv, error) {
	var dsd DriverStatesDrv
	dsd.initFsm()
	err := db.Model(&dsd).
		Column("state").
		Where("driver_uuid = ?", drvState.DriverUUID).
		Order("created_at DESC").
		Limit(1).Select()
	if err != nil {
		dsd.FSM.SetState(constants.DriverStateCreated)
		dsd.State = constants.DriverStateCreated
	}
	dsd.DriverUUID = drvState.DriverUUID
	dsd.Comment = drvState.Comment
	dsd.CreatedAt = drvState.CreatedAt

	if dsd.FSM.Can(drvState.State) {
		dsd.FSM.SetState(drvState.State)
		dsd.State = drvState.State
		_, err := db.Model(&dsd).Insert()
		if err != nil {
			return dsd, err
		}
		return dsd, nil
	}
	bds := dsd.basicDriverState()
	key := fmt.Sprintf("%s.%s", rabbit.StateKey, bds.State)
	es := fmt.Sprintf("Driver state transition failed. DriverUUID=%s, currentState=%s, newState=%s", dsd.DriverUUID, dsd.State, drvState.State)
	err = rabsender.SendJSONByAction(rabsender.AcChangeDriverState, key, bds)
	if err != nil {
		msg := fmt.Sprintf("Error sending driver state to rabbit. DriverUUID=%s, New state=%s. %s", dsd.DriverUUID, bds.State, err)
		logs.OutputError(msg)

	}
	return dsd, fmt.Errorf(es)

}

// getPreviousDriverState returns driver state, which was up to date
func getPreviousDriverState(driverUUID string) (DriverStatesDrv, error) {
	var drvStates []DriverStatesDrv
	err := db.Model(&drvStates).
		Where("driver_uuid = ?", driverUUID).
		Where("state <> ?", constants.DriverStateConsidering).
		Order("created_at desc").
		Limit(2).
		Select()
	if err != nil {
		return DriverStatesDrv{}, err
	}
	if len(drvStates) != 2 {
		return DriverStatesDrv{}, errors.New("not found 2 or more driver states")
	}
	return drvStates[1], nil
}

// getPreviousDriverState returns driver state, which was up to date
func getPreviousDriverStateWithoutWorking(driverUUID string) (DriverStatesDrv, error) {
	var drvStates DriverStatesDrv
	err := db.Model(&drvStates).
		Where("driver_uuid = ?", driverUUID).
		Where("state <> ?", constants.DriverStateConsidering).
		Where("state <> ?", constants.DriverStateWorking).
		Order("created_at desc").
		Limit(1).
		Select()
	if err != nil {
		return DriverStatesDrv{}, err
	}

	return drvStates, nil
}

// getPreviousDriverState returns driver state, which was up to date
func getCurrentAndPreviousDriverStates(driverUUID string) (current, previous DriverStatesDrv, err error) {
	var drvStates []DriverStatesDrv
	err = db.Model(&drvStates).
		Where("driver_uuid = ?", driverUUID).
		Order("created_at desc").
		Limit(2).
		Select()
	if err != nil {
		return DriverStatesDrv{}, DriverStatesDrv{}, err
	}
	if len(drvStates) != 2 {
		return DriverStatesDrv{}, DriverStatesDrv{}, errors.New("not found 2 or more driver states")
	}
	return drvStates[0], drvStates[1], nil
}

// Deprecated: лучше не использовать эту функцию, много багов
func setDriverPreviousState(driverUUID, msg string, expectedCurrentState ...string) error {
	var err error
	var drvSt DriverStatesDrv

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "setDriverPreviousState",
		"driverUUID": driverUUID,
	})

	currentState, stwc, err := getCurrentAndPreviousDriverStates(driverUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		DriverSetStatus(driverUUID, constants.DriverStateOnline, "Смена статуса водителя после неудачной попытки выяснения предыдущего статуса")
		return err
	}
	log = log.WithField("stwc", stwc)

	if len(expectedCurrentState) > 0 {
		stateMatchesExpected := false
		for _, state := range expectedCurrentState {
			if state == currentState.State {
				stateMatchesExpected = true
				break
			}
		}

		if !stateMatchesExpected {
			// если текущий статус не тот, с которого мы хотели откатиться, значит
			// что-то другое уже сменило статус и мы можем поставить неверный
			log.WithField("state", map[string]interface{}{
				"current":  currentState.State,
				"expected": expectedCurrentState,
			}).Warn("current status doesn't match expected")
			return nil
		}
	}

	// назначить на переключение статусов встречный заказ (сделать текущим (назначается автоматически))

	var haveCounterOrder bool
	err = tool.SendRequest(http.MethodGet,
		config.St.CRM.Host+"/orders/existcounterorder/"+driverUUID,
		nil,
		nil,
		&haveCounterOrder,
	)
	if err != nil {
		log.Error(errpath.Err(err))
		return errpath.Err(err)
	}
	log = log.WithField("haveCounterOrder", haveCounterOrder)

	if stwc.State != constants.DriverStateConsidering { // это уловие нужно для установки правильного статуса после отказа от заказа
		log = log.WithField(errpath.Infof(""), "i am in")
		drvSt = stwc
	} else {
		if haveCounterOrder {
			log = log.WithField(errpath.Infof(""), "i am in")

			drvSt, err = getPreviousDriverState(driverUUID)
			if err != nil {
				log.Errorln(errpath.Err(err, "Error getPreviousDriverState"))
				DriverSetStatus(driverUUID, constants.DriverStateOnline, "Смена статуса водителя после неудачной попытки выяснения предыдущего статуса")
				return err
			}
		} else {
			log = log.WithField(errpath.Infof(""), "i am in")

			drvSt, err = getPreviousDriverStateWithoutWorking(driverUUID)
			if err != nil {
				log.Errorln(errpath.Err(err, "Error getPreviousDriverState"))
				DriverSetStatus(driverUUID, constants.DriverStateOnline, "Смена статуса водителя после неудачной попытки выяснения предыдущего статуса")
				return err
			}
		}
	}
	log = log.WithField("drvSt", drvSt)

	if haveCounterOrder {
		log = log.WithField(errpath.Infof(""), "i am in")
		_, errWL := DriverSetStatus(driverUUID, constants.DriverStateWorking, msg)
		if errWL != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "set driver previos state",
				"driverUUID": driverUUID,
				"reason":     "Error DriverSetStatus",
			}).Warning(errpath.Infof("!!!!!!!!!!!!!!!!"), errWL.Error)
			// }).Warning(errWL.Error)
			return errWL.Error
		}

		log.Warnln(errpath.Infof("!!!!!!!!!!!!!!!!"))
		return nil
	}

	log = log.WithField(errpath.Infof(""), "i am in")
	_, errWL := DriverSetStatus(driverUUID, drvSt.State, msg)
	if errWL != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "set driver previos state",
			"driverUUID": driverUUID,
			"reason":     "Error DriverSetStatus",
		}).Warning(errpath.Infof("!!!!!!!!!!!!!!!!"), errWL.Error)
		// }).Warning(errWL.Error)
		return errWL.Error
	}
	log.Warnln(errpath.Infof("!!!!!!!!!!!!!!!!"))

	return nil
}

// MakeCounterOrderCurrentIfNeed - если есть встречный, сделать его текущим
func MakeCounterOrderCurrentIfNeed(driverUUID string) error {
	if err := tool.SendRequest(http.MethodPost,
		config.St.CRM.Host+"/orders/existcounterorder/"+driverUUID,
		nil,
		nil,
		nil,
	); err != nil {
		logs.Eloger.Errorln(errpath.Err(err))
		return errpath.Err(err)
	}

	return nil
}

func (dsd *DriverStatesDrv) basicDriverState() structures.DriverStates {
	var basic structures.DriverStates

	basic.DriverUUID = dsd.DriverUUID
	basic.State = dsd.State
	basic.Comment = dsd.Comment
	basic.CreatedAt = dsd.CreatedAt

	return basic
}

func (dsd *DriverStatesDrv) BlockedAutomatically() bool {
	if dsd == nil || dsd.Comment == "" {
		return false
	}
	return strings.Contains(dsd.Comment, BlockingActivityMarker) || strings.Contains(dsd.Comment, BlockingRejectionsMarker)
}

// // Здесь проверяем предыдущий статус водителя, если его нет, то создаем на модерации
// if err != nil {
// 	// если новый запрос на установку статуса на модерации то
// 	if state == variables.OnModerationDrv {
// 		newAppState.State = state
// 		newAppState.Comment = comment
// 		newAppState.DriverUUID = driverUUID
// 		err = saveAppState(newAppState)
// 		if err != nil {
// 			return fmt.Errorf("Error saving state - %s", err)
// 		}
// 		return nil
// 	}
// 	// если водитель не найден
// 	er := fmt.Sprintf("Not found prev driver state for UUID=%s. %s", driverUUID, err)
// 	logs.OutputError(er)
// 	return fmt.Errorf(er)
// }

// switch appState.State {
// case variables.OnModerationDrv:
// 	if state == variables.OnModerationDrv {
// 		newAppState.State = state
// 		newAppState.Comment = comment
// 		newAppState.DriverUUID = driverUUID
// 		err = saveAppState(newAppState)
// 		if err != nil {
// 			return fmt.Errorf("Error saving state - %s", err)
// 		}
// 		return nil
// 	}
// 	return fmt.Errorf("Sorry you are on moderation. New state=%s, Old state=%s", state, appState.State)

// case variables.BlockedDrv:
// 	return fmt.Errorf("Sorry you are blocked")
// case variables.WorkingDrv:
// 	return fmt.Errorf("Your order in progress, please finish your order first")
// case variables.AvailableDrv, variables.OnlineDrv, variables.OfflineDrv:
// 	switch state {
// 	case variables.OnlineDrv, variables.OfflineDrv:
// 		if appState.State == state {
// 			return fmt.Errorf("Your are already %s", state)
// 		}
// 		newAppState.State = state
// 		newAppState.Comment = comment
// 		newAppState.DriverUUID = driverUUID
// 		err = saveAppState(newAppState)
// 		if err != nil {
// 			return fmt.Errorf("Error saving state - %s", err)
// 		}
// 	default:
// 		return fmt.Errorf("Unknowns state %s. Please send 'online' or 'offline'", state)
// 	}
// default:
// 	return fmt.Errorf("Seems you have unknown state. Please call Denis")
// }

// func (osc *OrderStateCRM) saveNewState(e *fsm.Event) {
// 	osc.State = e.Dst
// 	_, err := db.Model(osc).Insert()
// 	if err != nil {
// 		msg := fmt.Sprintf("Error saving state. OrderUUID=%s, New state=%s. %s", osc.OrderUUID, e.Dst, err)
// 		logs.OutputError(msg)
// 	}
// 	var ordCRM OrderCRM

// 	_, err = db.Model(&ordCRM).Set("order_state = ?", e.Dst).Where("UUID = ?", osc.OrderUUID).Update()
// 	if err != nil {
// 		msg := fmt.Sprintf("Error saving state to order. OrderUUID=%s. %s", osc.OrderUUID, err)
// 		logs.OutputError(msg)
// 	}
// }
