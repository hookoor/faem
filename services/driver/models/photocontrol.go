package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

// PhotocontrolStatuses - статусы фотоконтроля
type PhotocontrolStatuses string

const (
	// PhotocontrolCreated - фотоконтроль создан и ждет загрузки фоток
	PhotocontrolCreated PhotocontrolStatuses = "created"
	// PhotocontrolModeration - фотоконтроль на модерации
	PhotocontrolModeration PhotocontrolStatuses = "moderation"
	// PhotocontrolPassed - фотоконтроль прошел проверку
	PhotocontrolPassed PhotocontrolStatuses = "passed"
	// PhotocontrolRejected - фотоконтроль не прошел проверку
	PhotocontrolRejected PhotocontrolStatuses = "rejected"
	// PhotocontrolExpired -  фотоконтроль просрочен
	PhotocontrolExpired PhotocontrolStatuses = "expired"
)

// PhotocontrolReservedNames - зарезервированные имена фотоrонторя
type PhotocontrolReservedNames string

const (
	// PhotocontrolNameRegistration - фотоконтроль при регистрации
	PhotocontrolNameRegistration PhotocontrolReservedNames = "registration"
	// PhotocontrolNameScheduledPhotoInspection - плановый фотоконтроль
	PhotocontrolNameScheduledPhotoInspection PhotocontrolReservedNames = "scheduled_photo_inspection"
)

// PhotocontrolReservedSnapshots - зарезервированные шаблоны фотографий
type PhotocontrolReservedSnapshots string

const ()

// SnapshotSketch - снимок для шаблона фотоконтроля по совместительству для инстанса фотоконтроля
type SnapshotSketch struct {
	tableName   struct{} `sql:"drv_phototrol_snapshot_sketch"`
	UUID        string   `json:"uuid" sql:",pk"`
	Name        string   `json:"name"`                        // (control_type)
	Title       string   `json:"title"`                       // русское описание
	Description string   `json:"description"`                 // описание как сфоткать
	ImageMask   tool.URL `json:"image_mask" sql:"image_mask"` // маска для картинки (показывает как сфоткать)
	// 2 поля для инстанса фотоконтроля:
	Status   PhotocontrolStatuses `json:"status,omitempty" sql:"-"`
	ImageURL tool.URL             `json:"image_url,omitempty" sql:"-"`

	CreatedAt time.Time `json:"-" sql:"default:now()"`
	UpdatedAt time.Time `json:"-"`
	Deleted   bool      `json:"-" sql:"default:false"`
}

// Postponement - инфа по отложению срока действия фотоконтроля
// meta: номер 0 имеет изначально заданный срок действия (для истории продления срока)
type Postponement struct {
	Count int   `json:"count"` // порядковый номер продления срока действия (сколько раз был отложен)
	Time  int64 `json:"time"`  // юникс истечения срока действия (на какую дату продлен срок действия)
}

// Phototrol - кусок фотоконтроля
type Phototrol struct {
	Name             string           `json:"name" sql:",not null;unique"`
	Title            string           `json:"title"`
	Description      string           `json:"description"`
	SnapshotSketches []SnapshotSketch `json:"snapshot_sketches" sql:"snapshot_sketches"`
}

// Photocontrol -
type Photocontrol struct {
	tableName struct{}              `sql:"drv_phototrol_photocontrol_tickets"`
	UUID      string                `json:"uuid" sql:",pk"`
	Creator   PhotocontrolCreator   `json:"operator"`
	Driver    PhotocontrolForDriver `json:"driver"`
	Phototrol
	Comment        string               `json:"comment"`
	Status         PhotocontrolStatuses `json:"status" sql:"default:moderation"`
	ActivateTime   int64                `json:"activate_time"`          // юникс время после которого фотоконтроль становиться активным (для этого поля нет логики(фронтеры отображают фотоконтроль при превышении этого времени))
	TimeValidity   int64                `json:"time_validity"`          // юникс истечения срока действия
	WarningTime    int64                `json:"warning_time"`           // юникс предупреждение о истечения срока действия
	Postponement   []Postponement       `json:"postponement"`           // количество попыток продлить срок действия
	Settings       PhotocontrolSettings `json:"settings"`               // настройки поведения фотоконтроля
	OriginSettings PhotocontrolSettings `json:"-" pg:"origin_settings"` // изначальные настройки которые копируются(наследуются) при автосоздании нового фотоконтроля
	Stages         Stages               `json:"stages"`                 // метки стадий которые прошел фотоконтроль (для единоразвого выполнения логики при смене состояния фотоконтроля)
	CreatedAtUnix  int64                `json:"created_at_unix"`

	CreatedAt    time.Time `json:"-" sql:"default:now()"`
	UpdatedAt    time.Time `json:"-"`
	Deleted      bool      `json:"-" sql:"default:false"`
	TaxiParkUUID *string   `json:"taxi_park_uuid"`
}

// PhotocontrolCreator -
type PhotocontrolCreator struct {
	UUID string `json:"uuid"`
	Role string `json:"role"`
	Name string `json:"name"`
}

// PhotocontrolForDriver -
type PhotocontrolForDriver struct {
	UUID  string `json:"uuid"`
	Alias int    `json:"alias"`
	Name  string `json:"name"`
}

// Stages - метки стадий которые прошел фотоконтроль
// meta: для единоразвого выполнения логики при смене состояния фотоконтроля
type Stages struct {
	Passed  bool `json:"passed"`
	Expired bool `json:"expired"`
	Warning bool `json:"warning"`
}

// PhotocontrolSketch - шаблон, имеет в себе определенный набор снимков которые должны быть присланы обязательно.
type PhotocontrolSketch struct {
	tableName struct{} `sql:"drv_phototrol_photocontrol_sketches"`
	UUID      string   `json:"uuid" sql:",pk"`
	Phototrol
	StartActivateTime int64                `json:"start_activate_time"` // (текущее время + значение поля) юникс время после которого фотоконтроль становиться активным
	StartTimeValidity int64                `json:"start_time_validity"` // (текущее время + значение поля) юникс истечения срока действия
	StartWarningTime  int64                `json:"start_warning_time"`  // (текущее время + значение поля) юникс предупреждение о истечения срока действия
	Settings          PhotocontrolSettings `json:"settings"`

	CreatedAt time.Time `json:"-" sql:"default:now()"`
	UpdatedAt time.Time `json:"-"`
	Deleted   bool      `json:"-" sql:"default:false"`
}

// ----------------------------
// ----------------------------
// ----------------------------

// PhotocontrolSettings - настройка фотоконтроля. Мета данные.
// Любая переодичность со значением -1 означает отсутствие переодичности.
type PhotocontrolSettings struct {
	// Expired - просрачиваемый
	Expired struct {
		Enable bool `json:"enable"`
		// Blocking - блокирующий водителя при просрочке
		Blocking struct {
			Enable bool `json:"enable"`
		} `json:"blocking"`
		// Warning - уведомляющий водителя при просрочке
		Warning struct {
			Enable bool `json:"enable"`
		} `json:"warning"`
	} `json:"expired"`
	// Postponed - продлеваемый (откладываемый)
	Postponed struct {
		Enable bool `json:"enable"`
		// Count - сколько раз может быть отложен
		Count int `json:"count"`
		// Time - на сколько секунд будет продлен (!!!сомнительный функционал)
		Time int `json:"time"`
	} `json:"postponed"`
	// Offspring - создание фотоконтроля-потомка (при просрочке первого создается второй)
	Offspring struct {
		Enable bool `json:"enable"`
		// Periodicity - значение смещения времен, дабы у потомка не было родительского времени просрочки (для ActivateTime, TimeValidity, WarningTime ...)
		Periodicity int `json:"periodicity"`

		// WhenPassed - при прохождении создается новый
		WhenPassed bool `json:"when_passed"`
		// WhenExpired - при просрочке первого создается новый
		WhenExpired bool `json:"when_expired"`
		// meta: при срабатывание одной из меток-условий все становятся false, дабы создался только один потомок
	} `json:"offspring"`
	// ActivityAccrual - начисление активности
	ActivityAccrual struct {
		Enable bool `json:"enable"`
		// Number - количество начисляемых nединиц
		Number int `json:"number"`
	} `json:"activity_accrual"`
	// AddToScoring - стоит ли добавить очки распределения при прохождении фотоконтроля
	AddToScoring struct {
		Enable bool `json:"enable"`
		// ScoringBoostActivityTime - сколько секунд будет длиться повышение очков распределения (time.Now()+ScoringBoostActivityTime)
		ScoringBoostActivityTime int64 `json:"scoring_boost_activity_time"`
	} `json:"add_to_scoring"`
	// Visibility - наблюдаемый.
	// true - при любой смене статуса он приходит с запросом на актуальные фотоконтроли. Можно следить за его состоянием
	// false - приходит с запросом на актуальные фотоконтроли только со статусом (created, moderation)
	Visibility bool `json:"visibility"`
}

// CreatePhotocontrolSetting -
func CreatePhotocontrolSetting() PhotocontrolSettings {
	var ps PhotocontrolSettings

	ps.Expired.Enable = true

	ps.Expired.Blocking.Enable = true
	ps.Expired.Warning.Enable = true

	ps.Postponed.Enable = false
	ps.Postponed.Count = 1
	ps.Postponed.Time = int(time.Hour.Seconds() * 24)

	ps.Offspring.Enable = false

	ps.ActivityAccrual.Enable = false

	ps.AddToScoring.Enable = false

	ps.Visibility = false

	return ps
}

// PhotocontrolTicketsFilter -
type PhotocontrolTicketsFilter struct {
	Name   string `json:"name"` // название тикета
	Title  string `json:"title"`
	Status string `json:"status"`

	DriverUUID       string `json:"driver_uuid"`
	Alias            string `json:"alias"` // алиас водителя
	DriverName       string `json:"driver_name"`
	AllowedTaxiParks []string

	CreatedAt time.Time `json:"created_at"`

	tool.Paginator
}

// ----------------------------
// ----------------------------
// ----------------------------
