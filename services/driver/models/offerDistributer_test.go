package models

//
//const (
//	exprEval   = `"comfort" in Driver.Tag and Distance > 500 and Distance < 1000`
//	clientRule = `100*Activity + 10*Karma + 200*VIP + (Waiting < 0.5 ? Waiting*60 : Waiting*90)`
//	driverRule = `100*Activity + 10*Karma + 30*Distance + (Waiting < 0.5 ? Waiting*60 : Waiting*90)`
//)
//
//type scoreData struct {
//	Driver   DriversApps
//	Distance float64
//}
//
//var (
//	exprResult bool
//	score      = scoreData{
//		Driver: DriversApps{
//			Driver: structures.Driver{
//				Tag: []string{"comfort"},
//			},
//		},
//		Distance: 700,
//	}
//)
//
//func BenchmarkExprEval(b *testing.B) {
//	var result interface{}
//	for i := 0; i < b.N; i++ {
//		result, _ = expr.Eval(exprEval, score)
//	}
//	exprResult, _ = result.(bool)
//}
//
//func BenchmarkExprCompile(b *testing.B) {
//	var result interface{}
//	node, err := expr.Parse(exprEval, expr.Env(scoreData{}))
//	assert.NoError(b, err)
//
//	for i := 0; i < b.N; i++ {
//		result, _ = node.Eval(score)
//	}
//	exprResult, _ = result.(bool)
//}
//
//var dummyOffers = []OfferDrv{
//	{
//		ID:                     0,
//		CreatedAt:              time.Now(),
//		ClientDistributionRule: clientRule,
//		Offer: structures.Offer{
//			Order: structures.Order{
//				Client: structures.Client{
//					Karma:    10,
//					Activity: 6,
//				},
//			},
//		},
//	},
//	{
//		ID:                     1,
//		CreatedAt:              time.Now().Add(-time.Second * 30),
//		ClientDistributionRule: clientRule,
//		Offer: structures.Offer{
//			Order: structures.Order{
//				Client: structures.Client{
//					Karma:    3,
//					Activity: 10,
//				},
//			},
//		},
//	},
//	{
//		ID:                     2,
//		CreatedAt:              time.Now().Add(-time.Second * 20),
//		ClientDistributionRule: clientRule,
//		Offer: structures.Offer{
//			Order: structures.Order{
//				Client: structures.Client{
//					Karma:    4,
//					Activity: 4,
//				},
//			},
//		},
//	},
//	{
//		ID:                     3,
//		CreatedAt:              time.Now().Add(-time.Second * 15),
//		ClientDistributionRule: clientRule,
//		Offer: structures.Offer{
//			Order: structures.Order{
//				Client: structures.Client{
//					Karma:    2,
//					Activity: 1,
//				},
//			},
//		},
//	},
//	{
//		ID:                     4,
//		CreatedAt:              time.Now().Add(-time.Second * 40),
//		ClientDistributionRule: clientRule,
//		Offer: structures.Offer{
//			Order: structures.Order{
//				Client: structures.Client{
//					Karma:    9,
//					Activity: 5,
//				},
//			},
//		},
//	},
//}
//
//func TestNormalizeOffers(t *testing.T) {
//	normalized := normalizeOffers(dummyOffers)
//	for _, val := range normalized {
//		if val.Karma < 0 || val.Karma > 1 {
//			t.Errorf("karma must be in range [0..1], instead got: %f", val.Karma)
//		}
//		if val.Activity < 0 || val.Activity > 1 {
//			t.Errorf("activity must be in range [0..1], instead got: %f", val.Activity)
//		}
//		if val.Waiting < 0 || val.Waiting > 1 {
//			t.Errorf("waiting must be in range [0..1], instead got: %f", val.Waiting)
//		}
//		if val.VIP < 0 || val.VIP > 1 {
//			t.Errorf("vip must be in range [0..1], instead got: %d", val.VIP)
//		}
//	}
//}
//
//func TestScoreAndSortOffers(t *testing.T) {
//	offers, err := scoreAndSortOffers(dummyOffers)
//	assert.NoError(t, err)
//
//	isSorted := sort.SliceIsSorted(offers, func(i, j int) bool {
//		left, right := offers[i], offers[j]
//		return left.DistributionMeta.Score > right.DistributionMeta.Score
//	})
//	assert.True(t, isSorted)
//}
//
//var dummyDrivers = []DriversApps{
//	{
//		ID: 0,
//		Driver: structures.Driver{
//			UUID:     "0",
//			Karma:    0,
//			Activity: 0,
//		},
//	},
//	{
//		ID: 1,
//		Driver: structures.Driver{
//			UUID:     "1",
//			Karma:    10,
//			Activity: 4,
//		},
//	},
//	{
//		ID: 2,
//		Driver: structures.Driver{
//			UUID:     "2",
//			Karma:    5,
//			Activity: 12,
//		},
//	},
//	{
//		ID: 3,
//		Driver: structures.Driver{
//			UUID:     "3",
//			Karma:    10,
//			Activity: 10,
//		},
//	},
//	{
//		ID: 4,
//		Driver: structures.Driver{
//			UUID:     "4",
//			Karma:    8,
//			Activity: 5,
//		},
//	},
//}
//
//var dummyWaitings = map[string]int{
//	"0": 600,
//	"1": 400,
//	"2": 30,
//	"3": 350,
//	"4": 500,
//}
//
//var dummyDistances = map[string]float64{
//	"0": 100,
//	"1": 250,
//	"2": 124,
//	"3": 180,
//	"4": 56,
//}
//
//func TestNormalizeDrivers(t *testing.T) {
//	normalized := normalizeDrivers(dummyDrivers, dummyWaitings, dummyDistances)
//	for _, val := range normalized {
//		if val.Karma < 0 || val.Karma > 1 {
//			t.Errorf("karma must be in range [0..1], instead got: %f", val.Karma)
//		}
//		if val.Activity < 0 || val.Activity > 1 {
//			t.Errorf("activity must be in range [0..1], instead got: %f", val.Activity)
//		}
//		if val.Waiting < 0 || val.Waiting > 1 {
//			t.Errorf("waiting must be in range [0..1], instead got: %f", val.Waiting)
//		}
//		if val.Distance < 0 || val.Distance > 1 {
//			t.Errorf("distance must be in range [0..1], instead got: %f", val.Distance)
//		}
//	}
//}
//
//func TestFindBestDriverSmartly(t *testing.T) {
//	_, bestScore, err := findBestDriverSmartly(driverRule, dummyDrivers, dummyWaitings, dummyDistances)
//	assert.NoError(t, err)
//
//	if bestScore <= 0 {
//		t.Error("best score must be grater than zero")
//	}
//}
