package models

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

// TODO: move to the remote config
const (
	driverMaxRating               = 5
	driverSignificantRatingCount  = 50
	driverSignificantRatingPeriod = time.Hour * 24

	clientMinBlacklistRating = 2
)

type (
	OrderRatingDrv struct {
		tableName struct{} `sql:"drv_order_rating"`
		structures.OrderRating
		ID        int
		CreatedAt time.Time
	}
)

func (orRat OrderRatingDrv) Save() error {
	err := orRat.validate()
	if err != nil {
		return fmt.Errorf("validate error: %s", err)
	}
	_, err = db.Model(&orRat).Insert()
	if err != nil {
		return fmt.Errorf("insert error: %s", err)
	}
	return err
}

func (orRat OrderRatingDrv) validate() error {
	exs, err := db.Model(&OfferStatesDrv{}).Where("order_uuid = ? AND driver_uuid = ?", orRat.OrderUUID, orRat.DriverUUID).Exists()
	if err != nil {
		return fmt.Errorf("error check order state exist,%s", err)
	}
	if !exs {
		return fmt.Errorf("not found such an order for the driver")
	}
	if orRat.Value > 5 || orRat.Value < 1 {
		return fmt.Errorf("invalid value")
	}
	return nil
}

func UpdateDriverKarma(driverUUID string) (float64, error) {
	if driverUUID == "" {
		return 0, errors.New("empty driverUUID provided")
	}

	significantRatingCount, err := fetchDriverSignificantRatingCount()
	if err != nil {
		return 0, errors.Wrap(err, "failed to fetch driver significant rating count")
	}

	var ratings []OrderDrv
	if err = db.Model(&ratings).
		Column("order_drv.client_rating", "order_drv.created_at").
		Join("INNER JOIN drv_offers AS offer_drv ON order_drv.uuid = offer_drv.order_uuid AND offer_drv.driver_uuid = ?", driverUUID).
		Where("(client_rating ->> 'value')::integer <> 0").
		Order("created_at DESC").
		Limit(significantRatingCount).
		Select(); err != nil {
		return 0, errors.Wrap(err, "failed to fetch driver existing rating count")
	}

	// Total existing rating counts may be less then significant count, append some records to match total
	for len(ratings) < significantRatingCount {
		ratings = append(ratings, OrderDrv{
			Order: structures.Order{
				ClientRating: structures.OrderRatingUnit{Value: driverMaxRating},
				CreatedAt:    time.Now().Add(-2 * driverSignificantRatingPeriod), // make a little bit less important by subtracting two periods
			},
		})
	}

	// Count the average
	num, denom := 0., 0.
	for _, rating := range ratings {
		daysDiff := time.Now().Sub(rating.Order.CreatedAt) / driverSignificantRatingPeriod
		if daysDiff < 1 {
			daysDiff = 1
		}
		weight := 1. / float64(daysDiff)
		num += weight * float64(rating.Order.ClientRating.Value)
		denom += weight
	}

	// Update the driver
	karma := num / denom
	_, err = db.Model((*DriversApps)(nil)).
		Where("uuid = ?", driverUUID).
		Set("karma = GREATEST(?, 0)", karma).
		Update()
	if err != nil {
		return 0, errors.Wrap(err, "failed to update driver rating")
	}

	// Notify listeners about driver's karma changing
	go func() {
		notification := structures.UserKarma{
			UserUUID: driverUUID,
			Karma:    karma,
		}
		if err := rabsender.SendJSONByAction(rabsender.AcDriverKarmaChanged, "", notification); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "notifying about driver karma changing",
				"driverUUID": driverUUID,
			}).Error(err)
		}
	}()

	return karma, nil
}

func fetchDriverSignificantRatingCount() (int, error) {
	return driverSignificantRatingCount, nil
}

func FetchClientMinBlacklistRating() (int, error) {
	return clientMinBlacklistRating, nil
}

func UpdateDriverBlacklistIfNeeded(orderRating structures.OrderRating, minClientRating int) error {
	if orderRating.Value > minClientRating || orderRating.ClientUUID == "" {
		return nil
	}

	_, err := db.Model((*DriversApps)(nil)).
		Where("uuid = ?", orderRating.DriverUUID).
		Set("blacklist = array_append(blacklist, ?)", orderRating.ClientUUID).
		Update()
	return errors.Wrap(err, "failed to update driver blacklist")
}
