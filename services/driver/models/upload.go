package models

import (
	"io"

	"github.com/pkg/errors"
)

const (
	maxUploadSize = 3e6 // 3MB
)

var validMimeTypes = map[string]struct{}{
	"image/jpeg": struct{}{},
	"image/png":  struct{}{},
}

type Upload struct {
	Filesize int64
	Filename string
	Filetype string
	File     io.Reader
}

func (u *Upload) Validate() error {
	if u.Filesize > maxUploadSize {
		return errors.Errorf("file is too large, maximum size is: %v bytes", maxUploadSize)
	}

	if _, found := validMimeTypes[u.Filetype]; !found {
		return errors.New("invalid file type provided")
	}
	return nil
}
