package models

import (
	"context"
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"math"

	"github.com/antonmedv/expr"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/jobqueue"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

const (
	lowBalance               = 10 // RUB
	penaltyForCancelledOrder = 0  // RUB
	mandatoryPrepay          = 30 // RUB

	msgCompleteAction          = "Оплата за выполненный заказ"
	msgTaxiParkCompleteAction  = "Оплата таксопарку за выполненный заказ"
	msgChangePaymentTypeToCash = "Способ оплаты заказа был изменен на наличные"
	msgRejectAction            = "Списание за отказ от предложенного заказа"
	msgTaxiParkRejectAction    = "Списание таксопарка отказ от предложенного заказа"
	msgCancelOrderAction       = "Списание за отказ от уже принятого заказа"
	msgDriverCompensation      = "Компенсация за стоимость выполненного заказа"
	msgWaitingAction           = "Оплата за ожидание во время выполнения поездки"
	msgPaymentByBonus          = "Оплата бонусами за выполненный заказ"
	msgInsuranceCharge         = "Оплата страхования"

	lblFinishedOrder  = "Перевозка пассажиров и багажа"
	lblCancelledOrder = lblFinishedOrder

	maxParallelTransferBalanceJobsAllowed = 97
)

const (
	// Default tariff values
	defaultRejExpr         = "DriverBalance - 0"
	defaultPercentCommExpr = "DriverBalance - OrderPrice*0.12"
	defaultPeriodCommExpr  = "DriverBalance - 0"
)

var (
	balanceTransferJobs = jobqueue.NewJobQueue(maxParallelTransferBalanceJobsAllowed)
)

// StartBillingAlgorithm применяет транзакцию исходоя из переданного offerUUID и driverUUID
func (os OfferStatesDrv) StartBillingAlgorithm(action string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "Start Billing Algorithm",
		"driverUUID": os.DriverUUID,
		"offerUUID":  os.OfferUUID,
	})

	driver, err := GetDriverByUUID(os.DriverUUID)
	if err != nil {
		log.Error("error getting driver by UUID,", err)
		return
	}
	billingData, err := GetBillingData(driver, os.OfferUUID)
	if err != nil {
		log.Error("error getting billing data,", err)
		return
	}
	var order OrderDrv
	if err = GetByUUID(os.OrderUUID, &order); err != nil {
		log.Error("error getting order data,", err)
		return
	}
	driver.TaxiParkData, err = GetTaxiParkFromCRM(driver.GetTaxiParkUUID())
	if err != nil {
		log.Error("error getting owner data,", err)
	}
	driver.DrvTariff.Markups, err = GetDriverTariffMarkups(driver.DrvTariff.UUID)
	if err != nil {
		log.Error("error getting  Markups,", err)
	}
	transaction := TransactionDriver{
		OwnerName:                driver.TaxiParkData.Name,
		OfferUUID:                os.OfferUUID,
		OrderUUID:                os.OrderUUID,
		DriverUUID:               os.DriverUUID,
		CurrentDriverBalance:     &driver.Balance,
		CurrentDriverCardBalance: &driver.CardBalance,
		Tariff: structures.TariffMetadata{
			UUID:       driver.DrvTariff.UUID,
			Name:       driver.DrvTariff.Name,
			TariffType: driver.DrvTariff.TariffType,
		},
		TaxiPark: structures.TaxiParkMetadata{
			UUID: driver.TaxiParkData.UUID,
			Name: driver.TaxiParkData.Name,
		},
	}

	if err = tariffTransaction(action, driver, billingData.Clone(), transaction, &order); err != nil {
		log.Error(err)
	}
	GenerateReceiptAfterCompletion(action, &order, billingData)
}

func tariffTransaction(
	action string,
	driver DriversApps,
	billingData *structures.BillingData,
	transaction TransactionDriver,
	order *OrderDrv,
) error {
	if billingData.OfferAcceptedFromOffline {
		offlineTariff, err := GetOfflineDriverTariff(driver.UUID)
		if err != nil {
			return errors.Wrap(err, "error getting offline driver tariff")
		}

		if offlineTariff.CommissionExp != "" {
			driver.DrvTariff.DriverTariff = offlineTariff
		}
	}

	transaction.Tariff = structures.TariffMetadata{
		UUID:       driver.DrvTariff.UUID,
		Name:       driver.DrvTariff.Name,
		TariffType: driver.DrvTariff.TariffType,
	}
	markup, _ := driver.DrvTariff.GetMarkupByTaxiParkUUID(driver.GetTaxiParkUUID())
	if action == BillingAfterRejectionAction { // after driver has rejected an offer
		var rejTransactions []TransactionDriver
		if driver.DrvTariff.RejectionExp == "" {
			driver.DrvTariff.RejectionExp = defaultRejExpr
		}
		ast, err := expr.Eval(driver.DrvTariff.RejectionExp, billingData)
		if err != nil {
			return errors.Wrap(err, "error applying expression")
		}
		transaction.TransactionType = structures.TransferTypeWithdraw
		transaction.Event = msgRejectAction
		transactionSum, _ := ast.(float64)
		transaction.Amount = RoundAmount(transactionSum)
		rejTransactions = append(rejTransactions, transaction)
		if markup.RejectionExpr != "" {
			ast, err := expr.Eval(markup.RejectionExpr, billingData)
			if err != nil {
				return errors.Wrap(err, "error applying taxi park rej expression")
			}

			transactionSum, ok := ast.(float64)
			if ok && transactionSum != 0 {
				rejTransactions = append(rejTransactions, TransactionDriver{
					TransactionType:          structures.TransferTypePayment,
					Event:                    msgTaxiParkRejectAction,
					Amount:                   RoundAmount(transactionSum),
					PayerAccountType:         structures.AccountTypeBonus,
					DriverUUID:               driver.UUID,
					PayeeUUID:                driver.TaxiParkData.UUID,
					PayerUUID:                driver.UUID,
					PayeeType:                structures.UserTypeTaxiPark,
					PayerType:                structures.UserTypeDriver,
					TaxiParkUUID:             driver.GetTaxiParkUUID(),
					OwnerName:                driver.TaxiParkData.Name,
					CurrentDriverBalance:     &driver.Balance,
					CurrentDriverCardBalance: &driver.CardBalance,
					Tariff: structures.TariffMetadata{
						UUID:       driver.DrvTariff.UUID,
						Name:       driver.DrvTariff.Name,
						TariffType: driver.DrvTariff.TariffType,
					},
				})
			} else {
				logs.Eloger.WithFields(logrus.Fields{
					"event":        "tariff transaction",
					"driverUUID":   driver.UUID,
					"orderUUID":    order.UUID,
					"taxiParkUUID": driver.TaxiParkData.UUID,
				}).Error("error cast to type. Rej Action")
			}
		}
		CreateDriverTransactions(rejTransactions...)
		return nil
	}

	var transactions []TransactionDriver
	// After a completed order
	if driver.DrvTariff.CommissionExp == "" {
		switch driver.DrvTariff.TariffType {
		case structures.DriverTariffPeriod:
			driver.DrvTariff.CommissionExp = defaultPeriodCommExpr
		default:
			driver.DrvTariff.CommissionExp = defaultPercentCommExpr
		}
		if driver.DrvTariff.CommissionExp == "" {
			return nil
		}
	}
	if markup.CompleteExpr != "" {
		ast, err := expr.Eval(markup.CompleteExpr, billingData)
		if err != nil {
			return errors.Wrap(err, "error applying taxi park complete expression")
		}

		transactionSum, ok := ast.(float64)
		if ok && transactionSum != 0 {
			transactions = append(transactions, TransactionDriver{
				TransactionType:          structures.TransferTypePayment,
				Event:                    msgTaxiParkCompleteAction,
				OrderUUID:                order.UUID,
				Amount:                   RoundAmount(transactionSum),
				PayerAccountType:         structures.AccountTypeBonus,
				DriverUUID:               driver.UUID,
				PayeeUUID:                driver.TaxiParkData.UUID,
				PayerUUID:                driver.UUID,
				PayeeType:                structures.UserTypeTaxiPark,
				PayerType:                structures.UserTypeDriver,
				TaxiParkUUID:             driver.GetTaxiParkUUID(),
				OwnerName:                driver.TaxiParkData.Name,
				CurrentDriverBalance:     &driver.Balance,
				CurrentDriverCardBalance: &driver.CardBalance,
				Tariff: structures.TariffMetadata{
					UUID:       driver.DrvTariff.UUID,
					Name:       driver.DrvTariff.Name,
					TariffType: driver.DrvTariff.TariffType,
				},
			})
		} else {
			logs.Eloger.WithFields(logrus.Fields{
				"event":        "tariff transaction",
				"driverUUID":   driver.UUID,
				"orderUUID":    order.UUID,
				"taxiParkUUID": driver.TaxiParkData.UUID,
			}).Error("error cast to type. Complete Action")
		}
	}

	var clientTransactions []TransactionClient

	if billingData.WaitingPrice == 0 { // there was no waiting
		ast, err := expr.Eval(driver.DrvTariff.CommissionExp, billingData)
		if err != nil {
			return errors.Wrap(err, "error applying expression")
		}
		transaction.WithFee = true
		transaction.TransactionType = structures.TransferTypePayment
		transaction.PayerAccountType = structures.AccountTypeCash
		if order.GetPaymentType() == constants.OrderPaymentTypeCard {
			transaction.PayerAccountType = structures.AccountTypeCard // remember to specify right payment type
			transaction.Prepaid = true
		}
		transaction.TotalOrderPrice = math.Abs(float64(billingData.GetActualOrderPrice()))
		// Special case for product delivery
		if order.Service.ProductDelivery {
			transaction.PayerAccountType = structures.AccountTypeCash
			transaction.Prepaid = false
			transaction.TotalOrderPrice = 0
		}
		transaction.ClientUUID = order.Client.UUID
		transaction.ClientPhone = order.GetClientPhone()
		transaction.Event = msgCompleteAction
		transactionSum, _ := ast.(float64)
		transaction.Amount = RoundAmount(transactionSum)
		if order.Tariff.BonusPayment != 0 { // split transactions by bonus
			billingData.DriverCompensation += order.Tariff.BonusPayment // remember to topup a driver
			var withdrawBonus TransactionClient
			transaction, withdrawBonus = splitBonusTransactions(transaction, order.Tariff.BonusPayment)
			clientTransactions = append(clientTransactions, withdrawBonus)
		}
		transactions = append(transactions, transaction)
	} else { // there was waiting time, we need to split transactions
		// Without waiting price
		withoutWaiting := billingData.Clone() // get a deep copy
		withoutWaiting.SubOrderPrice(withoutWaiting.WaitingPrice)
		ast, err := expr.Eval(driver.DrvTariff.CommissionExp, withoutWaiting)
		if err != nil {
			return errors.Wrap(err, "error applying expression")
		}
		transaction.WithFee = true
		transaction.TransactionType = structures.TransferTypePayment
		transaction.PayerAccountType = structures.AccountTypeCash
		if order.GetPaymentType() == constants.OrderPaymentTypeCard {
			transaction.PayerAccountType = structures.AccountTypeCard // remember to specify right payment type
			transaction.Prepaid = true
		}
		transaction.TotalOrderPrice = math.Abs(float64(withoutWaiting.GetActualOrderPrice()))
		// Special case for product delivery
		if order.Service.ProductDelivery {
			transaction.PayerAccountType = structures.AccountTypeCash
			transaction.Prepaid = false
			transaction.TotalOrderPrice = 0
		}
		transaction.ClientUUID = order.Client.UUID
		transaction.ClientPhone = order.GetClientPhone()
		transaction.Event = msgCompleteAction
		transactionSum, _ := ast.(float64)
		transaction.Amount = RoundAmount(transactionSum)
		if order.Tariff.BonusPayment != 0 { // split transactions by bonus
			billingData.DriverCompensation += order.Tariff.BonusPayment // remember to topup a driver
			var withdrawBonus TransactionClient
			transaction, withdrawBonus = splitBonusTransactions(transaction, order.Tariff.BonusPayment)
			clientTransactions = append(clientTransactions, withdrawBonus)
		}
		transactions = append(transactions, transaction)

		// Waiting price
		onlyWaiting := billingData.Clone()
		onlyWaiting.InsuranceCost = 0 // транзакция за время ожидания не должна включать страховку
		onlyWaiting.DecreaseOrderPriceTo(onlyWaiting.WaitingPrice)

		ast, err = expr.Eval(driver.DrvTariff.CommissionExp, onlyWaiting)
		if err != nil {
			return errors.Wrap(err, "error applying expression")
		}
		transaction.WithFee = true
		transaction.TransactionType = structures.TransferTypePayment
		transaction.PayerAccountType = structures.AccountTypeCash
		if order.GetPaymentType() == constants.OrderPaymentTypeCard {
			transaction.PayerAccountType = structures.AccountTypeCard // remember to specify right payment type
			transaction.Prepaid = false
		}
		transaction.TotalOrderPrice = math.Abs(float64(onlyWaiting.GetActualOrderPrice()))
		// Special case for product delivery
		if order.Service.ProductDelivery {
			transaction.PayerAccountType = structures.AccountTypeCash
			transaction.Prepaid = false
			transaction.TotalOrderPrice = 0
		}
		transaction.ClientUUID = order.Client.UUID
		transaction.ClientPhone = order.GetClientPhone()
		transaction.Event = msgWaitingAction
		transactionSum, _ = ast.(float64)
		transaction.Amount = RoundAmount(transactionSum)
		transactions = append(transactions, transaction)
	}

	// Decide if a compensation required
	if billingData.DriverCompensation > 0 {
		compensation := TransactionDriver{
			OwnerName:                transaction.OwnerName,
			OfferUUID:                transaction.OfferUUID,
			OrderUUID:                transaction.OrderUUID,
			DriverUUID:               transaction.DriverUUID,
			ClientUUID:               transaction.ClientUUID,
			ClientPhone:              transaction.ClientPhone,
			TransactionType:          structures.TransferTypeTopUp,
			Event:                    msgDriverCompensation,
			Amount:                   float64(billingData.DriverCompensation),
			CurrentDriverBalance:     &driver.Balance,
			CurrentDriverCardBalance: &driver.CardBalance,
		}
		transactions = append(transactions, compensation)
	}

	// check if there is need to charge insurance cost
	if billingData.InsuranceCost > 0 {
		//insuranceCharge := TransactionDriver{
		//	TransactionType: structures.TransferTypePayment,
		//	Event:           msgInsuranceCharge,
		//	WithFee:         true,
		//	Amount:          float64(billingData.InsuranceCost),
		//
		//	PayerUUID: driver.UUID,
		//	PayerType: structures.UserTypeDriver,
		//
		//	DriverUUID: driver.UUID,
		//	OrderUUID:  transaction.OrderUUID,
		//	OfferUUID:  transaction.OfferUUID,
		//	OwnerName:  driver.TaxiParkData.Name,
		//	Tariff: structures.TariffMetadata{
		//		UUID:       driver.DrvTariff.UUID,
		//		Name:       driver.DrvTariff.Name,
		//		TariffType: driver.DrvTariff.TariffType,
		//	},
		//	TaxiPark: structures.TaxiParkMetadata{
		//		UUID: driver.TaxiParkData.UUID,
		//		Name: driver.TaxiParkData.Name,
		//	},
		//	CurrentDriverBalance:     &driver.Balance,
		//	CurrentDriverCardBalance: &driver.CardBalance,
		//}
		//
		//transactions = append(transactions, insuranceCharge)
	}

	CreateDriverTransactions(transactions...)
	CreateClientTransactions(clientTransactions...)
	return nil
}

func RoundAmount(x float64) float64 {
	return math.Abs(tool.RoundTo(x, 2))
}

func splitBonusTransactions(transaction TransactionDriver, amount int) (primary TransactionDriver, bonus TransactionClient) {
	bonus = TransactionClient{
		TransactionType: structures.TransferTypeWithdraw,
		Event:           msgPaymentByBonus,
		Amount:          float64(amount),
		OwnerName:       transaction.OwnerName,
		OfferUUID:       transaction.OfferUUID,
		OrderUUID:       transaction.OrderUUID,
		DriverUUID:      transaction.DriverUUID,
		ClientUUID:      transaction.ClientUUID,
		ClientPhone:     transaction.ClientPhone,
		AccountType:     structures.AccountTypeBonus,
	}

	// Modify the source transaction
	primary = transaction
	if primary.WithFee {
		primary.TotalOrderPrice -= float64(amount)
		primary.TotalOrderPrice = math.Max(primary.TotalOrderPrice, 0) // just in case
	} else {
		primary.Amount -= float64(amount)
		primary.Amount = math.Max(primary.Amount, 0) // just in case
	}
	return
}

func GenerateReceiptAfterCompletion(action string, order *OrderDrv, billingData structures.BillingData) {
	if action != BillingAfterCompletionAction {
		return
	}
	orderPrice := billingData.GetActualOrderPrice() - order.Tariff.BonusPayment
	GenerateReceipt(order, orderPrice, lblFinishedOrder)
}

func GenerateReceipt(order *OrderDrv, orderPrice int, label string) {
	// Prevent test receipt generation
	if config.St.IsDevelopment() {
		return
	}

	if order.GetPaymentType() != constants.OrderPaymentTypeCard || orderPrice <= 0 {
		return
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "generating receipt",
		"orderUUID": order.UUID,
	})

	if order.Client.UUID == "" {
		log.Error("empty client uuid for an order with card payment")
		return
	}

	receiptData := structures.ReceiptData{
		Amount:     float64(orderPrice),
		OrderUUID:  order.UUID,
		ClientUUID: order.Client.UUID,
		Label:      label,
	}
	go func() {
		if err := rabsender.SendJSONByAction(rabsender.AcMakeReceipt, "", receiptData); err != nil {
			log.WithField("reason", "making a receipt").Error(err)
			return
		}
		log.WithField("reason", "transfer sent to RMQ").Debug("OK")
	}()
}

func PrepayOrder(orderUUID, offerUUID, driverUUID string) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "prepay order",
		"orderUUID": orderUUID,
	})

	var order OrderDrv
	if err := GetByUUID(orderUUID, &order); err != nil {
		log.WithField("reason", "failed to get order by uuid").Error(err)
		return
	}

	if order.Service.ProductDelivery || // must be already payed
		order.GetPaymentType() != constants.OrderPaymentTypeCard ||
		order.PaymentMeta.IsPrepaid {
		return // no need to continue
	}

	if order.Client.UUID == "" {
		log.Error("empty client uuid for an order with card payment")
		return
	}

	totalPrice := order.Tariff.TotalPrice
	if totalPrice <= 0 { // order price is somehow undefined
		totalPrice = mandatoryPrepay
	}

	// Check if there are some bonuses to use
	if order.Tariff.BonusPayment != 0 {
		totalPrice -= order.Tariff.BonusPayment
		if totalPrice <= 0 { // there is no need to prepay, totally bonus trip
			return
		}
	}

	err := rabsender.SendJSONByAction(rabsender.AcPrepayOrder, "", structures.PrepayOrder{
		IdempotencyKey: structures.GenerateUUID(),
		PayerUUID:      order.Client.UUID,
		SubjectUUID:    orderUUID,
		Amount:         float64(totalPrice),
		PayeeUUID:      driverUUID,
		Meta:           structures.PrepayOrderMeta{OfferUUID: offerUUID},
	})
	if err != nil {
		log.WithField("reason", "failed to send a message via RMQ").Error(err)
		return
	}

	// Save info about the prepay
	meta := order.PaymentMeta
	meta.IsPrepaid = true
	_, err = db.Model((*OrderDrv)(nil)).
		Set("payment_meta = ?", meta).
		Where("uuid = ?", orderUUID).
		Update()
	if err != nil {
		log.WithField("reason", "failed to save prepay flag").Error(err)
	}
}

func PenaltyOrRefundClientAfterCancelledOrder(offerState *OfferStatesDrv) {
	var cancelledByClient bool
	if offerState.Comment == constants.CancelReasonClient { // cancelled from CRM
		cancelledByClient = true
	}
	if !cancelledByClient { // else no need to inspect further, it's client's fault
		for _, reason := range constants.ListClientCancelReasons() { // cancelled from app
			if offerState.Comment == reason {
				cancelledByClient = true
				break
			}
		}
	}

	if !cancelledByClient { // not a client's fault, refund him
		RefundClientAfterCancelledOrder(offerState)
		return
	}

	// Definitely a client's fault, penalty him
	PenaltyClientForCancelledOrder(offerState)
}

func PenaltyClientForCancelledOrder(offerState *OfferStatesDrv) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "penalty client for cancelled order",
		"orderUUID": offerState.OrderUUID,
	})

	var order OrderDrv
	if err := GetByUUID(offerState.OrderUUID, &order); err != nil {
		log.WithField("reason", "failed to get order by uuid").Error(err)
		return
	}

	if order.Service.ProductDelivery || // no need to penalty
		order.GetPaymentType() != constants.OrderPaymentTypeCard ||
		!order.PaymentMeta.IsPrepaid {
		return // no need to continue
	}

	if order.Client.UUID == "" {
		log.Error("empty client uuid for an order with card payment")
		return
	}

	penalty := mandatoryPrepay
	err := rabsender.SendJSONByAction(rabsender.AcPenaltyClient, "", structures.PrepayOrder{ // reuse existing struct
		IdempotencyKey: structures.GenerateUUID(),
		PayerUUID:      order.Client.UUID,
		SubjectUUID:    offerState.OrderUUID,
		Amount:         float64(penalty),
		PayeeUUID:      offerState.DriverUUID,
		Meta:           structures.PrepayOrderMeta{OfferUUID: offerState.OfferUUID},
	})
	if err != nil {
		log.WithField("reason", "failed to send a message via RMQ").Error(err)
		return
	}

	// Generate a receipt
	GenerateReceipt(&order, penalty, lblCancelledOrder)
}

func RefundClientAfterCancelledOrder(offerState *OfferStatesDrv) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "refund client after cancelled order",
		"orderUUID": offerState.OrderUUID,
	})

	var order OrderDrv
	if err := GetByUUID(offerState.OrderUUID, &order); err != nil {
		log.WithField("reason", "failed to get order by uuid").Error(err)
		return
	}

	if order.Service.ProductDelivery || // no possibility to refund
		order.GetPaymentType() != constants.OrderPaymentTypeCard ||
		!order.PaymentMeta.IsPrepaid {
		return // no need to continue
	}

	if order.Client.UUID == "" {
		log.Error("empty client uuid for an order with card payment")
		return
	}

	err := rabsender.SendJSONByAction(rabsender.AcRefundClient, "", structures.PrepayOrder{ // reuse existing struct
		IdempotencyKey: structures.GenerateUUID(),
		PayerUUID:      order.Client.UUID,
		PayeeUUID:      order.Client.UUID, // payee intentionally equals to payer
		SubjectUUID:    offerState.OrderUUID,
		Meta:           structures.PrepayOrderMeta{OfferUUID: offerState.OfferUUID},
	})
	if err != nil {
		log.WithField("reason", "failed to send a message via RMQ").Error(err)
		return
	}
}

//GetBillingData возвращает заполненный объект данных для биллинга
func GetBillingData(driver DriversApps, offerUUID string) (structures.BillingData, error) {
	offer, err := GetOfferByUUID(offerUUID)
	if err != nil {
		return structures.BillingData{}, err
	}
	order := offer.TaximetrData.Order

	insuranceCost := order.Tariff.InsuranceCost
	// insurance cost is is already included in TotalOrderPrice, so we should
	// increase guaranteedDriverIncome to compensate
	guaranteedDriverIncome := order.Tariff.GuaranteedDriverIncome + insuranceCost

	orderPrice := order.Tariff.TotalPrice
	var actualOrderPrice *int
	var driverCompensation int
	// Process unpaid and usual orders differently
	isUnpaid := order.Promotion.IsUnpaid
	if isUnpaid {
		actualOrderPrice = new(int)                                                      // set to zero
		orderPrice = int(math.Max(float64(guaranteedDriverIncome), float64(orderPrice))) // make a guaranteed income count
		driverCompensation = orderPrice                                                  // full price compensation
	} else if guaranteedDriverIncome > orderPrice &&
		driver.CanReceiveGuaranteedIncome(GetCurrentBlockingConfig().GuaranteedDriverIncomeActivityLimit) { // an order with lowered price
		actualOrderPrice = new(int)
		*actualOrderPrice = orderPrice // remember the original price
		driverCompensation = guaranteedDriverIncome - orderPrice
		orderPrice = guaranteedDriverIncome // properly calculate commission
	}

	billingData := structures.BillingData{
		OrderPrice:               orderPrice - insuranceCost, // to exclude insurance cost from commission calculations
		OfferAcceptedFromOffline: offer.AcceptedFromOffline,
		DriverCompensation:       driverCompensation,
		ActualOrderPrice:         actualOrderPrice,
		WaitingPrice:             order.Tariff.WaitingPrice,
		InsuranceCost:            insuranceCost,
	}
	return billingData, nil
}

func WithdrawDriverForCancelledOrder(offerState structures.OfferStates) error {
	var order OrderDrv
	if err := GetByUUID(offerState.OrderUUID, &order); err != nil {
		return errors.Wrap(err, "failed to get an order")
	}
	tp, err := GetTaxiParkFromCRM(order.GetTaxiParkUUID())
	if err != nil {
		return errors.Wrap(err, "failed to get an owner")
	}
	transaction := TransactionDriver{
		TransactionType: structures.TransferTypeWithdraw,
		OwnerName:       tp.Name,
		OfferUUID:       offerState.OfferUUID,
		OrderUUID:       offerState.OrderUUID,
		DriverUUID:      offerState.DriverUUID,
		ClientUUID:      order.Client.UUID,
		ClientPhone:     order.GetClientPhone(),
		Event:           msgCancelOrderAction,
		Amount:          penaltyForCancelledOrder,
	}
	CreateDriverTransactions(transaction)
	return nil
}

func UpdateDriverBalance(transfer structures.CompletedTransfer) error {
	if transfer.PayeeType != structures.UserTypeDriver &&
		transfer.PayerType != structures.UserTypeDriver {
		return nil
	}

	if !(transfer.PayerAccountType == structures.AccountTypeCard ||
		transfer.PayerAccountType == structures.AccountTypeBonus ||
		transfer.PayeeAccountType == structures.AccountTypeCard ||
		transfer.PayeeAccountType == structures.AccountTypeBonus) {
		return nil
	}

	if transfer.PayeeType == structures.UserTypeDriver && transfer.PayeeBalance != structures.BillingSkipUpdate {
		driverUUID := transfer.PayeeUUID
		newBalance := structures.RoundUp(transfer.PayeeBalance)

		if err := setDriverBalance(driverUUID, transfer.PayeeAccountType, newBalance); err != nil {
			return errors.Wrapf(err, "failed to update driver's %s balance", driverUUID)
		}
	}

	if transfer.PayerType == structures.UserTypeDriver && transfer.PayerBalance != structures.BillingSkipUpdate {
		driverUUID := transfer.PayerUUID
		newBalance := structures.RoundUp(transfer.PayerBalance)

		if err := setDriverBalance(driverUUID, transfer.PayerAccountType, newBalance); err != nil {
			return errors.Wrapf(err, "failed to update driver's %s balance", driverUUID)
		}

		if transfer.PayerAccountType == structures.AccountTypeCard {
			// release transfer amount
			if cardBalanceHold, found := CurrentDriverCardBalanceHolders[driverUUID]; found {
				cardBalanceHold.Release(transfer.ID)
			}
		}
	}

	return nil
}

func ChangeOrderPaymentTypeToCash(offerUUID, orderUUID string) error {
	// Change offer's order
	var offer OfferDrv
	_, err := db.Model(&offer).
		Set(
			fmt.Sprintf(
				"taximetr_data = jsonb_set(COALESCE(taximetr_data, '{}'), '{order,payment_type}', '\"%s\"')",
				constants.OrderPaymentTypeCash,
			),
		).
		Where("uuid = ?", offerUUID).
		Returning("*").
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment type", orderUUID)
	}
	_, err = db.Model(&offer).
		Set(
			fmt.Sprintf(
				"taximetr_data = jsonb_set(COALESCE(taximetr_data, '{}'), '{order,tariff,payment_type}', '\"%s\"')",
				constants.OrderPaymentTypeRU[constants.OrderPaymentTypeCash],
			),
		).
		Where("uuid = ?", offerUUID).
		Returning("*").
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment type", orderUUID)
	}

	// Change order itself
	_, err = db.Model((*OrderDrv)(nil)).
		Set("payment_type = ?", constants.OrderPaymentTypeCash).
		Set(fmt.Sprintf("tariff = jsonb_set(COALESCE(tariff, '{}'), '{payment_type}', '\"%s\"')", constants.OrderPaymentTypeRU[constants.OrderPaymentTypeCash])).
		Where("uuid = ?", orderUUID).
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment type", orderUUID)
	}

	offer.InformDriverAboutOrderUpdate(msgChangePaymentTypeToCash)

	return nil
}

func TransferCardBalanceToBonus(ctx context.Context, driverUUID string, amount float64) error {
	return balanceTransferJobs.Execute(uuidHash(driverUUID), func() error {
		if amount <= 0 {
			return errors.New("Сумма должна быть положительной")
		}

		driver, err := GetDriverByUUID(driverUUID)
		if err != nil {
			return errors.Wrap(err, "failed to get a driver by uuid")
		}

		if driver.CardBalance < amount {
			return errors.Errorf(
				"У вас недостаточно средств. Необходимо иметь %d руб., а ваш баланс равен %d руб.",
				int64(amount), int64(driver.CardBalance),
			)
		}

		// get or init holder
		holder, found := CurrentDriverCardBalanceHolders[driverUUID]
		if !found {
			holder = NewCardBalanceHolder()
			CurrentDriverCardBalanceHolders[driverUUID] = holder
		}

		holdAmount := holder.GetHoldSum()
		if driver.CardBalance-holdAmount < amount {
			return errors.Errorf(
				"У вас недостаточно средств. В настоящий момент идет обработка транзакций на сумму %d руб.",
				int64(holdAmount),
			)
		}

		// If all is ok, then actually make a transfer
		go func() {
			transfer := proto.NewTransfer{
				NewTransfer: structures.NewTransfer{
					IdempotencyKey:   structures.GenerateUUID(),
					TransferType:     structures.TransferTypePayment,
					PayerUUID:        driverUUID,
					PayerType:        structures.UserTypeDriver,
					PayerAccountType: structures.AccountTypeCard,
					PayeeUUID:        driverUUID,
					PayeeType:        structures.UserTypeDriver,
					PayeeAccountType: structures.AccountTypeBonus,
					Amount:           amount,
					Description:      "Перевод с безналичного счета на бонусный",
				},
			}
			if err := rabsender.SendJSONByAction(rabsender.AcMakeDriverTransfer, "", transfer); err != nil {
				logs.LoggerForContext(ctx).WithField("event", "moving driver balance").Error(err)
				return
			}

			// put transfer amount on hold
			holder.Hold(transfer.IdempotencyKey, transfer.Amount)

			logs.LoggerForContext(ctx).WithField("event", "moving driver balance sent to RMQ").Debug("OK")
		}()

		return nil
	})
}

func setDriverBalance(driverUUID string, accountType structures.BillingAccountType, newBalance float64) error {
	q := db.Model((*DriversApps)(nil)).
		Where("uuid = ?", driverUUID)
	if accountType == structures.AccountTypeCard {
		q = q.Set("card_balance = ?", newBalance)
	} else {
		q = q.Set("balance = ?", newBalance)
	}

	_, err := q.Update()
	return err
}
