package models

import (
	"context"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

type BlockingConfig struct {
	structures.BlockingConfig
	mx sync.Mutex
}

var (
	currentBlockingConfig = BlockingConfig{
		BlockingConfig: structures.BlockingConfig{
			BlockDriverActivity:                 structures.DefaultBlockDriverActivity,
			GuaranteedDriverIncomeActivityLimit: structures.DefaultGuaranteedDriverIncomeActivityLimit,
		},
	}
)

func SetCurrentBlockingConfig(config structures.BlockingConfig) {
	currentBlockingConfig.mx.Lock()
	currentBlockingConfig.BlockingConfig = config
	currentBlockingConfig.mx.Unlock()
}

func GetCurrentBlockingConfig() structures.BlockingConfig {
	currentBlockingConfig.mx.Lock()
	defer currentBlockingConfig.mx.Unlock()

	return currentBlockingConfig.BlockingConfig
}

func BlockDriverIfNeeded(offerState structures.OfferStates) error {
	if offerState.DriverUUID == "" {
		return nil
	}

	prevState, err := getPreviousDriverState(offerState.DriverUUID)
	if err != nil {
		return errpath.Err(err, "failed to get a driver previous state")
	}

	// когда принимается встречный заказ, у водилы предыдущий статус бывает working
	// meta: когда принимается обычный заказ, у водилы предыдущий статус бывает online
	var rejectCounterOrder bool = false // нужна для ингнорирования наказания при отказе от встречного заказа
	if prevState.State == constants.DriverStateWorking {
		rejectCounterOrder = true
	}

	if !(offerState.State == constants.OrderStateRejected ||
		offerState.State == constants.OrderStateCancelled) &&
		rejectCounterOrder {
		return nil
	}

	driver, err := GetDriverByUUID(offerState.DriverUUID)
	if err != nil {
		return errors.Wrap(err, "failed to get a driver by uuid")
	}

	// Try to block for low activity
	if driver.Activity < GetCurrentBlockingConfig().BlockDriverActivity {
		if err := blockForLowActivity(&driver); err != nil {
			return errors.Wrap(err, "failed to block a driver for low activity")
		}
		return nil
	}

	return blockForRejectionIfNeeded(&driver, offerState)
}

func blockForLowActivity(driver *DriversApps) error {
	if err := SetDriverBlockingMeta(driver, 0, false); err != nil {
		return errors.Wrap(err, "failed to set driver blocking meta")
	}

	comment := "Заблокирован за низкое значение активности"
	_, err := SetDriverStatus(driver.UUID, constants.DriverStateBlocked, comment)
	if err != nil {
		return errors.Wrap(err, "failed to set driver status")
	}
	return nil
}

func blockForRejectionIfNeeded(driver *DriversApps, offerState structures.OfferStates) error {
	if offerState.State != constants.OrderStateRejected {
		return nil
	}

	offer, err := GetOfferByUUID(offerState.OfferUUID)
	if err != nil {
		return errors.Wrap(err, "failed to get an offer by UUID")
	}

	// Skip optional offers
	if offer.TaximetrData.OfferData.IsFree || offer.TaximetrData.Order.ItIsOptional() {
		return nil
	}

	// Try to block for rejections
	rejectionCount, err := CountDriverRejectedOffersForDay(driver.UUID)
	if err != nil {
		return errors.Wrap(err, "failed to count driver rejections count")
	}

	for limit, blockPeriod := range GetCurrentBlockingConfig().RejectionTable {
		if rejectionCount == limit { // need to block a driver
			if err := SetDriverBlockingMeta(driver, blockPeriod, true); err != nil {
				return errors.Wrap(err, "failed to set driver blocking meta")
			}

			comment := fmt.Sprintf("Заблокирован за %d отказов на %s", rejectionCount, blockPeriod)
			_, err := SetDriverStatus(driver.UUID, constants.DriverStateBlocked, comment)
			if err != nil {
				return errors.Wrap(err, "failed to set driver status")
			}
			return nil
		}
	}

	return nil
}

func SetDriverBlockingMeta(driver *DriversApps, period time.Duration, temporary bool) error {
	now := time.Now()
	if temporary {
		driver.Meta.BlockedAt = now.Unix()
		driver.Meta.BlockedUntil = now.Add(period).Unix()
		driver.Meta.UnblockedAt = 0
	} else {
		driver.Meta.BlockedAt = now.Unix()
		driver.Meta.BlockedUntil = 0
		driver.Meta.UnblockedAt = 0
	}

	_, err := db.Model(driver).
		Set("meta = ?meta").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update blocking meta")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcDriverDataUpdate, "", driver); err != nil {
		return errors.Wrap(err, "failed to publish driver changes")
	}
	return nil
}

func UnblockExpiredDrivers(ctx context.Context) error {
	var driversToUnblock []*DriversApps
	err := db.ModelContext(ctx, &driversToUnblock).
		Where("(meta ->> 'blocked_until')::bigint != 0").
		Where("(meta ->> 'blocked_until')::bigint < ?", time.Now().Unix()).
		Where("(meta ->> 'unblocked_at')::bigint < (meta ->> 'blocked_until')::bigint").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to select drivers to unblock")
	}

	// Ensure drivers are blocked
	driverUUIDs := make([]string, 0, len(driversToUnblock))
	for _, drv := range driversToUnblock {
		driverUUIDs = append(driverUUIDs, drv.UUID)
	}
	drvStates, err := GetCurrentDriversStatesFromCRM(driverUUIDs)
	if err != nil {
		return errors.Wrap(err, "failed to get driver states from CRM")
	}

	reason := "Разблокирован по прошествии времени блокировки"
	for _, drv := range driversToUnblock {
		if state, found := drvStates[drv.UUID]; found {
			flushState := state == constants.DriverStateBlocked
			if err := unblockDriver(ctx, drv, reason, flushState); err != nil {
				return errors.Wrap(err, "failed to unblock a driver")
			}
		}
	}

	return nil
}

func UnblockDriverIfNeeded(ctx context.Context, driver *DriversApps) error {
	if driver.Meta.BlockedUntil != 0 { // blocked for rejections
		return nil
	}

	currentState, err := GetCurrentDriverStateFromCRM(driver.UUID)
	if err != nil {
		return errors.Wrap(err, "failed to get current driver status from CRM")
	}

	if currentState != constants.DriverStateBlocked {
		return nil
	}

	// Check if need to unblock a driver
	if driver.Activity < GetCurrentBlockingConfig().BlockDriverActivity {
		return nil
	}

	reason := "Разблокирован после покупки необходимой активности"
	if err := unblockDriver(ctx, driver, reason, true); err != nil {
		return errors.Wrap(err, "failed to unblock a driver")
	}

	return nil
}

func unblockDriver(ctx context.Context, driver *DriversApps, reason string, flushState bool) error {
	if flushState {
		_, err := SetDriverStatus(driver.UUID, constants.DriverStateAvailable, reason)
		if err != nil {
			return errors.Wrap(err, "failed to set driver status")
		}
	}

	driver.Meta.UnblockedAt = time.Now().Unix()
	_, err := db.ModelContext(ctx, driver).
		Set("meta = ?meta").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update blocking meta")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcDriverDataUpdate, "", driver); err != nil {
		return errors.Wrap(err, "failed to publish driver changes")
	}
	return nil
}

func UpdateBlockingConfig(context.Context) error {
	cfg, err := GetBlockingConfigFromCRM()
	if err != nil {
		return errors.Wrap(err, "failed to get blocking config from CRM")
	}

	SetCurrentBlockingConfig(cfg)
	return nil
}

const (
	blockingConfigEndpoint = "/config/blocking"
)

func GetBlockingConfigFromCRM() (structures.BlockingConfig, error) {
	var result structures.BlockingConfig
	if err := request(http.MethodGet, config.St.CRM.Host+blockingConfigEndpoint, nil, &result); err != nil {
		return structures.BlockingConfig{}, errors.Wrap(err, "failed to fetch blocking config")
	}
	return result, nil
}
