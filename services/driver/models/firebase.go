package models

import (
	"errors"
	"fmt"
	"time"

	"github.com/go-pg/pg"
)

type (
	//DriverFirebaseToken firebase ключи водителей
	DriverFirebaseToken struct {
		tableName  struct{}  `sql:"drv_firebase_tokens"`
		ID         int       `json:"id" sql:",pk"`
		DriverUUID string    `json:"driver_uuid" sql:",unique"`
		Token      string    `json:"token"`
		UpdatedAt  time.Time `json:"updated_at" `
	}
)

// Save сохраняет в базу токен. Если для водителя уже есть токен - обновляет его
func (drFT DriverFirebaseToken) Save() error {
	drFT.UpdatedAt = time.Now()

	_, err := db.Model(&drFT).
		OnConflict("(driver_uuid) DO UPDATE").
		Insert()
	return err
}

// GetDriversFCMTokens возвращает все токены водителей по переданному массива идентификаторов.
// Чтобы получить вообще все токены, передай вторым параметром true
// Возвращается map[uuid]token
func GetDriversFCMTokens(driversUUID []string) (map[string]string, error) {
	if len(driversUUID) == 0 {
		return nil, errors.New("empty driversuuid")
	}

	drvFT := make([]DriverFirebaseToken, 0)
	query := db.Model(&drvFT).Column("token", "driver_uuid").Where("driver_uuid IN (?)", pg.In(driversUUID))
	if err := query.Select(); err != nil {
		return nil, fmt.Errorf("error finding current driver token,%s", err)
	}

	uuidFeatToken := make(map[string]string)
	for _, tok := range drvFT {
		uuidFeatToken[tok.DriverUUID] = tok.Token
	}

	return uuidFeatToken, nil
}

//GetCurrentDriverFCMToken возвращает актуальный fcm токен водителя
func GetCurrentDriverFCMToken(driverUUID string) (string, error) {
	var (
		drvFT DriverFirebaseToken
	)
	err := db.Model(&drvFT).
		Where("driver_uuid = ?", driverUUID).
		Select()
	if err != nil {
		return drvFT.Token, fmt.Errorf("error finding current driver token,%s", err)
	}
	return drvFT.Token, err
}

//GetCurrentDriversFCMToken возвращает актуальные fcm токены водитлей
func GetCurrentDriversFCMToken(driversUUID []string) (map[string]string, error) {
	var (
		drvFT []DriverFirebaseToken
	)
	tokens := make(map[string]string)
	err := db.Model(&drvFT).Where("driver_uuid in (?)", pg.In(driversUUID)).Select()
	if err != nil {
		return tokens, fmt.Errorf("error finding current driver token,%s", err)
	}
	for _, tokenData := range drvFT {
		tokens[tokenData.DriverUUID] = tokenData.Token
	}
	return tokens, nil
}
