package models

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
)

const taxiParkByUUIDEndpoint = "/taxiparks/"

func GetTaxiParkFromCRM(uuid string) (structures.TaxiPark, error) {
	var res structures.TaxiPark
	url := config.St.CRM.Host + taxiParkByUUIDEndpoint + uuid
	err := request(http.MethodGet, url, nil, &res)
	return res, err
}
