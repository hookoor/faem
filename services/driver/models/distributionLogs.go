package models

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"time"
)

// FIXME @maurdekhai: отрефакторить всё это говно

var DistributionLogs = make(chan DistributionLog)

type eventType string

const (
	eventTypeAssign eventType = "assign"
	eventTypeAccept eventType = "accept"
	eventTypeReject eventType = "reject"
)

type DistributionLog struct {
	tableName struct{} `sql:"drv_distribution_logs" pg:",discard_unknown_columns"`

	DriverUUID string
	OfferUUID  string
	OrderUUID  string
	EventType  eventType
	IsCounter  bool `sql:",notnull"`
	Distance   float64
	Round      *int
	Radius     float64
	CreatedAt  time.Time
}

func InsertDistributionLogs(ctx context.Context, logs []DistributionLog) error {
	query := db.ModelContext(ctx, &logs)
	if _, err := query.Insert(); err != nil {
		return err
	}
	return nil
}

func NewAssignLog(driverUUID, offerUUID, orderUUID string, isCounter bool, round int, radius, distance float64) DistributionLog {
	return DistributionLog{
		EventType:  eventTypeAssign,
		DriverUUID: driverUUID,
		OfferUUID:  offerUUID,
		OrderUUID:  orderUUID,
		IsCounter:  isCounter,
		Distance:   distance,
		Round:      &round,
		Radius:     radius,
		CreatedAt:  time.Now(),
	}
}

func NewAcceptLog(os structures.OfferStates) DistributionLog {
	isCounter, err := getCounterOrderMarker(os.OrderUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "NewAcceptLog",
		}).WithError(err).Error()
	}
	return DistributionLog{
		EventType:  eventTypeAccept,
		DriverUUID: os.DriverUUID,
		OfferUUID:  os.OfferUUID,
		OrderUUID:  os.OrderUUID,
		IsCounter:  isCounter,
		CreatedAt:  time.Now(),
	}
}

func NewRejectLog(os structures.OfferStates) DistributionLog {
	isCounter, err := getCounterOrderMarker(os.OrderUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "NewRejectLog",
		}).WithError(err).Error()
	}
	return DistributionLog{
		EventType:  eventTypeReject,
		DriverUUID: os.DriverUUID,
		OfferUUID:  os.OfferUUID,
		OrderUUID:  os.OrderUUID,
		IsCounter:  isCounter,
		CreatedAt:  time.Now(),
	}
}
