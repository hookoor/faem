package models

import (
	"context"
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
)

type (
	// InitData возращает структуру с данными инициализации
	InitData struct {
		DriverUUID        string             `json:"driver_uuid"`
		Alias             int                `json:"alias"`
		OperatorPhone     string             `json:"operator_phone"`
		CurrentState      DriverStateNotify  `json:"driver_state"`
		WebViewHost       string             `json:"web_view_host"`
		PhotoControlsData []PhotoControlData `json:"photocontrols_data"`
		OrderData         *OfferForDriver    `json:"order_data,omitempty"`
		CounterOrder      *OfferForDriver    `json:"counter_order_data,omitempty"`
		Balance           struct {
			Balance     float64 `json:"balance"`
			CardBalance float64 `json:"card_balance"`
			Message     string  `json:"message,omitempty"`
		} `json:"balance"`
	}
	// PhotoControlData - для get init data
	PhotoControlData struct {
		UUID         string               `json:"uuid"`
		ControlType  string               `json:"control_type"`
		Title        string               `json:"title"`
		Status       PhotocontrolStatuses `json:"status"`
		WarningTime  int64                `json:"warning_time"`
		TimeValidity int64                `json:"time_validity"`
		// Notified     bool                 `json:"notified"` // был ли водидель уведомлен пушом
	}

	//ExchangeType godoc
	ExchangeType struct {
		ExcName    string `json:"name"`
		RoutingKey string `json:"routing_key"`
		Mandatory  bool   `json:"mandatory"`
		Immediate  bool   `json:"immediate"`
	}

	// ConsumerType структура для подключения к очереди
	ConsumerType struct {
		Queue     string `json:"queue"`
		Consumer  string `json:"consumer"`
		AutoAck   bool   `json:"auto_ack"`
		Exclusive bool   `json:"exclusive"`
		NoLocal   bool   `json:"no_local"`
		NoWait    bool   `json:"no_wait"`
		Args      string `json:"args"`
	}
)

// GetInitData возвращает данные для подключения
func GetInitData(ctx context.Context, driverUUID string) (InitData, error) {
	var (
		data InitData
		err  error
	)
	currentState, err := GetCurrentDriverStateFromCRM(driverUUID)
	if err != nil {
		return InitData{}, fmt.Errorf("error getting current state, %s", err)
	}
	data.CurrentState.Value = currentState
	switch data.CurrentState.Value {
	case constants.DriverStateOnModerationAfterChange:
		data.CurrentState.Message = "Ваш аккаунт на модерации после изменения данных. Обратитесь к администратору"
	case constants.DriverStateOnModeration:
		data.CurrentState.Message = "Ваш аккаунт на модерации. Обратитесь к администратору"
	case constants.DriverStateBlocked:
		data.CurrentState.Message = "Ваш аккаунт заблокирован. Обратитесь к администратору"
		state, _ := GetCurrentDriverState(driverUUID)
		if state.BlockedAutomatically() {
			data.CurrentState.Message = fmt.Sprintf("Ваш аккаунт заблокирован. Причина: %s", state.Comment)
		}
	default:
		data.CurrentState.Message = ""
	}

	data.DriverUUID = driverUUID
	data.WebViewHost = config.St.WebView.Host
	conf, err := GetCurrentConfig()
	if err != nil {
		return InitData{}, fmt.Errorf("error getting current config, %s", err)
	}
	data.OperatorPhone = conf.OperatorPhone
	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return InitData{}, fmt.Errorf("error getting driver,%s", err)
	}
	data.Alias = driver.Alias

	{ // заполнение данных по фотоконтролю
		var controls []Photocontrol

		err := db.ModelContext(ctx, &controls).
			Where("driver ->> 'uuid' = ?", driverUUID).
			Where("status = ?", PhotocontrolCreated).
			Where("deleted is not true").
			Order("created_at").
			Select()
		if err != nil {
			return data, errpath.Err(err)
		}

		if len(controls) != 0 {
			for _, el := range controls {
				data.PhotoControlsData = append(data.PhotoControlsData, PhotoControlData{
					UUID:         el.UUID,
					ControlType:  el.Name,
					Status:       el.Status,
					WarningTime:  el.WarningTime,
					TimeValidity: el.TimeValidity,
					Title:        el.Title,
				})
			}
		}

	}

	data.Balance.Balance = driver.Balance
	data.Balance.CardBalance = driver.CardBalance
	if driver.DrvTariff.TariffType != structures.DriverTariffPeriod && driver.GetTotalBalance() < lowBalance {
		data.Balance.Message = "У вас низкий баланс. Некоторые заказы могут быть недоступны."
	}
	return data, nil
}

func getDriverAlias(driverUUID string) (int, error) {
	var (
		drvApp DriversApps
	)
	err := db.Model(&drvApp).
		Where("deleted is not true AND uuid = ?", driverUUID).
		First()
	return drvApp.Alias, err
}
