package models

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"sync"
	"time"
)

type BalanceHold struct {
	TransferID string
	Amount     float64
	CreatedAt  time.Time
}

type CardBalanceHolder struct {
	mx    sync.RWMutex
	holds map[string]BalanceHold
}

func NewCardBalanceHolder() *CardBalanceHolder {
	var holder CardBalanceHolder

	holder.holds = make(map[string]BalanceHold)

	return &holder
}

func (holder *CardBalanceHolder) Hold(transferID string, amount float64) {
	holder.mx.Lock()
	holder.holds[transferID] = BalanceHold{
		TransferID: transferID,
		Amount:     amount,
		CreatedAt:  time.Now(),
	}
	holder.mx.Unlock()
}

func (holder *CardBalanceHolder) Release(transferID string) {
	holder.mx.Lock()
	delete(holder.holds, transferID)
	holder.mx.Unlock()
}

func (holder *CardBalanceHolder) GetHoldSum() float64 {
	holder.mx.RLock()
	sum := 0.
	for _, hold := range holder.holds {
		sum += hold.Amount
	}
	holder.mx.RUnlock()

	return sum
}

func (holder *CardBalanceHolder) Refresh(lifetimeThreshold time.Duration) {
	holder.mx.Lock()

	now := time.Now()
	for id, hold := range holder.holds {
		if now.Sub(hold.CreatedAt) > lifetimeThreshold {
			logs.Eloger.WithFields(logrus.Fields{
				"event":       "refresh card balances hold",
				"transfer_id": hold.TransferID,
			}).Warn("deleting expired card balance hold")
			delete(holder.holds, id)
		}
	}

	holder.mx.Unlock()
}

var CurrentDriverCardBalanceHolders = make(map[string]*CardBalanceHolder)
