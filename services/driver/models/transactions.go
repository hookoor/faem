package models

import (
	"math"

	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

//TransactionDriver транзакции водителей
type TransactionDriver struct {
	TransactionType          structures.BillingTransferType
	Event                    string
	Amount                   float64
	TotalOrderPrice          float64
	WithFee                  bool
	OwnerName                string
	TaxiParkUUID             string
	OfferUUID                string
	OrderUUID                string
	PayeeType                structures.BillingUserType
	PayerType                structures.BillingUserType
	PayerUUID                string
	PayeeUUID                string
	PayerAccountType         structures.BillingAccountType
	PayeeAccountType         structures.BillingAccountType
	DriverUUID               string
	ClientUUID               string
	ClientPhone              string
	Tariff                   structures.TariffMetadata
	TaxiPark                 structures.TaxiParkMetadata
	Prepaid                  bool
	CurrentDriverBalance     *float64
	CurrentDriverCardBalance *float64
}

func (t *TransactionDriver) Skip() bool {
	if t == nil {
		return true
	}
	return (!t.WithFee && t.Amount <= 0) || (t.WithFee && t.Amount <= 0 && t.TotalOrderPrice <= 0)
}

//SetPayerAndPayeeData sets default payee and payer data for transaction
func (t *TransactionDriver) SetPayerAndPayeeData() {
	if t == nil {
		return
	}
	if t.PayeeType == "" {
		t.PayeeType = structures.UserTypeDriver
	}
	if t.PayerType == "" {
		t.PayerType = structures.UserTypeClient
	}
	if t.PayeeUUID == "" {
		t.PayeeUUID = t.DriverUUID
	}
	if t.PayerUUID == "" {
		t.PayerUUID = t.GetClientUUIDOrPhone()
	}
}

func (t *TransactionDriver) GetPayerAccountType() structures.BillingAccountType {
	if t == nil || t.PayerAccountType == "" {
		return structures.AccountTypeCash
	}
	return t.PayerAccountType
}

func (t *TransactionDriver) GetClientUUIDOrPhone() string {
	if t == nil {
		return ""
	}
	if t.ClientUUID != "" {
		return t.ClientUUID
	}
	return t.ClientPhone
}

type TransactionClient struct {
	TransactionType structures.BillingTransferType
	Event           string
	Amount          float64
	OwnerName       string
	OfferUUID       string
	OrderUUID       string
	DriverUUID      string
	ClientUUID      string
	ClientPhone     string
	Tariff          structures.TariffMetadata
	AccountType     structures.BillingAccountType
}

func (t *TransactionClient) GetAccountType() structures.BillingAccountType {
	if t == nil || t.AccountType == "" {
		return structures.AccountTypeCash
	}
	return t.AccountType
}

func (t *TransactionClient) GetClientUUIDOrPhone() string {
	if t == nil {
		return ""
	}
	if t.ClientUUID != "" {
		return t.ClientUUID
	}
	return t.ClientPhone
}

// CreateDriverTransactions применяет переданную транзакцию
func CreateDriverTransactions(transactions ...TransactionDriver) {
	for _, tr := range transactions {
		log := logs.Eloger.WithFields(logrus.Fields{
			"driverUUID":       tr.DriverUUID,
			"transactionEvent": tr.Event,
		})

		if tr.Skip() { // no need to change balance
			log.Debug("no need to change balance")
			continue
		}

		// Process the transaction in billing service
		newTransfer := structures.NewTransfer{
			IdempotencyKey: structures.GenerateUUID(),
			TransferType:   tr.TransactionType,
			Description:    tr.Event,
			PayeeType:      tr.PayeeType,
			Meta: structures.TransferMetadata{
				OwnerName:                tr.OwnerName,
				OfferUUID:                tr.OfferUUID,
				OrderUUID:                tr.OrderUUID,
				DriverUUID:               tr.DriverUUID,
				ClientID:                 tr.ClientUUID,
				ClientPhone:              tr.ClientPhone,
				DriverTariff:             tr.Tariff,
				DriverTaxiPark:           tr.TaxiPark,
				Prepaid:                  tr.Prepaid,
				CurrentDriverBalance:     tr.CurrentDriverBalance,
				CurrentDriverCardBalance: tr.CurrentDriverCardBalance,
			},
		}
		switch {
		case tr.TransactionType == structures.TransferTypePayment:
			tr.SetPayerAndPayeeData()

			newTransfer.PayerAccountType = tr.GetPayerAccountType()
			newTransfer.PayeeAccountType = tr.PayeeAccountType
			newTransfer.PayeeUUID = tr.PayeeUUID
			newTransfer.PayeeType = tr.PayeeType
			newTransfer.PayerUUID = tr.PayerUUID
			newTransfer.PayerType = tr.PayerType
			newTransfer.Amount = math.Abs(tr.Amount)
			if tr.WithFee { // we need to take a fee
				newTransfer.Amount = math.Abs(tr.TotalOrderPrice)
				newTransfer.Fee.Amount = math.Abs(tr.Amount)
				newTransfer.Fee.AccountType = structures.AccountTypeBonus
				newTransfer.Fee.PayerType = newTransfer.PayeeType
				newTransfer.Fee.PayerUUID = newTransfer.PayeeUUID
			}
		case tr.TransactionType == structures.TransferTypeWithdraw:
			newTransfer.PayerAccountType = structures.AccountTypeBonus
			newTransfer.PayerUUID = tr.DriverUUID
			newTransfer.PayerType = structures.UserTypeDriver
			newTransfer.Amount = math.Abs(tr.Amount)
		case tr.TransactionType == structures.TransferTypeTopUp:
			newTransfer.PayerAccountType = structures.AccountTypeBonus
			newTransfer.PayeeUUID = tr.DriverUUID
			newTransfer.PayeeType = structures.UserTypeDriver
			newTransfer.Amount = math.Abs(tr.Amount)
		default:
			log.Errorf("unexpected transfer type: %s", tr.TransactionType)
			return
		}

		go func() {
			if err := rabsender.SendJSONByAction(rabsender.AcMakeDriverTransfer, "", newTransfer); err != nil {
				log.WithField("event", "making a driver transfer").Error(err)
				return
			}
			log.WithField("event", "transfer sent to RMQ").Debug("OK")
		}()
	}
}

func CreateClientTransactions(transactions ...TransactionClient) {
	for _, tr := range transactions {
		log := logs.Eloger.WithFields(logrus.Fields{
			"client_uuid_or_phone": tr.GetClientUUIDOrPhone(),
			"transactionEvent":     tr.Event,
		})

		if tr.Amount == 0 { // no need to change balance
			log.Debug("no need to change balance")
			continue
		}

		if tr.TransactionType == structures.TransferTypePayment { // not allowed for now
			log.Debug("client transaction payment is not allowed")
			continue
		}

		// Process the transaction in billing service
		newTransfer := structures.NewTransfer{
			IdempotencyKey: structures.GenerateUUID(),
			TransferType:   tr.TransactionType,
			Description:    tr.Event,
			Meta: structures.TransferMetadata{
				OwnerName:    tr.OwnerName,
				OfferUUID:    tr.OfferUUID,
				OrderUUID:    tr.OrderUUID,
				DriverUUID:   tr.DriverUUID,
				ClientID:     tr.ClientUUID,
				ClientPhone:  tr.ClientPhone,
				DriverTariff: tr.Tariff,
			},
			PayerAccountType: tr.GetAccountType(),
			PayerUUID:        tr.GetClientUUIDOrPhone(),
			PayerType:        structures.UserTypeClient,
			Amount:           tr.Amount,
		}

		go func() {
			if err := rabsender.SendJSONByAction(rabsender.AcMakeDriverTransfer, "", newTransfer); err != nil {
				log.WithField("event", "making a driver transfer").Error(err)
				return
			}
			log.WithField("event", "transfer sent to RMQ").Debug("OK")
		}()
	}
}
