package models

import (
	"context"
	"time"
)

type DriverIncomeTimestamp struct {
	tableName struct{} `sql:"drv_income_stats"`

	DriverUUID     string    `json:"driver_uuid" sql:"driver_uuid"`
	HourTimestamp  time.Time `json:"hour_timestamp" sql:"hour_timestamp"`
	OrdersFinished int       `json:"orders_finished" sql:"orders_finished"`
	TotalIncome    int       `json:"total_income" sql:"total_income"`
	OffersRejected int       `json:"offers_rejected" sql:"offers_rejected"`
	LostIncome     int       `json:"lost_income" sql:"lost_income"`
}

func UpdateDriverIncomeTimestamp(ctx context.Context, incomeTimestamp *DriverIncomeTimestamp) error {
	q := db.ModelContext(ctx, incomeTimestamp).
		Where("driver_uuid = ?driver_uuid").
		Where("hour_timestamp = ?hour_timestamp")

	switch {
	case incomeTimestamp.OrdersFinished > 0:
		q = q.Set("orders_finished = orders_finished + ?orders_finished").
			Set("total_income = total_income + ?total_income")
	case incomeTimestamp.OffersRejected > 0:
		q = q.Set("offers_rejected = offers_rejected + ?offers_rejected").
			Set("lost_income = lost_income + ?lost_income")
	}

	res, err := q.Update()
	if err != nil {
		return err
	}

	// if corresponding timestamp doesn't exist
	if res.RowsAffected() == 0 {
		_, err := db.ModelContext(ctx, incomeTimestamp).Insert()
		if err != nil {
			return err
		}
	}

	return nil
}
