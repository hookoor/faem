package models

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"

	"github.com/antonmedv/expr"
	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// type Driver struct {
// 	UUID      string `json:"uuid"`
// 	Name      string `json:"name" required:"true"`
// 	Phone     string `json:"phone" description:"Телефон" required:"true"`
// 	Comment   string `json:"comment" description:"Comment"`
// 	Status    string `json:"state_name" sql:"status" description:"статус водителя"`
// 	Car       string `json:"car" description:"Машина"`
// 	Karma     int    `json:"karma" description:"Рейтинг"`
// 	Color     string `json:"color"`
// 	Tag       string `json:"tag"`
// 	RegNumber string `json:"reg_number" description:"Номера машины"`
// }

type (
	// DriversApps structure. The same table structure will be createad in DB
	DriversApps struct {
		tableName           struct{}                      `sql:"drv_apps" pg:",discard_unknown_columns"`
		ID                  int                           `json:"id" sql:",pk"`
		DeviceID            string                        `json:"device_id" description:"Login" required:"true"`
		Deleted             bool                          `json:"-" sql:"default:false"`
		OrdersQueue         string                        `json:"orders_queue" description:"Comment"`
		CreatedAt           time.Time                     `sql:"default:now()" json:"created_at"`
		UpdatedAt           time.Time                     `json:"updated_at"`
		RegistrationAddress string                        `json:"registration_address" sql:"-" description:"Адрес прописки"`
		LivingAddress       string                        `json:"living_address" sql:"-" description:"Фактическое местопроживание"`
		Password            string                        `json:"password,omitempty"`
		PaymentType         string                        `json:"payment_type" sql:"payment_type"`
		GetOrderRadius      float64                       `json:"get_order_radius" sql:"get_order_radius"`
		PhotocontrolData    []structures.PhotoControlData `json:"photocontrol_data"`
		structures.Driver
	}

	//Income структура вывода инфы о доходе водителя
	Income struct {
		EarningsToday          int                  `json:"earnings_today" description:"заработок за сегодня"`
		CountOfCompletedOrders int                  `json:"count_of_completed_orders"  description:"количество выполненных заказов"`
		Commission             int                  `json:"commission"  description:"комиссия"`
		Balance                float64              `json:"balance"`
		CardBalance            float64              `json:"card_balance"`
		Orders                 []OrderWithTimestamp `json:"orders"  description:"список всех заказов заказов"`
	}
	//OrderWithTimestamp чтобы на фронте можно было вывести дату без сложного парсинга
	OrderWithTimestamp struct {
		structures.Order
		CreatedAtTimestamp int64 `json:"created_at_timestamp"`
	}
	DriverCallTarget string

	IncomeStats struct {
		Earnings              int     `json:"earnings"`
		LostEarnings          int     `json:"lost_earnings"`
		OrdersFinished        int     `json:"orders_finished"`
		OffersRejected        int     `json:"offers_rejected"`
		OnlineTime            float64 `json:"online_time_minutes,omitempty"`
		AverageEarningPerHour float64 `json:"avg_earning_per_hour,omitempty"`
	}
)

const (
	driverLastLocationEndpoint                                 = "/drivers/getlocations"
	driversUUIDByStateEndpoint                                 = "/drivers/uuid/state/"
	markupsByUUIDEndpoint                                      = "/drivertariffs/markups/"
	manyMarkupsByUUIDEndpoint                                  = "/drivertariffs/manymarkups"
	driverByUUIDEndpoint                                       = "/drivers/"
	offlineDriverTariffEndpoint                                = "/drivertariffs/offline"
	driverCurrentStateEndpoint                                 = "/drivers/state/"
	driverCurrentStatesEndpoint                                = "/drivers/states"
	ordersCountByState                                         = "/orders/countbystate/"
	driverCurrentStateWithoutConsiderEndpoint                  = "/" + constants.InternalGroup + "/drivers/state/withoutconsider/"
	Client, Store                             DriverCallTarget = "client", "store"
)

// LocationData godoc
type LocationData struct {
	tableName struct{} `sql:"drv_locations"`
	ID        int      `sql:",pk"`
	structures.LocationData
	//DriverID   int       `json:"driver_id"`
	//DriverUUID string    `json:"driver_uuid"`
	//Latitude   float64   `json:"lat"`
	//Longitude  float64   `json:"lng"`
	//Satelites  int       `json:"sats"`
	//Timestamp  int64     `json:"time"`
	//CreatedAt  time.Time `json:"created_at" sql:"default:now()"`
}

// GetDriversUUIDsByState godoc
func GetDriversUUIDsByState(state string) ([]string, error) {
	var result []string
	uuidsAndTransTime, err := GetDriversUUIDsAndTransitionTimeByState(state)
	for uuid := range uuidsAndTransTime {
		result = append(result, uuid)
	}
	return result, err
}

// GetDriverTariffMarkups from crm
func GetDriverTariffMarkups(tariffUUID string) ([]structures.TariffMarkup, error) {
	var res []structures.TariffMarkup
	err := request(http.MethodGet, config.St.CRM.Host+markupsByUUIDEndpoint+tariffUUID, nil, &res)
	return res, err
}

// GetManyDriverTariffMarkups from crm. Map key is tariff uuid, value - related markups
func GetManyDriverTariffMarkups(tariffsUUID []string) (map[string][]structures.TariffMarkup, error) {
	res := make(map[string][]structures.TariffMarkup)
	body := structures.ManyMarkupsBody{
		TariffsUUID: tariffsUUID,
	}

	err := request(http.MethodGet, config.St.CRM.Host+manyMarkupsByUUIDEndpoint, &body, &res)
	return res, err
}

// GetDriversUUIDsAndTransitionTimeByState returns map where key is driverUUID and value is time when driver trans to this state
func GetDriversUUIDsAndTransitionTimeByState(state string) (map[string]time.Time, error) {
	result := make(map[string]time.Time)
	err := request(http.MethodGet, config.St.CRM.Host+driversUUIDByStateEndpoint+state, nil, &result)
	return result, err
}

func GetOrderCountByState(state string) (int, error) {
	type CountOrd struct {
		Count int    `json:"count"`
		State string `json:"state"`
	}
	var result CountOrd
	err := request(http.MethodGet, config.St.CRM.Host+ordersCountByState+state, nil, &result)
	if err != nil {
		return 0, err
	}
	return result.Count, nil
}

//GetIncomeData возвращает данные для раздела "доход водителя"
func GetIncomeData(uuid string) (Income, error) {
	var (
		income  Income
		orders  []structures.Order
		orderWT OrderWithTimestamp
		driver  DriversApps
	)

	err := GetByUUID(uuid, &driver)

	if err != nil {
		return income, fmt.Errorf("error getting driver,%s", err)
	}
	//переводим дробное число в проценты
	income.Commission = int(config.St.Drivers.Commission * 100)
	income.Balance = driver.Balance
	income.CardBalance = driver.CardBalance
	orders, income.EarningsToday, err = GetOrdersForDriverPerDay(driver.UUID)
	if err != nil {
		return income, fmt.Errorf("error getting orders for driver,%s", err)
	}
	for _, order := range orders {
		orderWT.Order = order
		orderWT.CreatedAtTimestamp = order.CreatedAt.Unix()
		income.Orders = append(income.Orders, orderWT)
	}
	income.CountOfCompletedOrders = len(income.Orders)
	return income, nil
}

func GetIncomeStats(uuid string, start time.Time, withDailyStats bool) (IncomeStats, error) {
	var (
		income IncomeStats
		err    error
	)

	incomeAggregated, err := GetAggregatedDriverIncome(uuid, start)
	if err != nil && err != pg.ErrNoRows {
		return income, err
	}

	income.Earnings = incomeAggregated.TotalIncome
	income.LostEarnings = incomeAggregated.LostIncome
	income.OrdersFinished = incomeAggregated.OrdersFinished
	income.OffersRejected = incomeAggregated.OffersRejected

	if withDailyStats {
		onlineTime, err := GetOnlineTime(uuid, start)
		if err != nil {
			return income, err
		}
		income.OnlineTime = onlineTime.Minutes()
		if onlineTime > time.Duration(0) {
			income.AverageEarningPerHour = float64(income.Earnings) / onlineTime.Hours()
		}
	}

	return income, nil

}

func GetDriverFromCRM(driverUUID string, authHeader map[string]string) (DriversApps, error) {
	var res DriversApps
	url := config.St.CRM.Host + driverByUUIDEndpoint + driverUUID
	err := request(http.MethodGet, url, nil, &res, authHeader)
	return res, err
}

// GetDriversForDistributionWithWaitingTime - возвращает список uuid для водителей которые подходят для распределения и количество секунд,
// прошедших с последнего перехода в онлайн
func GetDriversForDistributionWithWaitingTime() (map[string]int, error) {
	data := make(map[string]time.Time)
	url := fmt.Sprintf("%s/%s/getdriversfordistribution", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &data)

	now := time.Now().Unix()
	result := make(map[string]int)
	for drvUUID, statusUpdatedAt := range data {
		result[drvUUID] = int(now - statusUpdatedAt.Unix())
	}
	return result, err
}

// GetAllDriversUUID returns all driver ids
func GetAllDriversUUID() ([]string, error) {
	drvUUIDs := make([]string, 0)
	query := db.Model((*DriversApps)(nil)).Column("uuid").Where("deleted IS NOT TRUE")
	if err := query.Select(&drvUUIDs); err != nil {
		return nil, err
	}

	return drvUUIDs, nil
}

func getEarningsToday(offers []OfferDrv) int {
	var (
		sum     int
		timeNow time.Time
	)
	timeNow = time.Now()
	for _, offer := range offers {
		if DateEqual(offer.CreatedAt, timeNow) {
			sum += offer.TaximetrData.Order.Tariff.TotalPrice
		}
	}
	return sum
}

//DateEqual проверяет в один ли день две даты
func DateEqual(date1, date2 time.Time) bool {
	y1, m1, d1 := date1.Date()
	y2, m2, d2 := date2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

// GetOrdersForDriverPerDay ввозвращает все заказы driver за сегодня и его сегодняшний заработок
func GetOrdersForDriverPerDay(driverUUID string) ([]structures.Order, int, error) {
	var (
		offers []OfferDrv
		orders []structures.Order
	)
	err := db.Model(&offers).
		Where("driver_uuid = ? AND created_at > (now() - interval '24 hours') AND accepted_at IS NOT NULL AND cancelled_at IS NULL", driverUUID).
		Select()
	if err != nil {
		return orders, 0, err
	}
	for _, offer := range offers {
		offer.TaximetrData.Order.CreatedAt = offer.CreatedAt
		orders = append(orders, offer.TaximetrData.Order)
	}
	sum := getEarningsToday(offers)
	return orders, sum, nil
}

func GetActiveDriversUUIDs(inactiveDaysThreshold int) ([]string, error) {
	var onlineStatesWithinThreshold []DriverStatesDrv

	err := db.Model(&onlineStatesWithinThreshold).
		Where("state = ? AND created_at > now() - interval'? days'",
			constants.DriverStateOnline, inactiveDaysThreshold).
		ColumnExpr("DISTINCT driver_uuid").
		Select()

	if err != nil {
		return []string{}, err
	}

	var activeDriversUUIDs []string
	for _, driverState := range onlineStatesWithinThreshold {
		activeDriversUUIDs = append(activeDriversUUIDs, driverState.DriverUUID)
	}

	return activeDriversUUIDs, nil
}

// GetAggregatedDriverIncome
func GetAggregatedDriverIncome(driverUUID string, start time.Time) (DriverIncomeTimestamp, error) {
	var income DriverIncomeTimestamp
	err := db.Model(&income).
		Where("driver_uuid = ?", driverUUID).
		Where("hour_timestamp >= ?", start.Truncate(time.Hour)).
		Group("driver_uuid").
		ColumnExpr("SUM(orders_finished) as orders_finished").
		ColumnExpr("SUM(total_income) as total_income").
		ColumnExpr("SUM(offers_rejected) as offers_rejected").
		ColumnExpr("SUM(lost_income) as lost_income").
		Select()

	return income, err
}

// calcOnlineTime считает время нахождения в статусе 'онлайн'
// driverStates - массив стейтов, отсортированных по дате (по возрастанию)
// isOnlineBeforeStart - был ли водитель онлайн до первого стейта
// onlineStartTime - время начала онлайна. Нужно указать, если isOnlineBeforeStart == true
func calcOnlineTime(driverStates []DriverStatesDrv, isOnlineBeforeStart bool, onlineStartTime time.Time) time.Duration {
	var (
		onlineTime   time.Duration
		stateOnline  = constants.DriverStateOnline
		stateOffline = constants.DriverStateOffline
	)

	// суммируем интервалы online->offline
	isOnline := isOnlineBeforeStart
	for _, drvState := range driverStates {
		if drvState.State == stateOnline && !isOnline {
			onlineStartTime = drvState.CreatedAt
			isOnline = true
		} else if drvState.State == stateOffline && isOnline {
			onlineTime += drvState.CreatedAt.Sub(onlineStartTime)
			isOnline = false
		}
	}

	// добавляем время с начала последнего онлайна, если водитель все еще онлайн
	if isOnline {
		onlineTime += time.Now().Sub(onlineStartTime)
	}

	return onlineTime
}

// GetOnlineTime возвращает суммарное время driver на линии
func GetOnlineTime(driverUUID string, start time.Time) (time.Duration, error) {
	var (
		driverStates []DriverStatesDrv
		onlineTime   time.Duration
		stateOnline  = constants.DriverStateOnline
		stateOffline = constants.DriverStateOffline
	)

	// получаем последний апдейт статуса (on/offline) до даты start
	// чтобы могли учесть онлайн с момента start
	var onlineStartTime time.Time
	err := db.Model(&driverStates).
		Where("driver_uuid = ?", driverUUID).
		Where("created_at < ?", start).
		WhereInMulti("state IN (?)", stateOnline, stateOffline).
		Order("created_at DESC").Limit(1).
		Select()
	if err != nil {
		return onlineTime, err
	}

	isOnlineBeforeStart := false
	if len(driverStates) > 0 {
		isOnlineBeforeStart = driverStates[0].State == stateOnline
	}

	// получаем все апдейты статуса за период
	err = db.Model(&driverStates).
		Where("driver_uuid = ?", driverUUID).
		Where("created_at >= ?", start).
		WhereInMulti("state IN (?)", stateOnline, stateOffline).
		Order("created_at").
		Select()
	if err != nil {
		return onlineTime, err
	}

	onlineTime = calcOnlineTime(driverStates, isOnlineBeforeStart, onlineStartTime)

	return onlineTime, nil
}

// // ValidateCode валидирует код
// func ValidateCode(vdata VerificationCode) (TokenResponse, error) {

// 	var (
// 		response TokenResponse
// 		rg       RegisterSession
// 		err      error
// 	)

// 	// ЗАПЛАТКА ДЛЯ ВЕРИФИКАЦИИ
// 	if vdata.Code != 1080 {
// 		err = db.Model(&rg).
// 			Where("device_id = ? AND code = ? AND CURRENT_TIMESTAMP < expiration AND used_at IS NULL", vdata.DeviceID, vdata.Code).
// 			Returning("*").
// 			Order("created_at DESC").
// 			Limit(1).
// 			Select()
// 		if err != nil {
// 			return response, fmt.Errorf("Код не найден")
// 		}
// 	} else {
// 		err = db.Model(&rg).
// 			Where("device_id = ? AND used_at IS NULL", vdata.DeviceID).
// 			Order("created_at DESC").
// 			Limit(1).
// 			Select()
// 		if err != nil {
// 			return response, fmt.Errorf("Not found registration with this device ID")
// 		}

// 	}
// 	rg.UsedAt = time.Now()
// 	err = db.Update(&rg)
// 	if err != nil {
// 		return TokenResponse{}, err
// 	}
// 	// Обнуляем сессиию одноразого пароля
// 	// Если на этот номер или устройство есть аккаунты их надо заблокировать, а также их refresh токены
// 	// blockDriversApp(rg.Phone, rg.DeviceID)

// 	// Создаем новый водительский профиль или получаем существующий
// 	driverApp, err := findOrCreateDriverApp(rg.Phone, rg.DeviceID)
// 	if err != nil {
// 		return TokenResponse{}, err
// 	}

// 	// Генерируем и создаем
// 	response, err = generateJWT(driverApp.ID, driverApp.UUID, rg.DeviceID, rg.Phone)
// 	if err != nil {
// 		return response, err
// 	}

// 	return response, nil
// }

func GetDriverByPhoneIfExists(driverPhone string) (*DriversApps, error) {
	driver := new(DriversApps)
	query := db.Model(driver).
		Where("phone = ?", driverPhone).
		Where("deleted is not true").
		Order("created_at desc").
		Limit(1)

	if err := query.Select(); err != nil {
		return nil, err
	}

	return driver, nil
}

func DriverExists(phone string) bool {
	ok, err := db.Model((*DriversApps)(nil)).Where("deleted is not true AND phone = ?", phone).Exists()
	if err != nil {
		return false
	}

	return ok
}

func HaveRegControl(driverUUID string) (bool, error) {
	driver, err := GetDriverByUUID(driverUUID)
	if err != nil {
		return false, err
	}
	for _, controlData := range driver.PhotocontrolData {
		if controlData.ControlType == RegControlName {
			return true, nil
		}
	}
	return false, nil
}

// blockDriversApp блокирует уже созданные аккаунты с полученным номером телефона или ID устройства
func blockDriversApp(phone string, deviceID string) {
	var drApps []DriversApps
	_ = db.Model(&drApps).
		Where("phone = ? OR device_id = ?", phone, deviceID).
		Select()
	reason := fmt.Sprintf("заблокировано по приничине новой регистрации device_id=%s phone=%s", phone, deviceID)
	for _, drv := range drApps {
		_, err := SetDriverStatus(drv.UUID, "blocked", reason)
		if err != nil {
			logs.OutputError(err.Error())
		}

		_ = expireDriverToken(drv.UUID)
	}
}

// CreateNewDriverFromBroker создает нового водилу без deviceID
func (drv *DriversApps) CreateNewDriverFromBroker() error {
	check, err := db.Model(&DriversApps{}).Where("phone = ?", drv.Phone).Exists()
	if err != nil {
		return fmt.Errorf("error checking driver exists, %s", err)
	}
	if check {
		return fmt.Errorf("driver with phone [%s] already exists", drv.Phone)
	}
	if drv.UUID == "" {
		return fmt.Errorf("emptyUUID")
	}
	_, err = db.Model(drv).Insert()
	if err != nil {
		return err
	}
	// Promote a new driver
	rewardNewDriver(drv)
	return nil
}

func ConnectDriverToTarget(driverUUID, driverPhone string, target DriverCallTarget) *structures.ErrorWithLevel {
	actualOffer, state, err := GetCurrentDriverOrder(driverUUID)
	if err != nil {
		return structures.Error(err)
	}
	check := false
	for _, st := range constants.ListActiveOrderStates() {
		if st == state {
			check = true
			break
		}
	}
	if !check {
		return structures.Warning(fmt.Errorf("Нельзя звонить по неактивному заказу"))
	}
	clientPhone := actualOffer.Order.Client.MainPhone
	if actualOffer.Order.CallbackPhone != "" {
		clientPhone = actualOffer.Order.CallbackPhone
	}
	storePhone := actualOffer.Order.GetProductsData().StoreData.Phone

	var neededPhone string
	if target != Store {
		neededPhone = clientPhone
		if clientPhone == "" {
			return structures.Error(fmt.Errorf("Нет информации о номере телефона клиента"))
		}
	} else {
		neededPhone = storePhone
		if storePhone == "" {
			return structures.Error(fmt.Errorf("Нет информации о номере телефона заведения"))
		}
	}
	data := structures.NewConferenceData{
		DriverPhone: driverPhone,
		ClientPhone: neededPhone,
	}
	err = rabsender.SendJSONByAction(rabsender.AcNewConference, "", data)
	return structures.Error(err)
}

// createDriverAppIfNeed создает нового водителя если с таким номером никого не было
func createDriverAppIfNeed(drvData *VerificationPassword) (*DriversApps, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":          "Send to rabbit new driver",
		"device_id":      drvData.DeviceID,
		"taxi_park_uuid": drvData.TaxiParkUUID,
	})

	driver := new(DriversApps)
	driver.UUID = structures.GenerateUUID()
	driver.Phone = drvData.Phone
	driver.Password = drvData.Password
	driver.DeviceID = drvData.DeviceID
	driver.PhotocontrolData = []structures.PhotoControlData{{
		ControlType:      RegControl.Name,
		ControlTypeTitle: RegControl.Title,
		NextControlTime:  time.Now(),
		Status:           structures.ExpiredPhotoControlState,
		Notified:         true,
	}}

	query := db.Model(driver).
		Where("phone=?phone").
		Where("deleted is not true").
		Order("created_at desc").
		Limit(1)

	// Ищем водителя, если не находим - идем дальше и создаем
	if err := query.Select(); err != nil && err != pg.ErrNoRows {
		return nil, errors.Wrap(err, "error finding driver")
	} else if err == nil {
		return driver, nil
	}

	// Если водитель не найден (новый), то выполняется код ниже

	// Валидация UUID
	tpUUID, err := uuid.FromString(drvData.TaxiParkUUID)
	if err != nil {
		return nil, err
	}
	if tpUUID.Version() < uuid.V1 || tpUUID.Version() > uuid.V5 {
		return nil, errors.New("wrong taxi_park_uuid version")
	}
	driver.TaxiParkUUID = &drvData.TaxiParkUUID

	alias, err := generateAlias()
	if err != nil {
		err = errors.WithMessage(err, "error generating alias for driver")
		log.Error(err)
		return nil, err
	}
	driver.Alias = alias

	if _, err := db.Model(driver).Returning("*").Insert(); err != nil {
		return nil, err
	}

	_, err = SetDriverStatus(driver.UUID, constants.DriverStateOnModeration, "Новая регистрация через приложение")
	if err != nil {
		return nil, errors.Wrap(err, "error saving driver status")
	}

	// Promote a new driver
	rewardNewDriver(driver)
	driver.Status = constants.DriverStateOnModeration
	err = rabsender.SendJSONByAction(rabsender.AcNewDriverToCRM, "", driver)
	if err != nil {
		log.Error(err)
	}
	log.Info("Driver was sent to CRM")

	return driver, nil
}

// UpdateDriverIDIfNeed обновляет id  и позываной водилы если надо
func (drv *DriversApps) UpdateDeviceIDAndAliasIfNeed(deviceID string) error {
	if drv.DeviceID != deviceID {
		// SetDriverStatus(drv.UUID, variables.DriverStateOnModerationAfterChange, "Смена устройства")
		_, err := db.Model(drv).
			Where("uuid = ?", drv.UUID).
			Set("device_id = ?", deviceID).
			Returning("*").
			Update()
		if err != nil {
			return fmt.Errorf("error update drv, %s", err)
		}
		err = expireDriverToken(drv.UUID)
		if err != nil {
			return err
		}
	}
	if drv.Alias != 0 {
		return nil
	}
	var err error
	drv.Alias, err = generateAlias()
	if err != nil {
		return fmt.Errorf("error generate alias, %s", err)
	}
	err = rabsender.SendJSONByAction(rabsender.AcDriverDataUpdate, "", drv)
	if err != nil {
		return fmt.Errorf("error send drv data to rabbit after generate alias, %s", err)
	}
	_, err = db.Model(drv).
		Where("uuid = ?", drv.UUID).
		Set("alias = ?", drv.Alias).
		Returning("*").
		Update()
	if err != nil {
		return fmt.Errorf("error update drv alias, %s", err)
	}
	return nil
}
func randonQueueName() string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890")
	b := make([]rune, 8)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]

	}
	return string(b)
}

func generateAlias() (int, error) {
	var (
		drApps DriversApps
	)
	err := db.Model(&drApps).
		ColumnExpr("(MAX(alias)) AS alias").
		Select()
	if err != nil {
		return 0, fmt.Errorf("error finding last alias, %s", err)
	}
	return drApps.Alias + 1, nil
}

//GetCurrentDriversStates возвращает текущие статусы водителей
func GetCurrentDriversStates(driversUUID []string) ([]DriverStatesDrv, error) {
	var (
		drvStates []DriverStatesDrv
	)
	return drvStates, nil

	if len(driversUUID) == 0 {
	}
	err := db.Model(&drvStates).
		ColumnExpr("DISTINCT ON (driver_uuid) *").
		Order("driver_uuid").
		Order("created_at DESC").
		Where("driver_uuid in (?)", pg.In(driversUUID)).
		Select()
	if err != nil {
		return drvStates, err
	}

	return drvStates, nil
}

//GetCurrentDriverStateFromCRM возвращает текущий статус driver из crm
func GetCurrentDriverStateFromCRM(driverUUID string) (string, error) {
	var result string
	err := request(http.MethodGet, config.St.CRM.Host+driverCurrentStateEndpoint+driverUUID, nil, &result)
	return result, err
}

// GetCurrentDriverStateWithoutConsideringFromCRM -
func GetCurrentDriverStateWithoutConsideringFromCRM(driverUUID string) (string, error) {
	// var result string
	// err := request(http.MethodGet, config.St.CRM.Host+driverCurrentStateWithoutConsiderEndpoint+driverUUID, nil, &result) // стоит брать из водительской базы
	// if err != nil {
	// 	return "", errpath.Err(err)
	// }

	var drvStates DriverStatesDrv
	err := db.Model(&drvStates).
		Where("driver_uuid = ?", driverUUID).
		Where("state <> ?", constants.DriverStateConsidering).
		Column("state").
		Order("created_at desc").
		Limit(1).
		Select()
	if err != nil {
		return "", errpath.Err(err)
	}

	return drvStates.State, nil
}

//GetCurrentDriverState возвращает текущий статус driver
func GetCurrentDriverState(driverUUID string) (DriverStatesDrv, error) {
	var drvState DriverStatesDrv
	err := db.Model(&drvState).
		Where("driver_uuid = ?", driverUUID).
		Order("created_at DESC").
		Limit(1).
		Select()
	if err != nil {
		return DriverStatesDrv{}, err
	}
	return drvState, nil
}

//GetDriverByUUID возвращает водителя по uuid
func GetDriverByUUID(driverUUID string) (DriversApps, error) {
	var driver DriversApps
	err := db.Model(&driver).
		Where("uuid = ?", driverUUID).
		Select()
	if err != nil {
		return driver, err
	}
	return driver, err
}

//GetOfflineDriverTariff возвращает водителей по uuid
func GetOfflineDriverTariff(driveruuid string) (structures.DriverTariff, error) {
	var result structures.DriverTariff
	err := request(http.MethodGet, config.St.CRM.Host+offlineDriverTariffEndpoint+"/"+driveruuid, nil, &result)
	return result, err
}

//GetDriversByUUIDs возвращает водителей по uuid
func GetDriversByUUIDs(driverUUIDs ...string) ([]DriversApps, error) {
	if len(driverUUIDs) == 0 {
		return []DriversApps{}, nil
	}

	drivers := make([]DriversApps, 0, len(driverUUIDs))
	err := db.Model(&drivers).
		WhereIn("uuid IN (?)", driverUUIDs).
		Select()
	if err != nil {
		return []DriversApps{}, err
	}

	return drivers, err
}

//UpdateDriversPhotocontrolData godoc
// TODO: для каждого водилы делается отдельный запрос в базу.ОЧЕНЬ ПЛОХО, но go-pg не хочет обновлять данные массивом.Надо как то порешать
func UpdateDriversPhotocontrolData(drivers []DriversApps) ([]DriversApps, error) {
	for i := range drivers {
		_, err := db.Model(&drivers[i]).
			Where("uuid = ?uuid").
			Column("photocontrol_data").
			Returning("*").
			Update()
		if err != nil {
			return drivers, err
		}
	}

	return drivers, nil
}
func FillTariffMarkups(drivers []DriversApps) error {
	if len(drivers) < 1 {
		return nil
	}
	tariffsUUID := make([]string, 0, len(drivers))
	for i := range drivers {
		tariffsUUID = appendIfNotExists(tariffsUUID, drivers[i].DrvTariff.UUID)
	}
	markups, err := GetManyDriverTariffMarkups(tariffsUUID)
	if err != nil {
		return err
	}
	for i := range drivers {
		drivers[i].DrvTariff.Markups = markups[drivers[i].DrvTariff.UUID]
	}
	return nil
}

// HasEnoughBalance  REQUIRED FILLED MARKUPS FIELD IN DRIVER.DRIVERTARIFF
func (drv *DriversApps) HasEnoughBalance(offer *OfferDrv) (bool, error) {
	endBalance := drv.GetTotalBalance()
	billingData := structures.BillingData{
		DriverBalance: endBalance,
		OrderPrice:    offer.TaximetrData.Order.Tariff.TotalPrice,
	}

	if drv.DrvTariff.CommissionExp != "" {
		iEndBalance, err := expr.Eval(drv.DrvTariff.CommissionExp, billingData)
		if err != nil {
			return false, errors.Wrap(err, "failed to eval the end balance")
		}

		newBalance, casted := iEndBalance.(float64)
		if casted {
			if newBalance < 0 {
				return false, nil
			}
			endBalance = newBalance
		}
	}
	markup, _ := drv.DrvTariff.GetMarkupByTaxiParkUUID(drv.GetTaxiParkUUID())
	if markup.CompleteExpr != "" {
		billingData.DriverBalance = endBalance

		iEndBalance, err := expr.Eval(markup.CompleteExpr, billingData)
		if err != nil {
			return false, errors.Wrap(err, "failed to eval the end balance after the taxi park commission")
		}

		newBalance, casted := iEndBalance.(float64)
		if casted && newBalance < 0 {
			return false, nil
		}
	}

	return true, nil
}

func GetDriversLastLocations(driversUUID ...string) ([]structures.DriverCoordinates, error) {
	if len(driversUUID) == 0 {
		return nil, nil
	}
	var result []structures.DriverCoordinates
	err := request(http.MethodPost, config.St.CRM.Host+driverLastLocationEndpoint, driversUUID, &result)
	return result, err
}

func GetCurrentDriversStatesFromCRM(driverUUIDs []string) (map[string]string, error) {
	if len(driverUUIDs) == 0 {
		return nil, nil
	}

	var drvStates []DriversApps
	if err := request(http.MethodPost, config.St.CRM.Host+driverCurrentStatesEndpoint, driverUUIDs, &drvStates); err != nil {
		return nil, errors.Wrap(err, "failed to fetch current drivers states")
	}

	result := make(map[string]string)
	for _, drv := range drvStates {
		result[drv.UUID] = drv.Status
	}
	return result, nil
}

func GetDriverActivity(ctx context.Context, driverUUID string) (DriversApps, error) {
	var driver DriversApps
	err := db.ModelContext(ctx, &driver).
		Column("activity").
		Where("uuid = ?", driverUUID).
		Limit(1).
		Select()
	if err != nil {
		return DriversApps{}, errors.Wrap(err, "failed to select driver's activity")
	}
	return driver, nil
}

func request(method, url string, payload, response interface{}, headers ...map[string]string) error {
	r := &http.Client{
		Timeout: 15 * time.Second,
	}
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}

	req.Header.Set("Content-Type", "application/json")
	for _, headMap := range headers {
		for key, value := range headMap {
			req.Header.Add(key, value)
		}
	}

	resp, err := r.Do(req)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("failed to make a %s request", method))
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("%s request to %s failed with status: %d", method, url, resp.StatusCode)
		}
		return errors.Errorf("%s request to %s failed with status: %d and body: %s", method, url, resp.StatusCode, string(body))
	}
	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}

func rewardNewDriver(driver *DriversApps) {
	// Reward a new driver to promote him in the distribution process
	initialActivity, err := GetInitialDriverActivity()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get initial activity",
			"DriverUUID": driver.UUID,
		}).Error(err)
	} else {
		activityItem := DriverActivityHistoryItem{
			ID:             uuid.Must(uuid.NewV4()).String(),
			DriverUUID:     driver.UUID,
			Event:          "reward a new driver",
			ActivityChange: initialActivity,
		}
		driver.Activity, err = AddActivityForDriver(activityItem)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "reward a new driver with initial activity",
				"DriverUUID": driver.UUID,
			}).Error(err)
		}
	}

	// Set the initial rating
	driver.Karma, err = UpdateDriverKarma(driver.UUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "reward a new driver with initial rating",
			"DriverUUID": driver.UUID,
		}).Error(err)
	}
}

// SetScoringBoostTimeByPhotocontrolPassed - устанавливает время активности буста очков распределения (для скоринга)
// Meta: время активности устанавливается только если в самом фотоконтроле включен буст для скоринга
func (drv *DriversApps) SetScoringBoostTimeByPhotocontrolPassed(driverUUID string, boostTime int64) error {
	setTime := time.Now().Add(time.Second * time.Duration(boostTime)).Unix()

	err := db.Model(drv).
		Where("uuid = ?", driverUUID).
		Column("promotion").
		Select()
	if err != nil {
		return errpath.Err(err)
	}

	// теущее время активности должно быть меньше устанавливаемого
	if drv.Promotion.ScoringBoostTimeByPhotocontrolPassed < setTime {
		_, err = db.Model(drv).
			Where("uuid = ?", driverUUID).
			Set(fmt.Sprintf("promotion = jsonb_set(COALESCE(promotion, '{}'), '{scoring_boost_time_by_photocontrol_passed}', '%v')", setTime)).
			Update()
		if err != nil {
			return errpath.Err(err)
		}
	}

	return nil
}
