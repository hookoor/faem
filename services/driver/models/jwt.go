package models

import (
	"strconv"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/validation"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"

	"math/rand"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// const tokenExpiredTime = 120
// const refreshTokenExpiredMinutes = 30240
// const sessionExpirationMinutes = 2

// RegisterSession сессии для регистрации пользователей.
// здесь храним ИД сессии, телефон, серийник и время когда он будет актуальным
type RegisterSession struct {
	tableName  struct{}  `sql:"drv_reg_sessions"`
	SSID       string    `json:"ssid" sql:",pk" description:"Session ID"`
	DeviceID   string    `json:"device_id"`
	Phone      string    `json:"phone"`
	Code       int       `json:"code"`
	Password   string    `json:"password"`
	Expiration time.Time `json:"expiration_time"`
	UsedAt     time.Time `json:"used_time"`
	CreatedAt  time.Time `sql:"default:now()" json:"created_at"`
}

type (
	// DriverRegisterRequest структура запроса на регистрацию
	DriverRegisterRequest struct {
		DeviceID string `json:"device_id"`
		Phone    string `json:"phone"`
	}
	// NewRegResponseStruct REST response structs
	NewRegResponseStruct struct {
		Code            int    `json:"code"`
		Msg             string `json:"message"`
		NextRequestTime int64  `json:"next_request_time"`
	}
	// VerificationPassword
	VerificationPassword struct {
		DeviceID     string `json:"device_id" required_on:"create"`
		Phone        string `json:"phone" required_on:"create"`
		Password     string `json:"password" required_on:"create"`
		TaxiParkUUID string `json:"taxi_park_uuid"`
	}
	// VerificationCode структура для получения кода
	VerificationCode struct {
		DeviceID string `json:"device_id"`
		Code     int    `json:"code"`
	}
	// TokenResponse структура ответа с токеным
	TokenResponse struct {
		Token                  string `json:"token"`
		DriverUUID             string `json:"driver_uuid"`
		RefreshToken           string `json:"refresh_token"`
		RefreshTokenExpiration int64  `json:"refresh_expiration"`
	}
	// TokenClaim содержание самого токена
	TokenClaim struct {
		DriverUUID   string   `json:"driver_uuid"`
		DeviceID     string   `json:"device_id"`
		Phone        string   `json:"phone"`
		TaxiParkUUID []string `json:"taxi_parks_uuid"`
		jwt.StandardClaims
	}
	// RefreshRequest структура запроса refrash механизма
	RefreshRequest struct {
		RefreshToken string `json:"refresh"`
	}
)

func (cr *VerificationPassword) Validate() error {
	if err := validation.Create(cr); err != nil {
		return err
	}

	return nil
}

// JWTSessions godoc
type JWTSessions struct {
	tableName           struct{}  `sql:"drv_jwt_sessions"`
	ID                  int       `json:"id" sql:",pk"`
	DriverID            int       `json:"driver_id"`
	DriverUUID          string    `json:"driver_uuid"`
	RefreshToken        string    `json:"refresh_token" description:"Refresh token"`
	RefreshTokenUsed    time.Time `json:"refresh_token_used" description:"Refresh token used date"`
	RefreshTokenExpired time.Time `json:"refrash_expired" description:"Token expiration date"`
	CreatedAt           time.Time `sql:"default:now()" json:"created_at" `
}

const (
	RemindPasswordTag string = "remind_password"
)

// GetPasswordForDriver возвращает пароль, нужно ли отправлять смску, нужно ли создавать регистрационную сессию и ошибку
func GetPasswordForDriver(_, driverPhone string) (string, bool, bool, error) {
	driver, err := GetDriverByPhoneIfExists(driverPhone)
	if err != nil && err != pg.ErrNoRows {
		return "", false, false, err
	} else if err == pg.ErrNoRows { // Если водитель не найден в базе
		sess, err := GetSessionIfExists(driverPhone)
		if err != nil {
			return "", false, false, errors.Wrap(err, "error getting session")
		}
		if sess == nil { // Если водитель еще не вводил свой номер телефона
			return generatePassForDriver(), true, true, nil
		}

		return sess.Password, false, false, nil
	}

	if driver.Password == "" {
		driver.Password = generatePassForDriver()
		if err := setDriverPassword(driverPhone, driver.Password); err != nil {
			return driver.Password, false, false, err
		}
		return driver.Password, true, false, nil
	}
	return driver.Password, false, false, nil
}
func ValidatePassword(validata *VerificationPassword) (*DriversApps, error) {
	secPass := validata.Password == strconv.Itoa(GodModeInst.AppsVerificationCode.Value)

	driver, err := GetDriverByPhoneIfExists(validata.Phone)
	if err != pg.ErrNoRows {
		if err != nil {
			return nil, err
		}

		if err := driver.UpdateDeviceIDAndAliasIfNeed(validata.DeviceID); err != nil {
			return nil, err
		}

		if validata.Password == driver.Password || secPass {
			return driver, nil
		}
		return nil, errors.New("Пароль неверный")
	}

	sess, err := GetSessionIfExists(validata.Phone)
	if err != nil {
		return nil, err
	}
	if sess == nil {
		return nil, errors.New("Пароль еще не сгенерирован. Запросите его снова")
	}
	if sess.Password != validata.Password && !secPass {
		return nil, errors.New("Пароль неверный")
	}

	return createDriverAppIfNeed(validata)
}
func setDriverPassword(driverPhone, password string) error {
	var driver DriversApps
	_, err := db.Model(&driver).
		Where("phone = ?", driverPhone).
		Set("password = ?", password).
		Returning("*").
		Update()
	if err != nil {
		return err
	}
	err = rabsender.SendJSONByAction(rabsender.AcDriverDataUpdate, "", driver)
	return err
}
func generatePassForDriver() string {
	return strconv.Itoa(rangeIn(1000, 9999))
}

func GetSessionIfExists(phone string) (*RegisterSession, error) {
	sess := new(RegisterSession)
	query := db.Model(sess).
		Where("password is not null AND password != ?", RemindPasswordTag).
		Where("phone = ?", phone).
		Order("created_at desc").
		Limit(1)

	if err := query.Select(); err != nil && err != pg.ErrNoRows {
		return nil, err
	} else if err == pg.ErrNoRows {
		return nil, nil
	}

	return sess, nil
}

func NewRegSessionByPassword(pass, phone, deviceID string) (RegisterSession, error) {
	result := RegisterSession{
		Password:   pass,
		Phone:      phone,
		DeviceID:   deviceID,
		CreatedAt:  time.Now(),
		Expiration: time.Now().Add(time.Second * time.Duration(config.St.Application.SessionExpS)),
		SSID:       structures.GenerateUUID(),
	}
	_, err := db.Model(&result).
		Insert()
	return result, err
}

// GenerateVerificationCode generates code for verification telephone number
//func GenerateVerificationCode(rq DriverRegisterRequest) (int, int64, error) {
//
//	var sess RegisterSession
//	createNewSession := func(code int) error {
//		sess = RegisterSession{}
//		sess.SSID = structures.GenerateUUID()
//		sess.DeviceID = rq.DeviceID
//		sess.Phone = rq.Phone
//		dur := time.Second * time.Duration(config.St.Application.SessionExpS)
//		sess.Expiration = time.Now().Add(dur)
//		sess.Code = rangeIn(1000, 9999)
//		if code != 0 {
//			sess.Code = code
//		}
//		sess.CreatedAt = time.Now()
//		_, err := db.Model(&sess).Returning("*").Insert()
//		if err != nil {
//			return err
//		}
//		return nil
//	}
//	if rq.DeviceID == "" || rq.Phone == "" {
//		return 0, 0, errors.New("Phone and Device ID are required fields. RTFM")
//	}
//	lastSessQuery := db.Model(&sess).
//		Where("device_id = ? AND phone = ?", rq.DeviceID, rq.Phone)
//	check, err := lastSessQuery.Exists()
//	if err != nil {
//		return sess.Code, sess.Expiration.Unix(), err
//	}
//	if check {
//		err = lastSessQuery.
//			Limit(1).
//			Order("created_at desc").
//			Select()
//		if err != nil {
//			return sess.Code, sess.Expiration.Unix(), err
//		}
//		if sess.Expiration.Unix() > time.Now().Unix() {
//			return sess.Code, sess.Expiration.Unix(), errors.New("Код уже запрошен, попробуйте запросить новый код авторизации позже")
//		}
//		if !sess.UsedAt.IsZero() {
//			err = createNewSession(0)
//		} else {
//			err = createNewSession(sess.Code)
//		}
//		if err != nil {
//			return sess.Code, sess.Expiration.Unix(), err
//		}
//	} else {
//		err = createNewSession(0)
//		if err != nil {
//			return sess.Code, sess.Expiration.Unix(), err
//		}
//	}
//	// TODO: Сделать валидацию на номер телефона
//	// err = db.Model(&sess).
//	// 	Where("device_id = ? AND phone = ? AND CURRENT_TIMESTAMP < expiration", rq.DeviceID, rq.Phone).
//	// 	Select()
//	// if err == nil {
//	// 	return sess.Code, sess.Expiration.Unix(), errors.New("Код уже запрошен, попробуйте запросить новый код авторизации позже")
//	// }
//
//	// if err` != nil {
//	// 	return 0, 0, err
//	// }`
//
//	return sess.Code, sess.Expiration.Unix(), nil
//}

func rangeIn(low, hi int) int {
	return low + rand.Intn(hi-low)
}

func expireDriverToken(driverUUID string) error {

	var sessOld JWTSessions

	_, err := db.Model(&sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("driver_uuid = ?", driverUUID).
		Update()

	if err != nil {
		return err
	}
	return nil
}

func GenerateJWT(driver *DriversApps) (*TokenResponse, error) {
	if err := expireDriverToken(driver.UUID); err != nil {
		return nil, err
	}

	jwtSess, err := newRefreshToken(driver.UUID)
	if err != nil {
		return nil, err
	}

	jwtToken, err := newJWT(driver)
	if err != nil {
		return nil, err
	}

	tokenResp := &TokenResponse{
		RefreshToken:           jwtSess.RefreshToken,
		RefreshTokenExpiration: jwtSess.RefreshTokenExpired.UTC().Unix(),
		DriverUUID:             driver.UUID,
		Token:                  jwtToken,
	}

	return tokenResp, nil
}

// NewRefrashToken returns refresh token and expiration time
func newRefreshToken(driverUUID string) (JWTSessions, error) {
	var sessNew JWTSessions
	newToken, err := uuid.NewV4()
	if err != nil {
		return sessNew, err
	}

	sessNew.RefreshToken = newToken.String()
	dur := time.Minute * time.Duration(config.St.Application.RefreshTokenExpiredM)
	sessNew.RefreshTokenExpired = time.Now().Add(dur)
	sessNew.DriverUUID = driverUUID

	err = sessNew.saveTokenData()

	if err != nil {
		return sessNew, err
	}
	return sessNew, nil
}

// GenerateJWT generates new token
func newJWT(driver *DriversApps) (string, error) {
	dur := time.Minute * time.Duration(config.St.Application.TokenExpiredM)
	claims := TokenClaim{
		DriverUUID: driver.UUID,
		DeviceID:   driver.DeviceID,
		Phone:      driver.Phone,
		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(dur).Unix(),
		},
	}
	if driver.TaxiParkUUID != nil {
		claims.TaxiParkUUID = []string{*driver.TaxiParkUUID}
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	mySigningKey := []byte(config.JWTSecret())
	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return ss, nil
}

// saveTokenData expired existing and create new token for user
func (jwts *JWTSessions) saveTokenData() error {

	_, err := db.Model(jwts).Returning("*").Insert()
	if err != nil {
		return errpath.Err(err, "Error saving session data")
	}

	return nil
}

// RefreshJWTToken godoc
func RefreshJWTToken(token string) (*TokenResponse, error) {
	driverApp, err := expireRefreshToken(token)
	if err != nil {
		return nil, err
	}

	jwtSess, err := newRefreshToken(driverApp.UUID)
	if err != nil {
		return nil, err
	}

	jwtToken, err := newJWT(driverApp)
	if err != nil {
		return nil, err
	}

	newToken := &TokenResponse{
		RefreshToken:           jwtSess.RefreshToken,
		RefreshTokenExpiration: jwtSess.RefreshTokenExpired.UTC().Unix(),
		DriverUUID:             driverApp.UUID,
		Token:                  jwtToken,
	}

	return newToken, nil
}

// Expire token and return Driver ID
func expireRefreshToken(token string) (*DriversApps, error) {
	sessOld := new(JWTSessions)
	query := db.Model(sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("CURRENT_TIMESTAMP < refresh_token_expired").
		Where("refresh_token_used is NULL").
		Where("refresh_token = ?", token).
		Returning("*")

	if res, err := query.Update(); err != nil {
		return nil, errors.Wrap(err, "error updating refresh token")
	} else if res.RowsAffected() == 0 {
		return nil, errors.New("refresh token is not valid")
	}

	driverApp := new(DriversApps)
	err := db.Model(driverApp).Where("deleted is not true AND UUID = ?", sessOld.DriverUUID).First()
	if err != nil {
		return nil, err
	}

	return driverApp, nil
}

func GetCurrentRemindSessionIfExists(phone string) (sess RegisterSession, check bool, err error) {

	query := db.Model(&sess).
		Where("phone = ?", phone).
		Where("expiration > current_timestamp").
		Where("password = ?", RemindPasswordTag)
		// Where("device_id = ?",deviceID)
	check, err = query.
		Exists()
	if err != nil || !check {
		return
	}
	err = query.
		Order("created_at desc").
		Limit(1).
		Select()
	return
}
func CreateNewRemindSeesion(phone, deviceID string) (RegisterSession, error) {
	sess, err := NewRegSessionByPassword(RemindPasswordTag, phone, deviceID)
	return sess, err
}
