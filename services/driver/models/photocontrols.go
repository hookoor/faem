package models

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
)

// FIXME: hardcoded for now, allow managers to modify them later
const (
	// A driver can pass photo control every...
	ControlLifetimeDriverLicense = time.Hour * 24 * 2       // ~ 2 days
	ControlPeriodDriverLicense   = time.Hour * 24 * 30 * 12 // ~ 1 year
	ControlLifetimeAuto          = time.Hour * 24           // 1 days
	ControlPeriodAuto            = time.Hour * 24 * 7       // 1 week
	RegControlTimeout            = time.Hour * 24 * 30 * 12
	RegControlName               = "registration_control"
)

var (
	AvailableTypes = []ControlType{
		{
			Name:  "driver_license",
			Title: "Документы",
			Variants: []ControlVariant{
				{
					Name:        "front",
					Title:       "Спереди",
					Description: "Сфотографируйте переднюю сторону водительских прав",
				},
				{
					Name:        "back",
					Title:       "Сзади",
					Description: "Сфотографируйте обратную сторону водительских прав",
				},
			},
			Description:  "Сделайте фотографии ваших водительских прав для прохождения фотоконтроля",
			PassTimeout:  ControlPeriodDriverLicense,
			PassLifetime: ControlLifetimeDriverLicense,
		},
		{
			Name:  "auto",
			Title: "Автомобиль",
			Variants: []ControlVariant{
				// {
				// 	Name:        "front",
				// 	Title:       "Спереди",
				// 	Description: "Сфотографируйте переднюю сторону автомобиля",
				// },
				// {
				// 	Name:        "back",
				// 	Title:       "Сзади",
				// 	Description: "Сфотографируйте заднюю сторону автомобиля",
				// },
				{
					Name:        "left",
					Title:       "Слева",
					Description: "Сфотографируйте переднюю сторону автомобиля и левый бок",
				},
				{
					Name:        "right",
					Title:       "Справа",
					Description: "Сфотографируйте переднюю сторону автомобиля и правый бок",
				},
				// {
				// 	Name:        "salon",
				// 	Title:       "Салон",
				// 	Description: "Сфотографируйте салон автомобиля",
				// },
			},
			Description:  "Сделайте фотографии вашего автомобиля для прохождения фотоконтроля",
			PassTimeout:  ControlPeriodAuto,
			PassLifetime: ControlLifetimeAuto,
		},
	}
)

func (dsd *DriverStatesDrv) BlockByControlIfNeed() error {
	// пока не  протещено
	return nil
	driver, err := GetDriverByUUID(dsd.DriverUUID)
	if err != nil {
		return fmt.Errorf("getting driver by uuid error,%s", err)
	}
	//если в данных есть регистрационный фотоконтроль. Если его нет, значит водитель его уже прошел
	for _, data := range driver.PhotocontrolData {
		if data.ControlType == RegControlName {
			dsd.State = constants.DriverStateBlocked
			dsd.CreatedAt = time.Now()
			dsd.Comment = constants.DriverBlockReasonPhotocontrol
			_, err := SaveDriverStateFromCRM(dsd.DriverStates)
			if err != nil {
				return fmt.Errorf("error saving driver state,%s", err)
			}
			break
		}
	}
	return nil
}

var (
	RegControl = ControlType{
		Name:  RegControlName,
		Title: "Фотоконтроль при регистрации",
		Variants: []ControlVariant{
			{
				Name:        "auto_left",
				Title:       "Слева",
				Description: "Сфотографируйте переднюю сторону автомобиля и левый бок",
			},
			{
				Name:        "auto_right",
				Title:       "Справа",
				Description: "Сфотографируйте переднюю сторону автомобиля и правый бок",
			},
			// {
			// 	Name:        "driver_license_front",
			// 	Title:       "Водительские права спереди",
			// 	Description: "Сфотографируйте переднюю сторону водительских прав",
			// },
			// {
			// 	Name:        "driver_license_back",
			// 	Title:       "Водительские права сзади",
			// 	Description: "Сфотографируйте обратную сторону водительских прав",
			// },
			// {
			// 	Name:        "technical_certificate_front",
			// 	Title:       "Техпаспорт спереди",
			// 	Description: "Сфотографируйте лицевую сторону технического паспорта",
			// },
			// {
			// 	Name:        "technical_certificate_back",
			// 	Title:       "Техпаспорт сзади",
			// 	Description: "Сфотографируйте обратную сторону технического паспорта",
			// },
			// {
			// 	Name:        "insurance_policy",
			// 	Title:       "Cтраховой полис",
			// 	Description: "Сфотографируйте лицевую сторону полиса обязательного медицинского страхования",
			// },
			// {
			// 	Name:        "passport_personal_data",
			// 	Title:       "Личные данные",
			// 	Description: "Сфотографируйте страницу с личными данными из паспорта гражданина РФ",
			// },
			// {
			// 	Name:        "passport_reg_data",
			// 	Title:       "Информация о прописке",
			// 	Description: "Сфотографируйте страницу с информацией о прописке из паспорта гражданина РФ",
			// },
		},
		PassTimeout:  RegControlTimeout,
		PassLifetime: RegControlTimeout,
		Description:  "Загрузите все необходимые фотографии для прохождения регистрационного фотоконтроля",
	}
)

type ControlVariant struct {
	Name        string `json:"name"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Comment     string `json:"comment" sql:"-"`
}

type PhotoControlIn struct {
	DriverID       string
	ControlID      string
	ImageURL       string
	ControlType    string
	ControlVariant string
}

type ControlType struct {
	Name        string
	Title       string
	Variants    []ControlVariant
	Description string
	// PassTimeout compulsory time
	PassTimeout time.Duration
	// PassLifetime warning time
	PassLifetime time.Duration
}

type PhotoControl struct {
	tableName struct{} `sql:"drv_photocontrols"`
	structures.PhotoControl
	CreatedAt time.Time
}

func (c *ControlType) HasVariant(name string) bool {
	for _, cv := range c.Variants {
		if cv.Name == name {
			return true
		}
	}
	return false
}

func (phCon *PhotoControl) updateDriverControlData(controlType ControlType) error {
	if !phCon.Approved {
		return nil
	}
	driver, err := GetDriverByUUID(phCon.DriverID)
	if err != nil {
		return fmt.Errorf("not found driver by uuid [%s],%s", phCon.DriverID, err)
	}
	if phCon.ControlType == RegControlName {
		// если это регистрационный контроль, то нужно очистить данные в PhotocontrolData  и добавить все остальные фотоконтроли
		driver.PhotocontrolData = nil

		for _, contType := range AvailableTypes {
			driver.PhotocontrolData = append(driver.PhotocontrolData, structures.PhotoControlData{
				Status:           structures.OKPhotoControlState,
				ControlType:      contType.Name,
				ControlTypeTitle: contType.Title,
				NextControlTime:  time.Now().Add(contType.PassTimeout),
			})
		}
	} else {
		for i := range driver.PhotocontrolData {
			if driver.PhotocontrolData[i].ControlType == phCon.ControlType {
				driver.PhotocontrolData[i].Status = structures.OKPhotoControlState
				driver.PhotocontrolData[i].Notified = true
				driver.PhotocontrolData[i].NextControlTime = time.Now().Add(controlType.PassLifetime)
				break
			}
		}
	}
	_, err = db.Model(&driver).
		Where("uuid = ?", phCon.DriverID).
		Set("photocontrol_data = ?", driver.PhotocontrolData).
		Update()
	if err != nil {
		return fmt.Errorf("driver updating error, %s", err)
	}
	return err
}
func (phCon *PhotoControl) UpdatePhotoControl(controlType ControlType) error {
	_, err := db.Model(phCon).
		Where("control_id = ?control_id").
		UpdateNotNull()
	if err != nil {
		return fmt.Errorf("updating photocontrol error,%s", err)
	}
	err = phCon.updateDriverControlData((controlType))
	if err != nil {
		return fmt.Errorf("updating driver control data error,%s", err)
	}
	return nil
}
func (phCon *PhotoControl) UnlockDriverIfNeed() error {
	currentState, err := GetCurrentDriverState(phCon.DriverID)
	if err != nil {
		return fmt.Errorf("error getting current driver state,%s", err)
	}

	if currentState.State != constants.DriverStateBlocked ||
		currentState.Comment != constants.DriverBlockReasonPhotocontrol {
		return nil
	}

	var drvState DriverStatesDrv
	drvState.DriverUUID = phCon.DriverID
	drvState.Comment = "Перевод после прохождения фотоконтроля"
	drvState.initFsm()
	drvState.FSM.SetState(currentState.State)
	drvState.CreatedAt = time.Now()
	err = drvState.FSM.Event(constants.DriverStateAvailable)
	if err != nil {
		return fmt.Errorf("updating driver state error,%s", err)
	}
	return nil
}

func (c *ControlType) FilterAlreadyUploadedVariants(uploaded *PhotoControl) []ControlVariant {
	var result []ControlVariant
	for _, cv := range c.Variants {
		var found bool
		for _, ph := range uploaded.Photos {
			found = false
			if ph.ControlVariant == cv.Name {
				if ph.Status == structures.RejectedStatus {
					cv.Comment = ph.Comment
					break
				}
				found = true
				break
			}
		}
		if !found {
			result = append(result, cv)
		}
	}
	return result
}

func (c *ControlType) IsCompleted(variantLen int) bool {
	return len(c.Variants) == variantLen
}

func (p *PhotoControlIn) Validate(availableTypes []ControlType) error {
	if p.ControlID == "" {
		return errors.New("Укажите ID контроля")
	}

	// Look for provided type
	var (
		providedType ControlType
		found        bool
	)
	for _, ct := range availableTypes {
		if ct.Name == p.ControlType {
			providedType = ct
			found = true
			break
		}
	}
	if p.ControlType == RegControlName {
		providedType = RegControl
		found = true
	}
	if !found {
		return errors.New("Укажите корректный тип контроля")
	}

	// Look for provided variant
	if !providedType.HasVariant(p.ControlVariant) {
		return errors.New("Укажите корректный вариант контроля")
	}
	return nil
}

func (phCon *PhotoControl) NotifyDriver() error {
	driverToken, err := GetCurrentDriverFCMToken(phCon.DriverID)
	if err != nil {
		return fmt.Errorf("getting driver fcm token error,%s", err)
	}
	msg := phCon.notifyTitle()
	alert := AlertsDRV{
		DriverUUID: phCon.DriverID,
		Tag:        constants.FcmTagInitData,
		Message:    msg,
	}
	err = alert.Save()
	go fcmsender.SendMessageToDriverWithCentrifugo(phCon.DriverID, msg, driverToken, constants.FcmTagInitData, 25000, msg)
	if err != nil {
		return fmt.Errorf("error saving alert,%s", err)
	}

	return nil
}

func BlockDriversOutOfPhotoControl(data map[string]string) error {
	var (
		suitableDriversUUID []string
		driversUUID         []string
	)

	for i := range data {
		driversUUID = append(driversUUID, i)
	}
	if len(driversUUID) == 0 {
		return nil
	}
	drvStates, err := GetCurrentDriversStates(driversUUID)
	if err != nil {
		return fmt.Errorf("getting current drivers states error,%s", err)
	}
	for _, state := range drvStates {

		if state.suitableForBlockByControl() {

			suitableDriversUUID = append(suitableDriversUUID, state.DriverUUID)
			continue
		}

	}
	for _, drvuuid := range suitableDriversUUID {
		_, err := DriverSetStatus(drvuuid, constants.DriverStateBlocked, constants.DriverBlockReasonPhotocontrol)
		if err != nil {
			return fmt.Errorf("ser driver status error,%s", err)
		}
	}
	dataForNotify := make(map[string]string)
	for i, title := range data {
		dataForNotify[i] = title
	}
	err = notifyDriversAboutBlock(dataForNotify)

	return err
}
func notifyDriversAboutBlock(data map[string]string) error {

	if len(data) == 0 {
		return nil
	}
	var driversUUID []string

	for i := range data {
		driversUUID = append(driversUUID, i)
	}
	driversTokens, err := GetCurrentDriversFCMToken(driversUUID)
	if err != nil {
		return fmt.Errorf("getting drivers fcm token error,%s", err)
	}
	for driveruuid, driverToken := range driversTokens {
		title := fmt.Sprintf(`Фотоконтроль "%s" просрочен :(`, data[driveruuid])
		body := "Загрузите все необходимые фотографии для разблокировки"
		alert := AlertsDRV{
			DriverUUID: driveruuid,
			Tag:        constants.FcmTagInitData,
			Message:    title + ". " + body,
		}
		err = alert.Save()

		go fcmsender.SendMessageToDriverWithCentrifugo(driveruuid, "", driverToken, constants.FcmTagInitData, 25000, title, body)
		if err != nil {
			return fmt.Errorf("error saving alert,%s", err)
		}
	}
	return nil
}
func WarnDrivers(data map[string]string) error {
	var driversUUID []string
	for i := range data {
		driversUUID = append(driversUUID, i)
	}
	if len(driversUUID) == 0 {
		return nil
	}
	driversTokens, err := GetCurrentDriversFCMToken(driversUUID)
	if err != nil {
		return fmt.Errorf("getting drivers fcm token error,%s", err)
	}
	for driverUUID, driverToken := range driversTokens {
		msg := "Можете пройти фотоконтроль заранее!"

		body := fmt.Sprintf(`Подходит время обязательного фотоконтроля "%s"! Вы можете пройти его заранее уже сейчас`, data[driverUUID])
		alert := AlertsDRV{
			DriverUUID: driverUUID,
			Tag:        constants.FcmTagInitData,
			Message:    body,
		}
		err = alert.Save()
		go fcmsender.SendMessageToDriverWithCentrifugo(driverUUID, "", driverToken, constants.FcmTagInitData, 25000, msg, body)
		if err != nil {
			return fmt.Errorf("error saving alert,%s", err)
		}
	}
	return nil
}

func (phCon *PhotoControl) notifyTitle() string {
	if phCon.Approved {
		return "Фотоконтроль принят модератором"
	}

	return "Новые данные по фотоконтролю"
}
