package models

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/looplab/fsm"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

const (
	JobQueueNameOffers           = "offers"
	JobQueueAssignOrders         = "offers/assing"
	JobQueueLimitOffers          = 199 // prime numbers works best
	setOrderStateEndpoint string = "/orders/set/state"
)

type (
	// OfferStateRequest структура которую получаю от моб. приложения
	OfferStateRequest struct {
		State           string `json:"state"`
		OfferUUID       string `json:"offer_uuid"`
		TimeArrival     int64  `json:"time_arrival"`
		FreeTime        int    `json:"-"`
		ConfirmDistance int    `json:"confirm_distance"`
		Msg             string `json:"msg"`
	}
	// ResponseForOfferState идет в ответ на зарпос об изменении статуса заказа
	ResponseForOfferState struct {
		structures.ResponseStruct
		Tariff           *structures.Tariff `json:"tariff,omitempty"`
		FreeTime         int                `json:"free_time,omitempty"`
		CurrentState     *DriverStateNotify `json:"driver_state,omitempty"`
		DistanceToTarget int                `json:"distance_to_target,omitempty"`
		StartTime        int64              `json:"start_time"` // время в секундах
		State            struct {
			Value string `json:"value"`
			Msg   string `json:"message"`
		} `json:"order_state"`
	}
	// OfferStatesDrv структура для сохранения в БД
	OfferStatesDrv struct {
		tableName struct{}  `sql:"drv_offer_states"`
		ID        int       `json:"id" sql:"-"`
		UUID      string    `json:"uuid"`
		CreatedAt time.Time `sql:"default:now()" json:"created_at"`
		FSM       *fsm.FSM  `sql:"-"`
		//External чтобы не отправлять обратно в брокер
		External bool `sql:"-" json:"-" description:"пришел ли статус извне?"`
		structures.OfferStates
		LastState string `sql:"-"`
	}
)

// InitFsm инициализация стейтмашины
func (os *OfferStatesDrv) InitFsm() {
	os.FSM = fsm.NewFSM(
		constants.OrderStateCreated,
		fsm.Events{
			// Распределение по поэтапному алгоритму
			{
				Name: constants.OrderStateDistributing,
				Src: []string{
					constants.OrderStateCreated,
					constants.OrderStateRejected,
					constants.OrderStateOffered,
				},
				Dst: constants.OrderStateDistributing,
			},
			// Свободный заказ (поиск водителя)
			{
				Name: constants.OrderStateFree,
				Src: []string{
					constants.OrderStateRejected,
					constants.OrderStateCreated,
					constants.OrderStateDistributing,
				},
				Dst: constants.OrderStateFree,
			},
			// заказ отменен
			{
				Name: constants.OrderStateCancelled,
				Src:  constants.ListOrderStates(),
				Dst:  constants.OrderStateCancelled,
			},
			// Водитель не найден
			{
				Name: constants.OrderStateDriverNotFound,
				Src: []string{
					constants.OrderStateDistributing,
					constants.OrderStateFree,
				},
				Dst: constants.OrderStateDriverNotFound,
			},
			// Предложено водителю
			{
				Name: constants.OrderStateOffered,
				Src: []string{
					constants.OrderStateCreated,
					constants.OrderStateDistributing,
					constants.OrderStateFree,
					constants.OrderStateRejected,
				},
				Dst: constants.OrderStateOffered,
			},
			// Водитель отказал
			{
				Name: constants.OrderStateRejected,
				Src: []string{
					constants.OrderStateOffered,
				},
				Dst: constants.OrderStateRejected,
			},
			// Водитель принял заказ
			{
				Name: constants.OrderStateAccepted,
				Src: []string{
					constants.OrderStateOffered,
				},
				Dst: constants.OrderStateAccepted,
			},
			// Водитель начал выполнение заказа, направляется к клиенту
			{
				Name: constants.OrderStateStarted,
				Src: []string{
					constants.OrderStateAccepted,
				},
				Dst: constants.OrderStateStarted,
			},
			// на месте (приехал к пассажирам);
			{
				Name: constants.OrderStateOnPlace,
				Src: []string{
					constants.OrderStateStarted,
				},
				Dst: constants.OrderStateOnPlace,
			},
			// начато выполнение заказа с клиентом на борту
			{
				Name: constants.OrderStateOnTheWay,
				Src: []string{
					constants.OrderStateOnPlace,
					constants.OrderStateOnPoint,
					constants.OrderStateWaiting,
				},
				Dst: constants.OrderStateOnTheWay,
			},
			// доехал до промежуточной точки
			{
				Name: constants.OrderStateOnPoint,
				Src: []string{
					constants.OrderStateOnTheWay,
				},
				Dst: constants.OrderStateOnPoint,
			},
			// начал ожидание с клиентом
			{
				Name: constants.OrderStateWaiting,
				Src: []string{
					constants.OrderStateOnTheWay,
				},
				Dst: constants.OrderStateWaiting,
			},
			// оплата заказа
			{
				Name: constants.OrderStatePayment,
				Src: []string{
					constants.OrderStateOnTheWay,
					constants.OrderStateWaiting,
				},
				Dst: constants.OrderStatePayment,
			},
			// завершение заказа
			{
				Name: constants.OrderStateFinished,
				Src: []string{
					constants.OrderStatePayment,
				},
				Dst: constants.OrderStateFinished,
			},
		},
		fsm.Callbacks{
			"enter_state": func(e *fsm.Event) {
				err := os.saveOfferState(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":     "changing offer state",
						"offerUUID": os.OfferUUID,
						"orderUUID": os.OrderUUID,
						"reason":    "saveOfferState",
						"newState":  os.State,
					}).Error(err)
					e.Cancel(err)
					return
				}

				if GodModeInst.WriteoffOfActivity.Value {
					if err := ChangeDriversActivityIfNeeded(os.OfferStates); err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event":     "changing offer state",
							"offerUUID": os.OfferUUID,
						}).Error(err)
					}
					if err := BlockDriverIfNeeded(os.OfferStates); err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event":     "changing offer state",
							"offerUUID": os.OfferUUID,
						}).Error(err)
					}
				}
			},
			"before_event": func(e *fsm.Event) {
				os.LastState = os.FSM.Current()
			},
			fmt.Sprintf("before_%s", constants.OrderStateFree): func(e *fsm.Event) {
				os.DriverUUID = ""
			},
			fmt.Sprintf("before_%s", constants.OrderStateDistributing): func(e *fsm.Event) {
				os.DriverUUID = ""
			},
			fmt.Sprintf("after_%s", constants.OrderStateAccepted): func(e *fsm.Event) {
				DistributionLogs <- NewAcceptLog(os.OfferStates)
				os.UpdateOfferAcceptedTime()
				os.OrderRouteDataDriverToClient()
				DriverSetStatus(os.DriverUUID, constants.DriverStateWorking, "Смена статуса водителя при принятии заказа")
				os.ApplyEvent(constants.OrderStateStarted)
			},
			fmt.Sprintf("after_%s", constants.OrderStateDistributing): func(e *fsm.Event) {
				if os.DriverUUID != "" {
					setDriverPreviousState(os.DriverUUID, "Смена статуса водителя после отказа от заказа [?]")
				}
			},
			fmt.Sprintf("after_%s", constants.OrderStateRejected): func(e *fsm.Event) {
				DistributionLogs <- NewRejectLog(os.OfferStates)
				off, err := PrepareOfferForDistributionByUUID(os.OfferUUID)
				NextDriverStatus(os.DriverUUID, "Смена статуса водителя после отказа от заказа", StatusEventOfferRejected)
				if err == nil && !off.TaximetrData.Order.ItIsOptional() {
					os.StartBillingAlgorithm(BillingAfterRejectionAction)
				}
			},
			fmt.Sprintf("after_%s", constants.OrderStateOnPlace): func(e *fsm.Event) {
				go PrepayOrder(os.OrderUUID, os.OfferUUID, os.DriverUUID)
			},
			fmt.Sprintf("after_%s", constants.OrderStateFinished): func(e *fsm.Event) {
				NextDriverStatus(os.DriverUUID, "Смена статуса водителя после завершения заказа", StatusEventOrderFinished)
				MakeCounterOrderCurrentIfNeed(os.DriverUUID)
				os.StartBillingAlgorithm(BillingAfterCompletionAction)
			},
			fmt.Sprintf("after_%s", constants.OrderStateCancelled): func(e *fsm.Event) {
				NextDriverStatus(os.DriverUUID, "Смена статуса водителя после отмены заказа", StatusEventOrderCancelled)
				os.UpdateOfferСancelledTime()

				go RefundClientAfterCancelledOrder(os)
				MakeCounterOrderCurrentIfNeed(os.DriverUUID)
				//go PenaltyOrRefundClientAfterCancelledOrder(os) // TODO: enable to penalty clients for cancellation
			},
		},
	)
}

// ApplyEventWithMutex НЕ ВЫЗЫВАТЬ В КОЛЛБЕКАХ СТЕЙТМАШИНЫ
func (os OfferStatesDrv) ApplyEventWithMutex(event string, forcedTranslation ...bool) error {
	var (
		offState OfferStatesDrv
	)
	// с одним и тем же объектом Event два раза почему то не работает, поэтому заводим новый
	offState.OfferStates = os.OfferStates
	offState.InitFsm()
	ft := false
	if len(forcedTranslation) != 0 {
		ft = forcedTranslation[0]
	}
	// Protect the same offer to be executed concurrently
	offerJob := jobs.GetJobQueue(JobQueueNameOffers, JobQueueLimitOffers)
	err := offerJob.Execute(uuidHash(os.OfferUUID), func() error {
		IncMutexCountMetric("mutex")
		defer DecMutexCountMetric("mutex")

		var oldState string
		if !ft {
			currentState, err := GetOfferActualState(os.OfferUUID)
			if err != nil {
				return fmt.Errorf("error getting actual offer state, %s", err)
			}
			oldState = currentState.State
		} else {
			oldState = constants.OrderStateCreated
		}

		offState.FSM.SetState(oldState)
		err := offState.FSM.Event(event)
		return err
	})
	return err
}

// ApplyEvent применяет какое либо событие стейтмашины
func (os OfferStatesDrv) ApplyEvent(event string) error {
	var (
		offState OfferStatesDrv
	)
	// с одним и тем же объектом Event два раза почему то не работает, поэтому заводим новый
	offState.OfferStates = os.OfferStates
	offState.InitFsm()
	offState.FSM.SetState(os.State)
	err := offState.FSM.Event(event)
	return err
}

func (os OfferStatesDrv) OrderRouteDataDriverToClient() error {
	offer, err := GetOfferByUUID(os.OfferUUID)
	if err != nil {
		return errpath.Err(err)
	}

	order, err := OrderDrv{}.GetOrderByUUID(os.OrderUUID)
	if err != nil {
		return errpath.Err(err)
	}

	// We have already filled the required field on setting "offer_offered" state,
	// so now we need just to load it from the db and avoid calling OSRM
	order.RouteWayData.RouteFromDriverToClient = offer.TaximetrData.OfferData.RouteToClient

	// Save the order to the db immediately
	if err = UpdateOrderRouteWayData(&order); err != nil {
		return errpath.Err(err)
	}

	err = rabsender.SendJSONByAction(rabsender.AcOfferNewState, rabbit.RouteToClient, order)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "creating user",
			"orderUUID": order.UUID,
			"reason":    "sending to rabbit error",
		}).Error(err)
	}
	return nil
}

// GetOrderTotalPrice возвращает общий расчет заказа
func GetOrderTotalPrice(offerUUID string) (int, error) {
	var offer OfferDrv
	err := db.Model(&offer).
		Where("uuid = ?", offerUUID).
		Select()
	if err != nil {
		return 0, fmt.Errorf("Error select offer for  transaction(UUID=%s), %s", offerUUID, err)
	}
	return offer.TaximetrData.Order.Tariff.TotalPrice, nil
}

// UpdateOfferAcceptedTime обновляет время приема оффера
func (os *OfferStatesDrv) UpdateOfferAcceptedTime() error {
	var offer OfferDrv
	_, err := db.Model(&offer).
		Set("accepted_at = ?", time.Now()).
		Where("uuid = ?", os.OfferUUID).
		Update()
	if err != nil {
		return fmt.Errorf("error updating Accepted time to offer (UUID=%s), %s", os.OfferUUID, err)
	}
	return nil
}

// UpdateOfferСancelledTime обновляет время отмены оффера
func (os *OfferStatesDrv) UpdateOfferСancelledTime() error {
	var offer OfferDrv
	_, err := db.Model(&offer).
		Set("cancelled_at = ?", time.Now()).
		Where("uuid = ?", os.OfferUUID).Update()
	if err != nil {
		return fmt.Errorf("error updating cancelled time to offer (UUID=%s), %s", os.OfferUUID, err)
	}
	return nil
}

// GetOfferActualState return current state of offer
func GetOfferActualState(offerUUID string) (*OfferStatesDrv, error) {
	offer := new(OfferStatesDrv)
	query := db.Model(offer).Where("offer_uuid = ?", offerUUID).Order("start_state DESC")
	if err := query.Limit(1).Select(); err != nil {
		return nil, err
	}

	return offer, nil
}

// OfferSetState для проверки статуса самого оффера
func OfferSetState(driverUUID string, os OfferStateRequest) error {

	var (
		appState OfferStatesDrv
		offer    OfferDrv
	)
	appState.InitFsm()
	appState.Comment = os.Msg
	appState.DriverUUID = driverUUID
	if os.State == constants.OrderStateAccepted {
		appState.Comment = strconv.FormatInt(os.TimeArrival, 10)
	}

	appState.StartState = time.Now()
	// Смотрим есть ли такой оффер вообще
	err := db.Model(&offer).Where("uuid = ? AND driver_uuid = ?", os.OfferUUID, driverUUID).Select()
	if err != nil {
		return errpath.Err(err, fmt.Sprintf("Offer (UUID=%s) for driver (UUID=%s) not found", os.OfferUUID, driverUUID))
	}
	// чтобы узнать в каком состоянии сейчас находится оффер
	appState.OfferUUID = offer.UUID
	appState.OrderUUID = offer.OrderUUID
	err = db.Model(&appState).
		Column("state").
		Where("offer_uuid = ?", os.OfferUUID).
		Order("start_state DESC").
		Limit(1).Select()
	if err != nil {
		return errpath.Err(err, fmt.Sprintf("OfferState  for offer (UUID=%s) not found", os.OfferUUID))
	}

	// дабы не делать 2 записи с идентичными статусами
	if appState.State == os.State {
		return nil
	}
	appState.FreeTime = os.FreeTime
	appState.FSM.SetState(appState.State)
	err = appState.FSM.Event(os.State)
	if err != nil {
		es := fmt.Sprintf("Offer state transition failed. OfferUUID=%s, currentState=%s, %s", offer.UUID, appState.State, err)
		return errpath.Errorf(es)
	}

	// Finally, update the offer's arrival time
	if os.State == constants.OrderStateAccepted {
		offer.TaximetrData.OfferData.ArrivalTime = os.TimeArrival
		if err = UpdateOfferTaximeter(offer.TaximetrData); err != nil {
			return errpath.Errorf("failed to update offer taximeter data: %v", err)
		}
	}

	return nil
}

func SetOrderStateInCRM(state structures.OfferStates) error {
	url := config.St.CRM.Host + setOrderStateEndpoint
	return request(http.MethodPost, url, &state, nil)
}

func (os *OfferStatesDrv) saveOfferState(e *fsm.Event) error {
	var offer OfferDrv
	err := db.Model(&offer).
		Where("uuid = ?", os.OfferUUID).
		Select()
	if err != nil {
		return fmt.Errorf("Error finding offer (UUID=%s), %s", os.OfferUUID, err)
	}
	os.State = e.Dst
	os.StartState = time.Now()
	os.UUID = structures.GenerateUUID()
	// if os.State == variables.OrderStateFinished || os.State == variables.OrderStateRejected {
	// 	SetDriverStatus(os.DriverUUID, os.State, "Закончил выполнять заказ")
	// }

	if !os.External {
		err = SetOrderStateInCRM(os.OfferStates)
		if err != nil {
			return err
		}
		routingKey := fmt.Sprintf("%s.%s", rabbit.StateKey, os.State)
		err = rabsender.SendJSONByAction(rabsender.AcOfferNewState, routingKey, os.OfferStates)
		if err != nil {
			return err
		}
	}
	_, err = db.Model(os).Returning("*").Insert()
	if err != nil {
		return err
	}
	// msg := fmt.Sprintf("Status = %s, Offer UUID=%s, DriverUUID=%s", os.State, os.OfferUUID, os.DriverUUID)

	stateValue := fmt.Sprintf("state[%s]", os.State)
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "saving offer state",
		"reason":     "calling saveOfferState function",
		"offerUUID":  os.OfferUUID,
		"orderUUID":  os.OrderUUID,
		"driverUUID": os.DriverUUID,
		"value":      stateValue,
	}).Debug("Transition set new status")

	// logs.OutputOrderEvent("New offer state", msg, os.OrderUUID)

	return nil
}

// CalcStartTime возвращает время начала поездки (с первого перевода в on_the_way)
func CalcStartTime(driverUUID, offerUUID string) int64 {
	var (
		appState OfferStatesDrv
	)
	_ = db.Model(&appState).
		Where("state = ? AND driver_uuid = ? AND offer_uuid = ?", constants.OrderStateOnTheWay, driverUUID, offerUUID).
		Order("start_state").
		Limit(1).Select()
	unixStartTime := appState.StartState.Unix()
	if unixStartTime <= 0 {
		return -1
	}
	return unixStartTime
}

// OrderStateTransfer обрабатывает запрос со стороны водителя на изменение статуса заказа
func (orderStateReq OfferStateRequest) OrderStateTransfer(driverUUID string) (ResponseForOfferState, error) {
	var respWithState ResponseForOfferState
	respWithState.Code = http.StatusOK
	// Protect the same offer to be executed concurrently
	offerJob := jobs.GetJobQueue(JobQueueNameOffers, JobQueueLimitOffers)
	err := offerJob.Execute(uuidHash(orderStateReq.OfferUUID), func() error {
		IncMutexCountMetric("mutex")
		defer DecMutexCountMetric("mutex")

		realState, err := GetOfferActualState(orderStateReq.OfferUUID)
		if err != nil {
			startTime := CalcStartTime(driverUUID, orderStateReq.OfferUUID)
			respWithState.StartTime = startTime
			msg := fmt.Sprintf("error getting current order states,%s", err)
			respWithState.Code = http.StatusBadRequest
			respWithState.Msg = msg
			return errors.New(msg)
		}
		// чтобы он не мог принять оффер если время ответа прошло, но тикер на автоотмену еще не сработал
		validResTime := realState.CreatedAt.Unix() + config.St.Drivers.ResponseTime + 6
		// если водила пытается принять заказ, который на данный момент ему не предложен
		if (orderStateReq.State == constants.OrderStateAccepted || orderStateReq.State == constants.OrderStateRejected) && (realState.DriverUUID != driverUUID ||
			realState.State != constants.OrderStateOffered ||
			time.Now().Unix() > validResTime) {
			respWithState.Code = http.StatusBadRequest
			respWithState.Msg = "Предложение неактуально"
			return fmt.Errorf(respWithState.Msg)
		}
		respWithState.StartTime = CalcStartTime(driverUUID, orderStateReq.OfferUUID)
		respWithState.State.Value = realState.State
		respWithState.State.Msg = constants.OrderStateRU[realState.State]

		var skip bool
		confirmLogic := func(target string, step int) {
			if orderStateReq.ConfirmDistance == 0 {
				dist, err := GetDistanceToTarget(driverUUID, orderStateReq.OfferUUID, target)
				if err != nil {
					dist = -1
				}

				var inst LastDistance
				inst.DriverUUID = driverUUID
				inst.DistanceToTarget = dist
				inst.Step = step

				if dist > structures.AllowedDistance {
					respWithState.DistanceToTarget = dist
					skip = true
					respWithState.Code = http.StatusNotAcceptable
					respWithState.Msg = fmt.Sprintf("Вы находитесь в %v метрах от точки! Введите указанное расстояние для принудительного перевода статуса заказа", respWithState.DistanceToTarget)
					err = inst.SetDriverLastDistance()
					if err != nil {
						dist = -1
					}
				}
			} else {
				var inst LastDistance
				inst.GetDriverLastDistance(driverUUID)
				if inst.Step != step {
					skip = true
					respWithState.Msg = "ай-ай, нехорошо. Обнули confirm_distance"
				} else {
					if orderStateReq.ConfirmDistance == inst.DistanceToTarget {
						skip = false
					} else {
						skip = true
						respWithState.DistanceToTarget = inst.DistanceToTarget
						respWithState.Msg = fmt.Sprintf("Введенно неверное значение! Требуется %v", respWithState.DistanceToTarget)
					}
				}
			}
		}

		if orderStateReq.State == constants.OrderStateOnPlace && respWithState.State.Value == constants.OrderStateStarted {
			confirmLogic(constants.OrderStateOnPlace, 1)
		}

		if orderStateReq.State == constants.OrderStatePayment && (respWithState.State.Value == constants.OrderStateOnTheWay || respWithState.State.Value == constants.OrderStateWaiting) {
			confirmLogic(constants.OrderStatePayment, 2)
		}

		if !skip {
			if orderStateReq.State == constants.OrderStateOnPlace {
				freeTime, err := CalcBoardingFreeTime(orderStateReq.OfferUUID)
				if err != nil {
					msg := fmt.Sprintf("error calc boarding free time,%s", err)
					respWithState.Code = http.StatusBadRequest
					respWithState.Msg = msg
					return errors.New(msg)
				}
				respWithState.FreeTime = freeTime

				err = SetOfferOnPlaceTime(orderStateReq.OfferUUID)
				if err != nil {
					msg := fmt.Sprintf("error calc offer waiting time,%s", err)
					respWithState.Code = http.StatusBadRequest
					respWithState.Msg = msg
					return errors.New(msg)
				}
				// respWithState.Tariff = &trf

				startTime := CalcStartTime(driverUUID, orderStateReq.OfferUUID)
				respWithState.StartTime = startTime
				orderStateReq.FreeTime = freeTime
				orderStateReq.Msg = strconv.Itoa(respWithState.FreeTime)
				// return respWithState, nil
			}

			if orderStateReq.State == constants.OrderStateWaiting {
				freeTime, err := CalcWaitingFreeTime(orderStateReq.OfferUUID)
				if err != nil {
					respWithState.Code = http.StatusBadRequest
					return fmt.Errorf("error calc waiting free time,%s", err)
				}
				respWithState.FreeTime = freeTime
				startTime := CalcStartTime(driverUUID, orderStateReq.OfferUUID)
				respWithState.StartTime = startTime
				orderStateReq.FreeTime = freeTime
				orderStateReq.Msg = strconv.Itoa(respWithState.FreeTime)
				// return respWithState, nil
			}

			err := OfferSetState(driverUUID, orderStateReq)
			if err != nil {
				respWithState.Code = http.StatusBadRequest
				respWithState.Msg = fmt.Sprintf("Can't change offer state.%s", err)
				startTime := CalcStartTime(driverUUID, orderStateReq.OfferUUID)
				respWithState.StartTime = startTime
				return fmt.Errorf(respWithState.Msg)
			}

			respWithState.State.Value = orderStateReq.State
			respWithState.State.Msg = constants.OrderStateRU[orderStateReq.State]

			msg := fmt.Sprintf("Ok! OfferUUID = %v, State = %s", orderStateReq.OfferUUID, orderStateReq.State)
			respWithState.Code = http.StatusOK
			respWithState.Msg = msg
			startTime := CalcStartTime(driverUUID, orderStateReq.OfferUUID)
			respWithState.StartTime = startTime
			if orderStateReq.State == constants.OrderStatePayment ||
				orderStateReq.State == constants.OrderStateOnTheWay {
				trf, err := CalcOfferWaitingTime(orderStateReq.OfferUUID)
				if err != nil {
					msg := fmt.Sprintf("error calc offer waiting time,%s", err)
					respWithState.Code = http.StatusBadRequest
					respWithState.Msg = msg
					return errors.New(msg)
				}

				respWithState.Tariff = &trf
				return nil
			}
			return nil
		} else {
			respWithState.Code = http.StatusNotAcceptable
		}
		return nil
	})
	return respWithState, err
}
