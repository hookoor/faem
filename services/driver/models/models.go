package models

import (
	"fmt"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/crypto"
	"gitlab.com/faemproject/backend/faem/pkg/jobqueue"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/osrm"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
)

const (
	// BillingAfterRejectionAction godoc
	BillingAfterRejectionAction = "rej"
	// BillingAfterCompletionAction godoc
	BillingAfterCompletionAction = "comp"
	MessageForDriverWhenAutocall = " ВНИМАНИЕ! Свяжитесь с клиентом перед поездкой. Заказ создан более 10 минут назад."
	MessageForClientWhenAutocall = "К сожалению, пока мы не нашли машину, нажмите 2 для отмены заказа или ожидайте дальнейшего поиска $ Ваш заказ отменен, спасибо $ и, "
)

var (
	db     *pg.DB
	rb     *rabbit.Rabbit
	jobs   = jobqueue.NewJobQueues()
	osrmka *osrm.OSRM

	// GodModeInst -
	GodModeInst structures.GodMode
)

func init() {
	GodModeInst.Init()
}

func appendIfNotExists(inArray []string, element string) []string {
	for _, val := range inArray {
		if val == element {
			return inArray
		}
	}
	return append(inArray, element)
}
func GetKeys(in map[string]string) []string {
	var res []string
	for i := range in {
		res = append(res, i)
	}
	return res
}

// DrvAppVersion godoc
type DrvAppVersion struct {
	tableName struct{}  `pg:",discard_unknown_columns"`
	ID        int       `json:"id" sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
	Version   string    `json:"version"`
	Link      string    `json:"link"`
}

// OrderDrv структура для сохранения заказов внутри системы
type OrderDrv struct {
	tableName        struct{}  `sql:"drv_orders" pg:",discard_unknown_columns"`
	ID               int       `json:"id" sql:",pk"`
	NextAutocallTime time.Time `json:"-"`
	structures.Order
}
type ormStruct struct {
	Deleted   bool      `json:"-" sql:"default:false"`
	ID        int       `json:"id" sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
	UpdatedAt time.Time `json:"updated_at" `
}

// ConnectDB initialize connection to package var
func ConnectDB(conn *pg.DB) {
	db = conn
	osrmka = osrm.New(config.St.OSRM.Host)
}

// ConnectRabbit initialize connection to package var
func ConnectRabbit(rab *rabbit.Rabbit) {
	rb = rab
}

// MessageLongDistributionAdded returns true if message about long distribution already added to order comment
func (or *OrderDrv) MessageLongDistributionAdded(callInterval time.Duration) bool {
	timeInDistribuition := time.Now().Unix() - or.CreatedAt.Unix()
	return timeInDistribuition/int64(callInterval.Seconds()) != 1
}

// Change godoc
func (ver DrvAppVersion) Change() (DrvAppVersion, error) {
	if ver.Link == "" || ver.Version == "" {
		return ver, fmt.Errorf("fields version and link is required")
	}
	ver.CreatedAt = time.Now()
	_, err := db.Model(&ver).Insert()
	return ver, err
}
func UpdateOrdersNextCallTime(ordersUUID []string, dur time.Duration) error {
	if len(ordersUUID) == 0 {
		return nil
	}
	_, err := db.Model(&OrderDrv{}).
		Where("uuid in (?)", pg.In(ordersUUID)).
		Set("next_autocall_time = ?", time.Now().Add(dur)).
		Update()
	return err
}

// GetActualVersion возвращает актуальную версию приложения
func GetActualVersion() (DrvAppVersion, error) {
	var ver DrvAppVersion
	err := db.Model(&ver).
		Order("created_at DESC").
		Limit(1).
		Select()
	return ver, err
}

// GetByUUID returning object by interface and UUID
func GetByUUID(uuid string, mdl interface{}) error {
	err := db.Model(mdl).
		Where("uuid = ?", uuid).
		// Where("deleted is not true").
		Select()
	if err != nil {
		return err
	}
	return nil
}

// UpdateOrderRating -
func UpdateOrderRating(order *OrderDrv) error {
	_, err := db.Model(order).
		Set("driver_rating = ?driver_rating").
		Set("client_rating = ?client_rating").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}
	return nil
}

// UpdateOrderRouteWayData updates only route_way_data field
func UpdateOrderRouteWayData(order *OrderDrv) error {
	_, err := db.Model(order).
		Set("route_way_data = ?route_way_data").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}
	return nil
}

//
func (this OrderDrv) GetOrderByUUID(uuid string) (OrderDrv, error) {
	if uuid == "" {
		uuid = this.UUID
		if uuid == "" {
			return OrderDrv{}, fmt.Errorf("uuid is empty")
		}
	}

	err := db.Model(&this).Where("uuid = ?", uuid).Select()
	if err != nil {
		return OrderDrv{}, fmt.Errorf("Order not found [%s]", err)
	}

	return this, nil
}

func uuidHash(uuid string) int {
	return crypto.FNV(uuid)
}
