package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	xos "gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/smpp"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/db"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/handlers"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabhandler"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
	"gitlab.com/faemproject/backend/faem/services/driver/router"
	"gitlab.com/faemproject/backend/faem/services/driver/tickers"
	"gitlab.com/faemproject/backend/faem/services/driver/tickers/tickersmodern"
)

const (
	version = "0.2.0"

	brokerShutdownTimeout = 30 * time.Second
	serverShutdownTimeout = 30 * time.Second
	tickerShutdownTimeout = 30 * time.Second
)

func main() {

	bVersion := flag.Bool("ver", false, "Output current app vesion and exit")
	bEnvVars := flag.Bool("env", false, "Output envieroinment vars and exit")
	configPath := flag.String("config", "config/driver.toml", "Default config filepath")

	flag.Parse()
	config.InitConfig(*configPath)

	if *bVersion {
		fmt.Println("Version: ", version)
		os.Exit(0)
	}

	if *bEnvVars {
		config.PrintVars()
		os.Exit(0)
	}

	if err := logs.SetLogLevel(config.St.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(config.St.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	eloger := logs.Eloger

	conn, err := db.Connect()
	if err != nil {
		fmt.Println(err)
		os.Exit(4)
	}
	defer db.CloseDbConnection(conn)
	fmt.Println("Connecting to Postgres successfuly", conn)
	models.ConnectDB(conn)

	cloudStorage, err := cloudstorage.NewGCStorage(context.Background(), cloudstorage.Config{
		ProjectID:  config.St.CloudStorage.ProjectID,
		BucketName: config.St.CloudStorage.BucketName,
	})
	if err != nil {
		eloger.Errorf("failed to create a cloud storage client: %v", err)
	}

	if err = enableProfiler(); err != nil {
		eloger.Error(err)
	}

	smsSender, err := smpp.NewSmsSender(config.St.SMPP.Host, config.St.SMPP.User, config.St.SMPP.Password, config.St.SMPP.From)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "init",
			"reason": "Error connecting to SMPP server",
		}).Error(err)
	}

	// трассировка подключений
	go func() {
		for c := range smsSender.ConnStatus {
			logs.Eloger.WithFields(logrus.Fields{
				"value": c.Status(),
			}).Info("SMPP connection status")
		}
	}()

	// асинхронная проверка статусов отправки СМС
	go func() {
		for {
			mes := <-smsSender.SenderStatus
			switch mes.State {
			case "Ok":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Info("SMS sended!")
			case "Warn":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Warn("Sender attempt")
			case "Error":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Error("Sms not send")
			}
			if mes.State == "OK" {
			}
		}
	}()

	rb := rabbit.New()
	rb.Credits.User = config.St.Broker.UserCredits
	rb.Credits.URL = config.St.Broker.UserURL
	err = rb.Init(config.St.Broker.ExchagePrefix, config.St.Broker.ExchagePostfix)
	if err != nil {
		fmt.Println("Error connecting to Rabbit", err)
		os.Exit(5)
	}
	defer rb.CloseRabbit()
	models.ConnectRabbit(rb)

	err = rabsender.StartService(rb)
	if err != nil {
		fmt.Println("Error starting rabbit service", err)
		os.Exit(6)
	}
	defer rabsender.Wait(brokerShutdownTimeout)

	// Create a publisher
	pub := rabsender.Publisher{
		Rabbit:  rb,
		Encoder: &rabbit.JsonEncoder{},
	}
	if err = pub.Init(); err != nil {
		eloger.Fatalf("failed to init the publisher: %v", err)
	}
	defer pub.Wait(brokerShutdownTimeout)

	fcmCl := fcm.NewClient(config.St.FCM.ServerKey)
	fcmsender.Init(fcmCl)
	fmt.Println("Connecting to RabbitMQ successfuly")

	err = rabhandler.InitRabHandler(rb, conn)
	if err != nil {
		fmt.Printf("Error initing Rabbit handler [%s]", err)
		os.Exit(7)
	}
	defer rabhandler.Wait(brokerShutdownTimeout)

	dataBase := db.DB{
		ModelPhotocontrol: &db.ModelPhotocontrol{DB: conn},
	}
	srv := handlers.NewServer(smsSender,
		cloudStorage,
		&db.Repository{DB: conn},
		&pub,
		dataBase,
		tickersmodern.Init(conn, dataBase),
	)

	defer srv.Tickers.Wait(tickerShutdownTimeout)
	tickers.Init(conn)
	defer tickers.Wait(tickerShutdownTimeout)

	e := router.Init(eloger, srv)

	// Prometheus metrics
	bmetrics := models.BusinessMetrics()
	p := prometheus.NewPrometheus("echo", nil, bmetrics)
	p.Use(e)

	if err = models.InitPrometheus(p); err != nil {
		eloger.Errorf("Error init prometheus metrics: %v", err)
	}

	// Start an http server and remember to shut it down
	go web.Start(e, config.St.Application.Port)
	defer web.Stop(e, serverShutdownTimeout)

	// Wait for program exit
	<-xos.NotifyAboutExit()
}

func enableProfiler() error {
	// Enable profiler if required

	// Profiler initialization, best done as early as possible
	// if err := profiler.Start(profiler.Config{
	// 	Service:        "faem-driver",
	// 	ServiceVersion: version,
	// 	MutexProfiling: true,
	// }); err != nil {
	// 	return fmt.Errorf("failed to init the google cloud profiler: %v", err)
	// }
	return nil
}
