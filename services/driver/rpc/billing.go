package rpc

// Unused now, might get handly later

//import (
//	"bytes"
//	"encoding/json"
//	"io/ioutil"
//	"net/http"
//	"time"
//
//	"github.com/pkg/errors"
//)
//
//const (
//	maxRequestAttempts = 3
//	retryInterval      = 1 * time.Second
//)
//
//type BillingConfig struct {
//	Host     string
//	Username string
//	Password string
//}
//
//type BillingClient struct {
//	HttpClient *http.Client
//	Config     BillingConfig
//}
//
//func (c *BillingClient) request(method, url string, payload, response interface{}) error {
//	body, err := json.Marshal(payload)
//	if err != nil {
//		return errors.Wrap(err, "failed to marshal a payload")
//	}
//
//	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
//	if err != nil {
//		return errors.Wrap(err, "failed to create an http request")
//	}
//	req.Header.Set("Content-Type", "application/json")
//	req.SetBasicAuth(c.Config.Username, c.Config.Password)
//
//	resp, err := c.HttpClient.Do(req)
//	if err != nil {
//		return errors.Wrap(err, "failed to make a post request")
//	}
//	defer resp.Body.Close()
//
//	if resp.StatusCode >= http.StatusBadRequest {
//		body, err := ioutil.ReadAll(resp.Body)
//		if err != nil {
//			return errors.Errorf("post request to %s failed with status: %d", url, resp.StatusCode)
//		}
//		return errors.Errorf(
//			"post request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body),
//		)
//	}
//	if response != nil {
//		return json.NewDecoder(resp.Body).Decode(response)
//	}
//	return nil
//}
//
//type temporary interface {
//	Temporary() bool
//}
//
//func isTemporaryError(err error) bool {
//	if err == nil {
//		return false
//	}
//
//	cause := errors.Cause(err)
//	if te, ok := cause.(temporary); ok {
//		return te.Temporary()
//	}
//	return false
//}
