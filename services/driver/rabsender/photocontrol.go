package rabsender

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (p *Publisher) PhotoControlCompleted(photoControl structures.PhotoControl) error {
	return p.PublishByAction(AcPhotoControl, photoControl)
}

func (p *Publisher) initPhotocontrolExchange() error {
	photocontrolSender, err := p.Rabbit.GetSender(rabbit.CrmNewPhotoControlQueue)
	if err != nil {
		return err
	}

	err = photocontrolSender.ExchangeDeclare(
		rabbit.PhotocontrolExchange, // name
		"topic",                     // type
		true,                        // durable
		false,                       // auto-deleted
		false,                       // internal
		false,                       // no-wait
		nil,                         // arguments
	)
	return errors.Wrap(err, "failed to create an exchange")

}
