package rabsender

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

var (
	rb *rabbit.Rabbit
	wg sync.WaitGroup

	// AcOfferToDriver godoc
	AcOfferToDriver = "offer_to_driver"
	AcNewAutocall   = "new_driver_reg_autocall"
	// AcNewConference новая конференция
	AcNewConference = "new_сonference"
	// AcNewDriverToCRM godoc
	AcNewDriverToCRM = "new_driver"
	// AcOrderDataUpdateByTariff godoc
	AcOrderDataUpdateByTariff = "order_update_by_tariff"
	// AcOrderDataUpdate godoc
	AcOrderDataUpdate            = "order_update"
	AcOrderDataOnlyCommentUpdate = "order_update_comment"
	AcDriverDataUpdate           = "driver_update"
	// AcOfferNewState godoc
	AcOfferNewState = "new_offer_state"
	// AcDistributionResult godoc
	AcDistributionResult = "score_result"
	// AcChangeDriverState godoc
	AcChangeDriverState = "change_driver_state"
	// AcDriverLocation godoc
	AcDriverLocation = "driver_location"
	// AcPhotoControl godoc
	AcPhotoControl          = "photo_control"
	AcDriverActivityChanged = "driver_activity_changed"
	AcDriverKarmaChanged    = "driver_karma_changed"
	AcMakeDriverTransfer    = "make_driver_transfer"
	AcPrepayOrder           = "prepay_order"
	AcPenaltyClient         = "penalty_client"
	AcRefundClient          = "refund_client"
	AcMakeReceipt           = "make_receipt"

	// AcNewPushMailing godoc // удалить при переносе фотоконтроля в отдельный сервис
	AcNewPushMailing = "push_mailing"

	AcNewExpiredPhotocontrol = "exipired_photocontrol"
	AcBlockDriverState       = "block_driver_state"
)

func StartService(r *rabbit.Rabbit) error {
	rb = r
	return nil
}

// SendJSONByAction выбиракм экшен и отправляем
func SendJSONByAction(action, key string, payload interface{}, forDriverServiceToo ...bool) error {
	var err error
	forDriverParam := false
	if len(forDriverServiceToo) != 0 {
		forDriverParam = forDriverServiceToo[0]
	}

	switch action {
	case AcDistributionResult:
		err = sendJSON(rabbit.OrderExchange, rabbit.DistributionKey, payload)
	case AcChangeDriverState:
		err = sendJSON(rabbit.DriverExchange, key, payload)
	case AcOfferToDriver:
		// Новый оффер, ключ UUID водителя
		err = sendJSON(rabbit.OfferExchange, rabbit.NewKey, payload)
	case AcNewDriverToCRM:
		// Новый водитель
		err = sendJSON(rabbit.DriverExchange, rabbit.NewKey, payload)
	case AcOfferNewState:
		// Изменение статуса заказа
		err = sendJSON(rabbit.OrderExchange, key, payload)
	case AcNewConference:
		// structures.NewConferenceData
		err = sendJSON(rabbit.VoipExchange, rabbit.NewConferenceKey, payload)
	case AcOrderDataUpdate:
		// Изменение данных заказа
		err = sendJSON(rabbit.OrderExchange, rabbit.UpdateKey, payload, forDriverParam)
	case AcOrderDataOnlyCommentUpdate:
		// Изменение данных заказа при обновлении коммента
		err = sendJSON(rabbit.OrderExchange, rabbit.UpdateOnlyCommentKey, payload, forDriverParam)
	case AcOrderDataUpdateByTariff:
		// Изменение данных заказа из за тарифа
		err = sendJSON(rabbit.OrderExchange, rabbit.UpdateOrderTariffKey, payload)
	case AcNewAutocall:
		err = sendJSON(rabbit.VoipExchange, rabbit.AutoCallKey, payload)
	case AcDriverLocation:
		// Передача координат водителя
		err = sendJSON(rabbit.DriverExchange, rabbit.LocationKey, payload)
	case AcDriverDataUpdate:
		err = sendJSON(rabbit.DriverExchange, rabbit.UpdateKey, payload)
	case AcPhotoControl:
		// Передача готового фотоконтроля
		err = sendJSON(rabbit.DriverExchange, rabbit.PhotoControlKey, payload)
	case AcDriverActivityChanged:
		// Notify about driver's activity changing
		err = sendJSON(rabbit.DriverExchange, rabbit.ActivityChangedKey, payload)
	case AcDriverKarmaChanged:
		// Notify about driver's karma changing
		err = sendJSON(rabbit.DriverExchange, rabbit.KarmaChangedKey, payload)
	case AcMakeDriverTransfer:
		err = sendJSON(rabbit.BillingExchange, rabbit.NewKey, payload)
	case AcPrepayOrder:
		err = sendJSON(rabbit.BillingExchange, rabbit.PrepayKey, payload)
	case AcPenaltyClient:
		err = sendJSON(rabbit.BillingExchange, rabbit.PenaltyClient, payload)
	case AcRefundClient:
		err = sendJSON(rabbit.BillingExchange, rabbit.RefundClient, payload)
	case AcMakeReceipt:
		err = sendJSON(rabbit.BillingExchange, rabbit.GenerateReceipt, payload)
	case AcNewPushMailing: // пуш для водителя, удалить при переносе фотоконтроля в отдельный сервис
		err = sendJSON(rabbit.FCMExchange, rabbit.MailingKey, payload)
	case AcNewExpiredPhotocontrol: // отправка просроченного фотоконтроля
		err = sendJSON(rabbit.PhotocontrolExchange, rabbit.ExpiredKey, payload)
	case AcBlockDriverState:
		rkey := fmt.Sprintf("%s.%s", rabbit.StateKey, key)
		err = sendJSON(rabbit.DriverExchange, rkey, payload)
	default:
		err = fmt.Errorf("Unknown action, cant send")
	}
	return err
}

// SendJSON сабж
func sendJSON(exchange string, key string, payload interface{}, forDriverServiceToo ...bool) error {
	wg.Add(1)
	defer wg.Done()

	forDriverParam := false
	if len(forDriverServiceToo) != 0 {
		forDriverParam = forDriverServiceToo[0]
	}
	amqpHeader := amqp.Table{}
	amqpHeader["publisher"] = "driver"
	amqpHeader["fordrivertoo"] = forDriverParam
	pl, err := json.Marshal(payload)
	if err != nil {
		return nil
	}

	senderChannel, err := rb.GetSender("driverSender")
	if err != nil {
		return errors.Wrap(err, "failed to get a sender channel")
	}

	err = senderChannel.Publish(
		exchange, // exchange
		key,      // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         pl,
			Headers:      amqpHeader,
			DeliveryMode: amqp.Persistent,
		})
	if err != nil {
		return nil
	}
	msg := fmt.Sprintf("exchange = %s; key = %s", exchange, key)
	// logs.OutputDebugEvent("Send to Rabbit", msg, 0)

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Send JSON to RabbitMQ",
		"value": msg,
	}).Debug("Sended to RabbitMQ")

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("rabbit sender stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("rabbit sender stopped forcibly")
	}
}
