package rabsender

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
)

const notifierSenderChannel = "notifierSender"

// SendByCentrifugo -
func (p *Publisher) SendByCentrifugo(payload interface{}) error {
	senderChannel, err := p.Rabbit.GetSender(notifierSenderChannel)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	err = p.Publish(senderChannel, rabbit.NotifierExchange, rabbit.NewKey, payload)
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// CentrifugoPresence -
func CentrifugoPresence(channel, client string) (bool, error) {
	epoint := structures.EndPoints.Notyfire.IsPresence
	url := config.St.Notifier.Host + tool.SetURLParams(epoint.URL(), map[string]string{
		epoint.Params.Client():  client,
		epoint.Params.Channel(): channel,
	})

	var connExist bool
	if err := tool.SendRequest(epoint.Method(), url, nil, nil, &connExist); err != nil {
		return false, errpath.Err(err)
	}

	return connExist, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
