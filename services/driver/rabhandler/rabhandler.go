package rabhandler

import (
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
)

var (
	db *pg.DB

	rb     *rabbit.Rabbit
	wg     sync.WaitGroup
	closed = make(chan struct{})
)

func InitRabHandler(r *rabbit.Rabbit, conn *pg.DB) error {
	rb = r
	db = conn

	initters := []func() error{
		InitDriverDataRabbit,
		InitClientLocations,
		InitNewDriver,
		initTicketsEvents,
		initTelephonyEventsDRV,
		InitOffersRabbit,
		InitDriverStateRabbit,
		InitFCMMessageReceiver,
		initRabOrderSetRating,
		initBillingCompleted,
		initPaymentTypeChanged,
		InitDriverDeleted,
		InitUpdateGroupInDrivers,
		//InitPhotoControlReceiver,
		initDriverIncomeTracker,
	}

	for i, init := range initters {
		if err := init(); err != nil {
			return errors.Wrapf(err, "error initting #%d rabreciever", i)
		}
	}

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("subscriber stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("subscriber stopped forcibly")
	}
}
