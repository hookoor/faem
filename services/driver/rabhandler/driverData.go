package rabhandler

// По идее этот модуль отвечает за получение и отработку водительских статусов

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/models"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// InitNewDriver godoc
func InitNewDriver() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverDriverUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для получения водителей
	driverQueue, err := receiverChannel.QueueDeclare(
		rabbit.DriverNewDriverQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		driverQueue.Name,      // queue name
		rabbit.NewKey,         // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	newDRST, err := receiverChannel.Consume(
		driverQueue.Name,               // queue
		rabbit.DriverNewDriverConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewDriver(newDRST)
	return nil
}

// InitDriverDataRabbit godoc
func InitDriverDataRabbit() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverDriverUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для получения обновлений водителей
	driverDataQueue, err := receiverChannel.QueueDeclare(
		rabbit.DriverDriverUpdateQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		driverDataQueue.Name,  // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	newDRST, err := receiverChannel.Consume(
		driverDataQueue.Name,              // queue
		rabbit.DriverDriverUpdateConsumer, // consumer
		true,                              // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewDriverData(newDRST)
	return nil
}

func InitDriverDeleted() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverDriverDeleteQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.DriverDriverDeleteQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}

	// Биндим очередь
	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.DeleteKey,      // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		queue.Name,                         // queue
		rabbit.DriverDriverDeletedConsumer, // consumer
		true,                               // auto-ack
		false,                              // exclusive
		false,                              // no-local
		false,                              // no-wait
		nil,                                // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverDeleted(msgs)
	return nil
}

func handleNewDriverData(newDRST <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-newDRST:
			if d.Headers["publisher"] == "driver" {
				continue
			}
			var (
				uDrvData structures.Driver
				drvApp   models.DriversApps
			)
			err := json.Unmarshal(d.Body, &uDrvData)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new driver data",
					"reason": "Error unmarshalling data from rabbit",
				}).Error(err)
				continue
			}

			drvApp.Driver = uDrvData
			_, err = db.Model(&drvApp).Where("uuid = ?", uDrvData.UUID).UpdateNotNull()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new driver data",
					"reason": "Error updating driver",
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling new driver data",
				"driverUUID": uDrvData.UUID,
			}).Info("New driver data saved")
		}
	}
}

func handleNewDriver(newDRV <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-newDRV:
			if d.Headers["publisher"] == "driver" {
				continue
			}
			var (
				drvApp models.DriversApps
			)

			err := json.Unmarshal(d.Body, &drvApp.Driver)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":       "handling new driver",
				"driverUUID":  drvApp.UUID,
				"driverPhone": drvApp.Phone,
			})
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error unmarshalling data from rabbit",
				}).Error(err)
				continue
			}
			err = drvApp.CreateNewDriverFromBroker()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error saving driver",
				}).Error(err)
				continue
			}
			log.WithFields(logrus.Fields{}).Info("New driver")
		}
	}
}

func handleDriverDeleted(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "driver" {
				continue
			}

			var deleted structures.DeletedObject
			err := json.Unmarshal(d.Body, &deleted)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling deleted driver data",
					"reason": "Error unmarshalling data from rabbit",
				}).Error(err)
				continue
			}

			_, err = db.Model((*models.DriversApps)(nil)).
				Where("uuid = ?", deleted.UUID).
				Set("deleted = true").
				Update()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling deleted driver data",
					"reason": "Error deleting driver",
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling deleted driver data",
				"driverUUID": deleted.UUID,
			}).Info("Driver deleted")
		}
	}
}
