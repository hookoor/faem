package rabhandler

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func initTicketsEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverTicketsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}
	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverTicketsQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                 // queue name
		"#",                    // routing key
		rabbit.TicketsExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                       // queue
		rabbit.DriverTicketsConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleTickets(msgs)
	return nil
}

func handleTickets(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "driver" {
				continue
			}
			if d.RoutingKey != rabbit.StateKey && d.RoutingKey != rabbit.UpdateKey {
				continue
			}
			var (
				tick structures.Ticket
			)
			err := json.Unmarshal(d.Body, &tick)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":       "handling tickets",
				"ticketUUID":  tick.UUID,
				"ticketState": tick.Status,
				"routingKey":  d.RoutingKey,
			})
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error unmarshalling data",
				}).Error(err)
				continue
			}
			if (d.RoutingKey == rabbit.StateKey && !tick.Status.IsFiniteState()) || tick.SourceType != structures.DriverMember {
				continue
			}
			token, err := models.GetCurrentDriverFCMToken(tick.DriverData.UUID)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error getting token",
				}).Error(err)
				continue
			}
			var msgForAlert, body, msg string
			var lastComment structures.TicketComment
			if len(tick.Comments) != 0 {
				lastComment = tick.Comments[len(tick.Comments)-1]
				body = lastComment.Message
			}
			if d.RoutingKey == rabbit.StateKey {
				switch tick.Status {
				case structures.ResolvedStatus:
					msg = "Ваше обращение решено!"
				case structures.NotResolvedStatus:
					msg = "Ваше обращение не решено("
				}
				msgForAlert = fmt.Sprintf("%s Комментарий администратора: %s", msg, body)
			}
			if d.RoutingKey == rabbit.UpdateKey {
				if lastComment.SenderType != structures.UserCRMMember {
					continue
				}
				msg = "Новый комментарий к вашему обращению"
				msgForAlert = fmt.Sprintf("%s : %s", msg, body)
			}

			go fcmsender.SendMessageToDriverWithCentrifugo(
				tick.DriverData.UUID,
				msgForAlert,
				token,
				fcmpkg.MailingTag,
				36000,
				msg,
				body,
			)

			alert := models.AlertsDRV{
				DriverUUID: tick.DriverData.UUID,
				Tag:        fcmpkg.MailingTag,
				Message:    msgForAlert,
				TicketUUID: tick.UUID,
			}
			err = alert.Save()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error saving alert",
				}).Error(err)
				continue
			}
			log.WithFields(logrus.Fields{
				"reason": "done successfully",
			}).Info("new ticket state handled")
		}
	}
}
