package rabhandler

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/models"

	"github.com/go-pg/pg"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// InitUpdateGroupInDrivers -
func InitUpdateGroupInDrivers() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverDriverUpdateQueue)
	if err != nil {
		return errpath.Err(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для получения водителей
	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverUpdateGroupInDriversQueue, // name
		true,                                   // durable
		false,                                  // delete when unused
		false,                                  // exclusive
		false,                                  // no-wait
		nil,                                    // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		q.Name,                      // queue name
		rabbit.UpdateGroupInDrivers, // routing key
		rabbit.DriverExchange,       // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	delivery, err := receiverChannel.Consume(
		q.Name, // queue
		rabbit.DriverUpdateGroupInDriversConsumer, // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleUpdateGroupInDrivers(delivery)
	return nil
}

func handleUpdateGroupInDrivers(delivery <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case del := <-delivery:
			if del.Headers["publisher"] == "driver" {
				continue
			}
			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "handling update group in drivers",
			})
			var (
				uDrvData structures.UpdateDriverGroup
				drvApp   models.DriversApps
			)
			err := json.Unmarshal(del.Body, &uDrvData)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error unmarshalling data from rabbit",
				}).Error(errpath.Err(err))
				continue
			}

			var selectedDriversUUIDS []string
			for _, drv := range uDrvData.Drivers {
				selectedDriversUUIDS = append(selectedDriversUUIDS, drv.UUID)
			}
			_, err = db.Model(&drvApp).Where("uuid in (?)", pg.In(selectedDriversUUIDS)).Set("driver_group = ?", uDrvData.DriversGroups).UpdateNotNull()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error updating driver_group",
				}).Error(errpath.Err(err))
				continue
			}
			log.WithFields(logrus.Fields{
				"driverGroupUUID": uDrvData.DriversGroups.UUID,
			}).Info("update group in drivers successfuly")
		}
	}
}
