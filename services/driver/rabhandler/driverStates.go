package rabhandler

// По идее этот модуль отвечает за получение и отработку водительских статусов

import (
	"encoding/json"
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// InitDriverStateRabbit godoc
func InitDriverStateRabbit() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverDriverStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для получения статусов водитлей
	driverStateQueue, err := receiverChannel.QueueDeclare(
		rabbit.DriverDriverStateQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения статусов водителей
	err = receiverChannel.QueueBind(
		driverStateQueue.Name, // queue name
		"state.*",             // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера на получение статусов водителя
	newDRST, err := receiverChannel.Consume(
		driverStateQueue.Name,      // queue
		rabbit.DriverStateConsumer, // consumer
		true,                       // auto-ack
		false,                      // exclusive
		false,                      // no-local
		false,                      // no-wait
		nil,                        // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewDriverStates(newDRST)
	return nil
}

// TODO: перенести это в хэндлеры почему это в моделях?
func handleNewDriverStates(newDRST <-chan amqp.Delivery) {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling driver state",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-newDRST:
			drvState := new(structures.DriverStates)
			if err := json.Unmarshal(d.Body, drvState); err != nil {
				log.WithField("reason", "Error unmarshalling state from rabbit").Error(err)
				continue
			}
			log = log.WithFields(logrus.Fields{
				"driver_uuid": drvState.DriverUUID,
				"state":       drvState.State,
			})

			if d.Headers["publisher"] == "driver" &&
				!(drvState.State == constants.DriverStateBlocked || drvState.State == constants.DriverStateAvailable) {
				continue
			}

			if _, err := models.SaveDriverStateFromCRM(*drvState); err != nil {
				log.WithField("reason", "Error saving driver state").Error(err)
				continue
			}

			if !(drvState.State == constants.DriverStateBlocked || drvState.State == constants.DriverStateAvailable) {
				continue
			}

			token, err := models.GetCurrentDriverFCMToken(drvState.DriverUUID)
			if err != nil {
				log.WithField("reason", "Error getting driver fcm token").Error(err)
				break
			}

			title := "Изменение статуса аккаунта"
			msg := getInfoMessage(drvState.State, drvState.Comment)
			payload := title + ": " + msg

			alert := models.AlertsDRV{
				DriverUUID: drvState.DriverUUID,
				Tag:        constants.FcmTagInitData,
				Message:    msg,
			}
			if err := alert.Save(); err != nil {
				log.WithField("reason", "error saving alert").Error(err)
			}
			log.Info("New driver state")

			go fcmsender.SendMessageToDriverWithCentrifugo(drvState.DriverUUID, payload, token, constants.FcmTagInitData, 36000, title, msg)
		}
	}
}

func getInfoMessage(state, reason string) string {
	switch state {
	case constants.DriverStateAvailable:
		return "Ваш аккаунт разблокировали"
	case constants.DriverStateBlocked:
		comment := "К сожалению, ваш аккаунт заблокировали. Обратитесь к администратору."
		if reason != "" {
			comment += fmt.Sprintf(" Причина: %s", reason)
		}
		return comment
	}
	return ""
}
