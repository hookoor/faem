package rabhandler

import (
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

func initDriverIncomeTracker() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverIncomeTrackerQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverIncomeTrackerQueue, // name
		true,                            // durable
		false,                           // delete when unused
		false,                           // exclusive
		false,                           // no-wait
		nil,                             // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		rabbit.StateKey+".*", // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)

	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                             // queue
		rabbit.DriverIncomeTrackerConsumer, // consumer
		true,                               // auto-ack
		false,                              // exclusive
		false,                              // no-local
		false,                              // no-wait
		nil,                                // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOfferStates(msgs)

	return nil
}

func handleOfferStates(messages <-chan amqp.Delivery) {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order state",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-messages:
			var offerState structures.OfferStates

			// unmarshal incoming message
			err := json.Unmarshal(d.Body, &offerState)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error binding offer state",
				}).Error(errpath.Err(err))
				continue
			}

			// chose handle function based on state
			var handleFunc func(context.Context, structures.OfferStates, int) error
			switch offerState.State {
			case constants.OrderStateFinished:
				handleFunc = handleFinishedOffer
			case constants.OrderStateRejected:
				handleFunc = handleRejectedOffer
			default:
				continue
			}

			go func() {
				offer, err := models.GetOfferByUUID(offerState.OfferUUID)
				if err != nil {
					log.Error(err)
					return
				}

				price := offer.TaximetrData.Order.Tariff.TotalPrice
				err = handleFunc(context.Background(), offerState, price)
				if err != nil {
					log.Error(err)
				}
			}()
		}
	}
}

func handleFinishedOffer(ctx context.Context, offerState structures.OfferStates, offerPrice int) error {
	incomeTimestamp := models.DriverIncomeTimestamp{
		DriverUUID:    offerState.DriverUUID,
		HourTimestamp: offerState.StartState.Truncate(time.Hour),
		// increment values, or initial values for new timestamp
		OrdersFinished: 1,
		TotalIncome:    offerPrice,
	}

	return models.UpdateDriverIncomeTimestamp(ctx, &incomeTimestamp)
}

func handleRejectedOffer(ctx context.Context, offerState structures.OfferStates, offerPrice int) error {
	incomeTimestamp := models.DriverIncomeTimestamp{
		DriverUUID:    offerState.DriverUUID,
		HourTimestamp: offerState.StartState.Truncate(time.Hour),
		// increment values, or initial values for new timestamp
		OffersRejected: 1,
		LostIncome:     offerPrice,
	}

	return models.UpdateDriverIncomeTimestamp(ctx, &incomeTimestamp)
}
