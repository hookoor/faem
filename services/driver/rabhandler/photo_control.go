package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	dbpkg "gitlab.com/faemproject/backend/faem/services/driver/db"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func InitPhotoControl(rb *rabbit.Rabbit) error {
	receiverChannelPhotoControl, err := rb.GetReceiver(rabbit.DriverUpdatePhotoControlQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}
	q, err := receiverChannelPhotoControl.QueueDeclare(
		rabbit.DriverUpdatePhotoControlQueue, // name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannelPhotoControl.QueueBind(
		q.Name,                       // queue name
		rabbit.PhotoControlUpdateKey, // routing key
		rabbit.DriverExchange,        // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	photoControl, err := receiverChannelPhotoControl.Consume(
		q.Name,                            // queue
		rabbit.DriverPhotoControlConsumer, // consumer
		true,                              // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return err
	}
	go handleNewPhotoControlData(photoControl)

	return nil
}

func handleNewPhotoControlData(msgs <-chan amqp.Delivery) {
	for d := range msgs {
		if d.Headers["publisher"] == "driver" {
			continue
		}
		var (
			phCon models.PhotoControl
		)
		log := logs.Eloger.WithFields(logrus.Fields{
			"event": "handling new photocontrol data",
		})
		err := json.Unmarshal([]byte(d.Body), &phCon.PhotoControl)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "Error unmarshalling data",
			}).Error(err)
			continue
		}
		controlType := dbpkg.GetControlTypeByName(phCon.ControlType)
		if controlType.Name == "" {
			log.WithFields(logrus.Fields{
				"reason":      "Error finding control by name",
				"driverUUID":  phCon.DriverID,
				"controlUUID": phCon.ControlID,
				"controlName": phCon.ControlType,
			}).Error()
			continue
		}
		err = phCon.UpdatePhotoControl(controlType)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":      "Error saving data to db",
				"driverUUID":  phCon.DriverID,
				"controlUUID": phCon.ControlID,
			}).Error(err)
			continue
		}

		if phCon.Approved {
			err = phCon.UnlockDriverIfNeed()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason":      "unlock driver error",
					"driverUUID":  phCon.DriverID,
					"controlUUID": phCon.ControlID,
				}).Error(err)
				continue
			}
		}
		err = phCon.NotifyDriver()
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":      "notify driver error",
				"driverUUID":  phCon.DriverID,
				"controlUUID": phCon.ControlID,
			}).Error(err)
			continue
		}
		log.WithFields(logrus.Fields{
			"driverUUID":  phCon.DriverID,
			"controlUUID": phCon.ControlID,
		}).Info("photocontrol data handled successfully")
	}
}
