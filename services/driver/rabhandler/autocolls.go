package rabhandler

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func initTelephonyEventsDRV() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverTelephonyQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverTelephonyQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments

	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                   // queue name
		rabbit.AnswerAsterStatus, // routing key
		rabbit.VoipExchange,      // exchange  events.voip
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                      // queue
		rabbit.DriverTelephonyQueue, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go readingTelephonyData(msgs)
	return nil
}

// readingTelephonyData godoc
func readingTelephonyData(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			go handleTelephonyData(d)
		}
	}
}

func handleTelephonyData(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "handle new autocall",
		"routing_key": d.RoutingKey,
	})

	ac := new(structures.TelephonyCall)
	if err := json.Unmarshal([]byte(d.Body), ac); err != nil {
		log.WithField("reason", bindingError).Error(err)
		return
	} else if ac.OrderUUID == "" {
		log.Error(errors.New("empty order uuid"))
		return
	}
	log = log.WithFields(logrus.Fields{
		"uid":        ac.Uid,
		"order_uuid": ac.OrderUUID,
		"status":     ac.Status,
	})

	if ac.Type != structures.OnTheWayAutoCall && ac.Type != structures.OnplaceAutoCall {
		return
	}

	orState, err := models.GetCurrentOrderState(ac.OrderUUID)
	if err != nil {
		log.WithField("reason", "error finding last order state").Error(err)
		return
	}
	log = log.WithField("driver_uuid", orState.DriverUUID)

	var msg, msgTitle string
	switch ac.Type {
	case structures.OnTheWayAutoCall:
		msgTitle = "Вас ждут!"
		msg = "Клиент уведомлен о назначении автомобиля"
	case structures.OnplaceAutoCall:
		msgTitle = "Скоро к вам выйдут"
		msg = "Клиент уведомлен о прибытии автомобиля"
	default:
		log.WithField("reason", fmt.Sprintf("order in %s status", orState.State)).Warning("No alert required")
		return
	}
	payload := msgTitle + ". " + msg

	token, err := models.GetCurrentDriverFCMToken(orState.DriverUUID)
	if err != nil {
		log.WithField("reason", "Error getting driver fcm token").Error(err)
		return
	}

	neededTag := constants.FcmTagClientNotified
	alert := models.AlertsDRV{
		DriverUUID: orState.DriverUUID,
		Tag:        neededTag,
		Message:    msg,
	}
	if err := alert.Save(); err != nil {
		log.WithField("reason", "Error saving alert").Error(err)
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(orState.DriverUUID, payload, token, neededTag, 300, msgTitle, msg)
}
