package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func InitClientLocations() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverClientLocationsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.DriverClientLocationsQueue, // name
		true,                              // durable
		false,                             // delete when unused
		false,                             // exclusive
		false,                             // no-wait
		nil,                               // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		queue.Name,               // queue name
		rabbit.ClientLocationKey, // routing key
		rabbit.ClientExchange,    // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	newDRST, err := receiverChannel.Consume(
		queue.Name,                           // queue
		rabbit.DriverClientLocationsConsumer, // consumer
		true,                                 // auto-ack
		false,                                // exclusive
		false,                                // no-local
		false,                                // no-wait
		nil,                                  // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewClientLoc(newDRST)
	return nil
}

func handleNewClientLoc(newDRST <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-newDRST:
			go newClientLoc(d)
		}
	}
}

func newClientLoc(d amqp.Delivery) {
	if d.Headers["publisher"] == "driver" {
		return
	}
	var (
		clLoc structures.ClientLocation
	)

	err := json.Unmarshal(d.Body, &clLoc)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling client locations",
		"orderUUID":  clLoc.OrderUUID,
		"routingKey": d.RoutingKey,
		"clientUUID": clLoc.ClienUUID,
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error unmarshalling data from rabbit",
		}).Error(err)
		return
	}
	var lastState models.OfferStatesDrv
	err = db.Model(&lastState).
		Where("order_uuid = ?", clLoc.OrderUUID).
		Order("start_state DESC").
		Limit(1).Select()
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error finding last order state",
		}).Error(err)
		return
	}

	if lastState.DriverUUID == "" {
		return
	}
	token, err := models.GetCurrentDriverFCMToken(lastState.DriverUUID)
	if err != nil {
		log.WithFields(logrus.Fields{
			"orderUUID":  clLoc.OrderUUID,
			"clientUUID": clLoc.ClienUUID,
		}).Error(err)
		return
	}
	go fcmsender.SendMessageToDriverWithCentrifugo(lastState.DriverUUID, clLoc, token, constants.FcmTagClientLoc, 60)
	log.WithFields(logrus.Fields{
		"reason":     "new client loc handled",
		"DriverUUID": lastState.DriverUUID,
	}).Info()
}
