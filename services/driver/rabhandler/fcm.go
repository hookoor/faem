package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	fcmpkg "gitlab.com/faemproject/backend/core/shared/fcm"
	"gitlab.com/faemproject/backend/core/shared/logs"
	"gitlab.com/faemproject/backend/core/shared/rabbit"
	"gitlab.com/faemproject/backend/core/shared/structures"
	"gitlab.com/faemproject/backend/core/shared/variables"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	bindingError                  = "error binding data"
	maxFCMTokenNumberInOneRequest = 990
)

// InitFCMMessageReceiver godoc
func InitFCMMessageReceiver() error {
	receiverChannel, err := rb.GetReceiver(rabbit.DriverChatMessageQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	qChat, err := receiverChannel.QueueDeclare(
		rabbit.DriverChatMessageQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		qChat.Name,                         // queue name
		rabbit.ChatMessagesMarkedAsReadKey, // routing key
		rabbit.FCMExchange,                 // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		qChat.Name,         // queue name
		"#",                // routing key
		rabbit.FCMExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	mess, err := receiverChannel.Consume(
		qChat.Name,                // queue
		rabbit.DriverChatConsumer, // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go readingNewChatMessageToDriver(mess)
	return nil
}

// readingNewChatMessageToDriver also handling new push mailings
func readingNewChatMessageToDriver(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.RoutingKey == rabbit.ChatMessageToDriverKey {
				go handleNewChatMessageToDriver(d)
			}
			if d.RoutingKey == rabbit.ChatMessagesMarkedAsReadKey {
				go handleChatMessageMarkReadToDriver(d)
			}
			if d.RoutingKey == rabbit.MailingKey {
				go handleNewPushMailing(d)
			}
		}
	}
}

func handleChatMessageMarkReadToDriver(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handle new chat message marked as read",
		"routingKey": d.RoutingKey,
	})

	messages := new(structures.ChatMessagesMarkedAsRead)
	if err := json.Unmarshal([]byte(d.Body), messages); err != nil {
		log.WithField("reason", bindingError).Error(err)
		return
	}
	log = log.WithField("order_uuid", messages.OrderUUID)

	if messages.SenderType != structures.DriverMember {
		return
	}

	lastState := new(models.OfferStatesDrv)
	query := db.Model(lastState).Where("order_uuid = ?", messages.OrderUUID).Order("start_state DESC").Limit(1)
	if err := query.Select(); err != nil {
		log.WithField("reason", "Error finding last order state").Error(err)
		return
	} else if lastState.DriverUUID == "" {
		return
	}
	log = log.WithField("driver_uuid", lastState.DriverUUID)

	token, err := models.GetCurrentDriverFCMToken(lastState.DriverUUID)
	if err != nil {
		log.WithField("reason", "error getting driver fcm token").Error(err)
		return
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(
		lastState.DriverUUID,
		messages,
		token,
		fcmpkg.ChatMessagesReadTag,
		300,
	)

	log.Debug("data about marked as read messages sended to driver")
}

func handleNewPushMailing(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "handle new push mailing",
		"routing_key": d.RoutingKey,
	})

	message := new(structures.PushMailing)
	if err := json.Unmarshal([]byte(d.Body), message); err != nil {
		log.WithField("reason", bindingError).Error(err)
		return
	}
	log = log.WithField("message_type", message.Type)

	if _, ok := structures.DriverMailing[message.Type]; !ok {
		return
	}

	uuids, err := []string{}, error(nil)
	switch message.Type {
	case structures.MailingAllDriversType:
		uuids, err = models.GetAllDriversUUID()

	case structures.MailingOnlineDriversType:
		uuids, err = models.GetDriversUUIDsByState(variables.DriversStates["Online"])

	case structures.MailingDriversGroupType, structures.MailingInformDriverType:
		uuids = message.TargetsUUIDs

	case structures.MailingOfflineDriversType:
		uuids, err = models.GetDriversUUIDsByState(variables.DriversStates["Offline"])

	case structures.MailingOwnerDriversType:
		return
	}
	if err != nil {
		log.WithField("reason", "error getting drivers UUID list").Error(err)
	}

	uuidFeatToken, err := models.GetDriversFCMTokens(uuids)
	if err != nil {
		log.WithField("reason", "error getting drivers FCM token").Error(err)
		return
	}

	messageForAlert := message.Title + ". " + message.Message
	payload := interface{}(messageForAlert)

	mailingTag := fcmpkg.MailingTag
	if message.Payload != nil && message.Payload.URL != "" {
		mailingTag = fcmpkg.MailingURL
		payload = message.Payload
	}

	for key, val := range uuidFeatToken {
		go fcmsender.SendMessageToDriverWithCentrifugo(
			key,
			payload,
			val,
			mailingTag,
			3000,
			message.Title,
			message.Message,
		)
	}

	alerts := make([]models.AlertsDRV, 0, len(uuids))
	for _, drvuuid := range uuids {
		alerts = append(alerts, models.AlertsDRV{
			DriverUUID: drvuuid,
			Tag:        mailingTag,
			Message:    messageForAlert,
		})
	}

	if err := models.SaveManyAlerts(alerts); err != nil {
		log.WithField("reason", "error saving alerts").Error(err)
	}

	log.Info("new push mailing handled")
}

func handleNewChatMessageToDriver(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handle new chat message",
		"routingKey": d.RoutingKey,
	})

	message := new(structures.ChatMessages)
	if err := json.Unmarshal([]byte(d.Body), &message); err != nil {
		log.WithField("reason", bindingError).Error(err)
		return
	}
	log = log.WithFields(logrus.Fields{
		"receiver":    message.Receiver,
		"driver_uuid": message.DriverUUID,
		"order_uuid":  message.OrderUUID,
	})

	if message.Receiver != structures.DriverMember {
		log.WithField("reason", "invalid receiver").Error()
		return
	}

	if message.DriverUUID == "" {
		log.WithField("reason", "empty driver uuid").Debug()
		return
	}

	token, err := models.GetCurrentDriverFCMToken(message.DriverUUID)
	if err != nil {
		log.WithField("reason", "error getting driver FCM token").Error(err)
		return
	}

	messTitle := "Новое сообщение"
	switch message.Sender {
	case structures.ClientMember:
		messTitle = messTitle + " от клиента"
	case structures.UserCRMMember:
		messTitle = messTitle + " от оператора"
	}

	go fcmsender.SendMessageToDriverWithCentrifugo(
		message.DriverUUID,
		message,
		token,
		fcmpkg.ChatMessageTag,
		300,
		messTitle,
		message.Message,
	)
}
