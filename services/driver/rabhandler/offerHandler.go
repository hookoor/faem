package rabhandler

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

const (
	tickIntervalOfferRejection         = 5 * time.Second
	tickIntervalOfferSmartDistribution = 3 * time.Second
)

// InitOffersRabbit создает все необходимое для получения координат
// Вынесен отдельно для дальнешейго более легкого переноса на микросервис
func InitOffersRabbit() error {

	receiverChannelNewOrder, err := rb.GetReceiver(rabbit.DriverNewOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Получение нового заказа
	// Объявляем очередь
	q, err := receiverChannelNewOrder.QueueDeclare(
		rabbit.DriverNewOrderQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}

	receiverChannelUpdateOrder, err := rb.GetReceiver(rabbit.DriverOrderUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	qOrUpdate, err := receiverChannelUpdateOrder.QueueDeclare(
		rabbit.DriverOrderUpdateQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}

	receiverChannelOrderState, err := rb.GetReceiver(rabbit.DriverOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// очередь для статусов заказа
	qOrdSt, err := receiverChannelOrderState.QueueDeclare(
		rabbit.DriverOrderStateQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return err
	}

	// Биндим очередь для получения заказов
	err = receiverChannelNewOrder.QueueBind(
		q.Name,               // queue name
		rabbit.NewKey,        // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Биндим очередь для получения обновлений заказов
	err = receiverChannelUpdateOrder.QueueBind(
		qOrUpdate.Name,        // queue name
		rabbit.UpdateKey+".#", // routing key
		rabbit.OrderExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Биндим очередь для получения статусов заказов
	err = receiverChannelOrderState.QueueBind(
		qOrdSt.Name,          // queue name
		rabbit.StateKey+".*", // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	receiverChannelOrderRedistribution, err := rb.GetReceiver(rabbit.DriverOrderReDistrQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	qOrderRedistr, err := receiverChannelOrderRedistribution.QueueDeclare(
		rabbit.DriverOrderReDistrQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannelOrderRedistribution.QueueBind(
		qOrderRedistr.Name,       // queue name
		rabbit.ReDistributionKey, // routing key
		rabbit.OrderExchange,     // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	reDistrMess, err := receiverChannelOrderRedistribution.Consume(
		qOrderRedistr.Name,                // queue
		rabbit.DriverOrderReDistrConsumer, // consumer
		true,                              // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера на получение заказа
	msgs, err := receiverChannelNewOrder.Consume(
		q.Name,                        // queue
		rabbit.DriverNewOrderConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера на получение статусов заказов
	orderStates, err := receiverChannelOrderState.Consume(
		qOrdSt.Name,                        // queue
		rabbit.DriverNewOrderStateConsumer, // consumer
		true,                               // auto-ack
		false,                              // exclusive
		false,                              // no-local
		false,                              // no-wait
		nil,                                // args
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера на получение обновлений заказов
	orUpdate, err := receiverChannelUpdateOrder.Consume(
		qOrUpdate.Name,                   // queue
		rabbit.DriverOrderUpdateConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewOrders(msgs)
	wg.Add(1)
	go handleNewOrderStates(orderStates)
	wg.Add(1)
	go handleOrderReDistribution(reDistrMess)
	wg.Add(1)
	go handleOrderUpdate(orUpdate)

	dur, err := time.ParseDuration(config.St.Drivers.CheckUndistributedOffersPeriod)
	if err != nil {
		return fmt.Errorf("error parse CheckUndistributedOffersPeriod,%s", err)
	}
	interval, err := time.ParseDuration(config.St.Drivers.ClientCallPeriod)
	if err != nil {
		return fmt.Errorf("error parse ClientCallPeriod,%s", err)
	}
	wg.Add(1)
	go offerCancelTicker(dur, interval)

	wg.Add(1)
	go offerRejectionTicker(tickIntervalOfferRejection)

	wg.Add(1)
	go distributeOffersSmartly(tickIntervalOfferSmartDistribution)

	return nil
}

// offerRejectionTicker  проверяет предложения, на которые не были получены ответы
func offerRejectionTicker(tickInterval time.Duration) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(tickInterval):
			orsData, err := models.GetOrdersDataByState(constants.OrderStateOffered)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offer rejection ticker",
					"reason": "Error finding offered offers",
				}).Error(err)
				continue
			}
			if len(orsData) == 0 {
				continue
			}
			var ordUUIDs []string
			for _, or := range orsData {
				ordUUIDs = append(ordUUIDs, or.UUID)
			}
			neededMap, err := models.GetOffersUUIDByOrdersUUIDMap(ordUUIDs)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offer rejection ticker",
					"reason": "Error GetOffersUUIDByOrdersUUIDMap",
				}).Error(err)
				continue
			}
			var offerStates []models.OfferStatesDrv
			for _, orData := range orsData {
				offerStates = append(offerStates, models.OfferStatesDrv{
					OfferStates: structures.OfferStates{
						OrderUUID:  orData.UUID,
						OfferUUID:  neededMap[orData.UUID],
						StartState: orData.StateTransTime,
						DriverUUID: orData.DriverUUID,
						State:      orData.State,
					},
				})
			}
			msg := "Мы не получили от вас ответа на предложенный заказ и перевели вас в оффлайн"
			body := "Возможно у вас были какие то проблемы с сетью"
			var neededDriversUUID []string

			for _, offState := range offerStates {
				validResponseTime := offState.StartState.Unix() + config.St.Drivers.ResponseTime + 10
				if validResponseTime < time.Now().Unix() {
					neededDriversUUID = append(neededDriversUUID, offState.DriverUUID)
					alert := models.AlertsDRV{
						DriverUUID: offState.DriverUUID,
						Tag:        constants.FcmTagInitData,
						Message:    msg + ". " + body,
						OrderUUID:  offState.OrderUUID,
					}
					err = alert.Save()
					if err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event":  "offer rejection ticker",
							"reason": "Error saving alert",
						}).Error(err)
					}
					offState.RedistributeAfterNoResponse()
				}
			}
			if len(neededDriversUUID) == 0 {
				continue
			}
			uuidFeatToken, err := models.GetDriversFCMTokens(neededDriversUUID)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offer rejection ticker",
					"reason": "Error getting tokens",
				}).Error(err)
			} else {
				for key, val := range uuidFeatToken {
					go fcmsender.SendMessageToDriverWithCentrifugo(key, msg, val, constants.FcmTagInitData, 36000, msg, body)
				}
			}
		}
	}
}

func handleOrderReDistribution(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "driver" {
				continue
			}
			var (
				reDisData structures.OrderReDistributionData
			)
			err := json.Unmarshal(d.Body, &reDisData)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling redistribution order",
					"reason": "Error unmarshalling data",
				}).Error(err)
				continue
			}
			offer, err := models.GetOfferByOrderUUID(reDisData.OrderUUID)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling redistribution order",
					"reason": "Error finding offer by order uuid",
				}).Error(err)
				continue
			}

			offer.Redistribute(reDisData.DistData, "Перераспределение (запрос оператора)")
		}
	}
}

func handleOrderUpdate(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling update order data",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			forDriverTooParam, _ := d.Headers["fordrivertoo"].(bool)
			if d.Headers["publisher"] == "driver" && !forDriverTooParam {
				continue
			}

			orderWrap := new(structures.WrapperOrder)
			if err := json.Unmarshal(d.Body, orderWrap); err != nil {
				log.WithField("reason", "Error unmarshalling order").Error(err)
				continue
			}
			log = log.WithField("order_uuid", orderWrap.Order.UUID)

			orderDriver := models.OrderDrv{Order: orderWrap.Order}
			updateOffer, err := orderDriver.UpdateFromBroker(d.RoutingKey)
			if err != nil {
				log.WithField("reason", "Error updating order").Error(err)
				continue
			}
			log.Info("Order successfully updated")

			var msg string
			switch d.RoutingKey {
			case rabbit.UpdateOrderTariffKey:
				msg = "Тариф заказа изменен"
			case rabbit.UpdateOrderRoutesKey:
				msg = "Маршрут заказа изменен"
			case rabbit.UpdateOrderProductData:
				msg = "Номер заказа в заведении - " + orderDriver.GetProductsData().OrderNumberInStore
			case rabbit.UpdateIncreasedFareKey:
				if orderWrap.FareDifferences > 0 {
					updateOffer.Redistribute(updateOffer.TaximetrData.Order.CancelTime, "Увеличение надбавочной стоимости заказ")
				}
				msg = "Надбавочная стоимость изменена"
			default:
				log.Errorf("wrong rabbit routing key [%s] in handleOrderUpdate", d.RoutingKey)
				continue
			}

			go updateOffer.InformDriverAboutOrderUpdate(msg)
		}
	}
}

func distributeOffersSmartly(tickInterval time.Duration) {
	distributeOffersByStates(
		tickInterval,
	)
}

func distributeOffersByStates(tickInterval time.Duration, states ...string) {
	defer wg.Done()

	//logEvent := fmt.Sprintf("distribute offers ticker, state: %s", states)
	log := logs.Eloger.WithField("event", "distribution")

	for {
		select {
		case <-closed:
			return
		case <-time.After(tickInterval):
			err := distribution.Start()
			if err != nil {
				log.Error(err)
			}
		}
	}
}

// offerCancelTicker  блокирует просроченные заказы
func offerCancelTicker(tickInterval time.Duration, autocallInterval time.Duration) {
	defer wg.Done()

	var (
		offerState models.OfferStatesDrv
		freeOrders []models.OrderDrv
	)

	for {
		select {
		case <-closed:
			return
		case <-time.After(tickInterval):
			freeOrdersData, err := models.GetAllOrdersUUIDByStates(constants.OrderStateFree)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offerCancelTicker",
					"reason": "Error Get All Orders UUID By State",
				}).Error(err)
				continue
			}
			if len(freeOrdersData) == 0 {
				continue
			}

			freeOrdersUUIDs := make([]string, 0, len(freeOrdersData))
			for uuid := range freeOrdersData {
				freeOrdersUUIDs = append(freeOrdersUUIDs, uuid)
			}
			err = db.Model(&freeOrders).
				Where("uuid in (?)", pg.In(freeOrdersUUIDs)).
				Select()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offerCancelTicker",
					"reason": "Error finding free orders",
				}).Error(err)
				continue
			}
			offUUIDMap, err := models.GetOffersUUIDByOrdersUUIDMap(freeOrdersUUIDs)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "offerCancelTicker",
					"reason": "Error GetOffersUUIDByOrdersUUIDMap",
				}).Error(err)
				continue
			}
			var ordersForAutocall []models.OrderDrv
			for _, or := range freeOrders {
				if time.Now().Unix() > or.CancelTime.Unix() {
					//это нужно из за бага стейтмашины, которая не хочет применять ивенты два раза к одному и тому же объекту
					offerState = models.OfferStatesDrv{}
					offerState.InitFsm()
					offerState.OfferUUID = offUUIDMap[or.UUID]
					offerState.FSM.SetState(constants.OrderStateFree)
					offerState.OrderUUID = or.UUID
					err := offerState.ApplyEventWithMutex(constants.OrderStateDriverNotFound)
					if err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event":     "Cheking overdue offers",
							"offerUUID": offerState.OfferUUID,
							"value":     "DriverNotFound",
						}).Info("Blocked due to delay")
						continue
					}
					logs.Eloger.WithFields(logrus.Fields{
						"event":     "Cheking overdue offers",
						"offerUUID": offerState.OfferUUID,
						"value":     "DriverNotFound",
					}).Info("Blocked due to delay")
					continue
				} else {
					ordersForAutocall = append(ordersForAutocall, or)
				}
			}
			if len(ordersForAutocall) != 0 {
				err = sendAutocallIfNeed(ordersForAutocall, autocallInterval)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "Cheking overdue offers",
						"reason": "Error send autocall if need",
					}).Error(err)
					continue
				}
			}
		}
	}
}

func sendAutocallIfNeed(orders []models.OrderDrv, interval time.Duration) error {
	var neededOrders []models.OrderDrv
	var neededOrdersUUID []string
	for _, or := range orders {
		if or.NextAutocallTime.Unix() > time.Now().Unix() || or.Source != constants.OrderSourceCRM {
			continue
		}
		neededOrders = append(neededOrders, or)
		neededOrdersUUID = append(neededOrdersUUID, or.UUID)
	}
	if len(neededOrders) == 0 {
		return nil
	}

	err := models.UpdateOrdersNextCallTime(neededOrdersUUID, interval)
	if err != nil {
		return err
	}
	for _, or := range neededOrders {
		neededClientPhone := or.Client.MainPhone
		if or.CallbackPhone != "" {
			neededClientPhone = or.CallbackPhone
		}
		newAutoCall := structures.AutoOutCallRequest{
			Text:      models.MessageForClientWhenAutocall,
			OrderUUID: or.UUID,
			Phone:     neededClientPhone,
			Type:      structures.LongDistibNotificAutoCall,
			CreatedAt: time.Now(),
		}
		err = rabsender.SendJSONByAction(rabsender.AcNewAutocall, "", newAutoCall)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "Update Orders Next Call Time",
				"reason":    "Error send data to broker",
				"OrderUUID": or.UUID,
			}).Error(err)
			continue
		}
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "Update Orders Next Call Time",
			"reason":    "data to broker sended",
			"OrderUUID": or.UUID,
		}).Info(err)
		//  если коммент уже добавлен
		if or.MessageLongDistributionAdded(interval) {
			continue
		}
		newComm := or.GetComment() + models.MessageForDriverWhenAutocall
		err = rabsender.SendJSONByAction(rabsender.AcOrderDataOnlyCommentUpdate, "", structures.Order{
			UUID:    or.UUID,
			Comment: &newComm,
		}, true)
	}
	return err
}

func handleNewOrderStates(orderStates <-chan amqp.Delivery) {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order states",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-orderStates:
			if d.Headers["publisher"] == "driver" {
				continue
			}

			var (
				orderState, lastState models.OfferStatesDrv
				offer                 models.OfferDrv
				msg                   string
			)
			orderState.External = true

			if err := json.Unmarshal(d.Body, &orderState); err != nil {
				log.WithField("reason", "Error unmarshalling order state").Error(err)
				continue
			}

			query := db.Model(&lastState).Where("order_uuid = ?", orderState.OrderUUID).Order("start_state DESC").Limit(1)
			if err := query.Select(); err != nil && err != pg.ErrNoRows {
				log.WithField("reason", "Error finding last order state").Error(err)
				continue
			} else if err == pg.ErrNoRows {
				log.WithField("reason", "no state for this order").Warning(err)
				continue
			}
			log = log.WithField("order_uuid", lastState.OrderUUID)

			orderState.InitFsm()
			orderState.OfferUUID = lastState.OfferUUID
			orderState.FSM.SetState(lastState.State)
			log = log.WithField("state", orderState.State)
			log.Info("New offer state saved")

			query = db.Model(&offer).Where("order_uuid = ?", orderState.OrderUUID)
			if err := query.Select(); err != nil {
				log.WithField("reason", "Error find offer").Error(err)
				continue
			}

			if orderState.Flags.Has(structures.OfferStateFlagCounterOrder) {
				msg = "Встречный заказ был отменен"
			} else {
				msg = "Заказ был отменен"
			}
			//это чтобы водила видел причину отмены заказа
			for i, val := range constants.CancelReasonRU {
				if i == orderState.Comment {
					msg += ". Указанная причина отмены: " + val
					break
				}
			}
			orderState.DriverUUID = lastState.DriverUUID

			if err := orderState.ApplyEventWithMutex(orderState.State); err != nil {
				log.WithField("reason", "Error setting new state").Error(err)
				continue
			}

			if lastState.DriverUUID == "" ||
				constants.InDistributingStates(lastState.State) ||
				lastState.State == constants.OrderStateRejected {
				continue
			}

			token, err := models.GetCurrentDriverFCMToken(lastState.DriverUUID)
			if err != nil {
				log.WithField("orderUUID", orderState.OrderUUID).Error(err)
				continue
			}

			alert := models.AlertsDRV{
				DriverUUID: lastState.DriverUUID,
				Tag:        constants.FcmTagInitData,
				Message:    msg,
				OrderUUID:  orderState.OrderUUID,
			}
			if err := alert.Save(); err != nil {
				log.WithField("reason", "error saving alert").Error(err)
			}

			go fcmsender.SendMessageToDriverWithCentrifugo(orderState.DriverUUID, msg, token, constants.FcmTagInitData, 3600, "Заказ был отменен!", msg)
		}
	}
}

// handleNewOrders обрабатывает вновь поступивший заказ
// создается и сохраняется новый оффер и отправляется информация о старте отработки заказа
func handleNewOrders(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] != "crm" {
				continue
			}
			var (
				order structures.Order
			)
			err := json.Unmarshal(d.Body, &order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new order",
					"reason": "Error unmarshalling order",
				}).Error(err)
				continue
			}
			// Сохраняем ордер в местную базу
			err = models.SaveOrder(order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling new order",
					"reason":    "Error saving order to DB",
					"orderUUID": order.UUID,
				}).Error(err)
				continue
			}

			// Генерируем тело оффера но пока без водителя
			offer, err := models.GenerateOffer(order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling new order",
					"reason":    "Error generating offer",
					"orderUUID": order.UUID,
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling new order",
				"offerUUID": offer.UUID,
				"orderUUID": order.UUID,
			}).Info("New offer generated")

			// Сохраняем тело оффера
			OfferDrv, err := models.CreateOffer(offer)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling new order",
					"reason":    "Error saving offer to DB",
					"offerUUID": offer.UUID,
					"orderUUID": order.UUID,
				}).Error(err)
				continue
			}

			err = models.PrepareOfferForDistribution(OfferDrv, getSuitableDistState(order), "")
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling new order",
					"reason":    "Preparing an order for distribution",
					"offerUUID": offer.UUID,
					"orderUUID": order.UUID,
				}).Error(err)
			}
		}
	}
}

func getSuitableDistState(order structures.Order) string {
	if order.IncreasedFare < 0 {
		return constants.OrderStateFree
	}
	return constants.OrderStateDistributing
}
