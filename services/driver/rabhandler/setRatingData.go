package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/driver/models"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabOrderSetRating -
func initRabOrderSetRating() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmOrderUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverSetOrderRatingQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		rabbit.SetRatingKey,  // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannel.Consume(
		q.Name,                              // queue
		rabbit.DriverSetOrderRatingConsumer, // consumer
		true,                                // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderSetRating(msgs)
	return nil
}

func handleOrderSetRating(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling update order rating"

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			var order models.OrderDrv
			err := json.Unmarshal(d.Body, &order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     event,
					"reason":    "Error unmarshalling order",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}

			err = models.UpdateOrderRating(&order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     event,
					"reason":    "Error update order rating",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     event,
				"OrderUUID": order.UUID,
			}).Info("Order rating successfully update")

			// We have successfully updated order rating, so we can recalculate driver's karma also
			driverUUID := order.Driver.UUID
			if driverUUID == "" { // try to load driver uuid from the offer
				offer, err := models.GetOfferByOrderUUID(order.UUID)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":     event,
						"orderUUID": order.UUID,
					}).Error(err)
					continue
				}
				driverUUID = offer.DriverUUID
			}
			_, err = models.UpdateDriverKarma(driverUUID)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      event,
					"orderUUID":  order.UUID,
					"driverUUID": driverUUID,
				}).Error(err)
			}
		}
	}
}
