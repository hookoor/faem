package tickers

import (
	"context"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

const (
	distributionLogsFlushInterval = 15 * time.Second
)

func initDistributionLogs() {
	wg.Add(1)
	go flushDistributionLogs()
}

func flushDistributionLogs() {
	defer wg.Done()

	var buffer []models.DistributionLog
	timer := time.NewTicker(distributionLogsFlushInterval)
	for {
		select {
		case log := <-models.DistributionLogs:
			buffer = append(buffer, log)
		case <-timer.C:
			logger := logs.Eloger.WithField("event", "flushing distribution logs")
			if len(buffer) == 0 {
				logger.Info("no logs to insert")
				continue
			}
			if err := models.InsertDistributionLogs(context.Background(), buffer); err != nil {
				logger.WithError(err).Error("insert failed")
				continue
			}
			logger.Infof("inserted %d log entries", len(buffer))
			buffer = make([]models.DistributionLog, 0)
		case <-closed:
			timer.Stop()
			close(models.DistributionLogs)
			return
		}
	}
}
