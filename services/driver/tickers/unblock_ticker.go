package tickers

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	updateBlockingConfigInterval = time.Second * 45
	checkUnblockInterval         = time.Minute
)

func InitUnblockDriverTicker() {
	wg.Add(1)
	go updateBlockingConfig()

	wg.Add(1)
	go checkDriverBlockExpiration()
}

func updateBlockingConfig() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(updateBlockingConfigInterval):
			if err := models.UpdateBlockingConfig(context.Background()); err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "update blocking config ticker",
					"reason": "can't update blocking config",
				}).Error(err)
			}
		}
	}
}

func checkDriverBlockExpiration() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkUnblockInterval):
			if err := models.UnblockExpiredDrivers(context.Background()); err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "check drivers block expiration",
					"reason": "can't unblock expired drivers",
				}).Error(err)
			}
		}
	}
}
