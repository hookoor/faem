package tickers

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/db"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func InitPhotoControlDriversBlock() {
	wg.Add(1)
	go driversBlock()
}

func driversBlock() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(15 * time.Second):
			var (
				driversForUpdate, allDrivers []models.DriversApps
			)

			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "checking expired photocontrols",
			})
			err := dbConn.Model(&allDrivers).
				Column("photocontrol_data", "uuid").
				Select()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error getting photocontrol data",
				}).Error(err)
			}

			for _, driver := range allDrivers {
				for i, phdata := range driver.PhotocontrolData {
					timeNow := time.Now().Unix()
					if phdata.NextControlTime.Unix() < timeNow {
						if phdata.Status == structures.ExpiredPhotoControlState ||
							phdata.Status == structures.ModerationPhotoControlState {
							continue
						}
						driver.PhotocontrolData[i] = structures.PhotoControlData{
							ControlType:      phdata.ControlType,
							Status:           structures.ExpiredPhotoControlState,
							ControlTypeTitle: phdata.ControlTypeTitle,
							NextControlTime:  phdata.NextControlTime,
						}
						check, driverIndex := checkDriverAlreadyAppended(driversForUpdate, driver)
						if !check {
							driversForUpdate = append(driversForUpdate, driver)
							continue
						}
						driversForUpdate[driverIndex].PhotocontrolData[i] = driver.PhotocontrolData[i]
					}
					if isWarnTime(phdata, phdata.NextControlTime.Unix()) {
						if phdata.Status == structures.WarningPhotoControlState ||
							phdata.Status == structures.ModerationPhotoControlState {
							continue
						}
						driver.PhotocontrolData[i] = structures.PhotoControlData{
							ControlType:      phdata.ControlType,
							Status:           structures.WarningPhotoControlState,
							ControlTypeTitle: phdata.ControlTypeTitle,
							NextControlTime:  phdata.NextControlTime,
						}
						check, driverIndex := checkDriverAlreadyAppended(driversForUpdate, driver)
						if !check {
							driversForUpdate = append(driversForUpdate, driver)
							continue
						}
						driversForUpdate[driverIndex].PhotocontrolData[i] = driver.PhotocontrolData[i]
					}
				}
			}

			for _, typ := range models.AvailableTypes {
				for _, driver := range allDrivers {
					// если там есть регистрационный контроль, значит он еще не прошел регистрацию
					if haveRegControl(driver) {
						continue
					}
					//если данных о каком то фотоконтроле у водилы не было
					if !containsType(typ.Name, driver.PhotocontrolData) {

						driver.PhotocontrolData = append(driver.PhotocontrolData, structures.PhotoControlData{
							ControlType:      typ.Name,
							Status:           structures.WarningPhotoControlState,
							ControlTypeTitle: typ.Title,
							NextControlTime:  time.Now().Add(typ.PassLifetime),
						})
						driversForUpdate = appendOrChange(driversForUpdate, driver)
					}
				}
			}

			assimilateDrivers(allDrivers, driversForUpdate)

			driversForUpdate, err = notifyDriversIfNeed(allDrivers, driversForUpdate)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "driver notify error",
				}).Error(err)
				continue
			}

			if len(driversForUpdate) == 0 {
				log.WithFields(logrus.Fields{
					"reason": "no drivers for update",
				}).Debug()
				continue
			}

			updatedDrivers, err := models.UpdateDriversPhotocontrolData(driversForUpdate)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "update drivers photocontrol data error",
				}).Error(err)
				continue
			}
			log.WithFields(logrus.Fields{
				"reason":         "end of function",
				"drivers number": len(updatedDrivers),
			}).Info("drivers photocontrols data updatet successfully")
		}
	}
}

func haveRegControl(driver models.DriversApps) bool {
	for _, controlData := range driver.PhotocontrolData {
		if controlData.ControlType == models.RegControlName {
			return true
		}
	}
	return false
}

// assimilateDrivers записывает в массив со всеми водилами новые данные водил для обновления
func assimilateDrivers(allDrivers []models.DriversApps, driversForUpdate []models.DriversApps) {
	for i := range allDrivers {
		for j := range driversForUpdate {
			if allDrivers[i].UUID == driversForUpdate[j].UUID {
				allDrivers[i].PhotocontrolData = driversForUpdate[j].PhotocontrolData
				break
			}
		}
	}
}

// notifyDriversIfNeed возвращает модифицированный массив  driversForUpdate
func notifyDriversIfNeed(allDrivers []models.DriversApps, driversForUpdate []models.DriversApps) ([]models.DriversApps, error) {
	// insertInDrvFUp добавляет новые данные driversForUpdate
	insertInDrvFUp := func(drvs []models.DriversApps) {
		if len(drvs) == 0 {
			return
		}
		if len(driversForUpdate) == 0 {
			driversForUpdate = append(driversForUpdate, drvs...)
		}
		for _, drv := range drvs {
			for in := range driversForUpdate {
				if driversForUpdate[in].UUID == drv.UUID {
					driversForUpdate[in].PhotocontrolData = drv.PhotocontrolData
					return
				}
			}
			driversForUpdate = append(driversForUpdate, drv)
		}

	}
	var (
		driversForWarn, driversForBlock []models.DriversApps
	)
	warData := make(map[string]string)
	expData := make(map[string]string)
	//  смотрим есть ли что - то, о чем мы не сообщили водителям.Если есть, но добавляем данные в соответствующий массив
	for _, drv := range allDrivers {
		for phDataIndex, phdata := range drv.PhotocontrolData {
			if !phdata.Notified {

				switch phdata.Status {
				case structures.WarningPhotoControlState:
					warData[drv.UUID] = phdata.ControlTypeTitle
					drv.PhotocontrolData[phDataIndex].Notified = true
					driversForWarn = append(driversForWarn, drv)
				case structures.ExpiredPhotoControlState:
					expData[drv.UUID] = phdata.ControlTypeTitle
					drv.PhotocontrolData[phDataIndex].Notified = true
					driversForBlock = append(driversForBlock, drv)
				}

			}
		}
	}
	err := models.BlockDriversOutOfPhotoControl(expData)
	if err != nil {
		return driversForUpdate, fmt.Errorf("block drivers error,%s", err)
	}
	insertInDrvFUp(driversForBlock)
	err = models.WarnDrivers(warData)
	if err != nil {
		return driversForUpdate, fmt.Errorf("warn drivers error,%s", err)
	}
	insertInDrvFUp(driversForWarn)
	return driversForUpdate, nil
}

// checkDriverAlreadyAppended also returns the index in the array if it contains the driver
func checkDriverAlreadyAppended(drivers []models.DriversApps, driver models.DriversApps) (bool, int) {
	for i, drv := range drivers {
		if driver.UUID == drv.UUID {
			return true, i
		}
	}
	return false, -1
}

func appendOrChange(drivers []models.DriversApps, driver models.DriversApps) []models.DriversApps {
	for i, drv := range drivers {
		if driver.UUID == drv.UUID {
			drivers[i].PhotocontrolData = append(drivers[i].PhotocontrolData, driver.PhotocontrolData...)
			return drivers
		}
	}
	return append(drivers, driver)
}

func containsType(typeName string, phData []structures.PhotoControlData) bool {
	for _, val := range phData {
		if val.ControlType == typeName {
			return true
		}
	}

	return false
}

// getControlActualStatus also returns next required control time
func getControlActualStatus(control models.PhotoControl, allTypes []models.ControlType) (structures.PhotoControlStatusesType, time.Time) {
	reqType := db.GetControlTypeByName(control.ControlType)

	reqControlTime := control.CreatedAt.Add(reqType.PassTimeout)
	if reqControlTime.Unix() < time.Now().Unix() {
		if !control.Approved {
			return structures.ModerationPhotoControlState, reqControlTime
		}
		return structures.ExpiredPhotoControlState, reqControlTime
	}
	if control.CreatedAt.Add(reqType.PassLifetime).Unix() < time.Now().Unix() {
		return structures.WarningPhotoControlState, reqControlTime
	}
	return structures.OKPhotoControlState, reqControlTime
}

func getControlsByDriverUUID(controls []models.PhotoControl, uuid string) []models.PhotoControl {
	var result []models.PhotoControl
	for _, control := range controls {
		if control.DriverID == uuid {
			result = append(result, control)
		}
	}
	return result
}

func getPhotoControlDataIndexByType(driver models.DriversApps, phType string) int {
	for i := range driver.PhotocontrolData {
		if driver.PhotocontrolData[i].ControlType == phType {
			return i
		}
	}
	return 0
}

func warnTime(phdata structures.PhotoControlData) int64 {
	neededType := db.GetControlTypeByName(phdata.ControlType)
	return time.Now().Unix() - time.Now().Add(-neededType.PassLifetime).Unix()
}
func isWarnTime(phdata structures.PhotoControlData, expiredTime int64) bool {
	return (time.Now().Unix() > warnTime(phdata) && time.Now().Unix() < expiredTime)
}
