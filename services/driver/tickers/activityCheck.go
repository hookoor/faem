package tickers

import (
	"context"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

const (
	checkDriversActivityInterval         = 1 * time.Second
	checkDriversActivity_ScheduledHour   = 3
	checkDriversActivity_ScheduledMinute = 0
	checkDriversActivity_ScheduledSecond = 0
)

func InitActivityCheckTicker() {
	wg.Add(1)
	go checkDriversActivity()
}

// TODO: сделать планируемой задачей, а не по таймеру
func checkDriversActivity() {
	defer wg.Done()

	log := logs.Eloger.WithField("event", "inactive drivers penalty")
	loc, err := time.LoadLocation(config.St.Application.TimeZone)
	if err != nil {
		loc = time.Local
		log.WithError(err).Warn("unable to load location from config, using system timezone instead")
	}

	for {
		select {
		case <-time.After(checkDriversActivityInterval):
			now := time.Now().Truncate(time.Second)
			scheduledTo := time.Date(now.Year(), now.Month(), now.Day(),
				checkDriversActivity_ScheduledHour,
				checkDriversActivity_ScheduledMinute,
				checkDriversActivity_ScheduledSecond, 0, loc)
			if !now.Equal(scheduledTo) {
				continue
			}

			count, err := models.PenalizeInactiveDrivers(context.Background())
			if err != nil {
				log.Error(err)
				continue
			}
			log.WithField("inactive_drivers_count", count).Info("Penalized inactive drivers")
		case <-closed:
			return
		}
	}
}
