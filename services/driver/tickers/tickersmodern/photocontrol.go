package tickersmodern

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg/orm"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

const checkPhotocontrolPeriod = 2

// InitCheckPhotocontrolExparedTime - проверка тикетов фотоконтроля на истечение срока действия.
// meta: просроченым тикетам меняем статус на "expired"
func (ticker *Ticker) InitCheckPhotocontrolExparedTime() {
	wg.Add(1)
	go ticker.checkPhotocontrolExparedTimeTicker(context.Background())
}

func (ticker *Ticker) checkPhotocontrolExparedTimeTicker(ctx context.Context) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(time.Second * checkPhotocontrolPeriod):

			log := logs.Eloger.WithField("event", "check photocontrol expared time")

			var pcs []models.Photocontrol
			err := ticker.dbConn.Model(&pcs).
				Where("deleted is not true").
				WhereGroup(func(q *orm.Query) (*orm.Query, error) {
					q.WhereOr("status = ?", models.PhotocontrolModeration)
					q.WhereOr("status = ?", models.PhotocontrolPassed)
					return q, nil
				}).
				Select()
			if err != nil {
				log.WithField("reason", "error getting photocontrol tickets").Error(errpath.Err(err).Error())
				continue
			}

			for _, pc := range pcs {

				log.WithFields(logrus.Fields{
					"photocontrol": pc.UUID,
					"driver_uuid":  pc.Driver.UUID,
				})

				if pc.TimeValidity <= time.Now().Unix() && !pc.Stages.Expired {
					_, err = ticker.dbConn.ModelContext(ctx, (*models.Photocontrol)(nil)).
						Where("uuid = ?", pc.UUID).
						Set(fmt.Sprintf("stages = jsonb_set(COALESCE(stages, '{}'), '{expired}', '%t')", true)).
						Update()
					if err != nil {
						log.Error(errpath.Err(err).Error())
						continue
					}

					var pcup models.Photocontrol

					// запрос в базу на установление статуса expared
					_, err = ticker.dbConn.Model((*models.Photocontrol)(nil)).
						Where("uuid = ?", pc.UUID).
						Set("status = ?", models.PhotocontrolExpired).
						Returning("*").
						Update(&pcup)
					if err != nil {
						log.WithField("reason", "error setting status 'expared'").Error(errpath.Err(err).Error())
						continue
					}

					// отправка в кролик
					err = rabsender.SendJSONByAction(rabsender.AcNewExpiredPhotocontrol, rabbit.ExpiredKey, pcup)
					if err != nil {
						log.WithField("reason", "error setting status 'expared'").Error(errpath.Err(err).Error())
						continue
					}

					{ // блокировка водителя (отправка статуса блокировки для водителя)
						if pc.Settings.Expired.Enable && pc.Settings.Expired.Blocking.Enable {

							ds := structures.DriverStates{
								DriverUUID: pc.Driver.UUID,
								State:      constants.DriverStateBlocked,
								Comment:    fmt.Sprint("фотоконтроль по '", pc.Title, "' просрочен"),
								CreatedAt:  time.Now(),
							}

							err = rabsender.SendJSONByAction(rabsender.AcBlockDriverState, constants.DriverStateBlocked, ds)
							if err != nil {
								log.WithField("reason", "error setting status 'expared'").Error(errpath.Err(err).Error())
								continue
							}
						}
					}

					{ // логика автосоздания нового фотоконтроля при просрочке
						if pc.Settings.Offspring.Enable {
							if pc.Settings.Offspring.WhenExpired {

								err := ticker.createNewPhotocontrol(ctx, &pc)
								if err != nil {
									log.WithField("reason", "createNewPhotocontrol").Error(errpath.Err(err).Error())
									continue
								}

							}
						}
					}

					log.Info("photocontrol is expared")

					continue
				}

				if pc.WarningTime <= time.Now().Unix() && !pc.Stages.Warning {
					_, err = ticker.dbConn.ModelContext(ctx, (*models.Photocontrol)(nil)).
						Where("uuid = ?", pc.UUID).
						Set(fmt.Sprintf("stages = jsonb_set(COALESCE(stages, '{}'), '{warning}', '%t')", true)).
						Update()
					if err != nil {
						log.Error(errpath.Err(err).Error())
						continue
					}

					{ // предупреждение водилы
						if pc.Settings.Expired.Enable && pc.Settings.Expired.Warning.Enable {

							// ... (TODO: добавить пуш уведомление)

							_, err = ticker.dbConn.ModelContext(ctx, (*models.Photocontrol)(nil)).
								Where("uuid = ?", pc.UUID).
								Set(fmt.Sprintf("settings = jsonb_set(COALESCE(settings, '{}'), '{expired, warning, enable}', '%t')", false)).
								Update()
							if err != nil {
								log.WithField("reason", "").Error(errpath.Err(err).Error())
								continue
							}

							log.Info("photocontrol warning")
						}
					}

				}

				if pc.Status == models.PhotocontrolPassed && !pc.Stages.Passed {
					_, err = ticker.dbConn.ModelContext(ctx, (*models.Photocontrol)(nil)).
						Where("uuid = ?", pc.UUID).
						Set(fmt.Sprintf("stages = jsonb_set(COALESCE(stages, '{}'), '{passed}', '%t')", true)).
						Update()
					if err != nil {
						log.Error(errpath.Err(err).Error())
						continue
					}

					{ // логика автосоздания нового фотоконтроля при прохождении старого
						if pc.Settings.Offspring.Enable {
							err := ticker.createNewPhotocontrol(ctx, &pc)
							if err != nil {
								log.WithField("reason", "createNewPhotocontrol").Error(errpath.Err(err).Error())
								continue
							}
						}
					}

					if pc.Settings.ActivityAccrual.Enable {
						_, err := models.AddActivityForDriver(models.DriverActivityHistoryItem{
							ID:             structures.GenerateUUID(),
							DriverUUID:     pc.Driver.UUID,
							Event:          "photocontrol activity",
							ActivityChange: pc.Settings.ActivityAccrual.Number,
						})
						if err != nil {
							log.Error(errpath.Err(err).Error())
							continue
						}
					}

					if pc.Settings.AddToScoring.Enable {
						drv, err := models.GetDriverByUUID(pc.Driver.UUID)
						if err != nil {
							log.Error(errpath.Err(err).Error())
							continue
						}

						err = drv.SetScoringBoostTimeByPhotocontrolPassed(drv.UUID, pc.Settings.AddToScoring.ScoringBoostActivityTime)
						if err != nil {
							log.Error(errpath.Err(err).Error())
							continue
						}
					}

					log.Info("Photocontrol passed")
				}

			}

		}
	}
}

// createNewPhotocontrol - создает новый фотоконтроль и устанавливает метку создания фотоконтроля в false
func (ticker *Ticker) createNewPhotocontrol(ctx context.Context, pc *models.Photocontrol) error {
	var err error
	var newPhotocontrol models.Photocontrol

	{ // перебивка старых значений
		newPhotocontrol = *pc // заполнение старыми значениями

		// заполнение изначальных(не заполненных предыдущим фотоконтролем) снимоков-шаблонов
		var newSnapshotSketches []models.SnapshotSketch
		for _, snapshot := range newPhotocontrol.SnapshotSketches {
			snapshotSketch, err := ticker.DB.ModelPhotocontrol.GetPhotocontrolSnapshotSketchesByUUID(context.Background(), snapshot.UUID)
			if err != nil {
				return errpath.Err(err)
			}
			newSnapshotSketches = append(newSnapshotSketches, snapshotSketch)
		}
		newPhotocontrol.SnapshotSketches = newSnapshotSketches

		timeNow := time.Now()
		newPhotocontrol.UUID = structures.GenerateUUID()
		newPhotocontrol.Status = models.PhotocontrolCreated
		newPhotocontrol.CreatedAtUnix = timeNow.Unix()
		newPhotocontrol.CreatedAt = timeNow
		newPhotocontrol.UpdatedAt = timeNow
		newPhotocontrol.ActivateTime = pc.ActivateTime + int64(pc.Settings.Offspring.Periodicity)
		newPhotocontrol.TimeValidity = pc.TimeValidity + int64(pc.Settings.Offspring.Periodicity)
		newPhotocontrol.WarningTime = pc.WarningTime + int64(pc.Settings.Offspring.Periodicity)
		newPhotocontrol.Postponement = []models.Postponement{{Count: 0, Time: newPhotocontrol.TimeValidity}}
		newPhotocontrol.Settings = pc.OriginSettings
	}

	{ // отключаем функцию(метку) пораждения нового фотоконтроля
		pc.Settings.Offspring.Enable = false
		_, err = ticker.dbConn.ModelContext(ctx, (*models.Photocontrol)(nil)).
			Where("uuid = ?", pc.UUID).
			Set(fmt.Sprintf("settings = jsonb_set(COALESCE(settings, '{}'), '{offspring, enable}', '%t')", false)).
			Update()
		if err != nil {
			return errpath.Err(err)
		}
	}

	_, err = ticker.dbConn.ModelContext(ctx, &newPhotocontrol).Insert()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}
