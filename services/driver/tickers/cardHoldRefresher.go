package tickers

import (
	"time"

	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	cardHoldRefreshInterval = 5 * time.Second
	cardHoldLifetime        = 1 * time.Minute
)

// initCardHoldRefresher - запускает чистильщик холдов (замороженных средств) безналичного кошелька водителей
func initCardHoldRefresher() {
	wg.Add(1)
	go cardHoldRefresh()
}

func cardHoldRefresh() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(cardHoldRefreshInterval):
			for _, holder := range models.CurrentDriverCardBalanceHolders {
				holder.Refresh(cardHoldLifetime)
			}
		}
	}
}
