package tickers

import (
	"context"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

const (
	activityConfigUpdateInterval = 15 * time.Second
)

//InitFreeOrdersUpdateTicker запускает функции для обновления актуальных свободных заказов
func InitActivityConfigUpdateTicker() {
	wg.Add(1)
	go updateActivityConfig()
}

func updateActivityConfig() {
	defer wg.Done()

	interval := time.Duration(0)
	for {
		select {
		case <-closed:
			return
		case <-time.After(interval):
			interval = activityConfigUpdateInterval
			if err := models.UpdateActivityConfig(context.Background()); err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "update activity config ticker",
					"reason": "can't update activity config",
				}).Error(err)
			}
		}
	}
}
