package tickers

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func InitUnDeliveredOrdersTicker() {
	wg.Add(1)
	go unDeliveredOrdersTicker()
}

func unDeliveredOrdersTicker() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(2 * time.Second):
			log := logs.Eloger.WithField("event", "processing undelivered orders ticker")
			ordersAndDrivers, err := models.GetUndeliveredOrdersFromCRM()
			if err != nil {
				log.WithField("reason", "error GetAllOffersByState").Error(err)
				continue
			}
			if len(ordersAndDrivers) < 1 {
				//log.WithField("reason", "no sutible orders").Debug(err)
				continue
			}

			if err := models.RollBackUnDeliveredOrders(ordersAndDrivers); err != nil {
				log.WithField("reason", "error roll back orders").Error(err)
				continue
			}
			log.WithField("reason", "done").Infof("%v orders will be rollback", len(ordersAndDrivers))
		}
	}
}
