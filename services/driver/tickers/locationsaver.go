package tickers

import (
	"sort"
	"sync"
	"time"

	"github.com/looplab/fsm"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

const (
	saveLocationsPeriod = time.Second * 5
)

var (
	locationBuffer []models.LocationData
	mx             sync.Mutex
)

// DriverConnState setting driver apps status
type DriverConnState struct {
	tableName  struct{}  `sql:"drv_conn_states"`
	ID         int       `json:"id" sql:",pk"`
	CreatedAt  time.Time `sql:"default:now()" json:"created_at"`
	DriverUUID string    `json:"driver_uuid"`
	State      string    `json:"state" description:"Status" required:"true"`
	Comment    string    `json:"comment" description:"Comment"`
	StartedAt  time.Time `json:"started_at"`
}

// DriverStatesDrv setting driver apps status
type DriverStatesDrv struct {
	tableName struct{}  `sql:"drv_states"`
	ID        int       `json:"id" sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"created_at"`
	FSM       *fsm.FSM  `sql:"-"`
	structures.DriverStates
}

//AddLocationToBuffer добавляет координаты в очередь на сохранение
func AddLocationToBuffer(ld []models.LocationData) {
	if len(ld) < 1 {
		return
	}

	// Ensure the most recent coordinates will be placed last
	sort.Slice(ld, func(i, j int) bool {
		return ld[i].Timestamp < ld[j].Timestamp
	})

	now := time.Now()
	for i := range ld {
		ld[i].CreatedAt = now
	}

	mx.Lock()
	locationBuffer = append(locationBuffer, ld...)
	mx.Unlock()
}

//InitLocationSaver запускает функции для сохранения координат и проверки подключения водителей в отдельном потоке
func InitLocationSaver() {
	wg.Add(1)
	go saveLocationsToDBTicker(saveLocationsPeriod)

	dur, _ := time.ParseDuration(config.St.Drivers.CheckConnStatePeriod)
	wg.Add(1)
	go driverAvailTicker(dur)
}

//saveLocationsToDBTicker сохраняет локейшены из буффера в базу и отправялет их в брокер
func saveLocationsToDBTicker(timeout time.Duration) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(timeout):
			if len(locationBuffer) == 0 {
				continue
			}

			// Reduce coordinates count by accepting the most recent ones
			locationMap := getRecentLocationsAndFlushBuffer()
			if locationMap == nil {
				continue
			}

			locationsToInsert := make([]models.LocationData, 0, len(locationMap))
			for _, loc := range locationMap {
				locationsToInsert = append(locationsToInsert, loc)
			}

			go func() {
				err := rabsender.SendJSONByAction(rabsender.AcDriverLocation, "", locationsToInsert)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "Save locations to db ticker",
						"reason": "error sending locations to broker",
					}).Error(err)
				}
			}()

			_, err := dbConn.Model(&locationsToInsert).Insert()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "Save locations to db ticker",
					"reason": "insert error",
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":  "Save locations to db",
				"reason": "save successfully",
			}).Trace("saving to base was successful")
		}
	}
}

// driverAvailTicker тикер который проверят состояние соединения каждого водителя
func driverAvailTicker(timeout time.Duration) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(timeout):
			// Выбираем всех водителей которых потенциально можно перевести в оффлайн
			dur, _ := time.ParseDuration(config.St.Drivers.BlockAfterPeriod)
			dur = dur + time.Second*10 //чтобы время через которое мы блочим водителя из за координат отличалось от времени, через которое мы перестаем присылать заказы водителю
			blockTimeoutDur, _ := time.ParseDuration(config.St.Drivers.BlockTimeout)

			blockTime := time.Now().Add(-dur).Unix()
			blockTimeoutTime := time.Now().Add(-blockTimeoutDur)

			// FIXME: remove when ensure new version is working
			// drvStates := db.Model((*models.DriverStatesDrv)(nil)).
			// 	ColumnExpr("DISTINCT ON (driver_uuid) *").
			// 	Order("driver_uuid").
			// 	Order("id DESC")
			// drvLoc := db.Model((*LocationData)(nil)).
			// 	ColumnExpr("DISTINCT ON (driver_uuid) *").
			// 	Order("driver_uuid").
			// 	Order("timestamp DESC")

			// err := db.Model().
			// 	ColumnExpr("DISTINCT ON (driver_uuid) s.driver_uuid, dl.created_at, dl.timestamp").
			// 	TableExpr("(?) s", drvStates).
			// 	// ColumnExpr("DISTINCT ON (driver_uuid) *").
			// 	Join("inner join (?) dl on dl.driver_uuid=s.driver_uuid", drvLoc).
			// 	// TableExpr("(?) dl", drvLoc).
			// 	Where("s.state <> ?", variables.DriverStateOnModeration).
			// 	Where("s.state <> ?", variables.DriverStateWorking).
			// 	Where("s.state <> ?", variables.DriverStateOnModerationAfterChange).
			// 	Where("s.state <> ?", variables.DriverStateOffline).
			// 	Where("s.state <> ?", variables.DriverStateBlocked).
			// 	Where("s.state <> ?", variables.DriverStateCreated).
			// 	Where("s.started_at < ?", blockTimeoutTime).
			// 	Where("dl.timestamp < ?", blockTime).
			// 	Select(&res)

			onlineDriversUUID, err := models.GetDriversUUIDsAndTransitionTimeByState(constants.DriverStateOnline)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "Cheking drivers availability",
					"reason": "GetDriversUUIDsByState error",
				}).Error(err)
				continue
			}
			if len(onlineDriversUUID) == 0 {
				//logs.Eloger.WithFields(logrus.Fields{
				//	"event":     "Checking drivers availability",
				//	"blockTime": blockTime,
				//}).Debug("No drivers to block")
				continue
			}

			var onlineDriversAfterBlockTimeout []string
			// фильтруем водил, которые перешли в онлайн раньше допустимого времени
			for uuid, transitionTime := range onlineDriversUUID {
				if transitionTime.Unix() < blockTimeoutTime.Unix() {
					onlineDriversAfterBlockTimeout = append(onlineDriversAfterBlockTimeout, uuid)
				}
			}
			if len(onlineDriversAfterBlockTimeout) == 0 {
				//logs.Eloger.WithFields(logrus.Fields{
				//	"event":     "Cheking drivers availability",
				//	"blockTime": blockTime,
				//	"reason":    "empty onlineDriversAfterBlockTimeout",
				//}).Debug("No drivers to block")
				continue
			}

			drvLoc, err := models.GetDriversLastLocations(onlineDriversAfterBlockTimeout...)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "Cheking drivers availability",
					"reason": "GetDriversLastLocations error",
				}).Error(err)
				continue
			}
			for _, k := range drvLoc {
				if k.CreatedAt.Unix() > blockTime {
					continue
				}
				_, er := models.DriverSetStatus(k.DriverUUID, constants.DriverStateOffline, "Перевод в оффлайн по причине отсутствия координат")
				if er != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":      "Cheking drivers availability",
						"driverUUID": k.DriverUUID,
						"reason":     "switching to offline by script",
						"value":      "offline",
					}).Error(er)
					continue
				}
				token, err := models.GetCurrentDriverFCMToken(k.DriverUUID)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":      "Cheking drivers availability",
						"driverUUID": k.DriverUUID,
						"reason":     "Error getting driver fcm token",
					}).Error(err)
					continue
				}

				msg := "Мы не получали координат и отключили вас"
				body := "Возможно у вас были какие то проблемы с сетью"
				alert := models.AlertsDRV{
					DriverUUID: k.DriverUUID,
					Tag:        constants.FcmTagInitData,
					Message:    msg + ". " + body,
				}
				err = alert.Save()
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":      "Cheking drivers availability",
						"driverUUID": k.DriverUUID,
						"reason":     "error saving alert",
					}).Error(err)
				}

				go fcmsender.SendMessageToDriverWithCentrifugo(k.DriverUUID, msg, token, constants.FcmTagInitData, 120, msg, body)

				logs.Eloger.WithFields(logrus.Fields{
					"event":      "Cheking drivers availability",
					"driverUUID": k.DriverUUID,
					"created_at": k.CreatedAt.Unix(),
					"value":      "offline",
				}).Warn("Blocked by not recieving coordinates")
			}
		}
	}
}

func getRecentLocationsAndFlushBuffer() map[string]models.LocationData {
	locationMap := make(map[string]models.LocationData)

	mx.Lock()
	defer mx.Unlock()

	for _, loc := range locationBuffer {
		if loc.DriverUUID == "" || loc.Latitude == 0 || loc.Longitude == 0 {
			continue
		}
		locationMap[loc.DriverUUID] = loc
	}
	locationBuffer = []models.LocationData{} // remember to flush the buffer
	return locationMap
}
