package tickers

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func getRegionsFromCRM() ([]structures.Region, error) {
	var regions []structures.Region
	url := fmt.Sprintf("%s/%s/regions_for_distribution", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &regions)

	return regions, err
}

//InitFreeOrdersUpdateTicker запускает функции для обновления актуальных свободных заказов
func InitFreeOrdersUpdateTicker() {
	wg.Add(1)
	go freeOffersUpdateTicker()
}

func freeOffersUpdateTicker() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(time.Second):
			log := logs.Eloger.WithField("event", "update free offers array")

			regions, err := getRegionsFromCRM()
			if err != nil {
				log.WithField("reason", "error GetRegions").Error(err)
				continue
			}

			offersMap := make(map[int][]models.OfferDrv)
			ok := true
			for _, region := range regions {
				var offers []models.OfferDrv
				offers, err = models.GetRegionalOffersByState(region.ID, true, constants.OrderStateFree)
				if err != nil {
					log.WithField("reason", "error GetAllOffersByState").Error(err)
					ok = false
					break
				}
				offersMap[region.ID] = offers
			}

			if ok {
				models.AllFreeOffers = offersMap
			}
		}
	}
}
