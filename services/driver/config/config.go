package config

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/faemproject/backend/faem/pkg/logs"

	"github.com/spf13/viper"
)

var (
	// Envs - переменная в которой раньше хранились данные
	Envs map[string]string

	// St ([St]orage) - переменная для хранения конфигурации
	St AppConfig
)

// По новому стайлу все переменные хранятся в структуре St, но что бы не пережделывать
// оставил старую структуру [Envs], и просто ей присвоил новые данные, но сами данные
// также доступны и в новой структуре

// InitConfig initialize config
func InitConfig(filepath string) {
	// инициализируем дефольные значения
	initDefaults()

	// читаем конфиг
	viper.SetConfigFile(filepath)
	err := viper.ReadInConfig()
	if err != nil {
		logs.OutputError(fmt.Sprintf("Error reading config file. %s", err))
	}

	// А теперь можно почитать переменные окружения
	bindEnvVars()

	err = viper.Unmarshal(&St)
	if err != nil {
		fmt.Println(fmt.Errorf("Error unmarshalling config file. %s", err))
	}

	// St.CRM.Host = "http://localhost:1324/api/v2"
	// Присваеваем данные Env переменной с которой работают и другие системы
	Envs = initEnvsOld()
}

func initEnvsOld() map[string]string {

	m := map[string]string{
		"host":            viper.GetString("Database.Host"),
		"user":            viper.GetString("Database.User"),
		"password":        viper.GetString("Database.Password"),
		"port":            viper.GetString("Database.Port"),
		"db":              viper.GetString("Database.Db"),
		"brokerURL":       viper.GetString("Broker.UserURL"),
		"brokerUserURL":   viper.GetString("Broker.UserCredits"),
		"brokerDriverURL": viper.GetString("Broker.DriverURL"),
		"brokerDriver":    viper.GetString("Broker.DriverCredits"),
	}

	return m
}

func bindEnvVars() {
	viper.SetEnvPrefix("DRIVER")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}

// PrintVars output env vars
func PrintVars() {
	fmt.Println("\nEVIEROINMENT VARS:")
	fmt.Printf("host:       %s\n", Envs["host"])
	fmt.Printf("user:       %s\n", Envs["user"])
	fmt.Printf("password:   %s\n", Envs["password"])
	fmt.Printf("port:       %s\n", Envs["port"])
	fmt.Printf("db:         %s\n", Envs["db"])
	fmt.Printf("brokerURL:  %s\n", Envs["brokerURL"])
	fmt.Printf("brokerUserURL: %s\n", Envs["brokerUserURL"])
	fmt.Printf("brokerDriver: %s\n", Envs["brokerDriver"])
	fmt.Printf("brokerDriverURL: %s\n", Envs["brokerDriverURL"])

	fmt.Printf("SMPP: %s\n", St.SMPP.Host)
}
