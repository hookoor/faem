package config

import (
	"github.com/spf13/viper"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	developmentEnv = "development"
)

// AppConfig main configuration struct
type AppConfig struct {
	Application struct {
		Env                  string
		Port                 string
		TokenExpiredM        int64
		RefreshTokenExpiredM int64
		SessionExpS          int64
		LogLevel             string
		LogFormat            string
		TimeZone             string
	}
	WebView struct {
		Host string
	}
	Drivers struct {
		CheckConnStatePeriod           string
		BlockAfterPeriod               string
		AlertsExpirationTime           string
		BlockTimeout                   string //BlockTimeout time after which online drivers without coordinates will be blocked
		CheckUndistributedOffersPeriod string
		ClientCallPeriod               string
		ResponseTime                   int64
		Commission                     float32
	}
	Database struct {
		Host     string
		User     string
		Password string
		Port     int
		Db       string
	}
	Broker struct {
		UserURL        string
		UserCredits    string
		DriverURL      string
		DriverCredist  string
		ExchagePrefix  string
		ExchagePostfix string
	}
	Elastic struct {
		Host  string
		Index string
		Port  string
	}
	SMPP struct {
		Host     string
		User     string
		Password string
		From     string
	}
	CRM struct {
		Host string
	}
	Billing struct {
		Host     string
		Username string
		Password string
	}
	FCM struct {
		ServerKey string
	}
	OSRM struct {
		// Host - !!! значение перезаписывается базой
		Host string
	}
	CloudStorage struct {
		ProjectID  string
		BucketName string
	}
	Notifier struct {
		Host string
	}
}

func (a *AppConfig) IsDevelopment() bool {
	return a.Application.Env == developmentEnv
}

// Установка дефолтныйз значений
func initDefaults() {
	// APPLICATION DATA
	viper.SetDefault("Application.Env", developmentEnv)
	viper.SetDefault("Application.Port", structures.DriverPort)
	viper.SetDefault("Application.TokenExpiredM", 1200)
	viper.SetDefault("Application.RefreshTokenExpiredM", 30240)
	viper.SetDefault("Application.SessionExpS", 120)
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")
	viper.SetDefault("Application.TimeZone", "Europe/Moscow")

	// DRIVERS
	viper.SetDefault("Drivers.CheckConnStatePeriod", "15s")
	viper.SetDefault("Drivers.AlertsExpirationTime", "24h")
	viper.SetDefault("Drivers.CheckUndistributedOffersPeriod", "10s")
	viper.SetDefault("Drivers.BlockAfterPeriod", "2m")
	viper.SetDefault("Drivers.BlockTimeout", "30s")
	viper.SetDefault("Drivers.ClientCallPeriod", "10m")
	viper.SetDefault("Drivers.ResponseTime", 20)
	viper.SetDefault("Drivers.Commission", 0.05)

	// DATABASE DATA
	viper.SetDefault("Database.Host", "78.110.156.74")
	viper.SetDefault("Database.User", "barman")
	viper.SetDefault("Database.Password", "ba4man80")
	viper.SetDefault("Database.Port", 6001)
	viper.SetDefault("Database.Db", "driver")

	// RABBIT DATA
	viper.SetDefault("Broker.DriverURL", "78.110.156.74:6004")
	viper.SetDefault("Broker.DriverCredits", "android:android")
	viper.SetDefault("Broker.UserURL", "78.110.156.74:6004")
	viper.SetDefault("Broker.UserCredits", "barmen:kfclover97")
	viper.SetDefault("Broker.ExchagePrefix", "")
	viper.SetDefault("Broker.ExchagePostfix", "")

	// ELASTIC DATA
	viper.SetDefault("Elastic.Host", "78.110.156.74")
	viper.SetDefault("Elastic.Port", "6006")
	viper.SetDefault("Elastic.Index", "driver_logs")

	// WEBVIEW DATA
	viper.SetDefault("WebView.Host", "78.110.156.74:7001")
	// SMPP
	viper.SetDefault("SMPP.Host", "default_host:port")
	viper.SetDefault("SMPP.User", "faem")
	viper.SetDefault("SMPP.Password", "secret")
	viper.SetDefault("SMPP.From", "Faem")
	// CRM
	viper.SetDefault("CRM.Host", "https://crm.apis.stage.faem.pro/api/v2")
	// Billing
	viper.SetDefault("Billing.Host", "https://billing.apis.stage.faem.pro/rpc/v1")
	viper.SetDefault("Billing.Username", "")
	viper.SetDefault("Billing.Password", "")
	//FCM
	viper.SetDefault("FCM.ServerKey", "")
	// TODO: добавить в вайпер
	// open street routing machine
	viper.SetDefault("OSRM.Host", "http://osrm.faem.svc.cluster.local")

	viper.SetDefault("CloudStorage.ProjectID", "faem-staging-01")
	viper.SetDefault("CloudStorage.BucketName", "faem-staging-images-storage")

	viper.SetDefault("notifier.host", "https://notifier.apis.stage.faem.pro/api/v2")
}

// TODO: По хорошему надо также указать переменные для рэбита, а именно название очередей и пр.
// проблема в том что сейчас используются переменные из общего пакета, нужно сделать
// их дефольными, а из конфига их можно переписать
