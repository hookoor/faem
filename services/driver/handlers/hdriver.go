package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/tickers"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// GetInitData возвращает данные
func GetInitData(c echo.Context) error {
	ctx := c.Request().Context()
	var orderData models.OfferForDriver

	driverUUID := driverUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetInitData",
		"driverUUID": driverUUID,
	})

	returnData, err := models.GetInitData(ctx, driverUUID)
	if err != nil {
		res := logs.OutputRestError("Error getting init data", err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get broker data",
			"driverUUID": driverUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	orderData.OfferTaximetr, orderData.State.Value, err = models.GetCurrentDriverOrder(driverUUID)
	if err != nil {
		res := logs.OutputRestError("Error getting current driver order", err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get current order",
			"driverUUID": driverUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	var counterOrderData models.OfferForDriver
	counterOrderData.OfferTaximetr, counterOrderData.State.Value, err = models.GetCounterDriverOrder(driverUUID)
	if err != nil {
		err = errpath.Err(err, "Error getting counter driver order")
		log.WithField("reason", "GetCounterDriverOrder").Error(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	//чтобы заказ не отсылался когда он пустой
	if orderData.State.Value != "" {
		driver, err := models.GetDriverActivity(c.Request().Context(), driverUUID)
		if err != nil {
			res := logs.OutputRestError("error getting current driver activity", err)
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "get current driver activity",
				"driverUUID": driverUUID,
			}).Error(err)
			return c.JSON(http.StatusBadRequest, res)
		}

		orderData.StartTime = models.CalcStartTime(driverUUID, orderData.OfferTaximetr.OfferData.UUID)
		returnData.OrderData = &orderData
		if counterOrderData.OfferData.UUID != "" {
			returnData.CounterOrder = &counterOrderData
		}
		returnData.OrderData.Order.Tariff.SetTotalPriceForDriver(driver.Driver, models.GetCurrentBlockingConfig().GuaranteedDriverIncomeActivityLimit)
		// if returnData.OrderData.Order.Tariff.Unpaid {
		// 	returnData.OrderData.Order.Tariff.BonusPayment = returnData.OrderData.Order.Tariff.GuaranteedDriverIncome
		// }
		returnData.OrderData.OfferTaximetr.Order.Comment = getModifyComment(returnData.OrderData.Order, orderData.State.Value)
		for i, val := range constants.OrderStateRU {
			if i == orderData.State.Value {
				for in := range orderData.Order.Routes {
					if orderData.Order.Routes[in].FrontDoor == 0 {
						continue
					}
					frontDoorStr := fmt.Sprintf(", п. %v", orderData.Order.Routes[0].FrontDoor)
					orderData.Order.Routes[in].UnrestrictedValue += frontDoorStr
				}

				orderData.State.Message = val
				break
			}
		}
	}
	return c.JSON(http.StatusOK, returnData)
}

// SetDriverState driver status handler
func SetDriverState(c echo.Context) error {
	var (
		response ResponseWithDriverState
	)

	newState := c.QueryParam("state")
	// driverID := driverIDFromJWT(c)
	driverUUID := driverUUIDFromJWT(c)

	// Перенес эту строчку из стейтмашины в контроллер, ибо он ругался на циклческие зависимости.
	// По моему идея с разбивкой логики по разным пакетам для разных сущностей была такой себе
	oldState, err := models.GetCurrentDriverStateFromCRM(driverUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "set driver state",
			"driverUUID": driverUUID,
			"reason":     err,
			"value":      newState,
		}).Error("Error getting current driver state")
		res := logs.OutputRestError("Error setting new status", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	response.DriverState.Value = oldState

	if oldState == constants.DriverStateWorking || oldState == constants.DriverStateConsidering {
		response.ResponseStruct.Code = http.StatusBadRequest
		response.ResponseStruct.Msg = "Нельзя менять статус при выпонении заказа"
		return c.JSON(http.StatusBadRequest, response)
	}
	comm := fmt.Sprintf("DriverUUID=%s. New state=%s", driverUUID, newState)
	_, errWL := models.DriverSetStatus(driverUUID, newState, comm)
	if errWL != nil {
		msg := "Error setting new driver state"
		log := logs.Eloger.WithFields(logrus.Fields{
			"event":      "set driver state",
			"driverUUID": driverUUID,
			"reason":     errWL.Error,
			"value":      newState,
		})

		if errWL.Level == structures.WarningLevel {
			log.Warning(msg)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(msg)
		}

		response.ResponseStruct = logs.OutputRestError("Error setting new status", errWL.Error)
		return c.JSON(http.StatusBadRequest, response)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "set driver state",
		"driverUUID": driverUUID,
		"reason":     "driver request",
		"value":      newState,
	}).Info("Setted new state")

	response.ResponseStruct = structures.ResponseStruct{
		Code: 200,
		Msg:  "OK!",
	}
	response.DriverState.Value = newState
	return c.JSON(http.StatusOK, response)
}

func GetDriverByUUID(c echo.Context) error {
	var (
		driver models.DriversApps
	)
	uuid := driverUUIDFromJWT(c)
	header := map[string]string{
		"Authorization": c.Request().Header.Get(echo.HeaderAuthorization),
	}
	driver, err := models.GetDriverFromCRM(uuid, header)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get driver by uuid",
			"driverUUID": uuid,
			"reason":     err,
		}).Error("Error finding driver")
		res := logs.OutputRestError("Error finding driver", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	driver.Password = ""
	return c.JSON(http.StatusOK, driver)
}

func GetIncomeData(c echo.Context) error {

	uuid := driverUUIDFromJWT(c)
	IncomeData, err := models.GetIncomeData(uuid)
	if err != nil {

		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get income data for driver",
			"driverUUID": uuid,
			"reason":     err,
		}).Error("Error getting income data")

		res := logs.OutputRestError("Error getting income data", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, IncomeData)
}

//
func GetIncomeStats(c echo.Context) error {
	const (
		periodDay   = "day"
		periodWeek  = "week"
		periodMonth = "month"
		day         = 24 * time.Hour
	)

	uuid := driverUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "get income stats for driver",
		"driver_uuid": uuid,
	})

	var start time.Time
	withDailyStats := true
	now := localtime.TimeInZone(time.Now(), config.St.Application.TimeZone)
	periodStr := c.QueryParam("period")
	if periodStr == "" {
		// adding hour to start counting from current hour
		// i.e. [12.05 12:35] will count hours from [11.05 13:00] till [12.05 12:59]
		start = now.Add(-day + time.Hour).Truncate(time.Hour)
	} else {
		start = time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
		switch periodStr {
		case periodDay:
			break
		case periodWeek:
			start = start.Add(-6 * day)
			withDailyStats = false
		case periodMonth:
			start = start.Add(-30 * day)
			withDailyStats = false
		default:
			err := errors.New("invalid time filter: " + periodStr)
			log.WithError(err).Error("Error getting income stats")

			res := logs.OutputRestError("Error getting income stats", err)
			return c.JSON(http.StatusBadRequest, res)
		}
	}

	log.Info(start)

	incomeStats, err := models.GetIncomeStats(uuid, start, withDailyStats)
	if err != nil {
		log.WithError(err).Error("Error getting income stats")

		res := logs.OutputRestError("Error getting income stats", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, incomeStats)
}

//GetFreeOffers godoc
func GetFreeOffers(c echo.Context) error {
	uuid := driverUUIDFromJWT(c)
	ordersNumber, err := strconv.Atoi(c.QueryParam("number"))
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     err,
		}).Error("invalid number argument")

		res := logs.OutputRestError("invalid argument", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	sortType := c.QueryParam("sorttype")
	service := c.QueryParam("service")

	acState, err := models.GetCurrentDriverStateFromCRM(uuid)
	if err != nil {
		msg := "error getting driver actual state"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if acState != constants.DriverStateOnline {
		msg := "driver is not online"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     msg,
		}).Debug()

		return c.JSON(http.StatusOK, nil)
	}

	drvData, err := models.GetDriverByUUID(uuid)
	if err != nil {
		msg := "error getting driver data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	exceptDebug := true
	for _, item := range drvData.AvailableServices {
		if item.Name == "Дебаг" {
			exceptDebug = false
		}
	}

	taxiPark, err := models.GetTaxiParkFromCRM(drvData.GetTaxiParkUUID())
	if err != nil {
		msg := "error getting driver taxipark"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	taxDataFromOffers, err := models.GetFreeOrdersByDriver(taxiPark.RegionID, uuid, taxiPark.UUID, service, ordersNumber, exceptDebug, sortType)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free offers for driver",
			"driverUUID": uuid,
			"reason":     err,
		}).Error("Error getting free offers")

		res := logs.OutputRestError("Error getting free offers", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	for i := range taxDataFromOffers {
		taxDataFromOffers[i].Order.Tariff.SetTotalPriceForDriver(drvData.Driver, models.GetCurrentBlockingConfig().GuaranteedDriverIncomeActivityLimit)
	}

	return c.JSON(http.StatusOK, taxDataFromOffers)
}

// NewConference
func NewConference(c echo.Context) error {
	driverPhone, driverUUID := driverPhoneFromJWT(c), driverUUIDFromJWT(c)

	target := c.Param("target")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "new conference",
		"driverUUID": driverUUID,
		"target":     target,
	})

	errWL := models.ConnectDriverToTarget(driverUUID, driverPhone, models.DriverCallTarget(target))

	if errWL != nil {
		log.WithFields(logrus.Fields{"reason": "error connect driver to target"})

		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// GetFreeOfferDetails renders a free offer information with additional activity and route to client data.
func GetFreeOfferDetails(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	offerUUID := c.Param("uuid")
	offer, err := models.GetFreeOfferDetails(driverUUID, offerUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "get free order details",
			"driverUUID": driverUUID,
			"offerUUID":  offerUUID,
		}).Error(err)

		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	if offer != nil {
		driver, err := models.GetDriverActivity(c.Request().Context(), driverUUID)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "get free order details",
				"driverUUID": driverUUID,
			}).Error(err)

			res := logs.OutputRestError("error getting free order details", err)
			return c.JSON(http.StatusBadRequest, res)
		}

		offer.Order.Comment = getModifyComment(offer.Order, constants.OrderStateOffered)

		offer.Order.Tariff.SetTotalPriceForDriver(driver.Driver, models.GetCurrentBlockingConfig().GuaranteedDriverIncomeActivityLimit)
		// if offer.Order.Tariff.Unpaid {
		// 	offer.Order.Tariff.BonusPayment = offer.Order.Tariff.GuaranteedDriverIncome
		// }
	}
	return c.JSON(http.StatusOK, offer)
}

//AssignOrderToDriver godoc
func AssignOrderToDriver(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	offerUUID := c.Param("uuid")
	err := models.SendFreeOfferToDriverSafely(driverUUID, offerUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "assing order to the driver",
			"driverUUID": driverUUID,
			"reason":     err,
		}).Error("error send offer to the driver")
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  err.Error(),
		})
	}
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

//SaveDriverLocation godoc
func SaveDriverLocation(c echo.Context) error {
	var (
		driverLoc       []models.LocationData
		driverLocFilter []models.LocationData
	)
	driverUUID := driverUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "save driver location handler",
		"driverUUID": driverUUID,
	})
	if err := c.Bind(&driverLoc); err != nil {
		log.WithField("reason", "error binding data").Error(err)

		res := logs.OutputRestError("Error binding driver location data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	dur, _ := time.ParseDuration(config.St.Drivers.BlockAfterPeriod)
	coorExpirationTime := time.Now().Add(-dur).Unix()

	for i := range driverLoc {
		if delta := coorExpirationTime - driverLoc[i].Timestamp; delta > 0 {
			log.WithField("delta", delta).Warn("too old coordinates")
			continue
		}
		driverLoc[i].DriverUUID = driverUUID
		driverLocFilter = append(driverLocFilter, driverLoc[i])
	}
	tickers.AddLocationToBuffer(driverLocFilter)
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

//SaveDriverFirebaseToken godoc
func SaveDriverFirebaseToken(c echo.Context) error {
	var (
		driverTok models.DriverFirebaseToken
		err       error
	)
	if err = c.Bind(&driverTok); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "save driver firebase token handler",
			"reason": "error binding data",
		}).Error(err)

		res := logs.OutputRestError("Error binding driver firebase token data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	driverTok.DriverUUID = driverUUIDFromJWT(c)

	err = driverTok.Save()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "save driver firebase token handler",
			"reason": "error saving token",
		}).Error(err)

		res := logs.OutputRestError("Error saving driver firebase token data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

func getModifyComment(or structures.Order, orderState string) *string {
	comm := or.GetComment()
	var msg string
	if or.Service.ProductDelivery {
		switch orderState {
		case constants.OrderStateOffered,
			constants.OrderStateAccepted,
			constants.OrderStateStarted:
			storeData := or.GetProductsData().StoreData
			if storeData.IsNeedToEnter() {
				msg = "Вам нужно будет зайти в заведение и забрать заказ"
			}
		case constants.OrderStateOnPlace:
			msg = "Пожалуйста, выйдите и заберите товар, Вас ждут сотрудники заведения"
		}

		comm = fmt.Sprintf("%s\n\n%s", msg, comm)
	}
	storePhone := or.GetProductsData().StoreData.Phone
	if storePhone != "" {
		comm = fmt.Sprintf("%s\nТелефон заведения - %v", comm, storePhone)
	}
	if or.GetProductsData().Buyout {
		comm = fmt.Sprintf("%s\nВЫКУП - %v р.", comm, or.Tariff.ProductsPrice)
	}
	return &comm
}
