package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func GetActivityRate(c echo.Context) error {
	resp, err := models.GetActivityRate(c.Request().Context())
	if err != nil {
		msg := "failed to get activity rate"
		logs.LoggerForContext(c.Request().Context()).
			WithField("reason", msg).
			Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, resp)
}

type BuyActivityArgs struct {
	Count int `json:"count"`
}

type BuyActivityResp struct {
	Activity int `json:"activity"`
}

func BuyActivity(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "buying driver activity",
			"driverUUID": driverUUID,
		})

	var args BuyActivityArgs
	if err := c.Bind(&args); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	activity, err := models.BuyActivity(c.Request().Context(), driverUUID, args.Count)
	if err != nil {
		msg := "Ошибка покупки активности"
		log.WithField("reason", msg).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, BuyActivityResp{Activity: activity})
}

type TransferBalanceArgs struct {
	Amount float64 `json:"amount"`
}

func TransferCardBalanceToBonus(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "transfer driver balance",
			"driverUUID": driverUUID,
		})

	var args TransferBalanceArgs
	if err := c.Bind(&args); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := models.TransferCardBalanceToBonus(c.Request().Context(), driverUUID, args.Amount); err != nil {
		msg := "Ошибка изменения баланса"
		log.WithField("reason", msg).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, logs.OutputRestOK("Транзакция отправлена"))
}

func GetActivityHistory(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "buying driver activity",
			"driverUUID": driverUUID,
		})

	history, err := models.ActivityHistory(c.Request().Context(), driverUUID)
	if err != nil {
		msg := "Ошибка загрузки истории активности"
		log.WithField("reason", msg).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, history)
}
