package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// GetConfig godoc
func GetConfig(c echo.Context) error {
	conf, err := models.GetCurrentConfig()
	if err != nil {
		msg := "error getting current config"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting driver config",
			"reason": msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, conf)
}

// ChangeConfig godoc
func ChangeConfig(c echo.Context) error {
	var (
		conf models.ConfigDRV
	)
	err := c.Bind(&conf)
	if err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "changing driver config",
			"reason": msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	conf, err = models.ChangeCurrentConfig(conf)
	if err != nil {
		msg := "error change config"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "changing driver config",
			"reason": msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, conf)
}
