package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
)

// OrderRating godoc
func OrderRating(c echo.Context) error {
	var ordRat models.OrderRatingDrv
	if err := c.Bind(&ordRat); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "error binding data",
		}).Error(err)

		res := logs.OutputRestError("Error binding order rating data", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Fetch the order first
	var order models.OrderDrv
	if err := models.GetByUUID(ordRat.OrderUUID, &order); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "Error finding order",
		}).Error(err)
		res := logs.OutputRestError("Error finding order", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	order.DriverRating.Value = ordRat.Value
	order.DriverRating.Comment = ordRat.Comment

	ordRat.DriverUUID = driverUUIDFromJWT(c)
	ordRat.ClientUUID = order.GetClientPhone() // FIXME: a bit of dirty hack, but as we don't have full clients table we will use the phone
	if err := ordRat.Save(); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "error saving data",
		}).Error(err)

		res := logs.OutputRestError("error saving data", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Update driver's blacklist if required
	minClientRating, err := models.FetchClientMinBlacklistRating()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order rating",
			"orderUUID": ordRat.OrderUUID,
		}).Error(err)
		// continue intentionally
		minClientRating = 0
	}
	if err = models.UpdateDriverBlacklistIfNeeded(ordRat.OrderRating, minClientRating); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order rating",
			"orderUUID": ordRat.OrderUUID,
		}).Error(err)
		// continue intentionally
	}

	// Notify listeners about new rating
	if err = rabsender.SendJSONByAction(rabsender.AcOfferNewState, rabbit.SetRatingKey, order.Order); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "error send update order rating to broker",
		}).Error(err)
		res := logs.OutputRestError("Error update order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusBadRequest, ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	})
}
