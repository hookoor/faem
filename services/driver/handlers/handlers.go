package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg/urlvalues"
	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	emptyUUIDMsg        = "empty uuid"
	bindingDataErrorMsg = "error binding data"
)

// ResponseStruct godoc
type ResponseStruct struct {
	Code int    `json:"code"`
	Msg  string `json:"message"`
}

// // ResponseWithPrice нужна для пересчета стоимости по завершению заказа
// type ResponseWithPrice struct {
// 	ResponseStruct

// 	TripTime int               `json:"trip_time"` // время в секундах
// }

// ResponseWithDriverState ответ на запрос смены статуса водителя
type ResponseWithDriverState struct {
	structures.ResponseStruct
	DriverState models.DriverStateNotify `json:"driver_state"`
}

// MyOffers godoc
func MyOffers(c echo.Context) error {
	var returnData structures.OfferTaximetr
	return c.JSON(http.StatusOK, returnData)
}

// UserIDFromJWT return user ID
func driverIDFromJWT(c echo.Context) int {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["driver_id"].(float64)
	return int(userID)
}
func driverPhoneFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userUUID := claims["phone"].(string)
	return userUUID
}
func driverUUIDFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userUUID := claims["driver_uuid"].(string)
	return userUUID
}

func getPager(c echo.Context) (urlvalues.Pager, error) {
	var (
		pager urlvalues.Pager
	)
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return pager, fmt.Errorf("Error parsing pager limit,%s", err)
	}

	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return pager, fmt.Errorf("Error parse orders page,%s", err)
	}
	pager.Offset = (page - 1) * limit
	pager.Limit = limit
	return pager, nil
}

func taxiParkUUIDFromJWT(c echo.Context) []string {
	user, check := c.Get("user").(*jwt.Token)
	if !check {
		return nil
	}
	claims := user.Claims.(jwt.MapClaims)

	uuids, check := claims["taxi_parks_uuid"].([]interface{})

	if !check {
		return nil
	}
	var uuidsStr []string
	for _, val := range uuids {
		st, ok := val.(string)
		if !ok {
			continue
		}
		uuidsStr = append(uuidsStr, st)
	}
	return uuidsStr
}
