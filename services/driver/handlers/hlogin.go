package handlers

import (
	"fmt"
	"net/http"
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/smpp"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/db"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
	"gitlab.com/faemproject/backend/faem/services/driver/tickers/tickersmodern"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// Server smpp connection instance
type Server struct {
	CloudStorage          CloudStorage
	PhotoControls         PhotoControlRepository
	PhotoControlPublisher PhotoControlPublisher

	DB      db.DB
	Tickers *tickersmodern.Ticker

	sender *smpp.Sender

	wg     sync.WaitGroup
	closed chan struct{}
}

const (
	neededPasswordLen int = 4
)

// NewServer Sender New constructor
func NewServer(
	sender *smpp.Sender,
	cloudStorage CloudStorage,
	photoControlRepository PhotoControlRepository,
	photoControlPublisher PhotoControlPublisher,
	db db.DB,
	tickers *tickersmodern.Ticker,
) *Server {
	return &Server{
		CloudStorage:          cloudStorage,
		PhotoControls:         photoControlRepository,
		PhotoControlPublisher: photoControlPublisher,
		DB:                    db,
		sender:                sender,
		Tickers:               tickers,
	}
}

func ChangeDrvAppVersion(c echo.Context) error {
	var version models.DrvAppVersion
	if err := c.Bind(&version); err != nil {
		res := logs.OutputRestError("Error binding  data", err)

		logs.Eloger.WithFields(logrus.Fields{
			"event":  "change driver app version",
			"reason": "Error binding data",
		}).Error(err)

		return c.JSON(http.StatusBadRequest, res)
	}
	newVer, err := version.Change()
	if err != nil {
		res := logs.OutputRestError("Error change version", err)

		logs.Eloger.WithFields(logrus.Fields{
			"event":  "change driver app version",
			"reason": "Error change version",
		}).Error(err)

		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, newVer)
}

// RegisterDriverHandler запрос на валидацию нового водителя
func (s *Server) RegisterDriverHandler(c echo.Context) error {

	var loginData models.DriverRegisterRequest
	err := c.Bind(&loginData)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "new driver registration",
		"phone":    loginData.Phone,
		"deviceID": loginData.DeviceID,
	})
	if err != nil {
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	loginData.Phone, err = phoneverify.NumberVerify(loginData.Phone)
	if err != nil {
		msg := "Указан неверный номер"
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Warning(err)
		return c.JSON(http.StatusUnauthorized, structures.ResponseStruct{
			Code: http.StatusUnauthorized,
			Msg:  msg,
		})
	}
	pass, smsNeed, sessionNeed, err := models.GetPasswordForDriver(loginData.DeviceID, loginData.Phone)
	if err != nil {
		msg := "error getting password for driver"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusUnauthorized, res)
	}
	msgForResponse := "Введите пароль"
	if sessionNeed {
		_, err = models.NewRegSessionByPassword(pass, loginData.Phone, loginData.DeviceID)
		if err != nil {
			msg := "error creating new session"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
			res := logs.OutputRestError(msg, err)
			return c.JSON(http.StatusUnauthorized, res)
		}
	}
	if smsNeed {
		msgForResponse = "Введите пароль из смс сообщения"
		msg := fmt.Sprintf("Ваш постоянный пароль для входа в Faem: %s", pass)
		go s.sender.SendSMS(loginData.Phone, msg, s.sender.From)
	}

	// Чтобы мобилка понимала, что мы регаем нового водителя
	resp := struct {
		structures.ResponseStruct
		IsNewDriver bool `json:"is_new_driver"`
	}{
		ResponseStruct: structures.ResponseStruct{
			Code: http.StatusOK,
			Msg:  msgForResponse,
		},
		IsNewDriver: !models.DriverExists(loginData.Phone),
	}

	return c.JSON(http.StatusOK, resp)
}

func RemindPassword(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "remind driver password",
	})

	loginData := new(models.DriverRegisterRequest)

	if err := c.Bind(&loginData); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(bindingDataErrorMsg, err))
	}
	log = log.WithFields(logrus.Fields{
		"phone":    loginData.Phone,
		"deviceID": loginData.DeviceID,
	})

	sess, check, err := models.GetCurrentRemindSessionIfExists(loginData.Phone)
	if err != nil {
		msg := "error getting current remind session"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	if check {
		msg := "Вы пока не можете запросить пароль"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, models.NewRegResponseStruct{
			Code:            http.StatusBadRequest,
			Msg:             msg,
			NextRequestTime: sess.Expiration.Unix(),
		})
	}

	sess, err = models.CreateNewRemindSeesion(loginData.Phone, loginData.DeviceID)
	if err != nil {
		msg := "error create remind session"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	driver, err := models.GetDriverByPhoneIfExists(loginData.Phone)
	if err != nil && err != pg.ErrNoRows {
		msg := "error getting driver by phone"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	} else if err == pg.ErrNoRows {
		if err != nil {
			msg := "Пароль еще не сгенерирован. Запросите пароль заново"
			log.WithField("reason", msg).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
		}
	} else if len(driver.Password) != neededPasswordLen {
		msg := "Некорректный пароль. Обратитесь к администратору"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	codePieceForAutocall1 := driver.Password[:2]
	codePieceForAutocall2 := driver.Password[2:]

	msg := fmt.Sprintf("Здравствуйте, ваш код для входа $ %s, %s ,, $ ,, %s, %s , ", codePieceForAutocall1, codePieceForAutocall2, codePieceForAutocall1, codePieceForAutocall2)
	newDriverAutoCall := structures.AutoOutCallRequest{
		Text:      msg,
		Phone:     loginData.Phone,
		Type:      structures.RemindDriverPassType,
		CreatedAt: time.Now(),
	}
	//go s.sender.SendSMS(loginData.Phone, msg, s.sender.From)
	err = rabsender.SendJSONByAction(rabsender.AcNewAutocall, "", newDriverAutoCall)
	if err != nil {
		msg := "error sending autocall data to rabbit"
		res := models.NewRegResponseStruct{
			Code:            400,
			Msg:             fmt.Sprintf("%s,%s", msg, err),
			NextRequestTime: sess.Expiration.Unix(),
		}
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusUnauthorized, res)
	}

	return c.JSON(http.StatusOK, models.NewRegResponseStruct{
		Code:            http.StatusOK,
		Msg:             "Вам сейчас перезвонят",
		NextRequestTime: sess.Expiration.Unix(),
	})
}

// PasswordVerificationHandler ввод полученного через СМС кода
func PasswordVerificationHandler(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "password verification",
	})

	validata := new(models.VerificationPassword)
	if err := c.Bind(validata); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(bindingDataErrorMsg, err))
	}
	log = log.WithFields(logrus.Fields{
		"phone":     validata.Phone,
		"device_id": validata.DeviceID,
		"password":  validata.Password,
	})

	if err := validata.Validate(); err != nil {
		msg := "error validate input data"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	driver, err := models.ValidatePassword(validata)
	if err != nil {
		msg := "error validate password"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusUnauthorized, logs.OutputRestError(msg, err, http.StatusUnauthorized))
	}

	jwt, err := models.GenerateJWT(driver)
	if err != nil {
		msg := "error generate jwt"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.Info("Recieved correct password")

	return c.JSON(http.StatusOK, jwt)
}

// // PhoneVerificationHandler ввод полученного через СМС кода
// func PhoneVerificationHandler(c echo.Context) error {
// 	var validata models.VerificationCode
// 	if err := c.Bind(&validata); err != nil {
// 		res := logs.OutputRestError("error binding verification data", err)

// 		logs.Eloger.WithFields(logrus.Fields{
// 			"event":  "new driver registration",
// 			"reason": "error binding verification data",
// 		}).Error(err)

// 		return c.JSON(http.StatusBadRequest, res)
// 	}
// 	jwt, err := models.ValidateCode(validata)
// 	if err != nil {
// 		res := logs.OutputRestError("Error validating code", err, 401)

// 		logs.Eloger.WithFields(logrus.Fields{
// 			"event":  "new driver registration",
// 			"reason": "Validation code is wrong",
// 		}).Warn(err)

// 		return c.JSON(http.StatusUnauthorized, res)
// 	}

// 	msg := fmt.Sprintf("Token issued for DeviceID=%s", validata.DeviceID)
// 	logs.Eloger.WithFields(logrus.Fields{
// 		"event":      "new driver registration",
// 		"driverUUID": jwt.DriverUUID,
// 		"reason":     "Recieved correct SMS code",
// 	}).Info(msg)

// 	// logs.OutputDriverEvent("JWT issued", msg, jwt.DriverUUID)

// 	return c.JSON(http.StatusOK, jwt)

// }

// RefreshTokenHandler обновление ранее выданного токена токена
func RefreshTokenHandler(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "jwt token refresh",
	})

	refresh := new(models.RefreshRequest)
	if err := c.Bind(refresh); err != nil {
		msg := "error binding refresh token data"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	newToken, err := models.RefreshJWTToken(refresh.RefreshToken)
	if err != nil {
		msg := "error refreshing JWT token"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusUnauthorized, logs.OutputRestError(msg, err, http.StatusUnauthorized))
	}
	logs.Eloger.WithField("driver_uuid", newToken.DriverUUID).Info("Refresh token issued")

	return c.JSON(http.StatusOK, newToken)
}

//CheckConnections для проверки соединения
func CheckConnections(c echo.Context) error {
	return c.JSON(http.StatusOK, ResponseStruct{
		Code: 200,
		Msg:  "OK!",
	})
}

//CheckVersion для проверки версии приложения
func CheckVersion(c echo.Context) error {
	var (
		res struct {
			Version string `json:"version"`
			Link    string `json:"link"`
		}
	)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "check version",
	})

	ver, err := models.GetActualVersion()
	if err != nil {
		res := logs.OutputRestError("Error getting actual app version", err)
		log.WithFields(logrus.Fields{
			"reason": "Error getting actual app version",
		}).Error(err)
		return c.JSON(http.StatusUnauthorized, res)
	}
	res.Version = ver.Version
	res.Link = ver.Link
	return c.JSON(http.StatusOK, res)
}

func GetCurrentTime(c echo.Context) error {
	now := time.Now().Unix()
	return c.JSON(http.StatusOK, echo.Map{"current_time": now})
}
