package handlers

import (
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"net/http"
)

func GetInsuranceMessages(c echo.Context) error {
	var cfg structures.InsuranceParams
	url := config.St.CRM.Host + "/config/insurance"
	header := map[string]string{
		"Authorization": c.Request().Header.Get("Authorization"),
	}
	if err := tool.SendRequest(http.MethodGet, url, header, nil, &cfg); err != nil {
		msg := "error sending data to CRM while getting insurance messages"
		res := logs.OutputRestError(msg, err)
		logrus.WithFields(logrus.Fields{
			"reason": msg,
			"url":    url,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, cfg.Messages)
}
