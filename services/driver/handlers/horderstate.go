package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// OfferHandler godoc
func OfferHandler(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "set order state",
	})

	orderStateReq := new(models.OfferStateRequest)
	if err := c.Bind(orderStateReq); err != nil {
		msg := "error binding orders state data"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	driverUUID := driverUUIDFromJWT(c)
	respWithState, err := orderStateReq.OrderStateTransfer(driverUUID)
	if err != nil {
		logs.Eloger.WithField("reason", "error order state transfer").Error(err)
		return c.JSON(respWithState.Code, respWithState)
	}

	if orderStateReq.State == constants.OrderStateFinished || orderStateReq.State == constants.OrderStateRejected {
		state, err := models.GetCurrentDriverStateFromCRM(driverUUID)
		if err != nil {
			logs.Eloger.WithField("reason", "error getting current driver state").Error(err)
			return c.JSON(respWithState.Code, respWithState)
		}
		respWithState.CurrentState = &models.DriverStateNotify{
			Value: state,
		}
	}
	if respWithState.Tariff != nil {
		driver, err := models.GetDriverActivity(c.Request().Context(), driverUUID)
		if err != nil {
			logs.Eloger.WithField("reason", "error getting current driver activity").Error(err)
			return c.JSON(respWithState.Code, respWithState)
		}

		respWithState.Tariff.SetTotalPriceForDriver(driver.Driver, models.GetCurrentBlockingConfig().GuaranteedDriverIncomeActivityLimit)
	}

	return c.JSON(respWithState.Code, respWithState)
}
