package handlers

import (
	"fmt"
	"image/jpeg"
	"image/png"
	"mime/multipart"
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	structCS "gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// PhotocontrolFolderName - название папки для Google Cloud
const PhotocontrolFolderName = "photocontrol"

// ----------------------------
// ----------------------------
// ----------------------------

// GetPhotocontrolSnapshotSketchesList -
func (s *Server) GetPhotocontrolSnapshotSketchesList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetPhotocontrolSnapshotSketchesList",
	})

	res, err := s.DB.ModelPhotocontrol.GetPhotocontrolSnapshotSketchesList(c.Request().Context())
	if err != nil {
		log.WithField("reason", "GetPhotocontrolSnapshotSketchesList").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("GetPhotocontrolSnapshotSketchesList Done!")

	return c.JSON(http.StatusOK, res)
}

// CreatePhotocontrolSnapshotSketch -
func (s *Server) CreatePhotocontrolSnapshotSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var res models.SnapshotSketch

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":              "CreatePhotocontrolSnapshotSketch",
		"SnapshotSketchUUID": res.UUID,
	})

	err = c.Bind(&res)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	res, err = s.DB.ModelPhotocontrol.CreatePhotocontrolSnapshotSketch(ctx, &res)
	if err != nil {
		log.WithField("reason", "CreatePhotocontrolSnapshotSketch").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("CreatePhotocontrolSnapshotSketch Done!")

	return c.JSON(http.StatusOK, res)
}

// UpdatePhotocontrolSnapshotSketch -
func (s *Server) UpdatePhotocontrolSnapshotSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")
	var res models.SnapshotSketch

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdatePhotocontrolSnapshotSketch",
	})

	err = c.Bind(&res)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithField("SnapshotSketchUUID", res.UUID)
	log.WithField("SnapshotSketchName", res.Name)

	res, err = s.DB.ModelPhotocontrol.UpdatePhotocontrolSnapshotSketch(ctx, uuid, &res)
	if err != nil {
		log.WithField("reason", "updating").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("UpdatePhotocontrolSnapshotSketch Done!")

	return c.JSON(http.StatusOK, res)
}

// DeletePhotocontrolSnapshotSketch -
func (s *Server) DeletePhotocontrolSnapshotSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":              "DeletePhotocontrolSnapshotSketch",
		"SnapshotSketchUUID": uuid,
	})

	err = s.DB.ModelPhotocontrol.DeletePhotocontrolSnapshotSketch(ctx, uuid)
	if err != nil {
		log.WithField("reason", "deleting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("DeletePhotocontrolSnapshotSketch Done!")

	return c.JSON(http.StatusOK, "Done")
}

// ----------------------------
// ----------------------------
// ----------------------------

// GetPhotocontrolSketchesList -
func (s *Server) GetPhotocontrolSketchesList(c echo.Context) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetPhotocontrolSketchesList",
	})

	res, err := s.DB.ModelPhotocontrol.GetPhotocontrolSketchesList(c.Request().Context())
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("GetPhotocontrolSketchesList Done!")

	return c.JSON(http.StatusOK, res)
}

// GetPhotocontrolSketchByUUID -
func (s *Server) GetPhotocontrolSketchByUUID(c echo.Context) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetPhotocontrolSketchesList",
	})

	uuid := c.Param("uuid")
	log = log.WithField("uuid", uuid)

	res, err := s.DB.ModelPhotocontrol.GetPhotocontrolSketchByUUID(c.Request().Context(), uuid)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("GetPhotocontrolSketchesList Done!")

	return c.JSON(http.StatusOK, res)
}

// CreatePhotocontrolSketch -
func (s *Server) CreatePhotocontrolSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var res models.PhotocontrolSketch

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "CreatePhotocontrolSketch",
	})

	err = c.Bind(&res)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithField("PhotocontrolSketchUUID", res.UUID)
	log.WithField("PhotocontrolSketchName", res.Name)

	res, err = s.DB.ModelPhotocontrol.CreatePhotocontrolSketch(ctx, &res)
	if err != nil {
		log.WithField("reason", "create").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("CreatePhotocontrolSketch Done!")

	return c.JSON(http.StatusOK, res)
}

// UpdatePhotocontrolSketch -
func (s *Server) UpdatePhotocontrolSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")
	var res models.PhotocontrolSketch

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdatePhotocontrolSketch",
	})

	err = c.Bind(&res)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	res.UUID = uuid

	log.WithFields(logrus.Fields{
		"PhotocontrolSketchUUID": res.UUID,
		"PhotocontrolSketchName": res.Name,
	})

	res, err = s.DB.ModelPhotocontrol.UpdatePhotocontrolSketch(ctx, uuid, &res)
	if err != nil {
		log.WithField("reason", "updating").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("UpdatePhotocontrolSketch Done!")

	return c.JSON(http.StatusOK, res)
}

// DeletePhotocontrolSketch -
func (s *Server) DeletePhotocontrolSketch(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":                  "DeletePhotocontrolSketch",
		"PhotocontrolSketchUUID": uuid,
	})

	err = s.DB.ModelPhotocontrol.DeletePhotocontrolSketch(ctx, uuid)
	if err != nil {
		log.WithField("reason", "deleting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("DeletePhotocontrolSketch Done!")

	return c.JSON(http.StatusOK, "Done")
}

// ----------------------------
// ----------------------------
// ----------------------------

// GetPhotocontrolDriverTicketsList -
func (s *Server) GetPhotocontrolDriverTicketsList(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	driveruuid := c.Param("driveruuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetPhotocontrolDriverTicketsList",
	})

	pcontrols, err := s.DB.ModelPhotocontrol.GetPhotocontrolDriverTicketsList(ctx, driveruuid)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("GetPhotocontrolDriverTicketsList Done!")

	return c.JSON(http.StatusOK, pcontrols)
}

// GetPhotocontrolTicketsList -
func (s *Server) GetPhotocontrolTicketsList(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var filter models.PhotocontrolTicketsFilter
	var resp tool.PaginatorResult

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetPhotocontrolTicketsList",
	})

	err = c.Bind(&filter)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	err = filter.Paginator.Fasten()
	if err != nil {
		log.WithField("reason", "Paginator fasten").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	filter.AllowedTaxiParks = taxiParkUUIDFromJWT(c)

	pcontrols, count, err := s.DB.ModelPhotocontrol.GetPhotocontrolTicketsList(ctx, filter)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	for _, el := range pcontrols {
		resp.List = append(resp.List, el)
	}
	resp.RecordsCount = count

	log.Info("GetPhotocontrolTicketsList Done!")

	return c.JSON(http.StatusOK, resp)
}

// GetPhotocontrolTicketByUUID -
func (s *Server) GetPhotocontrolTicketByUUID(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":            "GetPhotocontrolTicketsList",
		"PhotocontrolUUID": uuid,
	})

	pcontrol, err := s.DB.ModelPhotocontrol.GetPhotocontrolTicketByUUID(ctx, uuid)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("GetPhotocontrolTicketsList Done!")

	return c.JSON(http.StatusOK, pcontrol)
}

// CreatePhotocontrolTicket -
func (s *Server) CreatePhotocontrolTicket(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var pcontrol models.Photocontrol

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "CreatePhotocontrolTicket",
	})

	jwtAuthorization := c.Request().Header.Get("Authorization")

	err = c.Bind(&pcontrol)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithFields(logrus.Fields{
		"PhotocontrolUUID": pcontrol.UUID,
		"PhotocontrolNmae": pcontrol.Name,
	})

	{ // заполнение данных о операторе

		// jwtSec := "AllYourBase"
		// tokenString := jwtAuthorization[7:]
		// tkn, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// 	return []byte(jwtSec), nil
		// })
		// if err != nil {

		// 	err = errpath.Err(err)
		// 	log.WithField("reason", "get data from jwt").Error(err)
		// 	return c.JSON(http.StatusBadRequest, err)
		// }

		pcontrol.Creator.UUID = ""
		pcontrol.Creator.Name = ""
		pcontrol.Creator.Role = ""
	}

	{ // заполнение данных о водителе
		if pcontrol.Driver.Alias != 0 && pcontrol.Driver.UUID == "" {
			var bind struct {
				Drivers []structures.Driver `json:"drivers"`
			}
			url := config.St.CRM.Host + "/drivers/filter"
			header := map[string]string{
				"Authorization": jwtAuthorization,
			}
			payload := `{
					"page": 1,
					"limit": 15,
					"alias":"` + strconv.Itoa(pcontrol.Driver.Alias) + `"
				}`
			err = tool.SendRequest(http.MethodPost, url, header, payload, &bind)
			if err != nil {
				err = errpath.Err(err)
				log.WithField("reason", "set driver data").Error(err)
				return c.JSON(http.StatusBadRequest, err)
			}
			if len(bind.Drivers) == 0 {
				err := errpath.Err(fmt.Errorf("cannot find driver by alias \"%d\"", pcontrol.Driver.Alias))
				log.WithField("reason", "set driver data").Error(err)
				return c.JSON(http.StatusBadRequest, err)
			}
			driver := bind.Drivers[0]
			pcontrol.Driver.UUID = driver.UUID
			pcontrol.Driver.Alias = driver.Alias
			pcontrol.Driver.Name = driver.Name
			pcontrol.TaxiParkUUID = driver.TaxiParkUUID
		}
	}

	if (pcontrol.Settings == models.PhotocontrolSettings{}) {
		pcontrol.Settings = models.CreatePhotocontrolSetting()
	}

	err = s.DB.ModelPhotocontrol.CreatePhotocontrolTicket(ctx, &pcontrol)
	if err != nil {
		log.WithField("reason", "creating").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	{ // сделать уведомление для водителя
		if pcontrol.Driver.UUID == "" {
			err = errpath.Errorf("DriverUUID is empty")
			log.WithField("reason", "DriverUUID is empty").Error(err)
			return c.JSON(http.StatusBadRequest, err.Error())
		}

		token, err := models.GetCurrentDriverFCMToken(pcontrol.Driver.UUID)
		if err != nil {
			err = errpath.Err(err)
			log.WithField("reason", "Error GetCurrentDriverFCMToken").Error(err)
			return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
				Code: http.StatusBadRequest, Msg: err.Error(),
			})
		}

		pload := "Вам назначен фотоконтроль: " + pcontrol.Title
		go fcmsender.SendMessageToDriverWithCentrifugo(pcontrol.Driver.UUID, pload, token, constants.FcmTagInitData, 36000, "Фотоконтроль", "Вам назначен фотоконтроль")

		// старая версия пуша
		// удалить если новая пуш пушка работает
		// var push = structures.PushMailing{
		// 	Type:         structures.MailingInformDriverType,
		// 	Title:        "Фотоконтроль",
		// 	Message:      "Вам назначен фотоконтроль",
		// 	TargetsUUIDs: []string{pcontrol.Driver.UUID},
		// }
		// err = rabsender.SendJSONByAction(rabsender.AcNewPushMailing, "", push)
		// if err != nil {

		// 	err = errpath.Err(err)
		// 	log.WithField("reason", "Error sending data to broker").Error(err)
		// 	return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
		// 		Code: http.StatusBadRequest,
		// 	})
		// }
	}

	log.Info("CreatePhotocontrolTicket Done!")

	return c.JSON(http.StatusOK, pcontrol)
}

// UpdatePhotocontrolTicket -
func (s *Server) UpdatePhotocontrolTicket(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var pcontrol models.Photocontrol
	uuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":            "UpdatePhotocontrolTicket",
		"PhotocontrolUUID": uuid,
	})

	err = c.Bind(&pcontrol)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithField("PhotocontrolName", pcontrol.Name)

	err = s.DB.ModelPhotocontrol.UpdatePhotocontrolTicket(ctx, &pcontrol, uuid)
	if err != nil {
		log.WithField("reason", "updating").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("UpdatePhotocontrolTicket Done!")

	return c.JSON(http.StatusOK, pcontrol)
}

// DeletePhotocontrolTicket -
func (s *Server) DeletePhotocontrolTicket(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	uuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":            "UpdatePhotocontrolTicket",
		"PhotocontrolUUID": uuid,
	})

	err = s.DB.ModelPhotocontrol.DeletePhotocontrolTicket(ctx, uuid)
	if err != nil {
		log.WithField("reason", "updating").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("UpdatePhotocontrolTicket Done!")

	msg := "Photocontrol " + uuid + " deleted"
	return c.JSON(http.StatusOK, msg)
}

// PassingPhotocontrolTicket -
func (s *Server) PassingPhotocontrolTicket(c echo.Context) error {
	memberUUID, member, err := structures.MemberFromJWT(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	if member != structures.DriverMember {
		return c.JSON(http.StatusBadRequest, errpath.Errorf("jwt member is not driver").Error())
	}
	// ---

	ctx := c.Request().Context()
	var pcontrol models.Photocontrol
	pcuuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":            "PassingPhotocontrolTicket",
		"PhotocontrolUUID": pcuuid,
	})

	pcontrol, err = s.DB.ModelPhotocontrol.GetPhotocontrolTicketByUUID(ctx, pcuuid)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var images map[string]*multipart.FileHeader = make(map[string]*multipart.FileHeader)
	for _, item := range pcontrol.SnapshotSketches {
		if item.Status != models.PhotocontrolRejected && item.Status != "" { // если статус картинки не Rejected, то новую катинку загружать не нужно
			continue
		}
		fileHeader, err := c.FormFile(item.Name)
		if err != nil {
			err = errpath.Err(err, "image by key '"+item.Name+"' not found")
			log.WithField("reason", "CreatePhotocontrolSnapshotSketch").Error(err)
			return c.JSON(http.StatusBadRequest, err.Error())
		}

		images[item.Name] = fileHeader
	}

	// ---
	// сохраняем в GC картинки и получить ссылки
	var imagesURL map[string]string = make(map[string]string)

	for key, val := range images {
		file, err := val.Open()
		if err != nil {
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
		var upImage = cloudstorage.Upload{
			Filename:   fmt.Sprint(structures.GenerateUUID(), "_", val.Filename),
			FolderName: fmt.Sprint(PhotocontrolFolderName, "/", memberUUID, "/", pcontrol.Name, "/", time.Now().Unix()),
			Filesize:   val.Size,
			Filetype:   val.Header.Get("Content-Type"),
			File:       file,
		}
		imageURL, err := s.CloudStorage.UploadPhoto(ctx, &upImage)
		if err != nil {
			err = errpath.Err(err)
			log.WithField("reason", "UploadPhoto").Error(err)
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
		imagesURL[key] = imageURL
	}

	// ---

	pcontrol.Status = models.PhotocontrolModeration

	for key, val := range imagesURL {
		for i := range pcontrol.SnapshotSketches {
			if pcontrol.SnapshotSketches[i].Name == key {
				pcontrol.SnapshotSketches[i].Status = models.PhotocontrolModeration
				pcontrol.SnapshotSketches[i].ImageURL = tool.URL(val)
			}
		}
	}

	{ // для возможности загружать по одной фотке за запрос
		pcontrol.Status = models.PhotocontrolModeration
		for i := range pcontrol.SnapshotSketches {
			if pcontrol.SnapshotSketches[i].Status != models.PhotocontrolModeration {
				pcontrol.Status = models.PhotocontrolCreated
			}
		}
	}

	err = s.DB.ModelPhotocontrol.PassingPhotocontrolTicket(ctx, &pcontrol)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "photocontrol passing").Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	// !!!!!!! сделать уведомление для оператора
	log.Info("PassingPhotocontrolTicket Done!")

	return c.JSON(http.StatusOK, pcontrol)
}

// PhotocontrolPostponement -
func (s *Server) PhotocontrolPostponement(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var data struct {
		UUID            string `json:"uuid"`
		NewTimeValidity int64  `json:"date_time"`
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "PhotocontrolPostponement",
	})

	err = c.Bind(&data)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithFields(logrus.Fields{
		"PhotocontrolUUID": data.UUID,
		"NewTimeValidity":  data.NewTimeValidity,
	})

	err = s.DB.ModelPhotocontrol.PhotocontrolPostponement(ctx, data.UUID, data.NewTimeValidity)
	if err != nil {
		log.WithField("reason", "PhotocontrolPostponement").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("PhotocontrolPostponement Done!")

	return c.JSON(http.StatusOK, "Photocontrol Postponement Done!")
}

// ----------------------------
// ----------------------------
// ----------------------------

// PassingPhotocontrolTicketNew -
func (s *Server) PassingPhotocontrolTicketNew(c echo.Context) error {
	memberUUID, member, err := structures.MemberFromJWT(c)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	if member != structures.DriverMember {
		return c.JSON(http.StatusBadRequest, errpath.Errorf("jwt member is not driver").Error())
	}
	// ---

	ctx := c.Request().Context()
	var pcontrol models.Photocontrol
	pcuuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":            "PassingPhotocontrolTicket",
		"PhotocontrolUUID": pcuuid,
	})

	pcontrol, err = s.DB.ModelPhotocontrol.GetPhotocontrolTicketByUUID(ctx, pcuuid)
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	formFiles, err := c.MultipartForm()
	if err != nil {
		log.WithField("reason", "getting").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// сохраняем в GC картинки и получить ссылки
	var imagesURL map[string]string = make(map[string]string)
	for ffkey, ffval := range formFiles.File {
		for _, item := range pcontrol.SnapshotSketches {
			if ffkey != item.Name {
				continue
			}
			if item.Status != models.PhotocontrolRejected && item.Status != models.PhotocontrolCreated { // если статус картинки не Rejected, то новую катинку загружать не нужно
				continue
			}

			file, err := ffval[0].Open()
			if err != nil {
				log.WithField("reason", errInvalidImageField).Error(err)
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}

			uploaddd := &structCS.UploadImage{
				FolderName: fmt.Sprint(PhotocontrolFolderName, "/", memberUUID, "/", pcontrol.Name, "/", time.Now().Unix()),
				Filesize:   ffval[0].Size,
				Filename:   ffval[0].Filename,
				Filetype:   ffval[0].Header.Get("Content-Type"),
			}

			if uploaddd.Filetype == "image/jpeg" {
				img, err := jpeg.Decode(file)
				if err != nil {
					log.Error(err)
					return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
				}
				uploaddd.Image = img
			}
			if uploaddd.Filetype == "image/png" {
				img, err := png.Decode(file)
				if err != nil {
					return errpath.Err(err)
				}
				uploaddd.Image = img
			}

			imageURL, err := s.CloudStorage.UploadImage(ctx, uploaddd)
			if err != nil {
				log.WithField("reason", "UploadPhoto").Error(errpath.Err(err))
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}

			imagesURL[item.Name] = imageURL
		}
	}

	// ---

	for key, val := range imagesURL {
		for i := range pcontrol.SnapshotSketches {
			if pcontrol.SnapshotSketches[i].Name == key {
				pcontrol.SnapshotSketches[i].Status = models.PhotocontrolModeration
				pcontrol.SnapshotSketches[i].ImageURL = tool.URL(val)
			}
		}
	}

	{ // для возможности загружать по одной фотке за запрос
		pcontrol.Status = models.PhotocontrolModeration
		for i := range pcontrol.SnapshotSketches {
			if pcontrol.SnapshotSketches[i].Status == models.PhotocontrolCreated {
				pcontrol.Status = models.PhotocontrolCreated
			}
		}
	}

	err = s.DB.ModelPhotocontrol.PassingPhotocontrolTicket(ctx, &pcontrol)
	if err != nil {
		log.WithField("reason", "photocontrol passing").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// !!!!!!! сделать уведомление для оператора
	log.Info("PassingPhotocontrolTicket Done!")

	return c.JSON(http.StatusOK, pcontrol)
}

// ----------------------------
// ----------------------------
// ----------------------------

// PhotocontrolPeridisityUpdate -
func (s *Server) PhotocontrolPeridisityUpdate(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	var data struct {
		UUID    string `json:"uuid"`
		Seconds int64  `json:"seconds"`
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "PhotocontrolPeridisityUpdate",
	})

	err = c.Bind(&data)
	if err != nil {
		log.WithField("reason", "binding").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.WithFields(logrus.Fields{
		"PhotocontrolUUID": data.UUID,
		"Seconds":          data.Seconds,
	})

	err = s.DB.ModelPhotocontrol.PeridisityUpdate(ctx, data.UUID, data.Seconds)
	if err != nil {
		log.WithField("reason", "PeridisityUpdate").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("PhotocontrolPeridisityUpdate Done!")

	return c.JSON(http.StatusOK, "PhotocontrolPeridisityUpdate Done!")
}

// ----------------------------
// ----------------------------
// ----------------------------

// CreateByDriverGroupTemplate -
func (s *Server) CreateByDriverGroupTemplate(c echo.Context) error {
	ctx := c.Request().Context()

	log := logs.Eloger.WithField("event", "CreateByDriverGroupTemplate")

	var bind struct {
		DriverGroupUUID   string                       `json:"driver_group_uuid"`
		GroupTemplateUUID string                       `json:"group_template_uuid"`
		Driver            models.PhotocontrolForDriver `json:"driver"`
	}
	err := c.Bind(&bind)
	if err != nil {
		log.WithField("reason", "binding error").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log = logs.Eloger.WithFields(logrus.Fields{
		"DriverGroupUUID":         bind.DriverGroupUUID,
		"DriverGroupTemplateUUID": bind.GroupTemplateUUID,
	})

	gp, err := s.DB.ModelPhotocontrol.GetPhotocontrolSketchByUUID(ctx, bind.GroupTemplateUUID)
	if err != nil {
		log.WithField("reason", "GetDriverGroupTemplateByUUID error").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	pControl, err := s.DB.ModelPhotocontrol.CreateByDriverGroupTemplate(ctx, bind.DriverGroupUUID, bind.Driver, &gp)
	if err != nil {
		log.WithField("reason", "CreateByDriverGroupTemplate error").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("CreateByDriverGroupTemplate Done!")
	return c.JSON(http.StatusOK, pControl)
}

// DeletePhotocontrolsByDriverGroup -
func (s *Server) DeletePhotocontrolsByDriverGroup(c echo.Context) error {
	ctx := c.Request().Context()
	driveruuid := c.Param("driveruuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "DeletePhotocontrolsByDriverGroup",
		"DriverUUID": driveruuid,
	})

	err := s.DB.ModelPhotocontrol.DeletePhotocontrolsByDriverGroup(ctx, driveruuid)
	if err != nil {
		log.WithField("reason", "DeletePhotocontrolsByDriverGroup error").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	log.Info("DeletePhotocontrolsByDriverGroup Done!")
	return c.JSON(http.StatusOK, "DeletePhotocontrolsByDriverGroup Done!")
}

// ----------------------------
// ----------------------------
// ----------------------------
