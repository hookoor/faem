package handlers

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	structCS "gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/db"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	uploadToCloudTimeout   = 60 * time.Second
	errFindPhotoControl    = "Ошибка поиска фотоконтроля"
	errSmoke               = "Что-то пошло не так"
	errCantBeginRegControl = "Вы не можете начать этот фотоконтроль"
	errCheckingRegControl  = "Ошибка проверки существования регистрационного контроля"
	errControlExists       = "Что-то пошло не так"
	errBadRequest          = "Некорректные данные"
	errPhotoExists         = "Вы уже загрузили это фото"
	errAlreadyPass         = "Вы уже загрузили все необходимые фото"
	errInvalidImageField   = "Некорректное фото"
	errUploadPhoto         = "Ошибка загрузки фото"
)

type CloudStorage interface {
	UploadPhoto(ctx context.Context, upload *structCS.Upload) (string, error)
	UploadImage(ctx context.Context, upload *cloudstorage.UploadImage) (string, error)
}

type PhotoControlRepository interface {
	UpdatePhotoControl(ctx context.Context, photoControl *models.PhotoControl) error
	CreateOrGetPhotocontrol(ctx context.Context, driverID string, controlType *models.ControlType) (models.PhotoControl, error)
	AllControlTypes(ctx context.Context) ([]models.ControlType, error)
	// GetControlTypeByName(ctx context.Context, name string) (models.ControlType, error)
	// RecentlyUploadedPhotoControl(ctx context.Context, driverID string, controlType *models.ControlType) ([]models.PhotoControl, error)
	GetPhotoControlByControlIDAndType(ctx context.Context, controlID, controlType string) (models.PhotoControl, error)
	// CreatePhotoControl(ctx context.Context, control *models.PhotoControl) error
	PhotoControlExists(ctx context.Context, phCon models.PhotoControlIn, cType *models.ControlType) (bool, error)
}

type PhotoControlPublisher interface {
	PhotoControlCompleted(photoControl structures.PhotoControl) error
}

type beginPhotoControlRequest struct {
	Type string `json:"type"`
}

type beginPhotoControlResponse struct {
	ID       string                  `json:"id"`
	Required []models.ControlVariant `json:"required"`
}

func (s *Server) BeginPhotoControl(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":      "start a photo control",
		"driverUUID": driverUUID,
	})

	var req beginPhotoControlRequest
	if err := c.Bind(&req); err != nil {
		log.WithField("reason", errBadRequest).Error(err)
		res := logs.OutputRestError(errBadRequest, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Look for already uploaded photo controls with the same type
	controlType := db.GetControlTypeByName(req.Type)
	if controlType.Name == "" {
		msg := "not found control type by name"
		log.WithField("reason", errBadRequest).Error(msg)
		res := logs.OutputRestError(errBadRequest, fmt.Errorf(msg))
		return c.JSON(http.StatusBadRequest, res)
	}
	if req.Type == models.RegControlName {
		checkRegControl, err := models.HaveRegControl(driverUUID)
		if err != nil {
			log.WithField("reason", errCheckingRegControl).Error(err)
			res := logs.OutputRestError(errCheckingRegControl, err)
			return c.JSON(http.StatusBadRequest, res)
		}
		if !checkRegControl {
			log.WithField("reason", errCantBeginRegControl).Error(err)
			res := logs.OutputRestError(errCantBeginRegControl, err)
			return c.JSON(http.StatusBadRequest, res)
		}
	}
	phCon, err := s.PhotoControls.CreateOrGetPhotocontrol(c.Request().Context(), driverUUID, &controlType)
	if err != nil {
		log.WithField("reason", errSmoke).Error(err)
		res := logs.OutputRestError(errSmoke, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Filter already uploaded variants for the type and keep the required
	requiredVariants := controlType.FilterAlreadyUploadedVariants(&phCon)
	if len(requiredVariants) < 1 {
		log.Error(errAlreadyPass)
		res := logs.OutputRestError(errAlreadyPass, errors.New(""))
		return c.JSON(http.StatusBadRequest, res)
	}

	log.WithField("controlID", phCon.ControlID).Info("OK")
	return c.JSON(http.StatusOK, beginPhotoControlResponse{
		ID:       phCon.ControlID,
		Required: requiredVariants,
	})
}

func (s *Server) UploadControlPhoto(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "upload control photos",
			"driverUUID": driverUUID,
			"controlID":  c.FormValue("id"),
		})

	controlID, controlType, controlVariant := c.FormValue("id"), c.FormValue("type"), c.FormValue("variant")
	photoControl := models.PhotoControlIn{
		DriverID:       driverUUID,
		ControlID:      controlID,
		ControlType:    controlType,
		ControlVariant: controlVariant,
	}

	controlTypes, err := s.PhotoControls.AllControlTypes(c.Request().Context())
	if err != nil {
		log.WithField("reason", errSmoke).Error(err)
		res := logs.OutputRestError(errSmoke, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	controlTypeFull := db.GetControlTypeByName(controlType)
	if controlTypeFull.Name == "" {
		msg := "not found control type by name"
		log.WithField("reason", errBadRequest).Error(msg)
		res := logs.OutputRestError(errBadRequest, fmt.Errorf(msg))
		return c.JSON(http.StatusBadRequest, res)
	}
	// Validate incoming object
	if err := photoControl.Validate(controlTypes); err != nil {
		log.WithField("reason", errBadRequest).Error(err)
		res := logs.OutputRestError(errBadRequest, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	phCon, err := s.PhotoControls.GetPhotoControlByControlIDAndType(c.Request().Context(), controlID, controlType)
	if err != nil {
		log.WithField("reason", errFindPhotoControl).Error(err)
		res := logs.OutputRestError(errFindPhotoControl, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	fileHeader, err := c.FormFile("image")
	if err != nil {
		log.WithField("reason", errInvalidImageField).Error(err)
		res := logs.OutputRestError(errInvalidImageField, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	file, err := fileHeader.Open()
	if err != nil {
		log.WithField("reason", errInvalidImageField).Error(err)
		res := logs.OutputRestError(errInvalidImageField, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	ctxUpload, cancel := context.WithTimeout(c.Request().Context(), uploadToCloudTimeout)
	defer cancel()

	photoControl.ImageURL, err = s.CloudStorage.UploadPhoto(ctxUpload, &structCS.Upload{
		Filesize: fileHeader.Size,
		Filename: fileHeader.Filename,
		Filetype: fileHeader.Header.Get("Content-Type"),
		File:     file,
	})
	if err != nil {
		log.WithField("reason", errUploadPhoto).Error(err)
		res := logs.OutputRestError(errUploadPhoto, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	missingControlVariants := controlTypeFull.FilterAlreadyUploadedVariants(&phCon)
	check := false
	for _, variant := range missingControlVariants {
		if variant.Name == controlVariant {
			check = true
		}
	}
	if !check {
		log.WithField("reason", errPhotoExists).Error(err)
		res := logs.OutputRestError(errPhotoExists, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	var title string
	for _, val := range controlTypes {
		title = val.Title
	}
	phCon.Photos = append(phCon.Photos, structures.Photo{
		ImageURL:       photoControl.ImageURL,
		ControlVariant: controlVariant,
		Title:          title,
	})
	if err = s.PhotoControls.UpdatePhotoControl(c.Request().Context(), &phCon); err != nil {
		log.WithField("reason", errSmoke).Error(err)
		res := logs.OutputRestError(errSmoke, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Check if all the required photos have been uploaded
	go func() {
		if err := s.notifyIfControlCompleted(context.Background(), controlID, controlType); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":       "notify CRM if a control is completed",
				"controlID":   controlID,
				"controlType": controlType,
			}).Error(err)
		}
	}()

	log.Info("OK")
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

func (s *Server) notifyIfControlCompleted(ctx context.Context, controlID, controlTypeName string) error {
	controlType := db.GetControlTypeByName(controlTypeName)
	if controlType.Name == "" {
		return fmt.Errorf("not found control type by name")
	}
	alreadyUploaded, err := s.PhotoControls.GetPhotoControlByControlIDAndType(ctx, controlID, controlTypeName)
	if err != nil {
		return err
	}
	if !controlType.IsCompleted(len(alreadyUploaded.Photos)) {
		return nil
	}
	// Notify CRM service about the completed photo control
	if err := s.PhotoControlPublisher.PhotoControlCompleted(alreadyUploaded.PhotoControl); err != nil {
		return err
	}
	driverForUpdate, err := models.GetDriverByUUID(alreadyUploaded.DriverID)
	if err != nil {
		return fmt.Errorf("error getting driver,%s", err)
	}
	for i := range driverForUpdate.PhotocontrolData {
		if driverForUpdate.PhotocontrolData[i].ControlType == controlTypeName {
			driverForUpdate.PhotocontrolData[i].Status = structures.ModerationPhotoControlState
		}
	}
	models.UpdateDriversPhotocontrolData([]models.DriversApps{driverForUpdate})
	logs.Eloger.WithFields(logrus.Fields{
		"event":       "notify CRM if a control is completed",
		"controlID":   controlID,
		"controlType": controlTypeName,
	}).Info("OK")

	return nil
}
