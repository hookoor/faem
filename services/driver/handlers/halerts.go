package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// GetAlerts godoc
func GetAlerts(c echo.Context) error {
	uuid := driverUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "getting driver alerts",
		"driverUUID": uuid,
	})
	pager, err := getPager(c)
	if err != nil {
		msg := "error getting pager"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	alerts, err := models.GetAlertsByDriverUUID(uuid, pager)
	if err != nil {
		msg := "error getting driver alerts"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	models.FillCreatedAtUnix(alerts)
	return c.JSON(http.StatusOK, alerts)
}

// MarkAlertsAsRead godoc
func MarkAlertsAsRead(c echo.Context) error {
	driverUUID := driverUUIDFromJWT(c)
	var inputStruct struct {
		AlertsUUID []string `json:"alerts_uuid"`
	}

	err := c.Bind(&inputStruct)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "getting driver alerts",
		"driverUUID": driverUUID,
		"alertsUUID": inputStruct.AlertsUUID,
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = models.MarkAlertsAsRead(driverUUID, inputStruct.AlertsUUID)
	if err != nil {
		msg := "error marking driver alerts as read"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	})
}
