package router

import (
	// cfg "gitlab.com/faemproject/backend/faem/services/crm/config"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	h "gitlab.com/faemproject/backend/faem/services/driver/handlers"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

const apiversion = "v2"

// Init - binding middleware and setup routers
func Init(l *logrus.Logger, s *h.Server) *echo.Echo {

	e := echo.New()
	web.UseHealthCheck(e)
	e.HTTPErrorHandler = web.DefaultHTTPErrorHandler

	// Middleware for logging
	// logrus.SetLevel(logrus.DebugLevel)
	// e.Use(logrusmiddleware.Hook())
	newLogger(l)
	e.Use(Hook())

	// Recover and CORS middlewar
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	//// Metrics
	//p := prometheus.NewPrometheus("echo", nil)
	//p.Use(e)

	// Authorisation group
	jwtGroup := e.Group("/api/" + apiversion + "/auth")
	jwtGroup.POST("/new", s.RegisterDriverHandler)
	jwtGroup.POST("/verification", h.PasswordVerificationHandler)
	jwtGroup.POST("/remind/password", h.RemindPassword)
	jwtGroup.POST("/refresh", h.RefreshTokenHandler)

	openGroup := e.Group("/api/" + apiversion)
	openGroup.POST("/checkconnections", h.CheckConnections)
	openGroup.POST("/checkversion", h.CheckVersion)
	openGroup.POST("/changeversion", h.ChangeDrvAppVersion)
	openGroup.GET("/config", h.GetConfig)
	openGroup.PUT("/config", h.ChangeConfig)
	openGroup.GET("/currenttime", h.GetCurrentTime)

	// JWT middleware
	o := e.Group("/api/" + apiversion)
	jwtSec := config.JWTSecret()
	o.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(jwtSec),
	}))
	e.Use(provideJWTMidlware(jwtSec))

	// Actions
	o.POST("/initdata", h.GetInitData)
	o.GET("/freeorders", h.GetFreeOffers)
	o.GET("/freeorder/:uuid", h.GetFreeOfferDetails)

	o.POST("/setstate", h.SetDriverState)
	o.GET("/assignorder/:uuid", h.AssignOrderToDriver) // назначение свободного офера водителю
	o.GET("/driverdata", h.GetDriverByUUID)
	o.GET("/callclient", h.NewConference)
	o.GET("/call/:target", h.NewConference)
	o.POST("/order", h.OfferHandler)

	o.POST("/order/rating", h.OrderRating)

	o.GET("/alerts/history", h.GetAlerts)
	o.PUT("/alerts/read", h.MarkAlertsAsRead)

	o.POST("/locations", h.SaveDriverLocation)
	o.POST("/firebasetoken", h.SaveDriverFirebaseToken)
	o.GET("/myoffers", h.MyOffers)
	o.GET("/incomedata", h.GetIncomeData)
	o.GET("/incomestats", h.GetIncomeStats)

	o.POST("/photocontrol/begin", s.BeginPhotoControl)
	o.POST("/photocontrol/upload", s.UploadControlPhoto)

	o.GET("/activity/rate", h.GetActivityRate)
	o.POST("/activity/buy", h.BuyActivity)
	o.GET("/activity/history", h.GetActivityHistory)

	o.POST("/balance/card2bonus", h.TransferCardBalanceToBonus)

	o.GET("/messages/insurance", h.GetInsuranceMessages)

	{ // photocontrol
		openGroup.GET("/photocontrol/snapshots", s.GetPhotocontrolSnapshotSketchesList)
		openGroup.POST("/photocontrol/snapshots", s.CreatePhotocontrolSnapshotSketch)
		openGroup.PUT("/photocontrol/snapshots/:uuid", s.UpdatePhotocontrolSnapshotSketch)
		openGroup.DELETE("/photocontrol/snapshots/:uuid", s.DeletePhotocontrolSnapshotSketch)

		openGroup.GET("/photocontrol/sketches", s.GetPhotocontrolSketchesList)
		openGroup.POST("/photocontrol/sketches", s.CreatePhotocontrolSketch)
		openGroup.PUT("/photocontrol/sketches/:uuid", s.UpdatePhotocontrolSketch)
		openGroup.DELETE("/photocontrol/sketches/:uuid", s.DeletePhotocontrolSketch)

		openGroup.GET("/photocontrol/gettickets/:driveruuid", s.GetPhotocontrolDriverTicketsList)
		openGroup.POST("/photocontrol/gettickets", s.GetPhotocontrolTicketsList)
		openGroup.GET("/photocontrol/getticket/:uuid", s.GetPhotocontrolTicketByUUID)
		o.POST("/photocontrol/ticket", s.CreatePhotocontrolTicket)
		openGroup.PUT("/photocontrol/ticket/:uuid", s.UpdatePhotocontrolTicket)
		openGroup.DELETE("/photocontrol/ticket/:uuid", s.DeletePhotocontrolTicket)
		o.POST("/photocontrol/passingticket/:uuid", s.PassingPhotocontrolTicketNew) // необходим jwt от водителя
		openGroup.POST("/photocontrol/postponement", s.PhotocontrolPostponement)

		o.POST("/photocontrol/peridisityupdate", s.PhotocontrolPeridisityUpdate)

		openGroup.POST("/photocontrol/template/createbydrvgroup", s.CreateByDriverGroupTemplate)
		openGroup.GET("/photocontrol/template/deletebydrvgroup/:driveruuid", s.DeletePhotocontrolsByDriverGroup)
	}

	return e
}

func provideJWTMidlware(jwtSec string) echo.MiddlewareFunc {
	return func(next echo.HandlerFunc) echo.HandlerFunc {
		return func(c echo.Context) error {
			if jwtSec == "" {
				jwtSec = "AllYourBase"
			}
			req := c.Request()
			token := req.Header.Get(echo.HeaderAuthorization)
			if len(token) <= 7 {
				return next(c)
			}
			tokenString := token[7:]
			tkn, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
				return []byte(jwtSec), nil
			})
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "provide jwt",
					"reason": "error parse jwt",
				}).Error(err)
				return next(c)
			}
			c.Set(string("user"), tkn)
			return next(c)
		}
	}
}
