package core

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
)

type Driver interface {
	GetUUID() string
	GetTaxiParkUUID() string

	GetScoring(distance float64) scoring.DriverScoring
	GetDistributionRadius() float64
	GetRemainingDistance() float64

	GetStatus() string

	HasPhoneInBlacklist(phone string) bool
	HasPaymentType(paymentType string) bool
	HasService(service structures.Service) bool
	HasFeatures(features structures.FeatureSlice) bool

	GetLocation() structures.PureCoordinates

	CheckSuitableForOffer(offer Offer) error
}

type DriverSlice []Driver

// Filter возвращает слайс, элементами которого являются все элементы ds,
// для которых predicate вернул true
func (ds DriverSlice) Filter(predicate func(Driver) bool) DriverSlice {
	var result DriverSlice
	for _, d := range ds {
		if predicate(d) {
			result = append(result, d)
		}
	}

	return result
}

// FilterFast возвращает подслайс ds до первого элемента, для которого
// predicate вернул false
//
// Работает быстрее Filter для фильтрации отсортированного ds
func (ds DriverSlice) FilterFast(predicate func(Driver) bool) DriverSlice {
	firstFalseIndex := len(ds)
	for i, d := range ds {
		if !predicate(d) {
			firstFalseIndex = i
			break
		}
	}

	return ds[:firstFalseIndex]
}

// Contains возвращает true, если в ds есть водитель с таким же Driver.GetUUID(), иначе false
func (ds DriverSlice) Contains(driver Driver) bool {
	for i := range ds {
		if ds[i].GetUUID() == driver.GetUUID() {
			return true
		}
	}

	return false
}

// Without возвращает копию слайса ds, но без водителя driver
func (ds DriverSlice) Without(driver Driver) DriverSlice {
	return ds.Filter(func(d Driver) bool {
		return d.GetUUID() != driver.GetUUID()
	})
}

func CreateScoringFromDrivers(drivers DriverSlice, distancesToOrder map[string]float64) []scoring.DriverScoring {
	scorings := make([]scoring.DriverScoring, len(drivers))
	for i, driver := range drivers {
		distance, ok := distancesToOrder[driver.GetUUID()]
		if !ok {
			// на всякий случай
			distance = 999999
		}
		if distance < 0 {
			// тоже на всякий случай
			distance = 0
		}
		scorings[i] = driver.GetScoring(distance)
	}

	return scorings
}
