package core

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

type Offer interface {
	GetUUID() string
	GetOrderUUID() string
	GetClientPhone() string
	GetPaymentType() string
	GetTaxiParkUUID() string

	HasDriverInRejectedList(driver Driver) bool
	HasDriverInBlacklist(driver Driver) bool
	GetService() structures.Service
	GetFeatures() structures.FeatureSlice

	GetScoring() scoring.OfferScoring

	GetState() string
	HasExceededAttemptLimit() bool

	GetStartLocation() structures.PureCoordinates
	IsOutOfTown() bool

	AssignToDriver(driver Driver) error
	ChangeState(newState string, comment string) (bool, error)

	TaxiParksCompatible(driver Driver, allowNeutral bool) bool

	// TODO: как нибудь убрать это
	GetOfferDrv() *models.OfferDrv
	GetDistributionRuleID() int
	GetDistributionRule() TaxiParkRule
	SetDistributionRule(rule TaxiParkRule)
}

type OfferSlice []Offer

func (os OfferSlice) Filter(predicate func(Offer) bool) OfferSlice {
	var result OfferSlice
	for _, o := range os {
		if predicate(o) {
			result = append(result, o)
		}
	}
	return result
}

func CreateScoringFromOffers(offers OfferSlice) []scoring.OfferScoring {
	scorings := make([]scoring.OfferScoring, len(offers))
	for i, o := range offers {
		scorings[i] = o.GetScoring()
	}

	return scorings
}
