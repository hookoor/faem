package scoring

import (
	"github.com/antonmedv/expr"
	"github.com/pkg/errors"
)

type Scoring interface {
	env() interface{}
}

func score(scorings []Scoring, rule string) ([]float64, error) {
	if len(scorings) == 0 {
		return []float64{}, nil
	}

	ruleExec, err := expr.Parse(rule, expr.Env(scorings[0].env()))
	if err != nil {
		return nil, errors.Wrap(err, "failed to parse rule")
	}

	result := make([]float64, len(scorings))
	for i := range scorings {
		score, err := ruleExec.Eval(scorings[i].env())
		if err != nil {
			return nil, errors.Wrap(err, "failed to evaluate rule")
		}

		result[i], _ = score.(float64)
	}

	return result, nil
}
