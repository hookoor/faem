package scoring

import (
	"gitlab.com/faemproject/backend/faem/pkg/normalization"
)

type OfferScoring struct {
	Booster       int     // наличие одноразового бустера
	VIP           int     // важный ли клиент
	VIO           int     // важный ли заказ
	OrderSource   string  // источник заказа
	Waiting       float64 `normalize:"default"` // время с момента создания заказа
	IncreasedFare float64 `normalize:"default"` // доля клиентской надбавки от полной стоимости заказа
	Activity      float64 `normalize:"default"` // активность клиента
	Karma         float64 `normalize:"default"` // рейтинг клиента
	// TODO: add fields
}

func (s OfferScoring) env() interface{} {
	return s
}

func ScoreOffers(offers []OfferScoring, rule string) ([]float64, error) {
	err := normalization.Normalize(&offers)
	if err != nil {
		return nil, err
	}

	scorings := make([]Scoring, len(offers))
	for i := range offers {
		scorings[i] = offers[i]
	}

	return score(scorings, rule)
}
