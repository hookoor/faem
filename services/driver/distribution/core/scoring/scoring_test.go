package scoring

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestScoreOffers(t *testing.T) {
	rule := "100 * Booster + Waiting * 3 + Karma * 2 + Activity + (OrderSource in [\"kek\"] ? 50 : 0) + IncreasedFare * 2.5"

	arr := []OfferScoring{
		{
			Booster:       0,
			OrderSource:   "crm",
			Waiting:       400,
			IncreasedFare: -0.05,
			Activity:      100,
			Karma:         5,
		},
		{
			Booster:       1,
			OrderSource:   "kek",
			Waiting:       100,
			IncreasedFare: 0,
			Activity:      100,
			Karma:         4,
		},
		{
			Booster:       1,
			OrderSource:   "crm",
			Waiting:       200,
			IncreasedFare: 0.1,
			Activity:      80,
			Karma:         4.5,
		},
	}

	expected := []float64{
		6,
		151 + 2.5/3,
		104.5,
	}

	res, err := ScoreOffers(arr, rule)
	assert.NoError(t, err)
	for i := range res {
		assert.Equal(t, expected[i], res[i])
	}
}

func TestScoreDrivers(t *testing.T) {
	rule := "100 * Booster + 12.5 * Waiting + (Distance < 0.5 ? 30 * Distance : 60 * Distance) "
	rule += " + ('kek' in PaymentTypes ? 50 : 0) + GroupWeight / 3"

	arr := []DriverScoring{
		{
			Booster:      0,
			Waiting:      100,
			Distance:     1.0 / 800,
			PaymentTypes: []string{"card"},
			GroupWeight:  8,
		},
		{
			Booster:      1,
			Waiting:      150,
			Distance:     1.0 / 500,
			PaymentTypes: []string{"asdf"},
			GroupWeight:  1,
		},
		{
			Booster:      0,
			Waiting:      400,
			Distance:     1.0 / 1000,
			PaymentTypes: []string{"kek"},
			GroupWeight:  3,
		},
	}

	expected := []float64{
		7.5 + 8.0/3,
		100 + 1.0/6*12.5 + 60 + 1.0/3,
		12.5 + 51,
	}

	res, err := ScoreDrivers(arr, rule)
	assert.NoError(t, err)
	for i := range res {
		assert.Equal(t, expected[i], res[i])
	}
}
