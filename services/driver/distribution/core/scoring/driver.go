package scoring

import (
	"gitlab.com/faemproject/backend/faem/pkg/normalization"
)

type DriverScoring struct {
	Booster            int     // наличие одноразового бустера
	PhotocontrolPassed int     // наличие буста за фотоконтроль
	RentedAuto         int     // арендованное ли авто
	BrandSticker       int     // наличие наклейки
	Activity           float64 `normalize:"default"` // активность водителя
	Waiting            float64 `normalize:"default"` // время с момента перехода в текущий статус (сек)
	Karma              float64 `normalize:"default"` // рейтинг водителя
	// TODO: доработать normalize:"inverse" и поменять
	Distance       float64  `normalize:"default"` // величина обратная расстоянию до заказа (1/x, x - расстояние в метрах)
	Tags           []string // теги водителя
	PaymentTypes   []string // выбранные типы оплаты
	GroupWeight    int      // вес группы
	TaxiParkWeight int      // вес таксопарка
	// TODO: add fields
}

func (s DriverScoring) env() interface{} {
	return s
}

func ScoreDrivers(drivers []DriverScoring, rule string) ([]float64, error) {
	err := normalization.Normalize(&drivers)
	if err != nil {
		return nil, err
	}

	scorings := make([]Scoring, len(drivers))
	for i := range drivers {
		scorings[i] = drivers[i]
	}

	return score(scorings, rule)
}
