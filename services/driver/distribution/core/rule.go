package core

type (
	// Rule - базовая структура правила распределения
	Rule struct {
		ID   int    `json:"id"`
		Expr string `json:"expr"`
	}

	RegionParams struct {
		Rounds                []float64 `json:"rounds"`
		OutOfTownMultiplier   float64   `json:"out_of_town_multiplier"`
		AllowNeutralTaxiParks bool      `json:"allow_neutral_taxi_parks"`
	}

	// RegionRule - структура регионального правила распределения
	RegionRule struct {
		Rule
		Params RegionParams `json:"params"`
	}

	TaxiParkParams struct {
		Rounds              []float64 `json:"rounds"`
		OutOfTownMultiplier float64   `json:"out_of_town_multiplier"`
		SmartAttemptsLimit  int       `json:"smart_attempts_limit"`
	}

	// TaxiParkRule - структура правила распределения таксопарка
	TaxiParkRule struct {
		Rule
		Params TaxiParkParams `json:"params"`
	}
)
