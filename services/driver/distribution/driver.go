package distribution

import (
	"errors"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/util/stringutil"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

type Driver struct {
	models.DriversApps
	Location          structures.PureCoordinates
	WaitTime          float64
	RemainingDistance float64 // расстояние оставшееся по текущему заказу
	ServiceUUIDs      []string
	FeatureUUIDs      []string
}

func (d *Driver) GetTaxiParkUUID() string {
	return d.DriversApps.GetTaxiParkUUID()
}

func (d *Driver) GetLocation() structures.PureCoordinates {
	return d.Location
}

func (d *Driver) GetUUID() string {
	return d.UUID
}

func btoi(b bool) int {
	if b {
		return 1
	}
	return 0
}

func (d *Driver) GetScoring(distance float64) scoring.DriverScoring {
	return scoring.DriverScoring{
		Booster:            btoi(d.Promotion.Booster),
		PhotocontrolPassed: btoi(d.Promotion.ScoringBoostTimeByPhotocontrolPassed > time.Now().Unix()),
		RentedAuto:         btoi(d.Promotion.RentedAuto),
		BrandSticker:       btoi(d.Promotion.BrandSticker),
		Activity:           float64(d.Activity),
		Waiting:            d.WaitTime,
		Karma:              d.Karma,
		Distance:           distance,
		Tags:               d.Tag,
		PaymentTypes:       d.PaymentTypes,
		GroupWeight:        d.Group.DistributionWeight,
		TaxiParkWeight:     d.TaxiParkData.DistributionWeight,
	}
}

var (
	ErrDriverBlacklisted      = errors.New("driver is in client's blacklist")
	ErrClientBlacklisted      = errors.New("client is in driver's blacklist")
	ErrOfferAlreadyRejected   = errors.New("driver already rejected offer")
	ErrPaymentTypeNotSelected = errors.New("driver haven't selected required payment type")
	ErrServiceNotAvailable    = errors.New("service is unavailable for driver")
	ErrFeatureNotAvailable    = errors.New("feature is unavailable for driver")
	ErrNotEnoughBalance       = errors.New("total balance is not enough for covering commissions")
)

func (d *Driver) CheckSuitableForOffer(offer core.Offer) error {
	var err error
	switch {
	case offer.HasDriverInRejectedList(d):
		// водитель уже отказывался от этого оффера
		err = ErrOfferAlreadyRejected
	case offer.HasDriverInBlacklist(d):
		// водитель в черном списке клиента
		err = ErrDriverBlacklisted
	case d.HasPhoneInBlacklist(offer.GetClientPhone()):
		// клиент в черном списке водителя
		err = ErrClientBlacklisted
	case !d.HasPaymentType(offer.GetPaymentType()):
		// водитель не подходит по типу оплаты
		err = ErrPaymentTypeNotSelected
	case !d.HasService(offer.GetService()):
		// водитель не подходит по виду услуги
		err = ErrServiceNotAvailable
	case !d.HasFeatures(offer.GetFeatures()):
		// водитель не подходит по опциям
		err = ErrFeatureNotAvailable
	case !d.HasEnoughBalance(offer):
		// у водителя недостаточно средств на оплату комиссий за заказ
		err = ErrNotEnoughBalance
	}

	return err
}

func (d *Driver) HasPaymentType(requiredType string) bool {
	for _, paymentType := range d.PaymentTypes {
		if paymentType == requiredType {
			return true
		}
	}
	return false
}

func (d *Driver) HasPhoneInBlacklist(phoneNumber string) bool {
	return stringutil.SliceContains(d.Blacklist, phoneNumber)
}

func (d *Driver) HasService(service structures.Service) bool {
	return stringutil.SliceContains(d.ServiceUUIDs, service.UUID)
}

func (d *Driver) HasFeature(feature structures.Feature) bool {
	return stringutil.SliceContains(d.FeatureUUIDs, feature.UUID)
}

func (d *Driver) HasFeatures(features structures.FeatureSlice) bool {
	for _, feature := range features {
		if !d.HasFeature(feature) {
			return false
		}
	}
	return true
}

func (d *Driver) HasEnoughBalance(offer core.Offer) bool {
	if ok, err := d.DriversApps.HasEnoughBalance(offer.GetOfferDrv()); !ok || err != nil {
		return false
	}
	return true
}

func (d *Driver) GetStatus() string {
	return d.Status
}

func (d *Driver) GetDistributionRadius() float64 {
	if d.GetOrderRadius <= 0 {
		return MaxDistributionRadius
	}

	return d.GetOrderRadius
}

func (d *Driver) GetRemainingDistance() float64 {
	return d.RemainingDistance
}

// GetDriversForDistribution - возвращает список uuid для водителей которые подходят для распределения и количество секунд,
// прошедших с последнего перехода в онлайн
func GetDriversForDistribution(regionID int) (map[string]float64, error) {
	result := make(map[string]float64)
	url := fmt.Sprintf("%s/%s/getdriversfordistribution/%d", config.St.CRM.Host, constants.InternalGroup, regionID)
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &result)

	return result, err
}

func fillGroupWeights(drivers []models.DriversApps) error {
	var neededUUIDs []string
outer:
	for _, driver := range drivers {
		groupUUID := driver.Group.UUID
		for _, uuid := range neededUUIDs {
			if uuid == groupUUID {
				continue outer
			}
		}

		neededUUIDs = append(neededUUIDs, groupUUID)
	}

	weightsMap, err := getDriverGroupsWeights(neededUUIDs)
	if err != nil {
		return err
	}

	for _, driver := range drivers {
		driver.Group.DistributionWeight = weightsMap[driver.Group.UUID]
	}

	return nil
}

func fillTaxiParkWeights(drivers []models.DriversApps) error {
	var neededUUIDs []string
outer:
	for _, driver := range drivers {
		taxiparkUUID := driver.GetTaxiParkUUID()
		for _, uuid := range neededUUIDs {
			if uuid == taxiparkUUID {
				continue outer
			}
		}

		neededUUIDs = append(neededUUIDs, taxiparkUUID)
	}

	weightsMap, err := getTaxiParksWeights(neededUUIDs)
	if err != nil {
		return err
	}

	for _, driver := range drivers {
		driver.TaxiParkData.DistributionWeight = weightsMap[driver.GetTaxiParkUUID()]
	}

	return nil
}

func fillServices(driver *Driver) error {
	return tool.SendRequest(
		http.MethodGet,
		fmt.Sprintf("%s/driver/%s/active_services", config.St.CRM.Host, driver.UUID),
		nil,
		nil,
		&driver.ServiceUUIDs,
	)
}

func fillFeatures(driver *Driver) error {
	return tool.SendRequest(
		http.MethodGet,
		fmt.Sprintf("%s/driver/%s/active_features", config.St.CRM.Host, driver.UUID),
		nil,
		nil,
		&driver.FeatureUUIDs,
	)
}
