package distribution

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

const (
	driverLocationLifetime = 45 * time.Second
)

func getDriversActualLocations(driverUUIDs []string) (map[string]structures.PureCoordinates, error) {
	locations, err := models.GetDriversLastLocations(driverUUIDs...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch drivers' last locations")
	}

	locationsMap := make(map[string]structures.PureCoordinates)
	for _, loc := range locations {
		// игнорируем истекшие координаты
		deadline := loc.CreatedAt.Add(driverLocationLifetime)
		if time.Now().After(deadline) {
			continue
		}

		locationsMap[loc.DriverUUID] = structures.PureCoordinates{Long: loc.Long, Lat: loc.Lat}
	}

	return locationsMap, nil
}

// TODO: refactor
func getDriversRouteDistances(drivers core.DriverSlice, dst structures.PureCoordinates) (map[string]float64, error) {
	if len(drivers) == 0 {
		return nil, nil
	}

	coordinates := make([]structures.PureCoordinates, 0, len(drivers)+1)
	coordinates = append(coordinates, dst)
	for _, d := range drivers {
		coordinates = append(coordinates, d.GetLocation())
	}

	data, err := osrm.New(config.St.OSRM.Host).Table(coordinates, coordinates[1:], coordinates[:1])
	if err != nil {
		return nil, err
	}

	result := make(map[string]float64)
	for i := range drivers {
		result[drivers[i].GetUUID()] = data.GetDistance(i, 0) + drivers[i].GetRemainingDistance()
	}

	return result, nil
}

func getDriversStraightDistances(drivers core.DriverSlice, dst structures.PureCoordinates) map[string]float64 {
	result := make(map[string]float64, len(drivers))
	for _, d := range drivers {
		driverLocation := d.GetLocation()
		result[d.GetUUID()] = gis.Distance(driverLocation.Lat, driverLocation.Long, dst.Lat, dst.Long)
	}

	return result
}
