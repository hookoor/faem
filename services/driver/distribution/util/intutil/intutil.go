package intutil

func SliceContains(slice []int, i int) bool {
	for _, el := range slice {
		if el == i {
			return true
		}
	}
	return false
}
