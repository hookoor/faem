package distribution

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"math"
)

const (
	distributionTypeGlobal = "region"
	distributionTypeLocal  = "taxipark"
)

type Match struct {
	offer      core.Offer
	driver     core.Driver
	DriverUUID string `json:"driver_uuid"`
	OfferUUID  string `json:"offer_uuid"`
	OrderUUID  string `json:"order_uuid"`
	Meta       struct {
		Rule             string                 `json:"rule"`
		Round            int                    `json:"round"`
		Radius           float64                `json:"radius"`
		Distance         float64                `json:"distance"`
		ScoringInput     *scoring.DriverScoring `json:"scoring_input"`
		ScoringResult    float64                `json:"scoring_result"`
		DistributionType string                 `json:"distribution_type"`
		Location         struct {
			Driver structures.PureCoordinates `json:"driver"`
			Order  structures.PureCoordinates `json:"order"`
		} `json:"location"`
		MatcherUUID string `json:"matcher_uuid"`
		IsCounter   bool   `json:"is_counter"`
	}
}

func MakeMatch(driver core.Driver, offer core.Offer) Match {
	m := Match{driver: driver, offer: offer}
	m.DriverUUID = driver.GetUUID()
	m.OfferUUID = offer.GetUUID()
	m.OrderUUID = offer.GetOrderUUID()
	m.Meta.Location.Driver = driver.GetLocation()
	m.Meta.Location.Order = offer.GetStartLocation()

	return m
}

func (m *Match) validateDistance() error {
	const distanceError = 0.05 // допустимая погрешность расстояния при распределении

	coords := []structures.PureCoordinates{
		m.driver.GetLocation(),
		m.offer.GetStartLocation(),
	}

	data, err := osrm.New(config.St.OSRM.Host).Route(coords)
	if err != nil {
		return errors.Wrap(err, "OSRM request failed")
	}
	if len(data.Routes) == 0 {
		return errors.New("OSRM returned zero-length route array")
	}

	distance := data.Routes[0].Distance
	expected := m.Meta.Distance

	if math.Abs(distance-expected) > expected*distanceError {
		logger.WithFields(logrus.Fields{
			"matcher_uuid":  m.Meta.MatcherUUID,
			"expected":      expected,
			"actual":        distance,
			"margin":        distanceError * expected,
			"driver_coords": coords[0],
			"offer_coords":  coords[1],
		}).Warn("distance to order mismatch")
	}

	expectedMax := m.Meta.Radius
	if expectedMax < distance && distance-expectedMax > expectedMax*distanceError {
		return errors.Errorf("wrong distance to order. expected: ≤%.0f(±%.0f%%), actual: %.0f",
			expectedMax, 100*distanceError, distance)
	}

	return nil
}

func (m *Match) AssignOfferToDriver() error {
	if err := m.validateDistance(); err != nil {
		return err
	}

	return m.offer.AssignToDriver(m.driver)
}
