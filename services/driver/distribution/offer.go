package distribution

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/util/intutil"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/util/stringutil"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"time"
)

type Offer struct {
	models.OfferDrv
	StartLocation    structures.PureCoordinates
	OutOfTown        bool
	DistributionRule core.TaxiParkRule
}

func (o *Offer) GetScoring() scoring.OfferScoring {
	return scoring.OfferScoring{
		Booster:       btoi(o.Order.Client.Promotion.Booster),
		VIP:           btoi(o.Order.Client.Promotion.IsVIP),
		VIO:           btoi(o.Order.Promotion.IsVIP),
		OrderSource:   o.Order.Source,
		Waiting:       time.Now().Sub(o.CreatedAt).Seconds(),
		IncreasedFare: o.CalcIncreasedFarePercent(),
		Activity:      float64(o.Order.Client.Activity),
		Karma:         o.Order.Client.Karma,
	}
}

func (o *Offer) GetDistributionRule() core.TaxiParkRule {
	return o.DistributionRule
}

func (o *Offer) IsOutOfTown() bool {
	return o.OutOfTown
}

func (o *Offer) GetTaxiParkUUID() string {
	return o.Order.GetTaxiParkUUID()
}

func (o *Offer) GetStartLocation() structures.PureCoordinates {
	return o.StartLocation
}

func (o *Offer) GetClientPhone() string {
	return o.Order.Client.MainPhone
}

func (o *Offer) GetPaymentType() string {
	return o.Order.PaymentType
}

func (o *Offer) GetService() structures.Service {
	return o.Order.Service
}

func (o *Offer) GetFeatures() structures.FeatureSlice {
	return o.Order.Features
}

func (o *Offer) GetState() string {
	return o.DistributionMeta.State
}

func (o *Offer) HasExceededAttemptLimit() bool {
	return o.DistributionMeta.SmartAttempts >= o.DistributionRule.Params.SmartAttemptsLimit
}

func (o *Offer) GetOfferDrv() *models.OfferDrv {
	return &o.OfferDrv
}

func (o *Offer) GetUUID() string {
	return o.UUID
}

func (o *Offer) GetOrderUUID() string {
	return o.OrderUUID
}

func (o *Offer) CalcIncreasedFarePercent() float64 {
	num := float64(o.Order.IncreasedFare)
	denom := float64(o.Order.Tariff.TotalPrice) - num
	if denom <= 1 {
		return num
	}

	return num / denom
}

func (o *Offer) HasDriverInBlacklist(driver core.Driver) bool {
	return stringutil.SliceContains(o.Order.Client.Blacklist, driver.GetUUID())
}

func (o *Offer) HasDriverInRejectedList(driver core.Driver) bool {
	if driver.GetStatus() == constants.DriverStateWorking {
		return stringutil.SliceContains(o.CounterRejectedDriversUUID, driver.GetUUID())
	}
	return stringutil.SliceContains(o.RejectedDriversUUID, driver.GetUUID())
}

func (o *Offer) TaxiParksCompatible(driver core.Driver, allowNeutral bool) bool {
	friendlyUUIDs := o.Order.TaxiParkData.FriendlyUUID
	unwantedUUIDs := o.Order.TaxiParkData.UnwantedUUID
	driverTpUUID := driver.GetTaxiParkUUID()

	// отбрасываем нежелательные таксопарки
	if stringutil.SliceContains(unwantedUUIDs, driverTpUUID) {
		return false
	}

	// сразу true, если можно распределять нейтральным, нежелательные мы уже отсекли
	if allowNeutral {
		return true
	}

	// есть ли таксопарк водителя в дружественных заказа
	return stringutil.SliceContains(friendlyUUIDs, driverTpUUID)
}

func fillTaxiParkRules(offers core.OfferSlice) error {
	var ruleIDs []int
	for _, offer := range offers {
		id := offer.GetDistributionRuleID()
		if !intutil.SliceContains(ruleIDs, id) {
			ruleIDs = append(ruleIDs, id)
		}
	}

	rules, err := GetTaxiParkRules(ruleIDs)
	if err != nil {
		return errors.Wrap(err, "failed to fetch distribution rules")
	}

	for i := range offers {
		rule := rules[offers[i].GetDistributionRuleID()]
		offers[i].SetDistributionRule(rule)
	}

	return nil
}

func (o *Offer) ChangeState(newState string, comment string) (bool, error) {
	if o.DistributionMeta.State != newState {
		return true, models.PrepareOfferForDistribution(o.OfferDrv, newState, comment)
	}
	return false, nil
}

func (o *Offer) AssignToDriver(driver core.Driver) error {
	o.OfferDrv.DriverUUID = driver.GetUUID()
	isCounter := driver.GetStatus() == constants.DriverStateWorking
	return models.AssignOfferToTheDriver(o.OfferDrv, false, isCounter)
}

func (o *Offer) SetDistributionRule(rule core.TaxiParkRule) {
	o.DistributionRule = rule
	o.DriverDistributionRule = rule.Expr
	o.ClientDistributionRule = " "
}

func (o *Offer) GetDistributionRuleID() int {
	return o.Order.TaxiParkData.DistributionRuleID
}
