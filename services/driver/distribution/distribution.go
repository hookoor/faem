package distribution

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
	"gitlab.com/faemproject/backend/faem/services/driver/rabsender"
	"golang.org/x/sync/errgroup"
	"sort"
	"sync"
)

var logger = logs.Eloger.WithField("event", "distribution")

func Start() error {
	regions, err := getRegionsFromCRM()
	if err != nil {
		return errors.Wrap(err, "failed to fetch regions from CRM")
	}

	err = fillRegionRules(regions)
	if err != nil {
		return errors.Wrap(err, "failed to fill distribution rules")
	}

	clientRule, err := getClientRule()
	if err != nil {
		return errors.Wrap(err, "failed to fetch client distribution rule")
	}

	for _, region := range regions {
		region.ClientDistributionRule = clientRule

		if _, found := distributionSync[region.Name]; !found {
			distributionSync[region.Name] = new(sync.Mutex)
		}

		go startByRegion(region)
	}

	return nil
}

// мапа мьютексов для очередности горутин распределения в рамках каждого региона
var distributionSync = make(map[string]*sync.Mutex)

func startByRegion(r *Region) {
	log := logger.WithFields(logrus.Fields{
		"region": r.Name,
	})

	distributionSync[r.Name].Lock()
	defer distributionSync[r.Name].Unlock()

	var (
		drivers core.DriverSlice
		offers  core.OfferSlice
		eg      errgroup.Group
	)

	eg.Go(func() error {
		var err error
		drivers, err = r.GetDriversForDistribution()
		if err != ErrNoDrivers {
			return err
		}
		return nil
	})
	eg.Go(func() error {
		var err error
		offers, err = r.GetOffersForDistribution()
		return err
	})
	if err := eg.Wait(); err != nil {
		log.WithError(err).Error("distribution error")
		return
	}

	log.WithFields(logrus.Fields{
		"driver_count": len(drivers),
		"offer_count":  len(offers),
	}).Info("starting distribution")
	err := distributeOffersToDrivers(offers, drivers, r)
	if err != nil && err != ErrNoDrivers {
		log.WithError(err).Error("distribution error")
	}
}

func distributeOffersToDrivers(offers core.OfferSlice, drivers core.DriverSlice, region *Region) error {
	log := logger

	offers = offers.Filter(func(o core.Offer) bool {
		// пропускаем офферы в статусе "свободный" и с достигнутым лимитом попыток назначений
		if o.HasExceededAttemptLimit() && o.GetState() == constants.OrderStateFree {
			return false
		}

		return true
	})

	// скорим офферы
	scorings := core.CreateScoringFromOffers(offers)
	scores, err := scoring.ScoreOffers(scorings, region.ClientDistributionRule.Expr)
	if err != nil {
		return errors.Wrap(err, "failed to score offers")
	}
	// сортируем по скорингу
	sort.Slice(offers, func(i, j int) bool {
		return scores[i] > scores[j]
	})

	for _, offer := range offers {
		offerLog := log.WithField("offer_uuid", offer.GetUUID())
		matcher, err := NewMatcher(offer, drivers)
		if err != nil {
			return err // вряд ли цикл продолжится без ошибок, поэтому сразу выходим
		}

		match, err := matcher.FindBestMatch(offer.GetDistributionRule(), region.DistributionRule)
		switch err {
		case ErrNoDrivers:
			noDriversLog := offerLog.WithField("reason", ErrNoDrivers)
			stateChanged, err := offer.ChangeState(constants.OrderStateFree, "Не удалось найти водителя")
			if stateChanged {
				if err != nil {
					msg := fmt.Sprintf("failed to change offer state to '%s'", constants.OrderStateFree)
					noDriversLog.WithError(err).Error(msg)
				} else {
					msg := fmt.Sprintf("changed offer state to '%s'", constants.OrderStateFree)
					noDriversLog.Info(msg)
				}
			} else {
				noDriversLog.Info("unable to distribute offer")
			}
			continue
		case nil:
			break
		default:
			// неизвестная ошибка (err != nil)
			offerLog.WithError(err).Error()
			continue
		}

		offerLog = offerLog.WithField("driver_uuid", match.DriverUUID)
		drivers = drivers.Without(match.driver)

		go func() { // само назначение в отдельной горутине
			if err := match.AssignOfferToDriver(); err != nil {
				offerLog.WithError(err).Error("failed to assign offer")
			} else {
				offerLog.Info("successfully assigned offer")
				// микролог распределения
				rabsender.SendJSONByAction(rabsender.AcDistributionResult, "", match)
				models.DistributionLogs <- models.NewAssignLog(
					match.DriverUUID, match.OfferUUID, match.OrderUUID,
					match.Meta.IsCounter,
					match.Meta.Round, match.Meta.Radius, match.Meta.Distance,
				)
			}
		}()
	}

	return nil
}
