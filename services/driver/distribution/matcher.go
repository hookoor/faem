package distribution

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"sort"
)

type Matcher struct {
	Offer             core.Offer
	Drivers           core.DriverSlice
	StraightDistances map[string]float64 // мапа ПРЯМЫХ (не по дорогам) расстояний между водителями и заказом
	UUID              string             // уникальный идентификатор, может быть нужен для отслеживания в логах
}

func NewMatcher(offer core.Offer, drivers core.DriverSlice) (*Matcher, error) {
	var m Matcher

	m.UUID = structures.GenerateUUID()
	m.Offer = offer

	// первичная фильтрация по услугам, опциям, балансу и т.д.
	m.Drivers = drivers.Filter(func(d core.Driver) bool {
		if err := d.CheckSuitableForOffer(offer); err != nil {
			return false
		}

		return true
	})

	m.StraightDistances = getDriversStraightDistances(m.Drivers, offer.GetStartLocation())
	// сортируем водителей по прямым расстояниям для упрощения подбора по радиусам в дальнейшем
	sort.Slice(m.Drivers, func(i, j int) bool {
		iUUID := m.Drivers[i].GetUUID()
		jUUID := m.Drivers[j].GetUUID()
		return m.StraightDistances[iUUID] < m.StraightDistances[jUUID]
	})

	return &m, nil
}

func (m *Matcher) FindBestMatch(localRule core.TaxiParkRule, globalRule core.RegionRule) (*Match, error) {
	var (
		match *Match
		err   error
	)

	match, err = m.MatchByTaxiPark(localRule)
	switch err {
	case nil:
		return match, nil
	case ErrNoDrivers:
		break
	default:
		return nil, err
	}

	match, err = m.MatchByRegion(globalRule)
	switch err {
	case nil:
		return match, nil
	case ErrNoDrivers:
		break
	default:
		return nil, err
	}

	return nil, ErrNoDrivers
}

func (m *Matcher) MatchByTaxiPark(rule core.TaxiParkRule) (*Match, error) {
	// фильтруем водителей по таксопарку
	drivers := m.Drivers.Filter(func(d core.Driver) bool {
		driverTpUUID := d.GetTaxiParkUUID()
		offerTpUUID := m.Offer.GetTaxiParkUUID()

		return driverTpUUID == offerTpUUID
	})

	// сначала пробуем обычное назначение
	onlineDrivers := drivers.Filter(func(d core.Driver) bool {
		return d.GetStatus() == constants.DriverStateOnline
	})
	match, err := m.match(onlineDrivers, rule.Expr, rule.Params.Rounds, rule.Params.OutOfTownMultiplier)
	if err == ErrNoDrivers {
		// если не нашли водителя, пробуем встречное назначение
		workingDrivers := drivers.Filter(func(d core.Driver) bool {
			return d.GetStatus() == constants.DriverStateWorking
		})
		match, err = m.match(workingDrivers, rule.Expr, rule.Params.Rounds, rule.Params.OutOfTownMultiplier)
		match.Meta.IsCounter = true
	}
	if err != nil {
		return nil, err
	}

	match.Meta.DistributionType = distributionTypeLocal

	return &match, nil
}

func (m *Matcher) MatchByRegion(rule core.RegionRule) (*Match, error) {
	// фильтруем водителей по таксопарку
	drivers := m.Drivers.Filter(func(d core.Driver) bool {
		return m.Offer.TaxiParksCompatible(d, rule.Params.AllowNeutralTaxiParks)
	})

	// сначала пробуем обычное назначение
	onlineDrivers := drivers.Filter(func(d core.Driver) bool {
		return d.GetStatus() == constants.DriverStateOnline
	})
	match, err := m.match(onlineDrivers, rule.Expr, rule.Params.Rounds, rule.Params.OutOfTownMultiplier)
	if err == ErrNoDrivers {
		// если не нашли водителя, пробуем встречное назначение
		workingDrivers := drivers.Filter(func(d core.Driver) bool {
			return d.GetStatus() == constants.DriverStateWorking
		})
		match, err = m.match(workingDrivers, rule.Expr, rule.Params.Rounds, rule.Params.OutOfTownMultiplier)
		match.Meta.IsCounter = true
	}
	if err != nil {
		return nil, err
	}

	match.Meta.DistributionType = distributionTypeGlobal

	return &match, nil
}

func (m *Matcher) match(drivers core.DriverSlice, rule string, rounds []float64, outOfTownMultiplier float64) (Match, error) {
	if len(drivers) == 0 {
		return Match{}, ErrNoDrivers
	}

	// ищем водителей во всех раундах распределения
	offerLocation := m.Offer.GetStartLocation()
	prevRadius := 0.0
	for i, distributionRadius := range rounds {
		if m.Offer.IsOutOfTown() {
			distributionRadius *= outOfTownMultiplier
		}

		// Сначала отсеиваем водителей, чья прямая дистанция до заказа превышает радиус распределения.
		// Дистанция по прямой меньше либо равна расстоянию по дорогам, поэтому в этот слайс уж точно
		// попадут все водители чье расстояние по дорогам входит в радиус распределения.
		// Это делается чтобы сузить круг водителей для запроса в OSRM
		radiusWithError := distributionRadius * 1.05 // на всякий случай с погрешностью 5%
		roundDrivers := drivers.FilterFast(func(d core.Driver) bool {
			return m.StraightDistances[d.GetUUID()] < radiusWithError
		})

		// грузим расстояния по дорогам от водителей до заказа
		routeDistances, err := getDriversRouteDistances(roundDrivers, offerLocation)
		if err != nil {
			return Match{}, err
		}

		// оставляем только водителей в радиусе текущего раунда
		driversInRadius := roundDrivers.Filter(func(d core.Driver) bool {
			distance := routeDistances[d.GetUUID()]
			return prevRadius <= distance && distance <= distributionRadius
		})

		// убираем из общего пула водителей с этого раунда
		drivers = drivers.Filter(func(d core.Driver) bool {
			return !driversInRadius.Contains(d)
		})

		// находим лучшего водителя в этом раунде
		bestMatch, err := m.findBestDriver(driversInRadius, routeDistances, rule)
		if err == ErrNoDrivers {
			// если не нашли водителя -- переходим к следующему раунду
			prevRadius = distributionRadius
			continue
		}
		if err != nil {
			return Match{}, err
		}

		bestMatch.Meta.Round = i
		bestMatch.Meta.Radius = distributionRadius

		return bestMatch, nil
	}

	return Match{}, ErrNoDrivers
}

func (m *Matcher) findBestDriver(drivers core.DriverSlice, distances map[string]float64, scoringRule string) (Match, error) {
	switch len(drivers) {
	case 0:
		return Match{}, ErrNoDrivers
	case 1:
		driver := drivers[0]
		match := MakeMatch(driver, m.Offer)
		match.Meta.Rule = scoringRule
		match.Meta.Distance = distances[driver.GetUUID()]
		match.Meta.MatcherUUID = m.UUID

		return match, nil
	}

	scorings := core.CreateScoringFromDrivers(drivers, distances)
	scores, err := scoring.ScoreDrivers(scorings, scoringRule)
	if err != nil {
		return Match{}, errors.Wrap(err, "failed to score drivers")
	}

	bestIndex := 0
	bestScore := scores[bestIndex]
	for i := range drivers {
		if scores[i] > bestScore {
			bestScore = scores[i]
			bestIndex = i
		}
	}

	match := MakeMatch(drivers[bestIndex], m.Offer)
	match.Meta.Rule = scoringRule
	match.Meta.Distance = distances[drivers[bestIndex].GetUUID()]
	match.Meta.ScoringInput = &scorings[bestIndex]
	match.Meta.ScoringResult = scores[bestIndex]
	match.Meta.MatcherUUID = m.UUID

	return match, nil
}
