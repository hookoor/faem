package distribution

import (
	"fmt"
	"net/http"
	"net/url"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

func getRegionsFromCRM() ([]*Region, error) {
	var regions []*Region
	url := fmt.Sprintf("%s/%s/regions_for_distribution", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &regions)

	return regions, err
}

func getClientRule() (core.Rule, error) {
	var rule core.Rule
	url := fmt.Sprintf("%s/%s/distribution_rules/client", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &rule)

	return rule, err
}

func getTaxiParksWeights(uuids []string) (map[string]int, error) {
	var body struct {
		UUID []string `json:"uuid"`
	}
	body.UUID = uuids

	result := make(map[string]int)
	url := fmt.Sprintf("%s/%s/taxiparks/weights_map", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, body, &result)

	return result, err
}

func getDriverGroupsWeights(uuids []string) (map[string]int, error) {
	var body struct {
		UUID []string `json:"uuid"`
	}
	body.UUID = uuids

	result := make(map[string]int)
	url := fmt.Sprintf("%s/%s/drivergroup/weights_map", config.St.CRM.Host, constants.InternalGroup)
	err := tool.SendRequest(http.MethodGet, url, nil, body, &result)

	return result, err
}

func getDriversFinishLocations(uuids []string) (map[string]structures.PureCoordinates, error) {
	endpoint := config.St.CRM.Host + "/orders/current/drivers?"
	params := make(url.Values)
	for _, uuid := range uuids {
		params.Add("driver_uuid", uuid)
	}
	endpoint += params.Encode()

	orderUUIDsMap := make(map[string]string)
	err := tool.SendRequest(http.MethodGet, endpoint, nil, nil, &orderUUIDsMap)
	if err != nil {
		return nil, err
	}

	orderUUIDs := make([]string, len(orderUUIDsMap))
	for _, uuid := range orderUUIDsMap {
		orderUUIDs = append(orderUUIDs, uuid)
	}
	offers, err := models.GetOffersByOrdersUUID(orderUUIDs)
	if err != nil {
		return nil, err
	}

	result := make(map[string]structures.PureCoordinates, len(offers))
	for _, o := range offers {
		finishPoint := o.TaximetrData.Order.Routes[len(o.TaximetrData.Order.Routes)-1]
		result[o.DriverUUID] = structures.PureCoordinates{
			Lat:  float64(finishPoint.Lat),
			Long: float64(finishPoint.Lon),
		}
	}

	return result, err
}
