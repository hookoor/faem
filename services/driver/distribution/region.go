package distribution

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

const (
	minDriverWaitTime = 4
)

var ErrNoDrivers = errors.New("no available drivers")
var ErrNoOffers = errors.New("no available offers")

type Region struct {
	structures.Region
	DistributionRule       core.RegionRule
	ClientDistributionRule core.Rule
}

func (r *Region) GetDriversForDistribution() (core.DriverSlice, error) {
	// получаем мапу с инфой о водителях в зависимости от их статуса:
	//  - online: время с момента начала онлайна
	//  - working: расстояние до конечной точки текущего заказа
	driversMap, err := GetDriversForDistribution(r.ID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch drivers")
	}

	if len(driversMap) == 0 {
		return nil, ErrNoDrivers
	}

	driverUUIDs := make([]string, 0, len(driversMap))
	for uuid := range driversMap {
		driverUUIDs = append(driverUUIDs, uuid)
	}

	// получаем всех водителей из БД
	driverModels, err := models.GetDriversByUUIDs(driverUUIDs...)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch drivers by UUIDs")
	}

	// заполняем тарифы всем водителям, понадобится чтобы понять достаточно ли баланса для принятия заказа
	err = models.FillTariffMarkups(driverModels)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fill tariff markups for drivers")
	}

	// получаем актуальные статусы из CRM
	statusesMap, err := models.GetCurrentDriversStatesFromCRM(driverUUIDs)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch current driver statuses from CRM")
	}

	// получаем последние координаты водителей (за исключением истекших)
	locationMap, err := getDriversActualLocations(driverUUIDs)
	if err != nil {
		return nil, err
	}

	// подгружаем конечные точки текущих заказов водителей (по сути для водителей в статусе 'working')
	finishLocationsMap, err := getDriversFinishLocations(driverUUIDs)
	if err != nil {
		return nil, err
	}

	// заполняем веса таксопарков
	if err := fillTaxiParkWeights(driverModels); err != nil {
		return nil, errors.Wrap(err, "failed to fill taxi parks distribution weight for drivers")
	}

	// заполняем веса групп
	if err := fillGroupWeights(driverModels); err != nil {
		return nil, errors.Wrap(err, "failed to fill group distribution weight for drivers")
	}

	drivers := make(core.DriverSlice, 0, len(driverModels))
	for _, driverModel := range driverModels {
		driver := Driver{DriversApps: driverModel}

		var found bool
		driver.Status, found = statusesMap[driver.UUID]
		if !found {
			continue
		}
		switch driver.Status {
		case constants.DriverStateOnline:
			driver.WaitTime, found = driversMap[driver.UUID]
			if !found {
				continue
			}
			driver.Location, found = locationMap[driver.UUID]
			if !found {
				continue
			}
		case constants.DriverStateWorking:
			driver.RemainingDistance, found = driversMap[driver.UUID]
			if !found {
				continue
			}
			finishPoint, found := finishLocationsMap[driver.UUID]
			if !found {
				continue
			}
			// для занятых водителей за местоположение берем конечную точку текущего заказа
			driver.Location = finishPoint
		default:
			// ненужный статус
			continue
		}

		{ // костыль для подгрузки опций и услуг водителей
			fillFeatures(&driver)
			fillServices(&driver)
		}

		drivers = append(drivers, &driver)
	}

	return drivers, nil
}

func (r *Region) GetOffersForDistribution() (core.OfferSlice, error) {
	availableOffers, err := models.GetRegionalOffersByState(r.ID, false, constants.OrderStateFree, constants.OrderStateDistributing)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch regional offers by state")
	}
	if len(availableOffers) == 0 {
		return nil, ErrNoOffers
	}

	err = models.FillOrderInOffers(availableOffers)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch orders data for offers")
	}

	err = models.FillTaxiParksInOffers(availableOffers)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch taxiparks data for offers")
	}

	offers := make(core.OfferSlice, 0, len(availableOffers))
	for _, offr := range availableOffers {
		startRoute := offr.Order.Routes[0]
		offer := Offer{
			OfferDrv: offr,
			StartLocation: structures.PureCoordinates{
				Lat:  float64(startRoute.Lat),
				Long: float64(startRoute.Lon),
			},
			OutOfTown: startRoute.OutOfTown,
		}

		offers = append(offers, &offer)
	}
	if len(offers) == 0 {
		return nil, ErrNoOffers
	}

	err = fillTaxiParkRules(offers)
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch distribution rules")
	}

	return offers, nil
}

func fillRegionRules(regions []*Region) error {
	var ruleIDs []int
	for _, region := range regions {
		ruleIDs = append(ruleIDs, region.DistributionRuleID)
	}

	rules, err := GetRegionRules(ruleIDs)
	if err != nil {
		return errors.Wrap(err, "failed to fetch distribution rules")
	}

	for i := range regions {
		regions[i].DistributionRule = rules[regions[i].DistributionRuleID]
	}

	return nil
}
