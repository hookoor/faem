package distribution

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/driver/config"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
)

const (
	MaxDistributionRadius             = 10000
	endpointGetDistributionRulesByIDs = "/" + constants.InternalGroup + "/distribution_rules"
)

func GetRegionRules(ids []int) (map[int]core.RegionRule, error) {
	rules := make([]core.RegionRule, 0, len(ids))
	err := getDistributionRules(ids, &rules)

	result := make(map[int]core.RegionRule, len(rules))
	for _, rule := range rules {
		result[rule.ID] = rule
	}

	return result, err
}

func GetTaxiParkRules(ids []int) (map[int]core.TaxiParkRule, error) {
	rules := make([]core.TaxiParkRule, 0, len(ids))
	err := getDistributionRules(ids, &rules)

	result := make(map[int]core.TaxiParkRule, len(rules))
	for _, rule := range rules {
		result[rule.ID] = rule
	}

	return result, err
}

func getDistributionRules(ids []int, resultArr interface{}) error {
	var body struct {
		IDs []int `json:"ids"`
	}

	body.IDs = ids
	url := config.St.CRM.Host + endpointGetDistributionRulesByIDs
	err := tool.SendRequest(http.MethodPost, url, nil, body, resultArr)

	return err
}
