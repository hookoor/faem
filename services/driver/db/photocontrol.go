package db

import (
	"context"
	"fmt"
	"time"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// IModelPhotocontrol -
type IModelPhotocontrol interface {

	// получить список снимков-шаблонов
	GetPhotocontrolSnapshotSketchesList(ctx context.Context) ([]models.SnapshotSketch, error)
	// получить снимк-шаблон по uuid
	GetPhotocontrolSnapshotSketchesByUUID(ctx context.Context, uuid string) (models.SnapshotSketch, error)
	// создать снимк-шаблон
	CreatePhotocontrolSnapshotSketch(ctx context.Context, snapshotSketch *models.SnapshotSketch) (models.SnapshotSketch, error)
	// обновить снимк-шаблон
	UpdatePhotocontrolSnapshotSketch(ctx context.Context, uuid string, snapshotSketch *models.SnapshotSketch) (models.SnapshotSketch, error)
	// удалить снимк-шаблон
	DeletePhotocontrolSnapshotSketch(ctx context.Context, uuid string) error

	// ------
	// шаблон фотоконтроля
	// шаблон фотоконтроля применим группам водителей

	// получить список шаблонов
	GetPhotocontrolSketchesList(ctx context.Context) ([]models.PhotocontrolSketch, error)
	// получить шаблон по uuid
	GetPhotocontrolSketchByUUID(ctx context.Context, uuid string) (models.PhotocontrolSketch, error)
	// создать шаблон
	CreatePhotocontrolSketch(ctx context.Context, photoSketch *models.PhotocontrolSketch) (models.PhotocontrolSketch, error)
	// обновить шаблон
	UpdatePhotocontrolSketch(ctx context.Context, uuid string, photoSketch *models.PhotocontrolSketch) (models.PhotocontrolSketch, error)
	// удалить шаблон
	DeletePhotocontrolSketch(ctx context.Context, uuid string) error

	// ------

	// получить список тикетов фотоконтроля для водителя
	GetPhotocontrolDriverTicketsList(ctx context.Context, driveruuiduuid string) ([]models.Photocontrol, error)
	// получить фильтрованный список тикетов фотоконтроля
	GetPhotocontrolTicketsList(ctx context.Context, filter models.PhotocontrolTicketsFilter) ([]models.Photocontrol, int, error)
	// получение фотоконтроля по uuid
	GetPhotocontrolTicketByUUID(ctx context.Context, uuid string) (models.Photocontrol, error)
	// создать тикет фотоконтроля
	CreatePhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol) error
	// загрузка фоток водителем
	PassingPhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol) error
	// продление фотоконтроля
	PhotocontrolPostponement(ctx context.Context, uuid string, newTimeValidity int64) error
	// обновление тикета фотоконтроля по совместительству процесс модерации.
	UpdatePhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol, uuid string) error
	// удалить тикет фотоконтроля
	DeletePhotocontrolTicket(ctx context.Context, uuid string) error

	// ------
	// SettingsUpdate

	// изменени переодичности создания нового фотоконтроля
	PeridisityUpdate(ctx context.Context, uuid string, seconds int64) error

	// ------

	// вызывается при перехде водителя в группу
	CreateByDriverGroupTemplate(ctx context.Context, driverGroupUUID string, driver models.PhotocontrolForDriver, ps *models.PhotocontrolSketch) (models.Photocontrol, error)
	// удаляет с водителя фотокнтроли которые есть в группе
	DeletePhotocontrolsByDriverGroup(ctx context.Context, driverUUID string) error
}

// ModelPhotocontrol -
type ModelPhotocontrol struct {
	DB *pg.DB
}

// --------------------------------------
// --------------------------------------
// --------------------------------------

// GetPhotocontrolSnapshotSketchesList -
func (p *ModelPhotocontrol) GetPhotocontrolSnapshotSketchesList(ctx context.Context) ([]models.SnapshotSketch, error) {
	var res []models.SnapshotSketch

	err := p.DB.ModelContext(ctx, &res).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}
	return res, nil
}

// GetPhotocontrolSnapshotSketchesByUUID -
func (p *ModelPhotocontrol) GetPhotocontrolSnapshotSketchesByUUID(ctx context.Context, uuid string) (models.SnapshotSketch, error) {
	var res models.SnapshotSketch

	err := p.DB.ModelContext(ctx, &res).
		Where("uuid = ?", uuid).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}
	return res, nil
}

// CreatePhotocontrolSnapshotSketch -
func (p *ModelPhotocontrol) CreatePhotocontrolSnapshotSketch(ctx context.Context, snapshotSketch *models.SnapshotSketch) (models.SnapshotSketch, error) {

	err := validateSnapshotSketch(*snapshotSketch)
	if err != nil {
		return *snapshotSketch, errpath.Err(err)
	}

	ex, err := p.DB.ModelContext(ctx, snapshotSketch).
		Where("deleted is not true").
		Where("name = ?", snapshotSketch.Name).
		Exists()
	if err != nil {
		return *snapshotSketch, errpath.Err(err)
	}
	if ex {
		return *snapshotSketch, errpath.Errorf("SnapshotSketch by name %s exist", snapshotSketch.Name)
	}

	snapshotSketch.UUID = structures.GenerateUUID()

	_, err = p.DB.ModelContext(ctx, snapshotSketch).Insert()
	if err != nil {
		return *snapshotSketch, errpath.Err(err)
	}

	return *snapshotSketch, nil
}

// UpdatePhotocontrolSnapshotSketch -
func (p *ModelPhotocontrol) UpdatePhotocontrolSnapshotSketch(ctx context.Context, uuid string, snapshotSketch *models.SnapshotSketch) (models.SnapshotSketch, error) {

	err := validateSnapshotSketch(*snapshotSketch)
	if err != nil {
		return *snapshotSketch, errpath.Err(err)
	}

	_, err = p.DB.ModelContext(ctx, snapshotSketch).Where("uuid = ?", uuid).Returning("*").UpdateNotNull()
	if err != nil {
		return *snapshotSketch, errpath.Err(err)
	}

	return *snapshotSketch, nil
}

// DeletePhotocontrolSnapshotSketch -
func (p *ModelPhotocontrol) DeletePhotocontrolSnapshotSketch(ctx context.Context, uuid string) error {

	_, err := p.DB.ModelContext(ctx, (*models.SnapshotSketch)(nil)).
		Where("uuid = ?", uuid).
		Set("deleted = ?", true).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

func validateSnapshotSketch(snapshotSketch models.SnapshotSketch) error {
	if snapshotSketch.Name == "" {
		return errpath.Errorf("empty snapshotSketch name")
	}

	return nil
}

// --------------------------------------
// --------------------------------------
// --------------------------------------

// GetPhotocontrolSketchesList -
func (p *ModelPhotocontrol) GetPhotocontrolSketchesList(ctx context.Context) ([]models.PhotocontrolSketch, error) {
	var res []models.PhotocontrolSketch

	err := p.DB.ModelContext(ctx, &res).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

// GetPhotocontrolSketchByUUID -
func (p *ModelPhotocontrol) GetPhotocontrolSketchByUUID(ctx context.Context, uuid string) (models.PhotocontrolSketch, error) {
	var res models.PhotocontrolSketch

	err := p.DB.ModelContext(ctx, &res).
		Where("uuid = ?", uuid).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

// CreatePhotocontrolSketch -
func (p *ModelPhotocontrol) CreatePhotocontrolSketch(ctx context.Context, photoSketch *models.PhotocontrolSketch) (models.PhotocontrolSketch, error) {

	err := validateSketch(*photoSketch)
	if err != nil {
		return *photoSketch, errpath.Err(err)
	}

	ex, err := p.DB.ModelContext(ctx, photoSketch).
		Where("deleted is not true").
		Where("name = ?", photoSketch.Name).
		Exists()
	if err != nil {
		return *photoSketch, errpath.Err(err)
	}
	if ex {
		return *photoSketch, errpath.Errorf("PhotocontrolSketch by name '%s' exist", photoSketch.Name)
	}

	photoSketch.UUID = structures.GenerateUUID()

	_, err = p.DB.ModelContext(ctx, photoSketch).Insert()
	if err != nil {
		return *photoSketch, errpath.Err(err)
	}

	return *photoSketch, nil
}

// UpdatePhotocontrolSketch -
func (p *ModelPhotocontrol) UpdatePhotocontrolSketch(ctx context.Context, uuid string, photoSketch *models.PhotocontrolSketch) (models.PhotocontrolSketch, error) {

	err := validateSketch(*photoSketch)
	if err != nil {
		return *photoSketch, errpath.Err(err)
	}

	_, err = p.DB.ModelContext(ctx, photoSketch).Where("uuid = ?", uuid).Returning("*").UpdateNotNull()
	if err != nil {
		return *photoSketch, errpath.Err(err)
	}

	return *photoSketch, nil
}

// DeletePhotocontrolSketch -
func (p *ModelPhotocontrol) DeletePhotocontrolSketch(ctx context.Context, uuid string) error {

	_, err := p.DB.ModelContext(ctx, (*models.PhotocontrolSketch)(nil)).
		Where("uuid = ?", uuid).
		Set("deleted = ?", true).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

func validateSketch(photoSketch models.PhotocontrolSketch) error {
	if photoSketch.Name == "" {
		return errpath.Errorf("empty sketch name")
	}

	return nil
}

// --------------------------------------
// --------------------------------------
// --------------------------------------

// GetPhotocontrolDriverTicketsList - список для водителя
func (p *ModelPhotocontrol) GetPhotocontrolDriverTicketsList(ctx context.Context, driveruuid string) ([]models.Photocontrol, error) {
	var res []models.Photocontrol

	err := p.DB.ModelContext(ctx, &res).
		Where("deleted is not true").
		Where("driver ->> 'uuid' = ?", driveruuid).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			q.WhereOr("status = ?", models.PhotocontrolCreated)
			q.WhereOr("status = ?", models.PhotocontrolModeration)
			q.WhereOr("status = ?", models.PhotocontrolRejected)
			return q, nil
		}).
		Order("name", "created_at").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

// GetPhotocontrolTicketsList -
func (p *ModelPhotocontrol) GetPhotocontrolTicketsList(ctx context.Context, filter models.PhotocontrolTicketsFilter) ([]models.Photocontrol, int, error) {
	var res []models.Photocontrol

	q := p.DB.ModelContext(ctx, &res).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.DriverUUID != "" {
				q.Where("driver ->> 'uuid' = ?", filter.DriverUUID)
			}
			if filter.Alias != "" {
				q.Where("driver ->> 'alias' = ?", filter.Alias)
			}
			if filter.DriverName != "" {
				q.Where("driver ->> 'name' = ?", filter.DriverName)
			}

			if filter.Name != "" {
				q.Where("name = ?", filter.Name)
			}
			if filter.Title != "" {
				q.Where("title = ?", filter.Title)
			}
			if filter.Status != "" {
				q.Where("status = ?", filter.Status)
			}
			if !filter.CreatedAt.IsZero() {
				q.Where("created_at = ?", filter.CreatedAt)
			}
			if len(filter.AllowedTaxiParks) != 0 {
				q.WhereIn("taxi_park_uuid IN (?)", filter.AllowedTaxiParks)
			}
			return q, nil
		}).
		Where("deleted is not true").
		Order("created_at").
		Apply(filter.Pager.Pagination)

	count, err := q.Count()
	if err != nil {
		return res, 0, errpath.Err(err)
	}

	err = q.Select()
	if err != nil {
		return res, 0, errpath.Err(err)
	}

	return res, count, nil
}

// GetPhotocontrolTicketByUUID -
func (p *ModelPhotocontrol) GetPhotocontrolTicketByUUID(ctx context.Context, uuid string) (models.Photocontrol, error) {
	var res models.Photocontrol

	err := p.DB.ModelContext(ctx, &res).
		Where("uuid = ?", uuid).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

// CreatePhotocontrolTicket - сохранение тикета фотоконтроля с набором необжодимых фотографий
func (p *ModelPhotocontrol) CreatePhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol) error {
	err := validatePcontrol(*phototrolTicket)
	if err != nil {
		return errpath.Err(err)
	}

	phototrolTicket.UUID = structures.GenerateUUID()
	phototrolTicket.Status = models.PhotocontrolCreated
	phototrolTicket.Postponement = append(phototrolTicket.Postponement, models.Postponement{Count: 0, Time: phototrolTicket.TimeValidity})
	phototrolTicket.CreatedAtUnix = time.Now().Unix()
	phototrolTicket.OriginSettings = phototrolTicket.Settings

	for i := range phototrolTicket.SnapshotSketches {
		phototrolTicket.SnapshotSketches[i].Status = models.PhotocontrolCreated
	}

	_, err = p.DB.ModelContext(ctx, phototrolTicket).Insert()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// PassingPhotocontrolTicket -
func (p *ModelPhotocontrol) PassingPhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol) error {

	_, err := p.DB.ModelContext(ctx, phototrolTicket).
		Where("uuid = ?uuid").
		Set("snapshot_sketches = ?snapshot_sketches").
		Set("status = ?status").
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// PhotocontrolPostponement -
func (p *ModelPhotocontrol) PhotocontrolPostponement(ctx context.Context, uuid string, newTimeValidity int64) error {

	pcontrol, err := p.GetPhotocontrolTicketByUUID(ctx, uuid)
	if err != nil {
		return errpath.Err(err)
	}

	if pcontrol.TimeValidity >= newTimeValidity {
		return errpath.Errorf("new time validity <= current validity time")
	}

	pcontrol.TimeValidity = newTimeValidity
	pcontrol.Postponement = append(pcontrol.Postponement, models.Postponement{Count: len(pcontrol.Postponement), Time: newTimeValidity})

	_, err = p.DB.ModelContext(ctx, &pcontrol).
		Where("uuid = ?uuid").
		Set("time_validity = ?time_validity").
		Set("postponement = ?postponement").
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// UpdatePhotocontrolTicket -
func (p *ModelPhotocontrol) UpdatePhotocontrolTicket(ctx context.Context, phototrolTicket *models.Photocontrol, uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}

	_, err := p.DB.ModelContext(ctx, phototrolTicket).
		Where("uuid = ?", uuid).
		Set("snapshot_sketches = ?snapshot_sketches").
		Set("status = ?status").
		Set("time_validity = ?time_validity").
		Returning("*").
		UpdateNotNull()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// DeletePhotocontrolTicket -
func (p *ModelPhotocontrol) DeletePhotocontrolTicket(ctx context.Context, uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}
	_, err := p.DB.ModelContext(ctx, (*models.Photocontrol)(nil)).
		Where("uuid = ?", uuid).
		Set("deleted = ?", true).
		Set("status = ?", structures.ExpiredPhotoControlState).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

func validatePcontrol(pcontrol models.Photocontrol) error {
	if pcontrol.Name == "" {
		return errpath.Errorf("photocontrol is empty")
	}
	if pcontrol.Driver.UUID == "" {
		return errpath.Errorf("driver uuid is empty")
	}

	{ // pcontrol settings validate
		if pcontrol.Settings.Offspring.Enable {
			if pcontrol.Settings.Offspring.Periodicity == 0 {
				return errpath.Errorf("PeriodicityCreate.Periodicity is empty")
			}
		}
	}

	return nil
}

// --------------------------------------
// --------------------------------------
// --------------------------------------

// PeridisityUpdate -
func (p *ModelPhotocontrol) PeridisityUpdate(ctx context.Context, uuid string, seconds int64) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}

	_, err := p.DB.ModelContext(ctx, (*models.Photocontrol)(nil)).
		Where("uuid = ?", uuid).
		Set(fmt.Sprintf("settings = jsonb_set(COALESCE(settings, '{}'), '{offspring, periodicity}', '%v')", seconds)).
		Set(fmt.Sprintf("origin_settings = jsonb_set(COALESCE(origin_settings, '{}'), '{offspring, periodicity}', '%v')", seconds)).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// --------------------------------------
// --------------------------------------
// --------------------------------------

// DriverGroupCreator -
const DriverGroupCreator string = "driver_group"

// CreateByDriverGroupTemplate -
func (p *ModelPhotocontrol) CreateByDriverGroupTemplate(ctx context.Context, driverGroupUUID string, driver models.PhotocontrolForDriver, ps *models.PhotocontrolSketch) (models.Photocontrol, error) {
	var pc models.Photocontrol
	pc.Creator.Name = DriverGroupCreator
	pc.Creator.UUID = driverGroupUUID
	pc.Driver = driver

	pc.Phototrol = ps.Phototrol
	timeNow := time.Now().Unix()
	pc.ActivateTime = timeNow + ps.StartActivateTime
	pc.TimeValidity = timeNow + ps.StartTimeValidity
	pc.WarningTime = timeNow + ps.StartWarningTime
	pc.Settings = ps.Settings

	err := p.CreatePhotocontrolTicket(ctx, &pc)
	if err != nil {
		return pc, errpath.Err(err)
	}

	return pc, nil
}

// DeletePhotocontrolsByDriverGroup -
func (p *ModelPhotocontrol) DeletePhotocontrolsByDriverGroup(ctx context.Context, driverUUID string) error {

	pcs, err := p.GetPhotocontrolDriverTicketsList(ctx, driverUUID)
	if err != nil {
		return errpath.Err(err)
	}

	for _, pc := range pcs {
		if pc.Creator.Name == DriverGroupCreator {
			err = p.DeletePhotocontrolTicket(ctx, pc.UUID)
			if err != nil {
				return errpath.Err(err)
			}
		}
	}

	return nil
}
