package db

import (
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/driver/config"

	pg "github.com/go-pg/pg"
)

var (
	modelsList []interface{}
)

// DB -
type DB struct {
	ModelPhotocontrol IModelPhotocontrol
}

func init() {
	// modelsList = append(modelsList, (*models.RegisterSession)(nil))
	// modelsList = append(modelsList, (*models.DriversApps)(nil))
	// modelsList = append(modelsList, (*models.JWTSessions)(nil))
	// modelsList = append(modelsList, (*models.DriverStatesDrv)(nil))
	// modelsList = append(modelsList, (*locationsaver.LocationData)(nil))
	// modelsList = append(modelsList, (*offers.OfferDrv)(nil))
	// modelsList = append(modelsList, (*offers.OfferStatesDrv)(nil))
	// modelsList = append(modelsList, (*offers.OfferStatesDrv)(nil))
	// modelsList = append(modelsList, (*offers.OrderDrv)(nil))
}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	logs.Eloger.Trace(q.FormattedQuery())
}

// Connect return DB connection
func Connect() (*pg.DB, error) {

	var conn *pg.DB

	addr := fmt.Sprintf("%s:%v", config.St.Database.Host, config.St.Database.Port)

	conn = pg.Connect(&pg.Options{
		Addr:     addr,
		User:     config.St.Database.User,
		Password: config.St.Database.Password,
		Database: config.St.Database.Db,
	})
	var n int
	conn.AddQueryHook(dbLogger{})

	_, err := conn.QueryOne(pg.Scan(&n), "SELECT 1")
	if err != nil {
		return conn, fmt.Errorf("Error conecting to DB. Host: %s, user: %s, db: %s", config.St.Database.Host, config.St.Database.User, config.St.Database.Db)
	}

	// if err := createSchema(conn); err != nil {
	// 	return conn, fmt.Errorf("Error creating DB schemas. %v", err)
	// }
	return conn, nil
}

// CloseDbConnection closing connection for defer in main
func CloseDbConnection(db *pg.DB) {
	db.Close()
}

// func createSchema(db *pg.DB) error {
// 	logrus.Info("Creatind tables if not exist...")
// 	for _, m := range modelsList {
// 		err := db.CreateTable(m, &orm.CreateTableOptions{
// 			IfNotExists:   true,
// 			FKConstraints: false,
// 		})
// 		if err != nil {
// 			return err
// 		}
// 	}
// 	return nil
// }
