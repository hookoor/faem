package db

import (
	"context"
	"time"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

type Repository struct {
	DB *pg.DB
}

func (p *Repository) UpdatePhotoControl(ctx context.Context, photoControl *models.PhotoControl) error {
	_, err := p.DB.Model(photoControl).Where("control_id = ?control_id").UpdateNotNull()
	if err != nil {
		return errors.New("ошибка обновления данных о фотоконтроле")
	}
	return nil
}

func (p *Repository) CreateOrGetPhotocontrol(
	ctx context.Context, driverID string, controlType *models.ControlType) (models.PhotoControl, error) {
	var result models.PhotoControl
	query := p.DB.ModelContext(ctx, &result).
		Where("driver_id = ?", driverID).
		Where("approved IS FALSE").
		Where("control_type = ?", controlType.Name).
		Where("created_at > ?", time.Now().Add(-controlType.PassTimeout)).
		Order("created_at DESC")
	check, err := query.
		Exists()
	if err != nil {
		return result, errors.Wrap(err, "Ошибка проверки существования последнего фотоконтроля в БД")
	}
	if check {
		err = query.
			Limit(1).
			Select()
		if err != nil {
			return result, errors.Wrap(err, "Ошибка чтения последнего фотоконтроля из БД")
		}
		return result, nil
	}
	newControl := models.PhotoControl{}
	newControl.ControlID = uuid.Must(uuid.NewV4()).String()
	newControl.DriverID = driverID
	newControl.ControlType = controlType.Name
	_, err = p.DB.ModelContext(ctx, &newControl).
		Insert()
	if err != nil {
		return newControl, errors.Wrap(err, "Ошибка записи нового фотоконтроля в БД")
	}
	return newControl, nil
}

func (p *Repository) GetPhotoControlByControlIDAndType(ctx context.Context, controlID, controlType string) (models.PhotoControl, error) {
	var result models.PhotoControl
	if err := p.DB.ModelContext(ctx, &result).
		Where("control_id = ?", controlID).
		Where("control_type = ?", controlType).
		Where("approved IS not true").
		Order("created_at desc").
		Limit(1).
		Select(); err != nil {
		return result, errors.Wrap(err, "Ошибка чтения последнего фотоконтроля из БД")
	}
	return result, nil
}

func (p *Repository) FilterLastPhotoControl(photoControls []models.PhotoControl) []models.PhotoControl {
	if len(photoControls) < 1 {
		return photoControls
	}

	firstControlID := photoControls[0].ControlID
	var result []models.PhotoControl
	for _, pc := range photoControls {
		if pc.ControlID != firstControlID {
			return result
		}
		result = append(result, pc)
	}
	return result
}

func (p *Repository) PhotoControlExists(
	ctx context.Context, phCon models.PhotoControlIn, cType *models.ControlType) (bool, error) {

	check, err := p.DB.ModelContext(ctx, &models.PhotoControl{}).
		Where("driver_id = ?", phCon.DriverID).
		Where("approved is not true").
		Where("control_type = ?", cType.Name).
		Where("created_at > ?", time.Now().Add(-cType.PassTimeout)).
		Order("created_at DESC").
		Exists()
	if err != nil {
		return false, errors.New("Ошибка проверки существования фотоконтроля")
	}
	return check, nil
}
