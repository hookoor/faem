package db

import (
	"context"

	"gitlab.com/faemproject/backend/faem/services/driver/models"
)

// FIXME: hardcoded for now, allow managers to modify them later

func (p *Repository) AllControlTypes(ctx context.Context) ([]models.ControlType, error) {
	return models.AvailableTypes, nil
}

func GetControlTypeByName(name string) models.ControlType {
	if name == models.RegControlName {
		return models.RegControl
	}
	for _, ct := range models.AvailableTypes {
		if ct.Name == name {
			return ct
		}
	}
	return models.ControlType{}
}
