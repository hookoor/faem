package orders

import (
	"context"
	"gitlab.com/faemproject/backend/faem/services/crm/surge"
	"sync"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

type ServiceSurgeIn struct {
	OrderCount  int
	DriverCount int
}

type RectGeoZone struct {
	Row          int
	Col          int
	ServicesMeta map[string]ServiceSurgeIn
}

type RectGeoZonesRow map[int]RectGeoZone

type RectGeoMap map[int]RectGeoZonesRow

// A little bit more clear structures to be stored in memory

type RectGeoZonesSurges map[int]structures.SurgeItem // col to surge

type RectGeoMapSurges map[int]RectGeoZonesSurges

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

var (
	CurrentServiceCorrection = ServiceCorrection{
		corrections: make(RegionalCorrections),
		config: models.SurgeConfig{
			ZoneSize: models.DefaultSurgeZoneSize,
			//AllowOutOfTown: models.DefaultSurgeOutOfTown,
		},
	}
)

type ServiceCorrectionMap map[string]RectGeoMapSurges // map service uuid to a correction
type RegionalCorrections map[int]ServiceCorrectionMap

type ServiceCorrection struct {
	corrections RegionalCorrections
	config      models.SurgeConfig
	rules       surge.RulesMap
	mx          sync.Mutex
}

func (sc *ServiceCorrection) Clear() {
	sc.mx.Lock()
	sc.corrections = make(RegionalCorrections)
	sc.mx.Unlock()
}

func (sc *ServiceCorrection) Load(lat, lon float64, regionID int, serviceUUID string) structures.SurgeItem {
	sc.mx.Lock()
	defer sc.mx.Unlock()

	ownDelta, driverCount, orderCount := 0., 0, 0
	row, col := sc.findRectGeoZone(lat, lon)
	if zone, found := sc.getGeoZone(row, col, regionID, serviceUUID); found {
		ownDelta = zone.CurDelta
		driverCount = zone.CurDriverCount
		orderCount = zone.CurOrderCount
	}

	// Flat if a zone does not exists
	flatDelta, totalNeighbours := 0., 0
	for i := -1; i < 2; i++ {
		for j := -1; j < 2; j++ {
			if i == 0 && j == 0 { // skip the current zone
				continue
			}
			var delta float64
			if zone, found := sc.getGeoZone(row+i, col+j, regionID, serviceUUID); found {
				delta = zone.CurDelta
			}
			flatDelta += delta
			totalNeighbours++
		}
	}

	flatDelta /= float64(totalNeighbours)
	delta := (ownDelta + flatDelta) / 2
	result := sc.rules.GetTable(regionID, serviceUUID).GetSurgeItemForDelta(delta) // get the flatten value
	result.IsFlat = true
	result.CurDriverCount = driverCount
	result.CurOrderCount = orderCount
	return result
}

func (sc *ServiceCorrection) Store(regionID int, serviceUUID string, row, col int, surge structures.SurgeItem) {
	sc.mx.Lock()
	defer sc.mx.Unlock()

	if _, found := sc.corrections[regionID]; !found {
		sc.corrections[regionID] = make(ServiceCorrectionMap)
	}
	if _, found := sc.corrections[regionID][serviceUUID]; !found {
		sc.corrections[regionID][serviceUUID] = make(RectGeoMapSurges)
	}

	serviceCorrections := sc.corrections[regionID][serviceUUID]
	if _, found := serviceCorrections[row]; !found {
		serviceCorrections[row] = make(RectGeoZonesSurges)
	}
	zonesRow := serviceCorrections[row]
	zonesRow[col] = surge
}

func (sc *ServiceCorrection) SetSurgeConfig(surgeConfig *models.SurgeConfig) {
	if surgeConfig == nil {
		return // skip nil configs
	}

	sc.mx.Lock()
	sc.config = *surgeConfig
	sc.mx.Unlock()
}

func (sc *ServiceCorrection) SetRulesMap(rulesMap surge.RulesMap) {
	if rulesMap == nil {
		return
	}

	sc.mx.Lock()
	sc.rules = rulesMap
	sc.mx.Unlock()
}

func (sc *ServiceCorrection) OutOfTownAllowed(regionID int, serviceUUID string) bool {
	return sc.rules.GetAllowedOutOfTown(regionID, serviceUUID)
}

func (sc *ServiceCorrection) findRectGeoZone(lat, lon float64) (row, col int) {
	// Do not use mutex!
	zoneSize := sc.config.ZoneSize

	origin := gis.OriginPoint

	width := gis.Distance(origin.Lat, origin.Long, origin.Lat, lon)
	col = int(width / zoneSize)
	if lon < origin.Long {
		col = -col - 1
	}

	height := gis.Distance(origin.Lat, origin.Long, lat, origin.Long)
	row = int(height / zoneSize)
	if lat < origin.Lat {
		row = -row - 1
	}

	return
}

func (sc *ServiceCorrection) getGeoZone(row, col int, regionID int, serviceUUID string) (structures.SurgeItem, bool) {
	// Do not use mutex!
	if serviceMap, found := sc.corrections[regionID]; found {
		if geoMap, found := serviceMap[serviceUUID]; found {
			if zonesRow, found := geoMap[row]; found {
				if zone, found := zonesRow[col]; found {
					return zone, true
				}
			}
		}
	}
	return structures.SurgeItem{}, false
}

func (sc *ServiceCorrection) GetTable(regionID int, serviceUUID string) surge.Table {
	return sc.rules.GetTable(regionID, serviceUUID)
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

func GetUndispatchedOrders(ctx context.Context, begin time.Time) ([]*OrderCRM, error) {
	var result []*OrderCRM
	err := db.ModelContext(ctx, &result).
		Column("service", "routes", "region_id").
		WhereInMulti(
			"order_state IN (?)",
			constants.OrderStateCreated, constants.OrderStateDistributing, constants.OrderStateFree,
		).
		Where("created_at > ?", begin).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to group undispatched order services")
	}
	return result, nil
}

func GetFreeDrivers(ctx context.Context, begin time.Time) ([]*models.DriverCRM, error) {
	var result []*models.DriverCRM
	err := db.ModelContext(ctx, &result).
		Column("uuid", "available_services", "region_id").
		Where("status = ?", constants.DriverStateOnline).
		Where("status_update_time > ?", begin).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to select free drivers")
	}
	return result, nil
}

func GetOrdersByStates(ctx context.Context, begin time.Time, states []string) ([]*OrderCRM, error) {
	var result []*OrderCRM
	err := db.ModelContext(ctx, &result).
		Column("service", "routes", "order_state").
		WhereIn("order_state IN (?)", states).
		Where("created_at > ?", begin).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to select orders by states")
	}
	return result, nil
}

func GetDriversByStates(ctx context.Context, begin time.Time, states []string) ([]*models.DriverCRM, error) {
	var result []*models.DriverCRM
	err := db.ModelContext(ctx, &result).
		Column("uuid", "available_services", "status").
		WhereIn("status in (?)", states).
		Where("status_update_time > ?", begin).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to select drivers by states")
	}
	return result, nil
}

func MapOrdersToDriversByZones(orders []*OrderCRM, drivers []*models.DriverCRM) RectGeoMap {
	makeOrderZone := func(row, col int, serviceUUID string) RectGeoZone {
		zone := RectGeoZone{
			Row:          row,
			Col:          col,
			ServicesMeta: make(map[string]ServiceSurgeIn),
		}
		zone.ServicesMeta[serviceUUID] = ServiceSurgeIn{OrderCount: 1}
		return zone
	}

	geoMap := make(RectGeoMap)

	// Fill geo map from orders
	for _, order := range orders {
		if len(order.Routes) < 1 {
			continue
		}

		start := order.Routes[0]
		row, col := CurrentServiceCorrection.findRectGeoZone(float64(start.Lat), float64(start.Lon))
		if zoneRow, found := geoMap[row]; found {
			if zone, found := zoneRow[col]; found {
				if serviceMeta, found := zone.ServicesMeta[order.Service.UUID]; found {
					zone.ServicesMeta[order.Service.UUID] = ServiceSurgeIn{OrderCount: serviceMeta.OrderCount + 1}
				} else {
					zone.ServicesMeta[order.Service.UUID] = ServiceSurgeIn{OrderCount: 1}
				}
			} else {
				zone := makeOrderZone(row, col, order.Service.UUID)
				zoneRow[col] = zone
			}
		} else { // add new zone
			zone := makeOrderZone(row, col, order.Service.UUID)
			geoMap[row] = make(RectGeoZonesRow)
			geoMap[row][col] = zone
		}
	}

	// Map drivers to orders

	makeDriverZone := func(row, col int, serviceUUIDs []string) RectGeoZone {
		zone := RectGeoZone{
			Row:          row,
			Col:          col,
			ServicesMeta: make(map[string]ServiceSurgeIn),
		}
		for _, srvUUID := range serviceUUIDs {
			zone.ServicesMeta[srvUUID] = ServiceSurgeIn{DriverCount: 1}
		}
		return zone
	}

	mapServiceUUIDs := func(services []structures.Service) []string {
		result := make([]string, 0, len(services))
		for _, srv := range services {
			result = append(result, srv.UUID)
		}
		return result
	}

	driverUUIDs := make([]string, 0, len(drivers))
	for _, driver := range drivers {
		driverUUIDs = append(driverUUIDs, driver.UUID)
	}
	driverLocations := models.CurrentDriverLocationsByUUIDs(driverUUIDs)
	for _, driver := range drivers {
		if drvLoc, found := driverLocations[driver.UUID]; found {
			row, col := CurrentServiceCorrection.findRectGeoZone(drvLoc.Latitude, drvLoc.Longitude)
			servicesUUIDS := mapServiceUUIDs(driver.AvailableServices)
			if zoneRow, found := geoMap[row]; found {
				if zone, found := zoneRow[col]; found {
					for _, drvService := range driver.AvailableServices {
						if serviceMeta, found := zone.ServicesMeta[drvService.UUID]; found {
							zone.ServicesMeta[drvService.UUID] = ServiceSurgeIn{
								OrderCount:  serviceMeta.OrderCount,
								DriverCount: serviceMeta.DriverCount + 1,
							}
						} else {
							zone.ServicesMeta[drvService.UUID] = ServiceSurgeIn{DriverCount: 1}
						}
					}
				} else {
					zone := makeDriverZone(row, col, servicesUUIDS)
					zoneRow[col] = zone
				}
			} else { // add new zone
				zone := makeDriverZone(row, col, servicesUUIDS)
				geoMap[row] = make(RectGeoZonesRow)
				geoMap[row][col] = zone
			}
		}
	}

	return geoMap
}
