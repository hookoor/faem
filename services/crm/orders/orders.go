package orders

import (
	"context"
	"fmt"
	"reflect"
	"strconv"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/pkg/structures/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

var (
	db              *pg.DB
	gisCrmZonesData gis.Gis
	osrmka          *osrm.OSRM
	dbQueryHook     *postgre.DBQueryTraceHook
)

const (
	ProductDeliveryOrdersConfirmationTime = 10 * time.Minute
	ProductDeliveryOrdersFindDriverTime   = 7 * time.Minute
	ProductDeliveryOrdersPaymentMinutes   = 5
	AccessDeniedMsg                       = "У вас нет прав для доступа к заказам этого партнера"
)

// ConnectDB initialize connection to package var
func ConnectDB(conn *pg.DB) {
	var err error
	db = conn
	dbQueryHook = models.GetDBQueryHook()
	osrmka = models.GetOSRMInstance()
	// osrmka = osrm.New(config.St.OSRM.Host)

	gisCrmZonesData, err = gis.New(db, models.CrmZone{}, "area")
	if err != nil {
		logrus.Errorf("(ConnectDB)error init crm zones data")
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// OrderServer -
type OrderServer struct {
	DB DB
}

// DB -
type DB struct {
	// DebugModel         IDebug
}

// NewServer Sender New constructor
func NewServer(conn *pg.DB) *OrderServer {
	return &OrderServer{
		DB: DB{
			// DebugModel:         InitDebugModel(conn, dbHook),
		},
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Criteria критерии фильтра заказов
// Если стоит тег ignore - это поле не учитывается как заполненное поле для сортировки
// Используется функцией isZero
type Criteria struct {
	MinDate                time.Time `json:"min_date"`
	MaxDate                time.Time `json:"max_date" ignore:""`
	StoreUUID              string    `json:"store_uuid"`
	Active                 bool      `json:"active"`
	OrderState             string    `json:"order_state"`
	ClientPhone            string    `json:"client_phone"`
	RouteFrom              string    `json:"route_from"`
	ID                     int       `json:"id"`
	RouteTo                string    `json:"route_to"`
	TaxiParksUUID          []string  `json:"taxi_parks_uuid" max_len:"1"`
	UserUUID               string    `json:"user_uuid"`
	FeatureUUID            []string  `json:"feature_uuid"`
	ServiceUUID            string    `json:"service_uuid"`
	ImportantFirst         bool      `json:"important_first"`
	OnlyProductsDelivery   bool      `json:"only_products_delivery"`
	DriverAlias            string    `json:"driver_alias"`
	OrderSource            []string  `json:"source"`
	SourceUUIDs            []string  `json:"source_of_orders_uuids"`
	OperatorLoginTelephony int       `json:"operator_alias"`
	PhoneLine              string    `json:"phone_line"`
	RegionIDs              []int     `json:"region_ids"`
	CounterOrdersOnly      bool      `json:"counter_orders_only"`
	UserRole               string    `json:"-" sql:"-" ignore:""` // сюда пишем роль юзера для фильтра

	// его возможно стоит переделать)
	Pager struct {
		urlvalues.Pager
		Page int
	} `json:"pager" ignore:""`
}

// CancelRequest godoc
type CancelRequest struct {
	Reason string
}

// OrderCRM main orders CRM struct
type OrderCRM struct {
	tableName struct{} `sql:"crm_orders" pg:",discard_unknown_columns"`
	structures.Order
	ID                        int                  `json:"id" sql:",pk"`
	ClUUID                    string               `json:"client_uuid"`
	SourceOfOrdersUUID        string               `json:"source_uuid" sql:"source_of_orders_uuid"` //
	SrUUID                    string               `json:"service_uuid"`
	FeaturesUUIDS             []string             `json:"features_uuids"`
	StartUserUUID             string               `json:"user_uuid"`
	OrderNumberInStore        string               `json:"order_number_in_store,omitempty" sql:"-"`
	StartUserName             string               `json:"user_name" sql:"start_user_name"`
	ProductsPrice             int                  `json:"products_price,omitempty" sql:"-"`
	StoreUUID                 string               `json:"-" sql:"-"`
	StartUserLoginTelethony   string               `json:"user_alias" sql:"start_user_login_telephony"`
	LastUserUUID              string               `json:"last_user_uuid"`
	DriverArrivalTimeData     DriverArrivalTimeCRM `json:"driver_arrival_time_data" sql:"driver_arrival_time_data"`
	ReasonOfCancellation      string               `json:"reason_of_cancellation"`
	ReasonOfCancellationTitle string               `json:"reason_of_cancellation_title"`
	FinishUserUUID            string               `json:"finished_user_uuid"`
	CancelTimeUnix            int64                `json:"cancel_time_unix" sql:"-"`
	AppointmentTime           time.Time            `json:"appointment_time" description:"Время назначения авто на заказ"`
	CompleteTime              time.Time            `json:"complete_time" description:"Врем завершения заказа"`
	CompleteTimeUnix          int64                `json:"complete_time_unix,omitempty" sql:"-"`
	PreparationTime           *int                 `json:"preparation_time,omitempty" sql:"-"`
	PickupTime                time.Time            `json:"-" description:"Прибытие авто"`
	CreatedAtUnix             int64                `json:"created_at_unix" description:"Дата создания в unix" sql:"-"`
	UpdatedAt                 time.Time            `json:"updated_at" `
	Deleted                   bool                 `json:"-" sql:"default:false"`
	OrderState                string               `json:"state_name"`
	DeliveredToDriver         bool                 `json:"delivered_to_driver"` // при каждом переходе в offer_offered переводится в false
	StateTitle                string               `json:"state_title" sql:"-"`
	TripTime                  int64                `json:"trip_time" sql:"-"`
	ReportedAppointment       bool                 `json:"reported_appointment"`
	StateTransferTime         time.Time            `json:"state_transfer_time"`
	ImportanceReasons         []ImportanceReason   `json:"importance_reason" sql:",type:text[]"`
	ReportedArrival           bool                 `json:"reported_arrival"`
	SendSMS                   bool                 `json:"send_sms"`
	Unpaid                    *bool                `json:"unpaid" sql:"-"`
	UnreadMessagesCount       int                  `json:"unread_messages_count" sql:"-"` // кол непрочитанных сообщений
	RegionID                  int                  `json:"region_id"`
	CounterOrderMarker        bool                 `json:"counter_order_marker"` // служебная метка, нужна для различия между текущим заказом и встречным при отображении водителям
	IsCounter                 bool                 `json:"is_counter"`           // реальная метка встречного заказа для истории, чтобы было видно что заказ был встречным даже после того как он станет текущим
}

func (or *OrderCRM) FromApp() bool {
	if or == nil {
		return false
	}

	return or.Order.FromClientApp()
}

type DriverArrivalTimeCRM struct {
	IndicatedArrivalTime int64 `json:"indicated_arrival_time"`
	ArrivedInFact        int64 `json:"arrived_in_fact"`
	IndicatedTime        int   `json:"indicated_time"`
	RemainingWaitingTime int   `json:"remaining_waiting_time"`
}

// UpdateDriverArrivedInFactTime обновляет фактическое время прибытия водителя
func UpdateDriverArrivedInFactTime(orderUUID string) error {
	_, err := db.Model(&OrderCRM{}).
		Set(fmt.Sprintf("driver_arrival_time_data = jsonb_set(COALESCE(driver_arrival_time_data, '{}'), '{arrived_in_fact}', '%v')", time.Now().Unix())).
		Where("uuid = ?", orderUUID).
		Update()
	return err
}

// GetUndeliveredOrderUUID returns order and driver UUIDs, which are not delivered in required time
func GetUndeliveredOrderUUID(allowedTaxiParks []string) (map[string]string, error) {
	var data []struct {
		UUID       string `json:"uuid"`
		DriverUUID string `json:"driver_uuid"`
	}
	query := db.Model(&OrderCRM{}).
		Where("order_state = ?", constants.OrderStateOffered).
		Where("not delivered_to_driver").
		Where("state_transfer_time < ?", getOrderDeliveryExpiredTime()).
		Where("deleted is not true")

	query = filterOrdersByTaxiPark(query, allowedTaxiParks)

	err := query.ColumnExpr("uuid,driver ->> 'uuid' as driver_uuid").
		Select(&data)

	res := make(map[string]string)
	for _, val := range data {
		res[val.UUID] = val.DriverUUID
	}

	return res, err
}
func getOrderDeliveryExpiredTime() time.Time {
	waitTime := time.Duration(config.St.Application.DeliveryTimeToDriverS) * time.Second
	return time.Now().Add(-waitTime)
}
func MarkAsDelivered(orderUUID, driverUUID string) error {
	_, err := db.Model(&OrderCRM{}).
		Where("uuid = ?", orderUUID).
		Where("order_state = ?", constants.OrderStateOffered).
		Where("not delivered_to_driver").
		Where("state_transfer_time > ?", getOrderDeliveryExpiredTime()).
		Where("driver ->> 'uuid' = ?", driverUUID).
		Where("deleted is not true").
		Set("state_transfer_time = CURRENT_TIMESTAMP").
		Set("delivered_to_driver = true").
		Update()
	return err
}

// UpdateOrderDriverArrivalTime парсит из стринга в int64 время прибытия и записывает в заказ
func UpdateOrderDriverArrivalTime(orderUUID, str string) error {
	arrTime, err := getDriverArrivalTimeData(str)
	if err != nil {
		return err
	}
	_, err = db.Model(&OrderCRM{}).
		Set("driver_arrival_time_data = ?", arrTime).
		Where("uuid = ?", orderUUID).
		Update()
	return err
}
func getDriverArrivalTimeData(strArrTime string) (DriverArrivalTimeCRM, error) {
	var result DriverArrivalTimeCRM
	arrTimeUnix, err := strconv.Atoi(strArrTime)
	if err != nil {
		return result, errors.Errorf("str parsing error, %s", err)
	}

	result.IndicatedArrivalTime = int64(arrTimeUnix)
	result.IndicatedTime = int(result.IndicatedArrivalTime - time.Now().Unix())
	if result.IndicatedTime < 0 {
		result.IndicatedTime = 0
	}
	return result, nil
}

// UpdateComment update only comment
func (or *OrderCRM) UpdateComment() error {
	_, err := db.Model(or).
		Where("uuid = ?", or.UUID).
		Set("comment = ?comment").
		Update()
	if err != nil {
		return err
	}
	return nil
}

// UpdateRoutesAndTariff update routes, route_way_data and tariff
func (or *OrderCRM) UpdateRoutesAndTariff() error {
	_, err := db.Model(or).
		Where("uuid = ?", or.UUID).
		Set("routes = ?routes").
		Set("tariff = ?tariff").
		Set("route_way_data = ?route_way_data").
		Update()
	if err != nil {
		return err
	}
	return nil
}

// UpdateIncreasedFare update increased_fare
func (or *OrderCRM) UpdateIncreasedFareAndTariff() error {
	_, err := db.Model(or).
		Where("uuid = ?", or.UUID).
		Set("increased_fare = ?increased_fare").
		Set("tariff = ?tariff").
		Update()
	if err != nil {
		return err
	}
	return nil
}

// Create creating order
func (or *OrderCRM) Create(userUUID string, member ...structures.ChatMembers) (*OrderCRM, error) {
	if err := or.validateCreation(); err != nil {
		return nil, errpath.Err(err, "Data validation error")
	}

	if or.SrUUID == "" {
		var soo models.SourceOfOrdersCRM

		soo, err := soo.GetByUUID(or.SourceOfOrdersUUID)
		if err != nil {
			return nil, errpath.Err(err)
		}
		if soo.DefaultServiceUUID == "" {
			return nil, errpath.Errorf("SourceOfOrdersCRM have empty default service")
		}
		or.SrUUID = soo.DefaultServiceUUID
	}

	service := new(models.ServiceCRM)
	if err := db.Model(service).Where("uuid = ?", or.SrUUID).Select(); err != nil {
		return nil, errpath.Err(err, "Data validation error")
	}
	or.Service = service.Service

	or.UUID = structures.GenerateUUID()
	if or.SendSMS {
		// чтобы uuid был короче в ссылке
		or.UUID = or.UUID[24:]
	}
	if or.OrderStart.IsZero() {
		or.OrderStart = models.CurrentTime()
	}

	var orderCreationTagFuel bool
	for _, n := range or.Service.Tag {
		if "fuel" == n {
			orderCreationTagFuel = true
			break
		}
	}

	if orderCreationTagFuel {
		tariff, err := GetFuelTariff()
		if err != nil {
			return nil, errpath.Err(err)
		}
		or.Tariff = tariff
	} else {
		tariff, err := GetTariffWithBonuses(or)
		if err != nil {
			return nil, errpath.Err(err)
		}
		or.Tariff = tariff
	}

	if len(or.Routes) != 1 {
		rwData, err := GetRouteWayData(or.Routes)
		if err != nil {
			return nil, errpath.Err(err)
		}
		or.RouteWayData = rwData
	}
	if err := or.setStartPointOutOfTown(); err != nil {
		logs.Eloger.WithField("event", "set start point out of town").Warn(err)
		// continue intentionally
	}

	if len(member) != 0 && member[0] == structures.UserCRMMember {
		user := new(models.UsersCRM)

		if err := models.GetByUUID(userUUID, user); err != nil {
			return nil, errpath.Err(err, "getting user by uuid")
		}
		or.StartUserUUID = userUUID
		or.StartUserName = user.Name
		or.StartUserLoginTelethony = user.LoginTelephony
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":     "order created",
		"reason":    "price calculated",
		"orderUUID": or.UUID,
		"value":     or.Tariff.TotalPrice,
	}).Info("Order price calculated")

	// Promotion fields
	or.Promotion.IsUnpaid = or.IsUnpaid()
	if or.ProductsPrice > 0 {
		or.Tariff.ProductsPrice = or.ProductsPrice
	}

	// сохраняем в БД

	if err := or.fillOnlyStoreDataIfNeed(); err != nil {
		return nil, err
	}
	or.fillDistributionData()

	nOrder, err := or.SaveNewOrder()
	if err != nil {
		return nil, err
	}

	return &nOrder, nil
}

func (or *OrderCRM) fillDistributionData() {
	if or == nil {
		return
	}
	if or.DistributionByTaxiPark == nil {
		or.DistributionByTaxiPark = boolPointer(true)
	}
}

func boolPointer(neededVal bool) *bool {
	return &neededVal
}

// fillOnlyStoreData godoc
func (or *OrderCRM) fillOnlyStoreDataIfNeed() error {
	if or.StoreUUID == "" {
		return nil
	}
	store, err := models.GetStoreByUUID(or.StoreUUID)
	if err != nil {
		return errors.Errorf("error getting store by uuid, %s", err)
	}
	if or.PreparationTime == nil {
		or.PreparationTime = store.OrderPreparationTimeSecond
	}
	or.ProductsData = &structures.ProductsDataInOrder{
		StoreData:          store.Store,
		OrderNumberInStore: or.OrderNumberInStore,
		Buyout:             true,
		PreparationTime:    *or.PreparationTime,
	}
	return nil
}

// Update godoc
func (or *OrderCRM) Update(uuid string, ownersUUID []string) (OrderCRM, error) {
	uOrder, err := or.validUpdate(uuid, ownersUUID)
	if err != nil {
		return uOrder, errors.Errorf("Data validation error. %v", err)
	}

	err = uOrder.fillColumnsWithObjects()
	if err != nil {
		return OrderCRM{}, err
	}

	uOrder.Tariff.WaitingBoarding = nil
	uOrder.Tariff.WaitingPoint = nil

	uOrder.Tariff, err = GetTariffWithBonuses(&uOrder)
	if err != nil {
		return OrderCRM{}, err
	}
	uOrder.RouteWayData, err = GetRouteWayData(uOrder.Routes)
	if err != nil {
		return OrderCRM{}, err
	}
	if err = uOrder.setStartPointOutOfTown(); err != nil {
		logs.Eloger.WithField("event", "set start point out of town").Warn(err)
		// continue intentionally
	}
	err = models.UpdateByPK(&uOrder)
	if err != nil {
		return uOrder, errors.Errorf("DB error. %v", err)
	}
	return uOrder, nil
}

//
func (or *OrderCRM) validUpdate(uuid string, ownersUUID []string) (OrderCRM, error) {
	flag := false
	var order OrderCRM
	var oldOrder OrderCRM
	err := models.GetByUUID(uuid, &oldOrder)
	if err != nil {
		return oldOrder, err
	}

	if !(structures.StringInArray(oldOrder.GetTaxiParkUUID(), ownersUUID)) && len(ownersUUID) != 0 {
		return oldOrder, errors.Errorf(AccessDeniedMsg)
	}
	if or.Comment != nil {
		order.Comment = or.Comment
		flag = true
	}
	if or.SrUUID != "" {
		order.SrUUID = or.SrUUID
		flag = true
	}
	if or.CallbackPhone != "" {
		order.CallbackPhone = or.CallbackPhone
		flag = true
	}
	if or.FixedPrice != 0 {
		order.FixedPrice = or.FixedPrice
		flag = true
	}
	order.Client = oldOrder.Client
	order.Driver = oldOrder.Driver
	order.Source = oldOrder.Source
	order.SourceOfOrdersUUID = oldOrder.SourceOfOrdersUUID
	order.Service = oldOrder.Service
	order.Tariff = oldOrder.Tariff
	if or.Tariff.TariffCalcType != "" {
		order.Tariff.TariffCalcType = or.Tariff.TariffCalcType
	} else {
		order.Tariff.TariffCalcType = structures.CalcWithDist // если тип не указан будет расчет по расстоянию
	}
	if or.Tariff.OrderTripTime != 0 {
		order.Tariff.OrderTripTime = or.Tariff.OrderTripTime
	}
	order.RouteWayData = oldOrder.RouteWayData
	order.Routes = oldOrder.Routes
	order.Promotion = oldOrder.Promotion
	if or.ClUUID != "" {
		order.ClUUID = or.ClUUID
		flag = true
	}
	if or.TaxiParkUUID != nil {
		order.TaxiParkUUID = or.TaxiParkUUID
		flag = true
	}
	if or.FeaturesUUIDS != nil {
		order.FeaturesUUIDS = or.FeaturesUUIDS
		flag = true
	}
	if or.IsOptional != nil {
		order.IsOptional = or.IsOptional
		flag = true
	}
	if or.Unpaid != nil {
		order.Promotion.IsUnpaid = or.IsUnpaid()
		flag = true
	}

	if or.Routes != nil {
		order.Routes = or.Routes
		flag = true
	}

	if !flag {
		return order, errors.Errorf("Required fields are empty")
	}
	order.UpdatedAt = time.Now()
	order.UUID = uuid
	order.Deleted = false
	return order, nil
}

func (or *OrderCRM) SaveNewOrder() (OrderCRM, error) {
	// Через время удалить этот блок, оставлен для обратной совместимости
	if or.SourceOfOrdersUUID == "" {
		srcf := &models.SrcOfOrdersForwarding{
			RegionID:  1,
			SourceKey: or.Source,
		}

		if err := srcf.FillSourceOfOrdersUUID(); err != nil {
			return OrderCRM{}, err
		}
		or.SourceOfOrdersUUID = srcf.SourceOfOrdersUUID
	}

	var (
		nOrder      OrderCRM
		isOptionale = true
	)

	nOrder.UUID = or.UUID
	nOrder.ClUUID = or.Client.UUID
	nOrder.Comment = or.Comment
	nOrder.Routes = or.Routes
	nOrder.ProductsData = or.ProductsData
	nOrder.IsOptional = or.IsOptional
	nOrder.FeaturesUUIDS = or.FeaturesUUIDS
	nOrder.SrUUID = or.SrUUID
	// Try to load SrUUID from the received service field
	if nOrder.SrUUID == "" {
		nOrder.SrUUID = or.Service.UUID
	}

	if or.IncreasedFare < 0 {
		nOrder.IsOptional = &isOptionale
	}

	nOrder.Tariff = or.Tariff
	nOrder.TaxiParkUUID = or.TaxiParkUUID
	nOrder.IncreasedFare = or.IncreasedFare
	nOrder.DistributionByTaxiPark = or.DistributionByTaxiPark

	nOrder.StartUserUUID = or.StartUserUUID
	nOrder.StartUserName = or.StartUserName
	nOrder.SendSMS = or.SendSMS
	nOrder.StartUserLoginTelethony = or.StartUserLoginTelethony

	nOrder.CallbackPhone = or.CallbackPhone
	nOrder.Client = or.Client
	nOrder.WithoutDelivery = or.WithoutDelivery
	nOrder.OwnDelivery = or.OwnDelivery
	nOrder.CancelTime = or.CancelTime
	nOrder.OrderStart = or.OrderStart
	nOrder.FixedPrice = or.FixedPrice
	nOrder.RouteWayData = or.RouteWayData
	nOrder.Promotion = or.Promotion

	if or.SourceOfOrdersUUID == "" {
		or.SourceOfOrdersUUID = or.SourceOfOrders.UUID
	}
	nOrder.SourceOfOrdersUUID = or.SourceOfOrdersUUID

	nOrder.PaymentType = or.PaymentType
	if nOrder.PaymentType == "" {
		nOrder.PaymentType = constants.OrderPaymentTypeCash
	}
	nOrder.CreatedAt = models.CurrentTime()
	soo, err := models.SourceOfOrdersCRM{}.GetByUUID(or.SourceOfOrdersUUID)
	if err != nil {
		return OrderCRM{}, errpath.Err(err)
	}
	if soo.SourceType == "api" {
		or.Source = soo.Name
	}
	nOrder.Source = or.Source

	// nOrder.fillingСoordinates()
	dur, _ := time.ParseDuration(config.St.Application.DistributingTime)
	nOrder.CancelTime = time.Now().Add(dur)

	if err := nOrder.fillColumnsWithObjects(); err != nil {
		return OrderCRM{}, errpath.Err(err)
	}

	neededState := constants.OrderStateCreated
	if or.StoreUUID != "" {
		switch nOrder.Source {
		case constants.OrderSourceDelivery:
			neededState = constants.OrderStateCooking
		default:
			neededState = constants.OrderStateConfirmation
			nOrder.ImportanceReasons = []ImportanceReason{WaitingForConformitionState}
		}
		if !nOrder.DeliveryRequired() && nOrder.Source == constants.OrderSourceCRM {
			neededState = constants.OrderStateTransToStore
		}
	} else {
		nOrder.Service.ProductDelivery = false
	}
	nOrder.OrderState = neededState

	if _, err := db.Model(&nOrder).Returning("*").Insert(); err != nil {
		return OrderCRM{}, errpath.Err(err)
	}

	// сохраняем новый статус в БД
	var orderState OrderStateCRM
	orderState.OrderUUID = nOrder.UUID
	orderState.State = neededState
	orderState.StartState = time.Now()

	if _, err := db.Model(&orderState).Returning("*").Insert(); err != nil {
		nErr := errors.Errorf("Error saving order state. %s", err)
		return OrderCRM{}, nErr
	}

	if err := rabsender.SendJSONByAction(rabsender.AcOrderState, orderState); err != nil {
		return nOrder, errpath.Err(err, "error sending new order state")
	}

	return nOrder, nil
}

func UpdateOrderCancelTime(orderUUID string, distTime time.Duration) error {
	_, err := db.Model(&OrderCRM{}).
		Where("uuid = ?", orderUUID).
		Set("cancel_time = ?", time.Now().Add(distTime)).
		Update()
	return err
}

func ReDistributionOrderValidate(orderUUID string) error {
	existCheck, err := db.Model(&OrderCRM{}).Where("uuid = ?", orderUUID).Exists()
	if err != nil {
		return errors.Errorf("error checking order existence, %s", err)
	}
	if !existCheck {
		return errors.Errorf("такого заказа не существует")
	}
	return nil
}

// GetOrderWithClientHistory возвращет заказ даты создания завершенных заказов клиента
func GetOrderWithClientHistory(orUUID string) (structures.OrderWithClientHistory, error) {
	var order OrderCRM
	var result structures.OrderWithClientHistory
	err := db.Model(&order).
		Where("uuid = ?", orUUID).
		Where("deleted is not true").
		Select()
	if err != nil {
		return result, err
	}
	ctx, cancel := context.WithTimeout(context.Background(), 13*time.Second)
	defer cancel()
	errG, ctx := errgroup.WithContext(ctx)
	var startZones []models.CrmZone
	var finishZones []models.CrmZone
	errG.Go(func() error {
		err := gisCrmZonesData.ZonesBelongingToThePoint(structures.PureCoordinates{
			Lat:  float64(order.Routes[0].Lat),
			Long: float64(order.Routes[0].Lon),
		}, &startZones)
		if err != nil {
			return errors.Errorf("error getting zones by points,%s", err)
		}
		return nil
	})

	if len(order.Routes) > 1 {
		errG.Go(func() error {
			lastRoute := order.Routes[len(order.Routes)-1]
			err := gisCrmZonesData.ZonesBelongingToThePoint(structures.PureCoordinates{
				Lat:  float64(lastRoute.Lat),
				Long: float64(lastRoute.Lon),
			}, &finishZones)
			if err != nil {
				return errors.Errorf("error finish getting zones by points,%s", err)
			}
			return nil
		})
	}
	var orders []OrderCRM
	errG.Go(func() error {
		err := db.ModelContext(ctx, &orders).
			Where("order_state = ?", constants.OrderStateFinished).
			Where("client ->> 'main_phone' ILIKE ?", order.Client.MainPhone).
			Where("deleted is not true").
			Column("created_at").
			Select()
		return err
	})
	err = errG.Wait()
	result.Order = order.Order
	for _, or := range orders {
		result.ClientOrderHistory = append(result.ClientOrderHistory, or.CreatedAt)
	}
	for _, zone := range finishZones {
		result.FinishZones = append(result.FinishZones, zone.Name)
	}
	for _, zone := range startZones {
		result.StartZones = append(result.StartZones, zone.Name)
	}
	return result, err
}

// fills the fields in which there are objects
// заполняет поля, в которых находятся объекты
func (or *OrderCRM) fillColumnsWithObjects() error {
	ctx := context.Background()

	if len(or.FeaturesUUIDS) > 0 {
		var featureCrm []models.FeatureCRM
		or.Features = structures.FeatureSlice{}
		err := db.Model(&featureCrm).
			Where("uuid in (?)", pg.In(or.FeaturesUUIDS)).
			Select()
		if err != nil {
			return errors.Errorf("error find features,%s", err)
		}

		if len(featureCrm) != 0 {
			for _, value := range featureCrm {
				or.Features = append(or.Features, value.Feature)
			}
		}
	} else if or.FeaturesUUIDS != nil { // empty slice provided, should clean up the field
		or.Features = structures.FeatureSlice{}
	}

	if or.SourceOfOrdersUUID != "" {

		var soo models.SourceOfOrdersCRM

		soo, err := soo.GetByUUID(or.SourceOfOrdersUUID)
		if err != nil {
			return errpath.Err(err, "error find SourceOfOrders")
		}

		or.SourceOfOrders = soo.SourceOfOrders

		tpUUID, err := models.GetTaxiParkUUIDBySourceOfOrdersUUID(or.SourceOfOrdersUUID)
		if err != nil {
			return errors.Errorf("ошибка поиска таксопарка, %s", err)
		}
		or.TaxiParkUUID = &tpUUID

		or.RegionID, err = models.RegionCRM{}.GetRegionIDByTaxiParkUUID(ctx, tpUUID)
		if err != nil {
			return errpath.Err(err)
		}

		models.FillTaxiParkData(or)

	}
	if or.SrUUID != "" {

		var serviceCRM models.ServiceCRM
		err := models.GetByUUID(or.SrUUID, &serviceCRM)
		if err != nil {
			return errors.Errorf("error find service,%s", err)
		}

		or.Service = serviceCRM.Service
	}
	return nil
}
func ReCreateOrdersByState(state, reason, operatorUUID string, secondAfterStateTransfer int64) (int, error) {
	var orders []OrderCRM
	err := db.Model(&orders).
		Where("order_state = ?", state).
		Where("deleted is not true").
		Select()
	if err != nil {
		return 0, errors.Errorf("orders select error, %s", err)
	}
	ordersLen := 0
	if len(orders) < 1 {
		return ordersLen, errors.Errorf("not found orders by provided state")
	}
	for _, order := range orders {
		if (time.Now().Unix() - order.StateTransferTime.Unix()) < secondAfterStateTransfer {
			continue
		}
		newState, err := CancelOrder(reason, order.UUID)
		if err != nil {
			return ordersLen, errors.Errorf("error cancel order with uuid(%s), %s", order.UUID, err)
		}
		newState.OperatorUUID = operatorUUID
		err = rabsender.SendJSONByAction(rabsender.AcOrderState, newState)
		if err != nil {
			return ordersLen, errors.Errorf("Error sending driverState to broker. %s", err)
		}
		if order.FromApp() {
			continue
		}
		order.StoreUUID = order.GetProductsData().StoreData.UUID
		err = order.CreateAndSendToBroker(operatorUUID)
		if err != nil {
			return ordersLen, errors.Errorf("error create order with uuid(%s), %s", order.UUID, err)
		}
		ordersLen++
	}
	return ordersLen, nil
}
func (or *OrderCRM) CreateAndSendToBroker(userUUID string) error {
	nOrder, err := or.Create(userUUID)
	if err != nil {
		return errors.Errorf("error creating order, %s", err)
	}
	or = nOrder
	sendData := structures.WrapperOrder{
		OperatorUUID: userUUID,
		Order:        nOrder.Order,
	}

	if nOrder.OrderState != constants.OrderStateCooking {
		err = rabsender.SendJSONByAction(rabsender.AcOrderToDriver, sendData)
		if err != nil {
			return errors.Errorf("error send new order to broker, %s", err)
		}
	}
	return err
}

//CancelOrder отменяет заказ
func CancelOrder(reason string, uuid string) (OrderStateCRM, error) {
	var (
		orderState, newOrderState OrderStateCRM
		order                     OrderCRM
		check                     bool
	)
	orderState.State = constants.OrderStateCancelled
	for _, val := range constants.ListCrmCancelReasons() {
		if reason == val {
			check = true
			break
		}
	}
	if !check {
		return OrderStateCRM{}, errors.Errorf("invalid cancel reason")
	}
	err := models.GetByUUID(uuid, &order)
	if err != nil {
		return OrderStateCRM{}, errors.Errorf("error getting order,%s", err)
	}
	if !ifActiveState(order.OrderState) {
		return OrderStateCRM{}, errors.Errorf("Нельзя отменить неактивный заказ")
	}
	orderState.Comment = reason
	orderState.OrderUUID = uuid
	newOrderState, err = orderState.NewOrderState()
	if err != nil {
		return OrderStateCRM{}, err
	}

	_, err = db.Model(&order).Where("uuid=?uuid").
		Set("reason_of_cancellation = ?, reason_of_cancellation_title = ?", reason, constants.CancelReasonRU[reason]).
		Update()
	if err != nil {
		return OrderStateCRM{}, err
	}

	return newOrderState, nil
}

// FillUnixField заполняет поля CreatedAtForFront  и CancelTimeUnix
func (or *OrderCRM) FillUnixField() {
	or.CreatedAtUnix = or.CreatedAt.Unix()
	or.CompleteTimeUnix = or.CompleteTime.Unix()
	or.CancelTimeUnix = or.CancelTime.Unix()
}

func (or *OrderCRM) IsUnpaid() bool {
	if or == nil || or.Unpaid == nil {
		return false
	}
	return *or.Unpaid
}

func (or *OrderCRM) FillPromotionFields() {
	if or == nil {
		return
	}
	or.Unpaid = &or.Promotion.IsUnpaid
	or.FixedPrice = or.Tariff.FixedPrice
}

func (or *OrderCRM) validateCreation() error {

	if len(or.Routes) == 0 {
		return errors.Errorf("At least 1 address is required")
	}
	for _, route := range or.Routes {
		if route.Lat == 0 || route.Lon == 0 {
			return errors.Errorf("Для адреса '%s' в не прописаны коорректные координаты. Пожалуйста, введите вместо него какой нибудь адрес поблизости", route.UnrestrictedValue)
		}
	}
	if or.Client.MainPhone == "" {
		return errors.Errorf("Client Main Phone is required field")
	}

	if or.SourceOfOrdersUUID == "" {
		return errpath.Errorf("источник заказов не заполнен")
	}
	if or.PaymentType != constants.OrderPaymentTypeOnlineTransfer {
		or.PaymentType = constants.OrderPaymentTypeCash
	}
	return nil
}

func (or *OrderCRM) setStartPointOutOfTown() error {

	//if or == nil || len(or.Routes) < 1 || or.Routes[0].OutOfTown {
	//	return nil
	//}
	//
	//startPoint := structures.PureCoordinates{
	//	Long: float64(or.Routes[0].Lon),
	//	Lat:  float64(or.Routes[0].Lat),
	//}
	//var startZone models.CrmZone
	//if err := gisCrmZonesData.ZonesBelongingToThePoint(startPoint, &startZone); err != nil {
	//	return errors.Wrap(err, "failed to get zones belonging to the start point")
	//}
	//or.Routes[0].OutOfTown = startZone.Name != models.MaxPriorityRouteName
	return nil
}

func (cr Criteria) truncOrdersByDate(query *orm.Query) error {
	switch cr.UserRole {
	case "operator":
		if cr.MinDate.IsZero() {
			query.Where("created_at > CURRENT_TIMESTAMP - INTERVAL '1 month'")
			break
		}
		query.Where("created_at > CURRENT_TIMESTAMP - INTERVAL '1 day'")

	case "admin", "super_admin", "owner", "hr_admin":
		if !cr.MinDate.IsZero() {
			query.Where("created_at >= ?", cr.MinDate)
		}
		if !cr.MaxDate.IsZero() {
			query.Where("created_at <= ?", cr.MaxDate)
		}

	default:
		return errors.Errorf("unsupported role %s", cr.UserRole)
	}

	return nil
}

/*
	Эта функция нужна, чтобы админы и овнеры не пытались выполнить count на все заказы в базе данных.
	Только региональные админы и owner'ы могут послать тяжелый запрос, вызываем isZero для этих ролей
	Месяц будет при пустом фильтре по времени. Чтобы оператор мог смотреть заказы клиента за месяц в карточке заказа
	Если criteria пустая (нет фильтрации), то count отдаём 1000.
	Если нет - считаём что там есть в базе.
	Добавил pager в тэг ignored так как он не влияет на Count, а вот функция isZero будет думать, что фильтрация есть
	Тэг max_len указывает, после какой длины массива мы перестаём его учитывать как валидный фильтр

	P.S.: Не трогать ресивер метода (не менять с указателя на значение, перестанет работать)

	Всёёё!
*/
func (cr *Criteria) getCount(query *orm.Query) (int, error) {
	if cr.UserRole == "owner" || cr.UserRole == "admin" || cr.UserRole == "hr_admin" {
		if isZero(reflect.ValueOf(cr)) {
			return 1000, nil
		}
	}

	count, err := query.Count()
	if err != nil {
		return 0, err
	}

	return count, nil
}

// Сообщает нам, пустая ли переданная структура
func isZero(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Func, reflect.Map, reflect.Slice:
		return v.IsNil()
	case reflect.Array:
		z := true
		for i := 0; i < v.Len(); i++ {
			z = z && isZero(v.Index(i))
		}
		return z
	case reflect.Struct:
		z := true
		for i := 0; i < v.NumField(); i++ {
			if v.Field(i).CanSet() {
				// Скипаем поля, которые переданы в мапе ignoredFileds
				if _, ok := v.Type().Field(i).Tag.Lookup("ignore"); ok {
					continue
				}

				if val, ok := v.Type().Field(i).Tag.Lookup("max_len"); ok {
					n, err := strconv.Atoi(val)
					if err != nil {
						panic(err)
					}

					if v.Field(i).Len() > n {
						return true
					}
					continue
				}

				// 	Проверка отдельно для временного типа
				if v.Field(i).Type().Name() == "Time" {
					t := v.Field(i).Interface().(time.Time)
					z = z && t.IsZero()
					continue
				}

				if !z {
					return z
				}

				// рекурсивно проходим по всем полям структуры
				z = z && isZero(v.Field(i))
			}
		}
		return z
	case reflect.Ptr:
		return isZero(reflect.Indirect(v))
	}

	z := reflect.Zero(v.Type())
	result := v.Interface() == z.Interface()

	return result
}

//OrdersListWithOptions - returns orders according to some criteria
func (cr *Criteria) OrdersListWithOptions() ([]OrderCRM, int, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	orders := make([]OrderCRM, 0)
	query := db.ModelContext(ctx, &orders).Where("deleted is not true")
	if err := cr.truncOrdersByDate(query); err != nil {
		return nil, 0, err
	}

	if cr.UserUUID != "" {
		query.Where("start_user_uuid = ?", cr.UserUUID)
	}
	if cr.OrderState != "" {
		query.Where("order_state = ?", cr.OrderState)
	}
	if cr.StoreUUID != "" {
		query.Where("products_data -> 'store' ->> 'uuid' = ?", cr.StoreUUID)
	}
	if cr.OnlyProductsDelivery {
		query.Where("products_data -> 'store' ->> 'uuid' is not null")
	}
	if cr.RouteFrom != "" {
		query.Where("routes -> 0 ->> 'unrestricted_value'  ILIKE ?", cr.RouteFrom+"%")
	}
	if cr.RouteTo != "" {
		query.Where("routes -> 1 ->> 'unrestricted_value'  ILIKE ?", cr.RouteTo+"%")
	}
	if cr.ClientPhone != "" {
		query.Where("client ->> 'main_phone' ILIKE ?", "%"+cr.ClientPhone+"%")
	}
	if cr.ServiceUUID != "" {
		query.Where("sr_uuid = ?", cr.ServiceUUID)
	}
	if cr.DriverAlias != "" {
		query.Where("driver ->> 'alias'  = ?", cr.DriverAlias)
	}
	if len(cr.OrderSource) != 0 {
		query.WhereIn("source IN (?)", cr.OrderSource)
	}
	if len(cr.SourceUUIDs) != 0 {
		query.WhereIn("source_of_orders_uuid IN (?)", cr.SourceUUIDs)
	}
	if len(cr.RegionIDs) != 0 {
		query.WhereIn("region_id IN (?)", cr.RegionIDs)
	}
	if len(cr.TaxiParksUUID) != 0 {
		query.WhereIn("taxi_park_uuid IN (?)", cr.TaxiParksUUID)
	}
	if cr.ID != 0 {
		query.Where("id = ?", cr.ID)
	}
	if cr.Active {
		query.WhereIn("order_state in (?)", constants.ListActiveOrderStates())
	}
	if cr.OperatorLoginTelephony != 0 {
		query.Where("start_user_login_telephony = ?", cr.OperatorLoginTelephony)
	}
	if len(cr.FeatureUUID) != 0 {
		query.Where("features_uuids @> ?", cr.FeatureUUID)
	}
	if cr.PhoneLine != "" {
		query.Where("owner ->> 'name' = ?", cr.PhoneLine)
	}
	if cr.ImportantFirst {
		if !cr.Active {
			return nil, 0, errors.New(`Важные заказы можно получить только при активном флаге "Активные"`)
		}

		query.OrderExpr("cardinality(COALESCE(importance_reasons, '{}')) desc") //сортировка по кол-ву элементов в этом массиве
	}
	if cr.CounterOrdersOnly {
		query.Where("is_counter")
	}

	count, err := cr.getCount(query)
	if err != nil {
		return nil, 0, errors.Wrap(err, "Ошибка подсчёта кол-ва заказов")
	}

	query.Order("created_at desc").Apply(cr.Pager.Pagination)
	if query.Select(); err != nil {
		return nil, 0, errors.Wrap(err, "Ошибка при поиске заказов")
	}

	return orders, count, nil
}

// MarkReportedField marks ReportedAppointment field as true if order in order_start state or ReportedArrival if
// it is in on_place state
func MarkReportedField(orderUUID string, callType ...string) error {
	var order OrderCRM
	err := models.GetByUUID(orderUUID, &order)
	if err != nil {
		return err
	}
	query := db.Model(&order).
		Where("uuid = ?", orderUUID)

	f := false
	if len(callType) > 0 {
		switch callType[0] {
		case structures.OnplaceAutoCall:
			query = query.
				Set("reported_arrival = true")
			f = true
		case structures.OnTheWayAutoCall:
			query = query.
				Set("reported_appointment = true")
			f = true
		default:
			return errors.Errorf("order in invalid state for this function [%s]", order.OrderState)
		}
	}
	if f {
		_, err = query.
			Update()
	}
	return err
}

//OrderList - return all Orders
func OrderList(pager urlvalues.Pager, allowedTaxiParks []string) ([]OrderCRM, int, error) {
	var (
		orders []OrderCRM
	)
	count, err := db.Model(&orders).
		Where("deleted is not true").
		Order("created_at DESC").
		Count()
	if err != nil {
		return orders, count, errors.Errorf("error counting the number of records,%s", err)
	}
	if count == 0 {
		return orders, count, nil
	}
	query := db.Model(&orders).
		Where("deleted is not true")

	query = filterOrdersByTaxiPark(query, allowedTaxiParks)

	err = query.Order("created_at DESC").
		Apply(pager.Pagination).
		Select()

	if err != nil {
		return orders, count, errors.Errorf("error select orders,%s", err)
	}
	return orders, count, nil
}

//OrderByUser - return all Orders by user
func OrderByUser(uuid string) ([]OrderCRM, error) {
	var orders []OrderCRM
	err := db.Model(&orders).
		Where("deleted is not true AND start_user_uuid = ?", uuid).
		Select()
	if err != nil {
		return orders, err
	}
	return orders, nil
}

// OrdersByDriver - return all Orders by driver
func OrdersByDriver(uuid string) ([]OrderCRM, error) {
	var orders []OrderCRM
	err := db.Model(&orders).
		Where("deleted is not true AND driver ->> 'uuid'  ILIKE ?", uuid).
		Select()
	if err != nil {
		return orders, err
	}
	return orders, nil
}

func (or *OrderCRM) featuresList() []string {
	var fs []string
	if len(or.Features) == 0 {
		return fs
	}
	for _, f := range or.Features {
		fs = append(fs, f.UUID)
	}
	return fs
}

// SetStateByUser godoc
func SetStateByUser(uuid, newstate string, uid int) (OrderCRM, error) {
	var nOrder OrderCRM
	nOrder.UUID = uuid

	_, err := db.Model(&nOrder).
		Set("order_state_uuid = ?, last_user_id = ?", newstate, uid).
		Where("uuid = ?", uuid).
		Returning("*").
		Update()
	if err != nil {
		return OrderCRM{}, err
	}

	return nOrder, nil
}

// SetDeleted godoc
func (or *OrderCRM) SetDeleted(userTaxiParksUUID []string) *structures.ErrorWithLevel {
	// TODO: вынести в общие методы и сделать его универсальным
	err := models.GetByUUID(or.UUID, or)
	if err != nil {
		return structures.Error(err)
	}
	acces := structures.StringInArray(or.GetTaxiParkUUID(), userTaxiParksUUID)
	if userTaxiParksUUID != nil && acces {
		return structures.Warningf(AccessDeniedMsg)
	}
	_, err = db.Model(or).
		Where("uuid=?uuid").
		Set("deleted = true").
		Update()
	return structures.Error(err)
}

// FillTripTime fills trip duration after on_the_way state or nil
func FillTripTime(orders ...OrderCRM) []OrderCRM {
	var (
		orStates                            []OrderStateCRM
		uuids                               []string
		wayTime, onTheWayTime, finishedTime int64
		checkFindOnTheWay                   bool
	)
	//orders[0].DriverArrivalTime
	if len(orders) == 0 {
		return nil
	} else {
		// пока не напишем эту функция нормально
		return orders
	}
	for _, order := range orders {
		uuids = append(uuids, order.UUID)
	}
	timeNowUnix := time.Now().Unix()
	err := db.Model(&orStates).
		Where("order_uuid in (?)", pg.In(uuids)).
		Where("state = ? or state = ?", constants.OrderStateOnTheWay, constants.OrderStateFinished).
		Order("start_state").
		Select()
	// чтобы ошибка не возвращалась, но мы ее хотя бы видели
	if err != nil {
		fmt.Printf("error getting order states,%s\n", err)
		return orders
	}
	for i := range orders {
		wayTime, onTheWayTime, finishedTime = 0, 0, 0
		checkFindOnTheWay = false
		if !orders[i].isRunning() {
			orders[i].TripTime = -1
			continue
		}
		for _, state := range orStates {
			if state.State == constants.OrderStateOnTheWay && state.OrderUUID == orders[i].UUID {
				onTheWayTime = state.StartState.Unix()
				wayTime = timeNowUnix - onTheWayTime
				checkFindOnTheWay = true
				break
			}
		}
		if checkFindOnTheWay {
			for _, state := range orStates {
				if state.State == constants.OrderStateFinished && state.OrderUUID == orders[i].UUID {
					finishedTime = state.StartState.Unix()
					wayTime = finishedTime - onTheWayTime
					break
				}
			}
			orders[i].TripTime = wayTime
		} else {
			orders[i].TripTime = -1
		}

	}
	return orders
}

func (or *OrderCRM) isRunning() bool {
	if or.OrderState == constants.OrderStateOnTheWay ||
		or.OrderState == constants.OrderStateWaiting ||
		or.OrderState == constants.OrderStatePayment ||
		or.OrderState == constants.OrderStateFinished {
		return true
	}
	return false
}

func UpdateOrderRating(order *OrderCRM) error {
	_, err := db.Model(order).
		Set("driver_rating = ?driver_rating").
		Set("client_rating = ?client_rating").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}
	return nil
}

//
func UpdateOrderRouteToClient(order *OrderCRM) error {
	_, err := db.Model(order).
		Set("route_way_data = ?route_way_data").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}

	return nil
}

func ifActiveState(state string) bool {
	for _, st := range constants.ListActiveOrderStates() {
		if state == st {
			return true
		}
	}
	return false
}
func SetOrderState(state *structures.OfferStates) error {
	orStateCRM := OrderStateCRM{
		OfferStates: *state,
	}

	if _, err := orStateCRM.NewOrderState(); err != nil {
		return err
	}

	if orStateCRM.State == constants.OrderStateAccepted {
		var orderCRM OrderCRM
		err := models.GetByUUID(orStateCRM.OrderUUID, &orderCRM)
		if err != nil {
			return errors.Errorf("error getting order by uuid, %s", err)
		}
		err = UpdateOrderDriverArrivalTime(orStateCRM.OrderUUID, orStateCRM.Comment)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":             "order update(add driver_arrival_time) [crm]",
				"reason":            "Error updating order",
				"orderUUID":         orStateCRM.OrderUUID,
				"orderStateComment": orStateCRM.Comment,
			}).Error(err)
		}
		err = rabsender.SendJSONByAction(rabsender.AcNewOrderDataWhenDriverAccepted, orderCRM.Order)
		if err != nil {
			return errors.Errorf("error send data to broker, %s", err)
		}
	}
	if orStateCRM.State == constants.OrderStateOnPlace {

		if err := UpdateDriverArrivedInFactTime(orStateCRM.OrderUUID); err != nil {
			return errors.Wrap(err, "error Update Driver Arrived In Fact Time")
		}
	}
	return nil
}
func GetOrdersDataByClientUUID(clUUID string) ([]structures.OrderWithDataForClient, error) {
	var orders []OrderCRM
	var result []structures.OrderWithDataForClient
	err := db.Model(&orders).
		Where("order_state in (?)", pg.In(constants.ListActiveOrderStates())).
		Where("client ->> 'uuid' = ?", clUUID).
		Where("deleted IS NOT TRUE").
		Returning("uuid,state_transfer_time,driver_arrival_time_data,order_state,driver ->> 'uuid'").
		Select()
	if err != nil {
		return result, err
	}
	for _, item := range orders {
		var freeTime int64
		if item.inWaitingStates() {
			pastTime := time.Now().Unix() - item.StateTransferTime.Unix()
			freeTime = int64(item.DriverArrivalTimeData.RemainingWaitingTime) - pastTime
		}

		result = append(result, structures.OrderWithDataForClient{
			UUID:                 item.UUID,
			State:                item.OrderState,
			DriverUUID:           item.Driver.UUID,
			StateTransferTime:    item.StateTransferTime,
			DriverArrivalTime:    item.DriverArrivalTimeData.ArrivedInFact,
			RemainingWaitingTime: freeTime,
		})
	}
	return result, nil
}
func (or *OrderCRM) inWaitingStates() bool {
	return or.OrderState == constants.OrderStateWaiting ||
		or.OrderState == constants.OrderStateOnPlace
}

func GetDriverCurrentOrder(driverUUID string, counterOrder bool) (structures.OrderUUIDWithStateTransferState, error) {
	var order OrderCRM

	query := db.Model(&order).
		WhereIn("order_state in (?)", constants.ListActiveOrderStates()).
		Where("driver ->> 'uuid' = ?", driverUUID).
		Where("deleted IS NOT TRUE").
		Where("counter_order_marker = ?", counterOrder).
		Returning("state_transfer_time, uuid, order_state")
	if err := query.Select(); err != nil && err != pg.ErrNoRows {
		return structures.OrderUUIDWithStateTransferState{}, err
	}

	return structures.OrderUUIDWithStateTransferState{
		UUID:           order.UUID,
		StateTransTime: order.StateTransferTime,
		State:          order.OrderState,
	}, nil
}

func GetDriversCurrentOrderUUIDs(driverUUID ...string) (map[string]string, error) {
	var orders []OrderCRM

	query := db.Model(&orders).
		Column("uuid", "driver").
		WhereIn("order_state in (?)", constants.ListActiveOrderStates()).
		WhereIn("driver ->> 'uuid' IN (?)", driverUUID).
		Where("deleted IS NOT TRUE").
		Where("counter_order_marker = false")
	if err := query.Select(); err != nil && err != pg.ErrNoRows {
		return nil, err
	}

	result := make(map[string]string, len(orders))
	for _, o := range orders {
		result[o.Driver.UUID] = o.UUID
	}

	return result, nil
}

// CounterOrderExists возвращает true, если у этого водителя есть активный встречный заказ
// важно: этот запрос должен совпадать с подзапросом в getWorkingDriversForDistribution
func CounterOrderExists(driverUUID string) (bool, error) {
	query := db.Model((*OrderCRM)(nil)).
		WhereIn("order_state in (?)", constants.ListActiveOrderStates()).
		Where("driver ->> 'uuid' = ?", driverUUID).
		Where("counter_order_marker = true").
		Where("deleted IS NOT TRUE")
	return query.Exists()
}

// MakeCounterOrderCurrentIfNeed - делает взятый встречный заказ текущим
func MakeCounterOrderCurrentIfNeed(driverUUID string) error {
	order, err := GetDriverCurrentOrder(driverUUID, true)
	if err != nil {
		return err
	}
	if order.UUID == "" {
		// нет встречного заказа
		return nil
	}

	return SetCounterOrderMarkerForOrder(order.UUID, false, false)
}

func OrdersDataByState(state string) ([]structures.OrderUUIDWithStateTransferState, error) {
	var orders []OrderCRM
	var result []structures.OrderUUIDWithStateTransferState
	err := db.Model(&orders).
		Where("order_state = ?", state).
		Where("deleted IS NOT TRUE").
		Select()
	for _, or := range orders {
		result = append(result, structures.OrderUUIDWithStateTransferState{
			UUID:           or.UUID,
			DriverUUID:     or.Driver.UUID,
			State:          or.OrderState,
			StateTransTime: or.StateTransferTime,
		})
	}
	return result, err
}

func OrdersDataByStates(states []string, regionID ...int) ([]structures.OrderUUIDWithStateTransferState, error) {
	var result []structures.OrderUUIDWithStateTransferState
	q := db.Model((*OrderCRM)(nil)).
		Column("uuid").
		ColumnExpr("driver ->> 'uuid' AS driver_uuid").
		ColumnExpr("state_transfer_time AS state_trans_time").
		ColumnExpr("order_state AS state").
		WhereIn("order_state IN (?)", states).
		Where("deleted IS NOT TRUE")
	if len(regionID) > 0 {
		q = q.WhereIn("region_id IN (?)", regionID)
	}
	err := q.Select(&result)

	return result, err
}

func UUIDByStates(states ...string) ([]string, error) {
	var orders []OrderCRM
	var result []string
	err := db.Model(&orders).
		WhereIn("order_state IN (?)", states).
		Where("deleted IS NOT TRUE").
		Select()
	for _, or := range orders {
		result = append(result, or.UUID)
	}
	return result, err
}

func SaveOrderFromChat(ord *OrderCRM) error {
	_, err := db.Model(ord).Returning("*").Insert()
	return err
}

func GetOrdersWithExpiredConfirmationTime() ([]string, error) {
	var uuids []string

	err := db.Model(&OrderCRM{}).
		Where("order_state = ?", constants.OrderStateConfirmation).
		Where("(extract(epoch from created_at) + ?) < extract(epoch from now())", int(ProductDeliveryOrdersConfirmationTime.Seconds())).
		Where("deleted IS NOT TRUE").
		Column("uuid").
		Select(&uuids)
	return uuids, err
}

func GetOrdersForFindDriver() ([]OrderCRM, error) {
	var orders []OrderCRM

	err := db.Model(&orders).
		Where("order_state = ?", constants.OrderStateCooking).
		Where("own_delivery is not true").
		Where("without_delivery IS NOT TRUE").
		Where("own_delivery is not true").
		Where("(extract(epoch from state_transfer_time) + (products_data ->> 'preparation_time')::integer) < ?", int(time.Now().Add(ProductDeliveryOrdersFindDriverTime).Unix())).
		Where("deleted IS NOT TRUE").
		Select()
	return orders, err
}
func InformDriverAboutOrderReadiness(orderUUID, userStoreUUID, message string) *structures.ErrorWithLevel {
	var order OrderCRM
	err := models.GetByUUID(orderUUID, &order)
	if err != nil {
		return structures.Error(err)
	}
	if !order.Service.ProductDelivery {
		return structures.Warning(errors.Errorf("invalid order type for this action"))
	}
	if order.GetProductsData().StoreData.UUID != userStoreUUID {
		return structures.Warning(errors.Errorf("Вам недоступны действия с заказами этого магазина"))
	}
	if order.OrderState != constants.OrderStateStarted &&
		order.OrderState != constants.OrderStateOnPlace {
		return structures.Warning(errors.Errorf("Заказ в неподходящем статусе для этого действия"))
	}
	if message == "" {
		message = "Заказ приготовлен и ждет героя, который заберет его"
	}
	phMailing := models.PushMailingCRM{
		PushMailing: structures.PushMailing{
			Message:      message,
			Title:        "Можно забирать заказ!",
			TargetsUUIDs: []string{order.Driver.UUID},
			Type:         structures.MailingInformDriverType,
		},
	}
	err = rabsender.SendJSONByAction(rabsender.AcNewPushMailing, phMailing)
	if err != nil {
		return structures.Error(errors.Errorf("error sending data to broker, %s", err))
	}
	return nil
}

func SetOrderNumberInStore(orderUUID, userStoreUUID, orderNumber string) (OrderCRM, *structures.ErrorWithLevel) {
	var order OrderCRM
	query := db.Model(&order).
		Where("uuid = ?", orderUUID).
		Where("deleted is not true")
	err := query.
		Select()
	if err != nil {
		return order, structures.Error(err)
	}
	if !order.Service.ProductDelivery {
		return order, structures.Warning(errors.Errorf("Заказ не подходит для этого действия"))
	}
	for _, invalidState := range constants.ListInactiveOrderStates() {
		if order.OrderState == invalidState {
			return order, structures.Warning(errors.Errorf("Заказ уже завершен"))
		}
	}
	if order.ProductsData == nil || order.GetProductsData().StoreData.UUID != userStoreUUID {
		return order, structures.Warning(errors.Errorf("Это действие вам недоступно"))
	}
	order.ProductsData.OrderNumberInStore = orderNumber

	_, err = query.Set("products_data = ?products_data").Returning("*").Update()
	if err != nil {
		return order, structures.Error(errors.Errorf("error update order products_data, %s", err))
	}
	return order, nil
}

func ConfirmOrder(orderUUID, userStoreUUID string, prepareTime *int, ownDelivery bool) *structures.ErrorWithLevel {
	var order OrderCRM
	query := db.Model(&order).
		Where("uuid = ?", orderUUID).
		Where("deleted is not true").
		Returning("*")
	err := query.
		Select()
	if err != nil {
		return structures.Error(err)
	}
	if !order.Service.ProductDelivery {
		return structures.Warning(errors.Errorf("Этот заказ не требует подтверждения"))
	}
	if order.OrderState != constants.OrderStateConfirmation {
		return structures.Warning(errors.Errorf("Этот заказ не ожидает подтверждения"))
	}
	// if order.GetProductsData().StoreData.UUID != userStoreUUID {
	// 	return structures.Warning(errors.Errorf("Вы не можете подтвердить этот заказ"))
	// }

	comment := "Время подготовки заказа стандартое для заведения"
	if prepareTime != nil {
		prepareTimeMin := float64(*prepareTime) / 60
		comment = fmt.Sprintf("Время подготовки заказа указано вручую: %v минут", prepareTimeMin)
		_, err = query.Set(fmt.Sprintf("products_data = jsonb_set(COALESCE(products_data, '{}'), '{preparation_time}', '%v')", *prepareTime)).
			Update()
		if err != nil {
			return structures.Error(errors.Errorf("error updating order preparation time, %s", err))
		}
	}
	_, err = db.Model(&order).
		Where("uuid = ?", orderUUID).
		Set(fmt.Sprintf("products_data = jsonb_set(COALESCE(products_data, '{}'), '{confirmation_time}', '%v')", time.Now().Unix())).
		Returning("*").
		Update()
	if err != nil {
		return structures.Error(errors.Errorf("error updating order confirmation_time time, %s", err))
	}
	// Update if the order has changed
	if order.OwnDelivery != ownDelivery {
		order.OwnDelivery = ownDelivery
		_, err = db.Model(&order).
			Where("uuid = ?uuid").
			Set("own_delivery = ?own_delivery").
			Update()
		if err != nil {
			return structures.Error(errors.Wrap(err, "failed to update an order"))
		}
		if err = rabsender.SendJSONByAction(rabsender.AcNewOrderData, order); err != nil {
			return structures.Error(errors.Wrap(err, "failed to notify about the updated order"))
		}
	}

	newState := constants.OrderStateCooking
	if order.WithoutDelivery {
		newState = constants.OrderStateTransToStore
	}
	orState := OrderStateCRM{
		OfferStates: structures.OfferStates{
			State:      newState,
			OrderUUID:  order.UUID,
			StartState: time.Now(),
			Comment:    comment,
		},
	}
	orState, err = orState.NewOrderState()
	if err != nil {
		return structures.Error(err)
	}
	err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState)
	if err != nil {
		return structures.Error(errors.Errorf("error sending new order state, %s", err))
	}
	err = RemoveImportantReasonFromOrder(orderUUID, WaitingForConformitionState)
	if err != nil {
		return structures.Error(errors.Errorf("error remove important reason from order, %s", err))
	}
	return nil
}

func GetCookedOrdersWithOwnDelivery(ctx context.Context) ([]OrderCRM, error) {
	var result []OrderCRM
	err := db.ModelContext(ctx, &result).
		Where("order_state = ?", constants.OrderStateCooking).
		Where("products_data -> 'store' ->> 'uuid' IS NOT NULL"). // detect if a store exists using an index
		Where("own_delivery").
		Where("CURRENT_TIMESTAMP >= state_transfer_time + interval '1 second' * (products_data  ->> 'preparation_time')::integer ").
		Select()
	return result, err
}

// TransToStateAndSetDeliveryTime для автоматического перевода заказов, у которых своя доставка
func TransToStateAndSetDeliveryTime(orderUUID, state string) error {
	order, err := UpdateOrderDeliveryTime(orderUUID)
	if err != nil {
		return errors.New("error update order delivery time")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcNewOrderData, order); err != nil {
		return errors.Wrap(err, "failed to notify about the updated order")
	}

	orState := OrderStateCRM{
		OfferStates: structures.OfferStates{
			State:      state,
			OrderUUID:  order.UUID,
			StartState: time.Now(),
			Comment:    fmt.Sprintf("Временная оценка доставки рассчитата автоматически (%v секунд)", order.EstimatedDeliveryTime),
		},
	}
	orState, err = orState.NewOrderState()
	if err != nil {
		return errors.Wrap(err, "failed to create a new order state")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
		return errors.Wrap(err, "failed to notify about the new order state")
	}
	return nil
}
func EstimateDelivery(ctx context.Context, orderUUID, userStoreUUID string, estimation int64) error {
	estimationTime := time.Now().Add(time.Second * time.Duration(estimation))
	if estimationTime.Before(time.Now()) {
		return errors.New("указанное время уже прошло")
	}

	var order OrderCRM
	err := db.ModelContext(ctx, &order).
		Where("uuid = ?", orderUUID).
		Where("deleted is not true").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to fetch an order")
	}

	if !order.Service.ProductDelivery {
		return errors.New("этот заказ не требует оценки времени доставки")
	}
	if order.OrderState != constants.OrderStateTransToStore {
		return errors.New("для этого заказа уже нельзя указать оценочное время доставки")
	}
	if order.GetProductsData().StoreData.UUID != userStoreUUID {
		return errors.New("вы не можете дать временную оценку этому заказу")
	}

	order.EstimatedDeliveryTime = estimationTime
	order.ArrivalTime = order.EstimatedDeliveryTime.Unix()
	_, err = db.ModelContext(ctx, &order).
		Where("uuid = ?uuid").
		Set("estimated_delivery_time = ?estimated_delivery_time").
		Set("arrival_time = ?arrival_time").
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update an order")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcNewOrderData, order); err != nil {
		return errors.Wrap(err, "failed to notify about the updated order")
	}

	orState := OrderStateCRM{
		OfferStates: structures.OfferStates{
			State:      constants.OrderStateOnTheWay,
			OrderUUID:  order.UUID,
			StartState: time.Now(),
			Comment:    "Временная оценка доставки заказа указана вручую",
		},
	}
	orState, err = orState.NewOrderState()
	if err != nil {
		return errors.Wrap(err, "failed to create a new order state")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
		return errors.Wrap(err, "failed to notify about the new order state")
	}
	return nil
}

func FinishDelivery(ctx context.Context, orderUUID, userStoreUUID string) error {
	var order OrderCRM
	err := db.ModelContext(ctx, &order).
		Where("uuid = ?", orderUUID).
		Where("deleted is not true").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to fetch an order")
	}

	if !order.Service.ProductDelivery {
		return errors.New("этот заказ не требует ручного завершения")
	}
	orderStoreUUID := order.GetProductsData().StoreData.UUID

	if userStoreUUID != "" &&
		orderStoreUUID != userStoreUUID {
		return errors.New("вы не можете завершить этот заказ")
	}

	orState := OrderStateCRM{
		OfferStates: structures.OfferStates{
			State:      constants.OrderStateFinished,
			OrderUUID:  order.UUID,
			StartState: time.Now(),
			Comment:    "Заказ завершен вручную",
		},
	}
	orState, err = orState.NewOrderState()
	if err != nil {
		return errors.Wrap(err, "failed to create a new order state")
	}
	if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
		return errors.Wrap(err, "failed to notify about the new order state")
	}
	return nil
}

func UpdateOrderDeliveryTime(orderUUID string) (OrderCRM, error) {
	var order OrderCRM

	err := models.GetByUUID(orderUUID, &order)
	if err != nil {
		return order, err
	}

	if len(order.Routes) < 2 {
		return order, errors.New("Необходимо 2 или более точек назначения")
	}

	routeWayData, err := GetRouteWayData(order.Routes)
	if err != nil {
		return order, err
	}
	arrTime := time.Now().Unix() + int64(float32(routeWayData.Geometry.Proper.Duration)*structures.OrderDurationCoeff)
	_, err = db.Model(&order).Set("estimated_delivery_time = ?", time.Unix(arrTime, 0)).
		Where("uuid = ?", orderUUID).
		Update()
	return order, err
}
func PayExpiredDeliveryOrders(ctx context.Context) error {
	var orders []*OrderCRM
	err := db.ModelContext(ctx, &orders).
		Where("products_data -> 'store' ->> 'uuid' IS NOT NULL"). // detect if a store exists using an index
		Where("order_state = ?", constants.OrderStateOnTheWay).
		Where("estimated_delivery_time IS NOT NULL AND CURRENT_TIMESTAMP >= estimated_delivery_time").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to select expired orders")
	}

	for _, order := range orders {
		orState := OrderStateCRM{
			OfferStates: structures.OfferStates{
				State:      constants.OrderStatePayment,
				OrderUUID:  order.UUID,
				StartState: time.Now(),
				Comment:    "Оценка времени доставки истекла",
			},
		}
		orState, err = orState.NewOrderState()
		if err != nil {
			return errors.Wrap(err, "failed to create a new order state")
		}
		if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
			return errors.Wrap(err, "failed to notify about the new order state")
		}
	}
	return nil
}

func FinishExpiredPayedDeliveryOrders(ctx context.Context) error {
	var orders []*OrderCRM
	err := db.ModelContext(ctx, &orders).
		Where("products_data -> 'store' ->> 'uuid' IS NOT NULL"). // detect if a store exists using an index
		Where("order_state = ?", constants.OrderStatePayment).
		Where("CURRENT_TIMESTAMP >= state_transfer_time + interval '1 minute' * ?", ProductDeliveryOrdersPaymentMinutes).
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to select expired orders")
	}

	for _, order := range orders {
		orState := OrderStateCRM{
			OfferStates: structures.OfferStates{
				State:      constants.OrderStateFinished,
				OrderUUID:  order.UUID,
				StartState: time.Now(),
				Comment:    "Заказ завершен автоматически",
			},
		}
		orState, err = orState.NewOrderState()
		if err != nil {
			return errors.Wrap(err, "failed to create a new order state")
		}
		if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
			return errors.Wrap(err, "failed to notify about the new order state")
		}
	}
	return nil
}

func PayExpiredSelfPickupOrders(ctx context.Context) error {
	var orders []*OrderCRM
	err := db.ModelContext(ctx, &orders).
		Where("products_data -> 'store' ->> 'uuid' IS NOT NULL"). // detect if a store exists using an index
		Where("order_state = ?", constants.OrderStateTransToStore).
		Where("without_delivery IS TRUE").
		Where("CURRENT_TIMESTAMP >= state_transfer_time + interval '1 second' * ((products_data ->> 'preparation_time')::integer)").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to select expired self pickup orders")
	}

	for _, order := range orders {
		orState := OrderStateCRM{
			OfferStates: structures.OfferStates{
				State:      constants.OrderStatePayment,
				OrderUUID:  order.UUID,
				StartState: time.Now(),
				Comment:    "Оценка времени приготовления истекла",
			},
		}
		orState, err = orState.NewOrderState()
		if err != nil {
			return errors.Wrap(err, "failed to create a new order state")
		}
		if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
			return errors.Wrap(err, "failed to notify about the new order state")
		}
	}
	return nil
}

func FinishExpiredSelfPickupOrders(ctx context.Context) error {
	var orders []*OrderCRM
	err := db.ModelContext(ctx, &orders).
		Where("products_data -> 'store' ->> 'uuid' IS NOT NULL"). // detect if a store exists using an index
		Where("order_state = ?", constants.OrderStatePayment).
		Where("without_delivery IS TRUE").
		Where("CURRENT_TIMESTAMP >= state_transfer_time + interval '1 minute' * ?", ProductDeliveryOrdersPaymentMinutes).
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to select expired self pickup orders")
	}

	for _, order := range orders {
		orState := OrderStateCRM{
			OfferStates: structures.OfferStates{
				State:      constants.OrderStateFinished,
				OrderUUID:  order.UUID,
				StartState: time.Now(),
				Comment:    "Заказ завершен автоматически",
			},
		}
		orState, err = orState.NewOrderState()
		if err != nil {
			return errors.Wrap(err, "failed to create a new order state")
		}
		if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
			return errors.Wrap(err, "failed to notify about the new order state")
		}
	}
	return nil
}

func (or *OrderCRM) SendToDriver() error {
	dur, _ := time.ParseDuration(config.St.Application.DistributingTime)
	or.CancelTime = time.Now().Add(dur)
	return rabsender.SendJSONByAction(rabsender.AcOrderToDriver, or)
}

// ---------
// ---------
// ---------

// SetCounterOrderMarkerForOrder - устанавливает метку встречного заказа для заказа. Если updatePersistentMarker - true,
// дополнительно обновляет значение поля заказа IsCounter
func SetCounterOrderMarkerForOrder(orderUUID string, marker bool, updatePersistentMarker bool) error {
	query := db.Model((*OrderCRM)(nil)).
		Where("uuid = ?", orderUUID).
		Set("counter_order_marker = ?", marker)
	if updatePersistentMarker {
		query.Set("is_counter = ?", marker)
	}
	if _, err := query.Update(); err != nil {
		return err
	}

	return nil
}

func GetCounterOrderMarkerForOrder(orderUUID string) (bool, error) {
	var marker bool
	query := db.Model((*OrderCRM)(nil)).
		Where("uuid = ?", orderUUID).
		Column("counter_order_marker")
	if err := query.Select(&marker); err != nil {
		return false, err
	}

	return marker, nil
}

// ---------
// ---------
// ---------
func filterOrdersByTaxiPark(query *orm.Query, allowedTaxiParks []string) *orm.Query {
	if len(allowedTaxiParks) != 0 {
		query = query.WhereIn("taxi_park_uuid IN (?)", allowedTaxiParks)
	}
	return query
}

// FIXME временный костыль, нужно разобраться с SourceOfOrdersUUID при внутренней передаче структуры заказа для перерасчетов
func (o *OrderCRM) LoadSourceUUID() error {
	query := db.Model(o).
		Where("uuid = ?uuid").
		Column("source_of_orders_uuid")
	if err := query.Select(&o.SourceOfOrdersUUID); err != nil {
		return err
	}

	return nil
}
