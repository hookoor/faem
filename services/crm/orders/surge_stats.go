package orders

import (
	"context"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"time"
)

type SurgeZone struct {
	Drivers []*models.DriverCRM
	Orders  []*OrderCRM
	// Костыль чтобы различать заказ старует из зоны или заканчивается в ней.
	// false означает, что зона является конечной точкой заказа
	OrderStartFlags []bool
}

type SurgeMap map[[2]int]*SurgeZone

func (surgeMap SurgeMap) GetOrCreateZone(row, col int) *SurgeZone {
	key := [2]int{row, col}
	if zone, found := surgeMap[key]; found {
		return zone
	} else {
		zone = new(SurgeZone)
		surgeMap[key] = zone
		return zone
	}
}

func BuildSurgeMap(orders []*OrderCRM, drivers []*models.DriverCRM) SurgeMap {
	surgeMap := make(SurgeMap)

	addOrderToMap := func(order *OrderCRM, start bool) {
		var route structures.Route
		if start {
			route = order.Routes[0]
		} else {
			route = order.Routes[len(order.Routes)-1]
		}

		row, col := CurrentServiceCorrection.findRectGeoZone(float64(route.Lat), float64(route.Lon))
		zone := surgeMap.GetOrCreateZone(row, col)
		zone.Orders = append(zone.Orders, order)
		zone.OrderStartFlags = append(zone.OrderStartFlags, start)
	}

	for _, order := range orders {
		if len(order.Routes) == 0 {
			continue
		}

		addOrderToMap(order, true)
		addOrderToMap(order, false)
	}

	driverUUIDs := make([]string, 0, len(drivers))
	for _, driver := range drivers {
		driverUUIDs = append(driverUUIDs, driver.UUID)
	}
	driverLocations := models.CurrentDriverLocationsByUUIDs(driverUUIDs)

	for _, driver := range drivers {
		if loc, found := driverLocations[driver.UUID]; found {
			row, col := CurrentServiceCorrection.findRectGeoZone(loc.Latitude, loc.Longitude)
			zone := surgeMap.GetOrCreateZone(row, col)
			zone.Drivers = append(zone.Drivers, driver)
		}
	}

	return surgeMap
}

func (surgeZone SurgeZone) SplitByService() map[string]*SurgeZone {
	serviceMap := make(map[string]*SurgeZone)

	for _, driver := range surgeZone.Drivers {
		for _, service := range driver.AvailableServices {
			if zone, found := serviceMap[service.UUID]; found {
				zone.Drivers = append(zone.Drivers, driver)
			} else {
				// create new zone
				serviceMap[service.UUID] = &SurgeZone{
					Drivers: []*models.DriverCRM{driver},
				}
			}
		}
	}

	for i, order := range surgeZone.Orders {
		serviceUUID := order.Service.UUID
		startFlag := surgeZone.OrderStartFlags[i]
		if zone, found := serviceMap[serviceUUID]; found {
			zone.Orders = append(zone.Orders, order)
			zone.OrderStartFlags = append(zone.OrderStartFlags, startFlag)
		} else {
			// create new zone
			serviceMap[serviceUUID] = &SurgeZone{
				Orders:          []*OrderCRM{order},
				OrderStartFlags: []bool{startFlag},
			}
		}
	}

	return serviceMap
}

func (sc *ServiceCorrection) CalcSurgeZoneStats(surgeMap SurgeMap) error {
	zoneSize := sc.config.ZoneSize
	ctx := context.Background()

	var newSurgesZonesCRM []models.SurgeZoneCRM
	var surgeHistoryItems []models.SurgeHistoryItemCRM

	timestamp := models.SurgeTimestampCRM{
		UUID:      structures.GenerateUUID(),
		CreatedAt: time.Now(),
	}

	// to check if zone already exists in db
	allZones, _ := models.GetSurgeZones(ctx, models.SurgeZoneFilter{})
	zonesMap := make(map[string]struct{})
	for _, zone := range allZones {
		zonesMap[zone.UUID] = struct{}{}
	}

	for dualIndex, zone := range surgeMap {
		row := dualIndex[0]
		col := dualIndex[1]
		uuid := structures.MakeSurgeZoneUUID(zoneSize, row, col)
		if _, found := zonesMap[uuid]; !found {
			// calculate polygon to create new entry in db
			rect := gis.CalcSurgeZonePolygon(zoneSize, row, col)
			surgeZoneCRM := models.SurgeZoneCRM{
				UUID:    uuid,
				Polygon: rect,
			}
			newSurgesZonesCRM = append(newSurgesZonesCRM, surgeZoneCRM)
		}

		serviceMap := zone.SplitByService()

		for serviceUUID, serviceZone := range serviceMap {
			surgeData := new(structures.SurgeDataV2)
			surgeData.Drivers = make(map[string]*structures.SurgeDriverInfo)
			surgeData.Orders = make(map[string]*structures.SurgeOrderInfo)

			for _, driver := range serviceZone.Drivers {
				status := driver.Status
				// add new struct to map if not present
				if _, found := surgeData.Drivers[status]; !found {
					surgeData.Drivers[status] = new(structures.SurgeDriverInfo)
				}
				surgeData.Drivers[status].DriverCount += 1
			}

			for i, order := range serviceZone.Orders {
				startFlag := serviceZone.OrderStartFlags[i]
				state := order.OrderState
				// add new struct to map if not present
				if _, found := surgeData.Orders[state]; !found {
					surgeData.Orders[state] = new(structures.SurgeOrderInfo)
				}

				if startFlag {
					surgeData.Orders[state].OrderStartCount += 1
				} else {
					surgeData.Orders[state].OrderEndCount += 1
				}
			}

			surgeHistoryItem := models.SurgeHistoryItemCRM{
				ZoneUUID:      uuid,
				ServiceUUID:   serviceUUID,
				TimestampUUID: timestamp.UUID,
				SurgeData:     surgeData,
			}
			surgeHistoryItems = append(surgeHistoryItems, surgeHistoryItem)
		}
	}

	if len(newSurgesZonesCRM) > 0 {
		_, err := db.Model(&newSurgesZonesCRM).
			OnConflict("DO NOTHING").
			Insert()
		if err != nil {
			return err
		}
	}

	if len(surgeHistoryItems) > 0 {
		_, err := db.ModelContext(ctx, &timestamp).Insert()
		if err != nil {
			return err
		}

		_, err = db.ModelContext(ctx, &surgeHistoryItems).Insert()
		if err != nil {
			return err
		}
	}

	return nil
}
