package orders

import (
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/constants"

	"github.com/pkg/errors"
)

func ChangeOrderPaymentTypeToCash(orderUUID string) error {
	_, err := db.Model((*OrderCRM)(nil)).
		Set("payment_type = ?", constants.OrderPaymentTypeCash).
		Set(fmt.Sprintf("tariff = jsonb_set(COALESCE(tariff, '{}'), '{payment_type}', '\"%s\"')", constants.OrderPaymentTypeRU[constants.OrderPaymentTypeCash])).
		Where("uuid = ?", orderUUID).
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment type", orderUUID)
	}
	return nil
}
