package orders

import (
	"math"
	"time"

	"github.com/antonmedv/expr"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// TariffSurcharges - сущноть - надбавки на клиентские тариффы
type TariffSurcharges struct {
	tableName struct{}  `sql:"crm_tariff_surcharges"`
	UUID      string    `json:"uuid"`
	Name      string    `json:"name"`
	Priority  int       `json:"priority"`
	IfExpr    string    `json:"if_expr" sql:"if_expr"`
	SurExpr   string    `json:"sur_expr" sql:"sur_expr"`
	RegionID  int       `json:"region_id" sql:"region_id"`
	Deleted   bool      `json:"deleted"`
	CreatedAt time.Time `json:"-"`
}
type evalTariffSurchargesStruct struct {
	TariffPrice int
}

func applyTariffSurchargesIfNeed(reigonID int, ord *OrderTariffChars, tariff *structures.Tariff) error {
	var inputStruct evalTariffSurchargesStruct
	ts, err := findTariffSurcharges(reigonID, ord)
	if err != nil {
		return err
	}
	// если никакой надбавки не найдено
	if ts.SurExpr == "" {
		return nil
	}

	inputStruct.TariffPrice = tariff.TotalPrice
	ast, err := expr.Eval(ts.SurExpr, inputStruct)
	if err != nil {
		return errors.Wrap(err, "error applying expression")
	}

	transactionSum, ok := ast.(float64)
	if !ok {
		return errors.Wrap(err, "type cast error when trying to apply a surcharge")
	}

	transactionSum = math.Round(transactionSum)
	if transactionSum == 0 {
		return nil
	}

	tariff.TotalPrice = int(transactionSum)
	diff := tariff.TotalPrice - inputStruct.TariffPrice
	if diff == 0 {
		return nil
	}

	ti := structures.TariffItem{
		Price: int(diff),
		Type:  structures.TariffItemTypeDefault,
	}
	if diff > 0 {
		ti.Name = "Повышенный коэффициент"
	} else {
		ti.Name = "Пониженный коэффициент"
	}
	tariff.Items = append(tariff.Items, ti)

	return nil
}

// findTariffSurcharges перебирает надбавки и возвращает подходящие
func findTariffSurcharges(reigonID int, ord *OrderTariffChars) (*TariffSurcharges, error) {
	ts := make([]TariffSurcharges, 0)
	query := db.Model(&ts).
		Where("deleted is not true").
		Where("region_id = ?", reigonID).
		Order("priority DESC")
	if err := query.Select(); err != nil {
		return nil, errors.Wrap(err, "error getting tariff rules from DB")
	} else if len(ts) == 0 {
		return &TariffSurcharges{}, nil
	}

	for i, v := range ts {
		ast, err := expr.Parse(v.IfExpr, expr.Env(&OrderTariffChars{}))
		if err != nil {
			return nil, errors.Wrapf(err, "error parsing tariff surcharges - '%s'", v.Name)
		}

		result, err := expr.Run(ast, ord)
		if err != nil {
			return nil, errors.Wrapf(err, "error running tariff surcharges exp - '%s'", v.Name)
		} else if result == true {
			return &ts[i], nil
		}
	}

	return &TariffSurcharges{}, nil
}

func (ts *TariffSurcharges) Update() error {
	if err := ts.validateForUpdate(); err != nil {
		return err
	}

	if err := models.UpdateByPK(ts); err != nil {
		return err
	}

	return nil
}

func GetTariffSurchargeByUUID(uuid string) (TariffSurcharges, error) {
	var result TariffSurcharges
	err := db.Model(&result).
		Where("deleted is not true").
		Where("uuid = ?", uuid).
		Select()
	return result, err
}

func GetTariffSurcharges(regions []int) ([]TariffSurcharges, error) {
	result := make([]TariffSurcharges, 0)
	query := db.Model(&result).Where("deleted is not true").WhereIn("region_id IN (?)", regions)
	if err := query.Order("priority DESC").Select(); err != nil {
		return nil, err
	}

	return result, nil
}

func SetTariffSurchargeDeleted(uuid string) error {
	err := models.CheckExistsUUID(&TariffSurcharges{}, uuid)
	if err != nil {
		return err
	}
	_, err = db.Model(&TariffSurcharges{}).Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}

func (ts *TariffSurcharges) Create() error {
	if err := ts.validateForCreate(); err != nil {
		return err
	}
	ts.UUID = structures.GenerateUUID()

	if _, err := db.Model(ts).Insert(); err != nil {
		return err
	}

	return nil
}

func (ts *TariffSurcharges) validate() error {
	if err := ts.checkValidExpr(); err != nil {
		return err
	}

	if ts.Name == "" {
		return errors.New("Введите название")
	}
	if ts.RegionID == 0 {
		return errors.New("empty region_id field")
	}

	return nil
}

func (ts *TariffSurcharges) validateForCreate() error {
	if err := ts.validate(); err != nil {
		return err
	}

	query := db.Model(ts).Where("name = ?", ts.Name).Where("deleted IS NOT true")
	if ok, err := query.Exists(); err != nil {
		return errors.Wrap(err, "error check exists")
	} else if ok {
		return errors.Errorf("Запись с именем %s уже существует", ts.Name)
	}

	return nil
}

func (ts *TariffSurcharges) validateForUpdate() error {
	if err := ts.validate(); err != nil {
		return err
	}

	query := db.Model(ts).Where("uuid=?uuid").Where("deleted IS NOT true")
	if ok, err := query.Exists(); err != nil {
		return errors.Wrap(err, "error check exists")
	} else if !ok {
		return errors.Errorf("Запись с именем %s и uuid %s не существует", ts.Name, ts.UUID)
	}

	return nil
}

func (ts *TariffSurcharges) checkValidExpr() error {
	if ts.IfExpr == "" || ts.SurExpr == "" {
		return errors.Errorf("Впишите оба выражения")
	}

	_, err := expr.Eval(ts.IfExpr, OrderTariffChars{})
	if err != nil {
		return errors.Errorf("Некорректное выражение условия, %s", err)
	}

	_, err = expr.Eval(ts.SurExpr, evalTariffSurchargesStruct{})
	if err != nil {
		return errors.Errorf("Некорректное выражение надбавки, %s", err)
	}
	return err
}
