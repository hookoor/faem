package orders

import (
	"bytes"
	"context"
	"crypto/sha1"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"math"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/pkg/errors"

	"github.com/antonmedv/expr"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/pkg/validation"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

type (
	// OrderTariffChars структура для хранения параметров заказа (для rule engin) для дальнешейго применения тарифа
	OrderTariffChars struct {
		ServiceName              string   `json:"service_name"`                // имя услуги
		ServiceUUID              string   `json:"service_uuid"`                // UUID услуги
		ServiceTags              []string `json:"service_tags"`                // тэги услуги
		TaxiParkUUID             string   `json:"taxi_park_uuid"`              // uuid таксопарка
		ServiceMaxBonusesPercent int      `json:"service_max_bonuses_percent"` // максимально возможная часть оплаты тарифа бонусами
		Source                   string   `json:"source"`                      // источник заказа
		Hour                     int      `json:"hour"`                        // час заказа
		Minute                   int      `json:"minute"`                      // минута заказа
		WeekDay                  int      `json:"week_day"`                    // день недели заказа
		Day                      int      `json:"day"`                         // число месяца
		Month                    int      `json:"month"`                       // номер месяца
		Minutestamp              int      `json:"minutestamp"`                 // количество минут от начала дня
		Points                   int      `json:"points"`                      // количество точек в маршруте
		GisData                  GisData  `json:"-"`
	}

	GisData struct {
		StartRegions  []structures.Zone // зоны в которые попала стартовая точка
		FinishRegions []structures.Zone // зоны в которые попала конечная точка
		RoadPiece     []FromGis         // список отрезков маршрута
	}

	// FromGis
	FromGis struct {
		ZoneName string       // название зоны
		ZoneIntr gis.Geometry // маршрут принадлежащий зоне
		Dist     float64      // расстояние маршрута в метрах
	}

	// TariffRule godoc
	TariffRules struct {
		tableName struct{}    `sql:"crm_tariff_rules, alias:tr" pg:",discard_unknown_columns"`
		ID        int         `json:"id"`
		UUID      string      `json:"uuid"`
		Name      string      `json:"name"`
		Comment   string      `json:"comment"`
		Active    *bool       `json:"active"`
		Priority  int         `json:"priority" sql:",notnull"`
		Expr      string      `json:"expr"`
		Variables interface{} `json:"variables"`
		RegionID  int         `json:"region_id"` // для разделения правил по регионам

		Deleted       bool      `json:"-"`
		CreatedAt     time.Time `json:"-"`
		CreatedAtUnix int64     `json:"created_at_unix" sql:"-"`
	}

	// TariffPrices тут храним все параметры тарификации.
	TariffPrices struct {
		tableName struct{}           `sql:"crm_tariff_prices" pg:",discard_unknown_columns"`
		UUID      string             `json:"uuid" sql:",pk"`
		RuleID    int                `json:"rule_id" sql:",notnull"` // правила к которому относится тарификатор 0 - любой
		Default   *bool              `json:"default" required_on:"create"`
		ZoneID    []int              `json:"-" sql:",type:int[]"`             // зона для которой оно действую 0 - любая
		RegionID  int                `json:"region_id" required_on:"create"`  // для разделения по регионам WARNING: заполняется в бд триггером tg_crm_tariff_prices_on
		Priority  int                `json:"priority"`                        // приоритет данного правила
		FieldType TariffPricesType   `json:"field_type" required_on:"create"` // тип правила, то есть в одной правиле может быть тарификация таксометра, фичи, услуги и т.д.
		Taximeter map[string]float64 `json:"taximeter"`                       // стоимость за километры (!!! unmarshal не биндит в map[float64]float64)
		Features  map[string]float32 `json:"features"`                        // стоимость каждой из фич, тут же можем хранить например минимальную стоимость
		Penalties map[string]float32 `json:"-"`                               // стоимости тарификации по зонам
		Services  map[string]float32 `json:"-"`                               // стоимости услуг
		DeletedAt time.Time          `json:"-" pg:",soft_delete"`
	}

	// // штрафы из зоны в зону
	// myZonesPenalty struct {
	// 	tableName    struct{} `sql:"crm_zone_penalties"`
	// 	ID           int      `sql:"id"`
	// 	Caption      string   `sql:"caption"`
	// 	RuleID       int      `sql:"rule_id"`
	// 	Priority     int      `sql:"priority"`
	// 	StartZoneID  int      `sql:"start_zone_id"`
	// 	FinishZoneID int      `sql:"finish_zone_id"`
	// 	Penalty      float64  `sql:"penalty"`
	// }

	// ShortTariff short tariff data
	ShortTariff struct {
		ServiceUUID      string                `json:"service_uuid"`
		ServiceImage     string                `json:"service_image"`
		ServiceImagesSet structures.ImagesSet  `json:"service_images_set"`
		ProductDelivery  bool                  `json:"product_delivery"`
		ProductsPrice    int                   `json:"products_price"`
		Name             string                `json:"name"`
		Currency         string                `json:"currency"`
		BonusPayment     int                   `json:"bonus_payment"`
		MaxBonusPayment  int                   `json:"max_bonus_payment"`
		TotalPrice       int                   `json:"total_price"`
		Surge            *structures.SurgeItem `json:"surge"`         // повышенный спрос
		PreCalculated    string                `json:"precalculated"` // рассчитанное значение тарифа, которое будет использовано при создании заказа
	}
)
type TariffPricesType string

const (
	Taximetr            TariffPricesType = "taximeter"
	TimeTaximeter       TariffPricesType = "time_taximeter"
	CountryTaximetr     TariffPricesType = "country_taximeter"
	Fix                 TariffPricesType = "fix"
	BoardingTime        TariffPricesType = "boarding_time"
	CountryBoardingTime TariffPricesType = "country_boarding_time"
	PointTime           TariffPricesType = "points_time"
)

// TariffFetureKey -
type TariffFetureKey string

const (
	//     "point": 35,
	//     "boarding": 39,
	//     "delivery": 0,
	//     "min_price": 48,
	//     "guaranteed_driver_income": 70,
	//     "min_time_calculation_price": 180
	guaranteedDriverIncomeForDelivery TariffFetureKey = "guaranteed_driver_income_for_delivery"
)

const (
	TariffClaimsPeriod = time.Second * 300
)

type TariffClaims struct {
	jwt.StandardClaims

	Surge *structures.SurgeItem `json:"surge"`
}

type TariffsCriteria struct {
	Name        string `json:"name"`
	ServiceName string `json:"service_name"`
	RegionIDs   []int  `json:"region_ids"`
}

func (cr TariffsCriteria) GetAllTariffRules() ([]TariffRules, error) {
	trules := new([]TariffRules)
	query := db.Model(trules).
		Where("deleted is not true").
		WhereIn("region_id IN (?)", cr.RegionIDs)

	if cr.Name != "" {
		query.Where("name ILIKE ?", cr.Name+"%")
	}
	if cr.ServiceName != "" {
		query.Where("variables ->> 'service_name' ILIKE ?", cr.ServiceName+"%")
	}

	if err := query.Order("priority DESC").Select(); err != nil {
		return nil, err
	}

	return *trules, nil
}

func (tr *TariffRules) validExists() error {
	query := db.Model(tr).
		Where("expr = ?expr").
		Where("uuid != ?uuid").
		Where("priority = ?priority").
		Where("deleted is not true")

	if ok, err := query.Exists(); err != nil {
		return errors.Errorf("error valid exists, %s", err)
	} else if ok {
		return errors.New("Запись с таким условием и приоритетом уже существует")
	}

	return nil
}

func (tr *TariffRules) Create() error {
	if err := tr.validateForCreate(); err != nil {
		return err
	}

	if _, err := db.Model(tr).Insert(); err != nil {
		return err
	}

	return nil
}

func (tr *TariffRules) Update() error {
	if err := tr.validateForUpdate(); err != nil {
		return err
	}

	if _, err := db.Model(tr).Where("uuid = ?uuid").UpdateNotNull(); err != nil {
		return err
	}

	return nil
}

func GetTariffRuleByUUID(uuid string) (TariffRules, error) {
	var res TariffRules
	err := db.Model(&res).Where("uuid = ?", uuid).
		Where("deleted is not true").Select()
	return res, err
}

func (tr *TariffRules) validateForCreate() error {
	if tr.Name == "" {
		return errors.New("Не указано название правила тарифов")
	}
	if tr.Expr == "" {
		return errors.New("Не указано выражение условия тарифов")
	}
	if tr.RegionID == 0 {
		return errors.New("Не указан регион тарифа")
	}

	tr.UUID = structures.GenerateUUID()
	tr.CreatedAt = time.Now()
	tr.Priority = 0

	if err := tr.checkValidExpr(); err != nil {
		return err
	}

	return tr.validExists()
}

// SetTariffRuleDeleted marks rule and suitable tariff prices (if need) as deleted
func SetTariffRuleDeleted(uuid string, deletePrices bool) error {
	var rule TariffRules
	_, err := db.Model(&rule).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Returning("*").
		Update()
	if err != nil || !deletePrices {
		return err
	}
	_, err = db.Model(&TariffPrices{}).
		Where("rule_id = ?", rule.ID).
		Set("deleted = true").
		Update()
	return err
}

func (tr *TariffRules) validateForUpdate() error {
	if tr.Expr == "" {
		return nil
	}
	return tr.checkValidExpr()
}

func (tr *TariffRules) FillUnixFields() {
	if tr == nil {
		return
	}
	tr.CreatedAtUnix = tr.CreatedAt.Unix()
}

func (tr *TariffRules) checkValidExpr() error {
	var variables OrderTariffChars
	if _, err := expr.Eval(tr.Expr, variables); err != nil {
		return errors.Errorf("%s: %v", tr.Expr, err)
	}

	return nil
}

func (tp *TariffPrices) validTaximetr() error {
	if len(tp.Taximeter) == 0 {
		return errors.Errorf("for this field type taximetr is requried")
	}
	var idxs []float64
	for key := range tp.Taximeter {
		k, err := strconv.ParseFloat(key, 64)
		if err != nil {
			return errpath.Err(err)
		}
		idxs = append(idxs, k)
	}
	// sort.Ints(idxs)
	sort.Float64s(idxs)
	if idxs[0] != 0 {
		return errors.Errorf("Первый элемент в таксиметре должен быть нулевым")
	}
	return nil
}

func (tr *TariffRules) IsActive() bool {
	if tr == nil || tr.Active == nil {
		return false
	}
	return *tr.Active
}

func (tP *TariffPrices) validFeatures() error {
	neededField := map[string]string{
		"point":                    "Цена за доп. точку",
		"boarding":                 "Цена за посадку",
		"delivery":                 "Наценка за доставку",
		"min_price":                "Минимальная цена",
		"guaranteed_driver_income": "Гарантированный доход водителя",
	}
	for i, fieldName := range neededField {
		check := false
		for name := range tP.Features {
			if i == name {
				check = true
				break
			}
		}
		if !check {
			return errors.Errorf("%s - обязательное поле", fieldName)
		}
	}
	return nil
}

func (tp *TariffPrices) validate() error {
	switch tp.FieldType {
	case Fix:
		if err := tp.validFeatures(); err != nil {
			return err
		}

	case CountryTaximetr, TimeTaximeter, Taximetr, PointTime, BoardingTime:
		if err := tp.validTaximetr(); err != nil {
			return err
		}

	default:
		return errors.Errorf("invalid field type: %s", tp.FieldType)

	}

	return nil
}

func (tp *TariffPrices) Create() error {
	if err := tp.validate(); err != nil {
		return err
	}

	if err := validation.Create(tp); err != nil {
		return err
	}

	if _, err := db.Model(tp).Insert(); err != nil {
		return err
	}

	return nil
}

func (tp *TariffPrices) GetTariffPriceByUUID() error {
	if err := db.Model(tp).WherePK().Select(); err != nil {
		return err
	}

	return nil
}

type CriteriaTariffPrices struct {
	RuleID     int      `json:"rule_id"`
	FieldTypes []string `json:"field_types"`
	RegionIDs  []int    `json:"region_ids"`
	Default    *bool    `json:"default"`
}

func (cr CriteriaTariffPrices) GetFilteredTariffPrices() ([]TariffPrices, error) {
	result := make([]TariffPrices, 0)
	query := db.Model(&result).WhereIn("region_id IN (?)", cr.RegionIDs)

	if cr.RuleID != 0 {
		query.Where("rule_id = ?", cr.RuleID)
	}
	if len(cr.FieldTypes) != 0 {
		query.WhereIn("field_type IN (?)", cr.FieldTypes)
	}
	if cr.Default != nil {
		query.Where("\"default\" IS ?", cr.Default)
	}

	if err := query.Select(); err != nil {
		return nil, err
	}

	for i, item := range result {
		nmap := map[string]float64{}
		for k, v := range item.Taximeter {
			f, err := strconv.ParseFloat(k, 64)
			if err != nil {
				return nil, errpath.Err(err)
			}
			if (f - float64(int(f))) != 0 {
				nmap[k] = v
			} else {
				nmap[fmt.Sprint(int(f))] = v
			}
		}
		result[i].Taximeter = nmap
	}

	return result, nil
}

func (tp *TariffPrices) Update() error {
	if err := tp.validate(); err != nil {
		return err
	}

	if res, err := db.Model(tp).WherePK().UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("tariff price with uuid %q not found", tp.UUID)
	}

	return nil
}

func (tp *TariffPrices) Delete() error {
	if res, err := db.Model(tp).WherePK().Delete(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("tariff price with uuid %q not found", tp.UUID)
	}

	return nil
}

type hashSum [sha1.Size]byte
type tariffCache struct {
	storage   map[hashSum][]ShortTariff
	storagemx sync.RWMutex

	cachetime time.Duration
	mxtime    time.Duration

	mxmap   map[string]*sync.RWMutex
	mxmapmx sync.RWMutex
}

var (
	tc = &tariffCache{
		storage:   make(map[hashSum][]ShortTariff),
		mxmap:     make(map[string]*sync.RWMutex),
		cachetime: time.Second * 2,
		mxtime:    time.Minute,
	}
)

var (
	errEmptyHashData = errors.New("empty order struct/empty route data")
)

func getOrderHash(order *OrderCRM) (hashSum, error) {
	if order == nil || len(order.Routes) == 0 {
		return hashSum{}, errEmptyHashData
	}

	var b bytes.Buffer
	e := gob.NewEncoder(&b)

	e.Encode(order.Routes)
	if len(order.FeaturesUUIDS) != 0 {
		e.Encode(order.FeaturesUUIDS)
	}

	return sha1.Sum(b.Bytes()), nil
}

func (t *tariffCache) addToCache(hash hashSum, resp []ShortTariff) {
	t.storage[hash] = resp

	time.AfterFunc(t.cachetime, func() {
		t.storagemx.Lock()
		defer t.storagemx.Unlock()

		delete(t.storage, hash)
	})
}

func (t *tariffCache) getFromCache(hash hashSum) ([]ShortTariff, bool) {
	val, ok := t.storage[hash]
	if !ok {
		return nil, false
	}

	return val, true
}

func (t *tariffCache) getClientMutex(clientIP string) (*sync.RWMutex, *time.Timer) {
	t.mxmapmx.RLock()
	mx, ok := t.mxmap[clientIP]
	t.mxmapmx.RUnlock()
	if !ok {
		mx = &sync.RWMutex{}

		t.mxmapmx.Lock()
		t.mxmap[clientIP] = mx
		t.mxmapmx.Unlock()

		timer := time.AfterFunc(t.mxtime, func() {
			t.mxmapmx.Lock()
			defer t.mxmapmx.Unlock()

			delete(t.mxmap, clientIP)
		})

		return mx, timer
	}

	return mx, nil
}

// GetAllTariffs calculates all tariffs from order parameters
func GetAllTariffs(or *OrderCRM, clientIP string) ([]ShortTariff, error) {
	mx, mxtimer := tc.getClientMutex(clientIP)
	hash, err := getOrderHash(or)
	if err != nil {
		return nil, err
	}

	mx.RLock()
	shTariffs, exists := tc.getFromCache(hash)
	if exists {
		mx.RUnlock()
		return shTariffs, nil
	}

	mx.RUnlock()
	mx.Lock()
	defer func() {
		tc.addToCache(hash, shTariffs)
		if mxtimer != nil {
			if !mxtimer.Stop() {
				<-mxtimer.C
			}
			mxtimer.Reset(tc.cachetime)
		}

		mx.Unlock()
	}()

	// Через время удалить этот блок, оставлен для обратной совместимости
	if or.SourceOfOrdersUUID == "" {
		srcf := &models.SrcOfOrdersForwarding{
			RegionID:  1,
			SourceKey: or.Source,
		}

		if err := srcf.FillSourceOfOrdersUUID(); err != nil {
			return nil, err
		}
		or.SourceOfOrdersUUID = srcf.SourceOfOrdersUUID
	}

	var (
		bonData  structures.BonusesData
		osrmData structures.OSRMData
		services []models.ServiceCRM
	)

	errG := errgroup.Group{}
	errG.Go(func() error {
		var err error
		bonData, err = or.getClientBonusesData()
		if err != nil {
			return errpath.Err(err, "ошибка получения данных о бонусах")
		}
		return nil
	})
	errG.Go(func() error {
		var err error
		//получаем osrmData 1 раз чтобы на каждую итерацию не делать запрос на osrm - ку
		osrmData, err = GetItinerary(or.Routes)
		if err != nil {
			return errpath.Err(err, "ошибка расчета дистанции")
		}
		return err
	})
	errG.Go(func() error {
		var err error
		services, err = models.ServicesListBySourceOfOrdersUUID(or.SourceOfOrdersUUID)
		if err != nil {
			return errors.Errorf("ошибка получения списка сервисов[%s]", err)
		}
		return nil
	})

	if err := errG.Wait(); err != nil {
		return nil, err
	}

	var suitableServices []models.ServiceCRM
	for _, srv := range services {
		var isFuel bool
		for _, tag := range srv.Tag {
			if tag == "fuel" {
				isFuel = true
				continue
			}
		}
		if !isFuel {
			suitableServices = append(suitableServices, srv)
		}
	}

	for _, ser := range suitableServices {
		or.SrUUID = ser.UUID

		if err := or.fillColumnsWithObjects(); err != nil {
			return nil, errpath.Err(err, "fillColumnsWithObjects error")
		}

		if or.Service.ProductDelivery {
			productsData, err := models.ValidateAndGetProductsByInputData(or.ProductsInput)
			if err != nil {
				return nil, errpath.Err(err, "error getting product ready data")
			}

			or.ProductsData = productsData
		}

		models.FillTaxiParkData(or)

		tariff, err := GetTariff(or, bonData, osrmData)
		if err != nil {
			logs.Eloger.Error(errors.Wrapf(err, "ошибка расчета тарифа для сервиса %s [%s]", or.Service.Name, or.Service.UUID))
			continue
		}

		serviceName := ser.Name
		if ser.GetClientName() != "" {
			serviceName = ser.GetClientName()
		}

		shTariffs = append(shTariffs, ShortTariff{
			ServiceUUID:      ser.UUID,
			ServiceImage:     ser.Image,
			ServiceImagesSet: ser.ImagesSet,
			Name:             serviceName,
			Currency:         tariff.Currency,
			ProductDelivery:  ser.ProductDelivery,
			ProductsPrice:    tariff.ProductsPrice,
			BonusPayment:     tariff.BonusPayment,
			MaxBonusPayment:  tariff.MaxBonusPayment,
			TotalPrice:       tariff.TotalPrice,
			Surge:            tariff.Surge,
			PreCalculated:    tariff.PreCalculated,
		})
	}

	return shTariffs, nil
}
func (or *OrderCRM) getClientBonusesData() (structures.BonusesData, error) {
	var bonData structures.BonusesData
	var resBon int
	_, err := phoneverify.NumberVerify(or.Client.MainPhone)
	if err != nil {
		return bonData, nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	errG, ctx := errgroup.WithContext(ctx)
	errG.Go(func() error {
		var err error
		resBon, err = getReservedBonusesByClient(ctx, or.Client.MainPhone)
		return err
	})
	errG.Go(func() error {
		var err error
		bonData, err = models.GetBonusesDataByPhone(ctx, or.Client.MainPhone)
		return err
	})
	if err := errG.Wait(); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "getClientBonusesData",
			"phone": or.Client.MainPhone,
		}).Error(err)
		return structures.BonusesData{}, nil
	}
	bonData.Bonuses -= float64(resBon)
	return bonData, err
}

// getReservedBonusesByClient вовращает кол-во бонусов, уже использующихся в активных заказах
func getReservedBonusesByClient(ctx context.Context, clientPhone string) (int, error) {
	var orders []OrderCRM
	err := db.ModelContext(ctx, &orders).
		Where("client ->> 'main_phone' = ?", clientPhone).
		Where("tariff ->> 'bonuses_payment' != '0'").
		WhereIn("order_state IN (?)", constants.ListActiveOrderStates()).
		Returning("tariff").
		Select()
	var result int
	for _, dat := range orders {
		result += dat.Tariff.BonusPayment
	}
	return result, err
}

// GetFuelTariff -
func GetFuelTariff() (structures.Tariff, error) {
	var tariff structures.Tariff

	tariff.Name = "Топливо"
	tariff.TariffCalcType = structures.CalcWithDist

	return tariff, nil
}

// GetTariffWithBonuses -
func GetTariffWithBonuses(or *OrderCRM) (structures.Tariff, error) {
	// Через время удалить этот блок, оставлен для обратной совместимости
	if or.SourceOfOrdersUUID == "" {
		srcf := &models.SrcOfOrdersForwarding{
			RegionID:  1,
			SourceKey: or.Source,
		}

		if err := srcf.FillSourceOfOrdersUUID(); err != nil {
			return structures.Tariff{}, errors.Wrap(err, "failed to fill source of orders")
		}
		or.SourceOfOrdersUUID = srcf.SourceOfOrdersUUID
	}

	errG := errgroup.Group{}
	var bonData structures.BonusesData
	errG.Go(func() error {
		var err error
		bonData, err = or.getClientBonusesData()
		if err != nil {
			return errors.Errorf("error getting bonuses data,%s", err)
		}
		return err
	})
	errG.Go(func() error {
		err := or.fillColumnsWithObjects()
		if err != nil {
			return errpath.Err(err, "fillColumnsWithObjects error")
		}
		return err
	})

	if err := errG.Wait(); err != nil {
		return structures.Tariff{}, err
	}

	if or.Service.ProductDelivery {
		productsData, err := models.ValidateAndGetProductsByInputData(or.ProductsInput)
		if err != nil {
			return structures.Tariff{}, errpath.Err(err, "error getting product ready data")
		}
		or.ProductsData = productsData
	}

	return GetTariff(or, bonData)
}

// GetTariff calculates tariff from order parameters.
// При расчете тарифа по времени в OSRMReadyData надо заполнить Duration
// При расчете с 1 роутом в OSRMReadyData надо заполнить : Сoor для расчета по коллометрам; Duration для расчета по времени
func GetTariff(or *OrderCRM, bonData structures.BonusesData, OSRMReadyData ...structures.OSRMData) (structures.Tariff, error) { //

	var (
		osrmData       structures.OSRMData
		tariff         structures.Tariff
		freeWay        bool // свободный маршрут (с 1 роутом)
		tariffCalcType structures.TariffCalcType
	)
	fillProdPrice := func() {
		if or.ProductsPrice <= 0 {
			prouctsPrice, productItems := or.getProductsPriceAndItems()
			tariff.ProductsPrice = prouctsPrice
			tariff.Items = append(tariff.Items, productItems...)
		} else {
			tariff.ProductsPrice = or.ProductsPrice
		}
	}
	if !or.DeliveryRequired() && or.Service.ProductDelivery {
		fillProdPrice()
		return tariff, nil
	}
	if or.Tariff.TariffCalcType == "" || or.Tariff.TariffCalcType == structures.CalcWithDist {
		tariffCalcType = structures.CalcWithDist
	}
	if or.Tariff.TariffCalcType == structures.CalcWithTime {
		tariffCalcType = structures.CalcWithTime
	}
	if or.Tariff.TariffCalcType == structures.CalcWithFix {
		tariffCalcType = structures.CalcWithFix // не реализованно
	}

	routsLen := len(or.Routes)
	if routsLen == 1 {
		freeWay = true
		if or.Tariff.TariffCalcType == "" {
			tariffCalcType = structures.CalcWithTime
		}
	}

	tariff.TariffCalcType = tariffCalcType

	// получаем данные для дальнешейго анализа и прогона правил
	ordChars := new(OrderTariffChars)
	if err := ordChars.fillSourceData(or); err != nil {
		return tariff, errpath.Err(err, "fillSourceData error")
	}

	rule, err := findTariffRule(ordChars, or.RegionID)
	if err != nil {
		return tariff, errors.Errorf("findTariffRule error [%s]", err)
	}

	ruleName := fmt.Sprintf("%s(%v)", rule.Name, rule.ID)
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "tariff generator",
		"reason":    "rule founded",
		"orderUUID": or.UUID,
		"value":     ruleName,
	}).Info("Tarrificator rule founded")

	tariffPrice, err := TariffPricesByRuleID(rule.ID, rule.RegionID)
	if err != nil {
		return tariff, errpath.Err(err)
	}

	countryTaximetr := map[float64]float64{}
	timeTaximeter := map[float64]float64{}
	taximetr := map[float64]float64{}
	var fixTariffs map[string]float32
	boardingTime := map[int]float64{}
	countryBoardingTime := map[int]float64{}
	for _, r := range tariffPrice {
		floatTaximetr := map[float64]float64{}
		for key, val := range r.Taximeter {
			k, err := strconv.ParseFloat(key, 64)
			if err != nil {
				return tariff, errpath.Err(err)
			}
			floatTaximetr[k] = val
		}
		switch r.FieldType {
		case Taximetr:
			if len(taximetr) == 0 {
				for key, val := range floatTaximetr {
					taximetr[float64(key)] = val
				}
			}
		case CountryTaximetr:
			if len(countryTaximetr) == 0 {
				for key, val := range floatTaximetr {
					countryTaximetr[float64(key)] = val
				}
			}
		case TimeTaximeter:
			if len(timeTaximeter) == 0 {
				timeTax := map[int]float64{}
				for key, val := range floatTaximetr {
					timeTaximeter[float64(key)] = val
					timeTax[int(key)] = val
				}
				tariff.TimeTaximeter = timeTax
			}
		case Fix:
			if len(fixTariffs) == 0 {
				fixTariffs = r.Features
				tariff.OrderMinPaymentWithTime = int(fixTariffs["min_time_calculation_price"])
			}
		case BoardingTime:
			if len(or.Tariff.WaitingBoarding) == 0 {
				for key, val := range floatTaximetr {
					boardingTime[int(key)] = val
				}
				tariff.WaitingBoarding = boardingTime
			}
		case CountryBoardingTime:
			if len(or.Tariff.WaitingBoarding) == 0 {
				for key, val := range floatTaximetr {
					countryBoardingTime[int(key)] = val
				}
			}
		case PointTime:
			if len(or.Tariff.WaitingPoint) == 0 {
				waitPoint := map[int]float64{}
				for key, val := range floatTaximetr {
					waitPoint[int(key)] = val
				}
				tariff.WaitingPoint = waitPoint
			}
		}
	}

	if len(taximetr) == 0 {
		return tariff, errors.Errorf("No taximetr rule found")
	}
	if len(countryTaximetr) == 0 {
		logrus.Warnf("(%s)не найден загородный таксометр\n", errpath.Func())
	}
	logrus.Debugln("(GetTariff)городской таксoметр :", taximetr)
	logrus.Debugln("(GetTariff)загородный таксoметр :", countryTaximetr)

	var taximeterPrice, fixPrice int

	//расчет дистанции по кривому маршруту
	if len(OSRMReadyData) == 0 {
		if !freeWay {
			osrmData, err = GetItinerary(or.Routes) // TODO: нельзя получить тариф для 1 точки в роута
			if err != nil {
				return structures.Tariff{}, errpath.Err(err, "ошибка расчета дистанции")
			}
			if osrmData.Routes[0].Distance > models.GodModeInst.ValidDistanceToGetTariff.Value {
				return structures.Tariff{}, errpath.Errorf("слишком большое расстояние для дальнейших расчетов: %v m", osrmData.Routes[0].Distance)
			}
		}
	} else {
		//чтобы можно было передать уже готовые osrm данные, дабы избежать лишнего запроса
		osrmData = OSRMReadyData[0]
	}

	if !freeWay {
		if len(osrmData.Routes) == 0 {
			return structures.Tariff{}, errpath.Err(err, "пустая osrm структура")
		}
	}

	var dist, cityDist, countryDist float64 = 0, 0, 0
	var triptime int = 0

	if freeWay {
		if or.Tariff.OrderCompleateDist == 0 && tariffCalcType == structures.CalcWithDist && or.OrderState == constants.OrderStatePayment {
			return tariff, errpath.Errorf("для вычисления по времени нужно передать в tariff расстояние")
		}
		dist = or.Tariff.OrderCompleateDist
		if or.Tariff.OrderTripTime == 0 && tariffCalcType == structures.CalcWithTime && or.OrderState == constants.OrderStatePayment {
			return tariff, errpath.Errorf("для вычисления по времени нужно передать в tariff время")
		}
		triptime = int(or.Tariff.OrderTripTime)
	} else {
		dist = osrmData.Routes[0].Distance
		// triptime = int(osrmData.Routes[0].Duration)
		triptime = int(or.Tariff.OrderTripTime)
	}

	// Пока закомменчено, данные не используются, но запрос сильно грузит бд
	//if !freeWay {
	//	err = ordChars.fillSourceGisData(or, osrmData.CoordinatesParse())
	//	if err != nil {
	//		return tariff, errpath.Err(err)
	//	}
	//}
	//
	//logrus.Debugln("(GetTariff)общее расстояние маршрута :", dist)
	//// логика расчета городской дистанции
	//for _, item := range ordChars.GisData.RoadPiece {
	//	if item.ZoneName == models.MaxPriorityRouteName { // Владикавказ
	//		cityDist += item.Dist
	//	}
	//}
	cityDist = dist // пробка закупоривающую расчет загороднего тарифа

	if len(countryTaximetr) == 0 {
		cityDist = dist
		countryDist = 0
	} else {
		cityDist = math.Ceil(cityDist)
		countryDist = dist - cityDist
	}
	logrus.Debugln("(GetTariff)расстояние в городе :", cityDist)
	logrus.Debugln("(GetTariff)расстояние не в городе :", countryDist)

	startPoint := structures.PureCoordinates{Long: float64(or.Routes[0].Lon), Lat: float64(or.Routes[0].Lat)}
	logrus.Debugln("(GetTariff)стартовая точка", startPoint)
	startZone := []models.CrmZone{}
	err = gisCrmZonesData.ZonesBelongingToThePoint(startPoint, &startZone)
	if err != nil {
		return structures.Tariff{}, errors.Errorf("(GetTariff):%s", err)
	}
	if len(startZone) == 0 {
		logrus.Warnf("(%s)Zone with start point not found", errpath.Func())
	} else {
		logrus.Debugln("(GetTariff)стартовая зона :", startZone[0])
	}

	finishPoint := structures.PureCoordinates{Long: float64(or.Routes[len(or.Routes)-1].Lon), Lat: float64(or.Routes[len(or.Routes)-1].Lat)}
	logrus.Debugln("(GetTariff)конечная точка", finishPoint)
	finishZone := []models.CrmZone{}
	err = gisCrmZonesData.ZonesBelongingToThePoint(finishPoint, &finishZone)
	if err != nil {
		return structures.Tariff{}, errors.Errorf("(GetTariff):%s", err)
	}
	if len(finishZone) == 0 {
		logrus.Warnf("(GetTariff)Zone with finish point not found")
	} else {
		logrus.Debugln("(GetTariff)конечная зона :", finishZone[0])
	}

	if !freeWay {
		if len(startZone) != 0 {
			inCity := false
			for _, zone := range startZone {
				if zone.Name == models.MaxPriorityRouteName {
					inCity = true
					break
				}
			}

			if !inCity && countryBoardingTime != nil {
				tariff.WaitingBoarding = countryBoardingTime
			}
		}
		if len(startZone) == 0 && countryBoardingTime != nil {
			tariff.WaitingBoarding = countryBoardingTime
		}
	}

	var penalty float64
	if len(startZone) != 0 && len(finishZone) != 0 {
		penalty, err = getPenaltyForTransitionInZone(startZone, finishZone)
		if err != nil {
			return structures.Tariff{}, errpath.Err(err)
		}
	}

	var cityTaximetrPrice, countryTaximetrPrice, timeTaximeterPrice int

	if dist > 0 {
		cityTaximetrPrice = distancePrice(taximetr, cityDist)
		countryTaximetrPrice = distancePrice(countryTaximetr, countryDist)
		timeTaximeterPrice = timerPrice(timeTaximeter, float64(triptime))

		if countryTaximetrPrice < 3 { // смягчение погрешности между OSRM и Postgis (метры)
			countryTaximetrPrice = 0
		}
		logrus.Debugln("(GetTariff)цена за городскую зону :", cityTaximetrPrice)
		logrus.Debugln("(GetTariff)цена за загороднюю зону :", countryTaximetrPrice)
		// taximetrPrice = distancePrice(taximetr, float32(dist))
		if tariffCalcType == structures.CalcWithDist {
			taximeterPrice = cityTaximetrPrice + countryTaximetrPrice
		}
		if tariffCalcType == structures.CalcWithTime {
			taximeterPrice = timeTaximeterPrice
		}
	} else {
		timeTaximeterPrice = timerPrice(timeTaximeter, float64(triptime))
		taximeterPrice = timeTaximeterPrice
		if !freeWay {
			return tariff, errors.Errorf("Can't calculate price. Distance < 0")
		}
	}

	distLog := fmt.Sprintf(
		"From: %s, (%v/%v). To: %s (%v/%v)",
		or.Routes[0].UnrestrictedValue,
		or.Routes[0].Lat,
		or.Routes[0].Lon,
		or.Routes[routsLen-1].UnrestrictedValue,
		or.Routes[routsLen-1].Lat,
		or.Routes[routsLen-1].Lon,
	)
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "tariff generator",
		"reason":    "distance calculated",
		"orderUUID": or.UUID,
		"value":     dist,
	}).Info(distLog)

	boarding, zone, points := boardingZoneAndPointPenalties(fixTariffs, ordChars, rule.ID)

	fixPrice = boarding + int(zone) + points
	priceLog := fmt.Sprintf("Taximetr:%v, Boarding:%v, ZonePenalty:%v, ExtraPoints:%v", taximeterPrice, boarding, zone, points)
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "tariff generator",
		"reason":    "price calculated",
		"orderUUID": or.UUID,
		"value":     fixPrice + taximeterPrice,
	}).Info(priceLog)

	// Define tariff information
	tariff.Name = rule.Comment
	tariff.Currency = "руб"
	tariff.PaymentType = "Наличные"
	if or.GetPaymentType() == constants.OrderPaymentTypeCard {
		tariff.PaymentType = "Картой"
	}
	if or.GetPaymentType() == constants.OrderPaymentTypeOnlineTransfer {
		tariff.PaymentType = "Онлайн перевод"
	}
	if or.IsUnpaid() || or.Promotion.IsUnpaid { // look for both fields
		tariff.PaymentType = "Бонусы (не брать оплату с клиента)"
	}

	summaryPrice := fixPrice + taximeterPrice

	var tItems []structures.TariffItem

	if taximeterPrice != 0 {
		if tariffCalcType == structures.CalcWithDist {
			if cityTaximetrPrice > 0 {
				tItems = append(tItems, structures.TariffItem{
					Name:  "Городской таксометр " + fmt.Sprintf("(%vm)", math.Ceil(cityDist)),
					Price: cityTaximetrPrice,
					Type:  structures.TariffItemTypeDefault,
				}) // Name: "Городской таксометр"
			}

			if countryTaximetrPrice > 0 {
				tItems = append(tItems, structures.TariffItem{
					Name:  "Загородный таксометр " + fmt.Sprintf("(%vm)", math.Ceil(countryDist)),
					Price: countryTaximetrPrice,
					Type:  structures.TariffItemTypeDefault,
				})
			}
			addItems, err := ordChars.getAdditionalTariffItems(float32(cityDist+countryDist), taximeterPrice)
			if err != nil {
				return tariff, errors.Errorf("error getting additional tariff items, %s", err)
			}
			if len(addItems) != 0 {
				for _, item := range addItems {
					summaryPrice += item.Price
				}
				tItems = append(tItems, addItems...)
			}

			tItems = append(tItems, structures.TariffItem{
				Name:  "Посадка",
				Price: boarding,
				Type:  structures.TariffItemTypeDefault,
			})
			if penalty != 0 {

				tItems = append(tItems, structures.TariffItem{
					Name:  "Надбавка за район",
					Price: int(penalty),
					Type:  structures.TariffItemTypeDefault,
				})
				summaryPrice += int(penalty)
			}
			if points != 0 {
				tItems = append(tItems, structures.TariffItem{
					Name:  "Доп. точка",
					Price: points,
					Type:  structures.TariffItemTypeDefault,
				})
			}
		}

		delivery := fixTariffs["delivery"]
		if delivery != 0 {
			tItems = append(tItems, structures.TariffItem{
				Name:  "Наценка за доставку",
				Price: int(delivery),
				Type:  structures.TariffItemTypeDefault,
			})
			summaryPrice += int(delivery)
		}

		if tariffCalcType == structures.CalcWithTime {
			tItems = append(tItems, structures.TariffItem{
				Name:  "Таксометр по времени " + fmt.Sprintf("(%vмин)", math.Ceil(float64(triptime/60))),
				Price: timeTaximeterPrice,
				Type:  structures.TariffItemTypeDefault,
			})
		}
	}

	if tariffCalcType == structures.CalcWithTime {
		// if summaryPrice < int(fixTariffs["min_time_calculation_price"]) {
		// 	tItems = append(tItems, structures.TariffItem{
		// 		Name:  "Надбавка до минимальной цены",
		// 		Price: int(fixTariffs["min_time_calculation_price"]) - summaryPrice,
		// 	})
		// 	tariff.TotalPrice = int(fixTariffs["min_time_calculation_price"])
		// } else {
		// 	tariff.TotalPrice = summaryPrice
		// }
		tariff.TotalPrice = timeTaximeterPrice + int(fixTariffs["min_time_calculation_price"])
	} else {
		if summaryPrice < int(fixTariffs["min_price"]) {
			tItems = append(tItems, structures.TariffItem{
				Name:  "Надбавка до минимальной цены",
				Price: int(fixTariffs["min_price"]) - summaryPrice,
				Type:  structures.TariffItemTypeDefault,
			})
			tariff.TotalPrice = int(fixTariffs["min_price"])
		} else {
			tariff.TotalPrice = summaryPrice
		}
	}

	tariff.Items = tItems

	if err := applyTariffSurchargesIfNeed(or.RegionID, ordChars, &tariff); err != nil {
		return tariff, errors.Errorf("error applying tariff surcharges, %s", err)
	}

	// Apply service correction for an order
	if or.SurgeAllowed(CurrentServiceCorrection.OutOfTownAllowed(or.RegionID, or.Service.UUID)) {
		if or.Tariff.Surge == nil { // if a completely new order
			precalcTariff, err := decodePrecalculatedTariff(or.Tariff.PreCalculated)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "tariff generator",
					"reason":     "failed to decode precalculated tariff",
					"order_uuid": or.UUID,
				}).Error(err)
			}
			if precalcTariff.Surge == nil { // just calculating a tariff before order creation
				surgeItem := CurrentServiceCorrection.Load(startPoint.Lat, startPoint.Long, or.RegionID, or.Service.UUID)
				tariff.Surge = &surgeItem
			} else {
				tariff.Surge = precalcTariff.Surge
			}
		} else {
			tariff.Surge = or.Tariff.Surge
		}

		if tariff.Surge.HasCorrection() {
			oldTotalPrice := tariff.TotalPrice
			tariff.TotalPrice = int(tariff.Surge.CalcCorrection(float64(oldTotalPrice), or.Source))
			newItem := structures.TariffItem{
				Price: tariff.TotalPrice - oldTotalPrice,
				Type:  structures.TariffItemTypeDefault,
			}
			if newItem.Price > 0 {
				newItem.Name = "Повышенный спрос"
			} else {
				newItem.Name = "Пониженный спрос"
			}
			tariff.Items = append(tariff.Items, newItem)
		}

		// Encode surge
		tariff.PreCalculated, err = encodePrecalculatedTariff(tariff.Surge)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "tariff generator",
				"reason":     "failed to encode precalculated tariff",
				"order_uuid": or.UUID,
			}).Error(err)
		}
	}

	// Further are independent of surcharges entries
	for _, item := range or.Features {
		if item.Price == 0 {
			continue
		}
		tariff.Items = append(tariff.Items, structures.TariffItem{
			Name:  fmt.Sprintf("Опция '%s'", item.Name),
			Price: item.Price,
			Type:  structures.TariffItemTypeDefault,
		})
		tariff.TotalPrice += int(item.Price)
	}
	if or.FixedPrice != 0 {
		tariff.TotalPrice = or.FixedPrice
		tariff.FixedPrice = or.FixedPrice
		tariff.Items = []structures.TariffItem{{Name: "Фиксированная цена", Price: or.FixedPrice, Type: structures.TariffItemTypeDefault}}
	}
	tariff.MaxBonusPayment = int(ordChars.ServiceMaxBonusesPercent * (tariff.TotalPrice) / 100)
	if !or.IsUnpaid() {
		if bonData.AutomaticWriteOff && bonData.Bonuses > 0 {
			switch bonData.Bonuses > float64(tariff.MaxBonusPayment) {
			case true:
				tariff.BonusPayment = tariff.MaxBonusPayment
			case false:
				tariff.BonusPayment = int(bonData.Bonuses)
			}
		}
	}
	fillProdPrice()
	// Process guaranteed driver income
	guaranteedDriverIncome, guaranteedExist := fixTariffs[constants.GuaranteedDriverIncomeKey]
	if !guaranteedExist {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting tariff",
			"reason": "key not found",
		}).Warnln(errpath.Errorf("key %s not found", string(constants.GuaranteedDriverIncomeKey)))
	}
	if or.Service.ProductDelivery && len(or.GetProductsData().Pruducts) != 0 {
		guaranteedIncomeForDelivery, guaranteedExist := fixTariffs[string(guaranteedDriverIncomeForDelivery)]
		if guaranteedExist {
			guaranteedDriverIncome = guaranteedIncomeForDelivery
		}
		if !guaranteedExist {
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "getting tariff",
				"reason": "key not found",
			}).Warnln(errpath.Errorf("key %s not found", string(guaranteedDriverIncomeForDelivery)))
		}
	}

	if guaranteedDriverIncome > 0 {
		tariff.Items = append(tariff.Items, structures.TariffItem{
			Name:  constants.GuaranteedDriverIncomeMsg,
			Price: int(guaranteedDriverIncome),
			Type:  structures.TariffItemTypeDefault,
		})
		tariff.GuaranteedDriverIncome = int(guaranteedDriverIncome)
		// guaranteedDriverIncome не должен входить в totalPrice
	}
	if or.IncreasedFare != 0 {
		tariff.Items = append(tariff.Items, structures.TariffItem{
			Name:  "Надбавка клиента",
			Price: int(or.IncreasedFare),
			Type:  structures.TariffItemTypeDefault,
		})
		tariff.TotalPrice += int(or.IncreasedFare)
	}
	//если заказ обновляется с црмки  и надо учесть старые надбавки за ожидание
	if or.Tariff.WaitingPrice != 0 {
		tariff.TotalPrice += or.Tariff.WaitingPrice
		tariff.Items = append(tariff.Items, getWaitingItemsIfExists(or.Tariff)...)
	}
	if supp := int(guaranteedDriverIncome) - tariff.TotalPrice; supp > 0 {
		tariff.SupplementToGuaranteedIncome = supp
	}

	insuranceEligible := or.RegionID == 1
	// search for illegible tags
	for _, tag := range or.Service.Tag {
		if strings.Contains(tag, "fuel") ||
			strings.Contains(tag, "delivery") {
			insuranceEligible = false
			break
		}
	}

	if insuranceEligible {
		// TODO: сделать по человечески, когда введем клиент-онли надбавки
		if insurance := CurrentInsurance.GetParams(); insurance.Enabled {
			//tariff.InsuranceCost = insurance.Cost TODO: пока включили страховку в стоимость тарифов
			tariff.TotalPrice += insurance.Cost

			insuranceItem := structures.TariffItem{
				Name:  insurance.Messages.InsuranceCost,
				Price: insurance.Cost,
				Type:  structures.TariffItemTypeDefault, // TODO: пока убрали систему с отображением отдельного окошка с инфой по страхованию
			}

			tariff.Items = append(tariff.Items, insuranceItem)
		}
	}

	return tariff, nil
}

// func freeTariff(or OrderCRM, oc OrderTariffChars) bool {
// 	if strings.Contains(or.Service.Name, "Доставка") {
// 		return false
// 	}
// 	if oc.Hour >= 9 && oc.Hour <= 15 && (oc.Day >= 1 && oc.Day <= 3) {
// 		for _, route := range or.Routes {
// 			if strings.Contains(route.UnrestrictedValue, "Город Ангелов") ||
// 				strings.Contains(route.UnrestrictedValue, "ШКОЛА №1 БЕСЛАН") {
// 				return true
// 			}
// 		}
// 	}
// 	return false
// }

// getWaitingItemsIfExists возвращает сначала айтем ожидания на точке, потом в пути
func getWaitingItemsIfExists(tariff structures.Tariff) []structures.TariffItem {
	var neededItems []structures.TariffItem
	for _, it := range tariff.Items {
		if strings.Contains(it.Name, structures.WaitingOnPointPrefix) {
			neededItems = append(neededItems, it)
		}
		if strings.Contains(it.Name, structures.WaitingOnTheWayPrefix) {
			neededItems = append(neededItems, it)
		}
	}
	return neededItems
}
func (or *OrderCRM) getProductsPriceAndItems() (total int, items []structures.TariffItem) {
	for _, prod := range or.GetProductsData().Pruducts {
		var toppingsPrice int
		for _, topp := range prod.Toppings {
			toppingsPrice += topp.Price
		}
		proPrice := prod.Price + prod.SelectedVariant.Price + toppingsPrice
		total += (proPrice * prod.Number)
		items = append(items, structures.TariffItem{
			Name:  prod.Name,
			Price: proPrice,
			Type:  structures.TariffItemTypeDefault,
		})
	}
	return
}

// getAdditionalTariffItems возвращает айтемы с учетом коэффициентов на цену километра по зонам
func (oc *OrderTariffChars) getAdditionalTariffItems(fullDistance float32, taximetrPrice int) ([]structures.TariffItem, error) {
	var zonesName []string
	zonesDis := make(map[string]float64)
	fullDistance = fullDistance / 1000 //километры
	for _, item := range oc.GisData.RoadPiece {
		zonesName = append(zonesName, item.ZoneName)
		zonesDis[item.ZoneName] = (item.Dist / 1000) //километры
	}
	if len(zonesName) == 0 {
		return nil, nil
	}
	var neededZones []models.CrmZone
	err := db.Model(&neededZones).
		Where("name in (?)", pg.In(zonesName)).
		Returning("name,distance_coeff").
		Select()
	if err != nil {
		return nil, errors.Errorf("error getting zones from bd, %s", err)
	}
	var result []structures.TariffItem
	// средняя цена за километр
	averageKMPrice := float32(taximetrPrice) / fullDistance
	for _, item := range neededZones {
		if item.DistanceCoeff == 1 || item.DistanceCoeff < 0 {
			continue
		}
		averageKMPriceInZone := float64(averageKMPrice) * zonesDis[item.Name]
		priceInZone := (item.DistanceCoeff * averageKMPriceInZone) - (averageKMPriceInZone)
		if math.Abs(priceInZone) < 0.5 {
			continue
		}
		result = append(result, structures.TariffItem{
			Name:  fmt.Sprintf("Коэффициент стоимости в зоне '%s'", item.Name),
			Price: int(priceInZone),
		})
	}
	return result, nil
}

// На данный момент возвращает стоимость посадки и доп. точки (без стоимости фич)
func boardingZoneAndPointPenalties(fixTariffs map[string]float32, orderChars *OrderTariffChars, ruleID int) (int, float64, int) {
	// стоимость посадки
	boarding := int(fixTariffs["boarding"])
	if orderChars.Points < 3 {
		return boarding, 0, 0
	}
	extraPoints := orderChars.Points - 2
	pointPrice := extraPoints * int(fixTariffs["point"])
	// TODO: добавить стоимость дополнительных фич
	return boarding, 0, pointPrice
}

// TariffPricesByRuleID - возвращает стоимость за километр по запрашиваему правилу
func TariffPricesByRuleID(ruleID int, regionID int) ([]TariffPrices, error) {
	var (
		tariffes []TariffPrices
		// taxes    map[int]float32
		// ins   TariffPrices
	)
	err := db.Model(&tariffes).
		Where("rule_id = ? OR \"default\"", ruleID).
		Where("region_id = ?", regionID).
		Order("priority DESC").
		Select()
	if err != nil {
		return tariffes, err
	}

	return tariffes, nil
}

func distancePrice(taximetr map[float64]float64, dist float64) int {
	var res int
	var meters bool = false
	if len(taximetr) != 0 {
		for _, val := range taximetr {
			if val > 100 {
				meters = true
				break
			}
		}
	}

	if meters {
		res = culcPrice(taximetr, dist/1000)
	} else {
		res = culcPrice(taximetr, dist/1000)
	}
	return res
}

func timerPrice(taximetr map[float64]float64, dist float64) int {
	return culcPrice(taximetr, dist/60)
}

// culcPrice - на входе получает карту стоимости за километр и длину пути в километрах, возвращает целую стоимость
func culcPrice(taximetr map[float64]float64, path float64) int {
	const invalidPrice = 0
	if len(taximetr) < 1 {
		return invalidPrice
	}

	var (
		sum  float64 = 0
		idxs []float64
	)

	for key := range taximetr {
		idxs = append(idxs, key)
	}

	if len(idxs) == 1 {
		return int(taximetr[idxs[0]] * float64(path))
	}
	sort.Float64s(idxs)

	last := len(idxs) - 1
	// Если общий путь больше наибольшего значения в мапе
	if idxs[last] < path {
		for i := 1; i < (last + 1); i++ {
			sum = sum + ((idxs[i] - idxs[i-1]) * taximetr[idxs[i-1]])
		}
		dis := path - idxs[last]
		sum = sum + (dis * taximetr[idxs[last]])

		return int(sum)
	}

	divPath := math.Ceil(float64(path)) // round to the nearest largest integer to reduce the sum
	for k, val := range idxs {
		// Ищем точку в тарифе до которой считать
		if val >= divPath || k == len(idxs)-1 {
			for i := 0; i < k-1; i++ {
				y := (float64(idxs[i+1] - idxs[i])) * taximetr[idxs[i]]
				sum = sum + y
			}
			if k > 0 {
				diff := path - idxs[k-1]
				sum = sum + (diff * taximetr[idxs[k-1]])
			} else {
				sum = sum + (path * taximetr[idxs[0]])
			}

			return int(sum)
		}
	}
	return invalidPrice
}

// // finalPrice на входе получает структуру стоимости за километрам и длину пути, и целую стоимость
// func culcPrice(taximetr map[int]float32, path float32) int {
// 	const invalidPrice = 0
// 	if len(taximetr) < 1 {
// 		return invalidPrice
// 	}

// 	var (
// 		sum  float32
// 		idxs []int
// 	)

// 	for key := range taximetr {
// 		idxs = append(idxs, key)
// 	}

// 	if len(idxs) == 1 {
// 		return int(taximetr[idxs[0]] * path)
// 	}
// 	sort.Ints(idxs)

// 	last := len(idxs) - 1
// 	// Если общий путь больше наибольшего значения в мапе
// 	if float32(idxs[last]) < path {
// 		for i := 1; i < (last + 1); i++ {
// 			sum = sum + (float32(idxs[i]-idxs[i-1]) * taximetr[idxs[i-1]])
// 			// fmt.Printf("%v. distance = %f; price=%f sum=%f\n", i, float32(idxs[i]-idxs[i-1]), taximetr[idxs[i-1]], sum)
// 		}
// 		dis := path - float32(idxs[last])
// 		sum = sum + (dis * taximetr[idxs[last]])

// 		return int(sum)
// 	}

// 	divPath := int(math.Ceil(float64(path))) // round to the nearest largest integer to reduce the sum
// 	for k, val := range idxs {
// 		// Ищем точку в тарифе до которой считать
// 		if val >= divPath || k == len(idxs)-1 {
// 			for i := 0; i < k-1; i++ {
// 				y := float32(idxs[i+1]-idxs[i]) * taximetr[idxs[i]]
// 				sum = sum + y
// 			}
// 			if k > 0 {
// 				diff := path - float32(idxs[k-1])
// 				sum = sum + (diff * taximetr[idxs[k-1]])
// 			} else {
// 				sum = sum + (path * taximetr[idxs[0]])
// 			}

// 			return int(sum)
// 		}
// 	}
// 	return invalidPrice
// }

// findTariffRule перебирает правила и возвращает подходящие
func findTariffRule(ord *OrderTariffChars, regionID int) (*TariffRules, error) {
	tRules := make([]TariffRules, 0)
	query := db.Model(&tRules).
		Where("deleted is not true").
		Where("active").
		Where("region_id = ?", regionID).
		Order("priority DESC")
	if err := query.Select(); err != nil {
		return nil, errors.Wrap(err, "error getting tariff rules from DB")
	}
	if len(tRules) == 0 {
		return nil, errors.New("tariff rules DB is empty")
	}

	for i, v := range tRules {
		ast, err := expr.Parse(v.Expr, expr.Env(&OrderTariffChars{}))
		if err != nil {
			return nil, errors.Wrapf(err, "error parsing rules - '%s'", v.Name)
		}

		result, err := expr.Run(ast, ord)
		if err != nil {
			return nil, errors.Wrapf(err, "error parsing rules - '%s'", v.Name)
		} else if result == true {
			return &tRules[i], nil
		}
	}

	return nil, errors.New("no one of rules match input data")
}

// fillSourceData заполняет структуру OrderTariffChars данными которы будут в последующем использоваться
// в выражениях для определения правила распределения
func (oc *OrderTariffChars) fillSourceData(or *OrderCRM) error {
	routesLength := len(or.Routes)

	oc.ServiceName = or.Service.Name
	oc.ServiceUUID = or.Service.UUID
	oc.TaxiParkUUID = or.GetTaxiParkUUID()
	oc.Source = or.Source
	oc.ServiceMaxBonusesPercent = or.Service.GetMaxBonusPayment()
	oc.ServiceTags = or.Service.Tag
	// if (or.OrderStart == time.Time{}) {
	// 	or.OrderStart = time.Now()
	// }

	timeNow := models.CurrentTime()
	if or.OrderStart.Unix() <= 0 {
		or.OrderStart = timeNow
	} else if or.OrderStart.Location() == time.UTC {
		or.OrderStart = localtime.TimeInZone(or.OrderStart, config.St.Application.TimeZone)
	}
	oc.WeekDay = int(or.OrderStart.Weekday())
	oc.Hour = or.OrderStart.Hour()
	oc.Minute = or.OrderStart.Minute()
	oc.Minutestamp = timeNow.Hour()*60 + timeNow.Minute() //or.OrderStart.Hour()*60 + or.OrderStart.Minute()
	oc.Points = routesLength
	var m time.Month
	_, m, oc.Day = or.OrderStart.Date()
	oc.Month = int(m)

	// TODO: ошибка при введении координаты (больше 6 знаков после запятой) [ERROR #42601 syntax error at or near "44.68362045288086"]
	routesCount := len(or.Routes)
	if routesCount == 0 {
		return errors.Errorf("(fillSourceData) ruote count = 0")
	}

	return nil
}

func (oc *OrderTariffChars) fillSourceGisData(or OrderCRM, road []structures.PureCoordinates) error {
	routesLength := len(or.Routes)

	coords := []structures.PureCoordinates{}
	for _, item := range or.Routes {
		coords = append(coords, structures.PureCoordinates{Long: float64(item.Lon), Lat: float64(item.Lat)})
	}

	gisres := []struct {
		models.CrmZone
		gis.AllPointsBelongingToAllZonesResultItems
	}{}
	gisCrmZonesData.AllPointsBelongingToAllZones(coords, &gisres)

	for _, item := range gisres {
		r, err := gis.WKTParse(item.Intr)
		if err != nil {
			errpath.Err(err)
		}

		for _, mult := range r.Points {
			for _, pt := range mult {
				cs := structures.PureCoordinates{Long: float64(or.Routes[0].Lon), Lat: float64(or.Routes[0].Lat)}
				zone := structures.Zone{}
				if pt == cs {
					oc.GisData.StartRegions = append(oc.GisData.StartRegions, zone)
				}
				if routesLength > 1 {
					cf := structures.PureCoordinates{Long: float64(or.Routes[routesLength-1].Lon), Lat: float64(or.Routes[routesLength-1].Lat)}
					if pt == cf {
						oc.GisData.FinishRegions = append(oc.GisData.FinishRegions, zone)
					}
				}
			}
		}
	}

	if len(road) > 2 {
		result := []struct {
			models.CrmZone
			gis.MetersInZonesResultItems
		}{}
		err := gisCrmZonesData.MetersInZones(road, &result)
		if err != nil {
			return errpath.Err(err)
		}
		for _, item := range result {
			oc.GisData.RoadPiece = append(oc.GisData.RoadPiece, FromGis{ZoneName: item.Name, ZoneIntr: item.Intr, Dist: item.Distance})
		}
	} else {
		logrus.Warning(errpath.Funcc(), "пустые координаты")
	}

	return nil
}

// GetItinerary - получение данных с помощью OSRM по роутам в OrderCRM
func GetItinerary(routes []structures.Route) (structures.OSRMData, error) {
	var (
		reqString string
		inst      structures.OSRMData
	)
	if len(routes) <= 1 {
		return structures.OSRMData{}, nil
	}
	client := &http.Client{}
	reqString += config.St.OSRM.Host
	reqString += "/route/v1/driving/"

	for i, item := range routes {
		//http://router.project-osrm.org/route/v1/driving/44.689657,43.025560;44.690966,43.029741;44.693541,43.028117?geometries=geojson
		reqString += strconv.FormatFloat(float64(item.Lon), 'f', 6, 64) + "," + strconv.FormatFloat(float64(item.Lat), 'f', 6, 64)
		if i != len(routes)-1 {
			reqString += ";"
		}
	}
	reqString += "?geometries=geojson"

	req, err := http.NewRequest(
		"GET",
		reqString,
		nil,
	)
	if err != nil {
		fmt.Println(err)
		return structures.OSRMData{}, err
	}

	//req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return structures.OSRMData{}, err
	}
	defer resp.Body.Close()

	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(&inst)
	if err != nil {
		return inst, errors.Errorf("ошибка декодинга ответа с OSRM,%s", err)
	}
	if inst.Code != "Ok" {

		return structures.OSRMData{}, errpath.Errorf("статус не ОК")
	}

	return inst, nil
}

// GetRouteWayData - преобразовывает osrm данные в нужный формат
func GetRouteWayData(routes []structures.Route) (structures.RouteWayData, error) {
	var routeWayData structures.RouteWayData
	var routeToDriverJson structures.RouteToDriverJson
	var step int

	osrmData, err := GetItinerary(routes)
	if err != nil {
		return structures.RouteWayData{}, errpath.Err(err, "ошибка расчета дистанции")
	}

	routeWayData.Geometry.Way.Coor = osrmData.Routes[0].Geometry.Coor
	routeWayData.Geometry.Way.Type = osrmData.Routes[0].Geometry.Type
	routeWayData.Geometry.Type = "Feature"
	routeWayData.Geometry.Proper.Duration = int(osrmData.Routes[0].Duration)
	routeWayData.Geometry.Proper.Distance = osrmData.Routes[0].Distance

	for k, inst := range osrmData.Routes[0].Geometry.Coor {
		if osrmData.Waypoints[step].Location[0] == inst[0] {
			if step < len(routes)-1 {
				routeToDriverJson.Type = "Feature"
				routeToDriverJson.Proper.Duration = int(osrmData.Routes[0].Legs[step].Duration)
				routeToDriverJson.Proper.Distance = osrmData.Routes[0].Legs[step].Distance
				routeWayData.Routes = append(routeWayData.Routes, routeToDriverJson)
			}
			if step > 0 {
				routeWayData.Routes[step-1].Way.Coor = append(routeWayData.Routes[step-1].Way.Coor, osrmData.Routes[0].Geometry.Coor[k])
			}
			step++
		}
		if step == len(routes) {
			break
		}
		routeWayData.Routes[step-1].Way.Type = "LineString"
		routeWayData.Routes[step-1].Way.Coor = append(routeWayData.Routes[step-1].Way.Coor, osrmData.Routes[0].Geometry.Coor[k])
	}
	return routeWayData, nil
}

func getPenaltyForTransitionInZone(startZones, targetZones []models.CrmZone) (float64, error) {
	var penalty models.ZonesPenalty
	if len(startZones) == 0 || len(targetZones) == 0 {
		return 0, nil
	}
	var sourceZonesUUID, targetZonesUUID []string
	for _, item := range startZones {
		sourceZonesUUID = append(sourceZonesUUID, item.UUID)
	}
	for _, item := range targetZones {
		targetZonesUUID = append(targetZonesUUID, item.UUID)
	}
	exist, err := db.Model(&penalty).
		Where("start_zone_uuid in (?) and finish_zone_uuid in (?)", pg.In(sourceZonesUUID), pg.In(targetZonesUUID)).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return -1, errors.Errorf("exist error (getPenaltyForTransitionInZone):%s", err)
	}
	if !exist {
		return 0, nil
	}
	err = db.Model(&penalty).
		Where("start_zone_uuid in (?) and finish_zone_uuid in (?)", pg.In(sourceZonesUUID), pg.In(targetZonesUUID)).
		Where("deleted is not true").
		Order("priority desc").
		First()
	if err != nil {
		return -1, errors.Errorf("(getPenaltyForTransitionInZone):%s", err)
	}

	return penalty.Penalty, nil
}

func UpdateOrderTariff(orderUUID string, tariff structures.Tariff) error {
	// if tariff.TotalPrice <= 0 {
	// 	return errors.Errorf("zero tariff total price")
	// }
	_, err := db.Model(&OrderCRM{}).
		Where("uuid = ?", orderUUID).
		Set("tariff = ?", tariff).
		Update()
	return err
}

func GetOrderTariff(orderUUID string) (structures.Tariff, error) {
	var order OrderCRM
	err := db.Model(&order).
		Where("uuid = ?", orderUUID).
		Where("deleted is not true", orderUUID).
		Column("tariff").
		Select()
	return order.Tariff, err
}

func encodePrecalculatedTariff(surge *structures.SurgeItem) (string, error) {
	jwtSecret := config.JWTSecret()
	claims := TariffClaims{Surge: surge}
	claims.IssuedAt = time.Now().Unix()
	claims.ExpiresAt = time.Now().Add(TariffClaimsPeriod).Unix()
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(jwtSecret))
}

func decodePrecalculatedTariff(tokenString string) (TariffClaims, error) {
	if tokenString == "" {
		return TariffClaims{}, nil
	}

	var result TariffClaims
	jwtSecret := config.JWTSecret()
	_, err := jwt.ParseWithClaims(tokenString, &result, func(token *jwt.Token) (interface{}, error) {
		return []byte(jwtSecret), nil
	})
	if err != nil {
		return TariffClaims{}, err
	}
	return result, nil
}
