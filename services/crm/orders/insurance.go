package orders

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"sync"
)

const (
	defaultInsuranceState = false
	defaultInsuranceCost  = 0
)

type Insurance struct {
	mx     sync.RWMutex
	config structures.InsuranceParams
}

var CurrentInsurance = Insurance{
	config: structures.InsuranceParams{
		Enabled: defaultInsuranceState,
		Cost:    defaultInsuranceCost,
	},
}

func (in *Insurance) GetParams() structures.InsuranceParams {
	in.mx.RLock()
	defer in.mx.RUnlock()

	return in.config
}

func (in *Insurance) UpdateConfig(config structures.InsuranceParams) {
	in.mx.Lock()
	in.config = config
	in.mx.Unlock()
}
