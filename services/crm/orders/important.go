package orders

import (
	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
)

type ImportanceReason string

const (
	UnreadChatMessage           ImportanceReason = "unread_chat_mess"
	UnResolvedTicket            ImportanceReason = "unresolved_ticket"
	LateDriver                  ImportanceReason = "late_driver"
	WaitingForConformitionState ImportanceReason = "waiting_for_conformition_state"
	ChatBotCustomerAssistance   ImportanceReason = "chat_bot_customer_assistance"
	driverLateTimeSecond        int              = 120 //время, на которое должен опоздывать водила чтобы заказ считался важным
)

func AddImportantReasonToOrder(reason ImportanceReason, ordersUUID ...string) error {
	if len(ordersUUID) == 0 {
		return nil
	}
	_, err := db.Model(&OrderCRM{}).
		WhereIn("uuid in (?)", ordersUUID).
		Where("not(?::text[] <@ (COALESCE(importance_reasons, '{}')))", pg.Array([]ImportanceReason{reason})). //если такого элемента в массиве нет
		Set("importance_reasons = (? || importance_reasons)", pg.Array([]ImportanceReason{reason})).           //добавить к существующему массиву элемент
		Update()
	return err
}
func RemoveImportantReasonFromOrder(orderUUID string, reason ImportanceReason) error {
	_, err := db.Model(&OrderCRM{}).
		Where("uuid = ?", orderUUID).
		Set("importance_reasons = array_remove(importance_reasons, ?)", reason). //удаляем из массива элемент
		Update()
	return err
}
func UnmarkAsImportant(orderUUID string) error {
	_, err := db.Model(&OrderCRM{}).
		Where("uuid = ?", orderUUID).
		Set("importance_reasons = null").
		Update()
	return err
}

func GetOrdersWithLateDriversUUIDs() ([]string, error) {
	var uuids []string
	err := db.Model(&OrderCRM{}).
		Where("order_state = ?", constants.OrderStateStarted).
		//если разница между текущим временем и временем когда водила должен был приехать, больше константы
		Where("((extract(epoch from current_timestamp) - (driver_arrival_time_data ->> 'indicated_arrival_time')::integer)) > ?", driverLateTimeSecond).
		//если разница между текущим временем и временем когда водила должен был приехать, больше константы
		Where("not(?::text[] <@ (COALESCE(importance_reasons, '{}')))", pg.Array([]ImportanceReason{LateDriver})).
		Where("deleted IS NOT TRUE").
		Column("uuid").
		Select(&uuids)
	return uuids, err
}
