package orders

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// GetDriversForDistribution returns map where key is driverUUID and value is time when driver trans to this state
func GetDriversForDistribution(ctx context.Context, regionID int) (map[string]float64, error) {
	onlineDrivers, err := getOnlineDriversForDistribution(ctx, regionID)
	if err != nil {
		return nil, err
	}

	workingDrivers, distancesToFinish, err := getWorkingDriversForDistribution(ctx, regionID)
	if err != nil {
		return nil, err
	}

	result := make(map[string]float64, len(onlineDrivers)+len(workingDrivers))
	for _, driver := range onlineDrivers {
		result[driver.UUID] = time.Now().Sub(driver.StatusUpdateTime).Seconds()
	}
	for i, driver := range workingDrivers {
		result[driver.UUID] = distancesToFinish[i]
	}

	return result, nil
}

func getOnlineDriversForDistribution(ctx context.Context, regionID int) ([]models.DriverCRM, error) {
	var drivers []models.DriverCRM
	query := db.ModelContext(ctx, &drivers).
		Column("uuid", "status_update_time").
		Where("status = ?", constants.DriverStateOnline).
		Where("region_id = ?", regionID).
		Where("deleted IS NOT true")
	if err := query.Select(); err != nil {
		return nil, err
	}

	return drivers, nil
}

func getWorkingDriversForDistribution(ctx context.Context, regionID int) ([]models.DriverCRM, []float64, error) {
	params, err := models.GetDistributionParams()
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to fetch distribution params")
	}
	counterCooldown := time.Duration(params.CounterAssignIntervalSeconds) * time.Second
	// если водителю назначался встречный заказ после этого времени, то сейчас он все еще будет в кд
	cooldownTime := time.Now().Add(-counterCooldown)

	var drivers []models.DriverCRM
	// подзапрос проверяющий существование встречного заказа
	// важно: логика должна совпадать с используемой в CounterOrderExists(driverUUID string)
	counterOrderQuery := db.ModelContext(ctx, (*OrderCRM)(nil)).
		WhereIn("order_state in (?)", constants.ListActiveOrderStates()).
		Where("driver ->> 'uuid' = drivers.uuid").
		Where("counter_order_marker = true").
		Where("deleted IS NOT true")
	query := db.ModelContext(ctx, &drivers).
		Column("uuid", "status_update_time").
		Where("status = ?", constants.DriverStateWorking).
		Where("region_id = ?", regionID).
		Where("deleted IS NOT true").
		Where("counter_order_switch IS true").                                                 // кто включил прием встречных
		Where("last_counter_assignment IS NULL OR last_counter_assignment < ?", cooldownTime). // у кого прошел кд на прием встречных
		Where("NOT EXISTS(?)", counterOrderQuery)                                              // у кого нет имеющегося встречного заказа
	if err := query.Select(); err != nil {
		return nil, nil, err
	}

	driverUUIDs := make([]string, 0, len(drivers))
	for _, d := range drivers {
		driverUUIDs = append(driverUUIDs, d.UUID)
	}
	if len(driverUUIDs) == 0 {
		return nil, nil, nil
	}

	// подгружаем текущие координаты этих водителей
	driverLocations := models.CurrentDriverLocationsByUUIDs(driverUUIDs)

	// подгружаем текущие заказы этих водителей
	ordersMap, err := getCurrentOrders(ctx, driverUUIDs)
	if err != nil {
		return nil, nil, errors.Wrap(err, "failed to fetch current orders")
	}

	var (
		drvCoords       []structures.PureCoordinates
		ordCoords       []structures.PureCoordinates
		suitableDrivers []models.DriverCRM
	)
	for _, d := range drivers {
		order, ok := ordersMap[d.UUID]
		if !ok {
			// не найден текущий заказ водителя
			continue
		}
		if len(order.Routes) < 2 || order.Tariff.TariffCalcType == structures.CalcWithTime {
			// в заказах на время (с одной точкой)
			// невозможно определить когда водитель закончит
			continue
		}
		if order.OrderState != constants.OrderStateOnTheWay {
			// встречные заказы предлагаются только водителям,
			// чей текущий заказ в статусе "в пути"
			continue
		}
		driverLocation := structures.PureCoordinates{
			Lat:  driverLocations[d.UUID].Latitude,
			Long: driverLocations[d.UUID].Longitude,
		}
		finishPoint := structures.PureCoordinates{
			Lat:  float64(order.Routes[len(order.Routes)-1].Lat),
			Long: float64(order.Routes[len(order.Routes)-1].Lon),
		}
		if gis.Distance(driverLocation.Lat, driverLocation.Long,
			finishPoint.Lat, finishPoint.Long) > float64(params.CounterOrderAcceptionRadius) {
			// если расстояние по прямой больше радиуса принятия встречных
			// нет смысла проверять расстояние по дорогам (OSRM)
			continue
		}

		suitableDrivers = append(suitableDrivers, d)
		drvCoords = append(drvCoords, driverLocation)
		ordCoords = append(ordCoords, finishPoint)
	}
	if len(suitableDrivers) == 0 {
		return nil, nil, nil
	}

	// грузим расстояния по дорогам
	table, err := osrm.New(config.St.OSRM.Host).Table(append(drvCoords, ordCoords...), drvCoords, ordCoords)
	if err != nil {
		return nil, nil, errors.Wrap(err, "OSRM table request failed")
	}
	var driversInRadius []models.DriverCRM
	var distances []float64
	for i, d := range suitableDrivers {
		dist := table.GetDistance(i, i)
		if dist > float64(params.CounterOrderAcceptionRadius) {
			continue
		}
		driversInRadius = append(driversInRadius, d)
		distances = append(distances, dist)
	}

	return driversInRadius, distances, nil
}

func getCurrentOrders(ctx context.Context, driverUUIDs []string) (map[string]OrderCRM, error) {
	var orders []OrderCRM
	query := db.ModelContext(ctx, &orders).
		WhereIn("order_state IN (?)", constants.ListActiveOrderStates()).
		WhereIn("driver ->> 'uuid' IN (?)", driverUUIDs).
		Where("counter_order_marker = false").
		Where("deleted IS NOT true")
	if err := query.Select(); err != nil {
		return nil, err
	}

	result := make(map[string]OrderCRM)
	for _, o := range orders {
		result[o.Driver.UUID] = o
	}

	return result, nil
}
