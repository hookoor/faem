package orders

import (
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// OfferStates struct {
// 	OrderUUID  string    `json:"order_uuid"`
// 	DriverUUID string    `json:"driver_uuid"`
// 	OfferUUID  string    `json:"offer_uuid"`
// 	State      string    `json:"state"`
// 	StartState time.Time `json:"start_state"`
// }

// OrderStateCRM - структура для хранения статуса
type OrderStateCRM struct {
	tableName struct{}  `sql:"crm_orderstates"`
	ID        int       `sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"created_at"`
	structures.OfferStates
}

// NewOrderState возвращает экземпляр ОрдерСтейта с уже настроенной стейт машиной
// если статус новый - вернет ошибку, так как статус не будет найден
// func NewOrderState(rq structures.OfferStates) (OrderStateCRM, error) {
// 	var osc OrderStateCRM
// 	osc.OrderUUID = rq.OrderUUID
// 	osc.DriverUUID = rq.DriverUUID
// 	osc.OfferUUID = rq.OfferUUID
// 	osc.StartState = rq.StartState
// 	err := db.Model(&osc).
// 		Column("state").
// 		Where("order_uuid = ?", osc.OrderUUID).
// 		Order("start_state DESC").
// 		Limit(1).
// 		Select()
// 	if err != nil {
// 		return OrderStateCRM{}, err
// 	}

// 	return osc, nil
// }

//NewOrderState создает новый статус заказа
func (ordState OrderStateCRM) NewOrderState() (OrderStateCRM, error) {
	ordState.StartState = time.Now()
	err := ordState.saveNewState()
	if err != nil {
		return OrderStateCRM{}, errors.Errorf("Error creating new orderstate,%s", err)
	}

	return ordState, nil
}

func (osc *OrderStateCRM) saveNewState() error {
	var ordCRM OrderCRM

	query := db.Model(&ordCRM).
		Set("order_state = ?", osc.State).
		Set("state_transfer_time = ?", time.Now()).
		Where("uuid = ?", osc.OrderUUID)
	// сохранение водителя в структуру заказа
	if osc.State == constants.OrderStateAccepted || osc.State == constants.OrderStateOffered {
		var (
			driver models.DriverCRM
		)
		err := db.Model(&driver).
			Where("uuid = ?", osc.DriverUUID).
			Where("deleted is not true").
			Limit(1).
			Select()
		if err != nil {
			return errors.Errorf("Error finding driver to order. DriverUUID=%s. %s", osc.DriverUUID, err)
		}
		query = query.
			Set("driver = ?", driver.Driver)
		if osc.State == constants.OrderStateAccepted {
			arTime, err := strconv.ParseInt(osc.Comment, 10, 64)
			if err != nil {
				return errors.Errorf("Error converting arrival time: %s", err)
			}
			query = query.
				Set("arrival_time = ?", arTime)
		}
	}
	if osc.State == constants.OrderStateRejected ||
		osc.State == constants.OrderStateFree ||
		osc.State == constants.OrderStateDistributing {
		query = query.
			Set("driver = null").
			Set("delivered_to_driver = false")
	}
	if osc.State == constants.OrderStateFinished ||
		osc.State == constants.OrderStateCancelled ||
		osc.State == constants.OrderStateDriverNotFound {
		query = query.
			Set("complete_time = current_timestamp")
	}
	if osc.State == constants.OrderStateWaiting || osc.State == constants.OrderStateOnPlace {
		query.Set(fmt.Sprintf("driver_arrival_time_data = jsonb_set(COALESCE(driver_arrival_time_data, '{}'), '{remaining_waiting_time}', '%v')", osc.FreeTime))
	}
	if osc.State == constants.OrderStateOnPlace {
		go RemoveImportantReasonFromOrder(osc.OrderUUID, LateDriver)
	}
	_, err := query.
		Update()
	if err != nil {
		return errors.Errorf("Error update order state. OrderUUID=%s, New state=%s. %s", osc.OrderUUID, osc.State, err)
	}
	_, err = db.Model(osc).
		Insert()
	return err
}

func GetOrderCurrentState(orderUUID string) (string, error) {
	var result OrderCRM
	err := db.Model(&result).
		Column("order_state").
		Where("uuid = ?", orderUUID).
		Select()
	return result.OrderState, err
}

func GetCurrent(orderUUID string) (string, error) {
	var result OrderCRM
	err := db.Model(&result).
		Column("order_state").
		Where("uuid = ?", orderUUID).
		Select()
	return result.OrderState, err
}
