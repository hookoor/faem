package orders

import "gitlab.com/faemproject/backend/faem/services/crm/models"

type regionalSurge struct {
	Orders  []*OrderCRM
	Drivers []*models.DriverCRM
}

func SplitOrdersAnDriversByRegion(orders []*OrderCRM, drivers []*models.DriverCRM) map[int]*regionalSurge {
	result := make(map[int]*regionalSurge)
	for _, order := range orders {
		if surgeData, ok := result[order.RegionID]; ok {
			surgeData.Orders = append(result[order.RegionID].Orders, order)
		} else {
			result[order.RegionID] = &regionalSurge{
				Orders: []*OrderCRM{order},
			}
		}
	}

	for _, driver := range drivers {
		if surgeData, ok := result[driver.RegionID]; ok {
			surgeData.Drivers = append(surgeData.Drivers, driver)
		} else {
			result[driver.RegionID] = &regionalSurge{
				Drivers: []*models.DriverCRM{driver},
			}
		}
	}

	return result
}
