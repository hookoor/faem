package config

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/faemproject/backend/faem/pkg/logs"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	// Envs - переменная в которой раньше хранились данные
	Envs map[string]string

	// St ([St]orage) - переменная для хранения конфигурации
	St AppConfig
)

// По новому стайлу все переменные хранятся в структуре St, но что бы не пережделывать
// оставил старую структуру [Envs], и просто ей присвоил новые данные, но сами данные
// также доступны и в новой структуре

// InitConfig initialize config
func InitConfig(filepath string) {
	// инициализируем дефольные значения
	initDefaults()

	// читаем конфиг
	viper.SetConfigFile(filepath)
	err := viper.ReadInConfig()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "Init config",
		}).Error(fmt.Sprintf("Error reading config file. %s", err))
		// logs.OutputError(fmt.Sprintf("Error reading config file. %s", err))
	}

	// А теперь можно почитать переменные окружения
	bindEnvVars()

	err = viper.Unmarshal(&St)
	if err != nil {
		fmt.Println(errors.Errorf("Error unmarshalling config file. %s", err))
	}

	Envs = initEnvsOld()
}

func initEnvsOld() map[string]string {

	m := map[string]string{
		"host":       viper.GetString("Database.host"),
		"user":       viper.GetString("Database.user"),
		"password":   viper.GetString("Database.password"),
		"port":       viper.GetString("Database.port"),
		"db":         viper.GetString("Database.db"),
		"brokerURL":  viper.GetString("Broker.userURL"),
		"brokerUser": viper.GetString("Broker.userCredits"),
	}

	return m
}

func bindEnvVars() {
	viper.SetEnvPrefix("CRM")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// PrintVars output env vars
func PrintVars() {
	fmt.Println("\nEVEIROINMENT VARS:")
	fmt.Printf("host:       %s\n", Envs["host"])
	fmt.Printf("user:       %s\n", Envs["user"])
	fmt.Printf("password:   %s\n", Envs["password"])
	fmt.Printf("port:       %s\n", Envs["port"])
	fmt.Printf("db:         %s\n", Envs["db"])
	fmt.Printf("brokerURL:  %s\n", Envs["brokerURL"])
	fmt.Printf("brokerUser: %s\n", Envs["brokerUser"])
}

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}
