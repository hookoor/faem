package config

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/spf13/viper"
)

// AppConfig main configuration struct
type AppConfig struct {
	Application struct {
		Port                 string
		TokenExpiredM        int64
		RefreshTokenExpiredM int64
		SessionExpM          int64
		DistributingTime     string
		// DeliveryTimeToDriverS - время для подтверждения того, что заказа пришел (пуш) по FSM водителю.
		// Если не подтвердить заказ перейдет из "offer_offered" в "smart_distribution".
		// (время ожидания доставки заказа до водителя)
		DeliveryTimeToDriverS    int
		DrvLocShelfLifeSec       int // DrvLocShelfLifeSec срок годности координат водителя для показывания на карте
		SaveDriverLocationPeriod string
		TimeZone                 string
		LogLevel                 string
		LogFormat                string
	}
	Database struct {
		Host     string
		User     string
		Password string
		Port     int
		Db       string
	}
	Broker struct {
		UserURL        string
		UserCredits    string
		DriverURL      string
		DriverCredist  string
		ExchagePrefix  string
		ExchagePostfix string
	}
	Elastic struct {
		Host  string
		Index string
		Port  string
	}
	Auth struct {
		Host      string
		ServiceID int
	}
	Bonuses struct {
		Host string
	}
	// open street routing machine
	OSRM struct {
		// Host - !!! значение перезаписывается базой
		Host string
	}
	TelephonyRecordStorage struct {
		Host string // адреса бакета
	}
	Billing struct {
		Host     string
		Username string
		Password string
	}
	Chat struct {
		Host string
	}
	CloudStorage struct {
		ProjectID  string
		BucketName string
	}
	DriverService struct {
		Host string
	}
}

// Установка дефолтныйз значений
func initDefaults() {
	// APPLICATION DATA
	viper.SetDefault("Application.Port", structures.CRMPort)
	viper.SetDefault("Application.TokenExpiredM", 1440)
	viper.SetDefault("Application.RefreshTokenExpiredM", 30240)
	viper.SetDefault("Application.SessionExpM", 30)
	viper.SetDefault("Application.DrvLocShelfLifeSec", 180)
	viper.SetDefault("Application.DistributingTime", "49m")
	viper.SetDefault("Application.DeliveryTimeToDriverS", 10)
	viper.SetDefault("Application.SaveDriverLocationPeriod", "5s")
	viper.SetDefault("Application.TimeZone", "Europe/Moscow")
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")

	// DATABASE DATA
	viper.SetDefault("Database.Host", "78.110.156.74")
	viper.SetDefault("Database.User", "barman")
	viper.SetDefault("Database.Password", "ba4man80")
	viper.SetDefault("Database.Port", 6001)
	viper.SetDefault("Database.Db", "crmDebug")
	// RABBIT DATA
	viper.SetDefault("Broker.UserURL", "78.110.156.74:6004")
	viper.SetDefault("Broker.UserCredits", "barmen:kfclover97")
	viper.SetDefault("Broker.ExchagePrefix", "")
	viper.SetDefault("Broker.ExchagePostfix", "")
	// ELASTIC DATA
	viper.SetDefault("Elastic.Host", "78.110.156.74")
	viper.SetDefault("Elastic.Port", "6006")
	viper.SetDefault("Elastic.Index", "crm_logs")

	// TODO: добавить в вайпер
	// open street routing machine
	viper.SetDefault("OSRM.Host", "http://osrm.faem.svc.cluster.local")

	// адреса бакета
	viper.SetDefault("TelephonyRecordStorage.Host", "https://storage.googleapis.com/faem-records-001/")

	//bonuses
	viper.SetDefault("Bonuses.Host", "https://bonuses.apis.stage.faem.pro/rpc/v1")
	//auth
	viper.SetDefault("Auth.Host", "http://localhost:1322/api/v3")
	viper.SetDefault("Auth.ServiceID", 1)
	// Billing
	viper.SetDefault("Billing.Host", "https://billing.apis.stage.faem.pro/rpc/v1")
	viper.SetDefault("Billing.Username", "")
	viper.SetDefault("Billing.Password", "")

	// Chat
	viper.SetDefault("Chat.Host", "https://chat.apis.stage.faem.pro/api/v2")

	viper.SetDefault("CloudStorage.ProjectID", "faem-staging-01")
	viper.SetDefault("CloudStorage.BucketName", "faem-staging-images-storage")

	viper.SetDefault("DriverService.Host", "https://driver.apis.stage.faem.pro/api/v2")
}

// TODO: По хорошему надо также указать переменные для рэбита, а именно название очередей и пр.
// проблема в том что сейчас используются переменные из общего пакета, нужно сделать
// их дефольными, а из конфига их можно переписать
