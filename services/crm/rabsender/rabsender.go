package rabsender

import (
	"encoding/json"
	"fmt"
	"reflect"
	"sync"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
)

var rb *rabbit.Rabbit
var wg *sync.WaitGroup

const (
	// AcNewPushMailing godoc
	AcNewPushMailing = "push_mailing"
	// AcPhotoControl godoc
	AcPhotoControl     = "photocontrol"
	AcNewTicketStatus  = "new_ticket_status"
	AcNewTicketComment = "new_ticket_comment"
	AcNewTicket        = "new_ticket_"
	// AcOrderToDriver ключ для отправки заказа
	AcOrderToDriver     = "new_order"
	AcNewSMS            = "new_sms"
	AcNewDriverToDriver = "new_driver"

	AcUpdateOrderProductData = "product_data_update"
	// AcDriverStateToDriver ключ для отправки водительского статуса
	AcDriverStateToDriver = "driver_state"
	// AcDriverData - ключ отправки данных для водителя
	AcDriverData = "driver_data"
	// AcOrderState ключ статуса заказа
	AcOrderState = "order_state"
	// AcNewDriverDataToDriver ключ данных водителей
	AcNewDriverDataToDriver = "update_driver"
	// AcNewOrderData новые данные заказа
	AcNewOrderData = "update_order"
	// AcOrderReDistibution перераспределение заказа
	AcOrderReDistibution   = "redistr_order"
	OrderDeliveredToDriver = "order_delivered_to_driver"
	// AcNewOrderDataFromCRM заказ обновил оператор
	AcNewOrderDataFromCRM = "update_order_crm"
	// AcCrmNewOptionsToClient ключ опций заказа
	AcCrmNewOptionsToClient          = "option_to_client"
	AcOperatorUnmarkOrderAsImportant = "important_unmark"
	AcNewOrderDataWhenDriverAccepted = "update_order_crm_driver_accepted"
	//AcClientData обновленный или новый клиент
	AcClientData = "change_to_client"

	// AcOperatorData - любые действия оператора
	AcOperatorData = "operator_events"

	// Make a new transfer for a driver
	AcMakeTransfer = "make_transfer"

	// AcNewDriverDataToDriver ключ данных водителей
	AcDeleteDriver = "delete_driver"
	AcDeleteClient = "delete_client"

	// AcPublishEvent ключ для отправки ивентов
	AcPublishEvent = "publish_event"
)

func ItitVars(r *rabbit.Rabbit, waiG *sync.WaitGroup) {
	rb = r
	wg = waiG
}

// SendJSONByAction выбиракм экшен и отправляем
func SendJSONByAction(action string, payload interface{}, key ...string) error {
	var err error
	switch action {
	case AcOrderToDriver:
		// orders.OrderCRM
		err = sendJSON(rabbit.OrderExchange, rabbit.NewKey, payload)
	case AcDriverStateToDriver:
		rkey := fmt.Sprintf("%s.%s", rabbit.StateKey, key[0])
		// DriverStates
		err = sendJSON(rabbit.DriverExchange, rkey, payload)
	case AcDriverData:
		err = sendJSON(rabbit.DriverExchange, key[0], payload)
	case AcOrderState:
		rkey := fmt.Sprintf("%s.%s", rabbit.StateKey, key)
		err = sendJSON(rabbit.OrderExchange, rkey, payload)
	case AcNewOrderData:
		// structures.Order
		var tag string
		if len(key) >= 1 {
			tag = key[0]
		}
		err = sendJSON(rabbit.OrderExchange, rabbit.UpdateKey, payload, tag)
	case AcOrderReDistibution:
		// structures.OrderReDistributionData
		err = sendJSON(rabbit.OrderExchange, rabbit.ReDistributionKey, payload)
	case AcNewOrderDataWhenDriverAccepted:
		err = sendJSON(rabbit.OrderExchange, rabbit.OrderUpdateFromCRMKey, payload, constants.OrderStateAccepted)
	case AcNewOrderDataFromCRM:
		// structures.Order
		err = sendJSON(rabbit.OrderExchange, rabbit.OrderUpdateFromCRMKey, payload)
	case AcNewDriverToDriver:
		// structures.Driver
		err = sendJSON(rabbit.DriverExchange, rabbit.NewKey, payload)
	case AcNewPushMailing:
		err = sendJSON(rabbit.FCMExchange, rabbit.MailingKey, payload)
	case OrderDeliveredToDriver:
		err = sendJSON(rabbit.OrderExchange, rabbit.OrderDeliveredKey, payload)
	case AcNewSMS:
		err = sendJSON(rabbit.ClientExchange, rabbit.SMSKey, payload)
	case AcNewDriverDataToDriver:
		// structures.Driver
		err = sendJSON(rabbit.DriverExchange, rabbit.UpdateKey, payload)
	case AcCrmNewOptionsToClient:
		// DataWithMarker
		err = sendJSON(rabbit.OrderExchange, rabbit.OptionsUpdateKey, payload)
	case AcClientData:
		err = sendJSON(rabbit.ClientExchange, rabbit.UpdateKey, payload)
	case AcNewTicketComment:
		err = sendJSON(rabbit.TicketsExchange, rabbit.UpdateKey, payload)
	case AcNewTicket:
		err = sendJSON(rabbit.TicketsExchange, rabbit.NewKey, payload)
	case AcNewTicketStatus:
		err = sendJSON(rabbit.TicketsExchange, rabbit.StateKey, payload)
	case AcOperatorData:
		err = sendJSON(rabbit.OperatorExchange, key[0], payload)
	case AcPhotoControl:
		err = sendJSON(rabbit.DriverExchange, rabbit.PhotoControlUpdateKey, payload)
	case AcOperatorUnmarkOrderAsImportant:
		err = sendJSON(rabbit.OrderExchange, rabbit.UnmarkKey, payload)
	case AcMakeTransfer:
		err = sendJSON(rabbit.BillingExchange, rabbit.NewKey, payload)
	case AcDeleteDriver:
		err = sendJSON(rabbit.DriverExchange, rabbit.DeleteKey, payload)
	case AcDeleteClient:
		err = sendJSON(rabbit.ClientExchange, rabbit.DeleteKey, payload)
	case AcPublishEvent:
		var tag string
		if len(key) >= 1 {
			tag = key[0]
		}
		err = sendJSON(rabbit.EventExchange, tag, payload)
	default:
		err = errors.Errorf("Unknown action, cant send")
	}
	if err != nil {
		return err
	}
	return nil
}

func hasInterfaceField(i interface{}, FieldName string) bool {
	value := reflect.ValueOf(i)
	// Check if the passed interface is a pointer
	if value.Type().Kind() != reflect.Ptr {
		value = reflect.New(reflect.TypeOf(i))
	}
	// 'dereference' with Elem() and get the field by name
	field := value.Elem().FieldByName(FieldName)
	if !field.IsValid() {
		return false
	}
	return true
}

// SendJSON сабж
func sendJSON(exchange string, key string, payload interface{}, tag ...string) error {
	wg.Add(1)
	defer wg.Done()

	amqpHeader := amqp.Table{}
	amqpHeader["publisher"] = "crm"
	if len(tag) != 0 {
		amqpHeader["tag"] = tag[0]
	} else {
		amqpHeader["tag"] = ""
	}
	pl, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	senderChannel, err := rb.GetSender("crmSender")
	if err != nil {
		return errors.Wrap(err, "failed to get a sender channel")
	}

	err = senderChannel.Publish(
		exchange, // exchange
		key,      // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         pl,
			Headers:      amqpHeader,
			DeliveryMode: amqp.Persistent,
		})
	keyExch := fmt.Sprintf("key=%s, exchange=%s", key, exchange)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "Sending to Rabbit [crm]",
			"value": keyExch,
		}).Error(err)
		return err
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event": "Sending to Rabbit [crm]",
		"value": keyExch,
	}).Debug("Sended: OK!")

	return nil
}
