package rabhandler

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

func initRabChatMess() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmChatMessagesQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmChatMessagesQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,              // queue name
		"chat.message"+".*", // routing key
		rabbit.FCMExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                         // queue
		rabbit.CrmChatMessagesConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleChatMessages(msgs)
	return nil
}

func handleChatMessages(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			switch d.RoutingKey {
			case rabbit.ChatMessageToOperatorKey:
				handleNewChatMessage(d)
			case rabbit.ChatMessagesMarkedAsReadKey:
				handleNewChatMessagesRead(d)
			}
		}
	}
}

func handleNewChatMessage(d amqp.Delivery) {
	if d.Headers["publisher"] == "crm" {
		return
	}
	var mess structures.ChatMessages
	err := json.Unmarshal([]byte(d.Body), &mess)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "handling new chat message",
		"messageUUID": mess.UUID,
		"orderUUID":   mess.OrderUUID,
		"senderType":  mess.Sender,
		"routingKey":  d.RoutingKey,
	})
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error binding data"}).Error(err)
		return
	}
	if mess.Receiver != structures.UserCRMMember {
		log.Debug("receiver is not operator")
		return
	}
	err = orders.AddImportantReasonToOrder(orders.UnreadChatMessage, mess.OrderUUID)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error adding important reason to order"}).Error(err)
		return
	}
	log.Info("chat message successfully received")
}

func handleNewChatMessagesRead(d amqp.Delivery) {
	if d.Headers["publisher"] == "crm" {
		return
	}
	var mess structures.ChatMessagesMarkedAsRead
	err := json.Unmarshal([]byte(d.Body), &mess)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling new chat messages read",
		"orderUUID":  mess.OrderUUID,
		"readerType": mess.ReaderType,
		"routingKey": d.RoutingKey,
	})
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error binding data"}).Error(err)
		return
	}
	if mess.ReaderType != structures.UserCRMMember {
		log.Debug("reader is not operator")
		return
	}
	err = orders.RemoveImportantReasonFromOrder(mess.OrderUUID, orders.UnreadChatMessage)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error remove important reason from order"}).Error(err)
		return
	}
	log.Info("chat message read successfully received")
}
