package rabhandler

import (
	"encoding/json"
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/services/driver/distribution"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"golang.org/x/net/context"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

type (
	// Events события
	Events struct {
		tableName struct{} `sql:"crm_events"`
		models.EventItem
		DriverData        []driverData             `json:"drivers" sql:"-"`
		OrderData         []orderData              `json:"orders" sql:"-"`
		OperatorData      []operatorData           `json:"users" sql:"-"`
		DriverCoordinates []models.LocationDataCRM `json:"driver_coordinates,omitempty" sql:"driver_coordinates"`
	}

	// Telephony - поля для заполнения ивента данными с телефонии
	Telephony struct {
		structures.TelephonyCall
		Datetime time.Time `json:"datetime"` // время звонка на стороне voip сервиса
	}

	orderData struct {
		UUID string `json:"uuid"`
		ID   int    `json:"id"`
	}
	driverData struct {
		UUID  string `json:"uuid"`
		Alias int    `json:"alias"`
	}
	operatorData struct {
		UUID      string `json:"uuid"`
		Name      string `json:"name"`
		Initiator bool   `json:"initiator"`
	}
	// EventsCriteria params by which going on evens filtering
	EventsCriteria struct {
		Pager                 urlvalues.Pager `json:"pager"`
		OrderUUID             string          `json:"order_uuid"`
		TicketUUID            string          `json:"ticket_uuid"`
		MinDate               time.Time       `json:"min_date"`
		MaxDate               time.Time       `json:"max_date"`
		DriverUUID            string          `json:"driver_uuid"`
		StoreUUID             string          `json:"store_uuid"`
		ClientUUID            string          `json:"client_uuid"`
		OperatorUUID          string          `json:"operator_uuid"`
		TelephonyCallerNumber string          `json:"caller_number"`
		Exten                 string          `json:"exten"`
	}
)

var (
	valuesForStoreUser = []string{
		constants.OrderStateCreated,
		constants.OrderStateCooking,
		constants.OrderStateAccepted,
		constants.OrderStateOnPlace,
		constants.OrderStateOnTheWay,
		constants.OrderStateFinished,
		constants.OrderStateCancelled,
	}
	orderStatesTransForStoreUser = map[string]string{
		constants.OrderStateCreated:   "Заказ создан",
		constants.OrderStateCooking:   "Заказ готовится",
		constants.OrderStateAccepted:  "Водитель назначен",
		constants.OrderStateOnPlace:   "Водитель подъехал",
		constants.OrderStateOnTheWay:  "Водитель доставляет заказ",
		constants.OrderStateFinished:  "Заказ доставлен",
		constants.OrderStateCancelled: "Заказ отменен",
	}
)

const (
	// EventsParamAllClientsPhones это можно прислать вместо номера клиента чтобы получить историю всех звонков
	EventsParamAllClientsPhones = "for_all_client"
)

func initEvents() error {
	err := initOrderEvents()
	if err != nil {
		return errors.Errorf("error init order events,%s", err)
	}
	err = initDriverEvents()
	if err != nil {
		return errors.Errorf("error init driver events,%s", err)
	}
	err = initFCMEvents()
	if err != nil {
		return errors.Errorf("error init FCM events,%s", err)
	}
	err = initTicketsEvents()
	if err != nil {
		return errors.Errorf("error init tickets events,%s", err)
	}
	err = initClientEvents()
	if err != nil {
		return errors.Errorf("error init client events,%s", err)
	}
	err = initOperatorEvents()
	if err != nil {
		return errors.Errorf("error init operator events,%s", err)
	}
	err = initTelephonyEvents()
	if err != nil {
		return errors.Errorf("error init operator events,%s", err)
	}
	if err = initBillingEvents(); err != nil {
		return errors.Errorf("error init billing events,%s", err)
	}
	if err = initEventsExchange(); err != nil {
		return errors.Errorf("error init short events exchange,%s", err)
	}

	return nil
}

func initEventsExchange() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventExchange)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.EventExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}
	return nil
}

func initOrderEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsOrderQueue, // name
		true,                    // durable
		false,                   // delete when unused
		false,                   // exclusive
		false,                   // no-wait
		nil,                     // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		"#",                  // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                     // queue
		rabbit.EventsOrderConsumer, // consumer
		true,                       // auto-ack
		false,                      // exclusive
		false,                      // no-local
		false,                      // no-wait
		nil,                        // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderEvents(msgs)
	return nil
}

func initFCMEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsFCMQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsFCMQueue, // name
		true,                  // durable
		false,                 // delete when unused
		false,                 // exclusive
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,             // queue name
		"#",                // routing key
		rabbit.FCMExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                   // queue
		rabbit.EventsFCMConsumer, // consumer
		true,                     // auto-ack
		false,                    // exclusive
		false,                    // no-local
		false,                    // no-wait
		nil,                      // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleFCMEvents(msgs)
	return nil
}

func initDriverEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsDriverQueue, // name
		true,                     // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		"#",                   // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                      // queue
		rabbit.EventsDriverConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverEvents(msgs)
	return nil
}

func initClientEvents() error {
	return nil
}

func initTicketsEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsTicketsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.TicketsExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsTicketsQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                 // queue name
		"#",                    // routing key
		rabbit.TicketsExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                       // queue
		rabbit.EventsTicketsConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleTicketsEvents(msgs)
	return nil
}
func initOperatorEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsOperatorQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.OperatorExchange, // name
		"topic",                 // type
		true,                    // durable
		false,                   // auto-deleted
		false,                   // internal
		false,                   // no-wait
		nil,                     // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsOperatorQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                  // queue name
		"#",                     // routing key
		rabbit.OperatorExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                        // queue name
		rabbit.ChatMessageToDriverKey, // routing key
		rabbit.FCMExchange,            // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                        // queue
		rabbit.EventsOperatorConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOperatorEvents(msgs)
	return nil
}

func initBillingEvents() error {
	channel, err := rb.GetReceiver(rabbit.EventsBillingQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = channel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := channel.QueueDeclare(
		rabbit.EventsBillingQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}

	err = channel.QueueBind(
		q.Name,                 // queue name
		"#",                    // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := channel.Consume(
		q.Name,                       // queue
		rabbit.EventsBillingConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleBillingEvents(msgs)
	return nil
}

func handleFCMEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "handling fcm events",
			})
			// publishHead := d.Headers["publisher"]
			// publisher, ok := publishHead.(string)
			// if !ok {
			// 	logs.Eloger.WithFields(logrus.Fields{
			// 		"event":  "handling fcm events",
			// 		"reason": "error parse publisher header",
			// 	}).Error(publishHead)
			// 	continue
			// }

			switch {
			case d.RoutingKey == rabbit.ChatMessageToDriverKey:
				var mess structures.ChatMessages
				memberHead := d.Headers[rabbit.ChatMessagesMemberHeaderName]
				memberUUID, ok := memberHead.(string)
				if !ok {
					log.WithFields(logrus.Fields{
						"reason": "error parse member header",
					}).Error(memberHead)
					continue
				}
				err := json.Unmarshal([]byte(d.Body), &mess)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "error binding new message data",
					}).Error(err)
					continue
				}

				if mess.Sender != structures.UserCRMMember {
					continue
				}
				err = newChatMessagesFromOperatorToDriver(mess, memberUUID)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "error creating new chatmessage event",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.ChatMessagesMarkedAsReadKey:
				var messData structures.ChatMessagesMarkedAsRead
				memberHead := d.Headers[rabbit.ChatMessagesMemberHeaderName]
				memberUUID, ok := memberHead.(string)
				if !ok {
					log.WithFields(logrus.Fields{
						"reason": "error parse member header",
					}).Error(memberHead)
					continue
				}
				err := json.Unmarshal([]byte(d.Body), &messData)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "error binding data",
					}).Error(err)
					continue
				}
				if messData.ReaderType != structures.UserCRMMember {
					continue
				}
				err = newMessagesMarkedAsReadEvent(messData, memberUUID)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "error creating event for messages read mark",
					}).Error(err)
					continue
				}
			default:
				log.WithFields(logrus.Fields{
					"reason": "routing key identification",
				}).Debug("unknown routing key")
				continue
			}
			log.WithFields(logrus.Fields{
				"reason": "new event by fcm",
			}).Info("event creation was succesfull")
		}
	}
}

func handleDriverEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			publishHead := d.Headers["publisher"]
			publisher, ok := publishHead.(string)
			if !ok {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling driver events",
					"reason": "error parse publisher header",
				}).Error(publishHead)
				continue
			}

			switch {
			case d.RoutingKey == rabbit.NewKey:
				var (
					driver structures.Driver
				)
				err := json.Unmarshal([]byte(d.Body), &driver)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error binding new order",
					}).Error(err)
					continue
				}
				err = newDriverEvent(driver, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error creating event for new driver",
					}).Error(err)
					continue
				}
			case strings.HasPrefix(d.RoutingKey, rabbit.StateKey):
				var (
					driverState structures.DriverStates
				)
				err := json.Unmarshal([]byte(d.Body), &driverState)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error binding new driver state",
					}).Error(err)
					continue
				}
				err = newDriverStateEvent(driverState, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error creating event for new driver state",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.UpdateKey:
				var bindData structures.WrapperDriver
				err := json.Unmarshal([]byte(d.Body), &bindData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error binding new driver data",
					}).Error(err)
					continue
				}

				err = newDriverUpdateEvent(bindData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error creating event for new driver data",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.ActivityChangedKey:
				var activityHistoryItem structures.ActivityHistoryItem
				if err := json.Unmarshal(d.Body, &activityHistoryItem); err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error binding activity history data",
					}).Error(err)
					continue
				}

				if err := newDriverActivityChangedEvent(activityHistoryItem, publisher); err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling driver events",
						"reason": "error creating event for changed driver activity",
					}).Error(err)
					continue
				}
			default:
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling driver events",
					"reason": "routing key identification",
				}).Debug("unknown routing key")
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "handling driver events",
				"reason": "new event by driver",
			}).Info("event creation was succesfull")
		}
	}
}

func handleOrderEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			publishHead := d.Headers["publisher"]
			publisher, ok := publishHead.(string)
			if !ok {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling order events",
					"reason": "error parse publisher header",
				}).Error(publishHead)
				continue
			}

			switch {
			case d.RoutingKey == rabbit.NewKey:
				var bindOrderData structures.WrapperOrder
				err := json.Unmarshal([]byte(d.Body), &bindOrderData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new order",
					}).Error(err)
					continue
				}
				err = newOrderEvent(bindOrderData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for new order",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.DistributionKey:
				var (
					disData distribution.Match
				)
				err := json.Unmarshal([]byte(d.Body), &disData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding distribution data",
					}).Error(err)
					continue
				}
				err = newDistributionEvent2(disData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for distribution data",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.ReDistributionKey:
				var (
					reDisData structures.OrderReDistributionData
				)
				err := json.Unmarshal([]byte(d.Body), &reDisData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding redistribution data",
					}).Error(err)
					continue
				}
				err = newReDistributionEvent(reDisData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for redistribution data",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.OrderDeliveredKey:

				var data structures.OrderDeliveredMessage

				err := json.Unmarshal([]byte(d.Body), &data)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":      "handling order events",
						"reason":     "error binding data",
						"routingKey": d.RoutingKey,
					}).Error(err)
					continue
				}
				err = newOrderDeliveredToDriverEvent(data)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for order delivered message",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.UnmarkKey:
				var (
					data structures.UnmarkOrderAsImportant
				)
				err := json.Unmarshal([]byte(d.Body), &data)
				log := logs.Eloger.WithFields(logrus.Fields{
					"event":        "handling order unmark events",
					"orderUUID":    data.OrderUUID,
					"operatorUUID": data.OperatorUUID,
				})
				if err != nil {
					log.WithFields(logrus.Fields{"reason": "error binding data"}).Error(err)
					continue
				}
				err = newOrderUnmarkEvent(data, publisher)
				if err != nil {
					log.WithFields(logrus.Fields{"reason": "error creating event"}).Error(err)
					continue
				}

			case strings.HasPrefix(d.RoutingKey, rabbit.StateKey):
				var (
					orderState structures.OfferStates
				)
				err := json.Unmarshal([]byte(d.Body), &orderState)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new order state",
					}).Error(errpath.Err(err))
					continue
				}
				err = newOrderStateEvent(orderState, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for new order state",
					}).Error(err)
					continue
				}

			case d.RoutingKey == rabbit.UpdateKey:
				var bindOrderData structures.WrapperOrder
				err := json.Unmarshal([]byte(d.Body), &bindOrderData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new order data",
					}).Error(err)
					continue
				}
				err = newOrderUpdataEvent(bindOrderData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error creating event for new order data",
					}).Error(err)
					continue
				}

			case d.RoutingKey == rabbit.UpdateIncreasedFareKey:
				var bindOrderData structures.WrapperOrder
				err := json.Unmarshal([]byte(d.Body), &bindOrderData)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new increased fare data",
					}).Error(err)
					continue
				}
				err = newOrderIncreasedFareEvent(bindOrderData, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new increased fare data",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.ActionOnOrderKey:
				var actionOnOrder structures.ActionOnOrder
				err := json.Unmarshal([]byte(d.Body), &actionOnOrder)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling order events",
						"reason": "error binding new action on order",
					}).Error(err)
					continue
				}
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "action on order event",
					"action":    actionOnOrder.Action,
					"orderUUID": actionOnOrder.OrderUUID,
				}).Info("new action on order")

			default:
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling order events",
					"reason": "routing key identification",
				}).Warnln(errpath.Errorf("unknown routing key %s", d.RoutingKey))
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "handling order events",
				"reason": "new event by order",
				"key":    d.RoutingKey,
			}).Info("event creation was succesfull")
		}
	}
}

func handleTicketsEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			publishHead := d.Headers["publisher"]
			publisher, ok := publishHead.(string)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":       "handling ticket events",
				"routing key": d.RoutingKey,
				"publisher":   publisher,
			})
			if !ok {
				log.WithFields(logrus.Fields{
					"reason": "error parse publisher header",
				}).Error(publishHead)
				continue
			}
			var tick structures.Ticket
			err := json.Unmarshal([]byte(d.Body), &tick)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error binding data",
				}).Error(err)
				continue
			}
			err = newTicketEvents(tick, publisher, d.RoutingKey)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error creating event",
				}).Error(err)
				continue
			}

			log.WithFields(logrus.Fields{
				"reason": "new event",
			}).Info("event creation was succesfull")
		}
	}
}
func handleOperatorEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			publishHead := d.Headers["publisher"]
			publisher, ok := publishHead.(string)
			if !ok {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling operator events",
					"reason": "error parse publisher header",
				}).Error(publishHead)
				continue
			}

			switch {
			case d.RoutingKey == rabbit.NewKey:
				var operator structures.WrapperOperator
				err := json.Unmarshal([]byte(d.Body), &operator)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error binding new operator event data",
					}).Error(err)
					continue
				}
				err = newOperatorEvent(operator, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error creating event for new operator",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.UpdateKey:
				var operator structures.WrapperOperator
				err := json.Unmarshal([]byte(d.Body), &operator)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error binding new operator data",
					}).Error(err)
					continue
				}
				err = newOperatorUpdateEvent(operator, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error creating event for new operator data",
					}).Error(err)
					continue
				}
			case d.RoutingKey == rabbit.LoggedIn:
				var operator structures.WrapperOperator

				err := json.Unmarshal([]byte(d.Body), &operator)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error binding operator",
					}).Error(err)
					continue
				}
				err = newOperatorLoggedInEvent(operator, publisher)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling operator events",
						"reason": "error creating event for new operator",
					}).Error(err)
					continue
				}
			default:
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling operator events",
					"reason": "routing key identification",
				}).Debug("unknown routing key")
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "handling operator events",
				"reason": "new event by operator",
			}).Info("event creation was succesfull")
		}
	}
}

func initTelephonyEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.EventsTelephonyQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.EventsTelephonyQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments

	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,              // queue name
		"#",                 // routing key
		rabbit.VoipExchange, // exchange  events.voip
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                         // queue
		rabbit.EventsTelephonyConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleTelephonyEvents(msgs)
	return nil
}
func handleTelephonyEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			log := logrus.WithFields(logrus.Fields{
				"event": "handling telephony events",
			})
			publishHead := d.Headers["publisher"]
			publisher, ok := publishHead.(string)
			if !ok {
				log.WithFields(logrus.Fields{
					"reason": "error parse publisher header",
				}).Error(publishHead)
				//continue
			}

			var call Telephony
			if err := json.Unmarshal([]byte(d.Body), &call); err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error unmarshal call",
				}).Error(err)
				continue
			}

			//заполняем адрес облачного хранилища
			if call.Record != "" {
				call.Record = config.St.TelephonyRecordStorage.Host + call.Record
			}

			// проверяем является ли входящий звонок автоколом, что бы уведомить водителя и оператора
			if call.Uid != "" && call.Status == structures.VOIPCallSatusANSWER {
				go func() {
					err := orders.MarkReportedField(call.OrderUUID, call.Type)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason":    "error MarkReportedField",
							"orderUUID": call.OrderUUID,
						}).Error(err)
					}
				}()
			}

			cl, err := getClientFromBD(call.CallerID)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error getClientFromBD",
				}).Error(err)
			}

			var event Events
			event.Publisher = publisher
			event.EventTime = time.Now()
			event.Event = constants.EventTelephony
			event.EventTrans = constants.EventRU[constants.EventTelephony]
			event.Value = call.Status
			event.Payload = call

			event.ClientUUID = append(event.ClientUUID, cl.UUID)

			err = event.Save()
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error creating telephony event - Hangup",
				}).Error(err)
				continue
			}
		}
	}
}

func handleBillingEvents(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			log := logs.Eloger.WithField("event", "handling billing events")
			switch {
			case d.RoutingKey == rabbit.CompletedKey:
				var completedTransfer structures.CompletedTransfer
				if err := json.Unmarshal(d.Body, &completedTransfer); err != nil {
					log.WithField("reason", "error binding completed transfer").Error(err)
					continue
				}
				if err := newBillingEvent(completedTransfer, "billing"); err != nil {
					log.WithField("reason", "error creating completed transfer event").Error(err)
					continue
				}
			default:
				log.WithField("reason", "routing key identification").Debug("unknown routing key")
				continue
			}
			log.WithField("reason", "new billing event").Info("event creation was successful")
		}
	}
}

func (ev Events) Save() error {
	if len(ev.DriverUUID) != 0 {
		driverLocations := models.CurrentDriverLocationsByUUIDs(ev.DriverUUID)
		drvLocations := make([]models.LocationDataCRM, 0, len(driverLocations))
		for _, drvLoc := range driverLocations {
			drvLocations = append(drvLocations, drvLoc)
		}
		ev.DriverCoordinates = drvLocations
	}

	ev.EventTimeUnix = time.Now().Unix()
	_, err := db.Model(&ev).Insert()
	if err != nil {
		return err
	}

	var eventItem models.EventItem
	eventItem.EventTime = ev.EventTime
	eventItem.TicketUUID = ev.TicketUUID
	eventItem.MessagesUUID = ev.MessagesUUID
	eventItem.EventTimeUnix = ev.EventTimeUnix
	eventItem.DriverUUID = ev.DriverUUID
	eventItem.OrderUUID = ev.OrderUUID
	eventItem.ClientUUID = ev.ClientUUID
	eventItem.OperatorUUID = ev.OperatorUUID
	eventItem.Publisher = ev.Publisher
	eventItem.Event = ev.Event
	eventItem.EventTrans = ev.EventTrans
	eventItem.Reason = ev.Reason
	eventItem.Value = ev.Value
	eventItem.Payload = ev.Payload
	eventItem.Comment = ev.Comment
	eventItem.ValueTitle = ev.ValueTitle
	eventItem.DriverCoordinates = ev.DriverCoordinates

	models.PublishEvent(eventItem)

	return nil
}

func newOrderEvent(order structures.WrapperOrder, publisher string) error {
	var (
		event Events
	)
	event.Payload = order.Tariff.Surge
	event.Publisher = publisher
	event.ClientUUID = []string{order.Client.UUID}
	event.EventTime = time.Now()
	event.OrderUUID = []string{order.UUID}
	event.OperatorUUID = []string{order.OperatorUUID}
	event.Event = constants.EventNewOrder
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новый заказ из " + publisher
	err := event.Save()
	return err
}

func newReDistributionEvent(data structures.OrderReDistributionData, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.EventTime = time.Now()
	event.OrderUUID = []string{data.OrderUUID}
	event.OperatorUUID = []string{data.OperatorUUID}
	event.DriverUUID = []string{data.CurrentDriverUUID}
	event.Event = constants.EventReDistribution
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Запрос на перераспределение из " + publisher
	err := event.Save()
	return err
}
func newOrderUnmarkEvent(data structures.UnmarkOrderAsImportant, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.EventTime = time.Now()
	event.OrderUUID = []string{data.OrderUUID}
	event.OperatorUUID = []string{data.OperatorUUID}
	event.Event = constants.EventUnmarkOrder
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Запрос на снятие отметки важности с заказа из " + publisher
	err := event.Save()
	return err
}
func newOrderStateEvent(orderState structures.OfferStates, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher

	if orderState.DriverUUID != "" {
		event.DriverUUID = append(event.DriverUUID, orderState.DriverUUID)
	}
	event.EventTime = time.Now()
	event.OrderUUID = []string{orderState.OrderUUID}
	event.OperatorUUID = []string{orderState.OperatorUUID}
	event.Event = constants.EventOrderState
	event.EventTrans = constants.EventRU[event.Event]
	event.Value = orderState.State
	event.Comment = orderState.Comment
	if event.Value == constants.OrderStateOffered {
		switch {
		case orderState.Flags.Has(structures.OfferStateFlagFreeOrder):
			event.Comment = "Заказ взят из списка свободных"
		case orderState.Flags.Has(structures.OfferStateFlagCounterOrder):
			event.Comment = "Встречный заказ"
		}
	}
	if event.Value == constants.OrderStateCancelled && orderState.Comment != "" {
		event.Comment = fmt.Sprintf("Указанная причина отмены: '%s'", constants.CancelReasonRU[orderState.Comment])
	}
	event.Reason = "Новый статус заказа из " + publisher
	err := event.Save()
	return err
}
func newOrderUpdataEvent(order structures.WrapperOrder, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.ClientUUID = []string{order.Client.UUID}
	event.EventTime = time.Now()
	event.OrderUUID = []string{order.UUID}
	event.OperatorUUID = []string{order.OperatorUUID}
	event.Event = constants.EventOrderUpdate
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новые данные заказа из " + publisher
	err := event.Save()
	return err
}
func newOrderIncreasedFareEvent(order structures.WrapperOrder, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.ClientUUID = []string{order.Client.UUID}
	event.EventTime = time.Now()
	event.OrderUUID = []string{order.UUID}
	event.OperatorUUID = []string{order.OperatorUUID}
	event.Event = constants.EventIncreasedFare
	event.EventTrans = constants.EventRU[event.Event]
	event.Value = fmt.Sprintf("%v:%v", (order.IncreasedFare - order.FareDifferences), order.IncreasedFare)
	pref := "Клиент увеличил надбавочную стоимость на %v рублей. %s"
	if order.FareDifferences < 0 {
		pref = "Клиент уменьшил надбавочную стоимость на %v рублей. %s"
	}
	post := fmt.Sprintf("C %v до %v", (order.IncreasedFare - order.FareDifferences), order.IncreasedFare)
	event.Reason = fmt.Sprintf(pref, math.Abs(float64(order.FareDifferences)), post)
	err := event.Save()
	return err
}
func newDriverUpdateEvent(driver structures.WrapperDriver, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.EventTime = time.Now()
	event.DriverUUID = []string{driver.UUID}
	event.OperatorUUID = []string{driver.OperatorUUID}
	event.Event = constants.EventDriverUpdate
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новые данные водителя из " + publisher
	err := event.Save()
	return err
}

func newDriverEvent(driver structures.Driver, publisher string) error {
	var (
		event Events
	)
	event.Publisher = publisher
	event.DriverUUID = []string{driver.UUID}
	event.EventTime = time.Now()
	event.Event = constants.EventNewDriver
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новый водитель из " + publisher
	err := event.Save()
	return err
}

func newDriverStateEvent(driverState structures.DriverStates, publisher string) error {
	var (
		event Events
	)

	event.Publisher = publisher
	if driverState.DriverUUID != "" {
		event.DriverUUID = append(event.DriverUUID, driverState.DriverUUID)
	}
	event.Value = driverState.State
	event.EventTime = time.Now()
	event.Comment = driverState.Comment
	event.Event = constants.EventDriverState
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новый статус водителя из " + publisher
	err := event.Save()
	return err
}

func newDriverActivityChangedEvent(activityHistoryItem structures.ActivityHistoryItem, publisher string) error {
	var event Events
	event.Publisher = publisher
	event.DriverUUID = []string{activityHistoryItem.UserUUID}
	event.OrderUUID = []string{activityHistoryItem.OrderUUID}
	event.EventTime = time.Now()
	event.Event = constants.EventActivityChanged
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = fmt.Sprintf("Изменение активности по причине: %s", activityHistoryItem.Event)
	event.Value = fmt.Sprintf("Изменение: %d", activityHistoryItem.ActivityChange)
	event.ValueTitle = event.Value
	event.Comment = fmt.Sprintf(
		"Текущее значение активности: %d, изменено на: %d", activityHistoryItem.Activity, activityHistoryItem.ActivityChange,
	)
	err := event.Save()
	return err
}

func newTicketEvents(tick structures.Ticket, publisher string, routingKey string) error {
	var (
		event            Events
		neededSourceType structures.ChatMembers
		source           string
	)
	neededSourceType = tick.SourceType
	if routingKey == rabbit.StateKey && len(tick.Comments) > 0 {
		neededSourceType = tick.Comments[len(tick.Comments)-1].SenderType
	}
	event.Publisher = publisher
	switch neededSourceType {
	case structures.ClientMember:
		source = "Клиент"
	case structures.TelegramBotMember:
		source = "Бот"
	case structures.DriverMember:
		source = "Водитель"
	case structures.UserCRMMember:
		source = "Оператор"
	}
	event.ClientUUID = []string{tick.ClientData.UUID}
	event.DriverUUID = []string{tick.DriverData.UUID}
	event.OperatorUUID = []string{tick.OperatorData.UUID}
	event.EventTime = time.Now()
	event.EventTimeUnix = event.EventTime.Unix()
	if routingKey == rabbit.NewKey {
		event.Event = constants.EventNewTicket
		event.Reason = source + " создал новый тикет"
		event.Value = string(tick.Status)
	}
	if routingKey == rabbit.StateKey {
		event.Event = constants.EventTicketNewStatus
		event.Reason = fmt.Sprintf("%s поменял стутус тикета", source)
		event.Value = string(tick.Status)
		if tick.Status == structures.ResolvedStatus || tick.Status == structures.NotResolvedStatus {
			neededPrefix := ""
			if tick.Status == structures.NotResolvedStatus {
				neededPrefix = "не"
			}
			switch len(tick.Comments) == 0 {
			case false:
				// take the message from the last comment
				event.Reason = fmt.Sprintf("%s отметил тикет как %sрешенный с комментарием '%s'", source, neededPrefix, tick.Comments[len(tick.Comments)-1].Message)
			case true:
				event.Reason = fmt.Sprintf("%s отметил тикет как %sрешенный", source, neededPrefix)
			}
		}
	}
	event.Publisher = publisher
	event.TicketUUID = tick.UUID
	event.EventTrans = constants.EventRU[event.Event]

	err := event.Save()
	return err
}

func newOperatorEvent(operator structures.WrapperOperator, publisher string) error {
	var event Events
	event.Publisher = publisher
	if operator.UUID == "" {
		event.OperatorUUID = []string{operator.OperatorUUID}
	} else {
		event.OperatorUUID = []string{operator.OperatorUUID, operator.UUID}
	}
	event.EventTime = time.Now()
	event.Event = constants.EventNewOperator
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новый оператор из " + publisher
	err := event.Save()
	return err
}

func newOperatorUpdateEvent(operator structures.WrapperOperator, publisher string) error {
	var event Events
	event.Publisher = publisher
	if operator.UUID == "" {
		event.OperatorUUID = []string{operator.OperatorUUID}
	} else {
		event.OperatorUUID = []string{operator.OperatorUUID, operator.UUID}
	}
	event.EventTime = time.Now()
	event.Event = constants.EventOperatorUpdate
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новые данные оператора из " + publisher
	err := event.Save()
	return err
}

func newOperatorLoggedInEvent(operator structures.WrapperOperator, publisher string) error {
	var event Events
	event.Publisher = publisher
	if operator.UUID == "" {
		event.OperatorUUID = []string{operator.OperatorUUID}
	} else {
		event.OperatorUUID = []string{operator.OperatorUUID, operator.UUID}
	}
	event.EventTime = time.Now()
	event.Event = constants.EventOperatorLogged
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Вход оператора в систему"
	err := event.Save()
	return err
}

func newMessagesMarkedAsReadEvent(messData structures.ChatMessagesMarkedAsRead, operatorUUID string) error {
	var event Events
	event.Publisher = "chat"
	if operatorUUID == "" {
		return errors.Errorf("empty operator uuid")
	}
	if len(messData.MessagesUUID) == 0 {
		return errors.Errorf("empty messages_uuid")
	}
	for _, uuid := range messData.MessagesUUID {
		event.MessagesUUID = append(event.MessagesUUID, uuid)
	}

	event.OrderUUID = append(event.OrderUUID, messData.OrderUUID)
	event.OperatorUUID = []string{operatorUUID}
	event.EventTime = time.Now()
	event.Event = constants.EventMessReadMany
	if len(messData.MessagesUUID) == 1 {
		event.Event = constants.EventMessReadOne
	}
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новая информация о сообщениях чата"
	err := event.Save()
	return err
}

func newOrderDeliveredToDriverEvent(messData structures.OrderDeliveredMessage) error {
	var event Events
	event.Publisher = "crm"
	event.DriverUUID = []string{messData.DriverUUID}
	event.OrderUUID = []string{messData.OrderUUID}
	event.EventTime = time.Now()
	event.Event = constants.EventOrderDelivered
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Запрос на подтверждение доставки заказа"
	err := event.Save()
	return err
}
func newChatMessagesFromOperatorToDriver(messData structures.ChatMessages, operatorUUID string) error {
	var event Events
	event.Publisher = "chat"
	if operatorUUID == "" {
		return errors.Errorf("empty operator uuid")
	}
	event.DriverUUID = []string{messData.DriverUUID}
	event.OrderUUID = []string{messData.OrderUUID}
	event.MessagesUUID = []string{messData.UUID}
	event.OperatorUUID = []string{operatorUUID}
	event.EventTime = time.Now()
	event.Event = constants.EventMessFromOperatorToDriver
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новая информация о сообщениях чата"
	err := event.Save()
	return err
}
func newDistributionEvent(disData []structures.DistributionData, publisher string) error {
	var (
		event Events
		check bool
	)

	//чтобы два раза один и тот же OrderUUID не записывать
	for _, val := range disData {
		check = false
		for _, orderUUID := range event.OrderUUID {
			if orderUUID == val.ScoreTable.OrderUUID {
				check = true
			}
		}
		if check {
			continue
		}

		event.OrderUUID = append(event.OrderUUID, val.ScoreTable.OrderUUID)
	}

	for _, val := range disData {
		check = false
		if val.ScoreTable.DriverUUID == "" {
			check = true
		}
		for _, driverUUID := range event.DriverUUID {
			if driverUUID == val.ScoreTable.DriverUUID {
				check = true
			}
		}
		if check {
			continue
		}
		event.DriverUUID = append(event.DriverUUID, val.ScoreTable.DriverUUID)
	}
	event.Payload = disData
	event.Event = constants.EventDistribution
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новые данные распределения из " + publisher
	if len(event.DriverUUID) == 0 {
		event.Reason = "Не найдены свободные водители для распределения"
	}
	err := event.Save()
	return err
}

func newDistributionEvent2(data distribution.Match, publisher string) error {
	var event Events

	event.OrderUUID = []string{data.OrderUUID}
	event.DriverUUID = []string{data.DriverUUID}
	event.Payload = data.Meta
	event.Event = constants.EventDistribution
	event.EventTrans = constants.EventRU[event.Event]
	event.Reason = "Новые данные распределения из " + publisher
	if len(event.DriverUUID) == 0 {
		event.Reason = "Не найдены свободные водители для распределения"
	}
	err := event.Save()
	return err
}

func newBillingEvent(transfer structures.CompletedTransfer, publisher string) error {
	var event Events
	event.Publisher = publisher
	event.EventTime = time.Now()
	event.Event = constants.EventNewTransfer
	event.EventTrans = constants.EventRU[event.Event]
	event.Value = fmt.Sprintf("%f", transfer.Amount)
	event.Comment = transfer.Description
	event.Payload = transfer.Meta
	if transfer.Meta.OrderUUID != "" {
		event.OrderUUID = []string{transfer.Meta.OrderUUID}
	}

	// Identify transfer type
	switch transfer.TransferType {
	case structures.TransferTypeForceSet:
		event.Reason = "Установка баланса"
		if transfer.PayeeType == structures.UserTypeDriver {
			event.DriverUUID = []string{transfer.PayeeUUID}
			event.Reason += " водителя"
		} else if transfer.PayeeType == structures.UserTypeClient {
			event.Reason += " клиента"
		} else if transfer.PayerType == structures.UserTypeTaxiPark {
			event.Reason += " таксопарка"
		}
	case structures.TransferTypeTopUp:
		event.Reason = "Пополнение баланса"
		if transfer.PayeeType == structures.UserTypeDriver {
			event.DriverUUID = []string{transfer.PayeeUUID}
			event.Reason += " водителя"
		} else if transfer.PayeeType == structures.UserTypeClient {
			event.Reason += " клиента"
		} else if transfer.PayerType == structures.UserTypeTaxiPark {
			event.Reason += " таксопарка"
		}
	case structures.TransferTypeWithdraw:
		event.Reason = "Списание со счета"
		if transfer.PayerType == structures.UserTypeDriver {
			event.DriverUUID = []string{transfer.PayerUUID}
			event.Reason += " водителя"
		} else if transfer.PayerType == structures.UserTypeClient {
			event.Reason += " клиента"
		} else if transfer.PayerType == structures.UserTypeTaxiPark {
			event.Reason += " таксопарка"
		}
	case structures.TransferTypePayment:
		event.Reason = "Перевод для"
		if transfer.PayeeType == structures.UserTypeDriver {
			event.DriverUUID = []string{transfer.PayeeUUID}
			event.Reason += " водителя"
		} else if transfer.PayeeType == structures.UserTypeClient {
			event.Reason += " клиента"
		} else if transfer.PayeeType == structures.UserTypeTaxiPark {
			event.Reason += " таксопарка"
		}
		event.Reason += " от"
		if transfer.PayerType == structures.UserTypeDriver {
			event.DriverUUID = []string{transfer.PayerUUID}
			event.Reason += " водителя"
		} else if transfer.PayerType == structures.UserTypeClient {
			event.Reason += " клиента"
		} else if transfer.PayerType == structures.UserTypeTaxiPark {
			event.Reason += " таксопарка"
		}
	}
	event.Reason += fmt.Sprintf(" на сумму %.2f ₽", transfer.Amount)
	if transfer.PayerAccountType == structures.AccountTypeCash {
		event.Reason += " наличными"
	} else if transfer.PayerAccountType == structures.AccountTypeCard {
		event.Reason += " безналичными"
	} else if transfer.PayerAccountType == structures.AccountTypeBonus {
		event.Reason += " бонусами"
	}

	return event.Save()
}

// GetEventsByOptions returns events by some options
func GetEventsByOptions(options EventsCriteria) ([]Events, int, error) {
	var (
		events []Events
	)
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	query := db.ModelContext(ctx, &events).
		Order("event_time DESC")
	if !options.MaxDate.IsZero() {
		query = query.
			Where("event_time < ?", options.MaxDate)
	}
	if !options.MinDate.IsZero() {
		query = query.
			Where("event_time > ? ", options.MinDate)
	}
	if options.DriverUUID != "" {
		query = query.
			Where("driver_uuid @> ARRAY[?]", options.DriverUUID)
	}
	if options.StoreUUID != "" {
		query = query.
			WhereIn("value in (?)", valuesForStoreUser)
	}
	if options.ClientUUID != "" {
		query = query.
			Where("client_uuid @> ARRAY[?]", options.ClientUUID)
	}
	if options.OrderUUID != "" {
		query = query.
			Where("order_uuid @> ARRAY[?]", options.OrderUUID)
	}
	//TODO: надо это оптимизировать
	if options.OperatorUUID != "" {
		query = query.
			Where("operator_uuid @> ARRAY[?]", options.OperatorUUID)
	}
	if options.Exten != "" {
		query = query.
			Where("payload ->> 'operator' = ?", options.Exten)
	}
	if options.TicketUUID != "" {
		query = query.
			Where("ticket_uuid = ?", options.TicketUUID)
	}
	// skipCountQuery костыль для того чтобы не делать запрос с count-м, ибо он получается долгий, а индексы не помогают че то
	skipCountQuery := false
	if options.TelephonyCallerNumber != "" {
		if options.TelephonyCallerNumber == EventsParamAllClientsPhones {
			//чтобы вернуть все записи, в которых есть хоть какой то callerid
			skipCountQuery = true
			query = query.Where("payload -> 'caller_id' IS NOT NULL")
		} else {
			query = query.Where("payload ->> 'caller_id' ILIKE ?", "%"+options.TelephonyCallerNumber+"%")
		}
	}
	var count int
	var err error
	//временно убрали каунты
	skipCountQuery = true
	if skipCountQuery {
		count = 10000
	} else {
		count, err = query.Count()
		if err != nil {
			return events, count, errors.Errorf("query c error,%s", err)
		}
	}
	err = query.Apply(options.Pager.Pagination).Select()
	if err != nil {
		return events, 0, errors.Errorf("query error,%s", err)
	}
	events, err = FillData(events, options.StoreUUID != "")
	if err != nil {
		return events, 0, errors.Errorf("filling data error,%s", err)
	}
	return events, count, nil
}

func FillData(events []Events, forStoreUser bool) ([]Events, error) {

	events, err := fillDriverData(events)
	if err != nil {
		return events, err
	}
	events, err = fillOrderData(events, forStoreUser)
	if err != nil {
		return events, err
	}
	events, err = fillOperatorData(events)
	if err != nil {
		return events, err
	}
	return events, nil
}

func fillOperatorData(events []Events) ([]Events, error) {
	var (
		operators       []models.UsersCRM
		allOperatorUUID []string
	)
	for _, event := range events {
		allOperatorUUID = append(allOperatorUUID, event.OperatorUUID...)
	}

	if len(allOperatorUUID) == 0 {
		return events, nil
	}
	err := db.Model(&operators).Where("uuid in (?)", pg.In(allOperatorUUID)).Select()
	if err != nil {
		return events, errors.Errorf("error finding operators,%s", err)
	}
	allOperatorNames := make(map[string]string)
	for _, operator := range operators {
		allOperatorNames[operator.UUID] = operator.Name
	}

	for j, event := range events {
		if len(event.OperatorUUID) == 0 {
			continue
		}

		for i, uuid := range event.OperatorUUID {
			oprData := operatorData{}
			oprData.UUID = uuid
			oprData.Name = allOperatorNames[uuid]
			if i == 0 {
				oprData.Initiator = true
			} else {
				oprData.Initiator = false
			}
			events[j].OperatorData = append(events[j].OperatorData, oprData)
		}

	}
	return events, nil
}

func fillDriverData(events []Events) ([]Events, error) {
	var (
		drivers       []models.DriverCRM
		allDriverUUID []string
	)
	for _, event := range events {
		allDriverUUID = append(allDriverUUID, event.DriverUUID...)
	}
	if len(allDriverUUID) == 0 {
		return events, nil
	}
	err := db.Model(&drivers).Where("uuid in (?)", pg.In(allDriverUUID)).Select()
	if err != nil {
		return events, errors.Errorf("error finding drivers,%s", err)
	}
	allDriverAlias := make(map[string]int)
	for _, driver := range drivers {
		allDriverAlias[driver.UUID] = driver.Alias
	}

	for j, event := range events {
		if len(event.DriverUUID) == 0 {
			continue
		}
		if event.Event == constants.EventDriverState {
			valueTitle := constants.DriverStateRU[event.Value]
			events[j].ValueTitle = valueTitle
		}
		for _, uuid := range event.DriverUUID {
			drvData := driverData{}
			drvData.Alias = allDriverAlias[uuid]
			drvData.UUID = uuid
			events[j].DriverData = append(events[j].DriverData, drvData)
		}
	}

	return events, nil
}

func fillOrderData(events []Events, forStoreUser bool) ([]Events, error) {
	var (
		orders       []orders.OrderCRM
		allOrderUUID []string
	)
	allOrderIDs := make(map[string]int)
	for _, event := range events {
		allOrderUUID = append(allOrderUUID, event.OrderUUID...)
	}
	if len(allOrderUUID) == 0 {
		return events, nil
	}
	err := db.Model(&orders).Where("uuid in (?)", pg.In(allOrderUUID)).Select()
	if err != nil {
		return events, errors.Errorf("error finding orders,%s", err)
	}
	for _, order := range orders {
		allOrderIDs[order.UUID] = order.ID
	}

	for j, event := range events {
		if len(event.OrderUUID) == 0 {
			continue
		}

		if event.Event == constants.EventOrderState {
			stateRus, exists := orderStatesTransForStoreUser[event.Value]
			if forStoreUser && exists {
				events[j].ValueTitle = stateRus
			} else {
				events[j].ValueTitle = constants.OrderStateRU[event.Value]
			}
		}
		for _, uuid := range event.OrderUUID {
			ordData := orderData{}
			ordData.ID = allOrderIDs[uuid]
			ordData.UUID = uuid
			events[j].OrderData = append(events[j].OrderData, ordData)
		}
	}
	return events, nil
}

// getClientFromBD - возвращает метку существования записи и соответствие по номеру
func getClientFromBD(phone string) (models.ClientCRM, error) {
	var client models.ClientCRM
	count, err := db.Model(&client).Where("main_phone = ?", phone).SelectAndCount()
	if count == 0 {
		return models.ClientCRM{}, nil
	}
	if count > 1 {
		return models.ClientCRM{}, errors.Errorf("Множественные записи")
	}
	if err != nil {
		return models.ClientCRM{}, err
	}
	return client, nil
}

//func fillWhenIncoming(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyIncoming
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
//func fillWhenRinging(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyIRinging
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
//func fillWhenStartcall(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyStartCall
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
//func fillWhenHangup(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyHangup
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
//func fillWhenAnswer(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyAnswer
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
//func fillWhenPlayend(call Telephony, publisher string, client models.ClientCRM) error {
//	var event Events
//
//	event.Publisher = publisher
//	event.EventTime = time.Now()
//	event.Event = variables.EventTelephonyPlayEnd
//	event.EventTrans = variables.EventsRus[event.Event]
//	event.Value = call.Status
//
//	event.Payload = call
//
//	if client.UUID != "" {
//		event.ClientUUID = append(event.ClientUUID, client.UUID)
//	}
//
//	err := event.Save()
//	return err
//}
