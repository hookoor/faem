package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabNewClient данные для получения клиентов из брокера
func initRabNewClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmNewClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmNewClientQueue, // name
		true,                     // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения клиентов
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.NewKey,         // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                      // queue
		rabbit.CrmNewClientConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewClient(msgs)
	return nil
}

func handleNewClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var (
				client    structures.Client
				clientCRM models.ClientCRM
			)
			err := json.Unmarshal([]byte(d.Body), &client)
			if err != nil {

				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new client",
					"reason": "Error unmarshalling client",
				}).Error(err)

				continue
			}

			clientCRM.Client = client
			clientCRM.CreatedAt = time.Now()

			// Сохраняем клиента в местную базу
			err = clientCRM.SaveClientAPP()
			if err != nil {

				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling new client",
					"reason":     "Error saving new client",
					"clientUUID": client.UUID,
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling new client",
				"clientUUID": client.UUID,
			}).Info("CRM created new client")
		}
	}
}

// -----------------
// -----------------
// -----------------

// initRabUpdateClient -
func initRabUpdateClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmUpdateClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmUpdateClientQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения клиентов
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                         // queue
		rabbit.CrmUpdateClientConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}
	wg.Add(1)
	go handleUpdateClient(msgs)
	return nil
}

func handleUpdateClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:

			if d.Headers["publisher"] == "crm" {
				continue
			}
			var (
				client    structures.Client
				clientCRM models.ClientCRM
			)

			log := logs.Eloger.WithField("event", "handling update client")

			err := json.Unmarshal([]byte(d.Body), &client)
			if err != nil {
				log.WithField("reason", "Error unmarshalling client").Error(errpath.Err(err))
				continue
			}
			log.WithField("clientUUID", client.UUID)

			clientCRM.Client = client
			clientCRM.UpdatedAt = time.Now()

			_, err = clientCRM.Update(client.UUID)
			if err != nil {
				log.WithField("reason", "Error update client").Error(errpath.Err(err))
				continue
			}

			log.Info("CRM update client")
		}
	}
}
