package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

func initNewOrderFromBot() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmNewOrderFromBotQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmNewOrderFromBotQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		rabbit.NewChatOrder,  // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannel.Consume(
		q.Name,                            // queue
		rabbit.CrmNewOrderFromBotConsumer, // consumer
		true,                              // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewOrderFromBot(msgs)
	return nil
}

func handleNewOrderFromBot(msgs <-chan amqp.Delivery) {
	lo := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order from bot",
	})
	defer wg.Done()
	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var newOrder structures.Order
			err := json.Unmarshal(d.Body, &newOrder)
			if err != nil {
				lo.WithField("reason", "Error unmarshalling order").Error(err)
				continue
			}

			crmOrder := prepareOrder(&newOrder)

			err = orders.SaveOrderFromChat(&crmOrder)
			if err != nil {
				lo.WithField("reason", "Error saving order from chat").Error(err)
				continue
			}
			lo.WithField("orderUUID", crmOrder.UUID).Info("Order Created")
		}
	}
}

func prepareOrder(ord *structures.Order) orders.OrderCRM {
	var ro orders.OrderCRM
	ro.UUID = ord.UUID
	ro.Comment = ord.Comment
	ro.Client = ord.Client
	ro.Source = ord.Source
	ro.CallbackPhone = ord.CallbackPhone
	ro.ClUUID = ord.Client.UUID
	ro.OrderState = string(proto.Consts.Order.CreationStates.StartChatting) //variables.OrderStateChatStart
	route1 := structures.Route{
		UnrestrictedValue: "Адрес не выбран",
		Value:             "Адрес не выбран",
		City:              "Владикавказ",
		Comment:           "такого адреса не существует",
		OutOfTown:         false,
		Lat:               43.03601,
		Lon:               44.654392,
	}
	ro.Routes = append([]structures.Route{}, route1, route1)
	return ro
}
