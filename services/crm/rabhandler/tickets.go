package rabhandler

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

func initTicketsHandler() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmTicketsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.TicketsExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmTicketsQueue, // name
		true,                   // durable
		false,                  // delete when unused
		false,                  // exclusive
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                 // queue name
		"#",                    // routing key
		rabbit.TicketsExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                    // queue
		rabbit.CrmTicketsConsumer, // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleTicketsEvents(msgs)
	return nil
}

func handleTicketsData(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			switch d.RoutingKey {
			case rabbit.NewKey:
				handleNewTicketsData(d)
			case rabbit.StateKey:
				handleTicketStatesData(d)
			}
		}
	}
}

func handleNewTicketsData(d amqp.Delivery) {
	var tick structures.Ticket
	err := json.Unmarshal([]byte(d.Body), &tick)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling new tickets data",
		"ticketUUID": tick.UUID,
		"orderUUID":  tick.OrderData.UUID,
		"routingKey": d.RoutingKey,
	})
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error binding data"}).Error(err)
		return
	}
	if tick.OrderData.UUID == "" {
		log.Debug("order uuid is empty")
		return
	}
	err = orders.AddImportantReasonToOrder(orders.UnResolvedTicket, tick.OrderData.UUID)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error adding important reason to order"}).Error(err)
		return
	}
	log.Info("ticket successfully received")
}

func handleTicketStatesData(d amqp.Delivery) {
	var tick structures.Ticket
	err := json.Unmarshal([]byte(d.Body), &tick)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling new ticket state data",
		"ticketUUID": tick.UUID,
		"status":     tick.Status,
		"orderUUID":  tick.OrderData.UUID,
		"routingKey": d.RoutingKey,
	})
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error binding data"}).Error(err)
		return
	}
	if tick.OrderData.UUID == "" || tick.Status != structures.ResolvedStatus {
		log.Debug("order uuid is empty or state is not resolver")
		return
	}
	err = orders.RemoveImportantReasonFromOrder(tick.OrderData.UUID, orders.UnResolvedTicket)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error remove important reason from order"}).Error(err)
		return
	}
	log.Info("ticket state successfully received")
}
