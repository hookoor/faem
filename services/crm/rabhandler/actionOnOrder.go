package rabhandler

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabNewDriver данные для получения водителей из брокера
func initRabActionOnOrder() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmNewDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmActionOnOrderQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения водителей
	err = receiverChannel.QueueBind(
		q.Name,                  // queue name
		rabbit.ActionOnOrderKey, // routing key
		rabbit.OrderExchange,    // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                          // queue
		rabbit.CrmActionOnOrderConsumer, // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleActionOnOrder(msgs)
	return nil
}

func handleActionOnOrder(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	logg := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling action on order",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var action structures.ActionOnOrder

			err := json.Unmarshal([]byte(d.Body), &action)
			if err != nil {
				logg.WithField("reason", "Error unmarshalling driver").Errorln(errpath.Err(err))
				continue
			}

			switch action.Action {
			case structures.ActionOnOrderCancelOrder:

				ord, err := orders.CancelOrder(constants.CancelReasonClient, action.OrderUUID)
				if err != nil {
					logg.Errorln(errpath.Err(err))
					continue
				}

				err = rabsender.SendJSONByAction(rabsender.AcOrderState, ord)
				if err != nil {
					res := fmt.Sprintf("Error sending canceled order to broker. %s", err)
					logs.OutputError(res)
				}
				logg.WithField("orederUUID", action.OrderUUID).Info("CRM order canceled")

			case structures.ActionOnOrderSetOrderImportant:
				err = orders.AddImportantReasonToOrder(orders.ChatBotCustomerAssistance, action.OrderUUID)
				if err != nil {
					logg.Errorln(errpath.Err(err))
					continue
				}
				logg.WithField("orederUUID", action.OrderUUID).Info("CRM set order important")

			default:
				logg.Warnln(errpath.Errorf("action on order not found"))
			}
		}
	}
}
