package rabhandler

import (
	"encoding/json"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

const (
	maxKarmaChangesAllowed     = 5
	maxBlacklistChangesAllowed = 5
)

func initRabKarmaChanged() error {
	if err := initDriverKarmaChanged(); err != nil {
		return errors.Wrap(err, "failed to init driver karma changing listener")
	}
	if err := initClientKarmaChanged(); err != nil {
		return errors.Wrap(err, "failed to init client karma changing listener")
	}
	return nil
}

func initDriverKarmaChanged() error {
	driverChannel, err := rb.GetReceiver(rabbit.CrmDriverKarmaChangedQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = driverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := driverChannel.QueueDeclare(
		rabbit.CrmDriverKarmaChangedQueue, // name
		true,                              // durable
		false,                             // delete when unused
		false,                             // exclusive
		false,                             // no-wait
		nil,                               // arguments
	)
	if err != nil {
		return err
	}

	err = driverChannel.QueueBind(
		q.Name,                 // queue name
		rabbit.KarmaChangedKey, // routing key
		rabbit.DriverExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := driverChannel.Consume(
		q.Name,                               // queue
		rabbit.CrmDriverKarmaChangedConsumer, // consumer
		true,                                 // auto-ack
		false,                                // exclusive
		false,                                // no-local
		false,                                // no-wait
		nil,                                  // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverKarmaChanged(msgs)
	return nil
}

func initClientKarmaChanged() error {
	clientChannel, err := rb.GetReceiver(rabbit.CrmClientKarmaChangedQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = clientChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := clientChannel.QueueDeclare(
		rabbit.CrmClientKarmaChangedQueue, // name
		true,                              // durable
		false,                             // delete when unused
		false,                             // exclusive
		false,                             // no-wait
		nil,                               // arguments
	)
	if err != nil {
		return err
	}

	err = clientChannel.QueueBind(
		q.Name,                 // queue name
		rabbit.KarmaChangedKey, // routing key
		rabbit.ClientExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := clientChannel.Consume(
		q.Name,                               // queue
		rabbit.CrmClientKarmaChangedConsumer, // consumer
		true,                                 // auto-ack
		false,                                // exclusive
		false,                                // no-local
		false,                                // no-wait
		nil,                                  // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleClientKarmaChanged(msgs)
	return nil
}

func handleDriverKarmaChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling driver karma changing"

	limit := limiter.NewConcurrencyLimiter(maxKarmaChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var karma structures.UserKarma
					if err := json.Unmarshal(d.Body, &karma); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					if err := models.UpdateDriverKarma(karma); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
					}
				}),
			)
		}
	}
}

func handleClientKarmaChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling client karma changing"

	limit := limiter.NewConcurrencyLimiter(maxKarmaChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var karma structures.UserKarma
					if err := json.Unmarshal(d.Body, &karma); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					if err := models.UpdateClientKarma(karma); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
					}
				}),
			)
		}
	}
}

func initRabClientBlacklistChanged() error {
	clientChannel, err := rb.GetReceiver(rabbit.CrmClientBlacklistChangedQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = clientChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := clientChannel.QueueDeclare(
		rabbit.CrmClientBlacklistChangedQueue, // name
		true,                                  // durable
		false,                                 // delete when unused
		false,                                 // exclusive
		false,                                 // no-wait
		nil,                                   // arguments
	)
	if err != nil {
		return err
	}

	err = clientChannel.QueueBind(
		q.Name,                     // queue name
		rabbit.BlacklistChangedKey, // routing key
		rabbit.ClientExchange,      // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := clientChannel.Consume(
		q.Name,                                   // queue
		rabbit.CrmClientBlacklistChangedConsumer, // consumer
		true,                                     // auto-ack
		false,                                    // exclusive
		false,                                    // no-local
		false,                                    // no-wait
		nil,                                      // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleClientBlacklistChanged(msgs)
	return nil
}

func handleClientBlacklistChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling client blacklist changing"

	limit := limiter.NewConcurrencyLimiter(maxBlacklistChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var blacklist structures.UserBlacklist
					if err := json.Unmarshal(d.Body, &blacklist); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					if err := models.UpdateClientBlacklist(blacklist); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
					}
				},
			))
		}
	}
}
