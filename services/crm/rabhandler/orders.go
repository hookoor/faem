package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

func initRabOrder() error {
	receiverChannelOrderState, err := rb.GetReceiver(rabbit.CrmOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannelOrderState.QueueDeclare(
		rabbit.CrmOrderStateQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения статусов заказов
	err = receiverChannelOrderState.QueueBind(
		q.Name,               // queue name
		"state.*",            // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	receiverChannelNewOrder, err := rb.GetReceiver(rabbit.CrmNewOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	orderFromClient, err := receiverChannelNewOrder.QueueDeclare(
		rabbit.CrmNewOrderQueue, // name
		true,                    // durable
		false,                   // delete when unused
		false,                   // exclusive
		false,                   // no-wait
		nil,                     // arguments
	)
	if err != nil {
		return err
	}

	// Биндим очередь для получения заказов
	err = receiverChannelNewOrder.QueueBind(
		orderFromClient.Name, // queue name
		rabbit.NewKey,        // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера
	msgs, err := receiverChannelOrderState.Consume(
		q.Name,                       // queue
		rabbit.CrmOrderStateConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера для принятия заказов с клиентского
	newOrders, err := receiverChannelNewOrder.Consume(
		orderFromClient.Name,       // queue
		rabbit.CrmNewOrderConsumer, // consumer
		true,                       // auto-ack
		false,                      // exclusive
		false,                      // no-local
		false,                      // no-wait
		nil,                        // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go receiveOrderStates(msgs)
	wg.Add(1)
	go handleOrder(newOrders)
	return nil
}

//handleOrder принимает новые заказы с клиенского приложения
func handleOrder(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			pub := d.Headers["publisher"]
			if pub == "crm" || pub == "client" {
				continue
			}
			var (
				order orders.OrderCRM
			)
			err := json.Unmarshal([]byte(d.Body), &order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new order [crm]",
					"reason": "Error unmarshalling order",
				}).Error(err)
				continue
			}

			order.StoreUUID = order.GetProductsData().StoreData.UUID

			order, err = order.SaveNewOrder()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new order [crm]",
					"reason": "Error saving order to db",
				}).Error(err)
				continue
			}

			// dur, _ := time.ParseDuration(config.St.Application.DistributingTime)
			// filledOrder.CancelTime = time.Now().Add(dur)

			if order.Service.ProductDelivery {
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling new order [crm]",
				"orderUUID": order.UUID,
			}).Info("order taken")

			err = order.SendToDriver()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling new order [crm]",
					"reason":    "Error sending order to broker",
					"orderUUID": order.UUID,
				}).Error(err)
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling new order [crm]",
				"orderUUID": order.UUID,
			}).Debug("Order send to broker")
			//отправлем новую структуру заказа в client чтобы там вбился тарифф
			// err = SendJSONByAction(AcNewOrderData, order.Order)
			// if err != nil {
			// 	logs.Eloger.WithFields(logrus.Fields{
			// 		"event":     "handling new order [crm]",
			// 		"reason":    "Error sending order data to broker",
			// 		"orderUUID": order.UUID,
			// 	}).Error(err)

			// }
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling new order [crm]",
				"orderUUID": order.UUID,
			}).Debug("New order data send to broker")
		}
	}
}

func receiveOrderStates(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			go handleOrderState(d)
		}
	}
}

func handleOrderState(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order state from broker [crm]",
	})

	publisher, ok := d.Headers["publisher"].(string)
	if !ok {
		log.WithFields(logrus.Fields{"reason": "error parse publisher header"}).Error()
		return
	} else if publisher == "crm" || publisher == "driver" {
		return
	}
	log = log.WithField("publisher", publisher)

	rqOrState := new(structures.OfferStates)
	if err := json.Unmarshal([]byte(d.Body), rqOrState); err != nil {
		log.WithField("reason", "error unmarshalling order state").Error(err)
		return
	}
	log = log.WithFields(logrus.Fields{
		"orderUUID": rqOrState.OrderUUID,
		"state":     rqOrState.State,
		"offerUUID": rqOrState.OfferUUID,
	})

	if err := orders.SetOrderState(rqOrState); err != nil {
		log.WithField("reason", "error set order state").Error(err)
		return
	}

	log.Infof("CRM change order %s state to %s", rqOrState.OrderUUID, rqOrState.State)
}
