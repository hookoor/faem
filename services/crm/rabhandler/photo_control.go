package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

func initPhotoControl() error {
	receiverChannelPhotoControl, err := rb.GetReceiver(rabbit.DriverNewOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}
	q, err := receiverChannelPhotoControl.QueueDeclare(
		rabbit.CrmNewPhotoControlQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannelPhotoControl.QueueBind(
		q.Name,                 // queue name
		rabbit.PhotoControlKey, // routing key
		rabbit.DriverExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	newPhotoControl, err := receiverChannelPhotoControl.Consume(
		q.Name,                         // queue
		rabbit.CrmPhotoControlConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	go handleNewPhotoControl(newPhotoControl)
	return nil
}

func handleNewPhotoControl(msgs <-chan amqp.Delivery) {
	for d := range msgs {
		if d.Headers["publisher"] == "crm" {
			continue
		}
		var (
			phCon models.PhotoControlCRM
		)
		log := logs.Eloger.WithFields(logrus.Fields{
			"event": "handling new photocontrol [crm]",
		})
		err := json.Unmarshal([]byte(d.Body), &phCon)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "Error unmarshalling data",
			}).Error(err)
			continue
		}
		err = phCon.Save()
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":      "Error saving data to db",
				"driverUUID":  phCon.DriverID,
				"controlUUID": phCon.ControlID,
			}).Error(err)
			continue
		}
		log.WithFields(logrus.Fields{
			"driverUUID":  phCon.DriverID,
			"controlUUID": phCon.ControlID,
		}).Info("photocontrol handled successfully")
	}
}
