package rabhandler

import (
	"sync"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
)

var (
	db *pg.DB

	rb     *rabbit.Rabbit
	wg     *sync.WaitGroup
	closed = make(chan struct{})
)

func InitVars(r *rabbit.Rabbit, conn *pg.DB, waitG *sync.WaitGroup) {
	rb = r
	db = conn
	wg = waitG
}

// StartService функция для непосредственной использования брокера
// здесь инцициализируем кастомеров, очереди и развешиваем ивенты
func StartService() error {
	err := initRabNewDriver()
	if err != nil {
		rsp := errors.Errorf("Error init driver exchange. %s", err)
		return rsp
	}
	err = initRabDriverUpate()
	if err != nil {
		return errors.Errorf("Error initRabDriverUpate. %s", err)
	}

	err = initRabDriverStates()
	if err != nil {
		rsp := errors.Errorf("Error init driver state exchange. %s", err)
		return rsp
	}
	err = initRabChatMess()
	if err != nil {
		rsp := errors.Errorf("Error init chat message receiver %s", err)
		return rsp
	}
	err = initTicketsHandler()
	if err != nil {
		rsp := errors.Errorf("Error init tickets receiver %s", err)
		return rsp
	}
	err = initRabNewClient()
	if err != nil {
		rsp := errors.Errorf("Error init new client queues. %s", err)
		return rsp
	}
	err = initRabUpdateClient()
	if err != nil {
		rsp := errors.Errorf("Error init new client queues. %s", err)
		return rsp
	}
	err = initBillingCompleted()
	if err != nil {
		rsp := errors.Errorf("Error initBillingCompleted. %s", err)
		return rsp
	}
	if err = initPaymentTypeChanged(); err != nil {
		return errors.Errorf("Error initPaymentTypeChanged. %s", err)
	}

	err = initRabOrder()
	if err != nil {
		rsp := errors.Errorf("Error init offer states exchange. %s", err)
		return rsp
	}

	err = initRabOrderUpdate()
	if err != nil {
		rsp := errors.Errorf("Error init initRabOrderUpdate. %s", err)
		return rsp
	}

	err = initRabOrderSetRating()
	if err != nil {
		rsp := errors.Errorf("Error init initRabOrderSetRating. %s", err)
		return rsp
	}

	err = initEvents()
	if err != nil {
		rsp := errors.Errorf("Error init events. %s", err)
		return rsp
	}

	err = initNewOrderFromBot()
	if err != nil {
		rsp := errors.Errorf("Error init new order from bot creation. %s", err)
		return rsp
	}

	// err = initPhotoControl()
	// if err != nil {
	// 	rsp := errors.Errorf("Error init photocontrol. %s", err)
	// 	return rsp
	// }

	if err = initRabActivityChanged(); err != nil {
		return errors.Errorf("Error initRabActivityChanged. %s", err)
	}

	if err = initRabKarmaChanged(); err != nil {
		return errors.Errorf("Error initRabKarmaChanged. %s", err)
	}

	if err = initRabClientBlacklistChanged(); err != nil {
		return errors.Errorf("Error initRabBlacklistChanged. %s", err)
	}

	if err = initRabActionOnOrder(); err != nil {
		return errors.Errorf("Error initRabBlacklistChanged. %s", err)
	}

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("broker stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("broker stopped forcibly")
	}
}
