package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabOrderSetRating -
func initRabOrderSetRating() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmSetOrderRatingQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmSetOrderRatingQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		rabbit.SetRatingKey,  // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannel.Consume(
		q.Name,                           // queue
		rabbit.CrmSetOrderRatingConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderSetRating(msgs)
	return nil
}

func handleOrderSetRating(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var (
				order orders.OrderCRM
			)
			err := json.Unmarshal([]byte(d.Body), &order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order rating",
					"reason":    "Error unmarshalling order",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			err = orders.UpdateOrderRating(&order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order rating",
					"reason":    "Error update order rating",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling update order rating",
				"OrderUUID": order.UUID,
			}).Info("Order rating successfully update")
		}
	}
}
