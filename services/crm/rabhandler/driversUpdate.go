package rabhandler

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabDriverUpate обновления данных водителя
func initRabDriverUpate() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmDriverUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmDriverUpdateQueue, // name
		true,                        // durable
		false,                       // delete when unused
		false,                       // exclusive
		false,                       // no-wait
		nil,                         // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения заказов
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                         // queue
		rabbit.CrmDriverUpdateConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverData(msgs)
	return nil
}

func handleDriverData(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var driver, driverForBind models.DriverCRM
			err := json.Unmarshal([]byte(d.Body), &driverForBind)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling new driver data [crm]",
				"routingKey": d.RoutingKey,
				"driverUUID": driver.UUID,
			})
			driver.Driver = driverForBind.Driver
			driver.Password = driverForBind.Password
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error binding data",
				}).Error(err)
				continue
			}
			err = models.UpdateByPK(&driver)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"reason": "Error saving new driver data",
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{}).Info("new driver data handled")
		}
	}
}
