package rabhandler

import (
	"encoding/json"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabDriverStates данные для получения водительских стаусов из брокера
func initRabDriverStates() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmDriverStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmDriverStateQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения заказов
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.StateKey+".*",  // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                        // queue
		rabbit.CrmDriverStateConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverState(msgs)
	return nil
}

func handleDriverState(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var driverState structures.DriverStates
			err := json.Unmarshal([]byte(d.Body), &driverState)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling driver state [crm]",
					"reason": "Error binding data",
				}).Error(err)

				continue
			}

			// Сохраняем статус драйвера в местную базу
			_, err = models.SaveDriverState(driverState)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling driver state [crm]",
					"reason":     "Error saving new driver state",
					"driverUUID": driverState.DriverUUID,
				}).Error(err)

				continue
			}
			// msg := fmt.Sprintf("new state=%s", driverState.State)
			// logs.OutputDriverEvent("CRM change driver state", msg, driverState.DriverUUID)
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling driver state [crm]",
				"value":      driverState.State,
				"driverUUID": driverState.DriverUUID,
			}).Info("CRM change driver state")
		}
	}
}
