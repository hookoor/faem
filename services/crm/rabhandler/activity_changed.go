package rabhandler

import (
	"encoding/json"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

const (
	maxActivityChangesAllowed = 5
)

func initRabActivityChanged() error {
	if err := initDriverActivityChanged(); err != nil {
		return errors.Wrap(err, "failed to init driver activity changing listener")
	}
	if err := initClientActivityChanged(); err != nil {
		return errors.Wrap(err, "failed to init client activity changing listener")
	}
	return nil
}

func initDriverActivityChanged() error {
	driverChannel, err := rb.GetReceiver(rabbit.CrmDriverActivityChangedQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = driverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := driverChannel.QueueDeclare(
		rabbit.CrmDriverActivityChangedQueue, // name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	if err != nil {
		return err
	}

	err = driverChannel.QueueBind(
		q.Name,                    // queue name
		rabbit.ActivityChangedKey, // routing key
		rabbit.DriverExchange,     // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := driverChannel.Consume(
		q.Name,                                  // queue
		rabbit.CrmDriverActivityChangedConsumer, // consumer
		true,                                    // auto-ack
		false,                                   // exclusive
		false,                                   // no-local
		false,                                   // no-wait
		nil,                                     // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleDriverActivityChanged(msgs)
	return nil
}

func initClientActivityChanged() error {
	clientChannel, err := rb.GetReceiver(rabbit.CrmClientActivityChangedQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = clientChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := clientChannel.QueueDeclare(
		rabbit.CrmClientActivityChangedQueue, // name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	if err != nil {
		return err
	}

	err = clientChannel.QueueBind(
		q.Name,                    // queue name
		rabbit.ActivityChangedKey, // routing key
		rabbit.ClientExchange,     // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := clientChannel.Consume(
		q.Name,                                  // queue
		rabbit.CrmClientActivityChangedConsumer, // consumer
		true,                                    // auto-ack
		false,                                   // exclusive
		false,                                   // no-local
		false,                                   // no-wait
		nil,                                     // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleClientActivityChanged(msgs)
	return nil
}

func handleDriverActivityChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling driver activity changing"

	limit := limiter.NewConcurrencyLimiter(maxActivityChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var activityItem models.CrmActivityHistoryItem
					if err := json.Unmarshal(d.Body, &activityItem); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					if err := models.UpdateDriverActivity(activityItem); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
					}
				}),
			)
		}
	}
}

func handleClientActivityChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling client activity changing"

	limit := limiter.NewConcurrencyLimiter(maxActivityChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var activityItem models.CrmActivityHistoryItem
					if err := json.Unmarshal(d.Body, &activityItem); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					if err := models.UpdateClientActivity(activityItem); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
					}
				}),
			)
		}
	}
}
