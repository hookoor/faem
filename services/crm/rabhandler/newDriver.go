package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"

	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabNewDriver данные для получения водителей из брокера
func initRabNewDriver() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmNewDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmNewDriverQueue, // name
		true,                     // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения водителей
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.NewKey,         // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                      // queue
		rabbit.CrmNewDriverConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewDriver(msgs)
	return nil
}

func handleNewDriver(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var (
				driverCRM models.DriverCRM
			)
			err := json.Unmarshal(d.Body, &driverCRM.Driver)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new driver",
					"reason": "Error unmarshalling driver",
				}).Error(err)

				continue
			}
			check, err := models.CheckExistsUUIDWithError(&driverCRM, driverCRM.UUID)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling new driver",
					"reason":     "error checking driver existing",
					"driverUUID": driverCRM.UUID,
				}).Error(err)
				continue
			}
			if check {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling new driver",
					"reason":     "Driver already exist",
					"driverUUID": driverCRM.UUID,
				}).Error(err)
				continue
			}
			// Сохраняем драйвера в местную базу
			_, err = driverCRM.SaveDriverFromDriverAPP()
			if err != nil {

				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling new driver",
					"reason":     "Error saving new driver",
					"driverUUID": driverCRM.UUID,
				}).Error(err)
				continue
			}
			err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driverCRM.Driver)
			if err != nil {

				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling new driver",
					"reason":     "Error send new driver data",
					"driverUUID": driverCRM.UUID,
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling new driver",
				"driverUUID": driverCRM.UUID,
			}).Info("CRM created new driver")
		}
	}
}
