package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabOrderUpdate обновления заказов
func initRabOrderUpdate() error {
	receiverChannel, err := rb.GetReceiver(rabbit.CrmOrderUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.CrmOrderUpdateQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения обновлений заказа
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.UpdateKey+".#", // routing key
		rabbit.OrderExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                        // queue
		rabbit.CrmOrderUpdateConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderUpdate(msgs)
	return nil
}

func handleOrderUpdate(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "crm" {
				continue
			}
			var (
				order orders.OrderCRM
			)
			err := json.Unmarshal([]byte(d.Body), &order.Order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order data",
					"reason":    "Error unmarshalling order",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			switch d.RoutingKey {
			case rabbit.UpdateOrderRoutesKey:
				err = order.UpdateRoutesAndTariff()
			case rabbit.UpdateIncreasedFareKey:
				err = order.UpdateIncreasedFareAndTariff()
			case rabbit.UpdateOnlyCommentKey:
				err = order.UpdateComment()
			case rabbit.UpdateOrderTariffKey:
				err = orders.UpdateOrderTariff(order.UUID, order.Tariff)
			default:
				err = models.UpdateByPK(&order)
			}
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order data",
					"reason":    "Error update order",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling update order data",
				"OrderUUID": order.UUID,
			}).Info("Order successfully update")
		}
	}
}
