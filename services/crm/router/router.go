package router

import (
	"log"

	"gitlab.com/faemproject/backend/core/shared/auth"
	"gitlab.com/faemproject/backend/core/shared/router"
	"gitlab.com/faemproject/backend/core/shared/web"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	h "gitlab.com/faemproject/backend/faem/services/crm/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
)

func authActions(rgh *router.Groups) {
	rgh.Public.POST("/login", h.LoginHandler, "Название эндпоинта")
	rgh.Public.POST("/refresh", h.LoginRefreshHandler, "Название эндпоинта")
}

func addresses(rgh *router.Groups) {
	rgh.Public.POST("", h.AddressesV2, "Название эндпоинта")
	rgh.Public.POST("/levenshtein", h.AddressesByLevenshtein, "Название эндпоинта")
}

// Специфичная для парнтеров информация
// ключ используется для идентификации партнера
func info(rgh *router.Groups) {
	rgh.Public.GET("/:source_id", h.GetSpecificInfo, "Название эндпоинта")
	rgh.Public.PUT("/:source_id", h.UpdateSpecificInfo, "Название эндпоинта")
	rgh.Public.POST("/:source_id", h.InsertSpecificInfo, "Название эндпоинта")
	rgh.Public.DELETE("/:source_id", h.DeleteSpecificInfo, "Название эндпоинта")
}

func sources(rgh *router.Groups) {
	rgh.Public.GET("/:uuid", h.GetSourceOfOrder, "Название эндпоинта")
	rgh.Public.GET("/:name/asterisk_hello", h.GetAsteriskHelloBySourceName, "Название эндпоинта")

	rgh.Private.POST("", h.CreateSourceOfOrder, "Название эндпоинта")
	rgh.Private.POST("/filter", h.SourceOfOrdersList, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateSourceOfOrder, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteSourceOfOrders, "Название эндпоинта")

	rgh.Private.GET("/options/:uuid", h.GetSourceOfOrderServices, "Название эндпоинта")
}

func taxiparks(rgh *router.Groups) {
	rgh.Public.GET("/:uuid", h.GetTaxiParkByUUID, "Название эндпоинта")
	rgh.Public.GET("/many", h.GetTaxiParksByUUIDs, "Название эндпоинта")
	rgh.Public.GET("/by_max_weight/:reg_id", h.GetTaxiParkByMaxWeight, "Название эндпоинта")

	rgh.Private.POST("", h.CreateTaxiPark, "Название эндпоинта")
	rgh.Private.POST("/filter", h.TaxiParkListByFilter, "Название эндпоинта")
	rgh.Private.POST("/:uuid/balance", h.SetTaxiParkBalance, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateTaxiPark, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteTaxiPark, "Название эндпоинта")
}

func regions(rgh *router.Groups) {
	rgh.Public.GET("/regions/:id", dhand.RegionsController.GetByID, "Название эндпоинта")
	rgh.Public.GET("/regions/driverstariffs/:id", dhand.RegionsController.GetDriversTariffs, "Название эндпоинта")

	rgh.Private.POST("", h.CreateRegion, "Название эндпоинта")
	rgh.Private.GET("", h.GetRegions, "Название эндпоинта")
	rgh.Private.GET("/:id", h.GetRegionByID, "Название эндпоинта")
	rgh.Private.PUT("/:id", h.UpdateRegion, "Название эндпоинта")
	rgh.Private.DELETE("/:id", h.DeleteRegion, "Название эндпоинта")
}

func districts(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateDistrict, "Название эндпоинта")
	rgh.Private.GET("", h.DistrictsList, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetDistrict, "Название эндпоинта")
	rgh.Private.GET("/region/:uuid", h.GetRegionDistricts, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateDistrict, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteDistrict, "Название эндпоинта")
}

func drivers(rgh *router.Groups) {
	rgh.Public.POST("/getlocations", h.GetDriversLocations, "Название эндпоинта")
	rgh.Public.POST("/nearest", h.GetNearestDriversLocData, "Название эндпоинта")
	rgh.Public.GET("/state/:driveruuid", h.GetDriverActualState, "Название эндпоинта")
	rgh.Public.POST("/states", h.GetDriversActualStates, "Название эндпоинта")
	rgh.Public.GET("/uuid/state/:state", h.GetDriversUUIDByState, "Название эндпоинта")

	rgh.Private.POST("", h.CreateDriver, "Название эндпоинта")
	rgh.Private.GET("/:uuid", dhand.Drivers.DriversCRUD.GetDriver, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", dhand.Drivers.DriversCRUD.UpdateDriver, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteDriver, "Название эндпоинта")

	rgh.Private.POST("/filter", dhand.Drivers.DriversCRUD.FilterDriversList, "Название эндпоинта")

	rgh.Private.PUT("/blacklist/add", h.AddNewPhoneToDriverBlackList, "Название эндпоинта")
	rgh.Private.PUT("/blacklist/remove", h.RemovePhoneFromDriverBlackList, "Название эндпоинта")

	rgh.Private.POST("/appversion", h.CheckDriverActualAppVersion, "Название эндпоинта")
	rgh.Private.PUT("/remove/service/:uuid", h.RemoveServiceFromAllDrivers, "Название эндпоинта")
	rgh.Private.POST("/:uuid/balance", h.SetDriverBalance, "Название эндпоинта")
	rgh.Private.POST("/balances", h.SetDriversBalances, "Название эндпоинта")
}

func driver(rgh *router.Groups) {
	rgh.Public.GET("/:uuid/active_services", h.GetDriverActiveServices, "Название эндпоинта")
	rgh.Public.GET("/:uuid/active_features", h.GetDriverActiveFeatures, "Название эндпоинта")

	rgh.Public.GET("/getcounterorderswitch", dhand.GetCounterOrderSwitch, "Название эндпоинта")
	rgh.Public.POST("/setcounterorderswitch", dhand.SetCounterOrderSwitch, "Название эндпоинта")

	rgh.Private.GET("/service-relation/:service-uuid", dhand.Drivers.ServicesRelation.SetRelation, "Название эндпоинта")
	rgh.Private.DELETE("/service-relation/:service-uuid", dhand.Drivers.ServicesRelation.DeleteRelation, "Название эндпоинта")

	rgh.Private.GET("/feature-relation/:feature-uuid", dhand.Drivers.FeaturesRelation.SetRelation, "Название эндпоинта")
	rgh.Private.DELETE("/feature-relation/:feature-uuid", dhand.Drivers.FeaturesRelation.DeleteRelation, "Название эндпоинта")

	rgh.Private.GET("/services", dhand.Drivers.ServicesRelation.GetServicesByDriverUUID, "Название эндпоинта")
	rgh.Private.GET("/features", dhand.Drivers.FeaturesRelation.GetFeaturesForDriver, "Название эндпоинта")
}

func penalties(rgh *router.Groups) {
	rgh.Private.GET("/zones", h.GetZones, "Название эндпоинта")
	rgh.Private.GET("", h.PenaltiesList, "Название эндпоинта")
	rgh.Private.POST("", h.CreatePenalty, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeletePenalty, "Название эндпоинта")
}

func blacklist(rgh *router.Groups) {
	rgh.Public.GET("/client/isin/:phone", h.IsTheClientBlacklisted, "Название эндпоинта")
	rgh.Public.GET("/phone/isin/:phone", h.IsPhoneInBlacklist, "Название эндпоинта")

	rgh.Private.GET("", h.GetBlackList, "Название эндпоинта")
	rgh.Private.GET("/:phone", h.GetBlackListedPhone, "Название эндпоинта")
	rgh.Private.POST("/:phone", h.UpdateBlackListedPhone, "Название эндпоинта")
	rgh.Private.POST("/filter", h.GetBlackListByFilter, "Название эндпоинта")
	rgh.Private.POST("", h.AddNewPhoneToBlackList, "Название эндпоинта")
	rgh.Private.DELETE("", h.RemovePhoneFromBlackList, "Название эндпоинта")
}

func drivertariffs(rgh *router.Groups) {
	rgh.Public.GET("/markups/:uuid", h.GetDriverTariffMarkupsByUUID, "Название эндпоинта")
	rgh.Public.GET("/manymarkups", h.GetManyDriverTariffMarkupsByUUID, "Название эндпоинта")
	rgh.Public.GET("/offline", h.GetOfflineDriverTariff, "Название эндпоинта")
	rgh.Public.GET("/offline/:driveruuid", h.GetDriverOfflineTariff, "Название эндпоинта")

	rgh.Private.POST("", h.CreateDriverTariff, "Название эндпоинта")
	rgh.Private.GET("", h.DriverTariffsList, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateDriverTariff, "Название эндпоинта")
	rgh.Private.GET("/filter", h.DriverTariffsListByFilter, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.DriverTariffByUUID, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteDriverTariff, "Название эндпоинта")
	rgh.Private.PUT("/setdefault/:uuid", h.SetDefaultDriverTariff, "Название эндпоинта")
	rgh.Private.PUT("/setoffline/:param", h.SetOfflineDriverTariff, "Название эндпоинта")
	rgh.Private.PUT("/markups/set/:uuid", h.AddMarkupToDriverTariff, "Название эндпоинта")
	rgh.Private.PUT("/markups/remove/:uuid", h.RemoveMarkupFromDriverTariff, "Название эндпоинта")
	rgh.Private.POST("/bind", h.HandleBindTariffToDriver, "Название эндпоинта")
}

func services(rgh *router.Groups) {
	rgh.Public.GET("/standard", h.GetStandardServices, "Название эндпоинта")

	rgh.Private.POST("", h.CreateService, "Название эндпоинта")
	rgh.Private.GET("", h.ServicesList, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetService, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateService, "Название эндпоинта")
	rgh.Private.PUT("/productdelivery/:uuid", h.SetServiceAsProductDelivery, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteService, "Название эндпоинта")

	rgh.Private.GET("/set/region/:service-set-id", h.GetServiceSet, "Название эндпоинта")
	rgh.Private.PUT("/set/region-active-setting/:service-set-id", h.UpdateServiceSet, "Название эндпоинта")
}

func features(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateFeature, "Название эндпоинта")
	rgh.Private.GET("", h.FeatureList, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetFeature, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateFeature, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteFeature, "Название эндпоинта")

	rgh.Private.POST("/set/region/seting", dhand.FeaturesSetForRegion.Seting, "Название эндпоинта")
	rgh.Private.POST("/set/region", dhand.FeaturesSetForRegion.GetFilteredList, "Название эндпоинта")
	rgh.Private.PUT("/set/region-active-setting/:feature-set-id", dhand.FeaturesSetForRegion.SettingByArray, "Название эндпоинта")
	rgh.Private.POST("/set/region-active/:feature-set-id", dhand.FeaturesSetForRegion.GetListWithActiveMarks, "Название эндпоинта")
	rgh.Private.PATCH("/set/region/:id", dhand.FeaturesSetForRegion.Update, "Название эндпоинта")
	rgh.Private.DELETE("/set/region/:id", dhand.FeaturesSetForRegion.Delete, "Название эндпоинта")

	rgh.Private.POST("/set/service/seting", dhand.FeaturesSetForService.SetFetureForService, "Название эндпоинта")
	rgh.Private.POST("/set/service", dhand.FeaturesSetForService.GetFilteredList, "Название эндпоинта")
	rgh.Private.PUT("/set/service-active-setting", dhand.FeaturesSetForService.SettingByArray, "Название эндпоинта")
	rgh.Private.POST("/set/service-active", dhand.FeaturesSetForService.GetListWithActiveMarks, "Название эндпоинта")
	rgh.Private.DELETE("/set/service/:id", dhand.FeaturesSetForService.Delete, "Название эндпоинта")
}

func tickets(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateTicket, "Название эндпоинта")
	rgh.Private.PUT("/comments/add/:uuid", h.AppendCommentToTicket, "Название эндпоинта")
	rgh.Private.PUT("/state/:uuid", h.ChangeTicketState, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetTickerByUUID, "Название эндпоинта")
	rgh.Private.GET("/filter", h.TicketsListWithOptions, "Название эндпоинта")
}

func increasedfare(rgh *router.Groups) {
	rgh.Public.GET("", h.GetCurrentIncreasedFareSteps, "Название эндпоинта")
	rgh.Private.POST("", h.CreateCurrentIncreasedFareSteps, "Название эндпоинта")
}

func users(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateUser, "Название эндпоинта")
	rgh.Private.GET("", h.UsersList, "Название эндпоинта")
	rgh.Private.GET("/filter", h.UsersListWithOptions, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetUser, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateUser, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteUser, "Название эндпоинта")
}

func driverNews(rgh *router.Groups) {
	rgh.Public.GET("/:uuid", h.GetDriverNews, "Название эндпоинта")

	rgh.Private.POST("", h.CreateDriverNews, "Название эндпоинта")
	rgh.Private.POST("/filter", h.GetDriverNewsList, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateDriverNews, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteDriverNews, "Название эндпоинта")
}

func driverGroup(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateDriverGroup, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetDriverGroupByUUID, "Название эндпоинта")
	rgh.Private.GET("", h.GetDriverGroupList, "Название эндпоинта")
	rgh.Private.POST("/filter", h.GetDriverGroupListByFilter, "Название эндпоинта")
	rgh.Private.POST("/drag/:uuid", h.DragDriversToGroup, "Название эндпоинта")
	rgh.Private.POST("/add/:uuid", h.AddDriversToGroup, "Название эндпоинта")
	rgh.Private.POST("/drop/:uuid", h.DropDriversFromGroup, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateDriverGroup, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteDriverGroup, "Название эндпоинта")
	rgh.Private.GET("/services/:uuid", h.GetDrvGroupServices, "Название эндпоинта")
	rgh.Private.GET("/tariffs/:uuid", h.GetDrvGroupTariffs, "Название эндпоинта")
}

func quickMessages(rgh *router.Groups) {
	rgh.Private.GET("", h.QuickMessagesList, "Название эндпоинта")
	rgh.Private.POST("", h.CreateQuickMessage, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateQuickMessage, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.SetQuickMessageDeleted, "Название эндпоинта")
}

func addressSetTo(rgh *router.Groups) {
	rgh.Private.POST("", dhand.AddressSetTo.CreateCityRelation, "Название эндпоинта")
	rgh.Private.GET("/:id", dhand.AddressSetTo.GetByID, "Название эндпоинта")
	rgh.Private.POST("/filter", dhand.AddressSetTo.GetFilteredList, "Название эндпоинта")
	rgh.Private.PUT("/:id", dhand.AddressSetTo.Update, "Название эндпоинта")
	rgh.Private.DELETE("/:id", dhand.AddressSetTo.Delete, "Название эндпоинта")
}
func tariffSurcharges(rgh *router.Groups) {
	rgh.Private.GET("", h.GetTariffSurcharges, "Название эндпоинта")
	rgh.Private.POST("", h.CreateTariffSurcharge, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateTariffSurcharge, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetTariffSurchargeByUUID, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.SetTariffSurchargeDeleted, "Название эндпоинта")
}
func photoControls(rgh *router.Groups) {
	rgh.Private.GET("/filter", h.PhotoControlsListWithOptions, "Название эндпоинта")
	rgh.Private.POST("/:uuid", h.UpdatePhotoControl, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetPhotoControlByUUID, "Название эндпоинта")
}

func clients(rgh *router.Groups) {
	rgh.Public.POST("/appversion", h.CheckClientAppVersion, "Название эндпоинта")
	rgh.Public.POST("/whereami", h.WhereAmI, "Название эндпоинта")
	rgh.Public.POST("/get_source_uuid", h.GetSourceOfOrderUUID, "Название эндпоинта")
	rgh.Private.GET("", h.ClientsList, "Название эндпоинта")
	rgh.Private.GET("/:uuid", h.GetClient, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateClient, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteClient, "Название эндпоинта")
}

func destinationPoints(rgh *router.Groups) {
	rgh.Private.POST("", h.CreateDesinationPoint, "Название эндпоинта")
	rgh.Private.GET("/categories", h.GetAllCategories, "Название эндпоинта")
	rgh.Private.GET("/filter", h.DestinationPointsListWithOptions, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateDesinationPoint, "Название эндпоинта")
	rgh.Private.GET("/reboot", h.InitAddressesByRequest, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.SetDestPointDeleted, "Название эндпоинта")
}

func stores(rgh *router.Groups) {
	rgh.Public.POST("/categories", h.CategoriesList, "Название эндпоинта")
	rgh.Public.POST("", h.StoreList, "Название эндпоинта")
	rgh.Public.GET("/:uuid", h.StoreByUUID, "Название эндпоинта")
	rgh.Public.GET("/url/:url", h.StoreByURL, "Название эндпоинта")

	rgh.Private.POST("/create", h.CreateStore, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateStore, "Название эндпоинта")
	rgh.Private.PUT("/priority", h.UpdateStoreSerialPriority, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.SetStoreDeletedByUUID, "Название эндпоинта")

	rgh.Private.POST("/categories/create", h.CreateStoreCategory, "Название эндпоинта")
}

func products(rgh *router.Groups) {
	rgh.Public.POST("", h.ProductsList, "Название эндпоинта")
	rgh.Public.POST("/fororder", h.ProductDataForOrder, "Название эндпоинта")
	rgh.Public.GET("/:uuid", h.GetProductByUUID, "Название эндпоинта")

	rgh.Private.POST("/create/", h.CreateProduct, "Название эндпоинта")
	rgh.Private.PUT("/:uuid", h.UpdateProduct, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.SetProductDeletedByUUID, "Название эндпоинта")

	rgh.Private.POST("/categories/", h.CreateNewProductCategory, "Название эндпоинта")
	rgh.Private.GET("/categories/:uuid", h.GetProductCategoriesByStoreUUID, "Название эндпоинта")
	rgh.Private.GET("/categories/", h.GetAllProductCategories, "Название эндпоинта")
	rgh.Private.DELETE("/categories/:uuid", h.SetProductCategoryDeleted, "Название эндпоинта")
}

func mailing(rgh *router.Groups) {
	rgh.Private.GET("/push", h.GetAllPushMailings, "Название эндпоинта")
	rgh.Private.POST("/push", h.CreateNewPushMailing, "Название эндпоинта")
}

func tariffs(rgh *router.Groups) {
	rgh.Private.POST("/prices", h.CreateTariffPrices, "Название эндпоинта")
	rgh.Private.POST("/prices/filter", h.TariffPriceList, "Название эндпоинта")
	rgh.Private.GET("/prices/:uuid", h.GetTariffPriceByUUID, "Название эндпоинта")
	rgh.Private.PUT("/prices/:uuid", h.UpdateTariffPrices, "Название эндпоинта")
	rgh.Private.DELETE("/prices/:uuid", h.SetTariffPriceDeleted, "Название эндпоинта")

	rgh.Private.POST("/rules/filter", h.TariffRulesList, "Название эндпоинта")
	rgh.Private.GET("/rules/:uuid", h.GetTariffRuleByUUID, "Название эндпоинта")
	rgh.Private.POST("/rules", h.CreateTariffRule, "Название эндпоинта")
	rgh.Private.POST("/rules/:uuid", h.UpdateTariffRule, "Название эндпоинта")
	rgh.Private.DELETE("/rules", h.SetTariffRuleDeleted, "Название эндпоинта")
}

func activities(rgh *router.Groups) {
	rgh.Private.GET("", h.GetActivities, "Название эндпоинта")
	rgh.Private.PUT("", h.UpdateActivities, "Название эндпоинта")
}

func hConfig(rgh *router.Groups) {
	rgh.Public.GET("/activity_rate", h.FetchActivityRate, "Название эндпоинта")
	rgh.Public.GET("/blocking", h.FetchBlockingConfig, "Название эндпоинта")
	rgh.Public.GET("/activity", h.FetchActivityConfig, "Название эндпоинта")

	rgh.Private.GET("/insurance", h.GetInsuranceParams, "Название эндпоинта")
	rgh.Private.PUT("/insurance", h.UpdateInsuranceParams, "Название эндпоинта")
}

func surge(rgh *router.Groups) {
	rgh.Private.GET("", h.GetSurgeConfig, "Название эндпоинта")
	rgh.Private.PUT("", h.UpdateSurgeConfig, "Название эндпоинта")
	rgh.Private.GET("/zones", h.GetSurgeZones, "Название эндпоинта")
	rgh.Private.GET("/timestamps", h.GetSurgeTimestamps, "Название эндпоинта")
	rgh.Private.POST("/history", h.GetSurgeZoneHistory, "Название эндпоинта")
	rgh.Private.GET("/history/live", h.GetLiveSurgeZoneHistory, "Название эндпоинта")
	rgh.Private.GET("/rules", h.ListSurgeRules, "Название эндпоинта")

	rgh.Private.POST("/rules", h.CreateSurgeRule, "Название эндпоинта")
	rgh.Private.GET("/rules/:id", h.GetSurgeRule, "Название эндпоинта")
	rgh.Private.PUT("/rules/:id", h.UpdateSurgeRule, "Название эндпоинта")
	rgh.Private.DELETE("/rules/:id", h.DeleteSurgeRule, "Название эндпоинта")
}

func prices(rgh *router.Groups) {
	rgh.Private.GET("", h.GetAgregatedPricesList, "Название эндпоинта")
	rgh.Private.GET("/filter", h.GetAgregatedPricesListByFilter, "Название эндпоинта")
	rgh.Private.DELETE("/:uuid", h.DeleteAgregatedPrices, "Название эндпоинта")
	rgh.Private.PUT("", h.InsertAgregatedPricesList, "Название эндпоинта")
}

func distribution(rgh *router.Groups) {
	rgh.Private.GET("/rules/:id", h.GetDistributionRule, "Название эндпоинта")
	rgh.Private.PUT("/rules/:id", h.UpdateDistributionRule, "Название эндпоинта")
}

func debug(rgh *router.Groups) {
	rgh.Private.PUT("/orders/recreate/bystate", h.ReCreateOrdersByState, "Название эндпоинта")
	// rgh.Private.
	// rgh.Private.
	// rgh.Private.
}

func internal(rgh *router.Groups) {
	rgh.Internal.GET("/distribution_rules/client", h.GetClientDistributionRule, "Название эндпоинта")
	rgh.Internal.GET("/getdriversfordistribution/:region_id", dhand.GetDriversForDistribution, "Название эндпоинта") // для получения водителей среди которых будет распределение
	rgh.Internal.GET("/regions_for_distribution", h.GetRegionsNoToken, "Название эндпоинта")
	rgh.Internal.POST("/distribution_rules", h.GetDistributionRulesByIDs, "Название эндпоинта") // правила распределения

	rgh.Internal.GET("/drivers/state/withoutconsider/:driveruuid", h.GetDriverActualStateWithoutConsidering, "Название эндпоинта") // получение текущего статуса водителя игнорируя статус "considering"
	rgh.Internal.PUT("/driver/:uuid/last_counter_assignment", h.UpdateDriverLastCounterAssignment, "Название эндпоинта")

	rgh.Internal.GET("/drivergroup/weights_map", h.GetDriverGroupWeights, "Название эндпоинта")
	rgh.Internal.GET("/taxiparks/weights_map", h.GetTaxiParksDistributionWeights, "Название эндпоинта")
}

// Справочник марок машин и цветов. Используется при регистрации водителя
func carbrands(rgh *router.Groups) {
	rgh.Private.GET("", h.GetAllCarBrands, "Название эндпоинта")
	rgh.Private.POST("", h.SetAllCarBrands, "Название эндпоинта")
}

func events(rgh *router.Groups) {
	rgh.Private.GET("/filter", h.GetEventsByOptions, "Название эндпоинта")
}

// func blacklist(public, private,internal *echo.Group) {
// 	private.
// 	private.
// 	private.
// 	private.
// }

// func blacklist(public, private,internal *echo.Group) {
// 	private.
// 	private.
// 	private.
// 	private.
// }

const apiversion = "v2"

var dhand *h.Handler

// Init - binding middleware and setup routers
func Init(l *logrus.Logger, hndl *h.Handler) *echo.Echo {
	dhand = hndl

	e := echo.New()
	web.UseHealthCheck(e)

	// logrus.SetLevel(logrus.DebugLevel)
	// e.Use(logrusmiddleware.Hook())
	newLogger(l)
	e.Use(Hook())

	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	// Пока нет политик - будет так
	auth.CasbinAuthEnabled = false

	casbinmw, err := auth.GetCasbinMidleware(config.St.Auth.Host, config.St.Auth.ServiceID)
	if err != nil {
		return nil
	}

	// Metrics
	//p := prometheus.NewPrometheus("echo", nil)
	//p.Use(e)
	//e.Use(prometheus.NewMetric())
	//e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))

	apiGroup := e.Group("/api/" + apiversion)
	tempPrivate := apiGroup.Group("", middleware.JWT([]byte(config.JWTSecret())))

	Router := router.GroupRouter{
		Groups: router.Groups{
			Public:   router.NewGroup(apiGroup.Group("", auth.ProvideJWTMidlware(config.JWTSecret()))),
			Private:  router.NewGroup(apiGroup.Group("", middleware.JWT([]byte(config.JWTSecret())), casbinmw)),
			Internal: router.NewGroup(apiGroup.Group("", auth.InternalMiddleware())),
		},
	}

	/*	Если мучает линтёр (VSCode) -  добавляем в ваш settings.json такой код:

		"gopls": {
		 	"analyses": {
				"composites": false
		 	}
		 },
	*/

	Router.Router.Hidden = map[string]router.RGFunc{
		"/debug":    {debug, "Дебаг группа"},
		"/internal": {internal, "Группа для внутренних запросов"},
	}

	Router.Router.Visible = map[string]router.RGFunc{
		"/activities":        {activities, "Название группы"},
		"/addresses":         {addresses, "Название группы"},
		"/addressessetto":    {addressSetTo, "Название группы"},
		"/auth":              {authActions, "Название группы"},
		"/blacklist":         {blacklist, "Название группы"},
		"/carbrands":         {carbrands, "Название группы"},
		"/clients":           {clients, "Название группы"},
		"/config":            {hConfig, "Название группы"},
		"/destinationpoints": {destinationPoints, "Название группы"},
		"/distribution":      {distribution, "Название группы"},
		"/districts":         {districts, "Название группы"},
		"/driver":            {driver, "Название группы"},
		"/drivergroup":       {driverGroup, "Название группы"},
		"/drivernews":        {driverNews, "Название группы"},
		"/drivers":           {drivers, "Название группы"},
		"/drivertariffs":     {drivertariffs, "Название группы"},
		"/events":            {events, "Название группы"},
		"/features":          {features, "Название группы"},
		"/increasedfare":     {increasedfare, "Название группы"},
		"/info":              {info, "Название группы"},
		"/mailing":           {mailing, "Название группы"},
		"/sources":           {sources, "Название группы"},
		"/taxiparks":         {taxiparks, "Название группы"},
		"/regions":           {regions, "Название группы"},
		"/penalties":         {penalties, "Название группы"},
		"/services":          {services, "Название группы"},
		"/tickets":           {tickets, "Название группы"},
		"/quickmessages":     {quickMessages, "Название группы"},
		"/tariffsurcharges":  {tariffSurcharges, "Название группы"},
		"/photocontrols":     {photoControls, "Название группы"},
		"/stores":            {stores, "Название группы"},
		"/products":          {products, "Название группы"},
		"/tariffs":           {tariffs, "Название группы"},
		"/surge":             {surge, "Название группы"},
		"/prices":            {prices, "Название группы"},
		"/users":             {users, "Название группы"},
	}

	router.ApiVersion = apiversion
	if err := Router.Route(); err != nil {
		log.Fatal(err)
	}

	/*
		/auth/login
		/auth/refresh
		/penalties/zones
		/blacklist/client/isin/:phone
		/blacklist/phone/isin/:phone
	*/

	// Всё что ниже еще предстоит передалать))

	// legacy
	{
		Router.Groups.Public.GET("/clients/isblacklisted/:phone", h.IsTheClientBlacklisted, "Название эндпоинта")
		Router.Groups.Public.GET("/blacklist/isinblacklist/:phone", h.IsPhoneInBlacklist, "Название эндпоинта")
	}

	tempPrivate.GET("/formulatemplatelist", h.FormulaTemplateList)

	// убрать и забыть как страшный сон
	tempPrivate.GET("/options", h.GetOptions)

	// нужно ли?
	tempPrivate.POST("/fuelorder", h.CreateFuelOrder)

	{
		Router.Groups.Public.GET("/orders/current/drivers", h.GetDriversCurrentOrders, "")

		Router.Groups.Public.GET("/orders/counter/driver/:driveruuid", h.GetDriverCounterOrder, "Название эндпоинта")
		Router.Groups.Public.GET("/orders/existcounterorder/:driveruuid", h.GetExistenceCounterOrder, "Название эндпоинта")
		Router.Groups.Public.POST("/orders/existcounterorder/:driveruuid", h.MakeCounterOrderCurrent, "Название эндпоинта")

		Router.Groups.Public.POST("/orders/counterordermarker/:orderuuid", h.SetCounterOrderMarkerForOrder, "Название эндпоинта")
		Router.Groups.Public.GET("/orders/counterordermarker/:orderuuid", h.GetCounterOrderMarkerForOrder, "Название эндпоинта")
	}

	Router.Groups.Public.POST("/orders/tariff", h.GetTariff, "Название эндпоинта")
	Router.Groups.Public.POST("/orders/tariffs", h.GetTariffs, "Название эндпоинта")

	Router.Groups.Public.GET("/order/:uuid", h.GetOrder, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/:uuid", h.UpdateOrder, "Название эндпоинта")
	Router.Groups.Public.GET("/order/delivery/:uuid", h.GetOrderByShortUUID, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/mark/delivery/:uuid", h.MarkOrderAsDelivered, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/confirm/:uuid", h.ConfirmOrder, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/estimatedelivery/:uuid", h.EstimateDelivery, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/finishdelivery/:uuid", h.FinishDelivery, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/numberinstore/:uuid", h.SetOrderNumber, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/important/unmark/:uuid", h.UnmarkOrderAsImportant, "Название эндпоинта")
	Router.Groups.Private.PUT("/order/readyinform/:uuid", h.InformDriverAboutOrderReady, "Название эндпоинта")

	Router.Groups.Public.PUT("/ordertariff/:uuid", h.UpdateOrderTariff, "Название эндпоинта")
	Router.Groups.Public.GET("/ordertariff/:uuid", h.GetOrderTariffByUUID, "Название эндпоинта")

	Router.Groups.Private.POST("/driverway", h.DriverWay, "Название эндпоинта")
	Router.Groups.Public.POST("/drivers/way/pave", h.GetRouteWay, "Название эндпоинта")

	Router.Groups.Private.POST("/orders", h.CreateOrder, "Название эндпоинта")
	Router.Groups.Private.GET("/orders", h.OrdersList, "Название эндпоинта")
	Router.Groups.Private.PUT("/orders/cancel/:uuid", h.CancelOrder, "Название эндпоинта")
	Router.Groups.Private.PUT("/orders/redistribution/:uuid", h.ReDistOrder, "Название эндпоинта")
	Router.Groups.Private.DELETE("/orders/:uuid", h.DeleteOrder, "Название эндпоинта")
	Router.Groups.Private.POST("/orders/filter", h.OrdersListWithOptions, "Название эндпоинта")
	Router.Groups.Private.GET("/orders/driver/:uuid", h.GetOrdersByDriver, "Название эндпоинта")

	Router.Groups.Public.GET("/orders/currentstate/:uuid", h.GetOrderCurrentState, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/uuidbystate/:state", h.UUIDByState, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/data/bystate/:state", h.OrderStateDataByState, "Название эндпоинта")
	Router.Groups.Public.POST("/orders/data/bystates", h.OrderStateDataByStates, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/current/driver/:driveruuid", h.DriverCurrentOrder, "Название эндпоинта")
	Router.Groups.Public.POST("/orders/set/state", h.SetOrderStatus, "Название эндпоинта")
	Router.Groups.Public.POST("/orders/from/client", h.CreateOrderFromClient, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/undelivered/", h.GetUnDeliveredOrdersUUID, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/withclienthistory/:uuid", h.GetOrderWithClHistory, "Название эндпоинта")
	Router.Groups.Public.GET("/orders/current/client/:clientuuid", h.GetClientOrderData, "Название эндпоинта")

	Router.Groups.Public.GET("/forbiddendriverapps", h.GetForbiddenDriverApps, "Название эндпоинта")
	Router.Groups.Public.POST("/forbiddendriverapps", h.SetForbiddenDriverApps, "Название эндпоинта")

	{ // эндпоинты общего назначения
		Router.Groups.Public.POST("/difragpictures", hndl.DifragPicture, "Название эндпоинта") // переконвертация изображений относительно
		Router.Groups.Public.POST("/takepicture", hndl.TakePicture, "Название эндпоинта")
		Router.Groups.Public.POST("/findaddress", h.FindAddress, "Название эндпоинта") // получение ближайшего адреса по координатам
	}
	{ // работа с конфигами
		Router.Groups.Public.GET("/configreinitialization", h.ConfigReinitialization, "Название эндпоинта")

		Router.Groups.Public.GET("/referralsystemparams", h.GetReferralSystemParams, "Название эндпоинта")
		Router.Groups.Public.PUT("/referralsystemparams", h.UpdateReferralSystemParams, "Название эндпоинта")

		Router.Groups.Public.GET("/godmode", hndl.GetGodModeParams, "Название эндпоинта")
		Router.Groups.Public.PUT("/godmode", hndl.SetGodModeParams, "Название эндпоинта")
	}

	return e
}
