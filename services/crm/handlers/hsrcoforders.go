package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// CreateSourceOfOrder godoc
func CreateSourceOfOrder(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "create source of orders",
		"user_uuid": userUUIDFromJWT(c),
	})

	sourceOfOrders := new(models.SourceOfOrdersCRM)
	if err := c.Bind(sourceOfOrders); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := sourceOfOrders.Create(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	//создание SpecifiInfo
	specInfo := new(models.SpecificInfo)
	specInfo.CompanyName = sourceOfOrders.Name
	specInfo.SourceID = sourceOfOrders.UUID
	if err := specInfo.Insert(c.Request().Context()); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	logs.Eloger.WithField("source_uuid", sourceOfOrders.UUID).Info("new source of orders created")
	logs.Eloger.WithField("company_name", specInfo.CompanyName).Info("new specific info created")

	return c.JSON(http.StatusOK, sourceOfOrders)
}

// SourceOfOrdersList godoc
func SourceOfOrdersList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "source of orders list",
	})

	filter := new(models.SourceOfOrdersFilter)
	if err := c.Bind(filter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if len(filter.TaxiparkUUIDs) == 0 {
		filter.TaxiparkUUIDs = taxiParkUUIDFromJWT(c)
	}

	sources, err := filter.SourceOfOrdersList()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, sources)
}

// GetSourceOfOrder godoc
func GetSourceOfOrder(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get source of order",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("source_uuid", uuid)

	source, err := models.GetSourceOfOrdersByUUID(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	if err := source.FillServices(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, source)
}

// UpdateSourceOfOrder godoc
func UpdateSourceOfOrder(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "get source of order",
		"user_uuid": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("source_uuid", uuid)

	source := new(models.SourceOfOrdersCRM)
	if err := c.Bind(source); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	source.UUID = uuid

	if err := source.Update(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	log.Infof("source of order %s was updated", uuid)

	return c.JSON(http.StatusOK, source)
}

// DeleteSourceOfOrders godoc
func DeleteSourceOfOrders(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "get source of order",
		"user_uuid": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("source_uuid", uuid)

	source := new(models.SourceOfOrdersCRM)
	source.UUID = uuid

	if err := source.SetDeleted(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Infof("source of order %s was deleted", uuid)

	return c.JSON(http.StatusOK, logs.OutputRestOK("Source of order deleted"))
}

func GetSourceOfOrderServices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get source of order services",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("source_uuid", uuid)

	source, err := models.GetSourceOfOrdersByUUID(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	if err := source.FillServices(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, source.Services)
}

func GetSourceOfOrderUUID(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting source of order uuid",
	})

	source := new(models.SrcOfOrdersForwarding)
	if err := c.Bind(source); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	log.WithFields(logrus.Fields{
		"source_key": source.SourceKey,
		"region_id":  source.RegionID,
	})

	if err := source.FillSourceOfOrdersUUID(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, source)
}

func GetAsteriskHelloBySourceName(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting asterisk hello by source of orders name",
	})

	hello, err := models.GetAsteriskHelloBySourceName(c.Param("name"))
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, hello)
}
