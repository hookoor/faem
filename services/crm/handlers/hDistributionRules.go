package handlers

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"net/http"
	"strconv"
)

func GetDistributionRulesByIDs(c echo.Context) error {
	log := logs.Eloger.WithField("event", "get distribution rules")

	var args struct {
		IDs []int `json:"ids"`
	}

	err := c.Bind(&args)
	if err != nil {
		log.WithError(err).Error(bindingDataErrorMsg)
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	ctx := c.Request().Context()
	rules, err := models.GetDistributionRulesByIDs(ctx, args.IDs)
	if err != nil {
		msg := "failed to fetch distribution rules from db"
		log.WithError(err).Error(msg)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rules)
}

func GetClientDistributionRule(c echo.Context) error {
	log := logs.Eloger.WithField("event", "get client distribution rules")

	ctx := c.Request().Context()
	rule, err := models.GetClientDistributionRule(ctx)
	if err != nil {
		msg := "failed to fetch client distributions rule from db"
		log.WithError(err).Error(msg)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rule)
}

func GetDistributionRule(c echo.Context) error {
	log := logs.Eloger.WithField("event", "GetDistributionRule")
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	rule, err := models.DistributionRuleByID(ctx, id)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		res := logs.OutputRestError(ErrModelFunc.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rule)
}

func UpdateDistributionRule(c echo.Context) error {
	log := logs.Eloger.WithField("event", "UpdateDistributionRule")
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	var rule models.DistributionRule
	if err := c.Bind(&rule); err != nil {
		log.WithError(ErrBindingData).Error(err)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}
	rule.ID = id

	if err := rule.Update(ctx); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		res := logs.OutputRestError(ErrModelFunc.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rule)
}
