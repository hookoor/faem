package handlers

import (
	"net/http"
	"strings"

	"github.com/pkg/errors"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

// UsersListWithOptions godoc
func UsersListWithOptions(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting users list by filter",
		"userUUID": userUUIDFromJWT(c),
	})

	pager, err := getPager(c)
	if err != nil {
		msg := "error getting pager"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	criteria := models.UsersFilterCriteria{
		Pager: pager,
		Name:  c.QueryParam("name"),
		Role:  c.QueryParam("role"),
	}

	if taxiparks := c.QueryParam("taxi_parks"); taxiparks != "" {
		criteria.TaxiParksUUID = strings.Split(taxiparks, ",")
		criteria.TPFilterApplied = true
	}
	criteria.TaxiParksUUID = getAllowedUUIDs(taxiParkUUIDFromJWT(c), criteria.TaxiParksUUID)

	userRole, err := userRoleFromJWT(c)
	if err != nil {
		msg := "error getting user's role from JWT"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	users, recordsNumber, err := criteria.GetByOptions(userRole)
	if err != nil {
		msg := "error getting users"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: users,
		Count:   recordsNumber,
	})
}

// CreateUser godoc
func CreateUser(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "updating user",
		"userUUID": userUUID,
	})

	usient := new(models.UsersCRM)
	if err := c.Bind(usient); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	nUser, err := usient.Create()
	if err != nil {
		msg := "creating error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	sendData := structures.WrapperOperator{
		OperatorUUID: userUUID,
		Users:        nUser.Users,
	}
	err = rabsender.SendJSONByAction(rabsender.AcOperatorData, sendData, rabbit.NewKey)
	if err != nil {
		msg := "sending to rabbit error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	log.WithField("newUserUUID", nUser.UUID).Info("new user created")

	models.FillTaxiParkData(nUser)

	return c.JSON(http.StatusOK, nUser)
}

// UsersList godoc
func UsersList(c echo.Context) error {
	usersTaxiParks := taxiParkUUIDFromJWT(c)
	users, err := models.UsersList(usersTaxiParks)
	if err != nil {
		msg := "error getting users list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting users list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	models.FillTaxiParksData(GetODArray(users))

	return c.JSON(http.StatusOK, users)
}

// GetUser godoc
func GetUser(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting user",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error(errors.New(msg))
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errors.New(msg)))
	}
	log = log.WithField("uuid", uuid)

	user := new(models.UsersCRM)
	if err := models.GetByUUID(uuid, user); err != nil {
		msg := "getting user by uuid error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	models.FillTaxiParkData(user)

	return c.JSON(http.StatusOK, user)
}

// UpdateUser godoc
func UpdateUser(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "updating user",
		"userUUID": userUUID,
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errors.New(msg)))
	}
	log = log.WithField("updatedUserUUID", uuid)

	usient := new(models.UsersCRM)
	if err := c.Bind(usient); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	uUser, err := usient.Update(uuid)
	if err != nil {
		msg := "update error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	log.Infof("user %s was updated", uuid)

	sendData := structures.WrapperOperator{
		OperatorUUID: userUUID,
		Users:        uUser.Users,
	}
	err = rabsender.SendJSONByAction(rabsender.AcOperatorData, sendData, rabbit.UpdateKey)
	if err != nil {
		msg := "sending to rabbit error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	models.FillTaxiParkData(uUser)

	return c.JSON(http.StatusOK, uUser)
}

// DeleteUser godoc
func DeleteUser(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "deleting user",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errors.New(msg)))
	}
	log = log.WithField("removableUserUUID", uuid)

	user := new(models.UsersCRM)
	user.UUID = uuid
	if err := user.SetDeleted(); err != nil {
		msg := "deleting error"
		log.WithField("reason", msg).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errors.New(msg)))
	}
	log.Warn("user was deleted")

	return c.JSON(http.StatusOK, logs.OutputRestOK("User Deleted"))
}
