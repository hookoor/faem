package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

func GetAgregatedPricesList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting agregated prices",
		"userUUID": userUUIDFromJWT(c)})
	prices, err := models.GetAgregatedPrices(c.Request().Context())
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error getting agregated prices list from db.",
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}
	return c.JSON(http.StatusOK, prices)
}

func GetAgregatedPricesListByFilter(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting agregated prices",
		"userUUID": userUUIDFromJWT(c)})

	var (
		prices   []models.AgregatedPrices
		criteria models.AgregatedPricesCriteria
		err      error
		userUUID = userUUIDFromJWT(c)
	)
	criteria.MinDate, criteria.MaxDate, err = getTimeParams(c)
	if err != nil {
		msg := "Error getting time params"
		res := logs.OutputRestError(msg, err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting agregated prices list by filter",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	criteria.Pager, err = getPager(c)
	if err != nil {
		msg := "Error getting pager"
		res := logs.OutputRestError(msg, err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting orders list by filter",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	prices, err = criteria.GetAgregatedPricesByFilter(c.Request().Context())
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error getting agregated prices list from db.",
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	return c.JSON(http.StatusOK, prices)
}

func InsertAgregatedPricesList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "inserting agregated prices",
		"userUUID": userUUIDFromJWT(c)})

	pricesArr := make([]models.AgregatedPrices, 0)
	if err := c.Bind(&pricesArr); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error binding agregated prices list into structure.",
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	if err := models.InsertAgregatedPrices(c.Request().Context(), pricesArr); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error inserting agregated prices list into db.",
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}
	return c.JSON(http.StatusOK, "done")

}

func DeleteAgregatedPrices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "deleting agregated prices",
		"userUUID": userUUIDFromJWT(c)})
	var prices models.AgregatedPrices
	prices.UUID = c.Param("uuid")
	err := prices.SetAgregatedPriceDeleted()
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error getting agregated prices list.",
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}
	return c.JSON(http.StatusOK, prices)
}
