package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabhandler"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// GetEventsByOptions godoc
func GetEventsByOptions(c echo.Context) error {
	var (
		cr              rabhandler.EventsCriteria
		err             error
		eventsWithCount struct {
			Events []rabhandler.Events `json:"events"`
			Count  int                 `json:"records_count"`
		}
	)
	userUUID, memberType, err := memberUUIDFromJWT(c)
	if err != nil {
		msg := "Error parce jwt"
		res := logs.OutputRestError(msg, err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting events list by filter",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	cr.MinDate, cr.MaxDate, err = getTimeParams(c)
	if err != nil {
		msg := "Error getting time params"
		res := logs.OutputRestError(msg, err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting events list by filter",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	cr.ClientUUID = c.QueryParam("clientuuid")
	cr.DriverUUID = c.QueryParam("driveruuid")
	if memberType == structures.DriverMember {
		cr.DriverUUID = userUUID
	}
	cr.OrderUUID = c.QueryParam("orderuuid")
	cr.TicketUUID = c.QueryParam("ticketuuid")
	cr.OperatorUUID = c.QueryParam("operatoruuid")
	myCallParam := c.QueryParam("mycallsonly")

	if myCallParam == "true" {
		exten, err := models.GetUserExten(userUUID)
		if err != nil {
			msg := "error GetUserExten"
			res := logs.OutputRestError(msg, err)
			logs.Eloger.WithFields(logrus.Fields{
				"event":    "getting events list by filter",
				"reason":   msg,
				"userUUID": userUUID,
			}).Error(err)
			return c.JSON(http.StatusBadRequest, res)
		}
		if exten == "" {
			return c.JSON(http.StatusOK, nil)
		}
		cr.Exten = exten
	}
	cr.TelephonyCallerNumber = c.QueryParam("callernumber")

	cr.Pager, err = getPager(c)
	if err != nil {
		msg := fmt.Sprintf("error getting pager")
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting events list by filter",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	cr.StoreUUID = storeUUIDFromJWT(c)
	events, count, err := rabhandler.GetEventsByOptions(cr)
	if err != nil {
		msg := fmt.Sprintf("error getting events by options")
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting events list by filter",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	eventsWithCount.Count = count
	eventsWithCount.Events = events
	return c.JSON(http.StatusOK, eventsWithCount)
}
