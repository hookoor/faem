package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

// CreateNewPushMailing godoc
func CreateNewPushMailing(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":    "creating new push mailing",
		"userUUID": userUUID,
	})

	phMailing := new(models.PushMailingCRM)
	if err := c.Bind(phMailing); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	phMailing.CreatorUUID = userUUID

	// TODO: Возможно сделать отдельные эндпоинты на пуши для водителей и для клиентов
	userRole, err := userRoleFromJWT(c)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	phMailing.TargetsTaxiparks = getAllowedUUIDs(taxiParkUUIDFromJWT(c), phMailing.TargetsTaxiparks)
	regIDs, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	phMailing.TargetsRegions = getAllowedIDs(regIDs, phMailing.TargetsRegions)

	phMailing, err = models.RestrictPushRecepients(phMailing, userRole)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := phMailing.Save(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log = log.WithField("mailing_uuid", phMailing.UUID)

	if err := rabsender.SendJSONByAction(rabsender.AcNewPushMailing, phMailing); err != nil {
		log.WithError(ErrSendingToRabbit).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrSendingToRabbit.Error(), err))
	}
	msg := "Новая push-рассылка создана"
	log.Info(msg)

	return c.JSON(http.StatusOK, logs.OutputRestOK(msg))
}

// GetAllPushMailings godoc
func GetAllPushMailings(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":    "push mailing list",
		"userUUID": userUUIDFromJWT(c),
	})

	phMailings, err := models.PushMailingList()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	models.FillCreatedAt(phMailings...)

	return c.JSON(http.StatusOK, phMailings)
}
