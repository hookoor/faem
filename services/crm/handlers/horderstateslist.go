package handlers

var ()

// TODO все эти методы надо переписать, оператору не нужно добавлять статусы, он может только
// получить список доступных. Вообще конечно можно забирать статусы из конфига
// Статусы не имеет смысла создавать еще и потому что все переходы статусов нужно записывать

// TODO сделать метод которвый возвращает список статусов заказа

// // CreateOrderState godoc
// func CreateOrderState(c echo.Context) error {

// 	var orderstate models.OrderStateCRM
// 	err := c.Bind(&orderstate)
// 	if err != nil {
// 		res := logs.OutputRestError("error binding data [OrderState]", err)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	nOrderState, err := orderstate.Create()
// 	if err != nil {
// 		res := logs.OutputRestError("error creating [OrderState]", err)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	msg := fmt.Sprintf("New orderstate ID=%v, Name=%s", nOrderState.ID, nOrderState.Name)
// 	uid := userIDFromJWT(c)
// 	logs.OutputEvent("OrderState created", msg, uid)

// 	return c.JSON(http.StatusOK, nOrderState)
// }

// // OrderStatesList godoc
// func OrderStatesList(c echo.Context) error {
// 	var orderstate []models.OrderStateCRM
// 	orderstate, err := models.OrderStatesList()
// 	if err != nil {
// 		cl := fmt.Sprintf("Not found by orderstates")
// 		res := logs.OutputRestError(cl, err, 404)
// 		return c.JSON(http.StatusNotFound, res)
// 	}
// 	return c.JSON(http.StatusOK, orderstate)
// }

// // GetOrderState godoc
// func GetOrderState(c echo.Context) error {
// 	uuid := c.Param("uuid")
// 	if uuid == "" {
// 		res := logs.OutputRestError("OrderState UUID empty", errors.Errorf("empty uuid"))
// 		return c.JSON(http.StatusBadRequest, res)
// 	}
// 	var orderstate models.OrderStateCRM
// 	err := models.GetByUUID(uuid, &orderstate)
// 	if err != nil {
// 		fr := fmt.Sprintf("Not found by UUID [OrderStateUUID=%v]", uuid)
// 		res := logs.OutputRestError(fr, err, 404)
// 		return c.JSON(http.StatusNotFound, res)
// 	}
// 	return c.JSON(http.StatusOK, orderstate)
// }

// // UpdateOrderState godoc
// func UpdateOrderState(c echo.Context) error {
// 	uuid := c.Param("uuid")
// 	if uuid == "" {
// 		res := logs.OutputRestError("OrderState UUID empty", errors.Errorf("empty uuid"))
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	var orderstate models.OrderStateCRM
// 	if err := c.Bind(&orderstate); err != nil {
// 		cl := fmt.Sprintf("error binding data [OrderStateID=%v]", orderstate.ID)
// 		res := logs.OutputRestError(cl, err)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	uOrderState, err := orderstate.Update(uuid)
// 	if err != nil {
// 		cl := fmt.Sprintf("Cant update [OrderStateID=%v]", orderstate.ID)
// 		res := logs.OutputRestError(cl, err)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	msg := fmt.Sprintf("OrderState updated ID=%v", uOrderState.ID)
// 	uid := userIDFromJWT(c)
// 	logs.OutputEvent("OrderState updated", msg, uid)

// 	return c.JSON(http.StatusOK, uOrderState)
// }

// // DeleteOrderState godoc
// func DeleteOrderState(c echo.Context) error {
// 	uuid := c.Param("uuid")
// 	if uuid == "" {
// 		res := logs.OutputRestError("OrderState UUID empty", errors.Errorf("empty uuid"))
// 		return c.JSON(http.StatusBadRequest, res)
// 	}
// 	var orderstate models.OrderStateCRM
// 	orderstate.UUID = uuid

// 	err := orderstate.SetDeleted()
// 	if err != nil {
// 		res := logs.OutputRestError("Cant delete [OrderState]", err)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}

// 	msg := fmt.Sprintf("OrderState deleted ID=%v", orderstate.ID)
// 	uid := userIDFromJWT(c)
// 	logs.OutputEvent("OrderState deleted", msg, uid)

// 	res := logs.OutputRestOK("OrderState Delete")
// 	return c.JSON(http.StatusOK, res)
// }
