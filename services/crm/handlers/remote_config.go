package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

func FetchActivityRate(c echo.Context) error {
	rateItem, err := models.FetchActivityRate(c.Request().Context())
	if err != nil {
		msg := "failed to fetch activity rate"
		logs.LoggerForContext(c.Request().Context()).
			WithField("reason", msg).
			Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, rateItem)
}

func FetchBlockingConfig(c echo.Context) error {
	blocking, err := models.FetchBlockingConfig(c.Request().Context())
	if err != nil {
		msg := "failed to fetch blocking config"
		logs.LoggerForContext(c.Request().Context()).
			WithField("reason", msg).
			Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, blocking)
}

func FetchActivityConfig(c echo.Context) error {
	activities, err := models.FetchActivityConfig(c.Request().Context())
	if err != nil {
		msg := "failed to fetch activity config"
		logs.LoggerForContext(c.Request().Context()).
			WithField("reason", msg).
			Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, activities)
}
