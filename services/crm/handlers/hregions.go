package handlers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// CreateRegion godoc
func CreateRegion(c echo.Context) error {
	creatorUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"events":       "creating region",
		"creator_uuid": creatorUUID,
	})

	region := new(models.RegionCRM)
	if err := c.Bind(region); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := region.Create(creatorUUID); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	logs.Eloger.WithField("region_id", region.ID).Info("new region created")

	return c.JSON(http.StatusOK, region)
}

func GetRegions(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"events": "getting regions list",
	})

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		err := errors.Wrap(err, "failed to get regions ids from JWT")
		log.WithError(err).Error()
		return c.JSON(http.StatusBadRequest, err)
	}

	regions, err := models.GetRegions(c.Request().Context(), allowedRegions...)
	if err != nil {
		err := errors.Wrap(err, "failed to fetch regions from db")
		log.WithError(err).Error()
		return c.JSON(http.StatusNotFound, logs.OutputRestError(err.Error(), err))
	}

	return c.JSON(http.StatusOK, regions)
}

func GetRegionByID(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"events": "getting region by ID",
	})

	regionID, err := strconv.Atoi(c.Param("id"))
	if err != nil || regionID == 0 {
		msg := "empty region :id param or error"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		err := errors.Wrap(err, "failed to get regions ids from JWT")
		log.WithError(err).Error()
		return c.JSON(http.StatusBadRequest, err)
	}
	filteredRegions := getIntSliceIntersection(allowedRegions, []int{regionID})

	regions, err := models.GetRegions(c.Request().Context(), filteredRegions...)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	} else if len(regions) == 0 {
		err = errors.New("no region with such id")
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, regions[0])
}

func GetRegionsNoToken(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"events": "getting regions list",
	})

	regions, err := models.GetRegions(c.Request().Context())
	if err != nil {
		err := errors.Wrap(err, "failed to fetch regions from db")
		log.WithError(err).Error()
		return c.JSON(http.StatusNotFound, logs.OutputRestError(err.Error(), err))
	}

	return c.JSON(http.StatusOK, regions)
}

// UpdateRegion godoc
func UpdateRegion(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "updating region",
		"user_uuid": userUUIDFromJWT(c),
	})

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(ErrEmptyID).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyID.Error(), err))
	}
	log = log.WithField("region_id", id)

	region := new(models.RegionCRM)
	if err := c.Bind(region); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	region.ID = id
	region.UpdatedAt = time.Now()

	if err := region.Update(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Infof("region with id %d was updated", id)

	return c.JSON(http.StatusOK, region)
}

// DeleteRegion godoc
func DeleteRegion(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "get source of order",
		"user_uuid": userUUIDFromJWT(c),
	})

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(ErrEmptyID).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyID.Error(), err))
	}
	log = log.WithField("region_id", id)

	region := new(models.RegionCRM)
	region.ID = id

	if err := region.SetDeleted(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Infof("region with id %d was deleted", id)

	return c.JSON(http.StatusOK, logs.OutputRestOK("Region deleted"))
}
