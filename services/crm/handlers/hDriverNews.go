package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// CreateDriverNews - обработка создания новости
func CreateDriverNews(c echo.Context) error {
	log := logrus.WithFields(logrus.Fields{
		"event":        "create feature",
		"operatorUUID": userUUIDFromJWT(c),
	})

	role, err := userRoleFromJWT(c)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	} else if !(role == "admin" || role == "super_admin" || role == "hr_admin" || role == "owner") {
		return c.JSON(http.StatusForbidden, "Forbidden")
	}

	dn := new(models.DriversNewsCRM)
	if err := c.Bind(dn); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if len(dn.RegionIDs) == 0 {
		err := errors.New("empty region ID slice")
		log.Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(err.Error(), err))
	}

	jwtRegIDs, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	dn.RegionIDs = getIntSliceIntersection(jwtRegIDs, dn.RegionIDs)

	if err := dn.Create(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Info("driver news created successfully")

	return c.JSON(http.StatusOK, dn)
}

// GetDriverNewsList - получение списка новостей
func GetDriverNewsList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting drivers news list",
	})

	filter := new(models.DriverNewsFilter)
	if err := c.Bind(filter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if _, ok := driverUUIDFromJWT(c); !ok {
		jwtRegIDs, err := regionsIDFromJWT(c)
		if err != nil {
			log.WithError(ErrGetAllowedRegions).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
		}
		filter.RegionIDs = getAllowedIDs(jwtRegIDs, filter.RegionIDs)
	}

	if len(filter.TaxiParkUUIDs) == 0 {
		filter.TaxiParkUUIDs = taxiParkUUIDFromJWT(c)
	}

	dnList, err := filter.DriversNewsList()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Info("getting driver news list")

	return c.JSON(http.StatusOK, dnList)
}

// GetDriverNews - возвращает новость по ее uuid
func GetDriverNews(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting drivers news by uuid",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	dn, err := models.DriversNewByUUID(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Info("getting driver news by uuid")

	return c.JSON(http.StatusOK, dn)
}

// UpdateDriverNews - обработка обновления новости
func UpdateDriverNews(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "updating drivers news by uuid",
		"creator_uuid": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	dn := new(models.DriversNewsCRM)
	if err := c.Bind(dn); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	dn.UUID = uuid

	if len(dn.RegionIDs) != 0 {
		jwtRegIDs, err := regionsIDFromJWT(c)
		if err != nil {
			log.WithError(ErrGetAllowedRegions).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
		}

		dn.RegionIDs = getAllowedIDs(jwtRegIDs, dn.RegionIDs)
	}

	if err := dn.Update(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log.Infof("driver news %s updated successfully", uuid)

	return c.JSON(http.StatusOK, dn)
}

// DeleteDriverNews - обработка удаления новости
func DeleteDriverNews(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "updating drivers news by uuid",
		"deletor_uuid": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	dn := new(models.DriversNewsCRM)
	dn.UUID = uuid

	if err := dn.SetDeleted(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	msg := "driver news deleted " + uuid
	log.Info(msg)

	return c.JSON(http.StatusOK, logs.OutputRestOK(msg))
}
