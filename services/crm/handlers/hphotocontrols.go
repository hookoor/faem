package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/pkg/errors"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

type PhotoControlsWithCount struct {
	PhotoControls []models.PhotoControlCRM `json:"photo_controls"`
	Count         int                      `json:"records_count"`
}

// UpdatePhotoControl godoc
func UpdatePhotoControl(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "getting photocontrol list by filter"})
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if uuid == "" {
		fr := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason":   fr,
			"userUUID": userUUID,
		}).Error()
		res := logs.OutputRestError(fr, errors.New(""), 404)
		return c.JSON(http.StatusNotFound, res)
	}
	var phCon models.PhotoControlCRM
	err := c.Bind(&phCon)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = phCon.Update(uuid)
	if err != nil {
		msg := "ошибка обновления данных о фотоконтроле"
		log.WithFields(logrus.Fields{
			"reason":      msg,
			"userUUID":    userUUID,
			"controlUUID": phCon.ControlID,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	go func() {
		err = rabsender.SendJSONByAction(rabsender.AcPhotoControl, phCon)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"reason": "Error sending data to broker",
			}).Error(err)
		}
	}()
	msg := "Данные о фотоконтроле обновлены"
	logs.Eloger.WithFields(logrus.Fields{
		"userUUID":    userUUID,
		"controlUUID": phCon.ControlID,
	}).Info(msg)
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})

}

// GetPhotoControlByUUID godoc
func GetPhotoControlByUUID(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "getting photocontrol by uuid"})
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if uuid == "" {
		fr := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason":   fr,
			"userUUID": userUUID,
		}).Error()
		res := logs.OutputRestError(fr, errors.New(""), 404)
		return c.JSON(http.StatusNotFound, res)
	}

	phCon, err := models.GetPhotoControlByUUID(uuid)
	if err != nil {
		msg := "ошибка получения фотоконтроля"
		log.WithFields(logrus.Fields{
			"reason":      msg,
			"userUUID":    userUUID,
			"controlUUID": phCon.ControlID,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	phCon.FillUnixField()
	return c.JSON(http.StatusOK, phCon)

}

// PhotoControlsListWithOptions godoc
func PhotoControlsListWithOptions(c echo.Context) error {
	var (
		criteria models.PCCriteria
		err      error
	)
	userUUID := userUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "getting photocontrol list by filter"})
	criteria.MinDate, criteria.MaxDate, err = getTimeParams(c)
	if err != nil {
		msg := "Error getting time params"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	criteria.Pager, err = getPager(c)
	if err != nil {
		msg := "Error getting pager"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	activeParam := c.QueryParam("active")
	if activeParam != "" {
		criteria.Active, err = strconv.ParseBool(activeParam)
		if err != nil {
			fr := fmt.Sprintf("Error active param Parsing")
			log.WithFields(logrus.Fields{
				"reason":   fr,
				"userUUID": userUUID,
			}).Error(err)
			res := logs.OutputRestError(fr, err, 404)
			return c.JSON(http.StatusNotFound, res)
		}
	}

	criteria.DriverUUID = c.QueryParam("driver_uuid")

	pControls, recordsNumber, err := criteria.PhotoControlListWithOptions()
	if err != nil {
		fr := fmt.Sprintf("Not found by photocontrols")
		log.WithFields(logrus.Fields{
			"reason":   fr,
			"userUUID": userUUID,
		}).Error(err)
		res := logs.OutputRestError(fr, err, 404)
		return c.JSON(http.StatusNotFound, res)
	}
	for i := range pControls {
		pControls[i].FillUnixField()
	}
	return c.JSON(http.StatusOK, PhotoControlsWithCount{
		PhotoControls: pControls,
		Count:         recordsNumber,
	})
}
