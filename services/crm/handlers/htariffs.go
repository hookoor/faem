package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

// TariffRulesList godoc
func TariffRulesList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "tariff rules list",
		"userUUID": userUUIDFromJWT(c),
	})

	trFilter := new(orders.TariffsCriteria)
	if err := c.Bind(trFilter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	regIds, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	trFilter.RegionIDs = getAllowedIDs(regIds, trFilter.RegionIDs)

	result, err := trFilter.GetAllTariffRules()
	if err != nil {
		msg := "error getting tariff rules from db"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	for i := range result {
		result[i].FillUnixFields()
	}

	return c.JSON(http.StatusOK, result)
}

// GetTariffRuleByUUID godoc
func GetTariffRuleByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "get tariff rule",
		"userUUID": userUUIDFromJWT(c),
		"uuid":     uuid,
	})

	res, err := orders.GetTariffRuleByUUID(uuid)
	if err != nil {
		msg := modelFuncErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	res.FillUnixFields()

	return c.JSON(http.StatusOK, res)
}

// CreateTariffRule godoc
func CreateTariffRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create tariff rule",
		"userUUID": userUUIDFromJWT(c),
	})

	trule := new(orders.TariffRules)
	if err := c.Bind(trule); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if err := trule.Create(); err != nil {
		msg := "error creating tariff rule"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	trule.FillUnixFields()

	return c.JSON(http.StatusOK, trule)
}

// UpdateTariffRule godoc
func UpdateTariffRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff rule",
		"userUUID": userUUIDFromJWT(c),
		"uuid":     c.Param("uuid"),
	})

	trule := new(orders.TariffRules)
	if err := c.Bind(trule); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if err := trule.Update(); err != nil {
		msg := "error update"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	trule.FillUnixFields()

	return c.JSON(http.StatusOK, trule)
}

// SetTariffRuleDeleted godoc
func SetTariffRuleDeleted(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "delete tariff rule",
		"userUUID": userUUIDFromJWT(c),
	})

	input := new(struct {
		UUID         string `json:"uuid"`
		DeletePrices bool   `json:"delete_prices"` //удалить ли связанные с правилом ценовые параметры
	})
	if err := c.Bind(input); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = logs.Eloger.WithFields(logrus.Fields{
		"uuid":         input.UUID,
		"deletePrices": input.DeletePrices,
	})

	if err := orders.SetTariffRuleDeleted(input.UUID, input.DeletePrices); err != nil {
		msg := "error delete"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

/*

	PRICES

*/

// GetTariffPriceByUUID godoc
func GetTariffPriceByUUID(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff price",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	tp := orders.TariffPrices{UUID: uuid}
	if err := tp.GetTariffPriceByUUID(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, tp)
}

// TariffPriceList godoc
func TariffPriceList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "tariff prices list",
		"userUUID": userUUIDFromJWT(c),
	})

	filter := new(orders.CriteriaTariffPrices)
	if err := c.Bind(filter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		msg := "error getting regionsID from JWT"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	filter.RegionIDs = getAllowedIDs(allowedRegions, filter.RegionIDs)

	result, err := filter.GetFilteredTariffPrices()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, result)
}

// TariffPricesTempCRUD - это говно нужно рефакторить, чтобы не нужно было юзать эту структуру
// Нужна, так как кто-то придумал хранить таксометр и тд в мапе
type TariffPricesTempCRUD struct {
	orders.TariffPrices
	// чтобы фронту было легче присылать мапу
	Prices []struct {
		Key   float64 `json:"key"`
		Value float64 `json:"value"`
	} `json:"prices_for_taximeter"`
}

func (tpCRUD *TariffPricesTempCRUD) convert() {
	tpCRUD.Taximeter = make(map[string]float64)
	for _, priceItem := range tpCRUD.Prices {
		tpCRUD.Taximeter[fmt.Sprintf("%.1f", priceItem.Key)] = priceItem.Value
	}
}

// CreateTariffPrices godoc
func CreateTariffPrices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create or update tariff price",
		"userUUID": userUUIDFromJWT(c),
	})

	tpCRUD := new(TariffPricesTempCRUD)
	if err := c.Bind(tpCRUD); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	log = log.WithField("rule_id", tpCRUD.RuleID)

	tpCRUD.convert()

	if err := tpCRUD.Create(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, tpCRUD.TariffPrices)
}

// UpdateTariffPrices godoc
func UpdateTariffPrices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff price",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	tpCRUD := new(TariffPricesTempCRUD)
	if err := c.Bind(tpCRUD); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	log = log.WithField("rule_id", tpCRUD.RuleID)
	tpCRUD.UUID = uuid

	tpCRUD.convert()

	if err := tpCRUD.Update(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, tpCRUD.TariffPrices)
}

// CreateOrUpdateTariffPrices godoc
func SetTariffPriceDeleted(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff price",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	tp := orders.TariffPrices{UUID: uuid}
	if err := tp.Delete(); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, logs.OutputRestOK("tariff price deleted, UUID: "+tp.UUID))
}
