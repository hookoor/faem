package handlers

import (
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"net/http"
)

func GetSpecificInfo(c echo.Context) error {
	var (
		s   models.SpecificInfo
		err error
	)
	s.SourceID = c.Param("source_id")
	logs.Eloger.WithFields(logrus.Fields{
		"event":  "getting specific info",
		"source": s.SourceID,
	})

	if err = s.Get(c.Request().Context()); err != nil {
		logs.Eloger.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, s)
}
func UpdateSpecificInfo(c echo.Context) error {
	var (
		s   models.SpecificInfo
		err error
	)
	s.SourceID = c.Param("source_id")
	logs.Eloger.WithFields(logrus.Fields{
		"event":  "updating specific info",
		"source": s.SourceID,
	})

	if err = c.Bind(&s); err != nil {
		logs.Eloger.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	if err = s.Update(c.Request().Context()); err != nil {
		logs.Eloger.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, s)
}
func InsertSpecificInfo(c echo.Context) error {
	var (
		err error
		s   models.SpecificInfo
	)
	s.SourceID = c.Param("source_id")

	logs.Eloger.WithFields(logrus.Fields{
		"event":  "inserting specific info",
		"source": s.SourceID,
	})

	if err = c.Bind(&s); err != nil {
		logs.Eloger.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	if err = s.Insert(c.Request().Context()); err != nil {
		logs.Eloger.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, s)
}
func DeleteSpecificInfo(c echo.Context) error {
	var (
		err error
		s   models.SpecificInfo
	)
	s.SourceID = c.Param("source_id")

	logs.Eloger.WithFields(logrus.Fields{
		"event":  "deleting specific info",
		"source": s.SourceID,
	})

	if err = s.Delete(c.Request().Context()); err != nil {
		logs.Eloger.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, s)
}
