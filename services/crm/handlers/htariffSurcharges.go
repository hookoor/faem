package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

// GetTariffSurcharges godoc
func GetTariffSurcharges(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting tariff surcharges list",
	})

	filteredRegions, err := getAllowedRegionIDsFromQuery(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}

	result, err := orders.GetTariffSurcharges(filteredRegions)
	if err != nil {
		msg := "ошибка получения записей"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	return c.JSON(http.StatusOK, result)
}

// GetAllTariffSurcharges godoc
func GetTariffSurchargeByUUID(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting tariff surcharge by uuid",
		"uuid":     uuid,
		"userUUID": userUUID,
	})
	if uuid == "" {
		log.WithFields(logrus.Fields{
			"reason": emptyUUIDMsg,
		}).Error("UUID empty in request")
		res := logs.OutputRestError(emptyUUIDMsg, errors.Errorf(""))
		return c.JSON(http.StatusBadRequest, res)
	}
	result, err := orders.GetTariffSurchargeByUUID(uuid)
	if err != nil {
		msg := "ошибка получения записи"
		logs.Eloger.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, result)
}

// CreateTariffSurcharge godoc
func CreateTariffSurcharge(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create tariff surcharge",
		"userUUID": userUUIDFromJWT(c),
	})

	ts := new(orders.TariffSurcharges)
	if err := c.Bind(ts); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := ts.Create(); err != nil {
		msg := "ошибка создания записи"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.Info("Запись создана")

	return c.JSON(http.StatusOK, ts)
}

// SetTariffSurcharge godoc
func SetTariffSurchargeDeleted(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff surcharge by uuid",
		"uuid":     uuid,
		"userUUID": userUUID,
	})

	err := orders.SetTariffSurchargeDeleted(uuid)
	if err != nil {
		msg := "ошибка удаления записи"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	msg := "Запись удалена"
	log.WithFields(logrus.Fields{}).Info(msg)
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})
}

// UpdateTariffSurcharge godoc
func UpdateTariffSurcharge(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update tariff surcharge by uuid",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	ts := new(orders.TariffSurcharges)
	if err := c.Bind(ts); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	ts.UUID = uuid

	if err := ts.Update(); err != nil {
		msg := "ошибка обновления записи"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.Info("Запись обновлена")

	return c.JSON(http.StatusOK, ts)
}
