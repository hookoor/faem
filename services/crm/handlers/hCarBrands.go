package handlers

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
)

// GetAllCarBrands -
func GetAllCarBrands(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logrus.WithFields(logrus.Fields{
		"event":    errpath.Func(),
		"userUUID": userUUID,
	})

	list, err := models.CarBrands{}.GetList()
	if err != nil {
		msg := "error getting car brands list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}

	log.Info("get all car brands")

	return c.JSON(http.StatusOK, list)
}

// SetAllCarBrands -
func SetAllCarBrands(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logrus.WithFields(logrus.Fields{
		"event":    errpath.Func(),
		"userUUID": userUUID,
	})

	brands := new(models.CarBrands)

	if err := c.Bind(brands); err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	nBrand, err := brands.Create()
	if err != nil {
		msg := "create error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("set new car brands done successfully")

	return c.JSON(http.StatusOK, nBrand)
}
