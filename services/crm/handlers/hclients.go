package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

type ControllerCustomers struct {
	dbi *models.DB
}

func NewControllerCustomers(db *models.DB) *ControllerCustomers {
	return &ControllerCustomers{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// ClientsList godoc
func ClientsList(c echo.Context) error {
	// FIXME: Временное решение
	role, err := userRoleFromJWT(c)
	if err != nil || role != "owner" {
		msg := "error getting client list"
		err := errors.Errorf("Только владелец может запрашивать список клиентов")
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "client list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusForbidden)
		return c.JSON(http.StatusForbidden, res)
	}

	clients, err := models.ClientsList()
	if err != nil {
		msg := "error getting client list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "client list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, clients)
}

// GetClient godoc
func GetClient(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "get client",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var client models.ClientCRM
	err := models.GetByUUID(uuid, &client)
	if err != nil {
		msg := "error getting client by uuid"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "get client",
			"uuid":   uuid,
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, client)
}

//Warning:(74, 6) Unused function 'GetRegionClients'
// GetRegionClients godoc
//func GetRegionClients(c echo.Context) error {
//	uuid := c.Param("uuid")
//	if uuid == "" {
//		msg := emptyUUIDMsg
//		logs.Eloger.WithFields(logrus.Fields{
//			"event":  "get client by region",
//			"reason": msg,
//		}).Error()
//		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
//		return c.JSON(http.StatusBadRequest, res)
//	}
//	clients, err := models.ClientsByRegion(uuid)
//	if err != nil {
//		msg := "error getting client by region"
//		logs.Eloger.WithFields(logrus.Fields{
//			"event":  "get client by region",
//			"uuid":   uuid,
//			"reason": msg,
//		}).Error(err)
//		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
//		return c.JSON(http.StatusBadRequest, res)
//	}
//	return c.JSON(http.StatusOK, clients)
//}

// UpdateClient godoc
func UpdateClient(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update client",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var client models.ClientCRM
	if err := c.Bind(&client); err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update client",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	uClient, err := client.Update(uuid)
	if err != nil {
		msg := "error update client"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "update client",
			"clientUUID": uuid,
			"reason":     msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = rabsender.SendJSONByAction(rabsender.AcClientData, uClient, "")
	if err != nil {
		res := logs.OutputRestError("Error sending to Client Backend [Client]", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	userUUID := userUUIDFromJWT(c)
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "update client",
		"clientUUID": uuid,
		"userUUID":   userUUID,
	}).Info("client update")

	return c.JSON(http.StatusOK, uClient)
}

// DeleteClient godoc
func DeleteClient(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "delete client",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var client models.ClientCRM
	client.UUID = uuid

	err := client.SetDeleted()
	if err != nil {
		msg := "error delete client"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "delete client",
			"clientUUID": uuid,
			"reason":     msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = rabsender.SendJSONByAction(rabsender.AcDeleteClient, structures.DeletedObject{UUID: client.UUID})
	if err != nil {
		res := logs.OutputRestError("error sending to RMQ", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	userUUID := userUUIDFromJWT(c)
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "delete client",
		"clientUUID": uuid,
		"userUUID":   userUUID,
	}).Info("client delete")

	res := logs.OutputRestOK("Client Delete")
	return c.JSON(http.StatusOK, res)
}

// IsTheClientBlacklisted -
func IsTheClientBlacklisted(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "check if client is  blacklisted",
	})

	phone := c.Param("phone")
	if phone == "" {
		res := "request parametr phone is empty"
		log.Error(errpath.Errorf(res))
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(res, errors.Errorf(res)))
	}

	var client models.ClientCRM

	is, err := client.IsTheCustomerBlacklisted(phone)
	if err != nil {
		msg := "cant get client"
		logs.Eloger.WithFields(logrus.Fields{
			"client phone": phone,
		}).Error(errpath.Err(err))
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"client phone": phone,
	}).Info("blacklisted", is)

	return c.JSON(http.StatusOK, is)
}

func CheckClientAppVersion(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "check driver current app version",
	})

	req := new(structures.AppVersion)
	if err := c.Bind(req); err != nil {
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	isActual, err := models.IsClientAppActual(req)
	if err != nil {
		msg := "error checking for actuality of client app version"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	msg := "OK"
	if !isActual {
		msg = "Outdated client app version!"
	}

	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})
}

func WhereAmI(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "determining client region",
	})

	coords := new(models.ClientRegion)

	if err := c.Bind(coords); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	if coords.Coordinates == nil {
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), errors.New("не указаны координаты")))
	}

	log.WithFields(logrus.Fields{
		"device_id": coords.DeviceID,
		"coords":    fmt.Sprintf("lat:%v, lon:%v", coords.Coordinates.Lat, coords.Coordinates.Long),
	})

	err := coords.WhereAmI(c.Request().Context())
	if err != nil {
		msg := "error getting closest region"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	return c.JSON(http.StatusOK, coords)
}
