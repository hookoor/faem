package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// AddressSetToController -
type AddressSetToController struct {
	dbi *models.DB
}

// NewAddressSetToController -
func NewAddressSetToController(db *models.DB) *AddressSetToController {
	return &AddressSetToController{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// GetByID -
func (rc *AddressSetToController) GetByID(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "GetByID AddressSet")

	stID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	region, err := rc.dbi.SetToAddressModel.GetByID(ctx, stID)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, region)
}

// GetFilteredList -
func (rc *AddressSetToController) GetFilteredList(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "GetFilteredList AddressSet")

	filter := new(models.SetToAddressFilter)
	if err := c.Bind(filter); err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusNotFound, errpath.Err(err).Error())
	}

	res, err := rc.dbi.SetToAddressModel.GetFilteredList(ctx, filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusNotFound, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, res)
}

// CreateCityRelation -
func (rc *AddressSetToController) Update(c echo.Context) error {
	var err error
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Update AddressSetTo")

	stID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var sta models.SetToAddress

	err = c.Bind(&sta)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	err = rc.dbi.SetToAddressModel.Update(ctx, &sta, stID)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, sta)
}

// Delete -
func (rc *AddressSetToController) Delete(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Delete AddressSet")

	regionID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	if err := rc.dbi.SetToAddressModel.Delete(ctx, regionID); err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	msg := fmt.Sprintf("Address set deleted, id: %d", regionID)
	log.Info(msg)

	return c.JSON(http.StatusOK, logs.OutputRestOK(msg))
}

// CreateCityRelation -
func (rc *AddressSetToController) CreateCityRelation(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "CreateCityRelation")

	var bind struct {
		SetID    *int    `json:"set_id"`
		CityName *string `json:"city_name"`
		Weight   *int    `json:"weight"`
	}

	err := c.Bind(&bind)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	res, err := rc.dbi.SetToAddressModel.CreateCityRelation(ctx, bind.SetID, bind.CityName, bind.Weight)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, res)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
