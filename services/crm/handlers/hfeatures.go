package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// CreateFeature godoc
func CreateFeature(c echo.Context) error {
	var (
		Feature models.FeatureCRM
	)
	userUUID := userUUIDFromJWT(c)
	err := c.Bind(&Feature)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create feature",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	nFeature, err := Feature.Create()
	if err != nil {
		msg := "create error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create feature",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":       "create feature",
		"userUUID":    userUUID,
		"featureUUID": nFeature.UUID,
	}).Info("new feature create successfully")

	err = SendOptionsToClient(constants.MarkerFeatures)
	if err != nil {
		msg := "error send options to broker"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create feature",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
	}

	return c.JSON(http.StatusOK, nFeature)
}

// FeatureList godoc
func FeatureList(c echo.Context) error {
	var Feature []models.FeatureCRM
	Feature, err := models.FeatureList("")
	if err != nil {
		msg := "error getting feature list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting features list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, Feature)
}

// GetFeature godoc
func GetFeature(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting feature by uuid",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var feature models.FeatureCRM
	err := models.GetByUUID(uuid, &feature)
	if err != nil {
		msg := "error getting feature by uuid"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "getting feature by uuid",
			"featureUUID": uuid,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, feature)
}

// UpdateFeature godoc
func UpdateFeature(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update feature",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	userUUID := userUUIDFromJWT(c)
	var Feature models.FeatureCRM
	if err := c.Bind(&Feature); err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "update feature",
			"featureUUID": uuid,
			"userUUID":    userUUID,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	uFeature, err := Feature.Update(uuid)
	if err != nil {
		msg := "error feature update"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "update feature",
			"featureUUID": uuid,
			"userUUID":    userUUID,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = SendOptionsToClient(constants.MarkerFeatures)
	if err != nil {
		msg := "error send options to broker"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "update feature",
			"userUUID":    userUUID,
			"featureUUID": uuid,
			"reason":      msg,
		}).Error(err)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":       "update feature",
		"userUUID":    userUUID,
		"featureUUID": uuid,
	}).Info("feature update done successfully")
	return c.JSON(http.StatusOK, uFeature)
}

// DeleteFeature godoc
func DeleteFeature(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "delete feature",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var Feature models.FeatureCRM
	Feature.UUID = uuid
	userUUID := userUUIDFromJWT(c)
	err := Feature.SetDeleted()
	if err != nil {
		msg := "error delete feature"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "delete feature",
			"userUUID":    userUUID,
			"featureUUID": uuid,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = SendOptionsToClient(constants.MarkerFeatures)
	if err != nil {
		msg := "error send options to broker"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "delete feature",
			"userUUID":    userUUID,
			"featureUUID": uuid,
			"reason":      msg,
		}).Error(err)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":       "delete feature",
		"userUUID":    userUUID,
		"featureUUID": uuid,
	}).Info("feature delete done successfully")
	res := logs.OutputRestOK("Feature Delete")
	return c.JSON(http.StatusOK, res)
}
