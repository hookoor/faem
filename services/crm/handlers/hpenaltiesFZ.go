package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// GetZones godoc
func GetZones(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	pager, err := getPager(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting zones list",
		"userUUID": userUUID,
	})
	if err != nil {
		msg := "Error getting pager"
		res := logs.OutputRestError(msg, err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting orders list by filter",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	result, count, err := models.GetZones(pager)
	if err != nil {
		msg := "error getting zones list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: result,
		Count:   count,
	})
}

// PenaltiesList godoc
func PenaltiesList(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting penalties list",
		"userUUID": userUUID,
	})
	var cr models.ZonesPenaltyFilterCriteria
	err := c.Bind(&cr)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	cr.Pager, err = getPager(c)
	if err != nil {
		msg := "Error getting pager"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	result, count, err := cr.GetPenalties()
	if err != nil {
		msg := "error getting penalties list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	result, err = models.FillPenaltiesZonesNames(result)
	if err != nil {
		msg := "error filling zones names"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: result,
		Count:   count,
	})
}

// CreatePenalty godoc
func DeletePenalty(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "delete zone penalty",
		"userUUID":    userUUID,
		"penaltyUUID": userUUID,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  msg,
		})
	}
	err := models.ZenPenaltyDelete(uuid)
	if err != nil {
		msg := "error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	log.Info("penalty deleted")
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "Запись удалена",
	})
}

// CreatePenalty godoc
func CreatePenalty(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create new penalty",
		"userUUID": userUUID,
	})
	var inPen struct {
		models.ZonesPenalty
		Duplicate bool `json:"duplicate"`
	}
	err := c.Bind(&inPen)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := inPen.Create(inPen.Duplicate)
	if errWL != nil {
		msg := "error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warn(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	log.Info("new penalty created")
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "Запись создана",
	})
}
