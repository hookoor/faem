package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type quickMessagesForFront struct {
	Receiver structures.ChatMembers `json:"receiver"`
	Messages []string               `json:"messages"`
}

// QuickMessagesList godoc
func QuickMessagesList(c echo.Context) error {
	userUUID, member, err := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create new quick message",
		"userUUID": userUUID,
		"member":   member,
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "memberUUIDFromJWT error",
		}).Error(err)
		return c.JSON(http.StatusUnauthorized, structures.ResponseStruct{
			Code: http.StatusUnauthorized,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	messages, err := models.QuickMessagesListByRequesting(member)
	if err != nil {
		msg := "error getting quick messages list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if member == structures.UserCRMMember {
		return c.JSON(http.StatusOK, messages)
	}
	var messagesForFront []quickMessagesForFront
	for _, mess := range messages {
		messagesForFront = appendMessageToReceiver(messagesForFront, mess.Message, mess.Receiver)
	}
	return c.JSON(http.StatusOK, messagesForFront)
}

// CreateQuickMessage godoc
func CreateQuickMessage(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create new quick message",
		"userUUID": userUUID,
	})

	var qMess models.QuickMessages
	err := c.Bind(&qMess)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = qMess.Save()
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error saving new quick message",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, qMess)
}

// SetQuickMessageDeleted godoc
func SetQuickMessageDeleted(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "delete quick message",
		"userUUID": userUUID,
	})
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetQuickMessageDeleted(uuid)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error deleting quick message",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "Запись удалена",
	})
}

// UpdateQuickMessage godoc
func UpdateQuickMessage(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create new quick message",
		"userUUID": userUUID,
	})
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var qMess models.QuickMessages
	err := c.Bind(&qMess)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = qMess.Update(uuid)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error updating quick message",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, qMess)
}

func appendMessageToReceiver(messages []quickMessagesForFront, mess string, res structures.ChatMembers) []quickMessagesForFront {

	messages = appendReceiverIfNotExists(messages, res)
	for i, el := range messages {
		if el.Receiver == res {
			messages[i].Messages = append(messages[i].Messages, mess)
			break
		}
	}
	return messages
}

func appendReceiverIfNotExists(messages []quickMessagesForFront, res structures.ChatMembers) []quickMessagesForFront {
	check := false
	for _, el := range messages {
		if el.Receiver == res {
			check = true
			break
		}
	}
	if !check {
		messages = append(messages, quickMessagesForFront{
			Receiver: res,
		})
	}
	return messages
}
