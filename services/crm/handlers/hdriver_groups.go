package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// CreateDriverGroup -
func CreateDriverGroup(c echo.Context) error {
	log := logrus.WithFields(logrus.Fields{
		"event":    "creating driver group",
		"userUUID": userUUIDFromJWT(c),
	})

	group := new(models.DriverGroup)
	if err := c.Bind(group); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(bindingDataErrorMsg, errpath.Err(err)))
	}

	if err := group.Create(); err != nil {
		log.WithField("reason", "create error").Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("create error", errpath.Err(err)))
	}

	log.WithField("groupUUID", group.UUID).Info("successfully created new driver group")

	return c.JSON(http.StatusOK, group)
}

// GetDriverGroupByUUID -
func GetDriverGroupByUUID(c echo.Context) error {
	uuid := c.Param("uuid")

	// userUUID := userUUIDFromJWT(c)
	userUUID := "none"

	log := logrus.WithFields(logrus.Fields{
		"event":     "getting driver group by uuid",
		"groupUUID": uuid,
		"userUUID":  userUUID,
	})

	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Errorf("empty uuid").Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var group models.DriverGroup
	err := models.GetByUUID(uuid, &group)
	if err != nil {
		msg := "error getting feature by uuid"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, group)
}

// GetDriverGroupList -
func GetDriverGroupList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting driver groups list",
	})

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		msg := "error getting regionsID from JWT"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	groups, err := models.DriverGroup{}.GetGroupsList(allowedRegions...)
	if err != nil {
		msg := "error getting driver groups list"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	return c.JSON(http.StatusOK, groups)
}

// GetDriverGroupListByFilter -
func GetDriverGroupListByFilter(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "GetDriverGroupListByFilter")

	filter := new(models.DriverGroupFilter)
	if err := c.Bind(filter); err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusNotFound, errpath.Err(err).Error())
	}

	regIDs, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	filter.RegionIDs = getAllowedIDs(regIDs, filter.RegionIDs)

	groups, err := models.DriverGroup{}.GetGroupsListByFilter(ctx, filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusNotFound, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, groups)
}

// AddDriversToGroup -
func AddDriversToGroup(c echo.Context) error {
	groupuuid := c.Param("uuid")
	// userUUID := userUUIDFromJWT(c)
	userUUID := "none"

	log := logrus.WithFields(logrus.Fields{
		"event":     "add driver to group",
		"groupUUID": groupuuid,
		"userUUID":  userUUID,
	})

	if groupuuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Errorf("empty uuid").Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	driveruuids := struct {
		Driveruuids []string `json:"drivers_uuids"`
	}{}

	err := c.Bind(&driveruuids)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var group models.DriverGroup
	group.UUID = groupuuid
	existuuids, err := group.AddDrivers(driveruuids.Driveruuids...)
	if err != nil {
		msg := "add driver error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("add drivers to group successfully")

	resp := struct {
		Exist  []structures.DriversDatForGroup `json:"exist_uuids,omitempty"`
		Status string
	}{
		Exist:  existuuids,
		Status: "Ok",
	}

	return c.JSON(http.StatusOK, resp)
}

// DropDriversFromGroup -
func DropDriversFromGroup(c echo.Context) error {
	groupuuid := c.Param("uuid")
	// userUUID := userUUIDFromJWT(c)
	userUUID := "none"

	log := logrus.WithFields(logrus.Fields{
		"event":     "add driver to group",
		"groupUUID": groupuuid,
		"userUUID":  userUUID,
	})

	if groupuuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Errorf("empty uuid").Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	driveruuids := struct {
		Driveruuids []string `json:"drivers_uuids"`
	}{}

	err := c.Bind(&driveruuids)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var group models.DriverGroup
	group.UUID = groupuuid
	notexistuuids, err := group.DropDrivers(driveruuids.Driveruuids...)
	if err != nil {
		msg := "add driver error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("drop drivers from group successfully")

	resp := struct {
		NotExist []structures.DriversDatForGroup `json:"not_exist_uuids,omitempty"`
		Status   string
	}{
		NotExist: notexistuuids,
		Status:   "Ok",
	}

	return c.JSON(http.StatusOK, resp)
}

// DragDriversToGroup -
func DragDriversToGroup(c echo.Context) error {
	groupuuid := c.Param("uuid")
	// userUUID := userUUIDFromJWT(c)
	userUUID := "none"

	log := logrus.WithFields(logrus.Fields{
		"event":     "drag driver to group",
		"groupUUID": groupuuid,
		"userUUID":  userUUID,
	})

	if groupuuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Errorf("empty uuid").Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	driveruuids := struct {
		Driveruuids []string `json:"drivers_uuids"`
	}{}

	err := c.Bind(&driveruuids)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var group models.DriverGroup
	group.UUID = groupuuid
	err = group.DragToGroup(groupuuid, driveruuids.Driveruuids...)
	if err != nil {
		msg := "drag driver error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("drag drivers to group successfully")

	resp := struct {
		Exist  []structures.DriversDatForGroup `json:"exist_uuids,omitempty"`
		Status string
	}{
		Status: "Ok",
	}

	return c.JSON(http.StatusOK, resp)
}

// UpdateDriverGroup -
func UpdateDriverGroup(c echo.Context) error {
	log := logrus.WithFields(logrus.Fields{
		"event":     "update driver group by uuid",
		"user_uuid": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithField("reason", ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("group_uuid", uuid)

	group := new(models.DriverGroup)
	if err := c.Bind(group); err != nil {
		log.WithField("reason", ErrBindingData).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), ErrBindingData))
	}

	if err := group.Update(uuid); err != nil {
		msg := "error driver group update"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errpath.Err(err)))
	}
	log.Info("driver group updated successfully")

	return c.JSON(http.StatusOK, group)
}

// DeleteDriverGroup -
func DeleteDriverGroup(c echo.Context) error {
	uuid := c.Param("uuid")
	// userUUID := userUUIDFromJWT(c)
	userUUID := "none"

	log := logrus.WithFields(logrus.Fields{
		"event":     "delete driver group by uuid",
		"groupUUID": uuid,
		"userUUID":  userUUID,
	})

	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Errorf(errpath.Errorf("empty uuid").Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err := models.DriverGroup{}.SetDeleted(uuid)
	if err != nil {
		msg := "error delete feature"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errpath.Err(err).Error())
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("driver group delete done successfully")

	res := logs.OutputRestOK("driver group delete")
	return c.JSON(http.StatusOK, res)
}

// GetDrvGroupServices - получение сервисов по uuid группы
func GetDrvGroupServices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get driver groups services bu UUID",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	shServices, err := models.GetDrvGroupServices(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, shServices)
}

// GetGroupServices - получение сервисов по uuid группы
func GetDrvGroupTariffs(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get driver groups tariffs bu UUID",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	shTariffs, err := models.GetDrvGroupTariffs(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, shTariffs)
}

func GetDriverGroupWeights(c echo.Context) error {
	var args struct {
		UUID []string `json:"uuid"`
	}

	err := c.Bind(&args)
	if err != nil {
		err := logs.OutputRestError("unable to bind request body", err)
		return c.JSON(http.StatusBadRequest, err)
	}

	weights, err := models.GetDriverGroupWeights(args.UUID)
	if err != nil {
		err := logs.OutputRestError("failed to fetch driver groups from db", err)
		return c.JSON(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, weights)
}
