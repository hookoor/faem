package handlers

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

const (
	uploadToCloudTimeout = 60 * time.Second

	emptyUUIDMsg        = "empty uuid"
	bindingDataErrorMsg = "error binding data"
	modelFuncErrorMsg   = "models function error"
)

// Общие ошибки хэндлеров
var (
	ErrEmptyUUID         = errors.New(emptyUUIDMsg)
	ErrBindingData       = errors.New(bindingDataErrorMsg)
	ErrModelFunc         = errors.New(modelFuncErrorMsg)
	ErrGetJWTValue       = errors.New("error getting value from JWT")
	ErrGetAllowedRegions = errors.New("error getting allowed region IDs from query param region_id")
	ErrEmptyID           = errors.New("error: empty ID param")
	ErrEmptySrcUUID      = errors.New("error: empty source of orders UUID param")
	ErrSendingToRabbit   = errors.New("error: cannot send data to the Rabbit")
)

// Handler -
type Handler struct {
	Server                *models.Server
	RegionsController     IRegionsController
	AddressSetTo          IAddressSetToController
	FeaturesSetForRegion  *ControllerFeaturesSetForRegion
	FeaturesSetForService *ControllerFeaturesSetForService
	Drivers               *ControllerDrivers
	Customers             *ControllerCustomers
}

// New -
func New(gcs *cloudstorage.GCStorage, conn *pg.DB, dbHook *postgre.DBQueryTraceHook) *Handler {

	dbInterface := models.DB{
		RegiosModel:                        models.InitRegionModel(conn, dbHook),
		DebugModel:                         models.InitDebugModel(conn, dbHook),
		GodModeModel:                       models.InitGodMode(conn, dbHook),
		CounterOrderSwitch:                 models.InitCounterOrderSwitch(conn, dbHook),
		SetToAddressModel:                  models.NewSetToAddressModel(conn, dbHook),
		FeaturesSetForRegionModel:          models.NewModelFeaturesSetForRegion(conn, dbHook),
		FeaturesSetForRegionalServiceModel: models.NewModelFeaturesSetForService(conn, dbHook),
		DriverGroupModel:                   models.NewModelDriverGroup(conn),

		DriverInteraction: models.NewDriverInteraction(conn),
	}

	dbInterface.DriverInteraction.Driver.UploadBDInterface(&dbInterface)

	return &Handler{
		Server:                models.NewServer(gcs, dbInterface),
		RegionsController:     NewRegionsController(&dbInterface),
		AddressSetTo:          NewAddressSetToController(&dbInterface),
		FeaturesSetForRegion:  NewControllerFeaturesSetForRegion(&dbInterface),
		FeaturesSetForService: NewControllerFeaturesSetForService(&dbInterface),
		Drivers:               NewControllerDrivers(&dbInterface),
		Customers:             NewControllerCustomers(&dbInterface),
	}
}

// IRegionsController -
type IRegionsController interface {
	GetByID(c echo.Context) error
	GetDriversTariffs(c echo.Context) error
}

// IAddressSetToController -
type IAddressSetToController interface {
	GetByID(c echo.Context) error
	GetFilteredList(c echo.Context) error
	Update(c echo.Context) error
	Delete(c echo.Context) error

	IAddressSetToControllerTemporary
}

// IAddressSetToControllerTemporary - временные методы, заменить на более правильное взаимодействие с сущностью
type IAddressSetToControllerTemporary interface {
	// CreateCityRelation -
	CreateCityRelation(c echo.Context) error
}

func claimsFromJWT(c echo.Context) (jwt.MapClaims, error) {
	user, ok := c.Get("user").(*jwt.Token)
	if !ok {
		return nil, errors.Errorf("cannot get jwt string from echo storage")
	}

	claims, ok := user.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.Errorf("cannot assert user.Claims to jwt mapclaims")
	}

	return claims, nil
}

// UserIDFromJWT return user ID
func userIDFromJWT(c echo.Context) int {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return 0
	}
	userID, ok := claims["user_id"].(float64)
	if !ok {
		return 0
	}
	return int(userID)
}

func userUUIDFromJWT(c echo.Context) string {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return ""
	}
	userUUID, ok := claims["user_uuid"].(string)
	if !ok {
		return ""
	}
	return userUUID
}

func loginFromJWT(c echo.Context) string {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return ""
	}
	login, ok := claims["login"].(string)
	if !ok {
		return ""
	}
	return login
}

func userRoleFromJWT(c echo.Context) (string, error) {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return "", err
	}
	userRole, ok := claims["role"].(string)
	if !ok {
		return "", errors.Errorf("cannot find/assert role from jwt claims")
	}
	return userRole, nil
}

func taxiParkUUIDFromJWT(c echo.Context) []string {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return nil
	}

	uuids, check := claims["taxi_parks_uuid"].([]interface{})
	if !check {
		return nil
	}

	uuidsStr := make([]string, len(uuids))
	for i, val := range uuids {
		uuid, ok := val.(string)
		if !ok {
			continue
		}
		uuidsStr[i] = uuid
	}

	return uuidsStr
}

func regionsIDFromJWT(c echo.Context) ([]int, error) {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return nil, err
	}

	ids, check := claims["regions_id"].([]interface{})
	if !check {
		return nil, errors.New("cannot assert regions_id from JWT")
	}

	idsInt := make([]int, len(ids))
	for i, val := range ids {
		id, ok := val.(float64)
		if !ok {
			continue
		}
		idsInt[i] = int(id)
	}

	return idsInt, nil
}

// ЖДЁМ ДЖЕНЕРИКОВ!!!
func getAllowedIDs(jwtIDs, fltIDs []int) []int {
	if len(jwtIDs) != 0 && len(fltIDs) == 0 {
		return jwtIDs
	}

	return getIntSliceIntersection(jwtIDs, fltIDs)
}

func getIntSliceIntersection(s1, s2 []int) []int {
	temp := make(map[int]struct{})
	inter := make([]int, 0)
	for _, tp := range s1 {
		temp[tp] = struct{}{}
	}

	for i, tp := range s2 {
		if _, ok := temp[tp]; ok {
			inter = append(inter, s2[i])
		}

	}

	return inter
}

// getAllowedIDs - Если нет филтра по айдишникам Возвращает id из JWT
func getAllowedUUIDs(jwtUUIDs, fltUUIDs []string) []string {
	if len(jwtUUIDs) != 0 && len(fltUUIDs) == 0 {
		return jwtUUIDs
	}

	return getStringSliceIntersection(jwtUUIDs, fltUUIDs)
}

func getStringSliceIntersection(s1, s2 []string) []string {
	temp := make(map[string]struct{})
	inter := make([]string, 0)
	for _, tp := range s1 {
		temp[tp] = struct{}{}
	}

	for i, tp := range s2 {
		if _, ok := temp[tp]; ok {
			inter = append(inter, s2[i])
		}

	}

	return inter
}

// GetODArray converts an array of types to an array of TaxiParkDependent if the type implements an interface
func GetODArray(in interface{}) []models.TaxiParkDependent {
	var res []models.TaxiParkDependent
	switch arrType := in.(type) {
	case []models.DriverCRM:
		for i := range arrType {
			res = append(res, &arrType[i])
		}
	case []models.UsersCRM:
		for i := range arrType {
			res = append(res, &arrType[i])
		}
	case []orders.OrderCRM:
		for i := range arrType {
			res = append(res, &arrType[i])
		}
	}
	return res
}

func storeUUIDFromJWT(c echo.Context) string {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return ""
	}
	storeUUID, ok := claims["store_uuid"].(string)
	if !ok {
		return ""
	}
	return storeUUID
}

func clientPhoneFromJWT(c echo.Context) (string, error) {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return "", err
	}
	userUUID, check := claims["phone"].(string)
	if !check {
		return "", errors.Errorf("invalid phone field in jwt")
	}
	return userUUID, nil
}

func driverUUIDFromJWT(c echo.Context) (string, bool) {
	claims, err := claimsFromJWT(c)
	if err != nil {
		return "", false
	}

	driverUUID, check := claims["driver_uuid"].(string)
	if !check {
		return "", false
	}

	return driverUUID, true
}

/* getAllowedRegionIDsFromQuery - Берёт из c.QueryParam("region_ids") слайс регионов
Затем сверяет его с разрешёнными регионами по JWT и возвращает слайс только тех регионов, который входят и в фильтр и в JWT.
Если фильтр  region_ids пустой - возвращает все доступные регионы из JWT
*/
func getAllowedRegionIDsFromQuery(c echo.Context) ([]int, error) {
	regions, err := getIntSliceFromQuery(c.QueryParam("region_ids"))
	if err != nil {
		return nil, errors.Wrapf(err, "wrong region param %s", c.QueryParam("region_ids"))
	}

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		return nil, errors.Wrapf(err, "error getting regionsID from JWT")
	}
	filteredRegions := getAllowedIDs(allowedRegions, regions)

	return filteredRegions, nil
}

func getIntSliceFromQuery(qparam string) ([]int, error) {
	var ints []int

	if params := qparam; params != "" {
		for _, v := range strings.Split(params, ",") {
			intv, err := strconv.Atoi(v)
			if err != nil {
				return nil, err
			}

			ints = append(ints, intv)
		}
	}

	return ints, nil
}

func getStringSliceFromQuery(qparam string) []string {
	if qparam == "" {
		return nil
	}

	return strings.Split(qparam, ",")
}

type RecordsWithCount struct {
	Records interface{} `json:"records"`
	Count   int         `json:"records_count"`
}
type OrdersWithCount struct {
	Orders []orders.OrderCRM `json:"orders"`
	Count  int               `json:"records_count"`
}

// SendOptionsToClient - отправляет все не удаленные опции (фичи, сервисы) с црмки на клиентское через кролика
// параметр маркер - специальные переменные в пакете variables/states.go
func SendOptionsToClient(marker string) error {

	if marker == constants.MarkerFeatures {
		var (
			allFeaturesWithMarker structures.DataWithMarker
		)
		allCrmFeatures, err := models.FeatureList("")
		if err != nil {
			return err
		}

		for _, ftr := range allCrmFeatures {
			allFeaturesWithMarker.Features = append(allFeaturesWithMarker.Features, ftr.Feature)
		}

		allFeaturesWithMarker.Marker = constants.MarkerFeatures

		err = rabsender.SendJSONByAction(rabsender.AcCrmNewOptionsToClient, allFeaturesWithMarker)
		if err != nil {
			res := fmt.Sprintf("Error sending AllFeatures to broker. %s", err)
			logs.OutputError(res)
		}

		return nil
	}
	if marker == constants.MarkerServices {
		var (
			allServicesWithMarker structures.DataWithMarker
		)

		allCrmServices, err := models.ServicesList(false)
		if err != nil {
			return err
		}

		allServicesWithMarker.Marker = constants.MarkerServices
		for _, srv := range allCrmServices {
			allServicesWithMarker.Services = append(allServicesWithMarker.Services, srv.Service)
		}

		err = rabsender.SendJSONByAction(rabsender.AcCrmNewOptionsToClient, allServicesWithMarker)
		if err != nil {
			res := fmt.Sprintf("Error sending AllServices to broker. %s", err)
			logs.OutputError(res)
		}

		return nil
	}

	return errors.Errorf("invalid marker")
}

func getPager(c echo.Context) (urlvalues.Pager, error) {
	var (
		pager urlvalues.Pager
	)
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return pager, errors.Errorf("Error parsing pager limit,%s", err)
	}

	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return pager, errors.Errorf("Error parse orders page,%s", err)
	}
	pager.Offset = (page - 1) * limit
	pager.Limit = limit
	return pager, nil
}
func getTimeParams(c echo.Context) (time.Time, time.Time, error) {
	var (
		minData, maxData time.Time
	)
	mindateParam := c.QueryParam("mindate")
	if mindateParam != "" {
		mindateUnix, err := strconv.ParseInt(mindateParam, 10, 64)
		if err != nil {
			return minData, maxData, errors.Errorf("error mindate parsing,%s", err)
		}
		minData = time.Unix(mindateUnix, 0)
	}
	maxdateParam := c.QueryParam("maxdate")
	if maxdateParam != "" {
		maxdateUnix, err := strconv.ParseInt(maxdateParam, 10, 64)
		if err != nil {
			return minData, maxData, errors.Errorf("error maxdate parsing,%s", err)
		}
		maxData = time.Unix(maxdateUnix, 0)
	}
	return minData, maxData, nil
}

func memberUUIDFromJWT(c echo.Context) (string, structures.ChatMembers, error) {
	user := c.Get("user")
	userTok, check := user.(*jwt.Token)
	if !check {
		return "", "", errors.Errorf("invalid user field in context")
	}
	claims := userTok.Claims.(jwt.MapClaims)
	clientUUID, check := claims["client_uuid"]
	if check {
		return clientUUID.(string), structures.ClientMember, nil
	}
	driverUUID, check := claims["driver_uuid"]
	if check {
		return driverUUID.(string), structures.DriverMember, nil
	}
	operUUID, check := claims["user_uuid"]
	if check {
		return operUUID.(string), structures.UserCRMMember, nil
	}
	return "", "", errors.Errorf("invalid uuid field in context")
}

func clientPhoneFromJWTIfExists(c echo.Context) (string, error) {
	_, userType, err := memberUUIDFromJWT(c)
	if err != nil || userType != structures.ClientMember {
		return "", err
	}
	return clientPhoneFromJWT(c)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// TakePicture - сохранение картинки в GC и путь до нее в базе соответствующей сущности
func (h *Handler) TakePicture(c echo.Context) error {

	pt := models.PictureTarget{
		Target: models.PictureTargetType(c.QueryParam(models.PictureTargetKey)),
		UUID:   c.QueryParam(models.PictureTargetUUID),
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":  "TakePicture",
		"Target": pt.Target,
		"uuid":   pt.UUID,
	})

	switch pt.Target {
	case models.StorePicture, models.ProductPicture, models.ServicePicture:
		//do nothing
	default:
		log.WithField("reason", "Target validate").Error("invalid target value")
		return c.JSON(http.StatusBadRequest, errpath.Errorf("invalid target value").Error())
	}
	// if pt.UUID == "" {
	// 	log.WithField("reason", "UUID validate").Error("empty target uuid")
	// 	return c.JSON(http.StatusBadRequest, errpath.Errorf("empty target uuid").Error())
	// }

	fileHeader, err := c.FormFile("image")
	if err != nil {
		log.WithField("reason", "get FormFile data").Error(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	file, err := fileHeader.Open()
	if err != nil {
		log.WithField("reason", "open file").Error(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	defer file.Close()

	upload := cloudstorage.Upload{
		Filename: fileHeader.Filename,
		Filesize: fileHeader.Size,
		Filetype: fileHeader.Header.Get("Content-Type"),
		File:     file,
	}

	ctxUpload, cancel := context.WithTimeout(c.Request().Context(), uploadToCloudTimeout)
	defer cancel()

	imagesPath, err := h.Server.SaveImage(ctxUpload, upload, pt)
	if err != nil {
		log.WithField("reason", "SaveImage").Error(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	jsonImagesPath, err := json.Marshal(imagesPath)
	if err != nil {
		log.WithField("reason", "Marshal").Error(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	if pt.UUID == "" {
		log.Info("Picture saved without target_uuid object")

		return c.JSON(http.StatusPartialContent, imagesPath)
	}

	switch pt.Target {
	case models.StorePicture:
		var store models.StoreCRM
		store.Image = string(jsonImagesPath)
		err := store.UpdateImage(pt.UUID)
		if err != nil {
			log.WithField("reason", "UpdateImage").Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
	case models.ProductPicture:
		var product models.ProductCRM
		product.Image = string(jsonImagesPath)
		err := product.UpdateImage(pt.UUID)
		if err != nil {
			log.WithField("reason", "UpdateImage").Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
	case models.ServicePicture:
		var store models.ServiceCRM
		store.Image = string(jsonImagesPath)
		err := store.UpdateImage(pt.UUID)
		if err != nil {
			log.WithField("reason", "UpdateImage").Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
	default:
		log.WithField("reason", pt.Target+" unknown picture target")
		return c.JSON(http.StatusBadRequest, pt.Target+" unknown picture target")
	}

	log.Info("Picture saved successfuly")

	return c.JSON(http.StatusOK, imagesPath)
}

// DifragPicture -
func (h *Handler) DifragPicture(c echo.Context) error {
	var bind struct {
		DifragPicture struct {
			Enable       bool   `json:"enable"`
			FilterPrefix string `json:"filter_prefix"`
		} `json:"difrag_picture"`
	}

	if err := c.Bind(&bind); err != nil {
		return c.JSON(http.StatusOK, errpath.Err(err).Error())
	}

	if bind.DifragPicture.Enable {
		err := h.Server.CloudStorage.DifragPic(context.Background(), models.ImageFullFileName, []cloudstorage.ReassignPicture{
			{
				Name: models.ImageSmallFileName,
				Size: tool.PictureSize{
					Height: models.ImageSmallHeight,
					Width:  models.ImageSmallWidth,
				},
			},
			{
				Name: models.ImageMediumFileName,
				Size: tool.PictureSize{
					Height: models.ImageMediumHeight,
					Width:  models.ImageMediumWidth,
				},
			},
		}, bind.DifragPicture.FilterPrefix)
		if err != nil {
			return c.JSON(http.StatusOK, errpath.Err(err).Error())
		}
	}

	return c.JSON(http.StatusOK, "Done")
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// FindAddress - получение адреса по координатам
func FindAddress(c echo.Context) error {
	var coor tool.Coordinates

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "address search",
	})

	if err := c.Bind(&coor); err != nil {
		err = errpath.Err(err)
		log.WithFields(logrus.Fields{
			"reason": "error binding data",
		}).Error(err)
		res := logs.OutputRestError("Error binding data [coordinates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.WithField("coord", fmt.Sprintf("lat:%v,lon:%v", coor.Lat, coor.Long))

	findRoute, err := models.GetClientLocation(coor.Lat, coor.Long)
	if err != nil {
		err = errpath.Err(err)
		log.WithFields(logrus.Fields{
			"reason": "error getting client location",
		}).Error(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	log.Info("address found successfully")

	return c.JSON(http.StatusOK, findRoute)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// GetGodModeParams -
func (h *Handler) GetGodModeParams(c echo.Context) error {
	gm, err := h.Server.DB.GodModeModel.GetGodModeParams()
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	return c.JSON(http.StatusOK, gm)
}

// SetGodModeParams -
func (h *Handler) SetGodModeParams(c echo.Context) error {
	bind := make(map[string]interface{})
	if err := c.Bind(&bind); err != nil {
		return c.JSON(http.StatusBadRequest, "bing error")
	}

	if val, exist := bind["writeoff_of_activity"]; exist {
		v, ok := val.(bool)
		if ok {
			models.GodModeInst.WriteoffOfActivity.Value = v
		}
	}
	if val, exist := bind["valid_distance_to_get_tariff"]; exist {
		v, ok := val.(float64)
		if ok {
			models.GodModeInst.ValidDistanceToGetTariff.Value = v
		}
	}
	if val, exist := bind["enable_toggle_acceptance_of_counter_orders"]; exist {
		v, ok := val.(bool)
		if ok {
			models.GodModeInst.EnableToggleAcceptanceOfCounterOrders.Value = v
		}
	}
	if val, exist := bind["apps_verification_code"]; exist {
		v, ok := val.(float64)
		if ok {
			models.GodModeInst.AppsVerificationCode.Value = int(v)
		}
	}

	if err := h.Server.DB.GodModeModel.SetGodModeParams(models.GodModeInst); err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	return c.JSON(http.StatusOK, models.GodModeInst)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
