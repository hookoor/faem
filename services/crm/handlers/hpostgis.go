package handlers

import (
	// "fmt"
	"net/http"

	// "gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// GetZonesByPoints -
func GetZonesByPoints(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "getting zones by points",
	})

	var coords []structures.PureCoordinates
	err := c.Bind(&coords)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	res, err := models.ZonesByPoints(coords)
	if err != nil {
		msg := "get zone by point"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.WithFields(logrus.Fields{
		"coords": coords,
	}).Info("получнны зоны вхождения по точкам")

	return c.JSON(http.StatusOK, res)
}
