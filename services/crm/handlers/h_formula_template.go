package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// FormulaTemplateList -
func FormulaTemplateList(c echo.Context) error {
	// временная заглушка
	ftlist := []models.FormulaTemplateCRM{
		{
			UUID: "f34c9477-a005-40b8-b59f-ff39c4af5d25",
			FormulaTemplate: structures.FormulaTemplate{
				Formula: "DriverBalance- (%B% + (OrderPrice > %D% ? OrderPrice * 0.01* %E% : OrderPrice * 0.01* %A%))",
				Comment: "За успешный заказ",
				Vars: []structures.FormulaTemplateVars{
					{
						Key:   "A",
						Value: "0",
						Name:  "Базовое списание от суммы заказа в процентах",
					},
					{
						Key:   "B",
						Value: "0",
						Name:  "Списание от суммы заказа в рублях",
					},
					{
						Key:   "D",
						Value: "0",
						Name:  "Если сумма заказа выше",
					},
					{
						Key:   "E",
						Value: "0",
						Name:  "То списать % от суммы заказа",
					},
				},
				Belong: "",
			},
		},
		{
			UUID: "d78a9477-a005-04b8-b59f-ff39c4af5d64",
			FormulaTemplate: structures.FormulaTemplate{
				Formula: "DriverBalance-%C%",
				Comment: "За отказ от заказа",
				Vars: []structures.FormulaTemplateVars{
					{
						Key:   "C",
						Value: "0",
						Name:  "Штраф за отказ в рублях",
					},
				},
				Belong: "",
			},
		},
	}

	return c.JSON(http.StatusOK, ftlist)
}
