package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// CreateCurrentIncreasedFareSteps godoc
func CreateCurrentIncreasedFareSteps(c echo.Context) error {

	var (
		steps models.IncreasedFare
	)
	userUUID := userUUIDFromJWT(c)
	if err := c.Bind(&steps); err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "creating steps for increased fare",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error()
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	steps, err := steps.CreateActualIncreasedFareSteps()
	if err != nil {
		msg := "error updating"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "creating steps for increased fare",
			"reason":   msg,
			"userUUID": userUUID,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":    "creating steps for increased fare",
		"userUUID": userUUID,
	}).Info("done succesfully")

	return c.JSON(http.StatusOK, steps)
}

// GetCurrentIncreasedFareSteps godoc
func GetCurrentIncreasedFareSteps(c echo.Context) error {

	var (
		steps models.IncreasedFare
	)
	steps, err := models.GetActualIncreasedFareSteps()
	if err != nil {
		msg := "ошибка получения корректных шагов для увеличения цены"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting steps for increased fare",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, steps)
}
