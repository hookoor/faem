package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

func GetActivities(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting activity config",
		"userUUID": userUUIDFromJWT(c),
	})
	activities, err := models.GetActivities(c.Request().Context())
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error getting activities from DB",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}
	return c.JSON(http.StatusOK, activities)
}

func UpdateActivities(c echo.Context) error {
	var values structures.ActivityConfig
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "updating activity config",
		"userUUID": userUUIDFromJWT(c),
	})
	if err := c.Bind(&values); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error binding config into structure",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}
	if err := models.UpdateActivities(c.Request().Context(), values); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error updating config into DB",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{Code: 200, Msg: "Конфиг активностей успешно обновлен!"})
}
