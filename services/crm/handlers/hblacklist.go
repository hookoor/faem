package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// AddNewPhoneToBlackList godoc
func AddNewPhoneToBlackList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "add new phone to black list",
	})

	userUUID, memberType, err := memberUUIDFromJWT(c)
	if err != nil {
		msg := "error parse jwt"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = logs.Eloger.WithField("userUUID", userUUID)

	data := new(models.BlackListData)
	if err := c.Bind(data); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = logs.Eloger.WithField("phone", data.Phone)

	data.Phone = formatDriverPhoneForOptions(data.Phone)
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	data.CreatorType = memberType
	data.CreatorUUID = userUUID

	if err := models.AddNumberToBlackList(data).Error; err != nil {
		msg := "error adding number to bl"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}

	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// RemovePhoneFromBlackList godoc
func RemovePhoneFromBlackList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "remove phone from black list",
	})

	userUUID, memberType, err := memberUUIDFromJWT(c)
	if err != nil {
		msg := "error parse jwt"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = logs.Eloger.WithFields(logrus.Fields{
		"userUUID": userUUID,
		"userType": memberType,
	})

	data := new(struct {
		Phone string `json:"phone"`
	})
	if err := c.Bind(data); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = log.WithField("phone", data.Phone)

	data.Phone = formatDriverPhoneForOptions(data.Phone)
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if errWL := models.RemovePhoneFromBlackList(data.Phone); errWL != nil {
		msg := "removing phone error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}

	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// GetBlackList godoc
func GetBlackList(c echo.Context) error {
	pager, err := getPager(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting black list",
		"userUUID": userUUIDFromJWT(c),
	})
	if err != nil {
		msg := "error getting pager"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	result, count, err := models.GetBlackList(pager)

	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: result,
		Count:   count,
	})
}
func GetBlackListedPhone(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting blacklisted phone",
		"userUUID": userUUIDFromJWT(c),
	})

	result, err := models.GetBlackListedPhone(c.Param("phone"))
	if err != nil {
		msg := "error getting blacklisted phone"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}

	return c.JSON(http.StatusOK, result)
}
func UpdateBlackListedPhone(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "updating blacklisted phone",
		"userUUID": userUUID,
	})

	phone := c.Param("phone")
	if phone == "" {
		msg := "empty phone"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "update driver tariff",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	blPhone := new(models.BlackListDataCRM)
	err := c.Bind(blPhone)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "update blacklisted phone",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	blPhone.Phone = phone
	errWL := blPhone.UpdateBlackListedPhone(phone)
	if errWL != nil {
		msg := "error updating blacklisted phone"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errWL)
		res := logs.OutputRestError(msg, errWL.Error, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":    "update blacklisted phone",
		"userUUID": userUUID,
	}).Info("blacklisted phone was updated successfully")

	return c.JSON(http.StatusOK, "updated")
}

func GetBlackListByFilter(c echo.Context) error {
	var (
		err      error
		criteria models.BlackListFilterCriteria
	)
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "filtered blacklist",
		"userUUID": userUUID,
	})
	criteria.Pager, err = getPager(c)
	if err != nil {
		msg := "error getting pager"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	criteria.Phone = c.QueryParam("phone")
	result, count, err := criteria.GetBlackListByFilter()
	if err != nil {
		msg := "error getting blacklist by filter"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: result,
		Count:   count,
	})
}

func IsPhoneInBlacklist(c echo.Context) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "checking if phone is blacklisted",
	})
	phone := c.Param("phone")
	if phone == "" {
		res := "request parameter 'phone' is empty"
		log.Error(errpath.Errorf(res))
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(res, errors.Errorf(res)))
	}
	phone = formatDriverPhoneForOptions(phone)
	phone, err := phoneverify.NumberVerify(phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: http.StatusBadRequest, Msg: msg})
	}
	is, err := models.IsPhoneInBlackList(phone)
	if err != nil {
		msg := "checking phone in blacklist"
		logs.Eloger.WithFields(logrus.Fields{
			"client phone": phone,
		}).Error(errpath.Err(err))
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"client phone": phone,
	}).Info("blacklisted:", is)

	return c.JSON(http.StatusOK, is)
}
