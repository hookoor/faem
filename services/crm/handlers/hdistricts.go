package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// CreateDistrict godoc
func CreateDistrict(c echo.Context) error {
	var district models.DistrictCRM
	userUUID := userUUIDFromJWT(c)
	err := c.Bind(&district)
	if err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "create district",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)

		return c.JSON(http.StatusBadRequest, res)
	}

	nDistrict, err := district.Create()
	if err != nil {
		msg := "error creating district"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create district",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "create district",
		"distrUUID": nDistrict.UUID,
		"userUUID":  userUUID,
	}).Error("district create successfully")
	return c.JSON(http.StatusOK, nDistrict)
}

// DistrictsList godoc
func DistrictsList(c echo.Context) error {
	var districts []models.DistrictCRM
	districts, err := models.DistrictsList()
	if err != nil {
		msg := "error creating district"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "create district",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, districts)
}

// GetDistrict godoc
func GetDistrict(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty UUID"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting district by uuid",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var district models.DistrictCRM
	err := models.GetByUUID(uuid, &district)
	if err != nil {
		msg := "error getting district"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "getting district by uuid",
			"distrUUID": uuid,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, district)
}

// GetRegionDistricts godoc
func GetRegionDistricts(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty UUID"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting region district",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	districts, err := models.DistrictsByRegion(uuid)
	if err != nil {
		msg := "error getting district by region"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting region district",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, districts)
}

// UpdateDistrict godoc
func UpdateDistrict(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty UUID"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update district",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var district models.DistrictCRM

	if err := c.Bind(&district); err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "update district",
			"distrUUID": uuid,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	userUUID := userUUIDFromJWT(c)
	uDistrict, err := district.Update(uuid)
	if err != nil {
		msg := "error update district"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "update district",
			"distrUUID": uuid,
			"userUUID":  userUUID,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "update district",
		"distrUUID": uuid,
		"userUUID":  userUUID,
	}).Info("district update")
	return c.JSON(http.StatusOK, uDistrict)
}

// DeleteDistrict godoc
func DeleteDistrict(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty UUID"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "delete district",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var district models.DistrictCRM
	district.UUID = uuid
	userUUID := userUUIDFromJWT(c)
	err := district.SetDeleted()
	if err != nil {
		msg := "empty UUID"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "delete district",
			"distUUID": uuid,
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":    "delete district",
		"distUUID": uuid,
		"userUUID": userUUID,
	}).Info("delete district")

	res := logs.OutputRestOK("District Delete")
	return c.JSON(http.StatusOK, res)
}
