package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

type ControllerDriversServicesRelation struct {
	dbi *models.DB
}

type ControllerDriversFeaturesRelation struct {
	dbi *models.DB
}

func NewControllerDriversServicesRelation(db *models.DB) *ControllerDriversServicesRelation {
	return &ControllerDriversServicesRelation{
		dbi: db,
	}
}

func NewControllerDriversFeaturesRelation(db *models.DB) *ControllerDriversFeaturesRelation {
	return &ControllerDriversFeaturesRelation{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (ctrl *ControllerDriversServicesRelation) SetRelation(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Set service relation")

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		errr := errpath.Errorf("driver uuid from JWT not found").Error()
		log.Errorf(errr)
		return c.JSON(http.StatusBadRequest, errr)
	}
	log = log.WithField("driver_uuid", driverUUID)

	serviceUUID := c.Param("service-uuid")
	if serviceUUID == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("service_uuid", serviceUUID)

	rel, err := ctrl.dbi.DriverInteraction.ServiceRealation.SetRelation(ctx, driverUUID, serviceUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, rel)
}

func (ctrl *ControllerDriversServicesRelation) DeleteRelation(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Delete service relation")

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		errr := errpath.Errorf("driver uuid from JWT not found").Error()
		log.Errorf(errr)
		return c.JSON(http.StatusBadRequest, errr)
	}
	log = log.WithField("driver_uuid", driverUUID)

	serviceUUID := c.Param("service-uuid")
	if serviceUUID == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("service_uuid", serviceUUID)

	err := ctrl.dbi.DriverInteraction.ServiceRealation.DeleteRelation(ctx, driverUUID, serviceUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, "Done")
}

// GetServicesByDriverUUID - воитель получает список сервисов из группы к которой он принадлежит
// meta: водители из одной и той же группы могут иметь доступ к разним сервисам в этой группе.
// 		 По умолчанию ему доступны все сервисы в группе, но он может отключить некоторые
func (ctrl *ControllerDriversServicesRelation) GetServicesByDriverUUID(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "GetServicesByDriverUUID")

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		errr := errpath.Errorf("driver uuid from JWT not found").Error()
		log.Errorf(errr)
		return c.JSON(http.StatusBadRequest, errr)
	}
	log = log.WithField("driver_uuid", driverUUID)

	// по юю водителя получить водителя
	var driver models.DriverCRM
	if err := driver.GetDriverByUUID(driverUUID); err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// по водителю получить юю группы
	group, err := ctrl.dbi.DriverGroupModel.GetGroup(ctx, driver.Group.UUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// получение региона с целью получить набор услуг
	region, err := ctrl.dbi.RegiosModel.GetByID(ctx, group.RegionID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// получение региональны сервисов с метками видимости для водителя
	regServices, err := models.GetServiceSet(ctx, region.ServicesSetID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// по юю группы получить список сервисов
	services, err := models.ServicesByUUID(ctx, group.ServicesUUID...)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// получить список активированных у водителя сервисов
	activeServices, err := ctrl.dbi.DriverInteraction.ServiceRealation.GetServicesByDriver(ctx, driverUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	type shortservice struct {
		ServiceUUID     string `json:"service_uuid"`
		ServiceName     string `json:"service_name"`
		Active          bool   `json:"active"`
		Enabled         bool   `json:"enabled"`
		Visible         bool   `json:"visible"`
		DisplayPriority int    `json:"display_priority"`
	}

	var response []shortservice
	for _, s := range services {
		for _, rs := range regServices {
			if rs.ServiceUUID == s.UUID {
				response = append(response, shortservice{
					ServiceUUID:     rs.ServiceUUID,
					ServiceName:     rs.ServiceName,
					Enabled:         rs.EnableForDriver,
					Visible:         rs.VisibleForDriver,
					DisplayPriority: rs.DisplayPriority,
				})
				break
			}
		}
	}
	// заполняем метку активной услуги у водителя
	for i := range response {
		for _, af := range activeServices {
			if response[i].ServiceUUID == af.UUID {
				response[i].Active = true
			}
		}
	}

	return c.JSON(http.StatusOK, response)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (ctrl *ControllerDriversFeaturesRelation) SetRelation(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Set feature relation")

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		err := errpath.Errorf("driver uuid from JWT not found")
		log.Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	log = log.WithField("driver_uuid", driverUUID)

	featureUUID := c.Param("feature-uuid")
	if featureUUID == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("feature_uuid", featureUUID)

	rel, err := ctrl.dbi.DriverInteraction.FeatureRealation.SetRelation(ctx, driverUUID, featureUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, rel)
}

func (ctrl *ControllerDriversFeaturesRelation) DeleteRelation(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "Delete feature relation")

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		errr := errpath.Errorf("driver uuid from JWT not found").Error()
		log.Errorf(errr)
		return c.JSON(http.StatusBadRequest, errr)
	}
	log = log.WithField("driver_uuid", driverUUID)

	featureUUID := c.Param("feature-uuid")
	if featureUUID == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("feature_uuid", featureUUID)

	err := ctrl.dbi.DriverInteraction.FeatureRealation.DeleteRelation(ctx, driverUUID, featureUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, "Done")
}

func (ctrl *ControllerDriversFeaturesRelation) GetFeaturesForDriver(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForService GetFeaturesForDriver")

	taxiparkUUID := taxiParkUUIDFromJWT(c)
	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		err := errpath.Errorf("driver uuid fom jwt not found")
		log.Errorf(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	log = log.WithFields(logrus.Fields{
		"driver UUID": driverUUID,
		"Taxiparks":   taxiparkUUID,
	})

	region, err := ctrl.dbi.RegiosModel.GetRegionByTaxiparkUUID(ctx, taxiparkUUID[0])
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	regFeatures, err := ctrl.dbi.FeaturesSetForRegionModel.GetFilteredList(ctx, models.FilterFeaturesSetForRegion{SetID: &region.FeaturesSetID})
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var featureUUIDs []string
	for _, f := range regFeatures {
		featureUUIDs = append(featureUUIDs, f.FeatureUUID)
	}

	features, err := models.FeatureCRM{}.GetFeaturesByUUIDs(ctx, featureUUIDs...)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// получение активных опций у водителя
	activeFeatures, err := ctrl.dbi.DriverInteraction.FeatureRealation.GetFeaturesByDriver(ctx, driverUUID)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	type featuresForFront struct {
		FeatureUUID     string `json:"feature_uuid"`
		FeatureName     string `json:"feature_name"`
		Active          bool   `json:"active"`
		Enabled         bool   `json:"enabled"`
		Visible         bool   `json:"visible"`
		DisplayPriority int    `json:"display_priority"`
	}

	var response []featuresForFront
	for _, rf := range regFeatures {
		for _, f := range features {
			if rf.FeatureUUID == f.UUID {
				response = append(response, featuresForFront{
					FeatureUUID:     rf.FeatureUUID,
					FeatureName:     f.Name,
					Enabled:         rf.EnableForDriver,
					Visible:         rf.VisibleForDriver,
					DisplayPriority: rf.DisplayPriority,
				})
				break
			}
		}
	}
	// заполняем метку активной опции у водителя
	for i := range response {
		for _, af := range activeFeatures {
			if response[i].FeatureUUID == af.UUID {
				response[i].Active = true
			}
		}
	}

	return c.JSON(http.StatusOK, response)
}
