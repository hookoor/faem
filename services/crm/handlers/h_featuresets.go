package handlers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// ControllerFeaturesSetForRegion -
type ControllerFeaturesSetForRegion struct {
	dbi *models.DB
}

// ControllerFeaturesSetForService -
type ControllerFeaturesSetForService struct {
	dbi *models.DB
}

// NewControllerFeaturesSetForRegion -
func NewControllerFeaturesSetForRegion(db *models.DB) *ControllerFeaturesSetForRegion {
	return &ControllerFeaturesSetForRegion{
		dbi: db,
	}
}

// NewControllerFeaturesSetForService -
func NewControllerFeaturesSetForService(db *models.DB) *ControllerFeaturesSetForService {
	return &ControllerFeaturesSetForService{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (ctrl *ControllerFeaturesSetForRegion) getRegionFeaturesSetID(c echo.Context) (int, error) {
	regionIDs, err := regionsIDFromJWT(c)
	if err != nil {
		return 0, errpath.Err(err)
	}

	if len(regionIDs) == 0 {
		return 0, errpath.Errorf("у вас не прописан регион")
	}

	// подразумеваю что региональный оператор имеет всего 1 регион в jwt
	region, err := ctrl.dbi.RegiosModel.GetByID(c.Request().Context(), regionIDs[0])
	if err != nil {
		return 0, errpath.Err(err)
	}

	return region.FeaturesSetID, nil
}

// Seting -
func (ctrl *ControllerFeaturesSetForRegion) Seting(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion Seting")

	featureUUID := c.QueryParam("featureuuid")
	regionID, err := strconv.Atoi(c.QueryParam("regionid"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	regionIDs, err := regionsIDFromJWT(c)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	if allowed := getAllowedIDs(regionIDs, []int{regionID}); len(allowed) == 0 {
		err = errpath.Errorf("у вас нет доступа к региону [%v]", regionID)
		log.Errorf(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	region, err := ctrl.dbi.RegiosModel.GetByID(ctx, regionID)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	err = ctrl.dbi.FeaturesSetForRegionModel.Seting(ctx, region.FeaturesSetID, featureUUID)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, "Done")
}

type responseFeaturesSetForRegion struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name"`
	Comment string `json:"comment"`
	Price   int    `json:"price"`
	// Active - метка того что опция активна в регионе
	Active           bool `json:"active"`
	EnableForDriver  bool `json:"enable_for_driver"`
	VisibleForDriver bool `json:"visible_for_driver"`
	EnableForClient  bool `json:"enable_for_client"`
	VisibleForClient bool `json:"visible_for_client"`
	DisplayPriority  int  `json:"display_priority"`
}

func (ctrl *ControllerFeaturesSetForRegion) SettingByArray(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion GetFilteredList")

	// --- Binding

	b, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var bind []responseFeaturesSetForRegion
	err = json.Unmarshal(b, &bind)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	ftrSetID, err := strconv.Atoi(c.Param("feature-set-id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// ---

	{ // на случай если понадобиться работать с регионом а не с набором
		// regionID, err := strconv.Atoi(c.QueryParam("regionid"))
		// if err != nil {
		// 	log.Errorf(errpath.Err(err).Error())
		// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		// }

		// regionIDs, err := regionsIDFromJWT(c)
		// if err != nil {
		// 	log.Errorf(errpath.Err(err).Error())
		// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		// }

		// if allowed := getAllowedIDs(regionIDs, []int{regionID}); len(allowed) == 0 {
		// 	err = errpath.Errorf("у вас нет доступа к региону [%v]", regionID)
		// 	log.Errorf(err.Error())
		// 	return c.JSON(http.StatusBadRequest, err.Error())
		// }

		// region, err := ctrl.dbi.RegiosModel.GetByID(ctx, regionID)
		// if err != nil {
		// 	log.Errorf(errpath.Err(err).Error())
		// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		// }
	}

	exstFeatures, err := ctrl.dbi.FeaturesSetForRegionModel.GetFilteredList(ctx, models.FilterFeaturesSetForRegion{SetID: &ftrSetID})
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	for _, bnd := range bind {
		var isActive bool
		var existFeatrID int = -1
		for _, ef := range exstFeatures {
			if ef.FeatureUUID == bnd.UUID {
				isActive = true
				existFeatrID = ef.ID
				break
			}
		}

		if bnd.Active && !isActive {
			err = ctrl.dbi.FeaturesSetForRegionModel.Seting(ctx, ftrSetID, bnd.UUID)
			if err != nil {
				log.Errorf(errpath.Err(err).Error())
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}
		}
		if !bnd.Active && isActive {
			err = ctrl.dbi.FeaturesSetForRegionModel.Delete(ctx, existFeatrID)
			if err != nil {
				log.Errorf(errpath.Err(err).Error())
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}

			{ // удалить опцию у всех водителей в регионе при удалении ее из региона
				// этот функционал заморожен по просьбе Андрея (возможно понадобится)

				// regions, err := ctrl.dbi.RegiosModel.GetByFeatureSetId(ctx, ftrSetID)
				// if err != nil {
				// 	log.Errorf(errpath.Err(err).Error())
				// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
				// }
				// var regionsIDs []int
				// for _, el := range regions {
				// 	regionsIDs = append(regionsIDs, el.ID)
				// }
				// drivers, err := ctrl.dbi.RegiosModel.GetRegionalDrivers(ctx, regionsIDs)
				// if err != nil {
				// 	log.Errorf(errpath.Err(err).Error())
				// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
				// }
				// var driversUUIDs []string
				// for _, el := range drivers {
				// 	driversUUIDs = append(driversUUIDs, el.UUID)
				// }

				// err = ctrl.dbi.DriverModel.FeatureRealation.DeleteRelationByDrivers(ctx, driversUUIDs, []string{bnd.UUID})
				// if err != nil {
				// 	log.Errorf(errpath.Err(err).Error())
				// 	return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
				// }
			}
		}

		err := ctrl.dbi.FeaturesSetForRegionModel.UpdateByFeatureUUID(ctx, models.UpdatableFieldsModelFeaturesSetForRegion{
			EnableForDriver:  &bnd.EnableForDriver,
			VisibleForDriver: &bnd.VisibleForDriver,
			EnableForClient:  &bnd.EnableForClient,
			VisibleForClient: &bnd.VisibleForClient,
			DisplayPriority:  &bnd.DisplayPriority,
		}, bnd.UUID)
		if err != nil {
			log.Errorf(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

	}

	return c.JSON(http.StatusOK, "Опции обновлены")
}

// GetFilteredList -
func (ctrl *ControllerFeaturesSetForRegion) GetFilteredList(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion GetFilteredList")

	FeaturesSetID, err := ctrl.getRegionFeaturesSetID(c)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	ftrSetForReg, err := ctrl.dbi.FeaturesSetForRegionModel.GetFilteredList(ctx, models.FilterFeaturesSetForRegion{SetID: &FeaturesSetID})
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var featuresUUIds []string
	for _, el := range ftrSetForReg {
		featuresUUIds = append(featuresUUIds, el.FeatureUUID)
	}
	features, err := models.FeatureCRM{}.GetFeaturesByUUIDs(ctx, featuresUUIds...)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	type fullResType struct {
		models.FeaturesSetForRegionCRM
		Feature models.FeatureCRM `json:"feature"`
	}
	var fullRes []fullResType

	for _, el := range ftrSetForReg {
		var needFtr models.FeatureCRM
		for _, f := range features {
			if f.UUID == el.FeatureUUID {
				needFtr = f
				break
			}
		}
		fullRes = append(fullRes, fullResType{FeaturesSetForRegionCRM: el, Feature: needFtr})
	}

	return c.JSON(http.StatusOK, fullRes)
}

// GetListWithActiveMarks - получение списка опций с меткой принадлежности(активности(active)) к региону
func (ctrl *ControllerFeaturesSetForRegion) GetListWithActiveMarks(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion GetFilteredList")

	var filter models.FilterFeaturesSetForRegion
	err := c.Bind(&filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	featureSetID, err := strconv.Atoi(c.Param("feature-set-id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	filter.SetID = &featureSetID

	featuresSet, err := ctrl.dbi.FeaturesSetForRegionModel.GetFilteredList(ctx, filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	features, err := models.FeatureList("")
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var respData []responseFeaturesSetForRegion

	for _, f := range features {
		var active bool
		var enableForDriver bool
		var visibleForDriver bool
		var enableForClient bool
		var visibleForClient bool
		var displayPriority int
		for _, el := range featuresSet {
			if f.UUID == el.FeatureUUID {
				active = true
				enableForDriver = el.EnableForDriver
				visibleForDriver = el.VisibleForDriver
				displayPriority = el.DisplayPriority
				enableForClient = el.EnableForClient
				visibleForClient = el.VisibleForClient
				break
			}
		}
		respData = append(respData, responseFeaturesSetForRegion{
			UUID:             f.UUID,
			Name:             f.Name,
			Comment:          f.Comment,
			Price:            f.Price,
			Active:           active,
			EnableForDriver:  enableForDriver,
			VisibleForDriver: visibleForDriver,
			EnableForClient:  enableForClient,
			VisibleForClient: visibleForClient,
			DisplayPriority:  displayPriority,
		})
	}

	return c.JSON(http.StatusOK, respData)
}

// Update -
func (ctrl *ControllerFeaturesSetForRegion) Update(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion Update")

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// TODO:
	// надо проверять может ли пользователь обновить запись по переданному id
	// он может выбрать только те id котрые ему доступны при получении
	// но он может поменять id ручками в url

	var fsfr models.FeaturesSetForRegionCRM
	err = c.Bind(&fsfr)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	res, err := ctrl.dbi.FeaturesSetForRegionModel.Update(ctx, &fsfr, id)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, res)
}

// Delete -
func (ctrl *ControllerFeaturesSetForRegion) Delete(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion Delete")

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// TODO:
	// надо проверять может ли пользователь удалить запись по переданному id
	// он может выбрать только те id котрые ему доступны при получении
	// но он может поменять id ручками в url

	err = ctrl.dbi.FeaturesSetForRegionModel.Delete(ctx, id)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, fmt.Sprintf("feature set for region with id = %v deleted sucessfuly", id))
}

// -------------------------------------------------

func (ctrl *ControllerFeaturesSetForService) getRegionFeaturesSetID(c echo.Context) (int, error) {
	regionIDs, err := regionsIDFromJWT(c)
	if err != nil {
		return 0, errpath.Err(err)
	}

	if len(regionIDs) == 0 {
		return 0, errpath.Errorf("у вас не прописан регион")
	}

	// подразумеваю что региональный оператор имеет всего 1 регион в jwt
	region, err := ctrl.dbi.RegiosModel.GetByID(c.Request().Context(), regionIDs[0])
	if err != nil {
		return 0, errpath.Err(err)
	}

	return region.FeaturesSetID, nil
}

// SetFetureForService -
func (ctrl *ControllerFeaturesSetForService) SetFetureForService(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForService SetFetureForService")

	featureUUID := c.QueryParam("featureuuid")
	serviceUUID := c.QueryParam("serviceuuid")

	log = log.WithFields(logrus.Fields{
		"featureUUID": featureUUID,
		"serviceUUID": serviceUUID,
	})

	// TODO:
	// надо проверять может ли пользователь устанавливать запись для своего региона с переданными featureUUID и serviceUUID
	// он может выбрать только те featureUUID, serviceUUID котрые ему доступны при получении
	// но он может установить featureUUID,serviceUUID ручками в запросе

	featuresSetID, err := ctrl.getRegionFeaturesSetID(c)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	err = ctrl.dbi.FeaturesSetForRegionalServiceModel.SetFetureForService(ctx, featuresSetID, serviceUUID, featureUUID)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, "Опции обновлены")
}

// GetFilteredList -
func (ctrl *ControllerFeaturesSetForService) GetFilteredList(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForService GetFilteredList")

	featuresSetID, err := ctrl.getRegionFeaturesSetID(c)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var filter models.FilterFeaturesSetForService
	err = c.Bind(&filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	fsfss, err := ctrl.dbi.FeaturesSetForRegionalServiceModel.GetFilteredList(ctx, featuresSetID, filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var featuresUUIds []string
	for _, el := range fsfss {
		featuresUUIds = append(featuresUUIds, el.FeatureUUID)
	}
	features, err := models.FeatureCRM{}.GetFeaturesByUUIDs(ctx, featuresUUIds...)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	type responseFeaturesSetForRegion struct {
		models.FeaturesSetForServiceCRM
		Feature models.FeatureCRM `json:"feature"`
	}
	var respData []responseFeaturesSetForRegion

	for _, el := range fsfss {
		var needFtr models.FeatureCRM
		for _, f := range features {
			if f.UUID == el.FeatureUUID {
				needFtr = f
				break
			}
		}
		respData = append(respData, responseFeaturesSetForRegion{FeaturesSetForServiceCRM: el, Feature: needFtr})
	}

	return c.JSON(http.StatusOK, respData)
}

type responseFeaturesSetForService struct {
	UUID             string `json:"uuid"`
	Name             string `json:"name"`
	Comment          string `json:"comment"`
	Price            int    `json:"price"`
	Active           bool   `json:"active"`
	EnableForClient  bool   `json:"enable_for_client"`
	VisibleForClient bool   `json:"visible_for_client"`
	DisplayPriority  int    `json:"display_priority"`
}

// GetListWithActiveMarks - получение списка опций с меткой принадлежности(активности(active)) к услуге
func (ctrl *ControllerFeaturesSetForService) GetListWithActiveMarks(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForService GetFilteredList")

	featuresSetID, err := strconv.Atoi(c.QueryParam("feature-set"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	serviceUUID := c.QueryParam("service-uuid")

	var filter models.FilterFeaturesSetForService
	err = c.Bind(&filter)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	featrSetForReg, err := ctrl.dbi.FeaturesSetForRegionModel.GetFilteredList(ctx, models.FilterFeaturesSetForRegion{SetID: &featuresSetID})
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	featrSetForRegService, err := ctrl.dbi.FeaturesSetForRegionalServiceModel.GetFilteredList(ctx, featuresSetID, models.FilterFeaturesSetForService{ServiceUUID: &serviceUUID})
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	features, err := models.FeatureList("")
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var respData []responseFeaturesSetForService

	for _, f := range features {
		var active bool
		var enableForClient bool
		var visibleForClient bool
		var displayPriority int

		for _, el := range featrSetForReg {
			if f.UUID == el.FeatureUUID {
				enableForClient = el.EnableForClient
				visibleForClient = el.VisibleForClient
				displayPriority = el.DisplayPriority
				break
			}
		}
		for _, el := range featrSetForRegService {
			if f.UUID == el.FeatureUUID {
				active = true
				break
			}
		}
		respData = append(respData, responseFeaturesSetForService{
			UUID:             f.UUID,
			Name:             f.Name,
			Comment:          f.Comment,
			Price:            f.Price,
			Active:           active,
			EnableForClient:  enableForClient,
			VisibleForClient: visibleForClient,
			DisplayPriority:  displayPriority,
		})
	}

	return c.JSON(http.StatusOK, respData)
}

// SettingByArray -
func (ctrl *ControllerFeaturesSetForService) SettingByArray(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForRegion GetFilteredList")

	// --- Binding

	featuresSetID, err := strconv.Atoi(c.QueryParam("feature-set"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}
	serviceUUID := c.QueryParam("service-uuid")

	b, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var bind []responseFeaturesSetForService
	err = json.Unmarshal(b, &bind)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// ---

	exstFeatures, err := ctrl.dbi.FeaturesSetForRegionalServiceModel.GetFilteredList(ctx, featuresSetID, models.FilterFeaturesSetForService{ServiceUUID: &serviceUUID})
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	for _, bnd := range bind {
		var isActive bool
		var existFtrID int
		for _, ef := range exstFeatures {
			if ef.FeatureUUID == bnd.UUID {
				isActive = true
				existFtrID = ef.ID
				break
			}
		}
		if bnd.Active && !isActive {
			err = ctrl.dbi.FeaturesSetForRegionalServiceModel.SetFetureForService(ctx, featuresSetID, serviceUUID, bnd.UUID)
			if err != nil {
				log.Errorf(errpath.Err(err).Error())
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}
		}

		if !bnd.Active && isActive {
			err = ctrl.dbi.FeaturesSetForRegionalServiceModel.Delete(ctx, featuresSetID, existFtrID)
			if err != nil {
				log.Errorf(errpath.Err(err).Error())
				return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
			}
		}

		err := ctrl.dbi.FeaturesSetForRegionModel.UpdateByFeatureUUID(ctx, models.UpdatableFieldsModelFeaturesSetForRegion{
			EnableForClient:  &bnd.EnableForClient,
			VisibleForClient: &bnd.VisibleForClient,
			DisplayPriority:  &bnd.DisplayPriority,
		}, bnd.UUID)
		if err != nil {
			log.Errorf(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

	}

	return c.JSON(http.StatusOK, "Опции обновлены")
}


// Delete -
func (ctrl *ControllerFeaturesSetForService) Delete(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "FeaturesSetForService Delete")

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	featuresSetID, err := ctrl.getRegionFeaturesSetID(c)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	err = ctrl.dbi.FeaturesSetForRegionalServiceModel.Delete(ctx, featuresSetID, id)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, fmt.Sprintf("feature set for service with id = %v deleted sucessfuly", id))
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
