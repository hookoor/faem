package handlers

import (
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"net/http"
	"strconv"
	"time"
)

func GetSurgeConfig(c echo.Context) error {
	remoteConfig, err := models.LoadRemoteConfig(c.Request().Context(), "surge")
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}
	return c.JSON(http.StatusOK, remoteConfig.GetValue())
}
func UpdateSurgeConfig(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "updating activity config",
		"userUUID": userUUIDFromJWT(c),
	})
	var config models.SurgeConfig
	if err := c.Bind(&config); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error binding config into structure",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}

	err := models.UpdateSurgeConfig(c.Request().Context(), config)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error updating surge config",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{Code: 200, Msg: "Конфиг повышенного спроса успешно обновлен!"})
}

func GetSurgeZones(c echo.Context) error {
	log := logs.Eloger.WithField("event", "getting surge zones")
	ctx := c.Request().Context()

	var filter models.SurgeZoneFilter
	err := c.Bind(&filter)
	if err != nil {
		return c.JSON(http.StatusInternalServerError, structures.ResponseStruct{Code: 500, Msg: "context binding error"})
	}

	zones, err := models.GetSurgeZones(ctx, filter)
	if err != nil {
		log.Error(err)
		return c.JSON(http.StatusInternalServerError, structures.ResponseStruct{Code: 500, Msg: err.Error()})
	}

	return c.JSON(http.StatusOK, zones)
}

func parseTime(timestampStr string) (time.Time, error) {
	if timestampStr == "" {
		return time.Time{}, nil
	}

	timestamp, err := strconv.ParseInt(timestampStr, 10, 64)
	if err != nil {
		return time.Time{}, err
	}
	return time.Unix(timestamp, 0), nil
}

func GetSurgeTimestamps(c echo.Context) error {
	log := logs.Eloger.WithField("event", "getting surge timestamps")
	ctx := c.Request().Context()

	var filter models.SurgeTimestampFilter
	var err error
	filter.PeriodStart, err = parseTime(c.QueryParam("period_start"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: "invalid timestamp in 'period_start'"})
	}

	filter.PeriodEnd, err = parseTime(c.QueryParam("period_end"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: "invalid timestamp in 'period_end'"})
	}

	timestamps, err := models.GetSurgeTimestamps(ctx, filter)
	if err != nil {
		log.Error(err)
		return c.JSON(http.StatusInternalServerError, structures.ResponseStruct{Code: 500, Msg: err.Error()})
	}

	return c.JSON(http.StatusOK, timestamps)
}

func GetSurgeZoneHistory(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting surge zone history",
	})
	ctx := c.Request().Context()

	var filter models.SurgeHistoryFilter
	err := c.Bind(&filter)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: "binding error"})
	}

	zones, err := models.GetSurgeZonesHistory(ctx, filter)
	if err != nil {
		log.WithField("filter", filter).Error(err)
		return c.JSON(http.StatusInternalServerError, structures.ResponseStruct{Code: 500, Msg: err.Error()})
	}

	if len(zones) == 0 {
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{Code: 404, Msg: "no history available with this filter or zone doesn't exist"})
	}

	return c.JSON(http.StatusOK, zones)
}

func GetLiveSurgeZoneHistory(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting live surge zone history",
	})
	ctx := c.Request().Context()

	var filter models.SurgeHistoryFilter
	err := c.Bind(&filter)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: "binding error"})
	}

	lastTimestamp, err := models.GetLastSurgeTimestamp(ctx)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}

	filter.TimestampsUUIDs = []string{lastTimestamp.UUID}
	zones, err := models.GetSurgeZonesHistory(ctx, filter)
	if err != nil {
		log.WithField("filter", filter).Error(err)
		return c.JSON(http.StatusInternalServerError, structures.ResponseStruct{Code: 500, Msg: err.Error()})
	}

	if len(zones) == 0 {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 404, Msg: "no history available with this filter or zone doesn't exist"})
	}

	return c.JSON(http.StatusOK, zones)
}

func ListSurgeRules(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "ListSurgeRules",
	})
	ctx := c.Request().Context()

	var params struct {
		Name string `query:"name"`
	}
	if err := c.Bind(&params); err != nil {
		log.WithError(err).Error(ErrBindingData)
		res := logs.OutputRestError(ErrBindingData.Error(), ErrBindingData)
		return c.JSON(http.StatusBadRequest, res)
	}

	rules, err := models.GetSurgeRules(ctx, models.FilterSubLike("name", params.Name))
	if err != nil {
		log.WithError(err).Error()
		res := logs.OutputRestError("failed to fetch surge rules", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rules)
}

func GetSurgeRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetSurgeRule",
	})
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(err).Error(ErrBindingData)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	rules, err := models.GetSurgeRules(ctx, models.FilterEquals("id", id))
	if err != nil {
		res := logs.OutputRestError("failed to fetch surge rule", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(rules) == 0 {
		err = errors.New("no rule with such id")
		res := logs.OutputRestError("failed to fetch surge rule", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, rules[0])
}

func CreateSurgeRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "CreateSurgeRule",
	})
	ctx := c.Request().Context()

	var rule models.SurgeRuleCRM
	if err := c.Bind(&rule); err != nil {
		log.WithError(err).Error()
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}
	rule.ID = 0

	res, err := models.CreateOrUpdateSurgeRule(ctx, rule)
	if err != nil {
		log.WithError(err).Error()
		res := logs.OutputRestError("failed to create surge rule", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, res)
}

func UpdateSurgeRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdateSurgeRule",
	})
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(err).Error(ErrBindingData)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}

	var rule models.SurgeRuleCRM
	if err := c.Bind(&rule); err != nil {
		log.WithError(err).Error(ErrBindingData)
		res := logs.OutputRestError(ErrBindingData.Error(), err)
		return c.JSON(http.StatusBadRequest, res)
	}
	rule.ID = id

	res, err := models.CreateOrUpdateSurgeRule(ctx, rule)
	if err != nil {
		log.WithError(err).Error()
		res := logs.OutputRestError("failed to update surge rule", ErrBindingData)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, res)
}

func DeleteSurgeRule(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "DeleteSurgeRule",
	})
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		log.WithError(err).Error(ErrBindingData)
		res := logs.OutputRestError(ErrBindingData.Error(), ErrBindingData)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = models.DeleteSurgeRule(ctx, id)
	if err != nil {
		log.WithError(err).Error()
		res := logs.OutputRestError("failed to delete surge rule", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, logs.OutputRestOK("Правило удалено"))
}
