package handlers

import (
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// CreateTaxiPark godoc
func CreateTaxiPark(c echo.Context) error {
	userLogin, userUUID := loginFromJWT(c), userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "create taxiParks",
		"user_uuid":  userUUID,
		"user_login": userLogin,
	})

	taxiPark := new(models.TaxiParkCRM)
	if err := c.Bind(taxiPark); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	taxiPark.CreatorData = &models.TPCreatorData{
		Name: userLogin,
		UUID: userUUID,
	}

	if err := taxiPark.Create(); err != nil {
		log.WithField("reason", modelFuncErrorMsg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(modelFuncErrorMsg, err))
	}
	log.WithField("taxi_park_uuid", taxiPark.UUID).Info("new taxiPark created")

	taxiPark.FillUnixFields()

	return c.JSON(http.StatusOK, taxiPark)
}

func SetTaxiParkBalance(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "setting taxipark balance",
		"userUUID": userUUID,
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	var args setBalanceArgs
	if err := c.Bind(&args); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	log = log.WithField("in", args)
	args.TargetUUID = uuid

	// Validate
	if err := args.validate(); err != nil {
		log.WithField("reason", "validating incoming args").Error(err)
		res := logs.OutputRestError("ошибка обновления баланса", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Transform to a proto message
	description := msgSetBalance
	if args.Comment != "" {
		description += fmt.Sprintf(", комментарий: %s", args.Comment)
	}
	transfer := structures.NewTransfer{
		IdempotencyKey:   structures.GenerateUUID(),
		PayerAccountType: args.getAccountType(),
		TransferType:     args.TransferType,
		Amount:           args.Amount,
		Description:      description,
		Meta:             structures.TransferMetadata{OperatorUUID: userUUID},
	}
	if args.TransferType == structures.TransferTypeWithdraw {
		transfer.PayerType = structures.UserTypeTaxiPark
		transfer.PayerUUID = args.TargetUUID
	} else {
		transfer.PayeeType = structures.UserTypeTaxiPark
		transfer.PayeeUUID = args.TargetUUID
	}

	go func() {
		if err := rabsender.SendJSONByAction(rabsender.AcMakeTransfer, transfer, ""); err != nil {
			log.WithField("event", "making a taxipark transfer").Error(err)
			return
		}
		log.WithField("event", "transfer sent to RMQ").Info("OK")
	}()

	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

// TaxiParkListByFilter godoc
func TaxiParkListByFilter(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "taxiParks list by filter",
	})

	filter := new(models.TaxiParkCriteria)
	if err := c.Bind(filter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	regIds, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	filter.RegionIDs = getAllowedIDs(regIds, filter.RegionIDs)
	filter.AllowedTaxiParksUUID = getAllowedUUIDs(taxiParkUUIDFromJWT(c), filter.AllowedTaxiParksUUID)

	taxiParks, count, err := filter.GetSuitableTaxiParks()
	if err != nil {
		log.WithField("reason", modelFuncErrorMsg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(modelFuncErrorMsg, err))
	}
	models.FillTpUnixFields(taxiParks)

	return c.JSON(http.StatusOK, RecordsWithCount{
		Count:   count,
		Records: taxiParks,
	})
}

// GetTaxiParksByUUIDs godoc
func GetTaxiParksByUUIDs(c echo.Context) error {
	in := struct {
		UUID []string `json:"uuid"`
	}{}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get taxi parks by uuids",
		"uuids": in.UUID,
	})
	err := c.Bind(&in)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	if len(in.UUID) == 0 {
		log.WithFields(logrus.Fields{"reason": emptyUUIDMsg}).Error()

		res := logs.OutputRestError(emptyUUIDMsg, errors.Errorf(emptyUUIDMsg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	taxiPark := models.GetTaxiParksByUUID(in.UUID)

	return c.JSON(http.StatusOK, taxiPark)
}

// GetTaxiParkByUUID godoc
func GetTaxiParkByUUID(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get taxiPark by uuid",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	taxiPark, err := models.GetTaxiParkByUUID(uuid)
	if err != nil {
		log.WithField("reason", modelFuncErrorMsg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(modelFuncErrorMsg, err))
	}

	taxiPark.FillUnixFields()
	return c.JSON(http.StatusOK, taxiPark)
}

// UpdateTaxiPark godoc
func UpdateTaxiPark(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update taxiPark by uuid",
		"userUUID": userUUIDFromJWT(c),
	})
	taxiPark := new(models.TaxiParkCRM)

	if taxiPark.UUID = c.Param("uuid"); taxiPark.UUID == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", taxiPark.UUID)

	if err := c.Bind(taxiPark); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := taxiPark.Update(); err != nil {
		log.WithField("reason", modelFuncErrorMsg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(modelFuncErrorMsg, err))
	}

	log.Info("taxiPark updated")

	taxiPark.FillUnixFields()
	return c.JSON(http.StatusOK, taxiPark)
}

// DeleteTaxiPark godoc
func DeleteTaxiPark(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "set taxiPark deleted",
		"userUUID": userUUIDFromJWT(c),
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("uuid", uuid)

	if err := models.SetTaxiParkDeleted(uuid); err != nil {
		log.WithField("reason", modelFuncErrorMsg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(modelFuncErrorMsg, err))
	}
	log.Infof("taxiPark %s deleted", uuid)

	return c.JSON(http.StatusOK, logs.OutputRestOK("Таксопарк удален"))
}

func GetTaxiParkByMaxWeight(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get taxipark By max weight",
	})

	regionID, err := strconv.Atoi(c.Param("reg_id"))
	if err != nil || regionID == 0 {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	log = log.WithField("region_id", regionID)

	tps, err := models.GetTaxiParkByMaxWeight(regionID)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, tps)
}

func GetTaxiParksDistributionWeights(c echo.Context) error {
	var args struct {
		UUID []string `json:"uuid"`
	}

	if err := c.Bind(&args); err != nil {
		logs.Eloger.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	weights, err := models.GetTaxiParksWeights(args.UUID)
	if err != nil {
		err := logs.OutputRestError("failed to fetch taxiparks from db", err)
		return c.JSON(http.StatusBadRequest, err)
	}

	return c.JSON(http.StatusOK, weights)
}
