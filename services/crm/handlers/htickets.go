package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

type inputDataForChangeTicketState struct {
	Status  structures.TicketStatuses `json:"status"`
	Comment string                    `json:"comment"`
}

func CreateTicket(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create ticker",
	})

	ticket := new(models.TicketsCRM)
	if err := c.Bind(&ticket); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	ticket.TaxiParksUUID = getAllowedUUIDs(taxiParkUUIDFromJWT(c), ticket.TaxiParksUUID)
	if storeUUID := storeUUIDFromJWT(c); storeUUID != "" {
		ticket.StoreData.UUID = storeUUID
	}

	uuid, member, err := memberUUIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetJWTValue).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetJWTValue.Error(), err))
	}
	log = log.WithField("member_type", member)
	ticket.SourceType = member

	if member == structures.ClientMember {
		uuid, err = clientPhoneFromJWT(c)
		if err != nil {
			log.WithError(ErrGetJWTValue).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetJWTValue.Error(), err))
		}
	}
	log = log.WithField("member_uuid", uuid)

	if err := ticket.Create(uuid); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	ticket.CreatedAtUnix = ticket.CreatedAt.Unix()

	go func() {
		err = rabsender.SendJSONByAction(rabsender.AcNewTicket, ticket)
		if err != nil {
			msg := "error sending to broker"
			log.WithField("reason", msg).Error(err)
		}
	}()

	return c.JSON(http.StatusOK, ticket)
}

// GetTickerByUUID godoc
func GetTickerByUUID(c echo.Context) error {
	memberUUUID, member, err := memberUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if member == structures.ClientMember {
		memberUUUID, err = clientPhoneFromJWT(c)
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "get ticket by uuid",
		"memberType": member,
		"memberUUID": memberUUUID,
		"tickerUUID": uuid,
	})

	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	result, err := models.GetTicketByUUID(uuid)
	if err != nil {
		msg := "error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, result)
}

// ChangeTicketState godoc
func ChangeTicketState(c echo.Context) error {
	memberUUUID, member, err := memberUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "change ticket state",
		"memberType": member,
		"memberUUID": memberUUUID,
		"tickerUUID": uuid,
	})
	if member == structures.ClientMember {
		memberUUUID, err = clientPhoneFromJWT(c)
	}
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var inputdata inputDataForChangeTicketState
	err = c.Bind(&inputdata)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = inputdata.addCommentIfNeed(uuid, c)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error add comment",
		}).Warning(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusOK,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	tick, err := models.ChangeTicketStatus(uuid, inputdata.Status)
	if err != nil {
		msg := "error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	go func() {
		err = rabsender.SendJSONByAction(rabsender.AcNewTicketStatus, tick)
		if err != nil {
			msg := "error sending to broker"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
		}
	}()
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// AppendCommentToTicket godoc
func AppendCommentToTicket(c echo.Context) error {
	memberUUUID, member, err := memberUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "append comment to ticket",
		"memberType": member,
		"memberUUID": memberUUUID,
		"tickerUUID": uuid,
	})
	if member == structures.ClientMember {
		memberUUUID, err = clientPhoneFromJWT(c)
	}
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var comment structures.TicketComment
	err = c.Bind(&comment)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	comment.SenderType = member
	comment.SenderUUID = memberUUUID
	tick, err := models.AddCommentToTicket(uuid, comment)
	if err != nil {
		msg := "append comment to ticket error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	go func() {
		err = rabsender.SendJSONByAction(rabsender.AcNewTicketComment, tick)
		if err != nil {
			msg := "error sending to broker"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
		}
	}()
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func (inData *inputDataForChangeTicketState) addCommentIfNeed(uuid string, c echo.Context) error {
	if inData.Status == structures.NewStatus || inData.Status == structures.InWorkStatus {
		return nil
	}
	err := inData.validate()
	if err != nil {
		return err
	}
	var comment structures.TicketComment
	comment.SenderUUID, comment.SenderType, err = memberUUIDFromJWT(c)
	if err != nil {
		return err
	}
	comment.Message = inData.Comment
	_, err = models.AddCommentToTicket(uuid, comment)
	return err
}
func (inData *inputDataForChangeTicketState) validate() error {
	switch inData.Status {
	case structures.ResolvedStatus:
	case structures.NotResolvedStatus:
	case structures.NewStatus:
	case structures.InWorkStatus:
	default:
		return errors.Errorf("invalid status field")
	}
	if (inData.Status == structures.ResolvedStatus || inData.Status == structures.NotResolvedStatus) && inData.Comment == "" {
		return errors.Errorf("Для перевода в этот статус нужен комментарий")
	}
	return nil
}

// DestinationPointsListWithOptions godoc
func TicketsListWithOptions(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting ticket list by filter",
	})

	userUUID, memberType, err := memberUUIDFromJWT(c)
	if err != nil {
		log.WithField("reason", "error parse jwt")
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("error parse jwt", err))
	}
	log = log.WithFields(logrus.Fields{
		"user_uuid":   userUUID,
		"member_type": memberType,
	})

	criteria := new(models.TicketsFilterCriteria)
	criteria.Pager, err = getPager(c)
	if err != nil {
		log.WithField("reason", "error getting pager")
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("error getting pager", err))
	}

	if err := c.Bind(criteria); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	switch memberType {
	case structures.DriverMember:
		criteria.DriverUUID = userUUID
		criteria.SourceType = structures.DriverMember
		criteria.AllowedTaxiParks = taxiParkUUIDFromJWT(c)

	case structures.ClientMember:
		criteria.ClientPhone, err = clientPhoneFromJWT(c)
		if err != nil {
			msg := "error getting client phone from jwt"
			log.WithField("reason", msg).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
		}
		criteria.SourceType = structures.ClientMember

	case structures.UserCRMMember:
		userRole, err := userRoleFromJWT(c)
		if err != nil {
			msg := "error getting user's role from JWT"
			log.WithField("reason", msg).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
		}

		// чтобы эти роли могли видеть все тикеты
		if !(userRole == "super_admin" || userRole == "owner") {
			criteria.AllowedTaxiParks = getAllowedUUIDs(taxiParkUUIDFromJWT(c), []string{criteria.TaxiParkUUID})
		}
	}

	if storeUUID := storeUUIDFromJWT(c); storeUUID != "" {
		criteria.StoreUUID = storeUUID
	}

	tcks, recordsNumber, err := criteria.GetTicketsByOptions()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: tcks,
		Count:   recordsNumber,
	})
}
