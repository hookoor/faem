package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

func UpdateProduct(c echo.Context) error {
	var prod models.ProductCRM
	err := c.Bind(&prod)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "update product",
		"uuid":  uuid,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := prod.Update(uuid)
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		errWL.Print(log)

		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  errWL.Error.Error(),
		})
	}
	go rebuildProductCategoryIfNeed(prod.StoreUUID)
	prod.CreatedAtUnix = prod.CreatedAt.Unix()
	return c.JSON(http.StatusOK, prod)
}
func CreateProduct(c echo.Context) error {
	var prod models.ProductCRM
	err := c.Bind(&prod)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create product",
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := prod.Create()
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	go rebuildProductCategoryIfNeed(prod.StoreUUID)
	prod.CreatedAtUnix = prod.CreatedAt.Unix()
	return c.JSON(http.StatusOK, prod)
}
func CreateNewProductCategory(c echo.Context) error {
	var data models.ProductCategoriesCRM
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create prod category",
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := data.Create()
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, data)
}
func GetProductCategoriesByStoreUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get prod category",
	})
	data, err := models.GetAllProductCategoriesByStoreUUID(uuid)
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, data)
}
func GetAllProductCategories(c echo.Context) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get prod categories",
	})
	data, err := models.GetAllProductCategories()
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, data)
}
func SetProductCategoryDeleted(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "set product category deleted",
		"uuid":  uuid,
	})
	err := models.SetProductCategoryDeleted(uuid)
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func CreateStoreCategory(c echo.Context) error {
	var storeCategory models.StoreCategory
	err := c.Bind(&storeCategory)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "creating store category",
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := storeCategory.Create()
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, storeCategory)
}

func CreateStore(c echo.Context) error {
	var store models.StoreCRM
	err := c.Bind(&store)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create store",
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := store.Create()
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, store)
}

func UpdateStore(c echo.Context) error {
	var store models.StoreCRM
	uuid := c.Param("uuid")
	err := c.Bind(&store)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create store",
		"uuid":  uuid,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := store.Update(uuid)
	if errWL != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	store.CreatedAtUnix = store.CreatedAt.Unix()
	return c.JSON(http.StatusOK, store)
}

// UpdateStoreSerialPriority -
func UpdateStoreSerialPriority(c echo.Context) error {
	var err error
	ctx := c.Request().Context()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdateStoreSerialPriority",
	})
	var storeSerialPriority []models.StoreSerialPriority

	err = c.Bind(&storeSerialPriority)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}

	err = models.StoreCRM{}.UpdateSerialNumbers(ctx, storeSerialPriority)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "update serial number")
		return c.JSON(http.StatusNotFound, err.Error())
	}
	log.Info("UpdateStoreSerialPriority Done!")
	return c.JSON(http.StatusOK, "UpdateStoreSerialPriority Done!")
}

func StoreList(c echo.Context) error {
	var criteria models.StoreCriteria

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get stores by filter",
	})

	reqSource := c.Request().Header.Get("source")
	var takeStoreVisible = false
	if reqSource == constants.OrderSourceAndroid ||
		reqSource == constants.OrderSourceIOS ||
		reqSource == constants.OrderSourceDelivery {
		takeStoreVisible = true
	}

	err := c.Bind(&criteria)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	stores, count, err := criteria.GetStores(takeStoreVisible)
	if err != nil {
		msg := "error getting stores list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	for i := range stores {
		stores[i].CreatedAtUnix = stores[i].CreatedAt.Unix()
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: stores,
		Count:   count,
	})
}
func CategoriesList(c echo.Context) error {
	var criteria models.StoreCategoryCriteria

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get categories by filter",
	})
	err := c.Bind(&criteria)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	stores, count, err := criteria.GetCategories()
	if err != nil {
		msg := "error getting stores categories list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: stores,
		Count:   count,
	})
}
func StoreByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get store by uuid",
		"uuid":  uuid,
	})

	store, err := models.GetStoreByUUID(uuid)
	if err != nil {
		msg := "error getting store"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	store.CreatedAtUnix = store.CreatedAt.Unix()
	return c.JSON(http.StatusOK, store)
}
func StoreByURL(c echo.Context) error {
	url := c.Param("url")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting store by url",
		"uuid":  url,
	})

	store, err := models.GetStoreByURL(url)
	if err != nil {
		msg := "error getting store"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	store.CreatedAtUnix = store.CreatedAt.Unix()
	return c.JSON(http.StatusOK, store)
}
func GetProductByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get product by uuid",
		"uuid":  uuid,
	})

	prod, err := models.GetProductByUUID(uuid)
	if err != nil {
		msg := "error getting product"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	prod.CreatedAtUnix = prod.CreatedAt.Unix()
	return c.JSON(http.StatusOK, prod)
}
func SetProductDeletedByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "set product deleted by uuid",
		"uuid":  uuid,
	})
	storeUUID, err := models.SetProductDeleted(uuid)
	if err != nil {
		msg := "error deleting product"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	go rebuildProductCategoryIfNeed(storeUUID)
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
func SetStoreDeletedByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "set store deleted by uuid",
		"uuid":  uuid,
	})
	err := models.SetStoreDeleted(uuid)
	if err != nil {
		msg := "error deleting store"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func SetOrderNumber(c echo.Context) error {
	uuid := c.Param("uuid")
	var data struct {
		Number string `json:"number"`
	}
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "set order number in store",
		"userUUID":  userUUIDFromJWT(c),
		"orderUUID": uuid,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	order, errWL := orders.SetOrderNumberInStore(uuid, storeUUIDFromJWT(c), data.Number)
	if errWL != nil {
		log.WithFields(logrus.Fields{"reason": modelFuncErrorMsg})
		switch errWL.Level {
		case structures.WarningLevel:
			log.Warning(errWL.Error)
		case structures.ErrorLevel:
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	err = rabsender.SendJSONByAction(rabsender.AcUpdateOrderProductData, order)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send order number data to broker",
		}).Error(err)

		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func ConfirmOrder(c echo.Context) error {
	uuid := c.Param("uuid")
	var data struct {
		PrepareTime *int `json:"preparation_time"`
		OwnDelivery bool `json:"own_delivery"`
	}
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "confirm store by uuid",
		"prepare time": data.PrepareTime,
		"own delivery": data.OwnDelivery,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}

	errWL := orders.ConfirmOrder(uuid, storeUUIDFromJWT(c), data.PrepareTime, data.OwnDelivery)
	if errWL != nil {
		errWL.Print(log.WithField("reason", modelFuncErrorMsg))

		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func EstimateDelivery(c echo.Context) error {
	uuid, storeUUID := c.Param("uuid"), storeUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "estimate delivery time",
			"uuid":       uuid,
			"store uuid": storeUUID,
		})
	var in struct {
		DeliveryEstimationTime int64 `json:"delivery_estimation_time"`
	}

	if err := c.Bind(&in); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log = log.WithField("estimate delivery time", in.DeliveryEstimationTime)
	if err := orders.EstimateDelivery(c.Request().Context(), uuid, storeUUID, in.DeliveryEstimationTime); err != nil {
		log.WithField("reason", "can't estimate delivery time").Error(err)
		res := logs.OutputRestError("can't estimate delivery time", err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func FinishDelivery(c echo.Context) error {
	uuid, storeUUID := c.Param("uuid"), storeUUIDFromJWT(c)
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":      "finish order",
			"uuid":       uuid,
			"store uuid": storeUUID,
		})

	if err := orders.FinishDelivery(c.Request().Context(), uuid, storeUUID); err != nil {
		log.WithField("reason", "can't finish order").Error(err)
		res := logs.OutputRestError("can't finish order", err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func ProductsList(c echo.Context) error {
	var criteria models.ProductsCriteria
	err := c.Bind(&criteria)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "get products by filter",
		"storeUUID": criteria.StoreUUID,
		"category":  criteria.Category,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	res, count, err := criteria.GetProducts()
	if err != nil {
		msg := "error getting stores list"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	for i := range res {
		res[i].CreatedAtUnix = res[i].CreatedAt.Unix()
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: res,
		Count:   count,
	})
}

func ProductDataForOrder(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get products data for order",
	})

	inputData := make([]structures.ProductInputData, 0)
	if err := c.Bind(&inputData); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	res, err := models.ValidateAndGetProductsByInputData(inputData)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, res)
}

func rebuildProductCategoryIfNeed(storeUUID string) {
	err := models.RebuildProductCategoryIfNeed(storeUUID)
	if err == nil {
		return
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":  "add new prod category if need",
		"reason": "model func error",
	}).Error(err)
}
