package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"github.com/go-pg/pg/urlvalues"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/config"

	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

const (
	faemDeliveryHost            string = "d.faem.ru/"
	shortUUIDMaxLen             int    = 13
	endpointUnreadMessagesCount string = "/chat/history/unread"
)

// ReDistOrder godoc
func ReDistOrder(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order redistribution [crm]",
			"reason": "order UUID empty in request",
		}).Error("Order UUID empty in request")
		res := logs.OutputRestError("Order UUID empty", errors.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := orders.ReDistributionOrderValidate(uuid); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order redistribution [crm]",
			"orderUUID": uuid,
			"reason":    "validate error",
		}).Error(err)
		res := logs.OutputRestError("validate error", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	dur, _ := time.ParseDuration(config.St.Application.DistributingTime)
	distTime := time.Now().Add(dur)
	reDistData := structures.OrderReDistributionData{
		OrderUUID:    uuid,
		OperatorUUID: userUUIDFromJWT(c),
		DistData:     distTime,
	}
	err := rabsender.SendJSONByAction(rabsender.AcOrderReDistibution, reDistData)
	if err != nil {
		msg := "error sending data to broker"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order redistribution [crm]",
			"orderUUID": uuid,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := orders.UpdateOrderCancelTime(uuid, dur); err != nil {
		msg := "error update order cancel time"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order redistribution [crm]",
			"orderUUID": uuid,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// CreateOrder godoc
func CreateOrder(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "order creating [crm]",
	})

	order := new(orders.OrderCRM)
	if err := c.Bind(order); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	_, member, err := structures.MemberFromJWT(c)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	phoneIsInBlackList, err := models.IsPhoneInBlackList(order.Client.MainPhone)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	if phoneIsInBlackList {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  "Номер клиента находится в черном списке",
			Code: http.StatusBadRequest,
		})
	}

	if order.CallbackPhone != "" {
		formatNumber, err := phoneverify.NumberVerify(order.CallbackPhone)
		if err != nil {
			msg := "некорректный номер телефона для перезвона"
			logs.Eloger.WithFields(logrus.Fields{
				"reason":        msg,
				"callbackPhone": order.CallbackPhone,
			}).Error(err)
			res := logs.OutputRestError(msg, err)
			return c.JSON(http.StatusBadRequest, res)
		}
		order.CallbackPhone = formatNumber
	}

	storeUUID := storeUUIDFromJWT(c)
	if order.Source != constants.OrderSourceDelivery {
		if storeUUID != "" {
			msg := "Перейдите в партнерскую панель чтобы иметь возможность создать заказ"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)

			return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
				Msg:  msg,
				Code: http.StatusBadRequest,
			})
		}
		order.Source = constants.OrderSourceCRM
	}
	order.StoreUUID = storeUUID

	userUUID := userUUIDFromJWT(c)
	nOrder, err := order.Create(userUUID, member)
	if err != nil {
		log.WithField("reason", "error creating order").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("error creating [Order]", errpath.Err(err)))
	}
	log = log.WithField("orderUUID", nOrder.UUID)
	log.Infof("New Order created, From='%s'", nOrder.Routes[0].Value)

	sendData := structures.WrapperOrder{
		OperatorUUID: userUUID,
		Order:        nOrder.Order,
	}

	if nOrder.OrderState == constants.OrderStateCreated {
		err = rabsender.SendJSONByAction(rabsender.AcOrderToDriver, sendData)
		if err != nil {
			log.WithField("reason", "error sending order to broker").Error(err)
		}
	}

	if nOrder.SendSMS {
		neededPhone := nOrder.Client.MainPhone
		if nOrder.CallbackPhone != "" {
			neededPhone = nOrder.CallbackPhone
		}

		text := fmt.Sprintf("К вам едет доставка. Отслеживание - %s", faemDeliveryHost+nOrder.UUID)
		err = rabsender.SendJSONByAction(rabsender.AcNewSMS, structures.NewSMSData{
			Text:  text,
			Phone: neededPhone,
		})

		if err != nil {
			log.WithField("reason", "error sending order to broker").Error(err)
		}
	}
	log.Debug("Order send to broker")

	// Fill calculated fields
	nOrder.StateTitle = constants.OrderStateRU[nOrder.OrderState]
	nOrder.FillPromotionFields()
	nOrder.FillUnixField()
	models.FillTaxiParkData(nOrder)

	return c.JSON(http.StatusOK, nOrder)
}
func CreateOrderFromClient(c echo.Context) error {

	var order orders.OrderCRM

	err := c.Bind(&order)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  err.Error(),
			Code: http.StatusBadRequest,
		})
	}
	phoneIsInBlackList, err := models.IsPhoneInBlackList(order.Client.MainPhone)
	if err != nil {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  err.Error(),
			Code: http.StatusBadRequest,
		})
	}
	if phoneIsInBlackList {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  "Номер клиента находится в черном списке",
			Code: http.StatusBadRequest,
		})
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "order from client create",
		"orderUUID":   order.UUID,
		"clientPhone": order.Client.MainPhone,
	})

	if order.SourceOfOrdersUUID == "" {
		order.SourceOfOrdersUUID = c.Request().Header.Get(constants.HeaderSourceUUID)
	}

	// Через время вернуть этот блок, закомменчен для обратной совместимости
	// if OrderWrap.SourceOfOrdersUUID == "" {
	// 	log.WithError(ErrEmptySrcUUID).Error()
	// 	return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptySrcUUID.Error(), ErrEmptySrcUUID))
	// }

	order.StoreUUID = order.GetProductsData().StoreData.UUID

	order, err = order.SaveNewOrder()
	if err != nil {
		msg := "Error saving order to db"

		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	log.WithFields(logrus.Fields{"prodDelivery": order.Service.ProductDelivery}).Info("order taken")

	if order.Service.ProductDelivery {
		return c.JSON(http.StatusOK, structures.ResponseStructOk)
	}

	err = order.SendToDriver()
	if err != nil {
		msg := "Error sending order to broker"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	log.Debug("New order data sent to broker")
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// CreateOrder godoc
func ReCreateOrdersByState(c echo.Context) error {
	var input struct {
		State                    string `json:"state"`
		SecondAfterStateTransfer int64  `json:"second_after_state_transfer"`
	}
	userUUID := userUUIDFromJWT(c)
	err := c.Bind(&input)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "orders recreating",
		"user_uuid": userUUID,
		"state":     input.State,
		"seconds":   input.SecondAfterStateTransfer,
	})

	if err != nil {
		log.WithFields(logrus.Fields{"reason": bindingDataErrorMsg}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(bindingDataErrorMsg, err))
	}
	ordersNumber, err := orders.ReCreateOrdersByState(input.State, constants.CancelReasonSystemError, userUUID, input.SecondAfterStateTransfer)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order creating [crm]",
			"reason": "Error creating order",
		}).Error(err)

		res := logs.OutputRestError("Error creating [Order]", errpath.Err(err))
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  fmt.Sprintf("Пересоздано заказов - %v", ordersNumber),
	})
}

// CancelOrder godoc
func CancelOrder(c echo.Context) error {
	var (
		reason orders.CancelRequest
	)

	uuid := c.Param("uuid")
	if uuid == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order cancel [crm]",
			"reason": "Order UUID empty in request",
		}).Error("Order UUID empty in request")
		res := logs.OutputRestError("Order UUID empty", errors.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}
	err := c.Bind(&reason)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order cancel [crm]",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Order]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	OrderState, err := orders.CancelOrder(reason.Reason, uuid)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "cancel order [crm]",
			"orderUUID": OrderState.OrderUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	if counter, err := orders.GetCounterOrderMarkerForOrder(OrderState.OrderUUID); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "cancel order [crm]",
			"orderUUID": OrderState.OrderUUID,
		}).Error(err)
	} else if counter {
		OrderState.Flags |= structures.OfferStateFlagCounterOrder
	}
	OrderState.OperatorUUID = userUUIDFromJWT(c)
	err = rabsender.SendJSONByAction(rabsender.AcOrderState, OrderState)
	if err != nil {
		res := fmt.Sprintf("Error sending driverState to broker. %s", err)
		logs.OutputError(res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK",
	})
}

// UpdateOrder godoc
func InformDriverAboutOrderReady(c echo.Context) error {
	uuid := c.Param("uuid")
	userStoreUUID := storeUUIDFromJWT(c)
	var message struct {
		Message string `json:"message"`
	}
	err := c.Bind(&message)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":         "inform driver about order ready",
		"orderUUID":     uuid,
		"userStoreUUID": userStoreUUID,
	})
	if uuid == "" {
		err = errors.Errorf(emptyUUIDMsg)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	errWL := orders.InformDriverAboutOrderReadiness(uuid, userStoreUUID, message.Message)
	if errWL != nil {
		msg := "model func errof"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// UpdateOrder godoc
func UpdateOrder(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order updating [crm]",
			"reason": "Order UUID empty in request",
		}).Error("Order UUID empty in request")
		res := logs.OutputRestError("Order UUID empty", errors.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}
	var nOrder orders.OrderCRM

	var order orders.OrderCRM
	err := c.Bind(&order)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order updating [crm]",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Order]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	userID := userIDFromJWT(c)
	state := c.QueryParam("state")
	if state != "" {
		var err error
		nOrder, err = orders.SetStateByUser(uuid, state, userID)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "order updating [crm]",
				"reason": "error setting order state",
			}).Error(err)
			res := logs.OutputRestError("error setting order state", err)
			return c.JSON(http.StatusBadRequest, res)
		}
		msg := fmt.Sprintf("Order UUID='%s', New state='%s'", nOrder.UUID, nOrder.OrderState)
		// logs.OutputEvent("Order state changed", msg, userID)
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order updating [crm]",
			"orderUUID": nOrder.UUID,
			"reason":    "requesting changing state",
			"value":     nOrder.OrderState,
		}).Info(msg)
	}
	uOrder, err := order.Update(uuid, taxiParkUUIDFromJWT(c))
	if err != nil {
		fr := fmt.Sprintf("Cant update [OrderID=%v]", uOrder.UUID)
		res := logs.OutputRestError(fr, err)

		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order updating [crm]",
			"orderUUID": nOrder.UUID,
		}).Error(err)

		return c.JSON(http.StatusBadRequest, res)
	}

	userUUID := userUUIDFromJWT(c)
	var sendData structures.WrapperOrder
	sendData.OperatorUUID = userUUID
	sendData.Order = uOrder.Order

	err = rabsender.SendJSONByAction(rabsender.AcNewOrderData, sendData, rabbit.OrderUpdateByOperatorRequestTag)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order updating [crm]",
			"reason":    "Error sending order to broker",
			"orderUUID": uOrder.UUID,
		}).Error(err)
	}

	// Fill calculated fields
	uOrder.StateTitle = constants.OrderStateRU[uOrder.OrderState]
	uOrder.FillPromotionFields()
	// Когда нужно будет изменять параметры заказа код ставим сюда
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "order updating [crm]",
		"orderUUID": nOrder.UUID,
	}).Info("Order updated")
	uOrder.FillUnixField()
	uOrder = orders.FillTripTime(uOrder)[0]
	models.FillTaxiParkData(&uOrder)

	return c.JSON(http.StatusOK, uOrder)
}

// OrdersList godoc
func OrdersList(c echo.Context) error {
	var (
		Orders []orders.OrderCRM
		pager  urlvalues.Pager
	)
	pager, err := getPager(c)
	if err != nil {
		fr := fmt.Sprintf("error getting pager")
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "get order list",
			"reason": fr,
		}).Error(err)
		res := logs.OutputRestError(fr, err, 404)
		return c.JSON(http.StatusNotFound, res)
	}
	allowedTPUUIDs := taxiParkUUIDFromJWT(c)
	Orders, recordsNumber, err := orders.OrderList(pager, allowedTPUUIDs)
	if err != nil {
		fr := fmt.Sprintf("Error getting order list")
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "get order list",
			"reason": fr,
		}).Error(err)
		res := logs.OutputRestError(fr, err, 404)
		return c.JSON(http.StatusNotFound, res)
	}
	// Fill calculated fields
	for i := range Orders {
		Orders[i].StateTitle = constants.OrderStateRU[Orders[i].OrderState]
		Orders[i].FillPromotionFields()
		Orders[i].FillUnixField()
	}
	Orders = orders.FillTripTime(Orders...)
	models.FillTaxiParksData(GetODArray(Orders))
	return c.JSON(http.StatusOK, OrdersWithCount{
		Orders: Orders,
		Count:  recordsNumber,
	})
}

func restrictAccessInOrderCriteria(cr *orders.Criteria, c echo.Context) error {
	jwtRegIDs, err := regionsIDFromJWT(c)
	if err != nil {
		return errors.Wrap(err, ErrGetAllowedRegions.Error())
	}
	cr.RegionIDs = getAllowedIDs(jwtRegIDs, cr.RegionIDs)

	cr.TaxiParksUUID = getAllowedUUIDs(taxiParkUUIDFromJWT(c), cr.TaxiParksUUID)
	if len(cr.TaxiParksUUID) == 0 {
		return errors.New("you do not have authorized taxiparks")
	}

	if cr.StoreUUID == "" {
		cr.StoreUUID = storeUUIDFromJWT(c)
	}

	userRole, err := userRoleFromJWT(c)
	if err != nil || userRole == "" {
		return errors.Wrap(err, "error getting or empty userrole")
	}
	cr.UserRole = userRole

	cr.Pager.Offset = (cr.Pager.Page - 1) * cr.Pager.Limit
	cr.Pager.MaxLimit = 50

	return nil
}

// OrdersListWithOptions godoc
func OrdersListWithOptions(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting orders list by filter",
		"userUUID": userUUIDFromJWT(c),
	})

	//TODO: переделать эту шнягу под стандартный бинд echo
	//DONE: спустя 9 месяцев переделали))
	criteria := new(orders.Criteria)
	if err := c.Bind(criteria); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if err := restrictAccessInOrderCriteria(criteria, c); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	Orders, recordsNumber, err := criteria.OrdersListWithOptions()
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	// Fill calculated fields
	for i := range Orders {
		Orders[i].StateTitle = constants.OrderStateRU[Orders[i].OrderState]
		Orders[i].FillPromotionFields()
		Orders[i].FillUnixField()
	}
	Orders = orders.FillTripTime(Orders...)

	if err := setUnreadMessagesCount(c, Orders); err != nil {
		logrus.Warnln(errpath.Err(err))
	}
	models.FillTaxiParksData(GetODArray(Orders))

	return c.JSON(http.StatusOK, OrdersWithCount{
		Orders: Orders,
		Count:  recordsNumber,
	})
}

func setUnreadMessagesCount(c echo.Context, orders []orders.OrderCRM) error {
	if len(orders) != 0 {

		header := map[string]string{
			"Authorization": c.Request().Header.Get(echo.HeaderAuthorization),
		}

		var body structures.OrdersUUIDForUnreadMessages
		for _, item := range orders {
			body.OrderUUIDs = append(body.OrderUUIDs, item.Order.UUID)
		}
		var chmes map[string]int
		err := tool.SendRequest(
			http.MethodPost,
			config.St.Chat.Host+endpointUnreadMessagesCount,
			header,
			body,
			&chmes,
		)
		if err != nil {
			return errpath.Err(err)
		}

		if len(chmes) != 0 {
			for i := range orders {
				if val, ok := chmes[orders[i].UUID]; ok {
					orders[i].UnreadMessagesCount = val
				}
			}
		}
	}
	return nil
}

// UpdateOrderTariff godoc
func UpdateOrderTariff(c echo.Context) error {
	var (
		tariff structures.Tariff
	)
	uuid := c.Param("uuid")
	err := c.Bind(&tariff)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "update order tariff",
		"orderUUID":  uuid,
		"totalPrice": tariff.TotalPrice,
	})
	if uuid == "" {
		err = errors.Errorf("empty uuid")
	}
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = orders.UpdateOrderTariff(uuid, tariff)
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, 404)
		return c.JSON(http.StatusNotFound, res)
	}
	log.Info("tariff updated")
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// GetOrderCurrentState godoc
func GetOrderCurrentState(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "getting order current state by uuid",
		"orderUUID": uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	result, err := orders.GetOrderCurrentState(uuid)
	if err != nil {
		msg := "error getting order state"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, result)
}

// GetOrderTariffByUUID godoc
func GetOrderTariffByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "getting order tariff uuid",
		"orderUUID": uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	result, err := orders.GetOrderTariff(uuid)
	if err != nil {
		msg := "error getting order tariff"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, result)
}

// GetTariff godoc
func GetTariff(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(ctx).WithField("event", "getting tariff")

	ordWrap := &struct {
		orders.OrderCRM
		OSRM structures.OSRMData `json:"osrm,omitempty"`
	}{}
	if err := c.Bind(ordWrap); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if ordWrap.CreatedAtUnix != 0 {
		ordWrap.OrderStart = localtime.TimeInZone(time.Unix(ordWrap.CreatedAtUnix, 0), config.St.Application.TimeZone)
	}
	if ordWrap.Source == "" {
		ordWrap.Source = c.Request().Header.Get(structures.OrderSourceKey)
	}

	// FIXME временный костыль, нужно разобраться с SourceOfOrdersUUID при внутренней передаче структуры заказа для перерасчетов
	if ordWrap.UUID != "" {
		ordWrap.LoadSourceUUID()
	}

	if ordWrap.SourceOfOrdersUUID == "" {
		ordWrap.SourceOfOrdersUUID = c.Request().Header.Get(constants.HeaderSourceUUID)
	}

	// Через время вернуть этот блок, закомменчен для обратной совместимости
	// if OrderWrap.SourceOfOrdersUUID == "" {
	// 	log.WithError(ErrEmptySrcUUID).Error()
	// 	return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptySrcUUID.Error(), ErrEmptySrcUUID))
	// }

	tariff, err := orders.GetTariffWithBonuses(&ordWrap.OrderCRM)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, tariff)
}

// GetOrderWithClHistory godoc
func GetOrderWithClHistory(c echo.Context) error {
	orUUUID := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "get order with client history handler",
		"orderUUID": orUUUID,
	})
	result, err := orders.GetOrderWithClientHistory(orUUUID)
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, result)
}

// GetTariffs godoc
func GetTariffs(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting all tariffs",
	})

	order := new(orders.OrderCRM)
	if err := c.Bind(order); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if order.Client.MainPhone == "" {
		phone, err := clientPhoneFromJWTIfExists(c)
		if err != nil {
			//TODO: добавить обработку для незареганных челиков
		}
		order.Client.MainPhone = phone
	}
	order.Source = c.Request().Header.Get(structures.OrderSourceKey)

	if order.SourceOfOrdersUUID == "" {
		order.SourceOfOrdersUUID = c.Request().Header.Get(constants.HeaderSourceUUID)
	}
	// Через время вернуть этот блок, закомменчен для обратной совместимости
	// if Order.SourceOfOrdersUUID == "" {
	// 	log.WithError(ErrEmptySrcUUID).Error()
	// 	return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptySrcUUID.Error(), ErrEmptySrcUUID))
	// }

	tariffs, err := orders.GetAllTariffs(order, c.RealIP())
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	var exceptDebug []orders.ShortTariff
	for _, item := range tariffs {
		if item.Name == "Дебаг" {
			continue
		}
		exceptDebug = append(exceptDebug, item)
	}
	tariffs = exceptDebug
	return c.JSON(http.StatusOK, tariffs)
}

// GetOrderByShortUUID godoc
func GetOrderByShortUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting order by short uuid",
		"uuid":  uuid,
	})
	if uuid == "" || len(uuid) > shortUUIDMaxLen {
		msg := "invalid uuid"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError("", errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var ord orders.OrderCRM
	err := models.GetByUUID(uuid, &ord)
	if err != nil {
		msg := "error getting order"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	// Fill calculated fields
	ord.OrderState = constants.ReplaceStateForClientIfNeed(ord.OrderState, ord.Service.ProductDelivery)
	ord.StateTitle = constants.TranslateOrderStateForClient(ord.OrderState, ord.Service.ProductDelivery)
	ord.FillPromotionFields()
	ord.FillUnixField()
	ord = orders.FillTripTime(ord)[0]
	models.FillTaxiParkData(&ord)

	return c.JSON(http.StatusOK, ord)
}

// GetOrder godoc
func GetOrder(c echo.Context) error {
	var Order orders.OrderCRM
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order by uuid",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	err := models.GetByUUID(uuid, &Order)
	if err != nil {
		msg := "error getting order"
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "getting order by uuid",
			"orderUUID": uuid,
			"reason":    msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	// Fill calculated fields
	Order.StateTitle = constants.OrderStateRU[Order.OrderState]
	Order.FillPromotionFields()
	Order.FillUnixField()
	Order = orders.FillTripTime(Order)[0]
	models.FillTaxiParkData(&Order)
	return c.JSON(http.StatusOK, Order)
}

// GetOptions godoc
func GetOptions(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting options",
	})

	var (
		err                       error
		isStoreUser               bool
		serviceUUID               string
		tpUUIDs                   []string
		options                   models.Options
		reasons                   models.ReasonsForFront
		source                    models.OrderSourceForFront
		driverStates, orderStates models.StatesForFront
	)

	memuuid, memtype, err := memberUUIDFromJWT(c)
	if err != nil {
		log.WithError(err).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(err.Error(), err))
	}

	if memtype == structures.UserCRMMember {
		tpUUIDs = taxiParkUUIDFromJWT(c)
		isStoreUser = storeUUIDFromJWT(c) != ""
	}

	services, err := models.ServicesList(isStoreUser)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	if isStoreUser && len(options.Services) != 0 {
		serviceUUID = options.Services[0].UUID
	}

	options.Features, err = models.FeatureList(serviceUUID)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	if memtype == structures.ClientMember {
		for _, s := range services {
			if s.Visibility.ForClient {
				options.Services = append(options.Services, s)
			}
		}
	}
	if memtype == structures.DriverMember {
		for _, s := range services {
			if s.Visibility.ForDriver {
				options.Services = append(options.Services, s)
			}
		}
	}
	if memtype == structures.UserCRMMember {
		for _, s := range services {
			if s.Visibility.ForOperator {
				options.Services = append(options.Services, s)
			}
		}
	}

	options.SourcesOfOrders, err = models.PhoneLinesList(tpUUIDs...)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	for i := range options.SourcesOfOrders {
		err := options.SourcesOfOrders[i].FillServices()
		if err != nil {
			log.WithError(ErrModelFunc).Error(err)
			return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
		}
	}

	allowedRegions, err := regionsIDFromJWT(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	allowedTaxiParks := taxiParkUUIDFromJWT(c)

	options.Regions, err = models.GetRegionsForFront(allowedRegions, allowedTaxiParks)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	{ //
		var driverGroups []models.DriverGroup
		driverGroups, err = models.DriverGroup{}.GetGroupsList()
		if err != nil {
			log.WithError(ErrModelFunc).Error(err)
			return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
		}
		var shortDriverGroups []models.ShortDriverGroup
		for _, dg := range driverGroups {
			shortDriverGroups = append(shortDriverGroups, models.ShortDriverGroup{Name: dg.Name, UUID: dg.UUID})
		}
		options.DriverGroups = shortDriverGroups
	}

	for i, value := range constants.DriverStateRU {
		driverStates.StateName = i
		driverStates.StateTitle = value
		options.States.DriverStates = append(options.States.DriverStates, driverStates)
	}

	for i, value := range constants.OrderStateRU {
		orderStates.StateName = i
		orderStates.StateTitle = value
		options.States.OrderStates = append(options.States.OrderStates, orderStates)
	}
	crmEditableDriverStates := []string{constants.DriverStateAvailable, constants.DriverStateBlocked}
	for _, state := range crmEditableDriverStates {
		driverStates.StateName = state
		driverStates.StateTitle = constants.DriverStateRU[state]
		options.States.DriverStatesEditable = append(options.States.DriverStatesEditable, driverStates)
	}
	for _, value := range constants.ListCrmCancelReasons() {
		reasons.ReasonName = value
		reasons.ReasonTitle = constants.CancelReasonRU[value]
		options.ReasonsForCancel = append(options.ReasonsForCancel, reasons)
	}

	for _, value := range constants.ListOrderSources() {
		source.Name = value
		source.Title = constants.OrderSourceRU[value]
		options.OrderSources = append(options.OrderSources, source)
	}
	if memtype == structures.DriverMember {
		var driver models.DriverCRM
		err := models.GetByUUID(memuuid, &driver)
		if err != nil {
			log.WithError(ErrModelFunc).Error(err)
			return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
		}
		var group models.DriverGroup

		if driver.Group.UUID == "" {
			log.WithField("driverUUID", driver.UUID).Warnln("driver group uuid is empty")

			err = group.GetByName(models.DefaulDriverGroup)
			if err != nil {
				log.WithError(ErrModelFunc).Error(err)
				return c.JSON(http.StatusNotFound, logs.OutputRestError(ErrModelFunc.Error(), err))
			}
		}

		models.FillAvailabilityForDriver(options.Services, driver.Group.ServicesUUID)
		var servicesExceptDebug []models.ServiceCRM
		for _, item := range options.Services {
			if item.Name == "Дебаг" {
				continue
			}
			servicesExceptDebug = append(servicesExceptDebug, item)
		}

		//Фильтруем услуги для водителя по группам
		filteredServices := models.FilterServicesForDriver(servicesExceptDebug, driver.Group.ServicesUUID)
		if memtype == structures.DriverMember {
			var servs []models.ServiceCRM
			for _, s := range filteredServices {
				if s.Visibility.ForDriver {
					servs = append(servs, s)
				}
			}
			options.Services = servs
		}
		//Фильтруем опции для водителя по услугам
		filteredFeatures := models.FilterFeaturesForDriver(driver.AvailableServices, options.Features)
		options.Features = filteredFeatures
	}
	return c.JSON(http.StatusOK, options)
}

// GetOrdersByUser godoc
func GetOrdersByUser(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order by user",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var Orders []orders.OrderCRM
	Orders, err := orders.OrderByUser(uuid)
	if err != nil {
		msg := "error getting order by user"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order by user",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	// Fill calculated fields
	for i := range Orders {
		Orders[i].StateTitle = constants.OrderStateRU[Orders[i].OrderState]
		Orders[i].FillPromotionFields()
		Orders[i].FillUnixField()
	}
	Orders = orders.FillTripTime(Orders...)
	models.FillTaxiParksData(GetODArray(Orders))
	return c.JSON(http.StatusOK, Orders)
}

// GetOrdersByDriver возвращет все заказыЮ принятые
func GetOrdersByDriver(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order by driver",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var Orders []orders.OrderCRM
	Orders, err := orders.OrdersByDriver(uuid)
	if err != nil {
		msg := "error getting order by driver"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order by driver",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	// Fill calculated fields
	for i := range Orders {
		Orders[i].StateTitle = constants.OrderStateRU[Orders[i].OrderState]
		Orders[i].FillPromotionFields()
		Orders[i].FillUnixField()
	}
	Orders = orders.FillTripTime(Orders...)
	models.FillTaxiParksData(GetODArray(Orders))
	return c.JSON(http.StatusOK, Orders)
}

// DeleteOrder godoc
func DeleteOrder(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "delete order",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var Order orders.OrderCRM
	Order.UUID = uuid

	errWL := Order.SetDeleted(taxiParkUUIDFromJWT(c))
	if errWL != nil {
		res := logs.OutputRestError("Cant delete [Order]", errWL.Error)

		errWL.Print(
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "order deleting [crm]",
				"orderUUID": Order.UUID,
			}))

		return c.JSON(http.StatusBadRequest, res)
	}

	msg := fmt.Sprintf("Order deleted ID=%v", Order.UUID)
	uid := userIDFromJWT(c)
	logs.OutputEvent("Order deleted", msg, uid)

	res := logs.OutputRestOK("Order Delete")

	logs.Eloger.WithFields(logrus.Fields{
		"event":     "order deleting [crm]",
		"orderUUID": Order.UUID,
	}).Info("Order set as deleted")

	return c.JSON(http.StatusOK, res)
}

// DriverCurrentOrder -
func DriverCurrentOrder(c echo.Context) error {
	driverUUID := c.Param("driveruuid")
	result, err := orders.GetDriverCurrentOrder(driverUUID, false)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in state", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, result)
}

func GetDriversCurrentOrders(c echo.Context) error {
	var body struct {
		DriverUUIDs []string `query:"driver_uuid"`
	}
	if err := c.Bind(&body); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	result, err := orders.GetDriversCurrentOrderUUIDs(body.DriverUUIDs...)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, result)
}

// GetDriverCounterOrder -
func GetDriverCounterOrder(c echo.Context) error {
	driverUUID := c.Param("driveruuid")

	result, err := orders.GetDriverCurrentOrder(driverUUID, true)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in state", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, result)
}

func OrderStateDataByState(c echo.Context) error {
	state := c.Param("state")
	uuids, err := orders.OrdersDataByState(state)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in state", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, uuids)
}

func OrderStateDataByStates(c echo.Context) error {
	var args structures.OrdersDataByStatesArgs
	if err := c.Bind(&args); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order state data by states [crm]",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [OrderStateDataByStates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	var regionID []int
	if args.RegionID > 0 {
		regionID = append(regionID, args.RegionID)
	}
	ordrs, err := orders.OrdersDataByStates(args.States, regionID...)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in states", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, ordrs)
}

func UUIDByState(c echo.Context) error {
	state := c.Param("state")
	uuids, err := orders.UUIDByStates(state)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in state", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, uuids)
}

func MarkOrderAsDelivered(c echo.Context) error {
	memUUID, memtype, err := memberUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "mark order as delivered",
		"memtype":   memtype,
		"orderUUID": uuid,
		"memUUID":   memUUID,
	})
	if err == nil && memtype != structures.DriverMember {
		err = errors.New("invalid member type")
	}
	if err != nil {
		msg := "jwt error"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	err = orders.MarkAsDelivered(uuid, memUUID)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": modelFuncErrorMsg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(modelFuncErrorMsg, err))
	}
	go func() {
		err := rabsender.SendJSONByAction(rabsender.OrderDeliveredToDriver, structures.OrderDeliveredMessage{
			OrderUUID:  uuid,
			DriverUUID: memUUID,
		})
		if err != nil {
			log.WithFields(logrus.Fields{"reason": "error send data to broker"}).Error(err)
		}
	}()
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func GetUnDeliveredOrdersUUID(c echo.Context) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get undelivered orders uuid",
	})
	allowedTPUUIDs := taxiParkUUIDFromJWT(c)
	result, err := orders.GetUndeliveredOrderUUID(allowedTPUUIDs)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": modelFuncErrorMsg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(modelFuncErrorMsg, err))
	}
	return c.JSON(http.StatusOK, result)
}
func GetClientOrderData(c echo.Context) error {
	clUUID := c.Param("clientuuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":  "getting client order data",
		"clUUID": clUUID,
	})
	result, err := orders.GetOrdersDataByClientUUID(clUUID)
	if err != nil {
		msg := "Cant set order state"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, result)
}
func SetOrderStatus(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "set order status",
	})

	orderState := new(structures.OfferStates)
	if err := c.Bind(orderState); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	log = log.WithFields(logrus.Fields{
		"orderUUID": orderState.OrderUUID,
		"offerUUID": orderState.OfferUUID,
		"state":     orderState.State,
	})

	if err := orders.SetOrderState(orderState); err != nil {
		msg := "Cant set order state"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.Info("set order state successfully")

	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func UnmarkOrderAsImportant(c echo.Context) error {
	uuid := c.Param("uuid")
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "unmark order as important",
		"userUUID": userUUID,
	})
	err := orders.UnmarkAsImportant(uuid)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": modelFuncErrorMsg}).Error(err)

		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	go func() {
		err := rabsender.SendJSONByAction(rabsender.AcOperatorUnmarkOrderAsImportant, structures.UnmarkOrderAsImportant{
			OrderUUID:    uuid,
			OperatorUUID: userUUID,
		})
		if err != nil {
			log.WithFields(logrus.Fields{"reason": "error send data to broker"}).Error(err)
		}
	}()
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// ---------
// ---------
// ---------

// SetCounterOrderMarkerForOrder -
func SetCounterOrderMarkerForOrder(c echo.Context) error {
	var err error

	log := logs.Eloger.WithField("event", "SetCounterOrderMarkerForOrder")

	orderUUID := c.Param("orderuuid")
	var bind struct {
		IsCounterOrder         bool `json:"is_counter_order"`
		UpdatePersistentMarker bool `json:"update_persistent_marker"`
	}
	err = c.Bind(&bind)
	if err != nil {
		log.Errorln(errpath.Err(err))
		res := logs.OutputRestError("binding error", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.WithFields(logrus.Fields{
		"orderUUID":      orderUUID,
		"IsCounterOrder": bind.IsCounterOrder,
	})

	err = orders.SetCounterOrderMarkerForOrder(orderUUID, bind.IsCounterOrder, bind.UpdatePersistentMarker)
	if err != nil {
		log.Errorln(errpath.Err(err))
		res := logs.OutputRestError("Cant set counter order marker", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("SetCounterOrderMarkerForOrder DONE!")

	return c.JSON(http.StatusOK, nil)
}

// GetCounterOrderMarkerForOrder -
func GetCounterOrderMarkerForOrder(c echo.Context) error {
	log := logs.Eloger.WithField("event", "GetCounterOrderMarkerForOrder")

	orderUUID := c.Param("orderuuid")
	marker, err := orders.GetCounterOrderMarkerForOrder(orderUUID)
	if err != nil {
		log.Errorln(errpath.Err(err))
		res := logs.OutputRestError("Cant set counter order marker", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, marker)
}

// ---------
// ---------
// ---------

// CreateFuelOrder -
func CreateFuelOrder(c echo.Context) error {
	log := logs.Eloger.WithField("event", "CreateFuelOrder")

	order := new(orders.OrderCRM)
	if err := c.Bind(order); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	userUUID, member, err := structures.MemberFromJWT(c)
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}
	order.Client.UUID = userUUID
	order.Client.Name = string(member)

	phoneIsInBlackList, err := models.IsPhoneInBlackList(order.Client.MainPhone)
	if err != nil {
		err = errpath.Err(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  err.Error(),
			Code: http.StatusBadRequest,
		})
	}
	if phoneIsInBlackList {
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Msg:  "Номер клиента находится в черном списке",
			Code: http.StatusBadRequest,
		})
	}

	nOrder, err := order.Create(userUUID, member)
	if err != nil {
		err = errpath.Err(err)
		msg := "Error saving order to db"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if nOrder.Service.ProductDelivery {
		return c.JSON(http.StatusOK, structures.ResponseStructOk)
	}

	if err := nOrder.SendToDriver(); err != nil {
		msg := "Error sending order to broker"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.Debug("New order data sent to broker")

	return c.JSON(http.StatusOK, order)
}

// ---------
// ---------
// ---------
