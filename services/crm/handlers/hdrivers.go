package handlers

import (
	"context"
	"encoding/csv"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

const (
	msgSetBalance = "Изменение баланса администратором"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

type ControllerDrivers struct {
	DriversCRUD      *ControllerDriversCRUD
	ServicesRelation *ControllerDriversServicesRelation
	FeaturesRelation *ControllerDriversFeaturesRelation

	dbi *models.DB
}

func NewControllerDrivers(db *models.DB) *ControllerDrivers {
	return &ControllerDrivers{
		DriversCRUD:      NewControllerDriversCRUD(db),
		ServicesRelation: NewControllerDriversServicesRelation(db),
		FeaturesRelation: NewControllerDriversFeaturesRelation(db),
		dbi:              db,
	}
}

type ControllerDriversCRUD struct {
	dbi *models.DB
}

func NewControllerDriversCRUD(db *models.DB) *ControllerDriversCRUD {
	return &ControllerDriversCRUD{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// CreateDriver godoc
func CreateDriver(c echo.Context) error {
	ctx := c.Request().Context()
	userTPUUID := taxiParkUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "create driver",
		"taxiParkUUID": userTPUUID,
	})

	driver := new(models.DriverCRM)
	if err := c.Bind(driver); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	formatNumber, err := phoneverify.NumberVerify(driver.Phone)
	if err != nil {
		msg := "Некорректный номер телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
			"phone":  driver.Phone,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	driver.Phone = formatNumber

	nDriver, err := driver.Create(ctx)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}
	log = log.WithField("driverUUID", nDriver.UUID)

	nDriver.ActiveFeaturesUUIDs = driver.ActiveFeaturesUUIDs // заполнение-ответа активными опциями
	nDriver.ActiveServicesUUIDs = driver.ActiveServicesUUIDs // заполнение-ответа активными услугами

	err = rabsender.SendJSONByAction(rabsender.AcNewDriverToDriver, nDriver.Driver)
	if err != nil {
		log.WithField("reason", "error send driver to broker").Error(err)
	}
	log.Info("driver create successfully")

	driver.StateTitle = constants.DriverStateRU[driver.Status]
	models.FillDriversCreatedAtUnix(nDriver)
	models.FillTaxiParkData(&nDriver)

	return c.JSON(http.StatusOK, nDriver)
}

// DriversList godoc
func DriversList(c echo.Context) error {
	var driver []models.DriverCRM
	driver, err := models.DriversList()
	if err != nil {
		msg := "error getting drivers"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "drivers list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	for i := range driver {
		driver[i].StateTitle = constants.DriverStateRU[driver[i].Status]
	}
	models.FillDriversCreatedAtUnix(driver...)
	return c.JSON(http.StatusOK, driver)
}

// GetDriver godoc
func (ctrl *ControllerDriversCRUD) GetDriver(c echo.Context) error {
	ctx := c.Request().Context()
	memberUUID, memberType, err := memberUUIDFromJWT(c)

	var uuid string
	var needFillData bool
	switch memberType {
	case structures.DriverMember:
		uuid = memberUUID
	case structures.UserCRMMember:
		needFillData = true
		uuid = c.Param("uuid")
	default:
		err = errors.Errorf("invalid jwt type")
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "get driver by uuid",
		"memberUUID": memberUUID,
		"memberType": memberType,
		"driverUUID": uuid,
	})
	if err != nil {
		msg := "jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var driver models.DriverCRM

	err = driver.GetDriverByUUID(uuid)
	if err != nil {
		msg := "error getting driver by uuid"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	{ // заполнение данных о активных опциях из таблицы связей
		activeFeatures, err := ctrl.dbi.DriverInteraction.FeatureRealation.GetFeaturesByDriver(ctx, uuid)
		if err != nil {
			log.Errorln(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

		for _, f := range activeFeatures {
			driver.ActiveFeaturesUUIDs = append(driver.ActiveFeaturesUUIDs, f.UUID)
		}
	}
	{ // заполнение данных о активных услугах из таблицы связей
		activeServices, err := ctrl.dbi.DriverInteraction.ServiceRealation.GetServicesByDriver(ctx, uuid)
		if err != nil {
			log.Errorln(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

		for _, f := range activeServices {
			driver.ActiveServicesUUIDs = append(driver.ActiveServicesUUIDs, f.UUID)
		}
	}

	if needFillData {
		drivers, err := models.FillDriverLocation([]models.DriverCRM{driver})
		if err != nil {
			msg := "error filling driver location"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
			res := logs.OutputRestError(msg, err, http.StatusNotFound)
			return c.JSON(http.StatusNotFound, res)
		}
		driver = drivers[0]
		drivers, err = models.FillDriversCurrentAppVersion([]models.DriverCRM{driver})
		if err != nil {
			msg := "error filling driver location"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
		}
		models.FillDriversCreatedAtUnix(drivers...)
		driver = drivers[0]
	}
	driver.StateTitle = constants.DriverStateRU[driver.Status]

	models.FillTaxiParkData(&driver)

	{ // подмена названия тариффа на название надбавки если есть сопастовление по таксопарку

		for _, el := range driver.DrvTariff.Markups {
			if driver.TaxiParkData.UUID == el.TaxiParkUUID {
				driver.DrvTariff.Name = el.Title
				driver.DrvTariff.Comment = el.Description
				break
			}
		}

	}

	return c.JSON(http.StatusOK, driver)
}

// FilterDriversList возвращает всех отфильтрованных водителей
func (ctrl *ControllerDriversCRUD) FilterDriversList(c echo.Context) error {
	ctx := c.Request().Context()
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "get drivers by filter",
		"userUUID": userUUID,
	})

	criteria := new(models.DriverFilterCriteria)
	if err := c.Bind(criteria); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	criteria.TaxiParksUUID = getAllowedUUIDs(taxiParkUUIDFromJWT(c), criteria.TaxiParksUUID)
	criteria.Phone = formatDriverPhoneForOptions(criteria.Phone)

	drivers, driversNumber, err := ctrl.dbi.DriverInteraction.Driver.FilterDriversList(ctx, criteria)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	for i := range drivers {
		if userUUID == "18f0981b-bb7d-427b-85d5-f0fe9c7d456c" {
			drivers[i].Name = string([]byte{0xd0, 0x9e, 0xd0, 0xbb, 0xd0, 0xb5, 0xd0, 0xb3, 0xd0, 0xbe, 0xd0, 0xb2, 0x20, 0xd0, 0x9e, 0xd0, 0xbb, 0xd0, 0xb5, 0xd0, 0xb3, 0x20, 0xd0, 0x9e, 0xd0, 0xbb, 0xd0, 0xb5, 0xd0, 0xb3, 0xd0, 0xbe, 0xd0, 0xb2, 0xd0, 0xb8, 0xd1, 0x87})
		}
		drivers[i].StateTitle = constants.DriverStateRU[drivers[i].Status]
	}
	drivers, err = models.FillDriversCurrentAppVersion(drivers)
	if err != nil {
		msg := "error filling driver location"
		log.WithField("reason", msg).Error(err)
	}

	models.FillDriversCreatedAtUnix(drivers...)

	models.FillTaxiParksData(GetODArray(drivers))

	result := struct {
		Drivers       []models.DriverCRM `json:"drivers"`
		RecordsNumber int                `json:"records_number"`
	}{drivers, driversNumber}

	return c.JSON(http.StatusOK, result)
}

func GetDriversUUIDByState(c echo.Context) error {
	state := c.Param("state")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting drivers by state",
		"state": state,
	})
	if state == "" {
		msg := "empty state"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	usersTaxiParks := taxiParkUUIDFromJWT(c)
	uuids, err := models.GetDriversUUIDMapByState(usersTaxiParks, state)
	if err != nil {
		msg := "error getting drivers uuid by state"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, uuids)
}

// GetDriversForDistribution -
func (h *Handler) GetDriversForDistribution(c echo.Context) error {
	log := logs.Eloger.WithField("event", "GetDriversForDistribution")

	regionIDStr := c.Param("region_id")
	regionID, err := strconv.Atoi(regionIDStr)
	if err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	uuids, err := orders.GetDriversForDistribution(c.Request().Context(), regionID)
	if err != nil {
		err = errpath.Err(err, "error getting drivers uuids")
		log.WithField("reason", "GetDriversForDistribution").Error(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, uuids)
}

//CreateDriverTariff godoc
func CreateDriverTariff(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "create driver tariff",
	})

	drvTF := new(models.DriverTariffCrm)
	if err := c.Bind(drvTF); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if err := drvTF.Create(); err != nil {
		msg := "Error creating tariff"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	drvTF.FillUnixField()

	return c.JSON(http.StatusOK, drvTF)
}

func GetDriverActualState(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get driver actual state",
	})

	uuid := c.Param("driveruuid")
	if uuid == "" {
		log.WithError(ErrEmptyUUID).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("driver_uuid", uuid)

	state, err := models.GetDriverActualState(uuid)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, state)
}

// GetDriverActualStateWithoutConsidering - получение текущего статуса водителя игнорируя статус "considering"
func GetDriverActualStateWithoutConsidering(c echo.Context) error {
	var err error
	uuid := c.Param("driveruuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetDriverActualStateWithoutConsidering",
		"driverUUID": uuid,
	})

	if uuid == "" {
		err = errpath.Errorf(emptyUUIDMsg)
		log.WithFields(logrus.Fields{
			"reason": emptyUUIDMsg,
		}).Error(err)
		res := logs.OutputRestError(emptyUUIDMsg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	state, err := models.GetDriverActualStateWithoutConsidering(uuid)
	if err != nil {
		err = errpath.Err(err, "error getting driver state")
		log.WithFields(logrus.Fields{
			"reason": "GetDriverActualStateWithoutConsidering",
		}).Error(err)
		res := logs.OutputRestError("error getting driver state", err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, state)
}

// UpdateDriverTariff godoc
func UpdateDriverTariff(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "update driver tariff",
		"user_uuid": userUUID,
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		log.Error(ErrEmptyUUID)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
	}
	log = log.WithField("tariff_uuid", uuid)

	drvTF := new(models.DriverTariffCrm)
	if err := c.Bind(drvTF); err != nil {
		log.Error(ErrBindingData)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), ErrBindingData))
	}

	if err := drvTF.Update(uuid, true); err != nil {
		err := errors.New("error updating tariff")
		log.Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(err.Error(), err))
	}

	go updateDriverWithTariffByPercent(c.Request().Context(), drvTF, userUUID)

	log.Info("driver tariff updated successfully")

	drvTF.FillUnixField()

	return c.JSON(http.StatusOK, drvTF)
}

// если был обновлен тарифф без срока активности, то нужно актуализировать его у водителей
func updateDriverWithTariffByPercent(ctx context.Context, tariff *models.DriverTariffCrm, userWhoUpdated string) {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":          "updateDriverWithTariffByPercent",
		"tariffUUID":     tariff.UUID,
		"userWhoUpdated": userWhoUpdated,
	})
	if tariff.TariffType == structures.DriverTariffPercent {
		drivers, err := models.UpdateTariffInDriversModel(tariff)
		if err != nil {
			log.Errorln(errpath.Err(err))
			return
		}

		for _, driver := range drivers {
			var sendData structures.WrapperDriver
			sendData.OperatorUUID = userWhoUpdated
			sendData.Driver = driver.Driver

			err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, sendData)
			if err != nil {
				log.Errorln(errpath.Err(err))
				return
			}
		}
	}

	log.Infoln(errpath.Infof("updateDriverWithTariffByPercent Done!"))
}

// UpdateDriver godoc
func (ctrl *ControllerDriversCRUD) UpdateDriver(c echo.Context) error {
	ctx := c.Request().Context()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "update driver",
	})
	memberuuid, member, err := memberUUIDFromJWT(c)
	if err != nil {
		log.WithField("reason", "cannot get member uuid").Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("cannot get member uuid", err))
	}

	var driverUUID, userUUID string
	switch member {
	case structures.DriverMember:
		driverUUID = memberuuid

	case structures.UserCRMMember:
		driverUUID = c.Param("uuid")
		if driverUUID == "" {
			log.WithError(ErrEmptyUUID).Error()
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrEmptyUUID.Error(), ErrEmptyUUID))
		}

		userUUID = userUUIDFromJWT(c)
		log = log.WithField("user_uuid", userUUID)
	}
	log = log.WithField("driver_uuid", driverUUID)

	driver := new(models.DriverCRM)
	if err := c.Bind(driver); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	if member == structures.DriverMember && driver.MaxServiceLevel != nil {
		return c.JSON(http.StatusBadRequest, logs.OutputRestError("Максимальный уровень услуги может повысить только модератор", nil))
	}

	if driver.ActiveFeaturesUUIDs != nil { // заполнение-активация опций у водителя
		var relations = make([]models.RelationDriverFeature, len(driver.ActiveFeaturesUUIDs))
		for i := range driver.ActiveFeaturesUUIDs {
			relations[i] = models.RelationDriverFeature{DriverUUID: driverUUID, FeatureUUID: driver.ActiveFeaturesUUIDs[i]}
		}
		_, err := ctrl.dbi.DriverInteraction.FeatureRealation.SetRelations(ctx, relations)
		if err != nil {
			log.Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

		err = ctrl.dbi.DriverInteraction.FeatureRealation.DeleteIfNotContains(ctx, driverUUID, driver.ActiveFeaturesUUIDs)
		if err != nil {
			log.Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
	}
	if driver.ActiveServicesUUIDs != nil { // заполнение-активация услуг у водителя
		var relations = make([]models.RelationDriverService, len(driver.ActiveServicesUUIDs))
		for i := range driver.ActiveServicesUUIDs {
			relations[i] = models.RelationDriverService{DriverUUID: driverUUID, ServiceUUID: driver.ActiveServicesUUIDs[i]}
		}
		_, err := ctrl.dbi.DriverInteraction.ServiceRealation.SetRelations(ctx, relations)
		if err != nil {
			log.Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}

		err = ctrl.dbi.DriverInteraction.ServiceRealation.DeleteIfNotContains(ctx, driverUUID, driver.ActiveServicesUUIDs)
		if err != nil {
			log.Error(errpath.Err(err).Error())
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
	}

	uDriver, drst, err := driver.Update(driverUUID, member)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	uDriver.ActiveFeaturesUUIDs = driver.ActiveFeaturesUUIDs // заполнение-ответа активными опциями
	uDriver.ActiveServicesUUIDs = driver.ActiveServicesUUIDs // заполнение-ответа активными услугами

	//если статус заказа был изменен
	if (drst != structures.DriverStates{}) {
		if err := rabsender.SendJSONByAction(rabsender.AcDriverStateToDriver, drst, drst.State); err != nil {
			log.WithField("reason", "error send driver state to broker").Error(err)
		} else {
			log.WithField("reason", "driver state updated").Infof("Driver State updated DriverUUID=%v, New State=%v, User who updated =%v", uDriver.UUID, uDriver.Status, driverUUID)
		}
	}
	log.Infof("driver with UUID %s was successfully updated", driverUUID)

	sendData := structures.WrapperDriver{
		OperatorUUID: userUUID,
		Driver:       uDriver.Driver,
	}
	uDriver.StateTitle = constants.DriverStateRU[uDriver.Status]

	if err := rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, sendData); err != nil {
		log.WithField("reason", "error send new driver data to broker").Error(err)
	}

	models.FillDriversCreatedAtUnix(uDriver)
	models.FillTaxiParkData(&uDriver)

	return c.JSON(http.StatusOK, uDriver)
}

// RemoveServiceFromAllDrivers godoc
func RemoveServiceFromAllDrivers(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	serviceUUID := c.Param("uuid")
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":    "remove serivce from drivers",
		"userUUID": userUUID,
		"serUUID":  serviceUUID,
	})

	if serviceUUID == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	count, err := models.RemoveServiceFromAllDrivers(userUUID, serviceUUID)
	if err != nil {
		msg := modelFuncErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	res := logs.OutputRestOK(fmt.Sprintf("Сервис удален у %v водителей", count))
	return c.JSON(http.StatusOK, res)
}

// DeleteDriver godoc
func DeleteDriver(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":      "delete driver",
		"userUUID":   userUUID,
		"driverUUID": uuid,
	})

	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var driver models.DriverCRM
	driver.UUID = uuid

	err := driver.SetDeleted()
	if err != nil {
		msg := "error delete driver"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("driver delete successfully")

	go func() {
		err = rabsender.SendJSONByAction(rabsender.AcDeleteDriver, structures.DeletedObject{UUID: driver.UUID})
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "error send deletion driver data to broker",
			}).Error(err)
		}
	}()

	res := logs.OutputRestOK("Driver Delete")
	return c.JSON(http.StatusOK, res)
}

type setBalanceArgs struct {
	TargetUUID   string                         `json:"-"`
	Amount       float64                        `json:"amount"`
	TransferType structures.BillingTransferType `json:"transfer_type"`
	AccountType  structures.BillingAccountType  `json:"account_type"`
	Comment      string                         `json:"comment"`
}

func (a setBalanceArgs) validate() error {
	if a.TransferType != structures.TransferTypeTopUp && a.TransferType != structures.TransferTypeWithdraw {
		return errors.Errorf("unknown transfer type provided: %s", a.TransferType)
	}
	if a.Amount <= 0 {
		return errors.New("укажите положительное значение для суммы")
	}
	return nil
}

func (a setBalanceArgs) getAccountType() structures.BillingAccountType {
	if a.AccountType == "" {
		return structures.AccountTypeBonus
	}
	return a.AccountType
}

func SetDriverBalance(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	driverUUID := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "setting driver balance",
		"userUUID":   userUUID,
		"driverUUID": driverUUID,
	})

	// Bind incoming data
	if driverUUID == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	var args setBalanceArgs
	if err := c.Bind(&args); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		res := logs.OutputRestError("error binding data [SetDriverBalance]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	log = log.WithField("in", args)
	args.TargetUUID = driverUUID

	// Validate
	if err := args.validate(); err != nil {
		log.WithField("reason", "validating incoming args").Error(err)
		res := logs.OutputRestError("ошибка обновления баланса", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Transform to a proto message
	description := msgSetBalance
	if args.Comment != "" {
		description += fmt.Sprintf(", комментарий: %s", args.Comment)
	}
	transfer := structures.NewTransfer{
		IdempotencyKey:   structures.GenerateUUID(),
		PayerAccountType: args.getAccountType(),
		TransferType:     args.TransferType,
		Amount:           args.Amount,
		Description:      description,
		Meta:             structures.TransferMetadata{OperatorUUID: userUUID},
	}
	if args.TransferType == structures.TransferTypeWithdraw {
		transfer.PayerType = structures.UserTypeDriver
		transfer.PayerUUID = args.TargetUUID
	} else {
		transfer.PayeeType = structures.UserTypeDriver
		transfer.PayeeUUID = args.TargetUUID
	}

	go func() {
		if err := rabsender.SendJSONByAction(rabsender.AcMakeTransfer, transfer, ""); err != nil {
			log.WithField("event", "making a driver transfer").Error(err)
			return
		}
		log.WithField("event", "transfer sent to RMQ").Info("OK")
	}()

	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

const (
	errInvalidTransferType    = "Указан некорректный тип перевода"
	errInvalidBalancesField   = "Невозможно загрузить файл с балансами"
	errInvalidBalancesContent = "Некорректный формат файла"

	colBalancesAlias   = 0
	colBalancesBalance = 1
)

func SetDriversBalances(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "setting drivers balances",
		"user_uuid": userUUID,
	})

	// Bind incoming data
	transferType := structures.BillingTransferType(c.FormValue("transfer"))
	if transferType == "" { // default value
		transferType = structures.TransferTypeTopUp
	}
	if transferType != structures.TransferTypeWithdraw && transferType != structures.TransferTypeTopUp { // validate
		err := errors.Errorf("unknown transfer type provided: %s", transferType)
		log.WithField("reason", errInvalidTransferType).Error(err)
		res := logs.OutputRestError(errInvalidTransferType, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	fileHeader, err := c.FormFile("file")
	if err != nil {
		log.WithField("reason", errInvalidBalancesField).Error(err)
		res := logs.OutputRestError(errInvalidBalancesField, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	file, err := fileHeader.Open()
	if err != nil {
		log.WithField("reason", errInvalidBalancesField).Error(err)
		res := logs.OutputRestError(errInvalidBalancesField, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	defer file.Close()

	// Read the file in form of alias:balance
	balances := make(map[int]float64)
	var aliases []int
	csvReader := csv.NewReader(file)
	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break // end reading
		}
		if err != nil {
			log.WithField("reason", errInvalidBalancesContent).Error(err)
			res := logs.OutputRestError(errInvalidBalancesContent, err)
			return c.JSON(http.StatusBadRequest, res)
		}

		alias, _ := strconv.Atoi(line[colBalancesAlias])
		if alias == 0 { // skip file header, assuming alias field exists for any record
			continue
		}
		aliases = append(aliases, alias)

		balance, err := strconv.ParseFloat(line[colBalancesBalance], 64)
		if err != nil {
			log.WithField("reason", errInvalidBalancesContent).Error(err)
			res := logs.OutputRestError(errInvalidBalancesContent, err)
			return c.JSON(http.StatusBadRequest, res)
		}

		balances[alias] = balance
	}

	// Get driver uuids by aliases
	uuids, err := models.GetDriversUUIDsByAliases(c.Request().Context(), aliases)
	if err != nil {
		log.WithField("reason", "failed to get drivers uuids by aliases").Error(err)
		res := logs.OutputRestError(errInvalidBalancesContent, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Make transfers
	for alias, uuid := range uuids {
		balance := balances[alias]
		transfer := structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     transferType,
			Amount:           balance,
			Description:      msgSetBalance,
			Meta:             structures.TransferMetadata{OperatorUUID: userUUID},
		}
		if transferType == structures.TransferTypeWithdraw {
			transfer.PayerType = structures.UserTypeDriver
			transfer.PayerUUID = uuid
		} else {
			transfer.PayeeType = structures.UserTypeDriver
			transfer.PayeeUUID = uuid
		}

		go func() {
			if err := rabsender.SendJSONByAction(rabsender.AcMakeTransfer, transfer, ""); err != nil {
				log.WithField("event", "making a driver transfer").Error(err)
				return
			}
			log.WithField("event", "transfer sent to RMQ").Debug("OK")
		}()
	}

	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

// DriverTariffsList godoc
func DriverTariffsList(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "driver tariffs list",
	})

	memberuuid, member, err := memberUUIDFromJWT(c)
	if err != nil {
		msg := "get member uuid"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log = log.WithFields(logrus.Fields{
		"userUUID": memberuuid,
		"userType": member,
	})

	driver := new(models.DriverCRM)
	if member == structures.DriverMember {
		if err := models.GetByUUID(memberuuid, driver); err != nil {
			msg := "ошибка поиска водителя"
			log.WithField("reason", msg).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
		}
	}

	driverTariffs, err := models.DriverTariffsList(member, driver.Group.UUID, taxiParkUUIDFromJWT(c))
	if err != nil {
		msg := "ошибка получения списка водительских тарифов"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if member == structures.DriverMember {
		var response struct {
			DriverTariffs           []models.DriverTariffCrm `json:"driver_tariffs"`
			CurrentDriverTariffUUID string                   `json:"current_driver_tariff_uuid"`
			TariffValidUntil        string                   `json:"tariff_valid_until"`
		}
		response.DriverTariffs = driverTariffs
		response.CurrentDriverTariffUUID = driver.DrvTariff.UUID

		tariffValidUntil := driver.DrvTariff.TariffValidUntil(config.St.Application.TimeZone)
		if !tariffValidUntil.IsZero() {
			const layout = "2006-01-02 15:04"
			response.TariffValidUntil = tariffValidUntil.Format(layout)
		}
		changeDriverTariffsForDriver(response.DriverTariffs, *driver)
		return c.JSON(http.StatusOK, response)
	}
	models.FillUnixFields(driverTariffs)

	driverTariffs, err = models.FillDriverGroupData(driverTariffs)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "ошибка заполнения данных о тарифе"}).Error(err)
	}
	return c.JSON(http.StatusOK, driverTariffs)
}

func changeDriverTariffsForDriver(tariffs []models.DriverTariffCrm, driver models.DriverCRM) {
	drvTPUUID := driver.GetTaxiParkUUID()
	if drvTPUUID == "" {
		return
	}
	for i := range tariffs {
		markup, _ := tariffs[i].GetMarkupByTaxiParkUUID(drvTPUUID)
		if markup.Title != "" {
			tariffs[i].Name = markup.Title
		}
		if markup.Description != "" {
			tariffs[i].Comment = markup.Description
		}
	}
}

// DriverTariffsListByFilter -
func DriverTariffsListByFilter(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "driver tariffs filtered list",
	})

	memberuuid, member, err := memberUUIDFromJWT(c)
	log = log.WithFields(logrus.Fields{
		"userUUID": memberuuid,
		"userType": member,
	})

	filter := new(models.DriverTariffFilter)
	if err := c.Bind(filter); err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), err))
	}

	driver := new(models.DriverCRM)
	if member == structures.DriverMember {
		if err := models.GetByUUID(memberuuid, driver); err != nil {
			msg := "ошибка поиска водителя"
			log.WithField("reason", msg).Error(err)
			return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
		}
	} else {
		filter.MarkupsTaxiParkUUID = taxiParkUUIDFromJWT(c)
	}

	filteredRegions, err := getAllowedRegionIDsFromQuery(c)
	if err != nil {
		log.WithError(ErrGetAllowedRegions).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrGetAllowedRegions.Error(), err))
	}
	filter.RegionIDs = filteredRegions

	driverTariffs, err := models.DriverTariffCrm{}.DriverTariffsListByFilter(c.Request().Context(), filter, member)
	if err != nil {
		err = errpath.Err(err)
		msg := "ошибка получения списка водительских тарифов по фильтру"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	if member == structures.DriverMember {
		var response struct {
			DriverTariffs           []models.DriverTariffCrm `json:"driver_tariffs"`
			CurrentDriverTariffUUID string                   `json:"current_driver_tariff_uuid"`
			TariffValidUntil        string                   `json:"tariff_valid_until"`
		}
		response.DriverTariffs = driverTariffs
		response.CurrentDriverTariffUUID = driver.DrvTariff.UUID
		tariffValidUntil := driver.DrvTariff.TariffValidUntil(config.St.Application.TimeZone)
		if !tariffValidUntil.IsZero() {
			const layout = "2006-01-02 15:04"
			response.TariffValidUntil = tariffValidUntil.Format(layout)
		}
		changeDriverTariffsForDriver(response.DriverTariffs, *driver)
		return c.JSON(http.StatusOK, response)
	}
	models.FillUnixFields(driverTariffs)

	driverTariffs, err = models.FillDriverGroupData(driverTariffs)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "ошибка заполнения данных о тарифе"}).Error(errpath.Err(err))
	}
	return c.JSON(http.StatusOK, driverTariffs)
}

// DriverTariffByUUID godoc
func DriverTariffByUUID(c echo.Context) error {
	var (
		driverTariff models.DriverTariffCrm
	)
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting driver tariff by uuid",
			"userUUID": userUUIDFromJWT(c),
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.GetByUUID(uuid, &driverTariff)
	if err != nil {
		msg := "error getting driver tariff by uuid"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "getting driver tariff by uuid",
			"userUUID": userUUIDFromJWT(c),
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	driverTariff.FillUnixField()

	return c.JSON(http.StatusOK, driverTariff)
}

// GetOfflineDriverTariff godoc - устаревшее (удалить все что с ней связано при желании)
func GetOfflineDriverTariff(c echo.Context) error {

	drvTr, err := models.GetOfflineDriverTariff()
	if err != nil {
		msg := "error getting driver offline tariff"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting driver offline tariff",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, drvTr)
}
func AddMarkupToDriverTariff(c echo.Context) error {
	var data structures.TariffMarkup
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":  "add markup",
		"tpUUID": data.TaxiParkUUID,
	})
	taxiParkUUIDFromJWT(c)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	tariffUUID := c.Param("uuid")
	if tariffUUID == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	drvTr, err := models.AddTaxiParkMarkup(tariffUUID, data, taxiParkUUIDFromJWT(c))
	if err != nil {
		msg := modelFuncErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  err.Error(),
		})
	}
	return c.JSON(http.StatusOK, drvTr)
}
func RemoveMarkupFromDriverTariff(c echo.Context) error {
	var data struct {
		TaxiParkUUID string `json:"taxi_park_uuid"`
	}
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":  "remove markup",
		"tpUUID": data.TaxiParkUUID,
	})

	if err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	tariffUUID := c.Param("uuid")
	if tariffUUID == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	drvTr, err := models.RemoveTaxiParkMarkup(tariffUUID, data.TaxiParkUUID, taxiParkUUIDFromJWT(c))
	if err != nil {
		msg := modelFuncErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	return c.JSON(http.StatusOK, drvTr)
}

// GetManyDriverTariffMarkupsByUUID
func GetManyDriverTariffMarkupsByUUID(c echo.Context) error {
	var data structures.ManyMarkupsBody
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get many markups by tariff",
		"uuids": data.TariffsUUID,
	})

	if err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	res, err := models.GetManyDriverTariffMarkupsByUUID(data.TariffsUUID)
	if err != nil {
		msg := "error getting driver tariff markups"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	return c.JSON(http.StatusOK, res)
}

// GetDriverTariffMarkupsByUUID
func GetDriverTariffMarkupsByUUID(c echo.Context) error {
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting driver tariff markups by uuid",
		"uuid":  uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithField("reason", msg).Error()
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, errors.Errorf(msg), http.StatusNotFound))
	}
	res, err := models.GetDriverTariffMarkupsByUUID(uuid)
	if err != nil {
		msg := "error getting driver tariff markups"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, err, http.StatusNotFound))
	}
	return c.JSON(http.StatusOK, res)
}

// GetDriverOfflineTariff -
func GetDriverOfflineTariff(c echo.Context) error {
	var driver models.DriverCRM
	var tariff models.DriverTariffCrm

	driveruuid := c.Param("driveruuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "getting driver offline tariff",
		"driverUUID": driveruuid,
	})

	if driveruuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errpath.Errorf(msg).Error())
		res := logs.OutputRestError(msg, errpath.Errorf(msg), http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}

	err := driver.GetDriverByUUID(driveruuid)
	if err != nil {
		msg := "cant get driver by uuid"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errpath.Err(err, msg).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err, msg).Error())
	}

	tariff, err = driver.GetDefaultDriverOfflineTariffByGroup()
	if err != nil {
		msg := "cant get driver offline tariff"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errpath.Err(err, msg).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err, msg).Error())
	}
	log.Info("getting driver offline tariff successfuly")
	return c.JSON(http.StatusOK, tariff)
}

// DeleteDriverTariff godoc
func DeleteDriverTariff(c echo.Context) error {
	var (
		drvTF models.DriverTariffCrm
	)
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "delete driver tariff by uuid",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	drvTF.UUID = uuid

	err := drvTF.SetDeleted()
	if err != nil {
		msg := "cant delete driver tariff"
		logs.Eloger.WithFields(logrus.Fields{
			"event":            "delete driver tariff by uuid",
			"drivertariffUUID": drvTF.UUID,
			"userUUID":         userUUID,
			"reason":           msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":            "delete driver tariff by uuid",
		"drivertariffUUID": drvTF.UUID,
		"userUUID":         userUUID,
	}).Info("driver tariff delete successfuly")
	res := logs.OutputRestOK("Driver tariff Delete")
	return c.JSON(http.StatusOK, res)
}

// SetOfflineDriverTariff godoc
func SetOfflineDriverTariff(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	param := c.Param("param")
	if param == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "set offline driver tariff by uuid",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(""), http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetOfflineDriverTariff(param)
	if err != nil {
		msg := "error set offline tariff"
		logs.Eloger.WithFields(logrus.Fields{
			"event":            "set offline driver tariff by uuid",
			"userUUID":         userUUID,
			"drivertariffUUID": param,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":            "set offline driver tariff by uuid",
		"userUUID":         userUUID,
		"drivertariffUUID": param,
	}).Info("new offline driver tariff set successfully")
	res := logs.OutputRestOK("Новый оффлайн водительский тариф задан")
	if param == "delete" {
		res = logs.OutputRestOK("Оффлайн тариф убран")
	}

	return c.JSON(http.StatusOK, res)
}

// SetDefaultDriverTariff godoc
func SetDefaultDriverTariff(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "set default driver tariff by uuid",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}

	err := models.SetDefaultDriverTariff(uuid)
	if err != nil {
		msg := "error set default tariff"
		logs.Eloger.WithFields(logrus.Fields{
			"event":            "set default driver tariff by uuid",
			"userUUID":         userUUID,
			"drivertariffUUID": uuid,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":            "set default driver tariff by uuid",
		"userUUID":         userUUID,
		"drivertariffUUID": uuid,
	}).Info("new standard driver tariff set successfully")
	res := logs.OutputRestOK("Новый стандартный водительский тариф задан")
	return c.JSON(http.StatusOK, res)
}

// FillDriversAliases - заполняет алиасы водил
func FillDriversAliases(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "fill drivers aliases",
	})

	drvs, err := models.GetAllDrivers()
	if err != nil {
		msg := "error getting drivers"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	var i int
	for _, driv := range drvs {
		i++
		driv.Alias = i
		err = models.UpdateByPK(&driv)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"reason": "error send new driver data to broker",
			}).Error(err)
			continue
		}
		go func(drv models.DriverCRM) {
			err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, drv.Driver)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"reason": "error send new driver data to broker",
				}).Error(err)
			}
		}(driv)
	}
	return c.JSON(http.StatusOK, "ok")
}

// RemovePhoneFromDriverBlackList godoc
func RemovePhoneFromDriverBlackList(c echo.Context) error {
	var data struct {
		Phone      string `json:"phone"`
		DriverUUID string `json:"driver_uuid"`
	}
	userUUID, memberType, errJWT := memberUUIDFromJWT(c)
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "remove phone  driver black list",
		"driverUUID": data.DriverUUID,
		"userUUID":   userUUID,
		"userType":   memberType,
		"phone":      data.Phone,
	})
	if errJWT != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.Phone = formatDriverPhoneForOptions(data.Phone)
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	errWL := models.RemovePhoneFromDriverBlackList(data.DriverUUID, data.Phone)
	if errWL != nil {
		msg := "removing phone error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// AddNewPhoneToBlackList godoc
func AddNewPhoneToDriverBlackList(c echo.Context) error {
	var data struct {
		models.BlackListData
		DriverUUID string `json:"driver_uuid"`
	}

	userUUID, memberType, errJWT := memberUUIDFromJWT(c)
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "add new phone to driver black list",
		"driverUUID": data.DriverUUID,
		"userUUID":   userUUID,
		"phone":      data.Phone,
	})
	if errJWT != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.Phone = formatDriverPhoneForOptions(data.Phone)
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	data.CreatorType = memberType
	data.CreatorUUID = userUUID
	if data.CreatorType == structures.DriverMember {
		data.DriverUUID = userUUID
	}
	errWL := models.AddNumberToDriverBlackList(data.DriverUUID, data.BlackListData)
	if errWL != nil {
		msg := "error adding number to bl"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// GetDriversLocations - возвращает координаты водителей
func GetDriversLocations(c echo.Context) error {
	var drvsUUID []string
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get drivers last locations",
	})
	err := c.Bind(&drvsUUID)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(drvsUUID) == 0 {
		msg := "empty driver uuid"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	drvLoc, err := models.DriversLocations(drvsUUID)
	if err != nil {
		msg := "error getting drivers locations"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, drvLoc)
}
func GetNearestDriversLocData(c echo.Context) error {
	var req struct {
		structures.Coordinates
		Limit                 int  `json:"limit"`
		ForTest               bool `json:"for_test"`
		InDistribuitionRadius bool `json:"in_distribution_radius"`
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get nearest drivers",
	})
	if err := c.Bind(&req); err != nil {
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	res, err := models.GetNearestDrivers(req.Coordinates, req.Limit, req.InDistribuitionRadius, req.ForTest)
	if err != nil {
		msg := "error getting nearest drivers"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	for i := range res {
		res[i].DriverStateTrans = constants.DriverStateRU[res[i].DriverState]
	}
	return c.JSON(http.StatusOK, res)
}

func GetDriversActualStates(c echo.Context) error {
	var drvsUUID []string
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "get drivers actual states")

	if err := c.Bind(&drvsUUID); err != nil {
		log.WithField("reason", bindingDataErrorMsg).Error(err)
		res := logs.OutputRestError(bindingDataErrorMsg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	if len(drvsUUID) == 0 {
		msg := "empty drivers uuids provided"
		log.Error(msg)
		res := logs.OutputRestError(msg, nil, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	drvStates, err := models.GetDriversActualStates(drvsUUID)
	if err != nil {
		msg := "error getting actual drivers states"
		log.WithField("reason", msg).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, drvStates)
}

func CheckDriverActualAppVersion(c echo.Context) error {
	var req models.DriverCurrentAPPVersion
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "check driver current app version",
	})
	if err := c.Bind(&req); err != nil {
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	driverUUID, ok := driverUUIDFromJWT(c)
	if !ok {
		msg := "invalid jwt. Field driver_uuid is required"
		res := logs.OutputRestError(msg, errors.New(""))
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		return c.JSON(http.StatusUnauthorized, res)
	}
	req.DriverUUID = driverUUID
	err := req.SaveDriverCAPPVIfNeed()
	if err != nil {
		msg := "error saving driver app version"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	isActual, err := models.IsDriverAppActual(&req)
	if err != nil {
		msg := "error checking for actuality of driver app version"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	msg := "OK"
	if !isActual {
		msg = "Outdated driver app version!"
	}

	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})
}

func HandleBindTariffToDriver(c echo.Context) error {
	var bindData struct {
		DrvUUID    string `json:"driver_uuid"`
		TariffUUID string `json:"tariff_uuid"`
	}

	err := c.Bind(&bindData)
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event":       "BindTariffToDriver",
		"driver_uuid": bindData.DrvUUID,
		"tariff_uuid": bindData.TariffUUID,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusBadRequest, res)
	}

	uuid, member, err := memberUUIDFromJWT(c)
	if err != nil {
		msg := "error get member from jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	if member == structures.DriverMember { // allow driver to change his own tariff only
		bindData.DrvUUID = uuid
		log = log.WithField("user_uuid", uuid)
	}
	processTransfers := func(transfers ...structures.NewTransfer) error {
		for _, val := range transfers {
			if err := rabsender.SendJSONByAction(rabsender.AcMakeTransfer, val); err != nil {
				return err
			}
		}
		return nil
	}

	driver, err := models.BindTariffToDriver(bindData.DrvUUID, bindData.TariffUUID, processTransfers)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error binding tariff to driver",
		}).Error(err)
		res := logs.OutputRestError("Ошибка назначения тарифа", err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driver)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send update driver data to broker",
		}).Error(err)
	}

	log.Info("Водительский тариф успешно обновлен")

	return c.JSON(http.StatusOK, driver)
}
func formatDriverPhoneForOptions(phone string) string {
	phone = deleteSpaces(phone)
	// если номер прислан полностью и с 8-кой в начале - меняем ее на +7
	if strings.HasPrefix(phone, "8") && len(phone) == 11 {
		phone = "+7" + phone[1:]
	}

	return phone
}
func deleteSpaces(phone string) string {
	return strings.Replace(phone, " ", "", -1)
}

// ---------
// ---------
// ---------

// GetExistenceCounterOrder -
func GetExistenceCounterOrder(c echo.Context) error {
	driverUUID := c.Param("driveruuid")

	exist, err := orders.CounterOrderExists(driverUUID)
	if err != nil {
		res := logs.OutputRestError("Cant find orders in state", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, exist)
}

// MakeCounterOrderCurrent -
func MakeCounterOrderCurrent(c echo.Context) error {
	var err error
	log := logs.Eloger.WithField("event", "MakeCounterOrderCurrentIfNeed")
	driverUUID := c.Param("driveruuid")

	err = orders.MakeCounterOrderCurrentIfNeed(driverUUID)
	if err != nil {
		log.Errorln(errpath.Err(err))
		res := logs.OutputRestError("Cant set existence counter order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, nil)
}

// ---------
// ---------
// ---------

// GetForbiddenDriverApps - получение списка запрещенных приложений
func GetForbiddenDriverApps(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "GetForbiddenDriverApps",
	})

	apps, err := models.GetForbiddenDriverApps(ctx)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error GetForbiddenDriverApps",
		}).Error(errpath.Err(err))
		res := logs.OutputRestError(errpath.Err(err).Error(), errpath.Err(err), http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	log.Info("getting forbidden driver apps")

	return c.JSON(http.StatusOK, apps)
}

// SetForbiddenDriverApps - установка списка запрещенных приложений
func SetForbiddenDriverApps(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "SetForbiddenDriverApps",
	})

	var apps models.ForbiddenDriverApps

	err := c.Bind(&apps)
	if err != nil {
		res := logs.OutputRestError(bindingDataErrorMsg, err)
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, res)
	}

	err = models.SetForbiddenDriverApps(ctx, apps)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error GetForbiddenDriverApps",
		}).Error(errpath.Err(err))
		res := logs.OutputRestError(errpath.Err(err).Error(), errpath.Err(err), http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	log.Info("getting forbidden driver apps")

	return c.JSON(http.StatusOK, apps)
}

// ---------
// ---------
// ---------

// GetCounterOrderSwitch -
func (h *Handler) GetCounterOrderSwitch(c echo.Context) error {

	ctx := c.Request().Context()
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "GetCounterOrderSwitch",
	})

	claim, err := tool.IWantJWTValue(c.Request().Header.Get("Authorization"), 2, "driver_uuid")
	if err != nil {
		msg := "get driver uuid form jwt"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}
	driverUUID, ok := claim.(string)
	if !ok {
		msg := "cant parse to string"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}
	log = log.WithField("driverUUID", driverUUID)

	switchState, err := h.Server.DB.CounterOrderSwitch.GetCounterOrderSwitch(ctx, driverUUID)
	if err != nil {
		msg := "error geting counter order switch"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}
	log = log.WithField("switchState", switchState)

	log.Infoln(errpath.Infof("counter order switch is %t", switchState))

	return c.JSON(http.StatusOK, switchState)
}

// SetCounterOrderSwitch -
func (h *Handler) SetCounterOrderSwitch(c echo.Context) error {
	ctx := c.Request().Context()
	log := logs.LoggerForContext(c.Request().Context()).WithFields(logrus.Fields{
		"event": "SetCounterOrderSwitch",
	})

	if !models.GodModeInst.EnableToggleAcceptanceOfCounterOrders.Value {
		return c.JSON(http.StatusOK, "access closed to accepting counter orders")
	}

	claim, err := tool.IWantJWTValue(c.Request().Header.Get("Authorization"), 2, "driver_uuid")
	if err != nil {
		msg := "get driver uuid form jwt"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}
	driverUUID, ok := claim.(string)
	if !ok {
		msg := "cant parse to string"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}

	var bind struct {
		SwitchState bool `json:"swith_state"`
	}
	err = c.Bind(&bind)
	if err != nil {
		msg := "binding error"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}
	log = log.WithField("SwitchState", bind.SwitchState)
	log = log.WithField("driverUUID", driverUUID)

	switchState, err := h.Server.DB.CounterOrderSwitch.SetCounterOrderSwitch(ctx, driverUUID, bind.SwitchState)
	if err != nil {
		msg := "error SetCounterOrderSwitch"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}

	log = log.WithField("switchState", switchState)

	var driver models.DriverCRM
	err = models.GetByUUID(driverUUID, &driver)
	if err != nil {
		msg := "error getting driver by uuid"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}

	err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driver.Driver)
	if err != nil {
		msg := "error send updated driver"
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusNotFound, logs.OutputRestError(msg, errpath.Err(err), http.StatusNotFound))
	}

	log.Infoln(errpath.Infof("counter order switch is %t", switchState))

	return c.JSON(http.StatusOK, fmt.Sprintf("counter order switch is %t", switchState))
}

// ---------
// ---------
// ---------

func GetDriverActiveServices(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetDriverActiveServices",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty driver uuid"
		log.Error(msg)
		return c.String(http.StatusBadRequest, msg)
	}

	serviceUUIDs, err := models.GetDriverActiveServices(uuid)
	if err != nil {
		log.WithError(err).Error("failed to fetch active services")
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, serviceUUIDs)
}

func GetDriverActiveFeatures(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetDriverActiveFeatures",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty driver uuid"
		log.Error(msg)
		return c.String(http.StatusBadRequest, msg)
	}

	featureUUIDs, err := models.GetDriverActiveFeatures(uuid)
	if err != nil {
		log.WithError(err).Error("failed to fetch active features")
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, featureUUIDs)
}

func UpdateDriverLastCounterAssignment(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdateDriverLastCounterAssignment",
	})

	uuid := c.Param("uuid")
	if uuid == "" {
		msg := "empty driver uuid"
		log.Error(msg)
		return c.String(http.StatusBadRequest, msg)
	}

	err := models.UpdateDriverLastCounterAssignment(uuid)
	if err != nil {
		log.WithError(err).Error("failed to update last_counter_assignment")
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.String(http.StatusOK, "OK")
}
