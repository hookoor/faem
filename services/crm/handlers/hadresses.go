package handlers

import (
	"fmt"
	addr "gitlab.com/faemproject/backend/faem/services/crm/addresses"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
)

// Addresses godoc
// приоритет адресов выставляется относительно региона который мы получаем по uuid переданного "источника заказов"
func Addresses(c echo.Context) error {
	var err error

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "finding addresses",
	})

	request := new(struct {
		RequestString      string `json:"name"`
		SourceOfOrdersUUID string `json:"source_uuid"`
	})
	if err := c.Bind(request); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	request.SourceOfOrdersUUID = c.Request().Header.Get(constants.HeaderSourceUUID)

	var regid int = 1 // дефолтный регион при пустом источнике заказов
	if request.SourceOfOrdersUUID != "" {
		soo, err := models.SourceOfOrdersCRM{}.GetByUUID(request.SourceOfOrdersUUID)
		if err != nil {
			log.Error(errpath.Err(err))
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
		regid = soo.RegionID
	}

	CitiesPriority, err := models.RegionCRM{}.GetCityListByRegionID(regid)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	addresses := models.GetMatchesByContains(request.RequestString, CitiesPriority)

	if val, ok := map[string]string{
		"в чем сила, брат?": "В правде",
		"оппа":              "Олег",
	}[strings.ToLower(request.RequestString)]; ok {
		addresses = []structures.Route{{
			UnrestrictedValue: val,
		}}
	}

	return c.JSON(http.StatusOK, addresses)
}

func AddressesV2(c echo.Context) error {
	var err error

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "finding addresses",
	})

	request := new(struct {
		RequestString      string `json:"name"`
		SourceOfOrdersUUID string `json:"source_uuid"`
	})
	if err := c.Bind(request); err != nil {
		msg := bindingDataErrorMsg
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	var regid int = 1 // дефолтный регион при пустом источнике заказов
	if request.SourceOfOrdersUUID != "" {
		soo, err := models.SourceOfOrdersCRM{}.GetByUUID(request.SourceOfOrdersUUID)
		if err != nil {
			log.Error(errpath.Err(err))
			return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
		}
		regid = soo.RegionID
	}

	cityPriorities, err := models.RegionCRM{}.GetCityWeights(regid)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	routes := addr.ExperimentalAddressMatch(request.RequestString, cityPriorities)

	if val, ok := map[string]string{
		"в чем сила, брат?": "В правде",
		"оппа":              "Олег",
	}[strings.ToLower(request.RequestString)]; ok {
		routes = []structures.Route{{
			UnrestrictedValue: val,
		}}
	}

	return c.JSON(http.StatusOK, routes)
}

// AddressesByLevenshtein godoc
func AddressesByLevenshtein(c echo.Context) error {
	request := new(struct {
		RequestString string `json:"name"`
	})
	err := c.Bind(request)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "finding addresses by levenshtein",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	addresses := models.GetMatchesByLevenshtein(request.RequestString)
	return c.JSON(http.StatusOK, addresses)
}

// UpdateDesinationPoint godoc
func UpdateDesinationPoint(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	var dp models.DestinationPoint
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "update desination point",
		"userUUID":  userUUID,
		"pointUUID": uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := c.Bind(&dp); err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	errWL := dp.Update(uuid)
	if errWL != nil {
		msg := "error updating point"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	log.Info("point update successfully")
	return c.JSON(http.StatusOK, dp)
}

// CreateDesinationPoint godoc
func CreateDesinationPoint(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	var dp models.DestinationPoint
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create desination point",
		"userUUID": userUUID,
	})

	if err := c.Bind(&dp); err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	errWL := dp.Create()
	if errWL != nil {
		msg := "error creating point"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	log.Info("point create successfully")
	return c.JSON(http.StatusOK, dp)
}

// GetAllCategories godoc
func GetAllCategories(c echo.Context) error {
	// userUUID := userUUIDFromJWT(c)

	// log := logs.Eloger.WithFields(logrus.Fields{
	// 	"event":    "getting all categories for points",
	// 	"userUUID": userUUID,
	// })

	result := models.GetPublicPlacesCategory()
	return c.JSON(http.StatusOK, result)
}

// DestinationPointsListWithOptions godoc
func DestinationPointsListWithOptions(c echo.Context) error {
	var (
		criteria models.PointsFilterCriteria
		err      error
	)
	userUUID := userUUIDFromJWT(c)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "getting dest points list by filter",
		"userUUID": userUUID,
	})
	criteria.Pager, err = getPager(c)
	if err != nil {
		msg := "Error getting pager"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err = c.Bind(&criteria); err != nil {
		msg := bindingDataErrorMsg
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	pp, recordsNumber, err := criteria.GetDestinationPointsByFilter()
	if err != nil {
		msg := "error getting points"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: pp,
		Count:   recordsNumber,
	})
}
func InitAddressesByRequest(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "init addressesb by request",
		"userUUID": userUUID,
	})
	if !models.PermissibilityAddressesUpdate {
		msg := "Обновление адресов уже запущено, подождите несколько секунд"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Warn()
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	go models.InitAddress()
	msg := "Обновление адресов запущено"
	log.WithFields(logrus.Fields{
		"reason": msg,
	}).Info()
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})
}

func SetDestPointDeleted(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "set dest point deleted",
		"userUUID":  userUUID,
		"pointUUID": uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetDestinationPointDeleted(uuid)
	if err != nil {
		msg := "error setting point as remote"
		res := logs.OutputRestError(msg, err)
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, res)
	}
	msg := "Запись отмечена как удаленная"
	log.Info(msg)
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  msg,
	})
}
