package handlers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

type responseServicesSetForRegion struct {
	UUID    string `json:"uuid"`
	Name    string `json:"name"`
	Comment string `json:"comment"`
	Price   int    `json:"price"`
	// Active - метка того что опция активна в регионе
	Active           bool `json:"active"`
	EnableForDriver  bool `json:"enable_for_driver"`
	VisibleForDriver bool `json:"visible_for_driver"`
	EnableForClient  bool `json:"enable_for_client"`
	VisibleForClient bool `json:"visible_for_client"`
	DisplayPriority  int  `json:"display_priority"`
}

func GetServiceSet(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetServiceSet",
	})
	ctx := c.Request().Context()

	id, err := strconv.Atoi(c.Param("service-set-id"))
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrBindingData.Error(), ErrBindingData))
	}

	services, err := models.GetServiceSet(ctx, id)
	if err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(ErrModelFunc.Error(), err))
	}

	return c.JSON(http.StatusOK, services)
}

func UpdateServiceSet(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "UpdateServiceSet",
	})
	ctx := c.Request().Context()

	// --- Binding

	b, err := ioutil.ReadAll(c.Request().Body)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	var bind []models.ServiceSetItem
	err = json.Unmarshal(b, &bind)
	if err != nil {
		log.Errorf(errpath.Err(err).Error())
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	id, err := strconv.Atoi(c.Param("service-set-id"))
	if err != nil {
		log.WithError(ErrBindingData).Error(err)
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	// ---

	if err := models.UpdateServiceSet(ctx, id, bind); err != nil {
		log.WithError(ErrModelFunc).Error(err)
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, "Услуги обновлены")
}
