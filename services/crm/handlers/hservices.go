package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// CreateService godoc
func CreateService(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	var service models.ServiceCRM
	err := c.Bind(&service)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create service",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	nService, err := service.Create()
	if err != nil {
		msg := "creating error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "create service",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":    "create service",
		"userUUID": userUUID,
		"serUUID":  nService.UUID,
	}).Info("creating done successfully")

	SendOptionsToClient(constants.MarkerServices)

	return c.JSON(http.StatusOK, nService)
}

// GetStandardServices godoc
func GetStandardServices(c echo.Context) error {
	services, _, err := models.GetStandardServices()
	if err != nil {
		msg := "error getting"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "standard services list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, services)
}

// ServicesList godoc
func ServicesList(c echo.Context) error {
	var service []models.ServiceCRM
	service, err := models.ServicesList(false)
	if err != nil {
		msg := "error getting"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "services list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, service)
}

// SetServiceAsProductDelivery godoc
func SetServiceAsProductDelivery(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "set service product delivery",
		"userUUID":    userUUID,
		"serviceUUID": uuid,
	})
	if uuid == "" {
		msg := emptyUUIDMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetProductDelivery(uuid)
	if err != nil {
		msg := "model func error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusNotFound, structures.ResponseStruct{
			Code: http.StatusNotFound,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	SendOptionsToClient(constants.MarkerServices)
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

// GetService godoc
func GetService(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "get service",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var service models.ServiceCRM
	err := models.GetByUUID(uuid, &service)
	if err != nil {
		msg := "getting by uuid error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "get service",
			"serviceUUID": uuid,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, service)
}

// // ServiceGetDefault godoc
// func ServiceGetDefault(c echo.Context) error {
// 	service, err := models.GetDefaultService()
// 	if err != nil {
// 		msg := "error getting default service"
// 		logs.Eloger.WithFields(logrus.Fields{
// 			"event":  "get service",
// 			"reason": msg,
// 		}).Error(err)
// 		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
// 		return c.JSON(http.StatusNotFound, res)
// 	}
// 	return c.JSON(http.StatusOK, service)
// }

// UpdateService godoc
func UpdateService(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update service",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	userUUID := userUUIDFromJWT(c)
	var service models.ServiceCRM
	if err := c.Bind(&service); err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "update service",
			"serviceUUID": uuid,
			"userUUID":    userUUID,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	uService, err := service.Update(uuid)
	if err != nil {
		msg := "updating error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "update service",
			"serviceUUID": uuid,
			"userUUID":    userUUID,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":       "update service",
		"serviceUUID": uuid,
		"userUUID":    userUUID,
	}).Info("service was updated")

	SendOptionsToClient(constants.MarkerServices)

	return c.JSON(http.StatusOK, uService)
}

// DeleteService godoc
func DeleteService(c echo.Context) error {
	userUUID := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "deleting service",
			"userUUID": userUUID,
			"reason":   msg,
		}).Error()
		res := logs.OutputRestError(msg, errors.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var service models.ServiceCRM
	service.UUID = uuid

	err := service.SetDeleted()
	if err != nil {
		msg := "deleting error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "deleting service",
			"serviceUUID": uuid,
			"userUUID":    userUUID,
			"reason":      msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":       "deleting service",
		"serviceUUID": uuid,
		"userUUID":    userUUID,
	}).Info("service was delete")
	SendOptionsToClient(constants.MarkerServices)

	res := logs.OutputRestOK("Service Delete")
	return c.JSON(http.StatusOK, res)
}
