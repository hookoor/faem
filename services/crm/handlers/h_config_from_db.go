package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/tickers"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"

	"github.com/labstack/echo/v4"
)

// ConfigReinitialization -
func ConfigReinitialization(c echo.Context) error {
	var err error

	key := c.Param("key")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "ConfigReinitialization",
		"config key": key,
	})

	err = models.ConfigReinitialization()
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.ConfigReinitialization").Errorln(err.Error())
	}

	log.Infoln("Done!")

	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  key,
	})
}

// GetConfigValueByKey -
func GetConfigValueByKey(c echo.Context) error {
	key := structures.DBConfigKey(c.Param("key"))

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetConfigByKey",
		"config key": key,
	})

	res, err := models.GetConfigValueByKey(key)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.GetConfigValueByKey").Errorln(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	log.Infoln("Done!")

	return c.JSON(http.StatusOK, res)
}

// GetReferralSystemParams - retrieves referral values from db
func GetReferralSystemParams(c echo.Context) error {
	var err error

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetConfigByKey",
		"config key": structures.DBConfigReferralSystemParams,
	})

	rsp, err := models.GetReferralSystemParams()
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.GetConfigValueByKey").Errorln(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	log.Infoln("Done!")

	return c.JSON(http.StatusOK, rsp)
}

// UpdateReferralSystemParams - updates values in db
func UpdateReferralSystemParams(c echo.Context) error {
	var config structures.ReferralSystemParams

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "UpdateConfigByKey",
		"config key": structures.DBConfigReferralSystemParams,
	})

	if err := c.Bind(&config); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error binding config into structure",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}

	err := models.UpdateReferralSystemParams(c.Request().Context(), &config)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.GetConfigValueByKey").Errorln(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}
	return c.String(http.StatusOK, "Done updating referral config!")
}

// ------------------------------------------------------------------

func GetInsuranceParams(c echo.Context) error {
	rsp := orders.CurrentInsurance.GetParams()

	return c.JSON(http.StatusOK, rsp)
}

func UpdateInsuranceParams(c echo.Context) error {
	var config structures.InsuranceParams

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "UpdateConfigByKey",
		"config key": structures.DBConfigInsuranceParams,
	})

	if err := c.Bind(&config); err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error binding config into structure",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: err.Error()})
	}

	err := models.UpdateInsuranceParams(c.Request().Context(), &config)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.GetConfigValueByKey").Errorln(err.Error())

		return c.JSON(http.StatusBadRequest, err.Error())
	}

	msg := logs.OutputRestOK(
		fmt.Sprintf(
			"Изменения вступят в силу в течение %.0f секунд",
			tickers.InsuranceConfigUpdateInterval.Seconds(),
		),
		http.StatusOK)

	return c.JSON(http.StatusOK, msg)
}
