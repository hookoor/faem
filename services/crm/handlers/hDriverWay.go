package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

// DriverWay - TODO: caption
func DriverWay(c echo.Context) error {
	var coord []structures.Coordinates

	err := c.Bind(&coord)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting coordinates to build a route",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Coordinates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(coord) == 0 {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting coordinates to build a route",
			"reason": "empty data",
		}).Error(err)
		res := logs.OutputRestError("empty data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(coord) == 1 {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting coordinates to build a route",
			"reason": "not enough values",
		}).Error(err)
		res := logs.OutputRestError("not enough values", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	var someShit structures.Route
	var manyShit orders.OrderCRM
	for _, item := range coord {
		someShit.Lat = float32(item.Lat)
		someShit.Lon = float32(item.Long)
		manyShit.Routes = append(manyShit.Routes, someShit)
	}

	result, err := orders.GetItinerary(manyShit.Routes)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting coordinates to build a route",
			"reason": "get routing fro OSRM",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Coordinates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, result)
}

func GetRouteWay(c echo.Context) error {
	var coord []structures.Coordinates

	err := c.Bind(&coord)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":          "getting route way",
		"elements count": len(coord),
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Coordinates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(coord) < 2 {
		log.WithFields(logrus.Fields{
			"reason": "not enough values",
		}).Error(err)
		res := logs.OutputRestError("not enough values", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	result, err := models.GetItinerary(coord)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "get routing fro OSRM",
		}).Error(err)
		res := logs.OutputRestError("err getting itinerary", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, result)
}
