package handlers

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// RegionsController -
type RegionsController struct {
	dbi *models.DB
}

// NewRegionsController - creates a new regions controller.
func NewRegionsController(db *models.DB) *RegionsController {
	return &RegionsController{
		dbi: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// GetByID -
func (rc *RegionsController) GetByID(c echo.Context) error {
	ctx := c.Request().Context()

	regionID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	region, err := rc.dbi.RegiosModel.GetByID(ctx, regionID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, region)
}

// GetDriversTariffs -
func (rc *RegionsController) GetDriversTariffs(c echo.Context) error {
	ctx := c.Request().Context()

	regionID, err := strconv.Atoi(c.Param("id"))
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	region, err := rc.dbi.RegiosModel.GetTariffs(ctx, regionID)
	if err != nil {
		return c.JSON(http.StatusBadRequest, errpath.Err(err).Error())
	}

	return c.JSON(http.StatusOK, region)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
