package addresses

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"sort"
	"strings"
	"sync"
	"unicode/utf8"
)

const (
	maxAutocompleteCount = 10
	maxErrorsInInput     = 1
)

var cache struct {
	sync.RWMutex
	routes    map[string]structures.Route
	addresses []*Address
}

func UpdateCache(routes []structures.Route) {
	addresses := make([]*Address, 0, len(routes))
	routesMap := make(map[string]structures.Route)
	for _, route := range routes {
		routesMap[route.UUID] = route
		addresses = append(addresses, AddressFromRoute(route))
	}

	cache.Lock()
	cache.routes = routesMap
	cache.addresses = addresses
	cache.Unlock()
}

func scoreAndSort(addresses []*Address, inputWords []string, cityPriorities map[string]float64) {
	for i, addr := range addresses {
		addresses[i].Weight = scoreMatches(inputWords, addr) * totalWeightCoefficient(addr, cityPriorities)
	}

	sort.Slice(addresses, func(i, j int) bool {
		return addresses[i].Weight > addresses[j].Weight
	})
}

func ExperimentalAddressMatch(input string, cityPriorities map[string]float64) []structures.Route {
	cache.RLock()
	defer cache.RUnlock()

	input = prepareString(input)

	words := strings.Fields(input)

	prefixMatches := make([]*Address, 0)
	matches := make([]*Address, 0)
	partMatches := make([]*Address, 0)

	for _, addr := range cache.addresses {
		switch {
		case addr.ContainsAllPrefixes(words):
			prefixMatches = append(prefixMatches, addr)
		case addr.ContainsAll(words):
			matches = append(matches, addr)
		case addr.ContainsAny(words):
			partMatches = append(partMatches, addr)
		}
	}

	scoreAndSort(prefixMatches, words, cityPriorities)
	scoreAndSort(matches, words, cityPriorities)

	resultMatches := append(prefixMatches, matches...)

	// если не добрали до нужного числа вариантов, смотрим частичные совпадения
	if len(resultMatches) < maxAutocompleteCount && len(partMatches) > 0 {
		// сколько не хватает для выдачи максимального числа результатов
		remainingSlots := maxAutocompleteCount - len(resultMatches)

		sortedByCount := splitByOccurrencesCount(partMatches, words)

		for _, ms := range sortedByCount {
			scoreAndSort(ms, words, cityPriorities)
			cutSize := len(ms)
			if cutSize > remainingSlots {
				cutSize = remainingSlots
			}
			resultMatches = append(resultMatches, ms[:cutSize]...)

			remainingSlots -= cutSize
			if remainingSlots <= 0 {
				break
			}
		}
	} else if len(resultMatches) > maxAutocompleteCount {
		resultMatches = resultMatches[:maxAutocompleteCount]
	}

	result := make([]structures.Route, len(resultMatches))
	for i := range result {
		result[i] = cache.routes[resultMatches[i].UUID]
	}

	return result
}

func splitByOccurrencesCount(partMatches []*Address, words []string) [][]*Address {
	type countedMatches struct {
		matches []*Address
		count   int
	}

	var matchesWithCount []countedMatches
outer:
	for _, match := range partMatches {
		count := match.ContainsCount(words)
		for i, mwc := range matchesWithCount {
			if mwc.count == count {
				matchesWithCount[i].matches = append(mwc.matches, match)
				continue outer
			}
		}

		matchesWithCount = append(matchesWithCount, countedMatches{
			count:   count,
			matches: []*Address{match},
		})
	}

	sort.Slice(matchesWithCount, func(i, j int) bool {
		return matchesWithCount[i].count > matchesWithCount[j].count
	})

	result := make([][]*Address, len(matchesWithCount))
	for i := range matchesWithCount {
		result[i] = matchesWithCount[i].matches
	}

	return result
}

func totalWeightCoefficient(addr *Address, cityPriorities map[string]float64) float64 {
	return weightsMap[addr.Type] * (1 + cityPriorities[addr.City])
}

func matchAddress(addressPart string, inputWords []string, levenshteinThreshold int) float64 {
	addressWords := strings.Fields(addressPart)

	streak, highestStreak := 0.0, 0.0
	totalMatches := 0.0
	prevIndex := -1
	matchWeight := 0.0
	for _, inputWord := range inputWords {
		index := -1
		for i, addrWord := range addressWords {
			hasPrefix, exactMatch := HasPrefix(addrWord, inputWord, levenshteinThreshold)
			if hasPrefix {
				wordLen := float64(utf8.RuneCountInString(inputWord))
				weight := wordLen * wordLen / float64(utf8.RuneCountInString(addrWord))
				if !exactMatch {
					weight /= 5
				}
				matchWeight += weight
				totalMatches++
				index = i
				break
			}
		}

		if index >= 0 {
			// substring found
			if index > prevIndex {
				// substring is after previous substring
				streak++
				if streak > highestStreak {
					highestStreak = streak
				}
			} else {
				// substring is not after previous substring
				streak = 0
			}
		}

		prevIndex = index
	}

	totalScore := 0.0
	if totalMatches > 0 {
		totalScore = matchWeight * (highestStreak / totalMatches)
	}
	return totalScore
}

var weightsMap = map[DestinationPointType]float64{
	TypeCity:        40,
	TypeStreet:      25,
	TypeAddress:     13,
	TypePublicPlace: 8,
}

func containsDigits(words []string) bool {
	for _, word := range words {
		if strings.ContainsAny(word, "1234567890") {
			return true
		}
	}

	return false
}

func scoreMatches(inputWords []string, addr *Address) float64 {
	cityWeight := matchAddress(addr.City, inputWords, 4)
	streetWeight := matchAddress(addr.Street, inputWords, 4)
	houseWeight := 0.0
	if streetWeight > 0 { // сверяем номер дома только если улица совпала
		houseWeight = matchAddress(addr.House, inputWords, 3)
	}
	publicPlaceWeight := 0.0
	if addr.PublicPlace != "" {
		publicPlaceWeight = matchAddress(addr.PublicPlace, inputWords, 3)
		//publicPlaceWeight /= float64(utf8.RuneCountInString(addr.PublicPlace))
	}

	matchPoints := 0.0
	if cityWeight > 0 {
		if addr.Type == TypeCity {
			cityWeight *= 2
		}
		matchPoints++
	}
	if streetWeight > 0 {
		if addr.Type == TypeStreet || addr.Type == TypeAddress {
			streetWeight *= 2
		}
		matchPoints++
	}
	if publicPlaceWeight > 0 {
		if addr.Type == TypePublicPlace {
			publicPlaceWeight *= 2
		}
		matchPoints++
	}

	totalScore := cityWeight + streetWeight + houseWeight + publicPlaceWeight
	totalScore *= matchPoints

	return totalScore
}

func HasPrefix(s, pref string, levenshteinThreshold int) (hasPrefix bool, exactMatch bool) {
	exactMatch = true
	if len(pref) >= levenshteinThreshold {
		var typoCount int
		hasPrefix, typoCount = HasPrefixWithTypos(s, pref, maxErrorsInInput)
		if typoCount > 0 {
			exactMatch = false
		}
	} else {
		hasPrefix = strings.HasPrefix(s, pref)
	}

	return
}

func HasPrefixWithTypos(s, pref string, typoLimit int) (bool, int) {
	return hasPrefixWithTypos(chars(s), chars(pref), typoLimit)
}
