package addresses

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"strings"
)

type DestinationPointType int

const (
	TypeUndefined DestinationPointType = -1

	TypeCity DestinationPointType = iota
	TypeStreet
	TypePublicPlace
	TypeAddress
)

func convertDestinationPointType(types structures.DestinationPointTypes) DestinationPointType {
	switch types {
	case structures.AddressType:
		return TypeAddress
	case structures.PublicPlaceType:
		return TypePublicPlace
	case structures.StreetType:
		return TypeStreet
	case structures.CityType:
		return TypeCity
	}

	return TypeUndefined
}

type Address struct {
	UUID        string
	Type        DestinationPointType
	City        string
	Street      string
	House       string
	Full        string
	FullWords   []string
	PublicPlace string
	Weight      float64
}

func AddressFromRoute(route structures.Route) *Address {
	var addr Address

	addr.UUID = route.UUID
	addr.Type = convertDestinationPointType(route.PointType)
	addr.City = prepareString(route.City)
	addr.Street = prepareString(route.Street)
	addr.House = prepareString(route.House)
	if addr.Type == TypePublicPlace {
		addr.PublicPlace = prepareString(route.Value)
	}
	addr.Full = strings.Join([]string{
		addr.City,
		addr.Street,
		addr.House,
		addr.PublicPlace,
	}, " ")
	addr.FullWords = strings.Fields(addr.Full)

	return &addr
}

var replacer = strings.NewReplacer(
	",", " ",
	"(", " ",
	")", " ",
	"-", " ",
	"\"", " ",
	"'", " ",
	".", " ",
)

func prepareString(s string) string {
	s = strings.ToLower(s)
	s = replacer.Replace(s)

	return s
}

func (a *Address) ContainsAllPrefixes(inputWords []string) bool {
	for _, word := range inputWords {
		prefixFound := false
		for _, addr := range a.FullWords {
			if strings.HasPrefix(addr, word) {
				prefixFound = true
				break
			}
		}

		if !prefixFound {
			return false
		}
	}

	return true
}

func (a *Address) ContainsAll(inputWords []string) bool {
	for _, word := range inputWords {
		if !strings.Contains(a.Full, word) {
			return false
		}
	}

	return true
}

func (a *Address) ContainsAny(inputWords []string) bool {
	for _, word := range inputWords {
		if strings.Contains(a.Full, word) {
			return true
		}
	}

	return false
}

func (a *Address) ContainsCount(inputWords []string) int {
	matchesCount := 0
	for _, word := range inputWords {
		if strings.Contains(a.Full, word) {
			matchesCount++
		}
	}

	return matchesCount
}
