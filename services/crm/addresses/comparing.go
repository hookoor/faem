package addresses

type chars []rune

func (c chars) index(i int) rune {
	if i >= len(c) {
		return rune(0)
	}
	return c[i]
}

func hasPrefixWithTypos(s, pref chars, typoLimit int) (bool, int) {
	if len(pref) > len(s) {
		s = append(s, make(chars, len(pref)-len(s))...)
	}

	typoCount := 0
	hasPrefix := true
	for i, j := 0, 0; j < len(pref); i, j = i+1, j+1 {
		if s.index(i) == pref.index(j) {
			continue
		}

		typoCount++
		if typoCount > typoLimit {
			hasPrefix = false
			break
		}

		if s.index(i+1) == pref.index(j) {
			i++
			continue
		}

		if s.index(i) == pref.index(j+1) {
			j++
			continue
		}
	}

	return hasPrefix, typoCount
}
