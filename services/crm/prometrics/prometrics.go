package prometrics

import (
	promethGo "github.com/prometheus/client_golang/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
)

var (
	Pt prometheusData
)

type prometheusData struct {
	prometh          *prometheus.Prometheus
	OrderStatesCount *promethGo.GaugeVec
}

//// InitPrometheus initialize Prometheues instance
//func InitPrometheus(prometh *prometheus.Prometheus) error {
//	//init gloabal var
//	Pt.prometh = prometh
//
//	//setting up prometheusData metrics variables
//	for _, metricDef := range prometh.MetricsList {
//		switch metricDef.ID {
//		case "feamOrderStates":
//			Pt.OrderStatesCount = metricDef.MetricCollector.(*promethGo.GaugeVec)
//		}
//	}
//
//	if err := getInitOrderStatesValues(); err != nil {
//		return errors.Wrap(err, "Error getting start states")
//	}
//
//	return nil
//}
//
//func getInitOrderStatesValues() error {
//	for _, ordStatus := range variables.OrderStates {
//		count, err := orders.CountByState(ordStatus)
//		if err != nil {
//			return errors.Wrapf(err, "Error getting count of order in state: %v", ordStatus)
//		}
//		Pt.OrderStatesCount.WithLabelValues(ordStatus).Set(float64(count))
//	}
//	return nil
//}
//
//func CRMMetrics() []*prometheus.Metric {
//	var ordStatesCnt = &prometheus.Metric{
//		ID:          "feamOrderStates",
//		Name:        "order_states_count",
//		Description: "How many orders on each state",
//		Type:        "gauge_vec",
//		Args:        []string{"status"},
//	}
//	return []*prometheus.Metric{
//		ordStatesCnt,
//	}
//}
