package models

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func UpdateDriverKarma(karma structures.UserKarma) error {
	_, err := db.Model((*DriverCRM)(nil)).
		Where("uuid = ?", karma.UserUUID).
		Set("karma = GREATEST(?, 0)", karma.Karma).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update driver karma")
	}
	return nil
}

func UpdateClientKarma(karma structures.UserKarma) error {
	_, err := db.Model((*ClientCRM)(nil)).
		Where("uuid = ?", karma.UserUUID).
		Set("karma = GREATEST(?, 0)", karma.Karma).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update client karma")
	}
	return nil
}

func UpdateClientBlacklist(blacklist structures.UserBlacklist) error {
	client := ClientCRM{
		Client: structures.Client{
			UUID:      blacklist.UserUUID,
			Blacklist: blacklist.Blacklist,
		},
	}
	_, err := db.Model(&client).
		Where("uuid = ?uuid").
		Set("blacklist = ?blacklist").
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update client blacklist")
	}
	return nil
}
