package models

import (
	"time"

	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// PCCriteria критерии фильтра фотоконтроля
type PCCriteria struct {
	MinDate     time.Time       `json:"min_date"`
	MaxDate     time.Time       `json:"max_date"`
	Active      bool            `json:"active"`
	Pager       urlvalues.Pager `json:"-"`
	ControlType string          `json:"control_type"`
	DriverUUID  string          `json:"driver_uuid"`
}
type (
	PhotoControlCRM struct {
		tableName struct{} `sql:"crm_photocontrols"`
		structures.PhotoControl
		CreatedAt   time.Time `json:"-"`
		CreatedUnix int64     `json:"created_at" sql:"-"`
	}
)

func (phCon *PhotoControlCRM) Save() error {
	phCon.CreatedAt = time.Now()
	_, err := db.Model(phCon).Insert()
	return err
}

func (phCon *PhotoControlCRM) FillUnixField() {
	phCon.CreatedUnix = phCon.CreatedAt.Unix()
}

//PhotoControlListWithOptions - returns pcontrols according to some criteria
func (cr *PCCriteria) PhotoControlListWithOptions() ([]PhotoControlCRM, int, error) {
	var (
		count     int
		err       error
		pControls []PhotoControlCRM
	)
	query := db.Model(&pControls).
		Where("created_at <= ? AND created_at >= ?", cr.MaxDate, cr.MinDate).
		Where("control_type ILIKE ?", "%"+cr.ControlType+"%").
		Where("driver_id ILIKE ?", "%"+cr.DriverUUID+"%")
	if cr.Active {
		query = query.
			Where("approved is not true")
	}
	count, err = query.
		Count()
	if err != nil {
		return pControls, count, errors.Errorf("error counting the number of records,%s", err)
	}
	if count == 0 {
		return pControls, count, nil
	}
	err = query.
		Order("created_at desc").
		Apply(cr.Pager.Pagination).
		Select()
	if err != nil {
		return pControls, count, errors.Errorf("error finding photocontrols,%s", err)
	}
	return pControls, count, nil
}

//Update - update photocontrol by uuid
func (phCon *PhotoControlCRM) Update(uuid string) error {
	err := phCon.validUpdate()
	if err != nil {
		return err
	}
	_, err = db.Model(phCon).Where("control_id = ?", uuid).
		Returning("*").
		UpdateNotNull()
	if err != nil {
		return err
	}

	return nil
}
func (phCon *PhotoControlCRM) validUpdate() error {
	if len(phCon.Photos) == 0 {
		return errors.New("необходима информация о фотографиях")
	}
	err := phCon.checkApproved()
	return err
}

func (phCon *PhotoControlCRM) checkApproved() error {
	phCon.Approved = true
	for _, photo := range phCon.Photos {
		if photo.ControlVariant == "" {
			return errors.New("укажите тип для всех фотографий")
		}
		if !photoIsApproved(photo) {
			phCon.Approved = false
		}
	}
	return nil
}

// GetPhotoControlByUUID returning object by interface and UUID
func GetPhotoControlByUUID(uuid string) (PhotoControlCRM, error) {
	var phCon PhotoControlCRM
	err := db.Model(&phCon).
		Where("control_id = ?", uuid).
		Select()
	if err != nil {
		return phCon, err
	}
	return phCon, err
}
func photoIsApproved(photo structures.Photo) bool {
	return photo.Status == structures.ApprovedStatus
}
