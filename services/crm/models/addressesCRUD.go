package models

import (
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type PublicPlace struct {
	tableName    struct{} `sql:"crm_public_place"`
	UUID         string   `json:"uuid"`
	ID           int      `json:"-"`
	FullName     string   `json:"full_name"`
	Category     string   `json:"category"`
	Street       string   `json:"street"`
	Building     string   `json:"building"`
	Comment      string   `json:"comment"`
	SpecialOrder string   `json:"-"`
	OutOfTown    string   `json:"out_of_town"`
	Latitude     float32  `json:"lat"`
	Longitude    float32  `json:"lon"`
	HideAddress  string   `json:"-"`
	Region       string   `json:"region"`
	Synonym      string   `json:"-"`
	Name         string   `json:"name"`
	Priority     int      `json:"-"`
}
type PointsFilterCriteria struct {
	Street            string                           `json:"street"`
	UnrestrictedValue string                           `json:"unrestricted_value"`
	Category          string                           `json:"category"`
	House             string                           `json:"house"`
	City              string                           `json:"city"`
	PointType         structures.DestinationPointTypes `json:"point_type"`
	EmptyCoordinates  bool                             `json:"empty_coordinates"`
	OutOfTown         *bool                            `json:"out_of_town"`
	Deleted           bool                             `json:"deleted"`
	Pager             urlvalues.Pager                  `json:"-"`
}

func appendIfNotExists(inArray []string, element string) []string {
	for _, val := range inArray {
		if val == element {
			return inArray
		}
	}
	return append(inArray, element)
}
func GetPublicPlacesCategory() []string {
	var result []string
	for _, dp := range AllRoutes {
		if dp.Category == "" || dp.PointType != structures.PublicPlaceType {
			continue
		}
		result = appendIfNotExists(result, dp.Category)
	}
	return result
}
func (cr *PointsFilterCriteria) GetDestinationPointsByFilter() ([]DestinationPoint, int, error) {
	var (
		count  int
		err    error
		result []DestinationPoint
	)
	query := db.Model(&result).
		Where("unrestricted_value ILIKE ?", "%"+cr.UnrestrictedValue+"%")

	if cr.EmptyCoordinates {
		query = query.
			Where("lat is null or lon is null or lat = 0 or lon = 0")
	}
	if cr.Deleted {
		query = query.
			Where("deleted is true")
	} else {
		query = query.
			Where("deleted is not true")
	}
	if cr.Street != "" {
		query = query.
			Where("street ILIKE ?", "%"+cr.Street+"%")
	}
	if cr.House != "" {
		query = query.
			Where("street ILIKE ?", "%"+cr.House+"%")
	}

	if cr.PointType != "" {
		query = query.
			Where("point_type = ?", cr.PointType)
	}

	if cr.City != "" {
		query = query.
			Where("city = ?", cr.City)
	}
	if cr.Category != "" {
		query = query.
			Where("category = ?", cr.Category)
	}
	if cr.OutOfTown != nil {
		switch *cr.OutOfTown {
		case true:
			query = query.
				Where("out_of_town is true")
		case false:
			query = query.
				Where("out_of_town is not true")
		}
	}
	count, err = query.
		Count()
	if err != nil {
		return result, count, errors.Errorf("error counting the number of records,%s", err)
	}
	if count == 0 {
		return result, count, nil
	}
	err = query.
		Order("unrestricted_value").
		Apply(cr.Pager.Pagination).
		Select()
	if err != nil {
		return result, count, errors.Errorf("error finding public places,%s", err)
	}
	return result, count, nil
}
func (dp *DestinationPoint) Update(uuid string) *structures.ErrorWithLevel {
	dp.UUID = uuid
	errWL := dp.validation(uuid)
	if errWL != nil {
		return errWL
	}
	dp.Radius = 10000
	err := UpdateByPK(dp)
	if err != nil {
		return structures.Error(err)
	}
	return errWL
}
func (dp *DestinationPoint) Create() *structures.ErrorWithLevel {
	dp.Radius = 10000
	dp.UUID = structures.GenerateUUID()
	errWL := dp.validation(dp.UUID)
	if errWL != nil {
		return errWL
	}
	_, err := db.Model(dp).
		Returning("*").
		Insert()
	if err != nil {
		return structures.Error(err)
	}
	return nil
}
func (dp *DestinationPoint) validFields() *structures.ErrorWithLevel {
	if dp.Value == "" || dp.PointType == "" || dp.City == "" {
		return structures.Warning(errors.Errorf("Название, тип и город - обязательные поля"))
	}
	if dp.PointType == structures.PublicPlaceType && dp.Street == "" {
		return structures.Warning(errors.Errorf("Для общественного места необходимо указать как минимум улицу"))
	}
	if dp.PointType == structures.AddressType && dp.House == "" {
		return structures.Warning(errors.Errorf("Необходимо указать номер дома для адреса"))
	}
	return nil
}
func (dp *DestinationPoint) validation(uuid string) *structures.ErrorWithLevel {
	errWL := dp.validFields()
	if errWL != nil {
		return errWL
	}
	err := dp.fillFullNameAndType()
	if err != nil {
		return structures.Warning(err)
	}
	check, err := db.Model(dp).
		Where("unrestricted_value = ? and uuid != ?", dp.UnrestrictedValue, uuid).
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if check {
		return structures.Warning(errors.Errorf("Запись с таким адресом и названием уже существует"))
	}
	if dp.PointType == structures.PublicPlaceType {
		errWL := dp.fillCoordinatesIfNeed()
		if errWL != nil {
			return errWL
		}
	}
	if dp.locationsIsEmpty() {
		return structures.Warning(errors.Errorf("Координаты не могут быть пустыми"))
	}
	return nil
}
func SetDestinationPointDeleted(uuid string) error {
	_, err := db.Model(&DestinationPoint{}).
		Set("deleted = true").
		Where("uuid = ?", uuid).
		Update()
	return err
}
func (dp *DestinationPoint) fillFullNameAndType() error {
	switch dp.PointType {
	case structures.PublicPlaceType:
		dp.Type = "Общественное место"
		dp.UnrestrictedValue = dp.Value + ", " + dp.Street + " " + dp.House
	case structures.AddressType:
		if dp.City == "Владикавказ" { //горят твои огни в глазах у нас
		}
		dp.UnrestrictedValue = dp.Street + " " + dp.House
	case structures.StreetType:
		dp.Type = "Улица"
		dp.UnrestrictedValue = dp.City + ", " + dp.Street
		if dp.City == "Владикавказ" { //горят твои огни в глазах у нас
			dp.UnrestrictedValue = dp.Street
		}
	case structures.CityType:
		dp.Type = "Населенный пункт"
		dp.UnrestrictedValue = dp.City
	default:
		return errors.Errorf("invalid point type")
	}
	return nil
}

func (dp *DestinationPoint) fillCoordinatesIfNeed() *structures.ErrorWithLevel {
	if !dp.locationsIsEmpty() {
		return nil
	}
	neededAddress, check, err := dp.getAddressByStreetAndHouse()
	if err != nil {
		return structures.Error(errors.Errorf("error getting address, %s", err))
	}
	if !check {
		return structures.Warning(errors.Errorf("Такого адреса не существует. Создайте его или заполните координаты вручную"))
	}
	if neededAddress.locationsIsEmpty() {
		return structures.Warning(errors.Errorf("У адреса, к которому вы хотите привязать точку, некорректные координаты"))
	}
	dp.Lat, dp.Lon = neededAddress.Lat, neededAddress.Lon
	return nil
}
func (dp *DestinationPoint) locationsIsEmpty() bool {
	return dp.Lat == 0 || dp.Lon == 0
}

// getAddressByStreetAndHouse returns record with point type = address if it is exists or false if it is not exists
func (dp *DestinationPoint) getAddressByStreetAndHouse() (DestinationPoint, bool, error) {
	var result DestinationPoint
	query := db.Model(&result).
		Where("point_type = ?", structures.AddressType).
		Where("street = ?", dp.Street).
		Where("house = ?", dp.House)
	check, err := query.
		Exists()
	if err != nil {
		return result, check, errors.Errorf("error checking record exists,%s", err)
	}
	if !check {
		return result, check, nil
	}
	err = query.
		First()
	if err != nil {
		return result, check, errors.Errorf("error getting address, %s", err)
	}
	return result, check, nil
}
