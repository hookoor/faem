package models

import (
	"fmt"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
)

// ----------------
// ----------------
// ----------------

// ----------------
// ----------------
// ----------------

// IDebug -
type IDebug interface {
	GetGlobalVars()
}

// DebugModel -
type DebugModel struct {
	db           *pg.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// InitDebugModel -
func InitDebugModel(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *DebugModel {
	debugInst := DebugModel{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
	return &debugInst
}

// GetGlobalVars -
func (d DebugModel) GetGlobalVars() {
	for i := 0; i < 6; i++ {
		go func(itr int) {
			d.dbQueryTrace.StartTrace()
			_, err := d.db.Query(nil, fmt.Sprintf(`SELECT * FROM crm_drivers AS "driver_crm" LIMIT %v `, itr))
			if err != nil {
				fmt.Println(err)
			}
		}(i)
	}
}

// ----------------
// ----------------
// ----------------
