package models

import (
	"context"
	"time"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

// FeatureCRM - order frient structure
type FeatureCRM struct {
	tableName struct{} `sql:"crm_features" pg:",discard_unknown_columns"`
	ormStruct
	structures.Feature
}

// Create - save structure to database
func (fr *FeatureCRM) Create() (FeatureCRM, error) {
	nFeature, err := fr.validCreation()
	if err != nil {
		return nFeature, errors.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nFeature, err
	}

	nFeature.UUID = uu.String()
	_, err = db.Model(&nFeature).Returning("*").Insert()
	if err != nil {
		return nFeature, errors.Errorf("DB error. %v ", err)
	}
	return nFeature, nil
}

func (fr *FeatureCRM) validCreation() (FeatureCRM, error) {
	var newFeature FeatureCRM
	if fr.Name == "" {
		return newFeature, errors.Errorf("Field name is required")
	}
	newFeature.Name = fr.Name
	newFeature.Tag = fr.Tag
	newFeature.ServicesUUID = fr.ServicesUUID
	newFeature.Price = fr.Price
	newFeature.Comment = fr.Comment

	return newFeature, nil
}

func (FeatureCRM) GetFeaturesByUUIDs(ctx context.Context, featuresUUIDs ...string) ([]FeatureCRM, error) {
	if len(featuresUUIDs) == 0 {
		return nil, nil
	}

	var features []FeatureCRM

	err := db.ModelContext(ctx, &features).
		Where("deleted is not true").
		WhereIn("uuid in (?)", featuresUUIDs).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return features, nil
}

// FeatureList - return all Feature
func FeatureList(serviceUUID string) ([]FeatureCRM, error) {
	var (
		frients []FeatureCRM
	)
	query := db.Model(&frients).
		Where("deleted is not true").
		Order("updated_at DESC NULLS LAST").
		Order("created_at DESC NULLS LAST")

	if serviceUUID != "" {
		query.Where("(?) <@ services_uuid", pg.Array([]string{serviceUUID}))
	}
	err := query.
		Select()
	if err != nil {
		return frients, err
	}

	return frients, nil
}

// Update godoc
func (fr *FeatureCRM) Update(uuid string) (FeatureCRM, error) {

	uFeature, err := fr.validUpdate(uuid)
	if err != nil {
		return uFeature, errors.Errorf("Data validation error. %v", err)
	}

	_, err = db.Model(&uFeature).
		Where("uuid=?uuid").Returning("*").
		UpdateNotNull()
	if err != nil {
		return FeatureCRM{}, errpath.Err(err)
	}

	return uFeature, nil
}

//
func (fr *FeatureCRM) validUpdate(uuid string) (FeatureCRM, error) {

	flag := false
	var frient FeatureCRM

	if fr.Name != "" {
		frient.Name = fr.Name
		flag = true
	}
	if fr.Comment != "" {
		frient.Comment = fr.Comment
		flag = true
	}
	if fr.Price != 0 {
		frient.Price = fr.Price
		flag = true
	}
	if fr.Tag != nil {
		frient.Tag = fr.Tag
		flag = true
	}
	if fr.ServicesUUID != nil {
		frient.ServicesUUID = fr.ServicesUUID
		flag = true
	}
	if !flag {
		return frient, errors.Errorf("Required fields are empty")
	}
	frient.UpdatedAt = time.Now()
	frient.UUID = uuid
	err := CheckExistsUUID(&frient, uuid)
	if err != nil {
		return frient, err
	}

	return frient, nil
}

// // CheckExists return nil if fr.ID not exists or deleted
// // TODO: вынести в общие методы и сделать его универсальным
// func (fr *FeatureCRM) CheckExists(id int) error {
// 	exs, _ := db.Model(fr).Column("id").Where("id = ? AND deleted is not true", id).Exists()
// 	if !exs {
// 		return errors.Errorf("Not found ID=%v", id)
// 	}
// 	return nil
// }

// // BeforeUpdate godoc
// // TODO: вынести в общие методы и сделать его универсальным
// func (fr *FeatureCRM) BeforeUpdate(db orm.DB) error {
// 	fr.UpdatedAt = time.Nfr()
// 	return nil
// }

// SetDeleted godoc
func (fr *FeatureCRM) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var (
		frient FeatureCRM
	)
	err := CheckExistsUUID(&frient, fr.UUID)
	if err != nil {
		return err
	}
	_, err = db.Model(fr).Where("uuid=?uuid").Set("deleted = true").Update()
	if err != nil {
		return err
	}
	return nil
}

// FeatureByRegion godoc
func FeatureByRegion(regionid int) ([]FeatureCRM, error) {
	var frients []FeatureCRM
	err := db.Model(&frients).
		Where("region_id = ? AND deleted is not true", regionid).
		Select()
	if err != nil {
		return frients, err
	}
	return frients, nil
}
