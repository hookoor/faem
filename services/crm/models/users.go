package models

import (
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"github.com/go-pg/pg/urlvalues"
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"golang.org/x/crypto/bcrypt"
)

// UsersCRM - order user structure
type UsersCRM struct {
	tableName struct{} `sql:"crm_users" pg:",discard_unknown_columns"`
	ormStruct
	structures.Users
}
type UsersFilterCriteria struct {
	Active          *bool    `json:"active"`
	Name            string   `json:"name"`
	Role            string   `json:"role"`
	TaxiParksUUID   []string `query:"taxi_parks"`
	TPFilterApplied bool     `query:"-"`
	Pager           urlvalues.Pager
}

// GetUserExten godoc
func GetUserExten(userUUID string) (string, error) {
	var user UsersCRM
	err := db.Model(&user).Where("uuid = ?", userUUID).
		Select()
	return user.LoginTelephony, err
}

// Create - save structure to database
func (us *UsersCRM) Create() (*UsersCRM, error) {
	if err := us.validCreation(); err != nil {
		return nil, errors.Errorf("Data validation error. %v ", err)
	}

	hashedPass, err := bcrypt.GenerateFromPassword([]byte(us.Password), 14)
	if err != nil {
		return nil, errors.Errorf("Ошибка создания нового пароля")
	}
	us.Password = string(hashedPass)

	us.UUID = structures.GenerateUUID()

	_, err = db.Model(us).Returning("*").Insert()
	if err != nil {
		return nil, errors.Errorf("DB error. %v ", err)
	}

	return us, nil
}

func (us *UsersCRM) validCreation() error {
	if len(us.RegionIDs) == 0 {
		return errors.New("Пустое поле списка регионов")
	}

	if us.Name == "" {
		return errors.New("Field name is required")
	}

	if err := us.checkExistsUserByLogin(us.Login); err != nil {
		return err
	}

	return nil
}

func (us *UsersCRM) checkExistsUserByLogin(login string, uuid ...string) error {
	var userUUID string
	if len(uuid) != 0 {
		userUUID = uuid[0]
	}

	query := db.Model(&UsersCRM{}).
		Where("login = ?", login).
		Where("uuid != ?", userUUID).
		Where("deleted is not true")
	if ok, err := query.Exists(); err != nil {
		return errors.Errorf("error checking user exists by login,%s", err)
	} else if ok {
		return errors.Errorf("Пользователь с таким логином уже существует")
	}

	return nil
}

// UserByID - return User structure based on ID
func UserByID(id int) (UsersCRM, error) {
	var user UsersCRM
	user.ID = id
	err := db.Model(&user).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return user, err
	}
	return user, nil
}

// UsersList - return all Users
func UsersList(AllowedTaxiParksUUIDs []string) ([]UsersCRM, error) {
	var users []UsersCRM
	query := db.Model(&users).
		Where("deleted is not true")

	if len(AllowedTaxiParksUUIDs) != 0 {
		query = query.
			Where("taxi_parks_uuid && (?)", pg.Array(AllowedTaxiParksUUIDs))
	}
	if err := query.Select(); err != nil {
		return nil, err
	}
	return users, nil
}
func (cr *UsersFilterCriteria) GetByOptions(userRole string) ([]UsersCRM, int, error) {
	result := make([]UsersCRM, 0)
	query := db.Model(&result).
		Where("deleted is not true").
		Where("name ILIKE ?", "%"+cr.Name+"%").
		Order("created_at DESC")

	if cr.Active != nil {
		query.Where("active = ?", cr.Active)
	}
	if cr.Role != "" {
		query.Where("role = ?", cr.Role)
	}
	if cr.TPFilterApplied {
		query.Where("taxi_parks_uuid && (?)", pg.Array(cr.TaxiParksUUID))
	}

	count, err := query.Count()
	if err != nil {
		return nil, 0, err
	}

	if err := query.Apply(cr.Pager.Pagination).Select(); err != nil {
		return nil, 0, err
	}

	return result, count, nil
}

// Update godoc
func (us *UsersCRM) Update(uuid string) (*UsersCRM, error) {
	uUser, err := us.validUpdate(uuid)
	if err != nil {
		return nil, err
	}

	if err := UpdateByPK(uUser); err != nil {
		return uUser, errors.Errorf("DB error. %v", err)
	}

	return uUser, nil
}

//
func (us *UsersCRM) validUpdate(uuid string) (*UsersCRM, error) {

	flag := false
	user := new(UsersCRM)
	if len(us.RegionIDs) == 0 {
		return nil, errors.New("Пустое поле списка регионов")
	}
	if us.Name != "" {
		user.Name = us.Name
		flag = true
	}
	if us.Active != nil {
		user.Active = us.Active
		flag = true
	}
	if us.Comment != "" {
		user.Comment = us.Comment
		flag = true
	}
	if us.StoreUUID != nil {
		user.StoreUUID = us.StoreUUID
		flag = true
	}
	if us.Role != "" {
		user.Role = us.Role
		flag = true
	}
	if us.Login != "" {
		user.Login = us.Login
		flag = true
	}
	if us.TaxiParksUUID != nil {
		user.TaxiParksUUID = us.TaxiParksUUID
		flag = true
	}
	if us.RegionIDs != nil {
		user.RegionIDs = us.RegionIDs
		flag = true
	}
	if us.Password != "" {
		hashedPass, err := bcrypt.GenerateFromPassword([]byte(us.Password), 14)
		if err != nil {
			return user, errors.Errorf("Ошибка создания нового пароля")
		}
		user.Password = string(hashedPass)
		flag = true
	}
	if us.LoginTelephony != "" {
		user.LoginTelephony = us.LoginTelephony
		flag = true
	}
	if us.PasswordTelephony != "" {
		user.PasswordTelephony = us.PasswordTelephony
		flag = true
	}

	if !flag {
		return user, errors.Errorf("Required fields are empty")
	}
	// user.Users = us.Users
	user.UpdatedAt = time.Now()
	user.UUID = uuid

	if err := CheckExistsUUID(user, uuid); err != nil {
		return nil, err
	}

	if err := us.checkExistsUserByLogin(us.Login, uuid); err != nil {
		return nil, err
	}
	user.Deleted = false

	return user, nil
}

// // CheckExists return nil if us.ID not exists or deleted
// // TODO: вынести в общие методы и сделать его универсальным
// func (us *UsersCRM) CheckExists(id int) error {
// 	exs, _ := db.Model(us).Column("id").Where("id = ? AND deleted is not true", id).Exists()
// 	if !exs {
// 		return errors.Errorf("Not found ID=%v", id)
// 	}
// 	return nil
// }

// // BeforeUpdate godoc
// // TODO: вынести в общие методы и сделать его универсальным
// func (us *UsersCRM) BeforeUpdate(db orm.DB) error {
// 	us.UpdatedAt = time.Nus()
// 	return nil
// }

// SetDeleted godoc
func (us *UsersCRM) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var user UsersCRM
	err := CheckExistsUUID(&user, us.UUID)
	if err != nil {
		return err
	}
	us.Deleted = true
	_, err = db.Model(us).Where("uuid = ?uuid").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}

// UsersByRegion godoc
func UsersByRegion(uuid string) ([]UsersCRM, error) {
	var users []UsersCRM
	err := db.Model(&users).
		Where("region_uuid = ? AND deleted is not true", uuid).
		Select()
	if err != nil {
		return users, err
	}
	return users, nil
}
