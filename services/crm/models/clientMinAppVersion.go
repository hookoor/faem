package models

import "gitlab.com/faemproject/backend/faem/pkg/structures"

// IsClientAppActual - и так понятно
func IsClientAppActual(clApp *structures.AppVersion) (bool, error) {
	minVer, err := getMinVersionFromDB(structures.MinVerClientApp, structures.DBConfigKey(clApp.OS))
	if err != nil {
		return false, err
	}

	return isVersionActual(minVer, clApp.Version)
}
