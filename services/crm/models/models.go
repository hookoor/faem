package models

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/crypto"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/pkg/structures/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
)

var (
	db              *pg.DB
	dbQueryHook     *postgre.DBQueryTraceHook
	gisCrmZonesData gis.Gis
	osrmka          *osrm.OSRM

	// GodModeInst -
	GodModeInst structures.GodMode
)

func init() {
	GodModeInst.Init()
}

// ormStruct - старая версия, вместо неё используйте ormStructV2
type ormStruct struct {
	ID        int       `json:"-" sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"-" description:"Дата создания"`
	// UpdatedAt time.Time `json:"updated_at" `
	UpdatedAt time.Time `json:"-" `
	Deleted   bool      `json:"-" sql:"default:false"`
}

// ormStructV2 - Использовать для новых сущностей только это
type ormStructV2 struct {
	CreatedAt time.Time `json:"created_at" sql:"default:now()"`
	UpdatedAt time.Time `json:"updated_at"`
	DeletedAt time.Time `json:"-" pg:",soft_delete"`
}

// AllStates структура для вывода стейтов водителей и ордеров
type AllStates struct {
	DriverStates         []StatesForFront `json:"driver_states"`
	DriverStatesEditable []StatesForFront `json:"driver_states_editable"`
	OrderStates          []StatesForFront `json:"order_states"`
}

// StatesForFront godoc
type StatesForFront struct {
	StateName  string `json:"state_name"`
	StateTitle string `json:"state_title"`
}

// ReasonsForFront причины для отмены заказа
type ReasonsForFront struct {
	ReasonName  string `json:"reason_name"`
	ReasonTitle string `json:"reason_title"`
}

// OrderSourceForFront - источники заказа
type OrderSourceForFront struct {
	Name  string `json:"name"`
	Title string `json:"title"`
}

type TaxiParkForFront struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type ServiceForFront struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type RegionForFront struct {
	ID        int                `json:"id"`
	Name      string             `json:"name"`
	Services  []ServiceForFront  `json:"services"`
	TaxiParks []TaxiParkForFront `json:"taxi_parks"`
}

// Options godoc
type Options struct {
	Services         []ServiceCRM          `json:"services"`
	Features         []FeatureCRM          `json:"features"`
	SourcesOfOrders  []SourceOfOrdersCRM   `json:"sources"`
	States           AllStates             `json:"states"`
	ReasonsForCancel []ReasonsForFront     `json:"reasons_for_cancel"`
	OrderSources     []OrderSourceForFront `json:"order_sources"`
	DriverGroups     []ShortDriverGroup    `json:"driver_groups"`
	Regions          []RegionForFront      `json:"regions"`
}

// ShortDriverGroup - краткая инфа о группе для фильтра
type ShortDriverGroup struct {
	UUID string `json:"uuid"`
	Name string `json:"name" sql:"name"`
}

// ConnectDB initialize connection to package var
func ConnectDB(conn *pg.DB) *postgre.DBQueryTraceHook {
	db = conn
	var err error
	gisCrmZonesData, err = gis.New(db, CrmZone{}, "area")
	if err != nil {
		logrus.Errorf("(ConnectDB)error init crm zones data")
	}
	osrmka = osrm.New(config.St.OSRM.Host)

	dbHook := postgre.InitDebugSQLQueryHook(conn)
	dbQueryHook = dbHook

	initialization()

	return dbHook
}

func initialization() {
	_, err := GetGodModeParamsByTicker()
	if err != nil {
		fmt.Printf("error init god mode params %s", errpath.Err(err))
	}
}

// GetOSRMInstance -
func GetOSRMInstance() *osrm.OSRM { return osrmka }

// GetDBQueryHook -
func GetDBQueryHook() *postgre.DBQueryTraceHook { return dbQueryHook }

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// CloudStorage -
type CloudStorage interface {
	UploadImage(ctx context.Context, upload *cloudstorage.UploadImage) (string, error)

	DifragPic(ctx context.Context, originPictureName string, reassignPictures []cloudstorage.ReassignPicture, prefix string) error
}

// Server smpp connection instance
type Server struct {
	CloudStorage CloudStorage
	DB           DB
}

// DB -
type DB struct {
	RegiosModel                        IRegion
	DebugModel                         IDebug
	GodModeModel                       IGodMode
	SetToAddressModel                  ISetToAddress
	CounterOrderSwitch                 ICounterOrderSwitch
	FeaturesSetForRegionModel          IFeaturesSetForRegion
	FeaturesSetForRegionalServiceModel IFeaturesSetForService
	DriverGroupModel                   IDriverGroup
	DriverInteraction                  *DriverInteraction
}

// NewServer Sender New constructor
func NewServer(cloudStorage CloudStorage, DBInterface DB) *Server {
	return &Server{
		CloudStorage: cloudStorage,
		DB:           DBInterface,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// CheckExistsID return error if ID in mdl model not found
func CheckExistsID(mdl interface{}, id int) error {
	exs, _ := db.Model(mdl).Column("id").Where("id = ? AND deleted is not true", id).Exists()

	if !exs {
		return errors.Errorf("Запись ID=%v не найдена или удалена", id)
	}
	return nil
}

// CheckExistsUUID return error if UUID in mdl model not found
func CheckExistsUUID(mdl interface{}, uuid string) error {
	query := db.Model(mdl).Column("uuid").Where("uuid = ? AND deleted is not true", uuid)
	if ok, err := query.Exists(); err != nil {
		return err
	} else if !ok {
		return errors.Errorf("Запись UUID=%v не найдена или удалена", uuid)
	}
	return nil
}

// CheckExistsUUIDWithError godoc
// TODO: перевести все с CheckExistsUUID на CheckExistsUUIDWithError
func CheckExistsUUIDWithError(mdl interface{}, uuid string) (bool, error) {
	exs, err := db.Model(mdl).Column("uuid").Where("uuid = ? AND deleted is not true", uuid).Exists()
	return exs, err
}

// UpdateByPK update model by PK
func UpdateByPK(mdl interface{}) error {
	_, err := db.Model(mdl).Where("uuid=?uuid").Returning("*").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}

// CreateByStruct - created sended struct
func CreateByStruct(mdl interface{}) error {
	_, err := db.Model(mdl).Returning("*").Insert()
	if err != nil {
		return err
	}
	return nil
}

// GetByUUID returning object by interface and UUID
func GetByUUID(uuid string, mdl interface{}) error {
	err := db.Model(mdl).
		Where("deleted is not true AND uuid = ?", uuid).
		Select()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// GetByID returning object by interface and ID
func GetByID(id int, mdl interface{}) error {
	err := db.Model(mdl).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return err
	}
	return nil
}

func CurrentTime() time.Time {
	return localtime.TimeInZone(time.Now(), config.St.Application.TimeZone)
}
func ContainsProdCategory(arr []structures.ProductCategory, str structures.ProductCategory) bool {
	for _, val := range arr {
		if val == str {
			return true
		}
	}
	return false
}
func Contains(arr []string, str string) bool {
	for _, val := range arr {
		if val == str {
			return true
		}
	}
	return false
}
func CurrentTimeUnix() int64 {
	return localtime.TimeInZone(time.Now(), config.St.Application.TimeZone).Unix()
}

// // BeforeUpdate godoc
// func (os *ormStruct) BeforeUpdate(db orm.DB) error {
// 	os.UpdatedAt = time.Now()
// 	return nil
// }

// SetDeleted godoc
// func SetDeleted(mdl interface{}, id int) error {
// 	// err := os.CheckExists()
// 	// if err != nil {
// 	// 	return err
// 	// }
// 	mdl.Deleted = true
// 	_, err := db.Model(mdl).WherePK().UpdateNotNull()
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

func Request(ctx context.Context, method, url string, payload, response interface{}, basicAuth ...string) error {
	r := &http.Client{
		Timeout: 15 * time.Second,
	}
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}
	req = req.WithContext(ctx)
	req.Header.Set("Content-Type", "application/json")
	if len(basicAuth) > 1 {
		req.SetBasicAuth(basicAuth[0], basicAuth[1])
	}

	resp, err := r.Do(req)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("failed to make a %s request", method))
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("%s request to %s failed with status: %d", method, url, resp.StatusCode)
		}
		return errors.Errorf("%s request to %s failed with status: %d and body: %s", method, url, resp.StatusCode, string(body))
	}
	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}

func uuidHash(uuid string) int {
	return crypto.FNV(uuid)
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
