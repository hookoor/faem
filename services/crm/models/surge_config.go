package models

import (
	"context"
	"encoding/json"
	"time"
)

const (
	SurgeConfigKey        = "surge"
	DefaultSurgeInterval  = time.Minute * 5
	DefaultSurgeZoneSize  = 2000. // in meters
	DefaultSurgeOutOfTown = false
)

type SurgeConfig struct {
	Interval time.Duration `json:"interval"`  // stored in seconds
	ZoneSize float64       `json:"zone_size"` // in meters
}

// Returns SurgeConfig filled from map or filled by default values
func NewSurgeConfig(values map[string]interface{}) *SurgeConfig {
	if values == nil {
		values = make(map[string]interface{})
	}

	interval := DefaultSurgeInterval
	if iInterval, found := values["interval"]; found {
		if val, casted := iInterval.(float64); casted {
			interval = time.Second * time.Duration(val)
		}
	}

	zoneSize := DefaultSurgeZoneSize
	if iZoneSize, found := values["zone_size"]; found {
		if val, casted := iZoneSize.(float64); casted {
			zoneSize = val
		}
	}

	return &SurgeConfig{
		Interval: interval,
		ZoneSize: zoneSize,
	}
}

func UpdateSurgeConfig(ctx context.Context, config SurgeConfig) error {
	var invalues map[string]interface{}
	b, err := json.Marshal(config)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &invalues)
	if err != nil {
		return err
	}
	_, err = db.ModelContext(ctx, (*RemoteConfig)(nil)).Where("key = ?", "surge").Set("value = ?", invalues).Update()
	if err != nil {
		return err
	}
	return nil
}
