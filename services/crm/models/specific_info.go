package models

import (
	"context"
)

type (
	SpecificInfo struct {
		tableName          struct{} `sql:"crm_specific_info"`
		SourceID           string   `json:"-"`
		CompanyLogo        string   `json:"company_logo" `
		CompanyName        string   `json:"company_name" `
		UserAgreement      string   `json:"user_agreement" `
		CompanyColor       string   `json:"company_color" `
		InsuranceInfo      string   `json:"insurance_info" `
		ConfidentialPolicy string   `json:"confidential_policy" `
		Tarifs             string   `json:"tarifs" `
	}
)

func (s *SpecificInfo) Get(c context.Context) error {
	err := db.ModelContext(c, s).Where("source_id = ?", s.SourceID).Select()
	return err
}

func (s *SpecificInfo) Update(c context.Context) error {
	_, err := db.ModelContext(c, s).Where("source_id = ?", s.SourceID).UpdateNotNull()
	return err
}
func (s *SpecificInfo) Insert(c context.Context) error {
	_, err := db.ModelContext(c, s).Insert()
	return err
}
func (s *SpecificInfo) Delete(c context.Context) error {
	_, err := db.ModelContext(c, s).Where("source_id = ?", s.SourceID).Delete()
	return err
}
