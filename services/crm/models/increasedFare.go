package models

import (
	"time"
)

type (
	//IncreasedFare steps for increased fare for client
	IncreasedFare struct {
		tableName struct{}                  `sql:"crm_increased_fare"`
		ID        int                       `sql:"id" json:"-"`
		Steps     []stepsPercentWithComment `json:"steps_with_comment"`
		CreatedAt time.Time                 `json:"-"`
	}

	stepsPercentWithComment struct {
		StepPercent int    `json:"step_percent"`
		Comment     string `json:"step_comment"`
	}
)

// GetActualIncreasedFareSteps returns current steps for increased fare
func GetActualIncreasedFareSteps() (IncreasedFare, error) {
	var (
		lastSteps IncreasedFare
	)
	err := db.Model(&lastSteps).
		Order("created_at desc").
		Limit(1).
		Select()
	return lastSteps, err
}

// CreateActualIncreasedFareSteps updates current steps for increased fare
func (steps IncreasedFare) CreateActualIncreasedFareSteps() (IncreasedFare, error) {
	_, err := db.Model(&steps).
		Insert()
	return steps, err
}
