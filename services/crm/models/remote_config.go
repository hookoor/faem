package models

import (
	"context"
	"encoding/json"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type RemoteConfig struct {
	tableName struct{} `sql:"crm_config" pg:",discard_unknown_columns"`

	ID        string                 `json:"id"`
	Key       string                 `json:"key"`
	Value     map[string]interface{} `json:"value"`
	CreatedAt time.Time              `json:"-"`
	UpdatedAt time.Time              `json:"-"`
}

func LoadRemoteConfig(ctx context.Context, key string) (*RemoteConfig, error) {
	var result RemoteConfig
	err := db.ModelContext(ctx, &result).
		Where("key = ?", key).
		Limit(1).
		Select()
	if err != nil {
		return nil, errors.Wrapf(err, "failed to select config by key: %s", key)
	}
	return &result, nil
}

func (c *RemoteConfig) GetValue() map[string]interface{} {
	if c == nil || c.Value == nil {
		return make(map[string]interface{})
	}
	return c.Value
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

const (
	ActivityRateConfigKey = "activity"
	BlockingConfigKey     = "blocking"
	ActivityConfigKey     = "activity_values"
)

func FetchActivityRate(ctx context.Context) (*structures.ActivityPriceItem, error) {
	remoteConfig, err := LoadRemoteConfig(ctx, ActivityRateConfigKey)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load remote activity config")
	}

	result := structures.NewActivityPriceItem(remoteConfig.GetValue())
	return result, nil
}

func FetchBlockingConfig(ctx context.Context) (*structures.BlockingConfig, error) {
	remoteConfig, err := LoadRemoteConfig(ctx, BlockingConfigKey)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load remote blocking config")
	}

	result := structures.NewBlockingConfig(remoteConfig.GetValue())
	return result, nil
}
func FetchActivityConfig(ctx context.Context) (*structures.ActivityConfig, error) {
	remoteConfig, err := LoadRemoteConfig(ctx, ActivityConfigKey)
	if err != nil {
		return nil, errors.Wrap(err, "failed to load remote activity config")
	}

	data, _ := json.Marshal(remoteConfig.GetValue())
	var result structures.ActivityConfig
	err = json.Unmarshal(data, &result)
	if err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal remote config")
	}

	return &result, nil
}
