package models

import (
	"time"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// DriversNews - новость для водителя
type DriversNews struct {
	UUID          string   `json:"uuid" sql:",pk"`
	Title         string   `json:"title"`
	News          string   `json:"news"`
	TaxiParksUUID []string `json:"taxi_parks_uuid"  sql:",type:text[]"`
	RegionIDs     []int    `json:"region_ids"  sql:"region_ids,type:integer[]"`
}

// DriversNewsCRM - сущность: новость для водителя
type DriversNewsCRM struct {
	tableName struct{} `sql:"crm_driver_news"`
	ormStructV2
	DriversNews
}

// Create - создает и сохраняет новость в базу
func (dn *DriversNewsCRM) Create() error {
	if err := dn.validCreation(); err != nil {
		return errors.Wrap(err, "data validation error")
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return err
	}
	dn.UUID = uu.String()

	// Если не указан таксопарк - вписываем все таксопарки
	if len(dn.TaxiParksUUID) == 0 {
		query := db.Model((*TaxiParkCRM)(nil)).Column("uuid").
			Where("deleted IS NOT true").
			WhereIn("region_id IN (?)", dn.RegionIDs)
		if query.Select(&dn.TaxiParksUUID); err != nil {
			return err
		}
	}

	if _, err = db.Model(dn).Returning("*").Insert(); err != nil {
		return err
	}

	return nil
}

type DriverNewsFilter struct {
	TaxiParkUUIDs []string  `query:"taxi_parks_uuid"`
	RegionIDs     []int     `query:"region_ids"`
	Title         string    `query:"title"`
	News          string    `query:"news"`
	MinDate       time.Time `query:"min_date"`
	MaxDate       time.Time `query:"max_date"`
}

// DriversNewsList - возвращает все новости по разрешённым таксопаркам
func (flt DriverNewsFilter) DriversNewsList() ([]DriversNewsCRM, error) {
	dn := make([]DriversNewsCRM, 0)
	query := db.Model(&dn).
		Where("taxi_parks_uuid && (?)", pg.Array(flt.TaxiParkUUIDs)).
		Where("title ILIKE ?", "%"+flt.Title+"%").
		Where("news ILIKE ?", "%"+flt.News+"%")

	if len(flt.RegionIDs) != 0 {
		query.Where("region_ids && (?)", pg.Array(flt.RegionIDs))
	}
	if !flt.MinDate.IsZero() {
		query.Where("created_at >= ?", flt.MinDate)
	}
	if !flt.MaxDate.IsZero() {
		query.Where("created_at < ?", flt.MaxDate)
	}

	if err := query.Order("created_at DESC").Select(); err != nil {
		return nil, err
	}

	return dn, nil
}

// DriversNewsList - возвращает все новости по разрешённым таксопаркам
func DriversNewByUUID(uuid string) (*DriversNewsCRM, error) {
	dn := new(DriversNewsCRM)
	dn.UUID = uuid

	if err := db.Model(dn).WherePK().Select(); err != nil {
		return nil, err
	}

	return dn, nil
}

// Update - обновляет и сохраняет новость в базу
func (dn *DriversNewsCRM) Update() error {
	if err := dn.validUpdate(); err != nil {
		return errors.Wrap(err, "data validation error")
	}

	query := db.Model(dn).WherePK().Returning("*")
	if res, err := query.UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("drivers news with uuid %s was not found", dn.UUID)
	}

	return nil
}

// SetDeleted - помечает новость как удаленную
func (dn *DriversNewsCRM) SetDeleted() error {
	if res, err := db.Model(dn).WherePK().Delete(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("driver news with uuid %s was not found", dn.UUID)
	}

	return nil
}

// heap of functions =================

func (dn *DriversNewsCRM) validCreation() error {
	if dn.Title == "" {
		return errors.Errorf("Driver news title is empty")
	}
	if dn.News == "" {
		return errors.Errorf("Driver news is empty")
	}
	if len(dn.RegionIDs) == 0 {
		return errors.Errorf("Указывать регионы обязательно")
	}

	return nil
}

func (dn *DriversNewsCRM) validUpdate() error {
	if dn.Title == "" {
		return errors.Errorf("Driver news title is empty")
	}
	if dn.News == "" {
		return errors.Errorf("Driver news is empty")
	}
	dn.UpdatedAt = time.Now()

	return nil
}
