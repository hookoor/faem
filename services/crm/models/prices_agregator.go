package models

import (
	"context"
	"time"

	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
)

type (
	AgregatedPrices struct {
		tableName  struct{}  `sql:"crm_prices_agregator"`
		UUID       string    `json:"uuid"`
		AddressA   string    `json:"address_a" sql:"address_a"`
		AddressB   string    `json:"address_b" sql:"address_b"`
		FaemClient int       `json:"faem_client"`
		Length     int       `json:"length"`
		Faem       int       `json:"faem"`
		Maxim      int       `json:"maxim"`
		Yandex     int       `json:"yandex"`
		Deleted    bool      `json:"-"`
		CreatedAt  time.Time `json:"created_at"`
	}
	AgregatedPricesCriteria struct {
		MinDate time.Time       `json:"min_date"`
		MaxDate time.Time       `json:"max_date"`
		Pager   urlvalues.Pager `json:"-"`
	}
)

func GetAgregatedPrices(ctx context.Context) ([]AgregatedPrices, error) {
	var prices []AgregatedPrices
	err := db.ModelContext(ctx, &prices).
		Where("deleted is not true").
		Order("created_at DESC").
		Select()
	return prices, err
}

func (cr AgregatedPricesCriteria) GetAgregatedPricesByFilter(ctx context.Context) ([]AgregatedPrices, error) {

	var (
		prices []AgregatedPrices
		err    error
	)

	query := db.ModelContext(ctx, &prices)

	if !cr.MinDate.IsZero() {
		query = query.
			Where("created_at >= ?", cr.MinDate)
	}
	if !cr.MaxDate.IsZero() {
		query = query.
			Where("created_at <= ?", cr.MaxDate)
	}
	err = query.
		Order("created_at desc").
		Where("deleted is not true").
		Apply(cr.Pager.Pagination).
		Select()
	if err != nil {
		return prices, errors.Errorf("error finding agregated prices,%s", err)
	}
	return prices, err
}

func (p AgregatedPrices) SetAgregatedPriceDeleted() error {
	_, err := db.Model(&p).
		Where("uuid = ?", p.UUID).
		Set("deleted = ?", true).
		UpdateNotNull()
	return err
}
func InsertAgregatedPrices(ctx context.Context, prices []AgregatedPrices) error {
	_, err := db.ModelContext(ctx, &prices).Insert()
	return err
}
