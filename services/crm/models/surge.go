package models

import (
	"context"
	"fmt"
	"github.com/go-pg/pg/orm"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/services/crm/surge"
	"sort"
	"time"
)

// Models

type SurgeZoneCRM struct {
	tableName struct{} `sql:"crm_surge_zones"`

	UUID      string       `json:"uuid"`
	Polygon   []gis.Vertex `json:"polygon"`
	CreatedAt time.Time    `json:"-"`
}

type SurgeHistoryItemCRM struct {
	tableName struct{} `sql:"crm_surge_history"`

	UUID        string `json:"uuid"`
	ZoneUUID    string `json:"zone_uuid"`
	ServiceUUID string `json:"service_uuid"`
	// TODO: заменить интерфейс на structures.SurgeData(V2)
	SurgeData     interface{} `json:"surge_data"`
	TimestampUUID string      `json:"timestamp_uuid"`
	CreatedAt     time.Time   `json:"-"`
}

type SurgeTimestampCRM struct {
	tableName struct{} `sql:"crm_surge_timestamps"`

	UUID      string    `json:"uuid"`
	CreatedAt time.Time `json:"created_at"`
}

// Filters

type SurgeZoneFilter struct {
	ZonesUUIDs []string `json:"zones_uuids" query:"uuid"`
}

type SurgeTimestampFilter struct {
	PeriodStart time.Time `json:"period_start"`
	PeriodEnd   time.Time `json:"period_end"`
}

type SurgeHistoryFilter struct {
	ZoneUUID        string   `json:"zone_uuid,omitempty"      query:"zone_uuid"`
	TimestampsUUIDs []string `json:"timestamp_uuid,omitempty" query:"timestamp_uuid"`
	ServiceUUID     string   `json:"service_uuid,omitempty"   query:"service_uuid"`
}

// Functions

func GetSurgeZones(ctx context.Context, filter SurgeZoneFilter) ([]SurgeZoneCRM, error) {
	var zones []SurgeZoneCRM
	err := db.ModelContext(ctx, &zones).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if len(filter.ZonesUUIDs) > 0 {
				q.WhereIn("uuid IN (?)", filter.ZonesUUIDs)
			}
			return q, nil
		}).
		Select()
	if err != nil {
		return nil, err
	}

	return zones, nil
}

func GetSurgeTimestamps(ctx context.Context, filter SurgeTimestampFilter) ([]SurgeTimestampCRM, error) {
	var timestamps []SurgeTimestampCRM

	err := db.ModelContext(ctx, &timestamps).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if !filter.PeriodStart.IsZero() {
				q.Where("created_at >= ?", filter.PeriodStart)
			}
			if !filter.PeriodEnd.IsZero() {
				q.Where("created_at <= ?", filter.PeriodEnd)
			}
			return q, nil
		}).
		Select()
	if err != nil {
		return nil, err
	}

	return timestamps, nil
}

func GetSurgeZonesHistory(ctx context.Context, filter SurgeHistoryFilter) ([]SurgeHistoryItemCRM, error) {
	var surgeHistoryItems []SurgeHistoryItemCRM

	err := db.ModelContext(ctx, &surgeHistoryItems).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.ZoneUUID != "" {
				q.Where("zone_uuid = ?", filter.ZoneUUID)
			}
			if len(filter.TimestampsUUIDs) > 0 {
				q.WhereIn("timestamp_uuid in (?)", filter.TimestampsUUIDs)
			}
			if filter.ServiceUUID != "" {
				q.Where("service_uuid = ?", filter.ServiceUUID)
			}
			return q, nil
		}).
		Select()

	if err != nil {
		return []SurgeHistoryItemCRM{}, err
	}

	return surgeHistoryItems, nil
}

func GetLastSurgeTimestamp(ctx context.Context) (SurgeTimestampCRM, error) {
	var timestamp []SurgeTimestampCRM
	err := db.ModelContext(ctx, &timestamp).
		Order("created_at DESC").
		Limit(1).
		Select()
	if err != nil {
		return SurgeTimestampCRM{}, err
	}

	if len(timestamp) == 0 {
		return SurgeTimestampCRM{}, errors.New("no timestamps in database")
	}

	return timestamp[0], nil
}

// -------------------------------------------------------------------
// -------------------------------------------------------------------
// -------------------------------------------------------------------
type SurgeRuleCRM struct {
	tableName struct{} `sql:"crm_surge_rules"`

	ID   int    `json:"id"`
	Name string `json:"name"`
	surge.Rule

	CreatedAt time.Time `json:"-"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt time.Time `json:"-" pg:",soft_delete"`
}

func GetSurgeRulesMap(ctx context.Context) (surge.RulesMap, error) {
	var ruleReference []struct {
		RegionID    int
		ServiceUUID string
		SurgeRuleID int
	}
	query := db.ModelContext(ctx, (*RegionCRM)(nil)).
		Join("JOIN crm_set_to_service sts ON services_set_id = sts.set_id").
		Join("JOIN crm_services s ON s.uuid = sts.service_uuid").
		ColumnExpr("region_crm.id AS region_id").
		ColumnExpr("s.uuid AS service_uuid").
		Column("surge_rule_id")
	if err := query.Select(&ruleReference); err != nil {
		return nil, err
	}

	neededRuleIDs := make([]int, 0)
outer:
	for _, ref := range ruleReference {
		for _, id := range neededRuleIDs {
			if id == ref.SurgeRuleID { // такой id уже записан
				continue outer
			}
		}

		neededRuleIDs = append(neededRuleIDs, ref.SurgeRuleID)
	}

	rules, err := GetSurgeRules(ctx, FilterIn("id", neededRuleIDs))
	if err != nil {
		return nil, err
	}

	result := make(surge.RulesMap)
	for _, ref := range ruleReference {
		var rule surge.Rule
		for _, r := range rules {
			if ref.SurgeRuleID == r.ID {
				rule = r.Rule
				break
			}
		}

		result.SetRule(ref.RegionID, ref.ServiceUUID, rule)
	}

	return result, nil
}

type SQLQueryOption func(query *orm.Query)

func FilterIn(column string, seq interface{}) SQLQueryOption {
	return func(q *orm.Query) {
		q.WhereIn(fmt.Sprintf("%s IN (?)", column), seq)
	}
}

func FilterEquals(column string, val interface{}) SQLQueryOption {
	return func(q *orm.Query) {
		q.Where(fmt.Sprintf("%s = ?", column), val)
	}
}

func FilterLike(column string, val string) SQLQueryOption {
	return func(q *orm.Query) {
		q.Where(fmt.Sprintf("%s ILIKE ?", column), val)
	}
}

func FilterSubLike(column string, val string) SQLQueryOption {
	return FilterLike(column, "%"+val+"%")
}

func GetSurgeRules(ctx context.Context, options ...SQLQueryOption) ([]SurgeRuleCRM, error) {
	var rules []SurgeRuleCRM
	query := db.ModelContext(ctx, &rules)
	for _, op := range options {
		op(query)
	}
	if err := query.Select(); err != nil {
		return nil, err
	}

	// сортируем все таблицы для надежности
	for x := range rules {
		table := rules[x].SurgeTable
		sort.Slice(table, func(i, j int) bool {
			return table[i].Delta < table[j].Delta
		})
	}

	return rules, nil
}

func CreateOrUpdateSurgeRule(ctx context.Context, rule SurgeRuleCRM) (SurgeRuleCRM, error) {
	_, err := db.ModelContext(ctx, &rule).
		OnConflict("(id) DO UPDATE").
		Set("name = EXCLUDED.name").
		Set("surge_table = EXCLUDED.surge_table").
		Set("allow_out_of_town = EXCLUDED.allow_out_of_town").
		Set("updated_at = now()").
		Returning("*").
		Insert()

	return rule, err
}

func DeleteSurgeRule(ctx context.Context, id int) error {
	rule := SurgeRuleCRM{ID: id}
	query := db.ModelContext(ctx, &rule).WherePK()
	if res, err := query.Delete(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.New("surge rule doesn't exist")
	}

	return nil
}
