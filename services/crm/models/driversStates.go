package models

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// DriverStatesCRM setting driver apps status
type DriverStatesCRM struct {
	tableName struct{} `sql:"crm_driver_states"`
	ID        int      `json:"id" sql:",pk"`
	structures.DriverStates
}

// SaveDriverState - save structure to database
func SaveDriverState(newState structures.DriverStates) (DriverStatesCRM, error) {
	var (
		dbState DriverStatesCRM
		driver  DriverCRM
	)

	// if newState.State == variables.DriverStateCreated || newState.State == variables.DriverStateOnModeration {
	// 	var driver DriverCRM
	// 	err := CheckExistsUUID(&driver, newState.DriverUUID)
	// 	if err != nil {
	// 		driver.UUID = newState.DriverUUID
	// 		driver.CreatedAt = time.Now()
	// 		_, err = db.Model(&driver).Returning("*").Insert()
	// 		if err != nil {
	// 			return DriverStatesCRM{}, errors.Errorf("Saving new driver error. %v ", err)
	// 		}
	// 	}
	// }

	dbState.DriverUUID = newState.DriverUUID
	dbState.State = newState.State
	dbState.Comment = newState.Comment
	dbState.CreatedAt = newState.CreatedAt
	_, err := db.Model(&dbState).Returning("*").Insert()
	if err != nil {
		return DriverStatesCRM{}, err
	}
	driver.UUID = newState.DriverUUID
	driver.Status = dbState.State

	_, err = db.Model(&driver).Set("status = ? , status_update_time = ?", driver.Status, time.Now()).Where("uuid = ?", newState.DriverUUID).Update()
	if err != nil {
		return dbState, errors.Errorf("error update driver.state %s", err)
	}
	dbState.State = newState.State

	return dbState, nil

}

// TODO: Сделать возможность операторам изменять статусы водителям. Не смотря что в документации
// указано что может быть много статусов https://gitlab.com/mojitoproject/mmt/wikis/home (внизу)
// администратор может перевести только в 2 режима (блокировки и разблокировки)
