package models

import (
	"context"

	"github.com/go-pg/pg/orm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

// RelationDriverService - связующая таблица. связывает водителя с активной для него услугой.
// db: имеет уникальность связки полей  UNIQUE(driver_uuid, service_uuid)
type RelationDriverService struct {
	tableName struct{} `sql:"crm_zzz_m2m_active_services_for_drivers,alias:asfd"`

	ID int `json:"id" sql:",pk"`

	DriverUUID  string `json:"driver_uuid"`
	ServiceUUID string `json:"service_uuid"`
}

// RelationDriverFeature - связующая таблица. связывает водителя с активной для него опциией
// db: имеет уникальность связки полей  UNIQUE(driver_uuid, feature_uuid)
type RelationDriverFeature struct {
	tableName struct{} `sql:"crm_zzz_m2m_active_features_for_drivers,alias:m2m_active_driver_featrue"`

	ID int `json:"id" sql:",pk"`

	DriverUUID  string `json:"driver_uuid"`
	FeatureUUID string `json:"feature_uuid"`
}

type IRelationDriverService interface {
	// SetRelation - создает связующую запись с учетом конфликтов
	SetRelation(ctx context.Context, driverUUID, serviceUUID string) (*RelationDriverService, error)
	// SetRelations - создает связующие записи с учетом конфликтов
	SetRelations(ctx context.Context, relations []RelationDriverService) ([]RelationDriverService, error)
	GetServicesByDriver(ctx context.Context, driverUUID string) ([]ServiceCRM, error)
	// GetDriversUUIDsByService -
	GetDriversUUIDsByServices(ctx context.Context, servicesUUIDs []string) ([]string, error)
	DeleteRelation(ctx context.Context, driverUUID, serviceUUID string) error
	DeleteAllRelationByDriver(ctx context.Context, driverUUID string) error
	DeleteRelationsByDrivers(ctx context.Context, driversUUIDs, servicesUUIDs []string) error
	// DeleteIfNotContains - удаляет все записи не содержащие значения из переданного параметра
	DeleteIfNotContains(ctx context.Context, driverUUID string, servicesUUIDs []string) error
}

type IRelationDriverFeature interface {
	// SetRelation - создает связующую запись с учетом конфликтов
	SetRelation(ctx context.Context, driverUUID, featureUUID string) (*RelationDriverFeature, error)
	// SetRelations - создает связующие записи с учетом конфликтов
	SetRelations(ctx context.Context, relations []RelationDriverFeature) ([]RelationDriverFeature, error)
	GetFeaturesByDriver(ctx context.Context, driverUUID string) ([]FeatureCRM, error)
	// GetDriversUUIDsByFeatures -
	GetDriversUUIDsByFeatures(ctx context.Context, featuresUUIDs []string) ([]string, error)
	DeleteRelation(ctx context.Context, driverUUID, featureUUID string) error
	DeleteAllRelationByDriver(ctx context.Context, driverUUID string) error
	DeleteRelationsByDrivers(ctx context.Context, driversUUIDs, featuresUUIDs []string) error
	// DeleteIfNotContains - удаляет все записи не содержащие значения из переданного параметра
	DeleteIfNotContains(ctx context.Context, driverUUID string, featuresUUIDs []string) error
}

type ModelRelationDriverService struct {
	db orm.DB
}
type ModelelationDriverFeature struct {
	db orm.DB
}

func NewModelRelationDriverService(db orm.DB) *ModelRelationDriverService {
	return &ModelRelationDriverService{
		db: db,
	}
}
func NewModelRelationDriverFeature(db orm.DB) *ModelelationDriverFeature {
	return &ModelelationDriverFeature{
		db: db,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (m *ModelRelationDriverService) SetRelation(ctx context.Context, driverUUID, serviceUUID string) (*RelationDriverService, error) {
	rec := &RelationDriverService{
		DriverUUID:  driverUUID,
		ServiceUUID: serviceUUID,
	}

	if _, err := m.db.ModelContext(ctx, rec).Returning("*").OnConflict("do nothing").Insert(); err != nil {
		return nil, errpath.Err(err)
	}

	return rec, nil
}

func (m *ModelRelationDriverService) SetRelations(ctx context.Context, relations []RelationDriverService) ([]RelationDriverService, error) {
	if _, err := m.db.ModelContext(ctx, &relations).Returning("*").OnConflict("do nothing").Insert(); err != nil {
		return nil, errpath.Err(err)
	}

	return relations, nil
}

func (m *ModelRelationDriverService) GetServicesByDriver(ctx context.Context, driverUUID string) ([]ServiceCRM, error) {
	query := m.db.ModelContext(ctx, (*RelationDriverService)(nil)).Column("srv.*").
		Join("inner join crm_services as srv ON srv.uuid = asfd.service_uuid").
		Where("driver_uuid = ?", driverUUID)

	var services []ServiceCRM
	if err := query.Select(&services); err != nil {
		return nil, errpath.Err(err)
	}

	return services, nil
}

func (m *ModelRelationDriverService) GetDriversUUIDsByServices(ctx context.Context, servicesUUIDs []string) ([]string, error) {
	var driversUUIDs []string
	err := m.db.ModelContext(ctx, (*RelationDriverService)(nil)).
		Column("driver_uuid").
		WhereIn("service_uuid IN (?)", servicesUUIDs).
		Select(&driversUUIDs)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return driversUUIDs, nil
}

func (m *ModelRelationDriverService) DeleteRelation(ctx context.Context, driverUUID, serviceUUID string) error {
	query := m.db.ModelContext(ctx, (*RelationDriverService)(nil)).Where("driver_uuid = ?", driverUUID).Where("service_uuid = ?", serviceUUID)
	if _, err := query.Delete(); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelRelationDriverService) DeleteAllRelationByDriver(ctx context.Context, driverUUID string) error {
	_, err := m.db.ModelContext(ctx, (*RelationDriverService)(nil)).Where("driver_uuid = ?", driverUUID).Delete()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelRelationDriverService) DeleteRelationsByDrivers(ctx context.Context, driversUUIDs, servicesUUIDs []string) error {
	query := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).
		WhereIn("driver_uuid in (?)", driversUUIDs).
		WhereIn("service_uuid in (?)", servicesUUIDs)
	if _, err := query.Delete(); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelRelationDriverService) DeleteIfNotContains(ctx context.Context, driverUUID string, servicesUUIDs []string) error {
	_, err := m.db.ModelContext(ctx, (*RelationDriverService)(nil)).
		Where("driver_uuid = ?", driverUUID).
		WhereIn("service_uuid not in (?)", servicesUUIDs).
		Delete()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (m *ModelelationDriverFeature) SetRelation(ctx context.Context, driverUUID, featureUUID string) (*RelationDriverFeature, error) {
	rec := &RelationDriverFeature{
		DriverUUID:  driverUUID,
		FeatureUUID: featureUUID,
	}

	if _, err := m.db.ModelContext(ctx, rec).Returning("*").OnConflict("do nothing").Insert(); err != nil {
		return nil, errpath.Err(err)
	}

	return rec, nil
}

func (m *ModelelationDriverFeature) SetRelations(ctx context.Context, relations []RelationDriverFeature) ([]RelationDriverFeature, error) {
	if _, err := m.db.ModelContext(ctx, &relations).Returning("*").OnConflict("do nothing").Insert(); err != nil {
		return nil, errpath.Err(err)
	}

	return relations, nil
}

func (m *ModelelationDriverFeature) GetFeaturesByDriver(ctx context.Context, driverUUID string) ([]FeatureCRM, error) {
	query := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).Column("ftr.*").
		Join("inner join crm_features as ftr ON ftr.uuid = m2m_active_driver_featrue.feature_uuid").
		Where("driver_uuid = ?", driverUUID)

	var features []FeatureCRM
	if err := query.Select(&features); err != nil {
		return nil, errpath.Err(err)
	}

	return features, nil
}

func (m *ModelelationDriverFeature) GetDriversUUIDsByFeatures(ctx context.Context, featuresUUIDs []string) ([]string, error) {
	var driversUUIDs []string
	err := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).
		Column("driver_uuid").
		WhereIn("feature_uuid IN (?)", featuresUUIDs).
		Select(&driversUUIDs)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return driversUUIDs, nil
}

func (m *ModelelationDriverFeature) DeleteRelation(ctx context.Context, driverUUID, featureUUID string) error {
	query := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).Where("driver_uuid = ?", driverUUID).Where("feature_uuid = ?", featureUUID)
	if _, err := query.Delete(); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelelationDriverFeature) DeleteAllRelationByDriver(ctx context.Context, driverUUID string) error {
	query := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).Where("driver_uuid = ?", driverUUID)
	if _, err := query.Delete(); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelelationDriverFeature) DeleteRelationsByDrivers(ctx context.Context, driversUUIDs, featuresUUIDs []string) error {
	query := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).
		WhereIn("driver_uuid in (?)", driversUUIDs).
		WhereIn("feature_uuid in (?)", featuresUUIDs)
	if _, err := query.Delete(); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func (m *ModelelationDriverFeature) DeleteIfNotContains(ctx context.Context, driverUUID string, featuresUUIDs []string) error {
	_, err := m.db.ModelContext(ctx, (*RelationDriverFeature)(nil)).
		Where("driver_uuid = ?", driverUUID).
		WhereIn("feature_uuid not in (?)", featuresUUIDs).
		Delete()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
