package models

import (
	"context"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// ServiceCRM - order service structure
type ServiceCRM struct {
	tableName struct{} `sql:"crm_services" pg:",discard_unknown_columns"`
	structures.Service
	ormStruct
	DisplayPriority       *int    `json:"display_priority"`
	ForOperatorOnly       bool    `json:"for_operator_only"`
	AvailabilityForDriver *bool   `json:"availability_for_driver,omitempty" sql:"-"`
	Level                 *int    `json:"level,omitempty"`
	NameForClients        *string `json:"name_for_clients"`
	Visibility            struct {
		ForOperator bool `json:"for_operator"`
		ForDriver   bool `json:"for_driver"`
		ForClient   bool `json:"for_client"`
	} `json:"visibility"`
	RequiredFeatures  []string `json:"required_features" sql:",type:text[]"`
	AvailableFeatures []string `json:"available_features" sql:",type:text[]"`
}

// Create - save structure to database
func (sr *ServiceCRM) Create() (*ServiceCRM, error) {
	nService, err := sr.validCreation()
	if err != nil {
		return nil, errors.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}

	nService.UUID = uu.String()

	err = db.RunInTransaction(func(tx *pg.Tx) error {
		// инсертим новую услугу
		query := tx.Model(&nService).Returning("*")
		if _, err := query.Insert(); err != nil {
			return err
		}

		// получаем ID всех наборов услуг
		var setIDs []int
		query = tx.Model((*ServicesSetCRM)(nil)).
			Column("id")
		if err := query.Select(&setIDs); err != nil {
			return err
		}

		// связываем новую услугу с существующими наборами
		setItems := make([]ServiceSetItem, len(setIDs))
		for i, id := range setIDs {
			setItems[i] = ServiceSetItem{
				SetID:       id,
				ServiceUUID: nService.UUID,
			}
		}
		query = tx.Model(&setItems)
		if _, err := query.Insert(); err != nil {
			return err
		}

		return nil
	})

	return &nService, err
}

func (sr *ServiceCRM) validCreation() (ServiceCRM, error) {
	var newService ServiceCRM
	if sr.Name == "" {
		return newService, errors.Errorf("Field name is required")
	}
	newService.RequiredFeatures = sr.RequiredFeatures
	newService.Name = sr.Name
	newService.PriceCoeff = sr.PriceCoeff
	newService.Freight = sr.Freight
	newService.NameForClients = sr.NameForClients
	newService.DisplayPriority = sr.DisplayPriority
	newService.Image = sr.Image
	newService.MaxBonusPaymentPercent = sr.MaxBonusPaymentPercent
	newService.ForOperatorOnly = sr.ForOperatorOnly
	newService.Comment = sr.Comment
	newService.Tag = sr.Tag
	return newService, nil
}

// ServiceByUUID - return Service structure based on ID
func ServiceByUUID(uuid string) (ServiceCRM, error) {
	var service ServiceCRM
	err := db.Model(&service).
		Where("deleted is not true AND uuid = ?", uuid).
		Select()
	return service, err
}

func ServicesByUUID(ctx context.Context, uuids ...string) ([]ServiceCRM, error) {
	if len(uuids) == 0 {
		return nil, nil
	}
	var services []ServiceCRM
	err := db.ModelContext(ctx, &services).
		Where("deleted is not true").
		WhereIn("uuid in (?)", uuids).
		Select()
	return services, err
}

// // GetDefaultService - return Service structure based on ID
// func GetDefaultService() (ServiceCRM, error) {
// 	var service ServiceCRM
// 	err := db.Model(&service).
// 		Where("deleted is not true AND \"default\" = true").
// 		Select()
// 	if err != nil {
// 		return service, err
// 	}
// 	return service, nil
// }

// ServicesList - return all Services if productDelivery is false
func ServicesList(productDelivery bool, servicesUUID ...string) ([]ServiceCRM, error) {
	var services []ServiceCRM
	query := db.Model(&services).
		Where("deleted is not true ").
		Order("display_priority desc")
	if productDelivery {
		query.Where("product_delivery = true")
	}
	if len(servicesUUID) != 0 {
		query.WhereIn("uuid in (?)", servicesUUID)
	}
	err := query.Select()
	if err != nil {
		return services, err
	}
	return services, nil
}

// FillAvailabilityForDriver -
func FillAvailabilityForDriver(services []ServiceCRM, availableServicesUUID []string) {
	for i := range services {
		if services[i].ForOperatorOnly {
			services[i].AvailabilityForDriver = boolPointer(false)
			continue
		}
		services[i].AvailabilityForDriver = boolPointer(true)

		for _, seruu := range availableServicesUUID {
			if services[i].UUID == seruu {
				services[i].AvailabilityForDriver = boolPointer(true)
				break
			}
		}
		// value = services[i].GetLevel() <= level
	}
}

// Фильтруем услуги для водителя на основании его группы
func FilterServicesForDriver(services []ServiceCRM, ServicesUUID []string) []ServiceCRM {
	var filteredServices []ServiceCRM
	for _, val := range services {
		for _, gval := range ServicesUUID {
			if val.UUID == gval {
				filteredServices = append(filteredServices, val)
			}
		}
	}
	return filteredServices
}

//Фильтруем опции на основании отмеченных услуг у водителя
func FilterFeaturesForDriver(services []structures.Service, features []FeatureCRM) []FeatureCRM {
	var filteredFeatures []FeatureCRM
	ifeatures := features
	for _, service := range services {
		for _, feature := range ifeatures {
			for _, featureid := range feature.ServicesUUID {
				if featureid == service.UUID {
					filteredFeatures = append(filteredFeatures, feature)
					deleteItem(ifeatures, feature.UUID)
				}
			}
		}
	}
	return filteredFeatures
}

// для внутреннего пользования, удаляет фичу из массива если совпали айдишники
func deleteItem(arr []FeatureCRM, id string) {
	// Remove the element at index i from a.
	for i, v := range arr {
		if v.UUID == id {
			arr[i] = arr[len(arr)-1]       // Copy last element to index i.
			arr[len(arr)-1] = FeatureCRM{} // Erase last element (write zero value).
			arr = arr[:len(arr)-1]         // Truncate slice.
			break
		}
	}
}

func boolPointer(neededVal bool) *bool {
	return &neededVal
}

// GetClientName godoc
func (sr *ServiceCRM) GetClientName() string {
	if sr == nil || sr.NameForClients == nil {
		return ""
	}
	return *sr.NameForClients
}

// GetLevel godoc
func (sr *ServiceCRM) GetLevel() int {
	if sr.Level == nil {
		return 0
	}
	return *sr.Level
}

// Update godoc
func (sr *ServiceCRM) Update(uuid string) (ServiceCRM, error) {

	uService, err := sr.validUpdate(uuid)
	if err != nil {
		return uService, errors.Errorf("Data validation error. %v", err)
	}

	err = UpdateByPK(&uService)
	if err != nil {
		return uService, errors.Errorf("DB error. %v", err)
	}
	return uService, nil
}

//
func (sr *ServiceCRM) validUpdate(uuid string) (ServiceCRM, error) {
	flag := false
	var service ServiceCRM

	if sr.Name != "" {
		service.Name = sr.Name
		flag = true
	}
	if sr.Freight != nil {
		service.Freight = sr.Freight
		flag = true
	}
	if sr.NameForClients != nil {
		service.NameForClients = sr.NameForClients
		flag = true
	}
	if sr.DisplayPriority != nil {
		service.DisplayPriority = sr.DisplayPriority
		flag = true
	}
	if sr.Image != "" {
		service.Image = sr.Image
		flag = true
	}
	if sr.MaxBonusPaymentPercent != nil {
		service.MaxBonusPaymentPercent = sr.MaxBonusPaymentPercent
		flag = true
	}
	if sr.Comment != "" {
		service.Comment = sr.Comment
		flag = true
	}
	if sr.RequiredFeatures != nil {
		service.RequiredFeatures = sr.RequiredFeatures
		flag = true
	}
	if sr.Level != nil {
		service.Level = sr.Level
		flag = true
	}
	if sr.PriceCoeff != 0 {
		service.PriceCoeff = sr.PriceCoeff
		flag = true
	}
	if !flag {
		return service, errors.Errorf("Required fields are empty")
	}
	service.UpdatedAt = time.Now()
	service.UUID = uuid
	err := CheckExistsUUID(&service, uuid)
	if err != nil {
		return service, err
	}
	service.Deleted = false
	service.ForOperatorOnly = sr.ForOperatorOnly
	service.Visibility = sr.Visibility
	return service, nil
}

// UpdateImage -
func (sr *ServiceCRM) UpdateImage(uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}
	sr.UUID = uuid

	_, err := db.Model(sr).
		Where("uuid = ?", uuid).
		Set("images_set = ?", sr.Image).
		Update()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

func GetStandardServices() ([]structures.Service, []string, error) {
	var services []ServiceCRM
	err := db.Model(&services).Where("level <= 0 or level is null").Select()
	var result []structures.Service
	var tags []string
	for _, ser := range services {
		result = append(result, ser.Service)
		tags = append(tags, ser.Tag...)
	}

	return result, tags, err
}

//SetProductDelivery задает service как доставку товаров
func SetProductDelivery(uuid string) error {
	_, err := db.Model(&ServiceCRM{}).Where("uuid != ?", uuid).Set("product_delivery = false").Update()
	if err != nil {
		return errors.Errorf("ошибка сброса текущего сервиса доставки товаров,%s", err)
	}
	_, err = db.Model(&ServiceCRM{}).Where("uuid = ?", uuid).Set("product_delivery = true").Update()
	if err != nil {
		return errors.Errorf("ошибка назначения нового сервиса доставки товаров,%s", err)
	}
	return nil
}

// SetDeleted godoc
func (sr *ServiceCRM) SetDeleted() error {
	service, err := ServiceByUUID(sr.UUID)
	if err != nil {
		return err
	}
	if service.ProductDelivery {
		return errors.Errorf("Сначала укажите другой сервис как доставку продуктов")
	}
	sr.Deleted = true

	return db.RunInTransaction(func(tx *pg.Tx) error {
		// удаляем услугу из всех наборов
		query := tx.Model((*ServiceSetItem)(nil)).
			Where("service_uuid = ?", sr.UUID)
		if _, err := query.Delete(); err != nil {
			return err
		}

		// помечаем услугу как удаленную
		query = tx.Model(sr).Where("uuid = ?uuid")
		if _, err := query.UpdateNotNull(); err != nil {
			return err
		}

		return nil
	})
}
