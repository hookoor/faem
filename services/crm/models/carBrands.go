package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

const (
	brand string = "brand"
	color string = "color"
)

// CarBrands -
type CarBrands struct {
	tableName struct{}          `sql:"crm_car_brand_reference"`
	ID        int               `json:"-" sql:",pk"`
	Brands    map[string]string `json:"brands"`
	Colors    map[string]string `json:"colors"`
	CreatedAt time.Time         `json:"-" sql:"default:now()"`
}

// GetList -
func (CarBrands) GetList() (CarBrands, error) {
	var carBrands CarBrands

	err := db.Model(&carBrands).
		Order("created_at DESC").
		Limit(1).
		Select()
	if err != nil {
		return carBrands, errpath.Err(err, "DB error")
	}

	return carBrands, nil
}

// Create -
func (cb CarBrands) Create() (CarBrands, error) {
	err := cb.validCreation()
	if err != nil {
		return cb, errpath.Err(err, "validate error")
	}

	_, err = db.Model(&cb).Insert()
	if err != nil {
		return cb, errpath.Err(err, "DB error")
	}
	return cb, nil
}

func (cb *CarBrands) validCreation() error {
	for key, val := range cb.Brands {
		if key == "" {
			return errpath.Errorf("empty key in brands")
		}
		if val == "" {
			return errpath.Errorf("empty value in brands")
		}
	}
	for key, val := range cb.Colors {
		if key == "" {
			return errpath.Errorf("empty key in colors")
		}
		if val == "" {
			return errpath.Errorf("empty value in colors")
		}
	}

	return nil
}
