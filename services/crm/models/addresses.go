package models

import (
	"fmt"
	"math"
	"sort"
	"strings"
	"unicode"

	"github.com/labstack/gommon/log"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/addresses"
)

const (
	maxAutocompleteCount        = 10
	MaxPriorityRouteName string = "Владикавказ" //горят твои огни в глазах у нас
)

// PermissibilityAddressesUpdate false if dowloading addresses in action and true if not
var PermissibilityAddressesUpdate bool

type DestinationPoint struct {
	tableName struct{} `sql:"crm_destination_points"`
	Deleted   bool     `json:"-"`
	structures.Route
}

// AllRoutesMap тут хранятся все роуты
type RouteMap map[rune][]structures.Route

// RegionRoutesMap - карта[название регина(название города)]роуты принадлежащие этому городу
type RegionRoutesMap map[string][]structures.Route

func (r RouteMap) Put(route structures.Route) {

	var firstRune rune
	if len(route.UnrestrictedValue) > 0 {
		firstRune = unicode.ToLower([]rune(route.UnrestrictedValue)[0])
	}
	if i, ok := r[firstRune]; ok {
		r[firstRune] = append(i, route)
		return
	}
	r[firstRune] = []structures.Route{route}
}

func (r RegionRoutesMap) Put(route structures.Route) {
	if i, ok := r[route.City]; ok {
		r[route.City] = append(i, route)
		return
	}
	r[route.City] = []structures.Route{route}
}

var (
	AllRoutesMap RouteMap
	AllRoutes    []structures.Route

	// AllRoutesReg - карта роутов [город]адресса
	AllRoutesReg RegionRoutesMap
)

//InitAddress инициализирует  загрузку адресов
func InitAddress() {
	err := DownloadAddress()
	if err != nil {
		log.Error(err)
	}
}

// DownloadAddress - загружает из базы адреса, улицы и публичные места
func DownloadAddress() error {
	PermissibilityAddressesUpdate = false
	defer func() {
		PermissibilityAddressesUpdate = true
	}()
	var points []DestinationPoint
	err := db.Model(&points).
		Where("deleted is not true").
		Select()
	if err != nil {
		return err
	}
	var routes []structures.Route
	allRoutesMapNew := make(RouteMap)
	allRegionRoutesMapNew := make(RegionRoutesMap)
	for _, point := range points {
		if point.Lat == 0 || point.Lon == 0 {
			continue
		}
		point.ValueForSearch = generateValueForSearch(point.UnrestrictedValue)
		routes = append(routes, point.Route)
		allRoutesMapNew.Put(point.Route)
		allRegionRoutesMapNew.Put(point.Route)
	}

	addresses.UpdateCache(routes)
	AllRoutes = filterByPriority(MaxPriorityRouteName, routes)

	AllRoutesMap = allRoutesMapNew
	AllRoutesReg = allRegionRoutesMapNew

	fmt.Println("Initialization of destination points successfuly")
	return err
}

func filterByPriority(cityPriority string, arr []structures.Route) []structures.Route {
	var sortResult, sortResultLowPriority []structures.Route
	for _, item := range arr {
		if item.PointType == structures.StreetType {
			// если это улица владикавказа
			if item.City == "" || item.City == cityPriority {
				sortResult = append(sortResult, item)
			} else {
				sortResultLowPriority = append(sortResultLowPriority, item)
			}
		}
	}
	for _, item := range arr {
		if item.PointType == structures.PublicPlaceType {
			if item.City == cityPriority {
				sortResult = append(sortResult, item)
			} else {
				sortResultLowPriority = append(sortResultLowPriority, item)
			}
		}
	}
	for _, item := range arr {
		if item.PointType == structures.CityType {
			sortResult = append(sortResult, item)
		}
	}
	for _, item := range arr {
		if item.PointType == structures.AddressType {
			// если это улица владикавказа
			if item.City == cityPriority {
				sortResult = append(sortResult, item)
			} else {
				sortResultLowPriority = append(sortResultLowPriority, item)
			}

		}
	}

	sortResult = append(sortResult, sortResultLowPriority...)
	return sortResult
}
func splitBySpaces(str string) []string {
	return strings.Fields(str)
}
func generateValueForSearch(str string) string {
	str = strings.ToLower(str)
	str = strings.Replace(str, ",", "", -1)
	str = strings.Replace(str, ".", "", -1)
	return str
}
func containsStringsSequentially(str string, strgsArr []string) (int, bool) {
	previousIndex := -1
	numberOfEntries := 0
	checkSequentially := true
	for _, item := range strgsArr {
		index := strings.Index(str, item)
		if index >= 0 {
			numberOfEntries++
		}
		if index <= previousIndex {
			checkSequentially = false
		}
		previousIndex = index
	}
	return numberOfEntries, checkSequentially
}

// GetMatchesByContains возвращает наиболее релевантные адреса для переданной строки по вхождению
// meta: для нормального поиска надо чтобы у строк(адресов) по которым идет поиск, писать город в начале
func GetMatchesByContains(str string, citysPriority []string) []structures.Route {
	type consNumAndSequentially struct {
		consNum      int
		sequentially bool
	}

	var (
		// отсртированные роуты в нужном порядке
		// порядок зависит от переданных приоритетных-городов по убыванию
		allRoutesDesiredSort []structures.Route

		result, hasPrefRoute                  []structures.Route
		zeroDisRoute                          structures.Route
		numberOfCoincidences, numberOfPrefixs int
		zeroDistanceFound                     bool
	)
	allRoutesDesiredSort = AllRoutes

	resMap := make(map[consNumAndSequentially][]structures.Route)
	allFound := func() bool {
		return numberOfCoincidences >= maxAutocompleteCount && zeroDistanceFound
	}
	resultIsReady := func() bool {
		return len(result) >= maxAutocompleteCount
	}
	resultAlreadyContains := func(routeUUID string) bool {
		for _, it := range result {
			if it.UUID == routeUUID {
				return true
			}
		}
		return false
	}
	appendToResultIfNeed := func(item ...structures.Route) {
		for _, r := range item {
			if !resultAlreadyContains(r.UUID) {
				result = append(result, r)
			}
		}
	}

	if len(citysPriority) != 0 {
		allRoutesDesiredSort = regCityFilter(allRoutesDesiredSort, citysPriority)
	}

	comparedString := formatInputString(str)
	splitedString := splitBySpaces(comparedString)
	for i := 0; i < len(allRoutesDesiredSort); i++ {

		// если сравниваемая строка равна адресу в базе, то она сразу записыватся в результат
		// и инкрементируетс счетчик имеющихся записей дабы затем отнять его от количества необходимых
		comparedAddressString := allRoutesDesiredSort[i].ValueForSearch
		if comparedString == comparedAddressString {
			zeroDistanceFound = true
			zeroDisRoute = allRoutesDesiredSort[i]
			numberOfCoincidences++
			continue
		}

		if numberOfPrefixs >= maxAutocompleteCount {
			continue
		}
		num, check := containsStringsSequentially(comparedAddressString, splitedString)
		if num > 0 {
			resMap[consNumAndSequentially{consNum: num, sequentially: check}] =
				append(resMap[consNumAndSequentially{consNum: num, sequentially: check}], allRoutesDesiredSort[i])
		}

		// если начальный кусок Бадреса полностью совпадает с введенной строкой
		// то Бадрес сразу попадает в результат
		if strings.HasPrefix(comparedAddressString, comparedString) {
			hasPrefRoute = append(hasPrefRoute, allRoutesDesiredSort[i])
			numberOfCoincidences++
			numberOfPrefixs++
			if allFound() {
				break
			}
			continue
		}
		// if strings.Index(comparedAddressString, comparedString) > 0 {
		// 	matches = append(matches, AllRoutes[i])
		// 	numberOfCoincidences++
		// }
		if allFound() {
			break
		}

	}

	if zeroDistanceFound {
		result = append(result, zeroDisRoute)
	}
	if len(hasPrefRoute) != 0 {
		for _, hproute := range hasPrefRoute {
			appendToResultIfNeed(hproute)
			if resultIsReady() {
				return result
			}
		}
	}

	for i := len(splitedString); i > 0; i-- {
		//последовательные совпадения
		rts, checkEx := resMap[consNumAndSequentially{
			consNum:      i,
			sequentially: true,
		}]

		if checkEx {
			for _, item := range rts {
				appendToResultIfNeed(item)
				if resultIsReady() {
					return result
				}
			}
		}
		//непоследовательные совпадения
		rts, checkEx = resMap[consNumAndSequentially{
			consNum:      i,
			sequentially: false,
		}]
		if checkEx {
			for _, item := range rts {
				appendToResultIfNeed(item)
				if resultIsReady() {
					return result
				}
			}
		}
	}

	if !resultIsReady() {
		matchesWithOneDistance := GetMatchesByMaxDistance(comparedString, 1, maxAutocompleteCount-numberOfCoincidences)
		appendToResultIfNeed(matchesWithOneDistance...)
	}
	return result
}

// regCityFilter - фильтрация прядка адресов по переданному городскому приоритету
func regCityFilter(allRoutes []structures.Route, citysPriority []string) []structures.Route {
	const unfoundKey string = "unfoundTail"
	var res = make([]structures.Route, 0, len(allRoutes))
	var cityDistrbutionMap = make(map[string][]structures.Route, len(citysPriority))

	if citysPriority != nil {
		for _, route := range allRoutes {
			var havecp bool
			for _, cp := range citysPriority {
				if route.City == cp {
					cityDistrbutionMap[cp] = append(cityDistrbutionMap[cp], route)
					havecp = true
					break
				}

			}
			if !havecp {
				cityDistrbutionMap[unfoundKey] = append(cityDistrbutionMap[unfoundKey], route)
			}
		}

		for _, cp := range citysPriority {
			var streets, pplace, tail []structures.Route
			for _, route := range cityDistrbutionMap[cp] {
				switch route.PointType {
				case structures.StreetType, structures.AddressType:
					streets = append(streets, route)
				case structures.PublicPlaceType:
					pplace = append(pplace, route)
				default:
					tail = append(tail, route)
				}
			}

			res = append(res, streets...)
			res = append(res, pplace...)
			res = append(res, tail...)
		}
		// добавление адресов не принадлежащих
		// указанным в желаемых городах для региона
		res = append(res, cityDistrbutionMap[unfoundKey]...)
	} else {
		return allRoutes
	}

	return res
}

// GetMatchesByLevenshtein возвращает наиболее релевантные адреса для переданной строки
func GetMatchesByLevenshtein(str string) []structures.Route {
	var (
		matches, hasPrefRoute []structures.Route
		numberOfCoincidences  int
		zeroDistanceFound     bool
	)
	comparedString := strings.ToLower(str)
	firstRune := []rune(comparedString)[0]
	distances := make(map[float64][]structures.Route)

	// Look for the routes with the same first rune as provided
	prefixedRoutes := AllRoutesMap[firstRune]
	for _, route := range prefixedRoutes {
		comparedAddressString := strings.ToLower(route.UnrestrictedValue)
		if comparedString == comparedAddressString {
			zeroDistanceFound = true
			numberOfCoincidences++
			matches = append(matches, route)
			continue
		}

		if strings.HasPrefix(comparedAddressString, comparedString) {
			hasPrefRoute = append(hasPrefRoute, route)
			numberOfCoincidences++
			if numberOfCoincidences >= maxAutocompleteCount && zeroDistanceFound {
				break
			}
			continue
		}

		// в coeff записывается отношение длины присылаемой и сравниваемой строки. Нужно для того чтобы длинные строки были более релевантными, вроде работает
		coeff := float64(len(comparedString)) / float64(len(comparedAddressString))
		distance := float64(LevenshteinDistance(comparedString, comparedAddressString))
		distance *= coeff
		distances[distance] = append(distances[distance], route)
	}

	// If we still don't have enough items to return, look for the rest routes
	if numberOfCoincidences < maxAutocompleteCount {
		for k, routes := range AllRoutesMap {
			if k == firstRune { // already seen above
				continue
			}
			for _, route := range routes {
				comparedAddressString := strings.ToLower(route.UnrestrictedValue)
				// в coeff записывается отношение длины присылаемой и сравниваемой строки. Нужно для того чтобы длинные строки были более релевантными, вроде работает
				coeff := float64(len(comparedString)) / float64(len(comparedAddressString))
				distance := float64(LevenshteinDistance(comparedString, comparedAddressString))
				distance *= coeff
				distances[distance] = append(distances[distance], route)
			}
		}
	}

	var distancesForSort []float64
	for i := range distances {
		distancesForSort = append(distancesForSort, i)
	}
	matches = append(matches, hasPrefRoute...)
	sort.Float64s(distancesForSort)

outer:
	for _, value := range distancesForSort {
		for _, value2 := range distances[value] {
			numberOfCoincidences++
			if numberOfCoincidences >= maxAutocompleteCount {
				break outer
			}
			matches = append(matches, value2)
		}
	}

	if numberOfCoincidences > maxAutocompleteCount {
		matches = matches[:maxAutocompleteCount]
	}
	return matches
}

func formatInputString(str string) string {
	str = strings.ToLower(str)
	str = strings.TrimPrefix(str, " ")
	str = strings.TrimSuffix(str, " ")
	str = strings.Replace(str, ",", "", -1)
	str = strings.Replace(str, ".", "", -1)

	return str
}

// GetMatchesByMaxDistance возвращает нужно кол-во  адресов для переданной строки, если расстояние не превыщат maxDis
func GetMatchesByMaxDistance(str string, maxDis, neededAddressesCount int) []structures.Route {
	var result []structures.Route
	firstRune := []rune(str)[0]
	for i := 0; i < len(AllRoutesMap[firstRune]); i++ {
		dis := LevenshteinDistance(str, AllRoutesMap[firstRune][i].ValueForSearch)
		if dis > maxDis || dis == 0 {
			continue
		}
		result = append(result, AllRoutesMap[firstRune][i])
		if len(result) >= neededAddressesCount {
			break
		}
	}
	return result
}

// Distance вычисление дистанции между строками (по Левенштейну)
func LevenshteinDistance(s, t string) int {
	min := func(a, b, c int) int {
		if a < b && a < c {
			return a
		} else if b < c {
			return b
		}
		return c
	}

	r1, r2 := []rune(s), []rune(t)
	column := make([]int, len(r1)+1)

	for y := 1; y <= len(r1); y++ {
		column[y] = y
	}

	for x := 1; x <= len(r2); x++ {
		column[0] = x

		for y, lastDiag := 1, x-1; y <= len(r1); y++ {
			oldDiag := column[y]
			cost := 0
			if r1[y-1] != r2[x-1] {
				cost = 1
			}
			column[y] = min(column[y]+1, column[y-1]+1, lastDiag+cost)
			lastDiag = oldDiag
		}
	}

	return column[len(r1)]
}

func GetDestinationPointsByUUID(uuids []string) ([]DestinationPoint, error) {
	var res []DestinationPoint
	if len(uuids) == 0 {
		return res, nil
	}
	err := db.Model(&res).
		WhereIn("uuid in (?)", uuids).
		Where("deleted is not true").
		Select()
	return res, err
}

// --------
// --------
// --------

// GetClientLocation -
// TODO: не работает при отрицательных значениях координат(нужна другая логика расчета)
func GetClientLocation(lat float64, long float64) (structures.Route, error) {
	if len(AllRoutes) == 0 {
		return structures.Route{}, errpath.Errorf("route list is empty")
	}

	var accuracy = 7
	var clientLocation structures.Route
	var posInd = -1
	var minLat = float64(AllRoutes[0].Lat)
	var minLong = float64(AllRoutes[0].Lon)
	minPos := tool.RoundTo(math.Abs(minLat-minLong), accuracy)
	for i, inst := range AllRoutes {
		instLat := float64(inst.Lat)
		rLat := tool.RoundTo(math.Abs(instLat-lat), accuracy)
		instLong := float64(inst.Lon)
		rLong := tool.RoundTo(math.Abs(instLong-long), accuracy)
		r := tool.RoundTo(math.Abs(rLat+rLong), accuracy)
		if r < minPos {
			minPos = r
			posInd = i
		}
	}

	if posInd != -1 {
		clientLocation = AllRoutes[posInd]
	}

	if posInd == -1 {
		return structures.Route{}, errpath.Errorf("route not found [lat=%v; long=%v]", lat, long)
	}
	return clientLocation, nil
}

// --------
// --------
// --------
