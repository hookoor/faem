package models

import (
	"sync"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type TaxiParkDependent interface {
	SetTaxiParkData(...structures.TaxiPark)
	GetTaxiParksUUID() []string
}
type TaxiParkCRM struct {
	tableName struct{} `sql:"crm_taxi_parks,alias:crm_taxipark" pg:",discard_unknown_columns"`
	structures.TaxiPark
	// FIXME избавиться от этого поля
	//SourceOfOrdersUUIDs structures.StringSlice `json:"phonelines_uuid" sql:"phonelines_uuid,type:text[]"`
	CreatorData   *TPCreatorData `json:"creator_data,omitempty"`
	CreatedAt     time.Time      `json:"-"`
	Balance       float64        `json:"balance,omitempty"`
	CardBalance   float64        `json:"card_balance,omitempty"`
	CreatedAtUnix int64          `json:"created_at_unix,omitempty" sql:"-"`
	Deleted       bool           `json:"-"`
}

type TPCreatorData struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

type TaxiParksCache struct {
	m  map[string]TaxiParkCRM
	mx sync.RWMutex
}
type TaxiParkCriteria struct {
	AllowedTaxiParksUUID []string        `json:"taxi_parks_uuid,omitempty"`
	RegionIDs            []int           `json:"region_ids"`
	Name                 string          `json:"name"`
	Page                 int             `json:"page"`
	Limit                int             `json:"limit"`
	Pager                urlvalues.Pager `json:"-"`
}

var currentTaxiParks = NewTaxiParksArr()

func NewTaxiParksArr() *TaxiParksCache {
	return &TaxiParksCache{m: make(map[string]TaxiParkCRM)}
}

func InitCurrentTaxiParks() error {
	tp, err := GetAllTaxiParks()
	if err != nil {
		return errors.Wrap(err, "failed to load taxi parks")
	}

	for _, t := range tp {
		currentTaxiParks.Store(t)
	}

	return nil
}

func (cr *TaxiParkCriteria) GetSuitableTaxiParks() ([]TaxiParkCRM, int, error) {
	if err := cr.validateAndFillPager(); err != nil {
		return nil, 0, err
	}

	res := make([]TaxiParkCRM, 0)
	query := db.Model(&res).
		Where("deleted is not true").
		WhereIn("region_id IN (?)", cr.RegionIDs).
		Order("created_at desc")

	if cr.Name != "" {
		query.Where("name ILIKE ?", cr.Name+"%")
	}
	if len(cr.AllowedTaxiParksUUID) != 0 {
		query.WhereIn("uuid IN (?)", cr.AllowedTaxiParksUUID)
	}

	count, err := query.Count()
	if count == 0 || err != nil {
		return nil, 0, err
	}

	if err := query.Apply(cr.Pager.Pagination).Select(); err != nil {
		return nil, 0, err
	}

	return res, count, nil
}

func (tpc *TaxiParksCache) Store(data TaxiParkCRM) {
	tpc.mx.Lock()
	defer tpc.mx.Unlock()

	tpc.m[data.UUID] = data
}

func (cr *TaxiParkCriteria) validateAndFillPager() error {
	if cr.Page == 0 || cr.Limit == 0 {
		return errors.Errorf("empty pager params")
	}
	cr.Pager.Limit = cr.Limit
	cr.Pager.SetPage(cr.Page)
	return nil
}
func (tpc *TaxiParksCache) Get(uuid string) TaxiParkCRM {
	tpc.mx.RLock()
	defer tpc.mx.RUnlock()

	val, _ := tpc.m[uuid]
	return val
}
func GetTaxiParksByUUID(uuids []string) []structures.TaxiPark {
	var res []structures.TaxiPark
	for _, uuid := range uuids {
		res = append(res, currentTaxiParks.Get(uuid).TaxiPark)
	}
	return res
}
func GetTaxiParksCRMByUUID(uuids []string) []TaxiParkCRM {
	var res []TaxiParkCRM
	for _, uuid := range uuids {
		res = append(res, currentTaxiParks.Get(uuid))
	}
	return res
}
func GetTaxiParkByUUID(uuid string) (TaxiParkCRM, error) {
	var res TaxiParkCRM
	err := db.Model(&res).
		Where("uuid = ?", uuid).
		Select()

	return res, err
}
func GetAllTaxiParks() ([]TaxiParkCRM, error) {
	var res []TaxiParkCRM
	err := db.Model(&res).
		Order("created_at desc").
		Where("deleted is not true").
		Select()

	return res, err
}
func FillTpUnixFields(in []TaxiParkCRM) {
	for i := range in {
		in[i].CreatedAtUnix = in[i].CreatedAt.Unix()
	}
}
func (tp *TaxiParkCRM) FillUnixFields() {
	if tp == nil {
		return
	}
	tp.CreatedAtUnix = tp.CreatedAt.Unix()
}

// Update
func (tp *TaxiParkCRM) Update() error {
	tp.Balance = 0 // this field is not for updating
	/*	if err := tp.validPhoneLines(); err != nil {
		return err
	}*/

	if tp.RegionID == 0 {
		return errors.New("Пустое поле region_id")
	}

	query := db.Model(tp).Where("uuid = ?", tp.UUID).Returning("*")
	if _, err := query.UpdateNotNull(); err != nil {
		return err
	}

	if err := InitCurrentTaxiParks(); err != nil {
		return err
	}

	return nil
}

/*
func (tp *TaxiParkCRM) validPhoneLines() error {
	if len(tp.SourceOfOrdersUUIDs) == 0 {
		return nil
	}

	tpForName := new(TaxiParkCRM)
	query := db.Model(tpForName).
		Where("phonelines_uuid && ?", pg.Array(tp.SourceOfOrdersUUIDs)).
		Where("uuid != ?", tp.UUID).
		Column("name").
		Limit(1)

	if ok, err := query.Exists(); err != nil {
		return errors.Errorf("error valid phonelines, %s", err)
	} else if ok {
		return errors.Errorf("Какая то из указанных вами телефонных линий уже принадлежит таксопарку '%s'", tpForName.Name)
	}

	return nil
}*/

func SetTaxiParkDeleted(uuid string) error {
	query := db.Model(&TaxiParkCRM{}).Where("uuid = ?", uuid).Set("deleted = true")
	if _, err := query.Update(); err != nil {
		return err
	}

	if err := InitCurrentTaxiParks(); err != nil {
		return err
	}

	return nil
}

// TaxiParkCRM - save structure to database
func (tp *TaxiParkCRM) Create() error {
	if err := tp.validate(); err != nil {
		return err
	}
	tp.UUID = structures.GenerateUUID()

	if err := db.RunInTransaction(func(tx *pg.Tx) error {
		//Создание правила распределения для таскопарка
		rule := NewDistributionRule(RuleTypeTaxiPark)
		query := tx.Model(&rule).Returning("id")
		if _, err := query.Insert(); err != nil {
			return err
		}
		tp.DistributionRuleID = rule.ID

		//Создание дефолтной группы для таксопарка

		driverGroup := DriverGroup{}
		defaultTariff := DriverTariffCrm{}
		if err := db.Model(&defaultTariff).Where("offline is true").Limit(1).Select(); err != nil {
			return errors.WithMessage(err, "on default tariff querying")
		}
		driverGroup.DefaultTariffUUID = defaultTariff.UUID
		driverGroup.TaxiParkUUID = tp.UUID
		driverGroup.RegionID = tp.RegionID
		driverGroup.Name = "Дефолтная группа водителей для таксопарка: '" + tp.Name + "'"
		query = tx.Model(&driverGroup).Returning("uuid")
		if _, err := query.Insert(); err != nil {
			return err
		}
		tp.DefaultDriverGroupUUID = driverGroup.UUID

		_, err := tx.Model(tp).Returning("*").Insert()
		if err != nil {
			return err
		}

		return nil
	}); err != nil {
		return err
	}
	if err := InitCurrentTaxiParks(); err != nil {
		return err
	}
	return nil
}

func (tp *TaxiParkCRM) validate() error {
	if tp.Name == "" {
		return errors.New("Введите название")
	}
	if tp.RegionID == 0 {
		return errors.New("Не заполнено поле\"Регион\"")
	}
	return nil
}

func FillTaxiParksData(in []TaxiParkDependent) {
	var neededUUID []string
	for _, val := range in {
		if len(val.GetTaxiParksUUID()) == 0 {
			continue
		}
		neededUUID = append(neededUUID, val.GetTaxiParksUUID()...)
	}
	if len(neededUUID) < 1 {
		return
	}
	for i := range in {
		tpUUID := in[i].GetTaxiParksUUID()
		in[i].SetTaxiParkData(GetTaxiParksByUUID(tpUUID)...)
	}

	return
}

// func GetTaxiParksByUUID(uuids []string) (map[string]TaxiParkCRM, error) {
// 	res := make(map[string]TaxiParkCRM)
// 	if len(uuids) < 1 {
// 		return res, nil
// 	}
// 	var tp []TaxiParkCRM
// 	err := db.Model(&tp).
// 		WhereIn("uuid in (?)", uuids).
// 		Where("deleted is not true ").
// 		Order("created_at desc").
// 		Select()
// 	if err != nil {
// 		return res, err
// 	}
// 	for _, val := range tp {
// 		res[val.UUID] = val
// 	}
// 	return res, nil
// }
//

func FillTaxiParkData(td TaxiParkDependent) {
	if td == nil || len(td.GetTaxiParksUUID()) == 0 {
		return
	}
	tp := GetTaxiParksByUUID(td.GetTaxiParksUUID())

	td.SetTaxiParkData(tp...)
	return
}

func GetTaxiParkUUIDBySourceOfOrdersUUID(sooUUID string) (string, error) {
	var res SourceOfOrdersCRM
	err := db.Model(&res).
		Where("uuid = ?", sooUUID).
		Where("deleted_at is null").
		Column("taxi_park_uuid").
		Select()

	if err == pg.ErrNoRows {
		err = nil
	}
	return res.TaxiParkUUID, err
}

func setTaxiParkBalance(tpUUID string, accountType structures.BillingAccountType, newBalance float64) error {
	q := db.Model((*TaxiParkCRM)(nil)).
		Where("uuid = ?", tpUUID)
	if accountType == structures.AccountTypeCard {
		q = q.Set("card_balance = ?", newBalance)
	} else {
		q = q.Set("balance = ?", newBalance)
	}

	_, err := q.Update()
	return errors.Wrapf(err, "failed to update taxi park's %v balance", newBalance)
}

func GetTaxiParksWeights(taxiparkUUIDs []string) (map[string]int, error) {
	var taxiparks []TaxiParkCRM

	err := db.Model(&taxiparks).
		WhereIn("uuid in (?)", taxiparkUUIDs).
		Column("uuid", "distribution_weight").
		Select()
	if err != nil {
		return nil, err
	}

	result := make(map[string]int)
	for _, taxipark := range taxiparks {
		result[taxipark.UUID] = taxipark.DistributionWeight
	}

	return result, nil
}

func GetTaxiParkByMaxWeight(regionID int) ([]TaxiParkCRM, error) {
	tps := make([]TaxiParkCRM, 0)
	query := db.Model(&tps).Column("uuid", "name", "comment").
		Where("deleted IS NOT TRUE").
		Where("region_id = ?", regionID).
		Order("distribution_weight DESC")
	if err := query.Select(); err != nil {
		return nil, err
	}

	return tps, nil
}
