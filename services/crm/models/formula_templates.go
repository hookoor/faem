package models

import "gitlab.com/faemproject/backend/faem/pkg/structures"

// FormulaTemplateCRM -
type FormulaTemplateCRM struct {
	tableName struct{}
	UUID      string `json:"uuid"`
	structures.FormulaTemplate
	ormStruct
}
