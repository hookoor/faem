package models

// // OrderStateCRM - order orderState structure
// type OrderStateCRM struct {
// 	tableName struct{}  `sql:"crm_orderstates"`
// 	ID        int       `json:"id" sql:",pk"`
// 	CreatedAt time.Time `sql:"default:now()" json:"created_at"`
// 	structures.OfferStates
// }

// func SaveOrderState(orderState structures.OfferStates) (OrderStateCRM, error) {
// 	return OrderStateCRM{}, nil
// }

// // Create - save structure to database
// func (ord *OrderStateCRM) Create() (OrderStateCRM, error) {
// 	nOrderState, err := ord.validCreation()
// 	if err != nil {
// 		return nOrderState, errors.Errorf("Data validation error. %v ", err)
// 	}

// 	uu, err := uuid.NewV4()
// 	if err != nil {
// 		return nOrderState, err
// 	}

// 	nOrderState.UUID = uu.String()
// 	_, err = db.Model(&nOrderState).Returning("*").Insert()
// 	if err != nil {
// 		return nOrderState, errors.Errorf("DB error. %v ", err)
// 	}
// 	return nOrderState, nil
// }

// func (ord *OrderStateCRM) validCreation() (OrderStateCRM, error) {
// 	var newOrderState OrderStateCRM
// 	if ord.Name == "" {
// 		return newOrderState, errors.Errorf("Field name is required")
// 	}
// 	newOrderState.Name = ord.Name
// 	newOrderState.Comment = ord.Comment
// 	newOrderState.Finished = ord.Finished

// 	return newOrderState, nil
// }

// // OrderStateByID - return OrderState structure based on ID
// func OrderStateByID(id int) (OrderStateCRM, error) {
// 	var orderState OrderStateCRM
// 	orderState.ID = id
// 	err := db.Model(&orderState).
// 		Where("deleted is not true AND id = ?", id).
// 		Select()
// 	if err != nil {
// 		return orderState, err
// 	}
// 	return orderState, nil
// }

// // OrderStatesList - return all OrderStates
// func OrderStatesList() ([]OrderStateCRM, error) {
// 	var orderStates []OrderStateCRM
// 	err := db.Model(&orderStates).
// 		Where("deleted is not true ").
// 		Select()
// 	if err != nil {
// 		return orderStates, err
// 	}
// 	return orderStates, nil
// }

// // Update godoc
// func (ord *OrderStateCRM) Update(uuid string) (OrderStateCRM, error) {

// 	uOrderState, err := ord.validUpdate(uuid)
// 	if err != nil {
// 		return uOrderState, errors.Errorf("Data validation error. %v", err)
// 	}

// 	err = UpdateByPK(&uOrderState)
// 	if err != nil {
// 		return uOrderState, errors.Errorf("DB error. %v", err)
// 	}
// 	return uOrderState, nil
// }

// //
// func (ord *OrderStateCRM) validUpdate(uuid string) (OrderStateCRM, error) {

// 	flag := false
// 	var orderState OrderStateCRM

// 	if ord.Name != "" {
// 		orderState.Name = ord.Name
// 		flag = true
// 	}
// 	if ord.Comment != "" {
// 		orderState.Comment = ord.Comment
// 		flag = true
// 	}
// 	if ord.Finished != nil {
// 		orderState.Finished = ord.Finished
// 		flag = true
// 	}

// 	if !flag {
// 		return orderState, errors.Errorf("Required fielord are empty")
// 	}
// 	orderState.UpdatedAt = time.Now()
// 	orderState.UUID = uuid
// 	err := CheckExistsUUID(&orderState, uuid)
// 	if err != nil {
// 		return orderState, err
// 	}
// 	orderState.Deleted = false
// 	return orderState, nil
// }

// // // CheckExists return nil if ord.ID not exists or deleted
// // // TODO: вынести в общие методы и сделать его универсальным
// // func (ord *OrderStateCRM) CheckExists(id int) error {
// // 	exs, _ := db.Model(ord).Column("id").Where("id = ? AND deleted is not true", id).Exists()
// // 	if !exs {
// // 		return errors.Errorf("Not found ID=%v", id)
// // 	}
// // 	return nil
// // }

// // // BeforeUpdate godoc
// // // TODO: вынести в общие методы и сделать его универсальным
// // func (ord *OrderStateCRM) BeforeUpdate(db orm.DB) error {
// // 	ord.UpdatedAt = time.Nord()
// // 	return nil
// // }

// // SetDeleted godoc
// func (ord *OrderStateCRM) SetDeleted() error {
// 	// TODO: вынести в общие методы и сделать его универсальным
// 	var orderState OrderStateCRM
// 	err := CheckExistsUUID(&orderState, ord.UUID)
// 	if err != nil {
// 		return err
// 	}
// 	ord.Deleted = true
// 	_, err = db.Model(ord).Where("uuid = ?uuid").UpdateNotNull()
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// // OrderStatesByRegion godoc
// func OrderStatesByRegion(uuid string) ([]OrderStateCRM, error) {
// 	var orderStates []OrderStateCRM
// 	err := db.Model(&orderStates).
// 		Where("region_uuid = ? AND deleted is not true", uuid).
// 		Select()
// 	if err != nil {
// 		return orderStates, err
// 	}
// 	return orderStates, nil
// }
