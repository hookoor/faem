package models

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// FeaturesSetCRM - таблица-набор на которую ссылается таблица-регион.
// в дальнейшем по id набора будут осуществляться сопоставления для региона с другими таблицами
type FeaturesSetCRM struct {
	tableName struct{} `sql:"crm_features_sets" pg:",discard_unknown_columns"`

	structures.FeaturesSet

	ormStructV2
}

// FeaturesSetForRegionCRM - связующая таблица.
// связывает опции(feature) с id набором который установлен у региона
// db: имеет уникальность связки полей  UNIQUE(set_id, feature_uuid)
type FeaturesSetForRegionCRM struct {
	tableName struct{} `sql:"crm_features_set_by_region,alias:featuresbr"`

	ID          int    `json:"id" sql:",pk"`
	SetID       int    `json:"set_id" sql:"set_id,notnull"`
	FeatureUUID string `json:"feature_uuid"`
	// EnableForDriver - активна ли для выбора.
	// условно водитель может видеть неактивную опцию в сером цвете
	EnableForDriver bool `json:"enable_for_driver"`
	// VisibleForDriver - скрыта для отображения у водителей (логика на фронте)
	VisibleForDriver bool `json:"visible_for_driver"`
	// EnableForClient - активна ли для выбора.
	// условно клиент может видеть неактивную опцию в сером цвете
	EnableForClient bool `json:"enable_for_client"`
	// VisibleForClient - скрыта для отображения у клиента (логика на фронте)
	VisibleForClient bool `json:"visible_for_client"`
	// DisplayPriority - порядок отображения по возростанию.
	// создавалась с типом serial, соответственно у новой записи будет самый низкий приритет отображения
	DisplayPriority int `json:"display_priority"`
}

// FeaturesSetForServiceCRM - связующая таблица.
// связывает опции по id услуги и id набора в разрезе набора для региона
// db: имеет уникальность связки полей  UNIQUE(set_id, service_uuid, feature_uuid)
type FeaturesSetForServiceCRM struct {
	tableName struct{} `sql:"crm_zzz_m2m_features_set_by_service,alias:featuresbs"`

	ID          int    `json:"id" sql:",pk"`
	SetID       int    `json:"set_id" sql:"set_id,notnull"`
	ServiceUUID string `json:"service_uuid"`
	FeatureUUID string `json:"feature_uuid"`
}

// FilterFeaturesSetForRegion -
type FilterFeaturesSetForRegion struct {
	SetID       *int    `json:"set_id" query:"set_id"`
	FeatureUUID *string `json:"feature_uuid" query:"feature_uuid"`
}

// FilterFeaturesSetForService -
type FilterFeaturesSetForService struct {
	ServiceUUID *string `json:"service_uuid" query:"service_uuid"`
	FeatureUUID *string `json:"feature_uuid" query:"feature_uuid"`
}

// UpdatableFieldsFeaturesSetForService -
type UpdatableFieldsFeaturesSetForService struct {
	VisibleForClient *bool `json:"visible_for_client"`
	EnableForClient  *bool `json:"enable_for_client"`
}

type UpdatableFieldsModelFeaturesSetForRegion struct {
	EnableForDriver  *bool
	VisibleForDriver *bool
	EnableForClient  *bool
	VisibleForClient *bool
	DisplayPriority  *int
}

// ---

// IFeaturesSetForRegion  -
type IFeaturesSetForRegion interface {
	// Seting -
	Seting(ctx context.Context, setID int, featureUUID string) error
	// GetFilteredList -
	GetFilteredList(ctx context.Context, filter FilterFeaturesSetForRegion) ([]FeaturesSetForRegionCRM, error)
	// Update -
	Update(ctx context.Context, ftrSetForReg *FeaturesSetForRegionCRM, id int) (*FeaturesSetForRegionCRM, error)
	// UpdateByFeatureUUID -
	UpdateByFeatureUUID(ctx context.Context, updata UpdatableFieldsModelFeaturesSetForRegion, featureuuid string) error
	// Delete -
	Delete(ctx context.Context, id int) error
}

// IFeaturesSetForService  -
type IFeaturesSetForService interface {
	// SetFetureForService -
	SetFetureForService(ctx context.Context, setID int, serviceUUID, featureUUID string) error
	// GetFilteredList -
	GetFilteredList(ctx context.Context, setID int, filter FilterFeaturesSetForService) ([]FeaturesSetForServiceCRM, error)
	// GetFeaturesByServiceForClient -
	GetFeaturesByServiceForClient(ctx context.Context, setID int, serviceUUID string) ([]FeaturesSetForServiceCRM, error)
	// GetGroupFeaturesUUIDsBySetID -
	GetGroupFeaturesUUIDsBySetID(ctx context.Context, setID int) ([]string, error)
	// Delete -
	Delete(ctx context.Context, setID int, id int) error
}

// ModelFeaturesSetForRegion  -
type ModelFeaturesSetForRegion struct {
	db           orm.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}
// ModelFeaturesSetForService -
type ModelFeaturesSetForService struct {
	db           orm.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// NewModelFeaturesSetForRegion -
func NewModelFeaturesSetForRegion(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *ModelFeaturesSetForRegion {
	return &ModelFeaturesSetForRegion{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
}
// NewModelFeaturesSetForService -
func NewModelFeaturesSetForService(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *ModelFeaturesSetForService {
	return &ModelFeaturesSetForService{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Seting -
func (fsr *ModelFeaturesSetForRegion) Seting(ctx context.Context, setID int, featureUUID string) error {
	var fsfr FeaturesSetForRegionCRM

	fsfr.SetID = setID
	fsfr.FeatureUUID = featureUUID

	_, err := fsr.db.ModelContext(ctx, &fsfr).Insert()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// GetFilteredList -
func (fsr *ModelFeaturesSetForRegion) GetFilteredList(ctx context.Context, filter FilterFeaturesSetForRegion) ([]FeaturesSetForRegionCRM, error) {
	var ftrSetForReg []FeaturesSetForRegionCRM

	// TODO: get region by id and extract set_id

	err := fsr.db.ModelContext(ctx, &ftrSetForReg).
		WhereOrGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.SetID != nil {
				q.Where("set_id = ?", filter.SetID)
			}
			if filter.FeatureUUID != nil {
				q.Where("feature_uuid = ?", filter.FeatureUUID)
			}
			return q, nil
		}).Select()

	if err != nil {
		return nil, errpath.Err(err)
	}

	return ftrSetForReg, nil
}

// Update -
func (fsr *ModelFeaturesSetForRegion) Update(ctx context.Context, ftrSetForReg *FeaturesSetForRegionCRM, id int) (*FeaturesSetForRegionCRM, error) {

	ormres, err := fsr.db.ModelContext(ctx, ftrSetForReg).
		Where("id = ?", id).
		Returning("*").
		UpdateNotNull()
	if err != nil {
		return nil, errpath.Err(err)
	}
	if ormres.RowsAffected() == 0 {
		return nil, errpath.Errorf("ни одна запись не обновлена")
	}

	return ftrSetForReg, nil
}

// Update -
func (fsr *ModelFeaturesSetForRegion) UpdateByFeatureUUID(ctx context.Context, updata UpdatableFieldsModelFeaturesSetForRegion, featureuuid string) error {

	query := fsr.db.ModelContext(ctx, (*FeaturesSetForRegionCRM)(nil)).
		Where("feature_uuid = ?", featureuuid)
	if updata.EnableForDriver != nil {
		query.Set("enable_for_driver = ?", updata.EnableForDriver)
	}
	if updata.VisibleForDriver != nil {
		query.Set("visible_for_driver = ?", updata.VisibleForDriver)
	}
	if updata.EnableForClient != nil {
		query.Set("enable_for_client = ?", updata.EnableForClient)
	}
	if updata.VisibleForClient != nil {
		query.Set("visible_for_client = ?", updata.VisibleForClient)
	}
	if updata.DisplayPriority != nil {
		query.Set("display_priority = ?", updata.DisplayPriority)
	}
	_, err := query.Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// Delete -
func (fsr *ModelFeaturesSetForRegion) Delete(ctx context.Context, id int) error {

	_, err := fsr.db.ModelContext(ctx, (*FeaturesSetForRegionCRM)(nil)).
		Where("id = ?", id).
		Delete()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// -------------------------------------------------

// SetFetureForService -
func (fss *ModelFeaturesSetForService) SetFetureForService(ctx context.Context, setID int, serviceUUID, featureUUID string) error {
	var fsfs FeaturesSetForServiceCRM

	fsfs.SetID = setID
	fsfs.ServiceUUID = serviceUUID
	fsfs.FeatureUUID = featureUUID

	_, err := fss.db.ModelContext(ctx, &fsfs).Insert()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// GetFilteredList -
func (fss *ModelFeaturesSetForService) GetFilteredList(ctx context.Context, setID int, filter FilterFeaturesSetForService) ([]FeaturesSetForServiceCRM, error) {
	var fsfs []FeaturesSetForServiceCRM

	err := fss.db.ModelContext(ctx, &fsfs).Where("set_id = ?", setID).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.ServiceUUID != nil {
				q.Where("service_uuid = ?", filter.ServiceUUID)
			}
			if filter.FeatureUUID != nil {
				q.Where("feature_uuid = ?", filter.FeatureUUID)
			}
			return q, nil
		}).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return fsfs, nil
}

// GetFeaturesByServiceForClient -
func (fss *ModelFeaturesSetForService) GetFeaturesByServiceForClient(ctx context.Context, setID int, serviceUUID string) ([]FeaturesSetForServiceCRM, error) {
	var fsfs []FeaturesSetForServiceCRM

	err := fss.db.ModelContext(ctx, &fsfs).
		Where("set_id = ?", setID).
		Where("service_uuid = ?", serviceUUID).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return fsfs, nil
}

// GetGroupFeaturesUUIDsBySetID -
func (fss *ModelFeaturesSetForService) GetGroupFeaturesUUIDsBySetID(ctx context.Context, setID int) ([]string, error) {
	var res []string

	err := fss.db.ModelContext(ctx, (*FeaturesSetForServiceCRM)(nil)).
		Where("set_id = ?", setID).
		Group("features_uuid").
		Select(&res)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return res, nil
}


// Delete -
func (fss *ModelFeaturesSetForService) Delete(ctx context.Context, setID int, id int) error {

	_, err := fss.db.ModelContext(ctx, (*FeaturesSetForServiceCRM)(nil)).
		Where("set_id = ?", setID).
		Where("id = ?", id).
		Delete()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
