package models

import (
	"time"

	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// BlackListData common struct - used in driver struct
type (
	BlackListData struct {
		Phone         string                 `json:"phone"`
		Comment       string                 `json:"comment"`
		CreatedAtUnix int64                  `json:"created_at_unix"`
		CreatorType   structures.ChatMembers `json:"creator_type"`
		CreatorUUID   string                 `json:"creator_uuid"`
		CreatorName   string                 `json:"creator_name"`
		Ignore        bool                   `json:"ignore"`
		OrderID       int                    `json:"order_id"`
	}
	BlackListFilterCriteria struct {
		Phone string `json:"phone"`
		Pager urlvalues.Pager
	}
)

// BlackListData struct for separate table with phone numbers that cannot call to faem
type BlackListDataCRM struct {
	tableName struct{} `sql:"crm_blacklist"`
	BlackListData
	UUID    string `json:"uuid"`
	Deleted bool   `json:"-"`
}

// GetBlackList returns numbers from black list table
func GetBlackList(pager urlvalues.Pager) (result []BlackListDataCRM, count int, err error) {
	query := db.Model(&result).
		Where("deleted is not true").
		Order("created_at_unix desc")
	count, err = query.
		Count()
	if count == 0 || err != nil {
		return
	}
	err = query.
		Apply(pager.Pagination).
		Select()
	return
}

// GetBlackListedPhone returns phone in blacklist
func GetBlackListedPhone(phone string) (result BlackListDataCRM, err error) {
	err = db.Model(&result).
		Where("deleted is not true").
		Where("phone = ?", phone).
		Select()
	return
}
func (bd *BlackListDataCRM) UpdateBlackListedPhone(phone string) *structures.ErrorWithLevel {
	bd.Phone = phone
	check, err := db.Model(bd).
		Where("phone = ?", phone).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if !check {
		return structures.Warning(errors.Errorf("Такого номера нет в черном списке"))
	}
	_, err = db.Model(bd).Where("phone = ?", phone).
		UpdateNotNull()
	if err != nil {
		return structures.Error(err)
	}
	return nil
}
func (cr *BlackListFilterCriteria) GetBlackListByFilter() (result []BlackListDataCRM, count int, err error) {
	query := db.Model(&result).
		Where("deleted is not true").
		Order("created_at_unix desc")

	if cr.Phone != "" {
		query = query.
			Where("phone ILIKE ?", "%"+cr.Phone+"%")
	}
	count, err = query.
		Count()
	if count == 0 || err != nil {
		return
	}
	err = query.
		Apply(cr.Pager.Pagination).
		Select()
	return
}

// RemovePhoneFromBlackList
func RemovePhoneFromBlackList(phone string) *structures.ErrorWithLevel {
	query := db.Model(&BlackListDataCRM{}).
		Where("phone = ?", phone).
		Where("deleted is not true")
	check, err := query.
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if !check {
		return structures.Warning(errors.Errorf("Такого номера нет в черном списке"))
	}
	_, err = query.
		Set("deleted = true").
		Update()
	return structures.Error(err)
}

// AddNumberToBlackList
func AddNumberToBlackList(bd *BlackListData) *structures.ErrorWithLevel {
	bl := BlackListDataCRM{
		BlackListData: *bd,
	}
	bl.CreatedAtUnix = time.Now().Unix()
	errWL := bl.validate()
	if errWL != nil {
		return errWL
	}
	err := bl.fillCreatorName()
	if err != nil {
		return structures.Error(err)
	}
	bl.UUID = structures.GenerateUUID()
	_, err = db.Model(&bl).
		Insert()
	if err != nil {
		return structures.Error(err)
	}
	return nil
}

func (bd *BlackListDataCRM) validate() *structures.ErrorWithLevel {
	if bd.CreatorType != structures.UserCRMMember {
		return structures.Warning(errors.Errorf("invalid user for this action"))
	}
	check, err := db.Model(bd).
		Where("phone = ?phone").
		Where("deleted is not true").
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if check {
		return structures.Warning(errors.Errorf("Этот номер уже добавлен в черный список"))
	}
	if bd.Comment == "" {
		return structures.Warning(errors.Errorf("Укажите причину добавления в комментарий"))
	}
	bd.Deleted = false
	return nil
}

// Checks if phone is in crm_blacklist
func IsPhoneInBlackList(phone string) (bool, error) {
	var bl BlackListDataCRM
	bl.Phone = phone
	check, err := db.Model(&bl).
		Where("phone = ?", phone).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return check, err
	}
	return check, nil
}
