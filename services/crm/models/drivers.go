package models

import (
	"context"
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
)

type DriverInteraction struct {
	Driver           IDriver
	ServiceRealation IRelationDriverService
	FeatureRealation IRelationDriverFeature
}

func NewDriverInteraction(db orm.DB) *DriverInteraction {
	return &DriverInteraction{
		Driver:           NewModelDriver(db),
		ServiceRealation: NewModelRelationDriverService(db),
		FeatureRealation: NewModelRelationDriverFeature(db),
	}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

type IDriver interface {
	UploadBDInterface(dbi *DB)

	FilterDriversList(ctx context.Context, cr *DriverFilterCriteria) ([]DriverCRM, int, error)
}

type ModelDriver struct {
	db  orm.DB
	dbi *DB
}

func NewModelDriver(db orm.DB) *ModelDriver {
	return &ModelDriver{
		db: db,
	}
}

func (md *ModelDriver) UploadBDInterface(dbi *DB) {
	md.dbi = dbi
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

//DriverFilterCriteria принмает список критерий для фильтра драйверов
type DriverFilterCriteria struct {
	States          []string        `json:"states"`
	Tags            []string        `json:"tags"`
	Phone           string          `json:"phone"`
	Alias           string          `json:"alias"`
	TaxiParksUUID   []string        `json:"taxi_parks_uuid"`
	RegionID        string          `json:"region_id"`
	Name            string          `json:"name"`
	DriverGroupName string          `json:"driver_group_uuid"`
	MinDateUnix     int64           `json:"mindate"`
	MaxDateUnix     int64           `json:"maxdate"`
	Page            int             `json:"page"`
	Limit           int             `json:"limit"`
	Pager           urlvalues.Pager `json:"-"`
	Color           string          `json:"color"`
	RegNumber       string          `json:"reg_number"`
	Car             string          `json:"car"`
	ActivityLow     int             `json:"activity_low"`
	ActivityHigh    int             `json:"activity_high"`

	ActiveServicesUUIDs []string `json:"active_services_uuids"`
	ActiveFeaturesUUIDs []string `json:"active_features_uuids"`
}

// LocationDataCRM координаты водителей
type LocationDataCRM struct {
	tableName struct{} `sql:"crm_drv_locations"`
	ID        int      `sql:",pk" json:"-"`
	structures.LocationData
}

// DrvCoordinates координаты водителей
type DrvCoordinates struct {
	SendingTime int64   `json:"sending_time"`
	Satellites  int     `json:"satellites"`
	Latitude    float64 `json:"latitude"`
	Longitude   float64 `json:"longitude"`
}

// DriverCRM - order driver structure
type DriverCRM struct {
	tableName struct{} `sql:"crm_drivers, alias:drivers"  pg:",discard_unknown_columns"`
	ormStruct
	structures.Driver
	DeviceData struct {
		CurrentAppVersion string `json:"current_app_version"`
		DeviceModel       string `json:"device_model"`
		AndroidVersion    string `json:"android_ver"`
	} `json:"device_data" sql:"-"`
	BlacklistData         []BlackListData `json:"blacklist_data"`
	DrivingLicenseNumber  string          `json:"driving_license_number" sql:"driving_license_number" description:"Номер водительского удостоверения"`
	DriverLicenseValidity string          `json:"driver_license_validity" sql:"driver_license_validity" description:"Срок действия водительского удостоверения"`
	PasportSeriesNumber   string          `json:"pasport_series_number" sql:"pasport_series_number" description:"Серия номер паспорта"`
	RegistrationAddress   string          `json:"registration_address" sql:"registration_address" description:"Адрес прописки"`
	LivingAddress         string          `json:"living_address" sql:"living_address" description:"Фактическое местопроживание"`
	TecPassportSeries     string          `json:"tec_passport_series" sql:"tec_passport_series" description:"Серия техпаспорта"`
	PaymentType           string          `json:"payment_type" sql:"payment_type"`
	StatusUpdateTime      time.Time       `json:"status_update_time" sql:"status_update_time"`
	GetOrderRadius        float64         `json:"get_order_radius" sql:"get_order_radius"`
	TariffUUID            string          `json:"tariff_uuid" sql:"-"`
	Coordinates           DrvCoordinates  `json:"сoordinates" sql:"-"`
	Password              string          `json:"password"`
	StateTitle            string          `json:"state_title" sql:"-"`
	CreatedAtUnix         int64           `json:"created_at_unix" sql:"-"`
	RegionID              int             `json:"region_id"` // WARNING: заполняется в бд триггером tg_crm_drivers_on
	// ActiveFeaturesUUIDs - список опций которые активны у водителя.
	// при обновлении предварительно надо удалить все имеющиеся у водителя из таблицы связей
	ActiveFeaturesUUIDs []string `json:"active_features_uuids" sql:"-"`
	// ActiveServicesUUIDs - список услуг которые присылает активны у водителя.
	// при обновлении предварительно надо удалить все имеющиеся у водителя из таблицы связей
	ActiveServicesUUIDs []string `json:"active_services_uuids" sql:"-"`
	// LastCounterAssignment - время последнего назначения встречного заказа
	LastCounterAssignment time.Time `json:"-"`
}

// Create - save structure to database
func (dr *DriverCRM) Create(ctx context.Context) (DriverCRM, error) {
	nDriver, err := dr.validCreation()
	if err != nil {
		return nDriver, err
	}
	err = nDriver.fillServicesAndFeatures(DriverCRM{})
	if err != nil {
		return nDriver, err
	}
	if len(nDriver.AvailableServices) == 0 {
		nDriver.AvailableServices, nDriver.Tag, err = GetStandardServices()
		if err != nil {
			return nDriver, errors.Errorf("error getting standart services. %s", err)
		}
	}
	{ // случайная генерация пароля
		var pswd string
		for i := 0; i < 4; i++ {
			pswd += strconv.Itoa(rand.Intn(10))
		}
		nDriver.Password = pswd
	}
	{ // содание алиаса
		var lastAlias int
		err := db.ModelContext(ctx, (*DriverCRM)(nil)).
			ColumnExpr("MAX(alias)").
			Select(&lastAlias)
		if err != nil {
			return DriverCRM{}, errpath.Err(err, "error finding last alias")
		}
		nDriver.Alias = lastAlias + 1
	}

	nDriver.UUID = structures.GenerateUUID()
	_, err = db.ModelContext(ctx, &nDriver).
		Returning("*").
		Insert()
	if err != nil {
		return nDriver, errors.Errorf("DB error. %v ", err)
	}
	return nDriver, nil
}

// RemovePhoneFromDriverBlackList
func RemovePhoneFromDriverBlackList(driverUUID, phone string) *structures.ErrorWithLevel {
	var driver DriverCRM
	err := GetByUUID(driverUUID, &driver)
	if err != nil {
		return structures.Error(err)
	}

	var newBlackList []string
	for _, item := range driver.Blacklist {
		if item != phone {
			newBlackList = append(newBlackList, item)
		}
	}
	if len(newBlackList) == len(driver.Blacklist) {
		return structures.Warning(errors.Errorf("Такого номера нет в черном списке"))
	}
	var newBlackListData []BlackListData
	for _, item := range driver.BlacklistData {
		if item.Phone != phone {
			newBlackListData = append(newBlackListData, item)
		}
	}
	driver.Blacklist = newBlackList
	driver.BlacklistData = newBlackListData
	_, err = db.Model(&driver).
		Where("uuid = ?", driverUUID).
		Set("blacklist = ?blacklist").
		Set("blacklist_data = ?blacklist_data").
		Update()
	if err != nil {
		return structures.Error(err)
	}
	err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driver)
	if err != nil {
		return structures.Error(err)
	}
	return nil
}

// AddNumberToDriverBlackList
func AddNumberToDriverBlackList(driverUUID string, bd BlackListData) *structures.ErrorWithLevel {
	var driver DriverCRM
	bd.CreatedAtUnix = time.Now().Unix()
	err := GetByUUID(driverUUID, &driver)
	if err != nil {
		return structures.Error(err)
	}
	err = bd.validate(driver.BlacklistData)
	if err != nil {
		return structures.Warning(err)
	}
	err = bd.fillCreatorName()
	if err != nil {
		return structures.Error(err)
	}
	driver.Blacklist = append(driver.Blacklist, bd.Phone)
	driver.BlacklistData = append(driver.BlacklistData, bd)
	_, err = db.Model(&driver).
		Where("uuid = ?", driverUUID).
		Set("blacklist = ?blacklist").
		Set("blacklist_data = ?blacklist_data").
		Update()
	if err != nil {
		return structures.Error(err)
	}
	err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driver)
	if err != nil {
		return structures.Error(err)
	}
	return nil
}
func (bd *BlackListData) fillCreatorName() error {
	var err error
	switch bd.CreatorType {
	case structures.DriverMember:
		var driver DriverCRM
		err = GetByUUID(bd.CreatorUUID, &driver)
		bd.CreatorName = driver.Name
	case structures.UserCRMMember:

		var user UsersCRM
		err = GetByUUID(bd.CreatorUUID, &user)
		bd.CreatorName = user.Name
	default:
		err = errors.Errorf("invalid creator type")
	}
	return err
}
func (bd *BlackListData) validate(oldBlackListData []BlackListData) error {
	for _, num := range oldBlackListData {
		if num.Phone == bd.Phone {
			return errors.Errorf("Этот номер уже добавлен в черный список водителя")
		}
	}
	if bd.Comment == "" {
		return errors.Errorf("Укажите причину добавления в комментарий")
	}
	return nil
}

// SaveDriverFromDriverAPP - save driver to database
func (dr *DriverCRM) SaveDriverFromDriverAPP() (*DriverCRM, error) {
	drvTF := new(DriverTariffCrm)
	taxiPark := new(TaxiParkCRM)
	driverGroup := new(DriverGroup)

	if err := db.Model(taxiPark).Where("uuid = ?", dr.TaxiParkUUID).Select(); err != nil {
		return nil, errors.Errorf("error finding driver taxipark. %v ", err)
	}
	if err := db.Model(driverGroup).Where("uuid = ?", taxiPark.DefaultDriverGroupUUID).Select(); err != nil {
		return nil, errors.Errorf("error finding default driver driver taxipark. %v ", err)
	}
	if err := db.Model(drvTF).Where("uuid = ?", driverGroup.DefaultTariffUUID).Select(); err != nil {
		return nil, errors.Errorf("error finding default driver tariff. %v ", err)
	}
	dr.DrvTariff = structures.SelectedDriverTariff{DriverTariff: drvTF.DriverTariff}
	// устанавливаем водиле дефолтную группу таксопарка
	dr.Group = driverGroup.DriverGroup
	// устанавливаем водиле дефолтный тариф группы
	dr.TariffUUID = driverGroup.DefaultTariffUUID

	//услуги водилам теперь добавляются согласно их группе

	//availableServices, tag, err := GetStandardServices()
	//if err != nil {
	//	return nil, err
	//}
	//dr.AvailableServices, dr.Tag = availableServices, tag

	if _, err := db.Model(dr).Insert(); err != nil {
		return nil, errors.Errorf("error insert new driver to database. %v ", err)
	}

	return dr, nil
}

func (dr *DriverCRM) validCreation() (DriverCRM, error) {
	var newDriver DriverCRM
	if dr.Phone == "" {
		return newDriver, errors.Errorf("Номер телефона - обязательное поле")
	}
	if dr.TaxiParkUUID != nil && *dr.TaxiParkUUID == "" {
		return newDriver, errors.Errorf("Таксопарк - обязательное поле")
	}

	check, err := db.Model(&DriverCRM{}).
		Where("phone = ?", dr.Phone).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return newDriver, errors.Errorf("error checking driver exists, %s", err)
	}
	if check {
		return newDriver, errors.Errorf("Водитель с таким номером уже зарегистрирован")
	}

	newDriver.Status = dr.Status
	newDriver.Name = dr.Name

	newDriver.DrivingLicenseNumber = dr.DrivingLicenseNumber
	newDriver.DriverLicenseValidity = dr.DriverLicenseValidity
	newDriver.PasportSeriesNumber = dr.PasportSeriesNumber
	newDriver.RegistrationAddress = dr.RegistrationAddress
	newDriver.TaxiParkUUID = dr.TaxiParkUUID
	newDriver.LivingAddress = dr.LivingAddress
	newDriver.TecPassportSeries = dr.TecPassportSeries

	newDriver.Comment = dr.Comment
	newDriver.RegNumber = dr.RegNumber
	newDriver.Phone = dr.Phone
	newDriver.Car = dr.Car
	newDriver.Color = dr.Color
	newDriver.Karma = dr.Karma
	newDriver.Status = constants.DriverStateOnModeration
	newDriver.Tag = dr.Tag
	newDriver.TariffUUID = dr.TariffUUID
	newDriver.MaxServiceLevel = dr.MaxServiceLevel

	return newDriver, nil
}

// GetDriverByUUID - return Driver structure
func (dr *DriverCRM) GetDriverByUUID(uuid string) error {

	err := db.Model(dr).
		Where("deleted is not true AND uuid = ?", uuid).
		Select()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// FilterDriversList - дергает из базы водил с заданными параметрами
func (md ModelDriver) FilterDriversList(ctx context.Context, cr *DriverFilterCriteria) ([]DriverCRM, int, error) {
	var drivers []DriverCRM

	err := cr.validateAndFillPager()
	if err != nil {
		return drivers, 0, err
	}
	query := db.Model(&drivers).
		Order("created_at DESC").
		Where("phone ILIKE ?", "%"+cr.Phone+"%").
		Where("deleted is not true").
		Apply(cr.Pager.Pagination)
	if len(cr.States) != 0 {
		query = query.
			Where("status in (?)", pg.In(cr.States))
	}
	if cr.Name != "" {
		query = query.
			Where("name ILIKE ?", "%"+cr.Name+"%")
	}
	if cr.MinDateUnix != 0 {
		query = query.
			Where("created_at >= ?", time.Unix(cr.MinDateUnix, 0))
	}
	if cr.MaxDateUnix != 0 {
		query = query.
			Where("created_at <= ?", time.Unix(cr.MaxDateUnix, 0))
	}
	if len(cr.Tags) != 0 {
		query = query.
			Where("tag::text[] && array[?]", pg.In(cr.Tags))
	}
	if cr.Alias != "" {
		query = query.
			Where("alias = ?", cr.Alias)
	}
	if len(cr.TaxiParksUUID) != 0 {
		query = query.
			WhereIn("taxi_park_uuid IN (?)", cr.TaxiParksUUID)
	}
	if len(cr.RegionID) != 0 {
		query = query.
			WhereIn("region_id = ?", cr.RegionID)
	}
	if cr.DriverGroupName != "" {
		query = query.Where("driver_group ->> 'uuid' = ?", cr.DriverGroupName)
	}
	if cr.Color != "" {
		query = query.
			Where("color ILIKE ?", "%"+cr.Color+"%")
	}
	if cr.Car != "" {
		query = query.
			Where("car ILIKE ?", "%"+cr.Car+"%")
	}
	if cr.RegNumber != "" {
		query = query.
			Where("reg_number ILIKE ?", "%"+cr.RegNumber+"%")
	}
	if cr.ActivityLow != 0 {
		query = query.
			Where("activity >= ?", cr.ActivityLow)
	}
	if cr.ActivityHigh != 0 {
		query = query.
			Where("activity <= ?", cr.ActivityHigh)
	}
	if len(cr.ActiveServicesUUIDs) != 0 {
		query = query.
			Join("join crm_zzz_m2m_active_services_for_drivers as srv on drivers.uuid = srv.driver_uuid").
			Column("drivers.*").
			WhereIn("srv.service_uuid in (?)", cr.ActiveServicesUUIDs)
	}
	if len(cr.ActiveFeaturesUUIDs) != 0 {
		query = query.
			Join("join crm_zzz_m2m_active_features_for_drivers as ftr on drivers.uuid = ftr.driver_uuid").
			Column("drivers.*").
			WhereIn("ftr.feature_uuid in (?)", cr.ActiveFeaturesUUIDs)
	}

	count, err := query.
		Count()
	if err != nil {
		return drivers, count, err
	}
	err = query.
		Apply(cr.Pager.Pagination).
		Select()
	if err != nil {
		return drivers, count, err
	}
	drivers, err = FillDriverLocation(drivers)
	if err != nil {
		return drivers, count, errors.Errorf("error fillind driver locations, %s", err)
	}

	return drivers, count, nil
}

func (cr *DriverFilterCriteria) validateAndFillPager() error {
	if cr.Page == 0 || cr.Limit == 0 {
		return errors.Errorf("empty pager params")
	}
	cr.Pager.Limit = cr.Limit
	cr.Pager.SetPage(cr.Page)
	return nil
}

// DriversList - return all Drivers
func DriversList() ([]DriverCRM, error) {
	var drivers []DriverCRM
	err := db.Model(&drivers).
		Where("deleted is not true").
		Order("created_at DESC").
		Select()
	if err != nil {
		return drivers, err
	}
	drivers, err = FillDriverLocation(drivers)
	if err != nil {
		return drivers, err
	}
	return drivers, nil
}

// FillDriverLocation - fill Drivers Coordinates
func FillDriverLocation(drivers []DriverCRM) ([]DriverCRM, error) {
	driverUUIDs := make([]string, 0, len(drivers))
	for _, drv := range drivers {
		driverUUIDs = append(driverUUIDs, drv.UUID)
	}

	drvLocations := CurrentDriverLocationsByUUIDs(driverUUIDs)
	for i, drv := range drivers {
		drvLoc, found := drvLocations[drv.UUID]
		if !found {
			continue
		}
		drivers[i].Coordinates.SendingTime = drvLoc.CreatedAt.Unix()
		drivers[i].Coordinates.Satellites = drvLoc.Satelites
		drivers[i].Coordinates.Latitude = drvLoc.Latitude
		drivers[i].Coordinates.Longitude = drvLoc.Longitude
	}
	return drivers, nil
}

// Update godoc
func (dr *DriverCRM) Update(uuid string, memberType structures.ChatMembers) (DriverCRM, structures.DriverStates, error) {
	uDriver, oldDriver, drst, err := dr.validUpdate(uuid)
	if err != nil {
		return uDriver, drst, err
	}

	if err := uDriver.fillServicesAndFeatures(oldDriver, memberType); err != nil {
		return uDriver, structures.DriverStates{}, errors.Errorf("%v", err)
	}

	if err := UpdateByPK(&uDriver); err != nil {
		return uDriver, structures.DriverStates{}, errors.Errorf("DB error. %v", err)
	}

	return uDriver, drst, nil
}

func (dr *DriverCRM) validUpdate(uuid string) (DriverCRM, DriverCRM, structures.DriverStates, error) {

	flag := false
	var (
		driverState structures.DriverStates
		driver      DriverCRM
		check       bool
	)
	err := GetByUUID(uuid, &driver)
	oldDriver := driver
	if err != nil {
		return driver, oldDriver, structures.DriverStates{}, err
	}

	if dr.Status == constants.DriverStateBlocked || dr.Status == constants.DriverStateAvailable {
		check = true
		driver.Status = dr.Status
		driverState.State = driver.Status
		driverState.CreatedAt = time.Now()
		driverState.DriverUUID = uuid
		driver.UUID = uuid
		_, err := SaveDriverState(driverState)
		if err != nil {
			return DriverCRM{}, oldDriver, structures.DriverStates{}, errors.Errorf("DB error(SetDriverState) %v", err)
		}

		// костыль для решения некоторых проблем операторами без привлечения разработчиков
		if dr.Status == constants.DriverStateAvailable {
			// удаляем координаты этого водителя из кэша
			currentDriverLocations.Purge(uuid)
		}

	}
	if dr.DrivingLicenseNumber != "" {
		driver.DrivingLicenseNumber = dr.DrivingLicenseNumber
		flag = true
	}

	if dr.DriverLicenseValidity != "" {
		driver.DriverLicenseValidity = dr.DriverLicenseValidity
		flag = true
	}

	if dr.MaxServiceLevel != nil {
		driver.MaxServiceLevel = dr.MaxServiceLevel
		flag = true
	}
	if dr.TaxiParkUUID != nil {
		driver.TaxiParkUUID = dr.TaxiParkUUID
		flag = true
	}
	if dr.PasportSeriesNumber != "" {
		driver.PasportSeriesNumber = dr.PasportSeriesNumber
		flag = true
	}

	if dr.RegistrationAddress != "" {
		driver.RegistrationAddress = dr.RegistrationAddress
		flag = true
	}

	if dr.LivingAddress != "" {
		driver.LivingAddress = dr.LivingAddress
		flag = true
	}

	if len(dr.PaymentTypes) != 0 {
		for _, val := range dr.PaymentTypes {
			key := structures.OrderPaymentType(val)
			if _, ok := structures.OrderPaymentTypes[key]; !ok {
				return driver, oldDriver, structures.DriverStates{}, errors.Errorf("Неккоректный тип оплаты %s", val)
			}
		}
		driver.PaymentTypes = dr.PaymentTypes
		flag = true
	}

	if dr.TecPassportSeries != "" {
		driver.TecPassportSeries = dr.TecPassportSeries
		flag = true
	}

	if dr.Name != "" {
		driver.Name = dr.Name
		flag = true
	}

	if dr.FullRegistrationNumber != "" {
		driver.FullRegistrationNumber = dr.FullRegistrationNumber
		flag = true
	}

	if dr.Tag != nil {
		driver.Tag = dr.Tag
		flag = true
	}

	if dr.Karma != 0 {
		driver.Karma = dr.Karma
		flag = true
	}
	if dr.Comment != "" {
		driver.Comment = dr.Comment
		flag = true
	}
	if dr.TariffUUID != "" {
		driver.TariffUUID = dr.TariffUUID
		flag = true
	}
	if dr.RegNumber != "" {
		driver.RegNumber = dr.RegNumber
		flag = true
	}
	if dr.Color != "" {
		driver.Color = dr.Color
		flag = true
	}
	if dr.Phone != "" {
		formattedPhone, err := phoneverify.NumberVerify(dr.Phone)
		if err != nil {
			return driver, oldDriver, structures.DriverStates{}, errors.Errorf("неккоректный номер телефона, %s", err)
		}
		alias, checkExists, err := checkDriverExistsByPhone(formattedPhone, uuid)
		if err != nil {
			return driver, oldDriver, structures.DriverStates{}, errors.Errorf("error check driver exists by phone, %s", err)
		}
		if checkExists {
			msg := fmt.Sprintf("Аккаунт с таким номером телефона уже зарегистрирован. Удалите аккаунт с позывным %v для возможности задать этот номер", alias)
			return driver, oldDriver, structures.DriverStates{}, errors.Errorf(msg)
		}
		driver.Phone = formattedPhone
		flag = true
	}
	if dr.Car != "" {
		driver.Car = dr.Car
		flag = true
	}

	if dr.Activity != 0 {
		driver.Activity = dr.Activity
		flag = true
	}
	if len(dr.ActiveFeaturesUUIDs) != 0 {
		driver.ActiveFeaturesUUIDs = dr.ActiveFeaturesUUIDs
		flag = true
	}
	if len(dr.ActiveServicesUUIDs) != 0 {
		driver.ActiveServicesUUIDs = dr.ActiveServicesUUIDs
		flag = true
	}

	if check {
		return driver, oldDriver, driverState, nil
	} else if !flag {
		return driver, oldDriver, structures.DriverStates{}, errpath.Errorf("Required fields are empty")
	}
	driver.UpdatedAt = time.Now()
	driver.UUID = uuid

	driver.Deleted = false
	return driver, oldDriver, structures.DriverStates{}, nil
}
func checkDriverExistsByPhone(phone, uuid string) (driverAlias int, check bool, err error) {
	query := db.Model((*DriverCRM)(nil)).Column("alias").Where("phone = ? and uuid != ?", phone, uuid).Where("deleted is not true")
	if err := query.Select(&driverAlias); err != nil && err != pg.ErrNoRows {
		return 0, false, err
	} else if err == pg.ErrNoRows {
		return 0, false, nil
	}

	return driverAlias, true, nil
}

//fillServicesAndFeatures заполняет структуру сервисов, фич и тарифа
func (dr *DriverCRM) fillServicesAndFeatures(oldDriver DriverCRM, memberType ...structures.ChatMembers) error {
	var driverTariff DriverTariffCrm
	if dr.TariffUUID != "" {
		err := GetByUUID(dr.TariffUUID, &driverTariff)
		if err != nil {
			return errors.Errorf("error getting driver tariff by uuid [%s],%v", dr.TariffUUID, err)
		}

		dr.DrvTariff = structures.SelectedDriverTariff{
			DriverTariff: driverTariff.DriverTariff,
			PayedAt:      dr.DrvTariff.PayedAt, // important to keep "PayedAt" field if filled
		}
		//} else {
		//	if err := dr.fillDefaultDriverTariff(); err != nil {
		//		return errors.Errorf("error filling driver tariff: %v", err)
		//	}
		// FIXME: при обновлении водителя вызывается эта функция, фронт никогда не передает tariff_uuid -> тариф слетает
	}
	oldServices := oldDriver.AvailableServices
	serviceIsAlreadyOn := func(uuid string) bool {
		for _, item := range oldServices {
			if item.UUID == uuid {
				return true
			}
		}
		return false
	}
	var memType structures.ChatMembers
	if len(memberType) != 0 {
		memType = memberType[0]
	}
	//Обнуляем поля
	dr.AvailableFeatures = make([]structures.Feature, 0)
	dr.AvailableServices = make([]structures.Service, 0)
	features, err := FeatureList("")
	if err != nil {
		return errors.Errorf("error getting features list,%v", err)
	}

	services, err := ServicesList(false, oldDriver.Group.ServicesUUID...)
	if err != nil {
		return errors.Errorf("error getting services list,%v", err)
	}

	// Ущербный костыль, убрать весь блок, когда придумаем что-нибудь с тегами:
	{
		tagServices := make([]ServiceCRM, 0)
		query := db.Model(&tagServices).
			Where("deleted is not true").
			Where("tag && ?", pg.Array(dr.Tag))

		if len(oldDriver.Group.ServicesUUID) > 0 {
			query.WhereIn("uuid NOT IN (?)", oldDriver.Group.ServicesUUID)
		}

		if err := query.Order("display_priority desc").Select(); err != nil {
			if err != pg.ErrNoRows {
				return errors.Wrap(err, "error getting services list by tags")
			}
		}
		services = append(services, tagServices...)
	}

	var reqFeatures []string
	dr.Tag = sortByUnique(dr.Tag)
	for _, value := range dr.Tag {
		for _, service := range services {
			for _, serviceTag := range service.Tag {
				if serviceTag == value {
					reqFeatures = append(reqFeatures, service.RequiredFeatures...)
					// если сервис уже включен, то его надо добавить сразу без валидации
					if serviceIsAlreadyOn(service.UUID) {
						dr.AvailableServices = append(dr.AvailableServices, service.Service)
						continue
					}
					// если водитель пытается назначить себе услугу, доступную только операторам
					if memType == structures.DriverMember && service.ForOperatorOnly {
						return errors.Errorf("Услугу %s может назначать только оператор", service.Name)
					}
					// if service.GetLevel() > dr.GetMaxServiceLevel() {
					// 	return errors.Errorf("Вам недоступен сервис %s", service.Name)
					// }
					dr.AvailableServices = append(dr.AvailableServices, service.Service)
				}
			}
		}
		for _, feature := range features {
			for _, featureTag := range feature.Tag {
				if featureTag == value {
					dr.AvailableFeatures = append(dr.AvailableFeatures, feature.Feature)
				}
			}
		}
	}
	featureIsRequired := func(fUUID string) bool {
		for _, uu := range reqFeatures {
			if fUUID == uu {
				return true
			}
		}
		return false
	}
	featureIsAlreadyAdded := func(fUUID string) bool {
		for _, featur := range dr.AvailableFeatures {
			if featur.UUID == fUUID {
				return true
			}
		}
		return false
	}
	for _, feature := range features {
		if featureIsRequired(feature.UUID) && !featureIsAlreadyAdded(feature.UUID) {
			dr.AvailableFeatures = append(dr.AvailableFeatures, feature.Feature)
			continue
		}

	}

	return nil
}
func (dr *DriverCRM) fillDefaultDriverTariff() error {
	var (
		driverTaxipark     TaxiParkCRM
		defaultDriverGroup DriverGroup
		defaultTariff      DriverTariffCrm
	)

	// получаем таксопарк к которому принадлежит водила
	if err := GetByUUID(*dr.TaxiParkUUID, &driverTaxipark); err != nil {
		return errors.Wrap(err, "taxipark")
	}
	// получаем дефолтную группу этого таксопарка
	if err := GetByUUID(driverTaxipark.DefaultDriverGroupUUID, &defaultDriverGroup); err != nil {
		return errors.Wrap(err, "group")
	}
	// получаем дефолтный тариф этой группы
	if err := GetByUUID(defaultDriverGroup.DefaultTariffUUID, &defaultTariff); err != nil {
		return errors.Wrap(err, "tariff")
	}
	dr.DrvTariff = structures.SelectedDriverTariff{
		DriverTariff: defaultTariff.DriverTariff,
		PayedAt:      dr.DrvTariff.PayedAt,
	}

	return nil
}

func (dr *DriverCRM) GetMaxServiceLevel() int {
	if dr.MaxServiceLevel == nil {
		return 0
	}
	return *dr.MaxServiceLevel
}

// SetDeleted godoc
func (dr *DriverCRM) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var driver DriverCRM
	err := CheckExistsUUID(&driver, dr.UUID)
	if err != nil {
		return err
	}
	dr.Deleted = true
	_, err = db.Model(dr).Where("uuid = ?uuid").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}
func FillDriversCreatedAtUnix(drivers ...DriverCRM) {
	for i := range drivers {
		drivers[i].CreatedAtUnix = drivers[i].CreatedAt.Unix()
	}
}

// UpdateTariffInDriversModel -
func UpdateTariffInDriversModel(tariff *DriverTariffCrm) ([]DriverCRM, error) {
	if tariff.UUID == "" {
		return nil, errpath.Errorf("empty tariff UUID")
	}

	var drivers []DriverCRM
	// _, err := db.ModelContext(ctx, &drivers). // из-за контекста len(drivers) = 0 (не всегда)
	// связанно из-за преждевременного завершения контекста в хендлере
	_, err := db.Model(&drivers).
		Where("tariff ->> 'uuid' = ?", tariff.UUID).
		Where("deleted is not true").
		Set("tariff = ?", *tariff).
		Returning("*").
		Update()
	if err != nil {
		return drivers, errpath.Err(err)
	}

	return drivers, nil
}

// GetDriversUUIDMapByState - Returns map where key is driverUUID and value is time when driver trans to this state
func GetDriversUUIDMapByState(allowedTaxiParks []string, state string) (map[string]time.Time, error) {
	drivers, err := GetDriversUUIDSliceByState(allowedTaxiParks, state)
	if err != nil {
		return nil, err
	}

	result := make(map[string]time.Time)
	for _, drv := range drivers {
		result[drv.UUID] = drv.StatusUpdateTime
	}
	return result, nil
}

// GetDriversUUIDSliceByState returns slice if drivers, having provided state
func GetDriversUUIDSliceByState(allowedTaxiParks []string, state string) ([]DriverCRM, error) {
	check := false
	for _, st := range constants.ListDriverStates() {
		if st == state {
			check = true
			break
		}
	}
	if !check {
		return nil, errors.Errorf("invalid state")
	}

	var drivers []DriverCRM
	query := db.Model(&drivers).Where("status = ?", state)

	if len(allowedTaxiParks) != 0 {
		query = query.
			WhereIn("taxi_park_uuid IN (?)", allowedTaxiParks)
	}

	err := query.
		Returning("uuid, status_update_time").
		Select()
	if err != nil {
		return nil, err
	}

	return drivers, nil
}

func GetDriverActualState(driverUUID string) (string, error) {
	var driver DriverCRM
	err := db.Model(&driver).
		Where("uuid = ?", driverUUID).
		Column("status").
		Select()
	return driver.Status, err
}

// GetDriverActualStateWithoutConsidering -
func GetDriverActualStateWithoutConsidering(driverUUID string) (string, error) {
	var driver DriverStatesCRM
	err := db.Model(&driver).
		Where("driver_uuid = ?", driverUUID).
		Where("state <> ?", constants.DriverStateConsidering).
		Column("state").
		Order("created_at desc").
		Limit(1).
		Select()
	if err != nil {
		return "", errpath.Err(err)
	}
	return driver.State, nil
}

func GetDriversActualStates(driverUUIDs []string) ([]DriverCRM, error) {
	var drivers []DriverCRM
	err := db.Model(&drivers).
		WhereIn("uuid IN (?)", driverUUIDs).
		Column("uuid", "status").
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to select drivers from db")
	}
	return drivers, nil
}

// DriversLocations - дергает локации водил
func DriversLocations(driverUUIDs []string) ([]structures.DriverCoordinates, error) {
	drvLocations := CurrentDriverLocationsByUUIDs(driverUUIDs)
	drvCoordinates := make([]structures.DriverCoordinates, 0, len(drvLocations))
	for _, locItem := range drvLocations {
		drvCoordinates = append(drvCoordinates, structures.DriverCoordinates{
			Coordinates: structures.Coordinates{
				DriverUUID: locItem.DriverUUID,
				Lat:        locItem.Latitude,
				Long:       locItem.Longitude,
			},
			CreatedAt: locItem.CreatedAt,
		})
	}
	return drvCoordinates, nil
}

// GetAllDrivers - Получает список всех водителей, выбирая тех, которые входят в allowedTaxiParks.
// Если allowedTaxiParks не передан - возвращает всех водителей, в том числе удалённых (deleted)
func GetAllDrivers(allowedTaxiParks ...string) ([]DriverCRM, error) {
	var drivers []DriverCRM
	query := db.Model(&drivers).
		Order("created_at")

	if len(allowedTaxiParks) != 0 {
		query = query.
			Where("deleted is not true").
			WhereIn("taxi_park_uuid IN (?)", allowedTaxiParks)
	}

	if err := query.Select(); err != nil {
		return nil, err
	}

	return drivers, nil
}

func UpdateDriverOrTaxiParkBalance(transfer structures.CompletedTransfer) error {
	if transfer.PayeeType != structures.UserTypeDriver &&
		transfer.PayerType != structures.UserTypeDriver &&
		transfer.PayeeType != structures.UserTypeTaxiPark &&
		transfer.PayerType != structures.UserTypeTaxiPark {
		return nil
	}

	if !(transfer.PayerAccountType == structures.AccountTypeCard ||
		transfer.PayerAccountType == structures.AccountTypeBonus ||
		transfer.PayeeAccountType == structures.AccountTypeCard ||
		transfer.PayeeAccountType == structures.AccountTypeBonus) {
		return nil
	}

	var err error
	if (transfer.PayeeType == structures.UserTypeDriver || transfer.PayeeType == structures.UserTypeTaxiPark) &&
		transfer.PayeeBalance != structures.BillingSkipUpdate {
		newBalance := structures.RoundUp(transfer.PayeeBalance)
		switch transfer.PayeeType {
		case structures.UserTypeDriver:
			err = setDriverBalance(transfer.PayeeUUID, transfer.PayeeAccountType, newBalance)
		case structures.UserTypeTaxiPark:
			err = setTaxiParkBalance(transfer.PayeeUUID, transfer.PayeeAccountType, newBalance)
		}
		if err != nil {
			return errors.Wrapf(err, "failed to update targets (%s) %s balance", transfer.PayeeType, transfer.PayeeUUID)
		}
	}

	if (transfer.PayerType == structures.UserTypeDriver || transfer.PayerType == structures.UserTypeTaxiPark) &&
		transfer.PayerBalance != structures.BillingSkipUpdate {
		newBalance := structures.RoundUp(transfer.PayerBalance)

		switch transfer.PayerType {
		case structures.UserTypeDriver:
			err = setDriverBalance(transfer.PayerUUID, transfer.PayerAccountType, newBalance)
		case structures.UserTypeTaxiPark:
			err = setTaxiParkBalance(transfer.PayerUUID, transfer.PayerAccountType, newBalance)
		}
		if err != nil {
			return errors.Wrapf(err, "failed to update targets (%s) %s balance", transfer.PayerType, transfer.PayerUUID)
		}
	}

	return nil
}

func setDriverBalance(drUUID string, accountType structures.BillingAccountType, newBalance float64) error {
	q := db.Model((*DriverCRM)(nil)).
		Where("uuid = ?", drUUID)
	if accountType == structures.AccountTypeCard {
		q = q.Set("card_balance = ?", newBalance)
	} else {
		q = q.Set("balance = ?", newBalance)
	}

	_, err := q.Update()
	return errors.Wrapf(err, "failed to update driver %v balance", newBalance)
}

func RemoveServiceFromAllDrivers(userUUID, serviceUUID string) (int, error) {
	var drivers []DriverCRM
	var service ServiceCRM
	err := GetByUUID(serviceUUID, &service)
	if err != nil {
		return 0, errors.Errorf("error getting service, %s", err)
	}
	err = db.Model(&drivers).
		Where("deleted is not true").
		Where("tag::text[]  && ?", pg.Array(service.Tag)).
		Select()
	driversLen := len(drivers)
	for _, dr := range drivers {
		var neededTags []string

		for _, tag := range dr.Tag {
			check := false
			for _, serTag := range service.Tag {
				if serTag == tag {
					check = true
				}
			}
			if !check {
				neededTags = append(neededTags, tag)
			}
		}
		dr.Tag = neededTags
		var neededServices []structures.Service
		for _, ser := range dr.AvailableServices {
			if ser.UUID != serviceUUID {
				neededServices = append(neededServices, ser)
			}
		}
		dr.AvailableServices = neededServices
		sendData := structures.WrapperDriver{
			OperatorUUID: userUUID,
			Driver:       dr.Driver,
		}
		_, err = db.Model(&dr).Where("uuid = ?", dr.UUID).Update()
		if err != nil {
			return driversLen, errors.Errorf("error update driver data (%s), %s", dr.UUID, err)
		}
		err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, sendData)
		if err != nil {
			return driversLen, errors.Errorf("error send driver data (%s) to broker, %s", dr.UUID, err)
		}
	}
	return driversLen, nil
}

// GetDefaultDriverTariffByGroup - returns the default tariff by group
func (dr *DriverCRM) GetDefaultDriverTariffByGroup() (DriverTariffCrm, error) {
	var drvTariff DriverTariffCrm

	if dr.Group.DefaultTariffUUID == "" {
		return DriverTariffCrm{}, errpath.Errorf("default tariff uuid is empty")
	}
	err := db.Model(&drvTariff).Where("uuid = ?", dr.Group.DefaultTariffUUID).Select()
	if err != nil {
		return DriverTariffCrm{}, errpath.Err(err)
	}
	return drvTariff, nil
}

// GetDefaultDriverOfflineTariffByGroup - returns the default offline tariff by group
func (dr *DriverCRM) GetDefaultDriverOfflineTariffByGroup() (DriverTariffCrm, error) {
	var drvTariff DriverTariffCrm

	if dr.Group.DefaultTariffOfflineUUID == "" {
		return DriverTariffCrm{}, errpath.Errorf("default offline tariff uuid is empty")
	}
	err := db.Model(&drvTariff).Where("uuid = ?", dr.Group.DefaultTariffOfflineUUID).Select()
	if err != nil {
		return DriverTariffCrm{}, errpath.Err(err)
	}
	return drvTariff, nil
}

func GetDriversUUIDsByAliases(ctx context.Context, aliases []int) (map[int]string, error) {
	if len(aliases) < 1 {
		return nil, nil
	}

	var drivers []DriverCRM
	err := db.ModelContext(ctx, &drivers).
		Column("uuid", "alias").
		WhereIn("alias IN (?)", aliases).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to fetch drivers uuids by aliases")
	}

	result := make(map[int]string)
	for _, drv := range drivers {
		result[drv.Alias] = drv.UUID
	}
	return result, nil
}

// ForbiddenDriverApps - (crm_config ключ - forbidden_driver_apps)
type ForbiddenDriverApps struct {
	Apps []structures.SmartphoneApp `json:"apps"`
}

// GetForbiddenDriverApps -
func GetForbiddenDriverApps(ctx context.Context) (ForbiddenDriverApps, error) {
	var apps ForbiddenDriverApps
	var conf RemoteConfig

	err := db.ModelContext(ctx, &conf).Where("key = ?", "forbidden_driver_apps").Select()
	if err != nil {
		return apps, errpath.Err(err)
	}
	data, err := json.Marshal(conf.Value)
	if err != nil {
		return apps, errpath.Err(err)
	}

	err = json.Unmarshal(data, &apps)
	if err != nil {
		return apps, errpath.Err(err)
	}

	return apps, nil
}

// SetForbiddenDriverApps -
func SetForbiddenDriverApps(ctx context.Context, apps ForbiddenDriverApps) error {

	_, err := db.ModelContext(ctx, (*RemoteConfig)(nil)).Where("key = ?", "forbidden_driver_apps").Set("value = ?", apps).Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// ---------
// ---------
// ---------

// ICounterOrderSwitch -
type ICounterOrderSwitch interface {
	GetCounterOrderSwitch(ctx context.Context, driverUUID string) (bool, error)
	SetCounterOrderSwitch(ctx context.Context, driverUUID string, switchState bool) (bool, error)
}

// CounterOrderSwitch -
type CounterOrderSwitch struct {
	db           *pg.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// InitCounterOrderSwitch -
func InitCounterOrderSwitch(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *CounterOrderSwitch {
	counterOrderSwitch := CounterOrderSwitch{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
	return &counterOrderSwitch
}

// GetCounterOrderSwitch -
func (c CounterOrderSwitch) GetCounterOrderSwitch(ctx context.Context, driverUUID string) (bool, error) {
	var drv DriverCRM
	var res bool

	{ // validate
		if driverUUID == "" {
			return res, errpath.Errorf("empty uuid")
		}
	}

	err := c.db.ModelContext(ctx, &drv).
		Where("uuid = ?", driverUUID).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	res = *drv.CounterOrderSwitch

	return res, nil
}

// SetCounterOrderSwitch -
func (c CounterOrderSwitch) SetCounterOrderSwitch(ctx context.Context, driverUUID string, switchState bool) (bool, error) {
	var drv DriverCRM
	var res bool

	{ // validate
		if driverUUID == "" {
			return res, errpath.Errorf("empty uuid")
		}
	}

	_, err := c.db.ModelContext(ctx, &drv).
		Where("uuid = ?", driverUUID).
		Set("counter_order_switch = ?", switchState).
		Returning("counter_order_switch").
		Update()
	if err != nil {
		return res, errpath.Err(err)
	}

	res = *drv.CounterOrderSwitch

	return res, nil
}

func sortByUnique(old []string) (new []string) {
	alreadyAdded := func(str string) bool {
		for _, item := range new {
			if item == str {
				return true
			}
		}
		return false
	}
	for _, item := range old {
		if alreadyAdded(item) {
			continue
		}
		new = append(new, item)
	}
	return
}

// ---------
// ---------
// ---------

func GetDriverActiveServices(driverUUID string) ([]string, error) {
	var serviceUUIDs []string
	query := db.Model((*RelationDriverService)(nil)).
		Column("service_uuid").
		Where("driver_uuid = ?", driverUUID)
	if err := query.Select(&serviceUUIDs); err != nil {
		return nil, err
	}

	return serviceUUIDs, nil
}

func GetDriverActiveFeatures(driverUUID string) ([]string, error) {
	var featureUUIDs []string
	query := db.Model((*RelationDriverFeature)(nil)).
		Column("feature_uuid").
		Where("driver_uuid = ?", driverUUID)
	if err := query.Select(&featureUUIDs); err != nil {
		return nil, err
	}

	return featureUUIDs, nil
}

func UpdateDriverLastCounterAssignment(driverUUID string) error {
	query := db.Model((*DriverCRM)(nil)).
		Where("uuid = ?", driverUUID).
		Set("last_counter_assignment = now()")
	if _, err := query.Update(); err != nil {
		return err
	}

	return nil
}
