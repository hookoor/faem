package models

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

type (
	StoreCRM struct {
		tableName struct{} `sql:"crm_stores" pg:",discard_unknown_columns"`
		structures.Store
		DestinationPoints     []structures.Route `json:"destination_points" sql:"-"`
		DestinationPointsUUID []string           `json:"destination_points_uuid" sql:",type:text[]"`
		CreatedAt             time.Time          `json:"-"`
		Deleted               bool               `json:"-"`
		CreatedAtUnix         int64              `json:"created_at_unix" sql:"-"`
	}
	StoreCriteria struct {
		Type     string          `json:"type"`
		Name     string          `json:"name"`
		Page     int             `json:"page"`
		Limit    int             `json:"limit"`
		Category string          `json:"category"`
		Pager    urlvalues.Pager `json:"-"`
	}
	ProductCRM struct {
		tableName struct{} `sql:"crm_products" pg:",discard_unknown_columns"`
		structures.Product
		CreatedAt     time.Time `json:"-"`
		Priority      int       `json:"priority"`
		Deleted       bool      `json:"-"`
		CreatedAtUnix int64     `json:"created_at_unix" sql:"-"`
	}
	ProductsCriteria struct {
		Category  structures.ProductCategory `json:"category"`
		Page      int                        `json:"page"`
		Limit     int                        `json:"limit"`
		StoreUUID string                     `json:"store_uuid"`
		Pager     urlvalues.Pager            `json:"-"`
	}
	ProductCategoriesCRM struct {
		tableName     struct{}                   `sql:"crm_product_categories" pg:",discard_unknown_columns"`
		UUID          string                     `json:"uuid"`
		Category      structures.ProductCategory `json:"category"`
		StoreUUID     string                     `json:"store_uuid,omitempty"`
		StoreType     string                     `json:"store_type"`
		CreatedAt     time.Time                  `json:"-"`
		Deleted       bool                       `json:"-"`
		CreatedAtUnix int64                      `json:"created_at_unix" sql:"-"`
	}
	StoreCategory struct {
		tableName   struct{}  `sql:"crm_store_categories" pg:",discard_unknown_columns"`
		UUID        string    `json:"uuid"`
		Name        string    `json:"name"`
		Url         string    `json:"url"`
		Priority    int       `json:"priority"`
		Description string    `json:"description"`
		Deleted     bool      `json:"deleted"`
		CreatedAt   time.Time `json:"created_at"`
	}
	StoreCategoryCriteria struct {
		Page     int             `json:"page"`
		Limit    int             `json:"limit"`
		Priority int             `json:"priority"`
		Pager    urlvalues.Pager `json:"-"`
	}
)

func (ec *StoreCriteria) GetStores(takeStoreVisible bool) (res []StoreCRM, count int, err error) {
	ec.Pager, err = validateAndFillPager(ec.Page, ec.Limit)
	if err != nil {
		return
	}
	query := db.Model(&res).
		Where("deleted is not true").
		Order("serial_number asc", "created_at desc")
	if ec.Name != "" {
		query.Where("name ILIKE ?", "%"+ec.Name+"%")
	}
	if ec.Type != "" {
		query.Where("type = ?", ec.Type)
	}
	if ec.Category != "" {
		query.Where("(?) <@ categories", pg.Array([]string{ec.Category}))
	}
	if takeStoreVisible {
		query.Where("visible")
	}
	count, err = query.Count()
	if err != nil || count == 0 {
		return
	}
	err = query.
		Apply(ec.Pager.Pagination).
		Select()
	if err != nil {
		return
	}
	res, err = fillDestPoints(res)
	return
}
func (ec *StoreCategoryCriteria) GetCategories() (res []StoreCategory, count int, err error) {
	ec.Pager, err = validateAndFillPager(ec.Page, ec.Limit)
	if err != nil {
		return
	}
	query := db.Model(&res).
		Where("deleted is not true").
		Order("priority asc")
	count, err = query.Count()
	if err != nil || count == 0 {
		return
	}
	err = query.
		Apply(ec.Pager.Pagination).
		Select()
	if err != nil {
		return
	}
	return
}

func fillDestPoints(stores []StoreCRM) ([]StoreCRM, error) {
	var neededUUIDs []string
	for _, val := range stores {
		neededUUIDs = append(neededUUIDs, val.DestinationPointsUUID...)
	}

	if len(neededUUIDs) == 0 {
		return stores, nil
	}
	points, err := GetDestinationPointsByUUID(neededUUIDs)
	if err != nil {
		return stores, errors.Errorf("error getting dest points by uuid,%s", err)
	}
	getPoint := func(uuid string) (DestinationPoint, bool) {
		for _, point := range points {
			if uuid == point.UUID {
				return point, true
			}
		}
		return DestinationPoint{}, false
	}
	for i := range stores {
		for _, destUUID := range stores[i].DestinationPointsUUID {
			point, exists := getPoint(destUUID)
			if !exists {
				continue
			}
			stores[i].DestinationPoints = append(stores[i].DestinationPoints, point.Route)
		}
	}
	return stores, nil
}

func (cr *ProductsCriteria) GetProducts() (res []ProductCRM, count int, err error) {
	cr.Pager, err = validateAndFillPager(cr.Page, cr.Limit)
	if err != nil {
		return
	}

	query := db.Model(&res).
		Where("deleted is not true").
		Order("priority desc")
	if cr.Category != "" {
		query.Where("category = ?", cr.Category)
	}
	if cr.StoreUUID != "" {
		query.Where("store_uuid = ?", cr.StoreUUID)
	}
	count, err = query.Count()
	if err != nil || count == 0 {
		return
	}
	err = query.
		Apply(cr.Pager.Pagination).
		Select()
	return
}

func validateAndFillPager(page, limit int) (urlvalues.Pager, error) {
	var res urlvalues.Pager
	if page == 0 || limit == 0 {
		return res, errors.Errorf("empty pager params")
	}
	res.Limit = limit
	res.SetPage(page)
	return res, nil
}

// getVariantByUUID return needed or standard variant
func (pr *ProductCRM) getVariantByUUID(uuid string) *structures.ProductVariant {
	if pr == nil || len(pr.Variants) == 0 {
		return nil
	}
	standardVariant := pr.Variants[0]
	for _, vart := range pr.Variants {
		if vart.Standard {
			standardVariant = vart
		}
		if uuid == vart.UUID {
			return &vart
		}
	}
	return &standardVariant
}

// getToppingsByUUID return needed toppings
func (pr *ProductCRM) getToppingsByUUID(uuids []string) []structures.Topping {
	if pr == nil || len(pr.Toppings) == 0 {
		return nil
	}
	uuids = sortByUnique(uuids)
	var res []structures.Topping
	for _, top := range pr.Toppings {
		for _, uuid := range uuids {
			if top.UUID == uuid {
				res = append(res, top)
				break
			}
		}
	}
	return res
}
func GetProductsByUUID(uuids []string) ([]ProductCRM, error) {
	if len(uuids) == 0 {
		return nil, nil
	}
	var res []ProductCRM
	err := db.Model(&res).
		WhereIn("uuid in (?)", uuids).
		Where("deleted is not true").
		Order("created_at desc").
		Select()
	return res, err
}
func GetProductByUUID(uuid string) (ProductCRM, error) {
	var res ProductCRM
	err := db.Model(&res).
		Where("uuid = ?", uuid).
		Where("deleted is not true").
		Select()
	return res, err
}

// UpdateImage -
func (pr *ProductCRM) UpdateImage(uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}
	pr.UUID = uuid

	_, err := db.Model(pr).
		Where("uuid = ?", uuid).
		Set("image = ?", pr.Image).
		Update()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

func SetProductDeleted(uuid string) (string, error) {
	var prod ProductCRM
	_, err := db.Model(&prod).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Returning("*").
		Update()
	return prod.StoreUUID, err
}
func SetStoreDeleted(uuid string) error {
	_, err := db.Model(&StoreCRM{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}
func ValidateAndGetProductsByInputData(data []structures.ProductInputData) (*structures.ProductsDataInOrder, error) {
	if len(data) == 0 {
		return nil, nil
	}
	var neededProdUUID []string
	variantsUUID := make(map[string]string)
	toppingsUUID := make(map[string][]string)
	productsNumber := make(map[string]int)
	for _, item := range data {
		if item.UUID == "" {
			return nil, errors.Errorf("empty uuid in item")
		}
		if item.Number < 1 {
			return nil, errors.Errorf("invalid number field for product (%s)", item.UUID)
		}
		variantsUUID[item.UUID] = item.VariantUUID
		toppingsUUID[item.UUID] = item.ToppingsUUID
		productsNumber[item.UUID] = item.Number
		neededProdUUID = append(neededProdUUID, item.UUID)
	}
	products, err := GetProductsByUUID(neededProdUUID)
	if err != nil || len(products) == 0 {
		return nil, errors.Errorf("error getting products by uuid,%s", err)
	}
	var res structures.ProductsDataInOrder
	validStoreUUID := products[0].StoreUUID
	for _, prod := range products {
		if prod.StoreUUID != validStoreUUID {
			return nil, errors.Errorf("Нельзя заказывать товары из разных заведений")
		}
		redData := structures.ProductReadyData{
			ProductCommonData: prod.ProductCommonData,
		}
		if len(prod.Variants) != 0 {
			redData.SelectedVariant = *prod.getVariantByUUID(variantsUUID[prod.UUID])
		}
		if len(prod.Toppings) != 0 {
			redData.Toppings = prod.getToppingsByUUID(toppingsUUID[prod.UUID])
		}
		redData.Number = productsNumber[prod.UUID]
		res.Pruducts = append(res.Pruducts, redData)
	}
	store, err := GetStoreByUUID(validStoreUUID)
	res.StoreData = store.Store
	//TODO:  навесить на это логику
	res.Buyout = true
	res.PreparationTime = store.GetOrderPreparationTimeSecond()
	return &res, nil
}

func (pc *ProductCategoriesCRM) Create() *structures.ErrorWithLevel {
	err := pc.fillStoreType()
	if err != nil {
		return structures.Error(errors.Errorf("error filling store type, %s", err))
	}
	errWL := pc.validCreate()
	if errWL != nil {
		return errWL
	}
	_, err = db.Model(pc).Insert()
	pc.CreatedAtUnix = pc.CreatedAt.Unix()
	return structures.Error(err)
}
func SetProductCategoryDeleted(uuid string) error {
	_, err := db.Model(&ProductCategoriesCRM{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}

// RebuildProductCategoryIfNeed проходится по продуктам заведения и добавляет в него отсутствующие категории или удаляет ненужные
func RebuildProductCategoryIfNeed(storeUUID string) error {
	if storeUUID == "" {
		return nil
	}
	store, err := GetStoreByUUID(storeUUID)
	if err != nil {
		return errors.Errorf("error getting store, %s", err)
	}
	actualCategories, err := GetCategoriesFromProducts(storeUUID)
	if err != nil {
		return errors.Errorf("error categories from product, %s", err)
	}
	store.ProductCategories = selectNeededCategories(store.ProductCategories, actualCategories)

	_, err = db.Model(&store).
		Where("uuid = ?", storeUUID).
		Set("product_categories = ?product_categories").
		Update()
	return err
}
func formatOne(in structures.ProductCategory) structures.ProductCategory {
	return structures.ProductCategory(strings.ToLower(string(in)))
}
func format(in []structures.ProductCategory) []structures.ProductCategory {
	for i := range in {
		lower := strings.ToLower(string(in[i]))
		in[i] = structures.ProductCategory(lower)
	}
	return in
}
func selectNeededCategories(storeCat, actualCat []structures.ProductCategory) []structures.ProductCategory {
	var needed, missing []structures.ProductCategory
	storeCat, actualCat = format(storeCat), format(actualCat)
	//those who already are
	for _, cat := range storeCat {

		checkContains := false
		for _, acCat := range actualCat {
			if acCat == cat {
				checkContains = true
				break
			}
		}
		if checkContains {
			needed = append(needed, cat)
		}
	}
	// which are missing
	for _, acCat := range actualCat {

		checkContains := false
		for _, cat := range needed {
			if acCat == cat {
				checkContains = true
				break
			}
		}
		if !checkContains {
			missing = append(missing, acCat)
		}
	}
	needed = append(needed, missing...)
	return needed
}
func GetCategoriesFromProducts(storeUUID string) ([]structures.ProductCategory, error) {
	var categories []structures.ProductCategory
	err := db.Model(&ProductCRM{}).
		ColumnExpr("DISTINCT ON (category) category").
		Where("store_uuid = ?", storeUUID).
		Where("deleted is not true").
		Order("category").
		Select(&categories)
	return categories, err
}

func (pc *ProductCategoriesCRM) validCreate() *structures.ErrorWithLevel {
	pc.Category = structures.ProductCategory(strings.ToLower(string(pc.Category)))
	if pc.Category == "" {
		return structures.Warning(errors.Errorf("Введите категорию"))
	}
	if pc.StoreType == "" {
		return structures.Warning(errors.Errorf("Введите тип заведений"))
	}
	check, err := pc.checkExistsProdCategory()
	if err != nil {
		return structures.Error(errors.Errorf("error checking exists prod category, %s", err))
	}
	if check {
		return structures.Warning(errors.Errorf("Такая категория уже существует"))
	}
	return nil
}
func (pc *ProductCategoriesCRM) fillStoreType() (err error) {
	if pc.StoreUUID == "" {
		return
	}
	pc.StoreType, err = getStoreTypeByUUID(pc.StoreUUID)
	return
}
func (pc *ProductCategoriesCRM) checkExistsProdCategory() (bool, error) {
	return db.Model(pc).
		Where("category = ?category").
		Where("store_type = ?store_type").
		Where("deleted is not true").
		Exists()
}
func GetAllProductCategories() ([]ProductCategoriesCRM, error) {
	var res []ProductCategoriesCRM
	err := db.Model(&res).
		Where("deleted is not true").
		Order("category").
		Select()
	for i := range res {
		res[i].CreatedAtUnix = res[i].CreatedAt.Unix()
	}
	return res, err
}
func GetAllProductCategoriesByStoreUUID(storeUUID string) ([]structures.ProductCategory, error) {
	//если res задать как []structures.ProductCategory, go pg возвращает одну строку, в которой склеены все элементы массива product_categories в базе.
	var res StoreCRM
	err := db.Model(&res).
		Where("uuid = ?", storeUUID).
		Where("deleted is not true").
		Column("product_categories").
		Select()

	return res.ProductCategories, err
}
func getStoreTypeByUUID(storeUUID string) (string, error) {
	var res string
	err := db.Model(&StoreCRM{}).
		Where("uuid = ?", storeUUID).
		Where("deleted is not true").
		Column("type").
		Select(&res)
	if err != nil {
		return res, errors.Errorf("error getting store from db, %s", err)
	}
	return res, nil
}
func GetStoreByUUID(uuid string) (StoreCRM, error) {
	var store StoreCRM
	err := db.Model(&store).
		Where("deleted is not true").
		Where("uuid = ?", uuid).
		Select()
	if err != nil {
		return store, errors.Errorf("error getting store from db, %s", err)
	}
	storeFillData, err := fillDestPoints([]StoreCRM{store})
	if err != nil || len(storeFillData) == 0 {
		return store, errors.Errorf("error fill dest points,%s", err)
	}
	store = storeFillData[0]
	store.CreatedAtUnix = store.CreatedAt.Unix()
	return store, nil
}
func GetStoreByURL(url string) (StoreCRM, error) {
	var store StoreCRM
	err := db.Model(&store).
		Where("deleted is not true").
		Where("url = ?", url).
		Select()
	if err != nil {
		return store, errors.Errorf("error getting store from db, %s", err)
	}
	storeFillData, err := fillDestPoints([]StoreCRM{store})
	if err != nil || len(storeFillData) == 0 {
		return store, errors.Errorf("error fill dest points,%s", err)
	}
	store = storeFillData[0]
	store.CreatedAtUnix = store.CreatedAt.Unix()
	return store, nil
}
func (st *StoreCRM) Create() *structures.ErrorWithLevel {
	errWL := st.validForCreate()
	if errWL != nil {
		return errWL
	}
	st.UUID = structures.GenerateUUID()
	st.CreatedAt = time.Now()
	_, err := db.Model(st).
		Returning("*").
		Insert()
	return structures.Error(err)
}

func (st *StoreCRM) Update(uuid string) *structures.ErrorWithLevel {
	st.UUID = uuid

	errWL := st.validForUpdate()
	if errWL != nil {
		return errWL
	}
	_, err := db.Model(st).
		Where("uuid = ?", uuid).
		Returning("*").
		UpdateNotNull()
	return structures.Error(err)
}

// StoreSerialPriority -
type StoreSerialPriority struct {
	UUID         string `json:"uuid"`
	SerialNumber int    `json:"serial_number"`
}

// UpdateSerialNumbers -
func (StoreCRM) UpdateSerialNumbers(ctx context.Context, storeSerialPriority []StoreSerialPriority) error {
	var err error
	var queryString string

	queryString += `
		CREATE TEMPORARY TABLE serial_numbers (
			uuid text,
			new_comment int
		);
		`

	queryString += "insert into serial_numbers values\n"
	for i, sp := range storeSerialPriority {
		if i+1 == len(storeSerialPriority) {
			queryString += fmt.Sprintf("('%s',%v);\n", sp.UUID, sp.SerialNumber)
			break
		}
		queryString += fmt.Sprintf("('%s',%v),\n", sp.UUID, sp.SerialNumber)
	}

	queryString += `
		UPDATE crm_stores as st SET serial_number = (select new_comment from serial_numbers where serial_numbers.uuid = st.uuid)
		where (select new_comment from serial_numbers where serial_numbers.uuid = st.uuid) is not null;`
	queryString += `
		drop table serial_numbers;`

	_, err = db.Query(nil, queryString)
	if err != nil {
		return errpath.Err(err)
	}

	return err
}

// UpdateImage -
func (st *StoreCRM) UpdateImage(uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}
	st.UUID = uuid

	_, err := db.Model(st).
		Where("uuid = ?", uuid).
		Set("image = ?", st.Image).
		Update()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}
func (st *StoreCRM) validRoutes() *structures.ErrorWithLevel {
	destPoints, err := GetDestinationPointsByUUID(st.DestinationPointsUUID)
	if err != nil {
		return structures.Error(errors.Errorf("error getting dest points from db, %s", err))
	}
	if len(st.DestinationPointsUUID) != len(destPoints) {
		return structures.Warning(errors.Errorf("invalid dest points uuid"))
	}
	return nil
}
func (st *StoreCRM) validForUpdate() *structures.ErrorWithLevel {
	if len(st.DestinationPointsUUID) == 0 {
		errWL := st.validRoutes()
		if errWL != nil {
			return errWL
		}
	}
	// if st.Type != "" && !st.validType() {
	// 	return structures.Warning(errors.Errorf("Некорректный тип заведения"))
	// }
	if st.WorkSchedule != nil {
		day, check := st.validWorkShedule()
		if !check {
			return structures.Warning(errors.Errorf("Некорректный график работы для %v дня", day))
		}
	}
	if st.GetOrderPreparationTimeSecond() < 0 {
		return structures.Warning(errors.Errorf("Среднее время подготовки заказа не может быть меньше нуля"))
	}
	return st.validCategories()
}
func (pr *ProductCRM) Create() *structures.ErrorWithLevel {
	errWL := pr.validForCreate()
	if errWL != nil {
		return errWL
	}

	pr.CreatedAt = time.Now()
	pr.UUID = structures.GenerateUUID()
	_, err := db.Model(pr).
		Returning("*").
		Insert()
	if err != nil {
		return structures.Error(errors.Errorf("error insert data, %s", err))
	}
	// err = updateStoreProductsCategoriesIfNeed(pr.StoreUUID)
	// if err != nil {
	// 	return structures.Error(errors.Errorf("error updating store prod categories, %s", err))
	// }
	return nil
}
func (pr *ProductCRM) Update(uuid string) *structures.ErrorWithLevel {
	oldProd, err := GetProductByUUID(uuid)
	if err != nil {
		return structures.Error(errors.Errorf("error getting product by uuid, %s", err))
	}
	pr.StoreUUID = oldProd.StoreUUID
	errWL := pr.validForUpdate(uuid)
	if errWL != nil {
		return errWL
	}

	_, err = db.Model(pr).
		Where("uuid = ?", uuid).
		Returning("*").
		UpdateNotNull()
	if err != nil {
		return structures.Error(errors.Errorf("error updating data, %s", err))
	}
	// err = updateStoreProductsCategoriesIfNeed(pr.StoreUUID)
	// if err != nil {
	// 	return structures.Error(errors.Errorf("error updating store prod categories, %s", err))
	// }
	return nil
}
func (pr *ProductCRM) validForCreate() *structures.ErrorWithLevel {
	if pr.Name == "" {
		return structures.Warning(errors.Errorf("Укажите название товара"))
	}
	if pr.Price <= 0 {
		return structures.Warning(errors.Errorf("Некорректная цена товара"))
	}
	if pr.StoreUUID == "" {
		return structures.Warning(errors.Errorf("Укажите заведение"))
	}
	if pr.Category == "" {
		return structures.Warning(errors.Errorf("Укажите категорию товара"))
	}
	pr.Category = formatOne(pr.Category)

	err := pr.validToppings()
	if err != nil {
		return structures.Warning(err)
	}
	err = pr.validVariants()
	if err != nil {
		return structures.Warning(err)
	}
	errWL := checkProductExistsByName(pr.StoreUUID, pr.Name, "")
	return errWL
}
func (pr *ProductCRM) validForUpdate(uuid string) *structures.ErrorWithLevel {
	if pr.Name != "" {
		errWL := checkProductExistsByName(pr.StoreUUID, pr.Name, uuid)
		if errWL != nil {
			return errWL
		}
	}
	if pr.Price < 0 {
		return structures.Warning(errors.Errorf("Некорректная цена товара"))
	}
	pr.Category = formatOne(pr.Category)

	err := pr.validToppings()
	if err != nil {
		return structures.Warning(err)
	}
	err = pr.validVariants()
	if err != nil {
		return structures.Warning(err)
	}
	return nil
}
func (pr *ProductCRM) validToppings() error {
	for i, top := range pr.Toppings {
		if top.Name == "" {
			return errors.Errorf("Введите названия для всех топпингов")
		}
		pr.Toppings[i].UUID = structures.GenerateUUID()
	}
	return nil
}
func (pr *ProductCRM) validVariants() error {
	if len(pr.Variants) == 0 {
		return nil
	}
	checkStandardVar := false
	for i, top := range pr.Variants {
		if top.Name == "" {
			return errors.Errorf("Введите названия для всех вариантов товара")
		}
		if top.Standard {
			checkStandardVar = true
		}
		pr.Variants[i].UUID = structures.GenerateUUID()
	}
	if len(pr.Variants) == 1 {
		pr.Variants[0].Standard = true
		checkStandardVar = true
	}
	if !checkStandardVar {
		return errors.Errorf("Укажите один вариант как стандартный")
	}
	return nil
}
func updateStoreProductsCategoriesIfNeed(storeUUID string) error {
	var products []ProductCRM
	var storeCategories []structures.ProductCategory
	err := db.Model(&products).
		ColumnExpr("DISTINCT ON (category) category").
		Where("store_uuid = ?", storeUUID).
		Where("deleted is not true").
		Select(&storeCategories)
	if err != nil {
		return errors.Errorf("error getting all categories by store, %s", err)
	}
	_, err = db.Model(&StoreCRM{}).
		Where("uuid = ?", storeUUID).
		Set("product_categories = ?", pg.Array(storeCategories)).
		Update()
	return err
}

func checkProductExistsByName(storeUUID, productName, uuid string) *structures.ErrorWithLevel {
	check, err := db.Model(&StoreCRM{}).
		Where("uuid = ?", storeUUID).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return structures.Error(errors.Errorf("error getting store from db by uuid, %s", err))
	}
	if !check {
		return structures.Warning(errors.Errorf("store with uuid (%s) not found", storeUUID))
	}
	check, err = db.Model(&ProductCRM{}).
		Where("store_uuid = ?", storeUUID).
		Where("name = ?", productName).
		Where("uuid != ?", uuid).
		Where("deleted is not true").
		Exists()
	if err != nil {
		return structures.Error(errors.Errorf("error getting product from db by uuid, %s", err))
	}
	if check {
		return structures.Warning(errors.Errorf("В этом заведении уже есть товар с таким названием"))
	}
	return nil
}
func (st *StoreCRM) validForCreate() *structures.ErrorWithLevel {
	if len(st.DestinationPointsUUID) == 0 {
		return structures.Warning(errors.Errorf("Укажите возможные адреса для заведения"))
	}
	if st.Name == "" {
		return structures.Warning(errors.Errorf("Укажите название заведения"))
	}
	if st.Phone == "" {
		return structures.Warning(errors.Errorf("Укажите номер телефона заведения"))
	}
	if st.Type == "" {
		return structures.Warningf("Укажите тип заведения")
	}
	day, check := st.validWorkShedule()
	if !check {
		return structures.Warning(errors.Errorf("Некорректный график работы для %v дня", day))
	}
	if st.GetOrderPreparationTimeSecond() < 0 {
		return structures.Warning(errors.Errorf("Среднее время подготовки заказа не может быть меньше нуля"))
	}
	errWL := st.validRoutes()
	return errWL
}

func (st *StoreCRM) validCategories() *structures.ErrorWithLevel {
	if st.ProductCategories == nil {
		return nil
	}
	cats, err := GetCategoriesFromProducts(st.UUID)
	if err != nil {
		return structures.Errorf("error getting categories from product")
	}
	st.ProductCategories = format(st.ProductCategories)
	for _, val := range cats {
		check := false

		for _, val2 := range st.ProductCategories {
			if val2 == val {
				check = true
				break
			}
		}
		if !check {
			return structures.Warningf("Отсутствует необходимая категория '%s'", val)
		}
	}
	return nil
}

// func (st *StoreCRM) validType() bool {
// 	switch st.Type {
// 	case structures.Restaurant,
// 		structures.Shop,
// 		structures.Pharmacy:
// 		return true
// 	default:
// 		return false
// 	}
// }

func (st *StoreCRM) validWorkShedule() (int, bool) {
	validData := make(map[int]bool)
	for i := 1; i <= 7; i++ {
		validData[i] = false
	}
	for _, item := range st.WorkSchedule {
		workMinutes := item.WorkEnding - item.WorkBeginning
		checkMinusElement := item.WorkEnding < 0 || item.WorkBeginning < 0
		if ((workMinutes <= 0 && !item.IsWorkRoundTheClock()) || checkMinusElement) && !item.DayOff {
			continue
		}
		validData[item.WeekDay] = true
	}
	for key, value := range validData {
		if !value {
			return key, value
		}
	}
	return 0, true
}

//
func (sc *StoreCategory) Create() *structures.ErrorWithLevel {
	errWL := sc.validForCreate()
	if errWL != nil {
		return errWL
	}
	sc.UUID = structures.GenerateUUID()
	sc.CreatedAt = time.Now()
	_, err := db.Model(sc).
		Returning("*").
		Insert()
	return structures.Error(err)
}

func (sc *StoreCategory) validForCreate() *structures.ErrorWithLevel {
	if sc.Name == "" {
		return structures.Warning(errors.Errorf("Укажите имя для категории"))
	}
	if sc.Url == "" {
		return structures.Warning(errors.Errorf("Укажите URL для категории"))
	}
	return nil
}
