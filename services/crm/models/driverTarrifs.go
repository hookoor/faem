package models

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/pkg/errors"

	"github.com/antonmedv/expr"
	"github.com/go-pg/pg/orm"
	"github.com/gofrs/uuid"

	"gitlab.com/faemproject/backend/faem/pkg/jobqueue"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
)

const (
	maxParallelJobsAllowed   = 97
	validMapMarkupOrderPrice = 100
	msgBuyTariff             = "Покупка тарифа: %s"
	msgBuyTariffTaxiPark     = "Списание таксопарк за покупку тарифа: %s"
)

var (
	tariffJobs = jobqueue.NewJobQueue(maxParallelJobsAllowed)
)

//DriverTariffCrm тарифы водителей
type DriverTariffCrm struct {
	tableName struct{} `sql:"crm_driver_tariff" pg:",discard_unknown_columns"`
	structures.DriverTariff
	СommissionExpTemp      FormulaTemplateCRM       `json:"comm_expr_temp" sql:"comm_expr_temp"` // значения переменных для формулы СommissionExp
	RejectionExpTemp       FormulaTemplateCRM       `json:"rej_expr_temp" sql:"rej_expr_temp"`   // значения переменных для формулы RejectionExp
	DriversGroupsData      []DriversGroupsShortData `json:"driver_groups_data" sql:"-"`
	MaxCompleteCommission  int                      `json:"max_complete_commission"`
	MaxPeriodCommission    int                      `json:"max_period_commission"`
	MaxRejectionCommission int                      `json:"max_rejection_commission"`
	Deleted                bool                     `json:"-" sql:"default:false"`
	CreatedAt              time.Time                `json:"-"`
	CreatedAtUnix          int64                    `json:"created_at_unix" sql:"-"`
}
type DriversTariffCashe struct {
	m  map[string]DriverTariffCrm
	mx sync.RWMutex
}

var currentDriverTariffs = NewDriverTariffArr()

func NewDriverTariffArr() *DriversTariffCashe {
	return &DriversTariffCashe{m: make(map[string]DriverTariffCrm)}
}
func InitCurrentDriverTariffs() error {
	tpUUIDs := make([]string, 0)
	query := db.Model((*TaxiParkCRM)(nil)).Column("uuid").Where("deleted IS NOT TRUE")
	if err := query.Select(&tpUUIDs); err != nil {
		return err
	}

	tp, err := DriverTariffsList("", "", tpUUIDs)
	if err != nil {
		return errors.Errorf("failed to load driver tariffs, %s", err)
	}

	for _, t := range tp {
		currentDriverTariffs.Store(t)
	}
	return nil
}
func (drvTF *DriversTariffCashe) Get(uuid string) DriverTariffCrm {
	drvTF.mx.RLock()
	defer drvTF.mx.RUnlock()

	val := drvTF.m[uuid]
	return val
}

func (drvTF *DriversTariffCashe) Store(data DriverTariffCrm) {
	drvTF.mx.Lock()
	defer drvTF.mx.Unlock()

	drvTF.m[data.UUID] = data
}

type DriversGroupsShortData struct {
	UUID string `json:"uuid"`
	Name string `json:"name"`
}

// Create создает новый тариф для водителя
func (drvTF DriverTariffCrm) Create() error {
	uu, err := uuid.NewV4()
	if err != nil {
		return err
	}
	drvTF.UUID = uu.String()

	drvTF.CreatedAt = time.Now()
	drvTF.Markups = nil //они задаются отдельно

	if err := drvTF.prepare(false); err != nil {
		return errors.Errorf("Ошибка валидации %s", err)
	}

	if _, err = db.Model(&drvTF).Insert(); err != nil {
		return err
	}

	if err := InitCurrentDriverTariffs(); err != nil {
		return err
	}

	return nil
}

// DriverTariffsList возвращает список всех водительских тарифов
func DriverTariffsList(memType structures.ChatMembers, neededGroupUUID string, userTaxiParks []string) ([]DriverTariffCrm, error) {
	regQuery := db.Model((*RegionCRM)(nil)).Column("region_crm.id").
		Join("JOIN crm_taxi_parks as tp ON region_crm.id = tp.region_id").
		WhereIn("tp.uuid IN (?)", userTaxiParks)

	drvTFS := make([]DriverTariffCrm, 0)
	query := db.Model(&drvTFS).Where("deleted IS NOT TRUE").Where("region_id IN (?)", regQuery)
	if neededGroupUUID != "" {
		query.Where("drivers_groups_uuid && ARRAY[?]", neededGroupUUID)
	}
	if memType == structures.DriverMember {
		query.Where("is_secret IS NOT TRUE")
	}

	if err := query.Select(); err != nil {
		return nil, errpath.Err(err)
	}

	for i := range drvTFS {
		drvTFS[i].FilterMarkupsByTaxiPark(userTaxiParks)
	}

	return drvTFS, nil
}

func (dr *DriverTariffCrm) filterMarkups(taxiParksUUID []string) {
	if len(taxiParksUUID) < 1 {
		return
	}
	var neededMarkups structures.TariffMarkupsArray
	for _, mar := range dr.Markups {
		if structures.StringInArray(mar.TaxiParkUUID, taxiParksUUID) {
			continue
		}
		neededMarkups = append(neededMarkups, mar)
	}
	dr.Markups = neededMarkups
}

// DriverTariffFilter - критерии по которым будут фильтроваться тарифы
type DriverTariffFilter struct {
	DriverGroupsUUIDs   []string `query:"driver_groups_uuids"`
	MarkupsTaxiParkUUID []string
	RegionIDs           []int  `query:"-"`
	Name                string `query:"name"`
	Offline             *bool  `query:"offline"`
	Default             *bool  `query:"default"`
}

// DriverTariffsListByFilter - возвращает список всех водительских тарифов по фильтру
func (DriverTariffCrm) DriverTariffsListByFilter(ctx context.Context, filter *DriverTariffFilter, memType structures.ChatMembers) ([]DriverTariffCrm, error) {
	drvTFS := make([]DriverTariffCrm, 0)
	q := db.ModelContext(ctx, &drvTFS).
		Where("deleted is not true").
		WhereIn("region_id IN (?)", filter.RegionIDs)

	if filter.Name != "" {
		q.Where("name ILIKE ?", filter.Name+"%")
	}
	if filter.Offline != nil {
		q.Where("offline is ?", filter.Offline)
	}
	if filter.Default != nil {
		q.Where("\"default\" is ?", filter.Default)
	}
	if memType == structures.DriverMember {
		q.Where("is_secret IS NOT TRUE")
	}

	// получение записи если в колонке записи (типа text[]) есть хоть один из искомых uuid
	if len(filter.DriverGroupsUUIDs) != 0 {
		q.WhereGroup(func(query *orm.Query) (*orm.Query, error) {
			for _, uuid := range filter.DriverGroupsUUIDs {
				query.WhereOr("? = ANY (drivers_groups_uuid)", uuid)
			}
			return query, nil
		})
	}

	if err := q.Select(); err != nil {
		return nil, errpath.Err(err)
	}

	for i := range drvTFS {
		drvTFS[i].FilterMarkupsByTaxiPark(filter.MarkupsTaxiParkUUID)
	}

	return drvTFS, nil
}

//SetDeleted удаляет тариф
func (drvTF DriverTariffCrm) SetDeleted() error {
	err := validDriverTariffDelete(drvTF.UUID)
	if err != nil {
		return err
	}
	_, err = db.Model(&drvTF).Where("uuid = ?uuid").Set("deleted = true").
		Update()
	if err != nil {
		return err
	}
	return InitCurrentDriverTariffs()
}

func validDriverTariffDelete(uuid string) error {
	var drvTF DriverTariffCrm
	err := db.Model(&drvTF).Where("uuid = ?", uuid).
		Select()
	if err != nil {
		return err
	}
	if drvTF.Default {
		return errors.Errorf("Для удаления пометьте другой тариф как стандартный")
	}
	if drvTF.Offline {
		return errors.Errorf("Для удаления пометьте другой тариф как оффлайновый")
	}
	return nil
}

// GetOfflineDriverTariff returns tariff for orders which accepted drivers from offline state
func GetOfflineDriverTariff() (DriverTariffCrm, error) {
	var drvTF DriverTariffCrm
	check, err := db.Model(&drvTF).Where("offline = true").
		Exists()
	if err != nil {
		return drvTF, err
	}
	if !check {
		return drvTF, nil
	}
	err = db.Model(&drvTF).Where("offline = true").
		Select()
	return drvTF, err
}

// GetDriverTariffMarkupsByUUID
func GetDriverTariffMarkupsByUUID(uuid string) ([]structures.TariffMarkup, error) {
	return currentDriverTariffs.Get(uuid).Markups, nil
}

func GetDriverTariffByUUID(uuid string) (DriverTariffCrm, error) {
	return currentDriverTariffs.Get(uuid), nil
}

// GetManyDriverTariffMarkupsByUUID
func GetManyDriverTariffMarkupsByUUID(uuids []string) (map[string][]structures.TariffMarkup, error) {
	res := make(map[string][]structures.TariffMarkup)
	for _, val := range uuids {
		res[val] = currentDriverTariffs.Get(val).Markups
	}
	return res, nil
}

// FillDriverGroupData
func FillDriverGroupData(tariffs []DriverTariffCrm) ([]DriverTariffCrm, error) {
	var neededUUID []string
	for _, gr := range tariffs {
		neededUUID = append(neededUUID, gr.DriversGroupsUUID...)
	}
	groups, err := GetDriversGroupsByUUID(neededUUID)
	if err != nil {
		return tariffs, errpath.Err(err, "error getting grous")
	}
	for i := range tariffs {
		for _, group := range groups {
			if structures.StringInArray(group.UUID, tariffs[i].DriversGroupsUUID) {
				tariffs[i].DriversGroupsData = append(tariffs[i].DriversGroupsData, DriversGroupsShortData{
					UUID: group.UUID,
					Name: group.Name,
				})
			}
		}
	}
	return tariffs, nil
}
func FillUnixFields(arr []DriverTariffCrm) {
	for i := range arr {
		arr[i].FillUnixField()
	}
}
func (drvTF *DriverTariffCrm) FillUnixField() {
	drvTF.CreatedAtUnix = drvTF.CreatedAt.Unix()
}

// Update обновляет тариф
func (drvTF *DriverTariffCrm) Update(uuid string, skipMarkups bool) error {
	drvTF.UUID = uuid
	if skipMarkups {
		drvTF.Markups = nil //они задаются отдельно
	}

	if err := drvTF.prepare(true); err != nil {
		return errors.Wrap(err, "Ошибка валидации %v")
	}

	//иначе до null это поле не сбить
	query := db.Model(&DriverTariffCrm{}).Where("uuid = ?", uuid).Set("drivers_groups_uuid = ?drivers_groups_uuid")
	if _, err := query.Update(); err != nil {
		return err
	}

	if _, err := db.Model(drvTF).Where("uuid = ?", uuid).Returning("*").UpdateNotNull(); err != nil {
		return err
	}

	if err := InitCurrentDriverTariffs(); err != nil {
		return err
	}

	return nil
}

func (drvTF *DriverTariffCrm) validMarkups(forUpdate bool) error {
	if drvTF == nil || len(drvTF.Markups) == 0 {
		return nil
	}
	maxCom, maxRej := drvTF.MaxCompleteCommission, drvTF.MaxRejectionCommission
	if forUpdate {
		drvTFForValid, err := GetDriverTariffByUUID(drvTF.UUID)
		if err != nil {
			return err
		}
		maxCom, maxRej = drvTFForValid.MaxCompleteCommission, drvTFForValid.MaxRejectionCommission
	}

	existTaxiParks := make(map[string]bool)
	for _, mar := range drvTF.Markups {
		check := existTaxiParks[mar.TaxiParkUUID]
		if check {
			return errors.New(" ,для таксопарка допустима только одна наценка в рамках одного тарифа")
		}
		if mar.CompleteExpr != "" {
			if err := checkValidBillingExpr(mar.CompleteExpr, maxCom); err != nil {
				return errors.Errorf(" ,некорректное выражение для комиссии с выполненного заказа, %s. TPUUID (%s)", err, mar.TaxiParkUUID)
			}
		}
		if mar.RejectionExpr != "" {
			if err := checkValidBillingExpr(mar.RejectionExpr, maxRej); err != nil {
				return errors.Errorf(" ,некорректное выражение для штрафа за отказ от заказа, %s. TPUUID (%s)", err, mar.TaxiParkUUID)
			}
		}
		if drvTF.TariffType == structures.DriverTariffPeriod && mar.ForPeriodTariffs > float64(drvTF.MaxPeriodCommission) {
			return errors.Errorf(" ,превышено максимальное значение наценки. TPUUID (%s)", mar.TaxiParkUUID)
		}
		existTaxiParks[mar.TaxiParkUUID] = true
	}
	return nil
}
func (drvTF *DriverTariffCrm) prepare(forUpdate bool) error {
	if drvTF.TariffType == "" { // set the default type if not exists
		drvTF.TariffType = structures.DriverTariffPercent
	}

	// Check the tariff type
	if err := drvTF.TariffType.Validate(); err != nil {
		return errors.Errorf("%s", err)
	}

	if drvTF.Name == "" {
		return errors.Errorf("впишите название сервиса")
	}
	if drvTF.RegionID == 0 {
		return errors.Errorf("указан пустой region_id")
	}

	count, err := db.Model(&DriverTariffCrm{}).
		Where("name = ?", drvTF.Name).
		Where("deleted is not true").
		Where("uuid != ?", drvTF.UUID).
		Count()
	if err != nil {
		return errors.Errorf("невозможно проверить существование тарифа с таким именем, %s", err)
	}
	if count != 0 {
		return errors.Errorf("тариф с таким именем уже существует")
	}

	// In case of default tariff type
	if drvTF.TariffType == structures.DriverTariffPercent {
		if drvTF.CommissionExp == "" {
			return errors.Errorf("впишите выражение для комиссии с выполненного заказа")
		}
		if err := checkValidBillingExpr(drvTF.CommissionExp); err != nil {
			return errors.Errorf("некорректное выражения для завершенного заказа")
		}
		if drvTF.RejectionExp == "" {
			return errors.Errorf("впишите выражение для штрафа за отказ от заказа")
		}
		if err := checkValidBillingExpr(drvTF.RejectionExp); err != nil {
			return errors.Errorf("некорректное выражения для отказа от заказа")
		}
	}
	if err := drvTF.validMarkups(forUpdate); err != nil {
		return err
	}
	if drvTF.TariffType != structures.DriverTariffPeriod {
		return nil
	}

	// In case of driver's change
	if drvTF.Period == 0 {
		return errors.Errorf("укажите период действия смены")
	}
	if drvTF.PeriodPrice == 0 {
		return errors.Errorf("укажите стоимость смены")
	}
	return nil
}

// checkValidBillingExpr - проверяет валидность выражений. Можно передать максимальное значение, которое должно выдывать выражение
func checkValidBillingExpr(expression string, maxValue ...int) error {
	billingData := structures.BillingData{OrderPrice: validMapMarkupOrderPrice}

	val, err := expr.Eval(expression, billingData)
	if err != nil {
		return errors.Errorf("%s: %s", expression, err)
	}
	if len(maxValue) == 0 {
		return nil
	}
	valFloat, check := val.(float64)
	if !check {
		return errors.Errorf("Некорректное выражение, %s,%s", expression, err)
	}
	if int(valFloat) > maxValue[0] {
		return errors.Errorf("Слишком большая сумма. Максимум: %v, передано :%v", maxValue[0], int(valFloat))
	}
	return nil
}

// GetDefaultDriverTariff returns the default tariff
func GetDefaultDriverTariff() (DriverTariffCrm, error) {
	var drvTariff DriverTariffCrm
	if err := db.Model(&drvTariff).Where("\"default\" IS TRUE").Limit(1).Select(); err != nil {
		return DriverTariffCrm{}, err
	}
	return drvTariff, nil
}

//SetOfflineDriverTariff задает тариф как оффлайновский. Если передать "delete", то сбросит текущий оффлайн тариф, не ставя новый
func SetOfflineDriverTariff(param string) error {
	_, err := db.Model(&DriverTariffCrm{}).Where("uuid != ?", param).Set("offline = false").Update()
	if err != nil {
		return errors.Errorf("ошибка сброса текущего оффлайн тарифа для водителей,%s", err)
	}
	if param == "delete" {
		return nil
	}
	_, err = db.Model(&DriverTariffCrm{}).Where("uuid = ?", param).Set("offline = true").Update()
	if err != nil {
		return errors.Errorf("ошибка назначения нового оффлайн тарифа для водителей,%s", err)
	}
	return InitCurrentDriverTariffs()
}

//SetDefaultDriverTariff задает тариф как стандартный дл новых водителей
func SetDefaultDriverTariff(uuid string) error {
	_, err := db.Model(&DriverTariffCrm{}).Where("uuid != ?", uuid).Set(" \"default\" = false").Update()
	if err != nil {
		return errors.Errorf("ошибка сброса текущего стандартного тарифа для водителей,%s", err)
	}
	_, err = db.Model(&DriverTariffCrm{}).Where("uuid = ?", uuid).Set("\"default\" = true").Update()
	if err != nil {
		return errors.Errorf("ошибка назначения нового стандартного тарифа для водителей,%s", err)
	}
	return InitCurrentDriverTariffs()
}

// BindTariffToDriver - привязывает тариф к водителю
func BindTariffToDriver(drvuuid, tariffuuid string, buyTariff func(...structures.NewTransfer) error) (DriverCRM, error) {
	var driver DriverCRM
	err := tariffJobs.Execute(uuidHash(drvuuid), func() error {
		err := db.Model(&driver).Where("uuid = ? AND deleted IS NOT TRUE", drvuuid).Limit(1).Select()
		if err != nil {
			return errors.Errorf("failed to find a driver by uuid: %s", err)
		}
		// Check if a driver already has payed tariff
		hasPayedTariff, tariffValidUntil := driver.DrvTariff.HasPayedTariff(config.St.Application.TimeZone)
		if hasPayedTariff {
			const layout = "2006-01-02 15:04"
			return errors.Errorf(
				"У вас уже куплен тариф \"%s\", он будет действовать до %s",
				driver.DrvTariff.Name, tariffValidUntil.Format(layout),
			)
		}

		var drvTC DriverTariffCrm
		err = GetByUUID(tariffuuid, &drvTC)
		if err != nil {
			return errors.Errorf("error getting tariff by uuid [%s]", err)
		}
		markup, _ := drvTC.GetMarkupByTaxiParkUUID(driver.GetTaxiParkUUID())
		taxiParkAmount := markup.ForPeriodTariffs
		fullPrice := taxiParkAmount + drvTC.PeriodPrice

		// If a new tariff is a driver's change, then check if there is enough balance to pay for it
		if drvTC.TariffType == structures.DriverTariffPeriod && float64(driver.Balance) < fullPrice {
			return errors.Errorf(
				"У вас на недостаточно средств, чтобы купить данный тариф: ваш баланс %.2f, необходимо: %.2f",
				float64(driver.Balance), fullPrice,
			)
		}

		driver, err = UpdateTariffOfDriver(drvuuid, structures.SelectedDriverTariff{
			DriverTariff: drvTC.DriverTariff,
			PayedAt:      time.Now().Unix(),
		})
		if err != nil {
			return errors.Errorf("update driver tariff error [%s]", err)
		}

		// If all is ok, then actually pay for the tariff
		go func() {
			if drvTC.TariffType != structures.DriverTariffPeriod {
				return // no need to buy a tariff
			}
			var transfers []structures.NewTransfer
			transfers = append(transfers, structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				PayerAccountType: structures.AccountTypeBonus,
				TransferType:     structures.TransferTypeWithdraw,
				Amount:           drvTC.PeriodPrice,
				Description:      fmt.Sprintf(msgBuyTariff, drvTC.Name),
				PayerType:        structures.UserTypeDriver,
				PayerUUID:        drvuuid,
				Meta: structures.TransferMetadata{
					CurrentDriverBalance:     &driver.Balance,
					CurrentDriverCardBalance: &driver.CardBalance,
					DriverTariff: structures.TariffMetadata{
						UUID:       drvTC.UUID,
						Name:       drvTC.Name,
						TariffType: drvTC.TariffType,
					},
				},
			})
			if taxiParkAmount > 0 {
				transfers = append(transfers, getPeriodTransferForTaxiPark(taxiParkAmount, driver, drvTC))
			}
			if err := buyTariff(transfers...); err != nil {
				logs.Eloger.WithField("event", "buying a tariff").Error(err)
				return
			}
			logs.Eloger.WithField("event", "buying a tariff sent to RMQ").Info("OK")
		}()
		return nil
	})
	return driver, err
}

func getPeriodTransferForTaxiPark(taxiParkAmount float64, driver DriverCRM, drvTC DriverTariffCrm) structures.NewTransfer {
	return structures.NewTransfer{
		IdempotencyKey:   structures.GenerateUUID(),
		TransferType:     structures.TransferTypePayment,
		Description:      fmt.Sprintf(msgBuyTariffTaxiPark, drvTC.Name),
		Amount:           taxiParkAmount,
		PayerAccountType: structures.AccountTypeBonus,
		PayeeUUID:        driver.GetTaxiParkUUID(),
		PayerUUID:        driver.UUID,
		PayeeType:        structures.UserTypeTaxiPark,
		PayerType:        structures.UserTypeDriver,
		Meta: structures.TransferMetadata{
			CurrentDriverBalance: &driver.Balance,
			DriverTariff: structures.TariffMetadata{
				UUID:       drvTC.UUID,
				Name:       drvTC.Name,
				TariffType: drvTC.TariffType,
			},
		},
	}
}
func GetDriversWithExpiredPeriodTariff() ([]DriverCRM, error) {
	var drivers []DriverCRM
	err := db.Model(&drivers).
		Column("uuid", "driver_group").
		Where("deleted IS NOT TRUE").
		Where("tariff ->> 'tariff_type' = ?", structures.DriverTariffPeriod).
		Where("NOW() > to_timestamp((tariff ->> 'payed_at')::integer) + interval '1 hour' * (tariff ->> 'period')::real").
		Select()
	if err != nil {
		return nil, err
	}
	return drivers, nil
}

func UpdateTariffOfDriver(drvuuid string, tariff structures.SelectedDriverTariff) (DriverCRM, error) {
	var drv DriverCRM
	_, err := db.Model(&drv).Where("uuid = ?", drvuuid).Set("tariff = ?", tariff).Returning("*").Update()
	if err != nil {
		return DriverCRM{}, errors.Errorf("ошибка назначения нового тарифа для водителя [%s]", err)
	}
	return drv, nil
}

// heap of functions ==============================================

func isDriverExist(uuid string) (bool, error) {
	var drv DriverCRM
	exs, err := db.Model(&drv).Column("uuid").Where("uuid = ? AND deleted is not true", uuid).Exists()
	if err != nil {
		return false, errors.Errorf("[%s]", err)
	}
	return exs, nil
}

func RemoveTaxiParkMarkup(tariffUUID string, tpUUID string, validTaxiParks []string) (DriverTariffCrm, error) {
	if err := validMarkip(tpUUID, validTaxiParks); err != nil {
		return DriverTariffCrm{}, err
	}

	tariff, _ := GetDriverTariffByUUID(tariffUUID)
	if tariff.UUID == "" {
		return tariff, errors.Errorf("tariff with uuid (%s) not found", tariffUUID)
	}

	found := false
	for i := range tariff.Markups {
		if tariff.Markups[i].TaxiParkUUID == tpUUID {
			found = true
			tariff.Markups = removeFromMarkups(tariff.Markups, i)
		}
	}
	if !found {
		return tariff, nil
	}

	if err := tariff.Update(tariff.UUID, false); err != nil {
		return tariff, err
	}
	return tariff, InitCurrentDriverTariffs()
}

func removeFromMarkups(s structures.TariffMarkupsArray, i int) structures.TariffMarkupsArray {
	if len(s) < 1 {
		return s
	}
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

func AddTaxiParkMarkup(tariffUUID string, markup structures.TariffMarkup, validTaxiParks []string) (DriverTariffCrm, error) {
	if err := validMarkip(markup.TaxiParkUUID, validTaxiParks); err != nil {
		return DriverTariffCrm{}, err
	}
	tariff, _ := GetDriverTariffByUUID(tariffUUID)
	if tariff.UUID == "" {
		return tariff, errors.Errorf("tariff with uuid (%s) not found", tariffUUID)
	}
	found := false
	for i := range tariff.Markups {
		if tariff.Markups[i].TaxiParkUUID == markup.TaxiParkUUID {
			found = true
			tariff.Markups[i] = markup
			break
		}
	}
	if !found {
		tariff.Markups = append(tariff.Markups, markup)
	}

	if err := tariff.Update(tariff.UUID, false); err != nil {
		return tariff, err
	}
	return tariff, InitCurrentDriverTariffs()
}
func (df *DriverTariffCrm) FilterMarkupsByTaxiPark(taxiParksUUID []string) {
	if len(taxiParksUUID) == 0 {
		return
	}
	var newMarkups structures.TariffMarkupsArray
	for _, val := range df.Markups {
		if structures.StringInArray(val.TaxiParkUUID, taxiParksUUID) {
			newMarkups = append(newMarkups, val)
		}
	}
	df.Markups = newMarkups
}

func validMarkip(tpUUID string, validTaxiParks []string) error {
	if tpUUID == "" {
		return errors.Errorf("empty tp uuid")
	}
	if len(validTaxiParks) != 0 && !structures.StringInArray(tpUUID, validTaxiParks) {
		return errors.Errorf("Вы не можете регулировать наценки этого таксопарка")
	}
	return nil
}
