package models

// PhotocontrolForDriver -
type PhotocontrolForDriver struct {
	UUID  string `json:"uuid"`
	Alias int    `json:"alias"`
	Name  string `json:"name"`
}

// PayloadForCreateByDriverGroupTemplate -
type PayloadForCreateByDriverGroupTemplate struct {
	DriverGroupUUID   string                `json:"driver_group_uuid"`
	GroupTemplateUUID string                `json:"group_template_uuid"`
	Driver            PhotocontrolForDriver `json:"driver"`
}
