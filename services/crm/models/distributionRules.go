package models

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/antonmedv/expr"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core"
	"gitlab.com/faemproject/backend/faem/services/driver/distribution/core/scoring"
	"strings"
	"time"
)

// FIXME refactor
type distributionVariables struct {
	Booster     string `json:"booster"`
	Activity    string `json:"activity"`
	Karma       string `json:"karma"`
	Distance    string `json:"distance"`
	GroupWeight string `json:"group_weight"`
	Waiting     struct {
		If   string `json:"if"`
		Then string `json:"then"`
		Else string `json:"else"`
	} `json:"waiting"`
	PaymentTypes struct {
		If   string `json:"if"`
		Then string `json:"then"`
		Else string `json:"else"`
	} `json:"payment_types"`
	PhotocontrolPassed string `json:"photocontrol_passed"`
	TaxiParkWeight     string `json:"taxipark_weight"`
}

// FIXME refactor
func (v distributionVariables) GenerateExpression() string {
	var operands []string
	if v.Booster != "" {
		op := fmt.Sprintf("%s*Booster", v.Booster)
		operands = append(operands, op)
	}
	if v.Activity != "" {
		op := fmt.Sprintf("%s*Activity", v.Activity)
		operands = append(operands, op)
	}
	if v.Karma != "" {
		op := fmt.Sprintf("%s*Karma", v.Karma)
		operands = append(operands, op)
	}
	if v.Distance != "" {
		op := fmt.Sprintf("%s*Distance", v.Distance)
		operands = append(operands, op)
	}
	if v.GroupWeight != "" {
		op := fmt.Sprintf("%s*GroupWeight", v.GroupWeight)
		operands = append(operands, op)
	}
	if v.Waiting.If != "" {
		op := fmt.Sprintf("(Waiting < %s ? Waiting*%s : Waiting*%s)", v.Waiting.If, v.Waiting.Then, v.Waiting.Else)
		operands = append(operands, op)
	}
	if v.PaymentTypes.If != "" {
		op := fmt.Sprintf("('%s' in PaymentTypes ? %s : %s)", v.PaymentTypes.If, v.PaymentTypes.Then, v.PaymentTypes.Else)
		operands = append(operands, op)
	}
	if v.PhotocontrolPassed != "" {
		op := fmt.Sprintf("%s*PhotocontrolPassed", v.PhotocontrolPassed)
		operands = append(operands, op)
	}
	if v.TaxiParkWeight != "" {
		op := fmt.Sprintf("%s*TaxiParkWeight", v.TaxiParkWeight)
		operands = append(operands, op)
	}

	return strings.Join(operands, " + ")
}

type DistributionRule struct {
	tableName struct{} `sql:"crm_distribution_rules"`

	ID        int                    `json:"id"`
	Expr      string                 `json:"expr"`
	Variables distributionVariables  `json:"variables"`
	Params    map[string]interface{} `json:"params"`

	ormStructV2
}

type ClientDistributionRule struct {
	tableName struct{} `sql:"crm_distribution_clients_rules"`

	ID       int                        `json:"id"`
	Expr     string                     `json:"expr"`
	Template structures.FormulaTemplate `json:"template"`
	Params   map[string]interface{}     `json:"params"`

	ormStructV2
}

type DistributionRuleType string

const (
	RuleTypeRegion   DistributionRuleType = "region"
	RuleTypeTaxiPark DistributionRuleType = "taxipark"
)

// FIXME refactor
func NewDistributionRule(ruleType DistributionRuleType) DistributionRule {
	switch ruleType {
	case RuleTypeRegion:
		return DistributionRule{
			Params: map[string]interface{}{
				"rounds":                   []float64{200.0, 500.0, 900.0, 1500.0},
				"out_of_town_multiplier":   2.0,
				"allow_neutral_taxi_parks": true,
			},
		}
	case RuleTypeTaxiPark:
		return DistributionRule{
			Params: map[string]interface{}{
				"rounds":                 []float64{200.0, 500.0, 900.0, 1500.0},
				"out_of_town_multiplier": 2.0,
				"smart_attempts_limit":   21,
			},
		}
	}

	return DistributionRule{}
}

func GetDistributionRulesByIDs(ctx context.Context, ids []int) ([]DistributionRule, error) {
	var rules []DistributionRule
	err := db.ModelContext(ctx, &rules).
		WhereIn("id in (?)", ids).
		Select()

	return rules, err
}

func GetClientDistributionRule(ctx context.Context) (ClientDistributionRule, error) {
	var rule ClientDistributionRule
	err := db.ModelContext(ctx, &rule).Limit(1).Select()

	return rule, err
}

func DistributionRuleByID(ctx context.Context, id int) (*DistributionRule, error) {
	var rule DistributionRule
	rule.ID = id

	query := db.ModelContext(ctx, &rule).WherePK()
	if err := query.Select(); err != nil {
		return nil, err
	}

	return &rule, nil
}

func (d *DistributionRule) validate() error {
	d.ormStructV2 = ormStructV2{UpdatedAt: time.Now()}

	d.Expr = d.Variables.GenerateExpression()
	_, err := expr.Eval(d.Expr, &scoring.DriverScoring{})
	if err != nil {
		return errors.Wrap(err, "rule engine error")
	}

	if err := d.validateParams(); err != nil {
		return errors.Wrap(err, "invalid params")
	}

	return nil
}

// FIXME refactor
func (d *DistributionRule) validateParams() error {
	data, err := json.Marshal(d.Params)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &core.TaxiParkParams{})
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, &core.RegionParams{})
	if err != nil {
		return err
	}

	return nil
}

func (d *DistributionRule) Update(ctx context.Context) error {
	if err := d.validate(); err != nil {
		return err
	}
	query := db.ModelContext(ctx, d).
		WherePK().
		Returning("*")
	if res, err := query.UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("distribution rule with id=%d doesn't exist", d.ID)
	}

	return nil
}
