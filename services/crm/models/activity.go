package models

import (
	"context"
	"encoding/json"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

type CrmActivityHistoryItem struct {
	tableName struct{} `sql:"crm_activity_history"`

	ID             string    `json:"id"`
	UserType       string    `json:"user_type"`
	UserUUID       string    `json:"user_uuid"`
	OrderUUID      string    `json:"order_uuid"`
	OfferUUID      string    `json:"offer_uuid"`
	Event          string    `json:"event"`
	ActivityChange int       `json:"activity_change" sql:",notnull"`
	Activity       int       `json:"activity" sql:"-"`
	CreatedAt      time.Time `json:"-"`
}

func UpdateDriverActivity(item CrmActivityHistoryItem) error {
	_, err := db.Model((*DriverCRM)(nil)).
		Where("uuid = ?", item.UserUUID).
		Set("activity = GREATEST(?, 0)", item.Activity).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update driver activity")
	}

	return CreateDriverActivityHistoryItem(item)
}

func CreateDriverActivityHistoryItem(item CrmActivityHistoryItem) error {
	item.ID = uuid.Must(uuid.NewV4()).String()
	item.UserType = "driver"
	return CreateActivityHistoryItem(item)
}

func UpdateClientActivity(item CrmActivityHistoryItem) error {
	_, err := db.Model((*ClientCRM)(nil)).
		Where("uuid = ?", item.UserUUID).
		Set("activity = GREATEST(?, 0)", item.Activity).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to update driver activity")
	}
	return CreateClientActivityHistoryItem(item)
}

func CreateClientActivityHistoryItem(item CrmActivityHistoryItem) error {
	item.ID = uuid.Must(uuid.NewV4()).String()
	item.UserType = "client"
	return CreateActivityHistoryItem(item)
}

func CreateActivityHistoryItem(item CrmActivityHistoryItem) error {
	_, err := db.Model(&item).Insert()
	return errors.Wrap(err, "failed to insert a new activity history item")
}

//GetActivities returns activity config from DB
func GetActivities(ctx context.Context) (*structures.ActivityConfig, error) {
	remoteConfig, err := LoadRemoteConfig(ctx, ActivityConfigKey)
	if remoteConfig == nil {
		return nil, err
	}
	config := remoteConfig.GetValue()

	data, err := json.Marshal(config)
	if err != nil {
		return nil, err
	}

	var result structures.ActivityConfig
	err = json.Unmarshal(data, &result)
	if err != nil {
		return nil, err
	}

	return &result, err
}

//UpdateActivities updates activity config
func UpdateActivities(ctx context.Context, values structures.ActivityConfig) error {
	var invalues map[string]interface{}
	b, err := json.Marshal(values)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &invalues)
	if err != nil {
		return err
	}
	_, err = db.ModelContext(ctx, (*RemoteConfig)(nil)).Where("key = ?", ActivityConfigKey).Set("value = ?", invalues).Update()
	if err != nil {
		return err
	}
	return nil
}
