package models

import (
	"context"
	"net/http"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
)

const (
	bonusesDataEndpoint = "/bonusesdata"
)

// ClientCRM - order client structure
type ClientCRM struct {
	tableName struct{} `sql:"crm_customers"`
	ormStruct
	structures.Client
	BrokerQueue string `json:"broker_queue" description:"Comment"`
}
type ClientAutomaticCharge struct {
	tableName struct{}  `sql:"client_automatic_charge"`
	UpdatedAt time.Time `json:"-"`
	Value     bool      `json:"value"`
	Phone     string    `json:"phone"`
}

func (cac *ClientAutomaticCharge) Save() *structures.ErrorWithLevel {
	if cac.Phone == "" {
		return structures.Warning(errors.Errorf("phone is required field"))
	}
	cac.UpdatedAt = time.Now()
	_, err := db.Model(cac).
		OnConflict("(phone) DO UPDATE").
		Insert()
	return structures.Error(err)
}

// Create - save structure to database
func (cl *ClientCRM) Create() (ClientCRM, error) {
	nClient, err := cl.validCreation()
	if err != nil {
		return nClient, errors.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nClient, err
	}

	nClient.UUID = uu.String()
	_, err = db.Model(&nClient).Returning("*").Insert()
	if err != nil {
		return nClient, errors.Errorf("DB error. %v ", err)
	}
	return nClient, nil
}

func GetBonusesDataByPhone(ctx context.Context, phone string) (structures.BonusesData, error) {
	var resp structures.BonusesData
	req := struct {
		Phone string `json:"phone"`
	}{
		Phone: phone,
	}
	err := Request(
		ctx,
		http.MethodPost,
		config.St.Bonuses.Host+bonusesDataEndpoint,
		req,
		&resp,
	)
	return resp, err
}

func (cl ClientCRM) validCreation() (ClientCRM, error) {
	var newClient ClientCRM
	// if cl.Name == "" {
	// 	return newClient, errors.Errorf("Field name is required")
	// }
	newClient.Client = cl.Client

	return newClient, nil
}

// ClientByID - return Client structure based on ID
func ClientByID(id int) (ClientCRM, error) {
	var client ClientCRM
	client.ID = id
	err := db.Model(&client).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return client, err
	}
	return client, nil
}

// ClientsList - return all Clients
func ClientsList() ([]ClientCRM, error) {
	var clients []ClientCRM
	err := db.Model(&clients).
		Where("deleted is not true ").
		Select()
	if err != nil {
		return clients, err
	}
	return clients, nil
}

// Update godoc
func (cl *ClientCRM) Update(uuid string) (ClientCRM, error) {

	uClient, err := cl.validUpdate(uuid)
	if err != nil {
		return uClient, errors.Errorf("Data validation error. %v", err)
	}

	err = UpdateByPK(&uClient)
	if err != nil {
		return uClient, errors.Errorf("DB error. %v", err)
	}
	return uClient, nil
}

//
func (cl *ClientCRM) validUpdate(uuid string) (ClientCRM, error) {

	flag := false
	var client ClientCRM

	if cl.DeviceID != "" {
		client.DeviceID = cl.DeviceID
		flag = true
	}
	if cl.Name != "" {
		client.Name = cl.Name
		flag = true
	}
	if cl.Comment != "" {
		client.Comment = cl.Comment
		flag = true
	}
	client.Blocked = cl.Blocked
	if cl.MainPhone != "" {

		formattedPhone, err := phoneverify.NumberVerify(cl.MainPhone)
		if err != nil {
			return client, errors.Errorf("неккоректный номер телефона, %s", err)
		}
		client.MainPhone = formattedPhone
		flag = true
	}
	if cl.Karma != 0 {
		client.Karma = cl.Karma
		flag = true
	}
	if cl.Phones != nil {
		client.Phones = cl.Phones
		flag = true
	}

	if !flag {
		return client, errors.Errorf("Необходимые поля пустые")
	}
	client.UpdatedAt = time.Now()
	client.UUID = uuid
	err := CheckExistsUUID(&client, uuid)
	if err != nil {
		return client, err
	}
	client.Deleted = false
	return client, nil
}

// SaveClientAPP - save client to database from Client backend
func (cl *ClientCRM) SaveClientAPP() error {
	_, err := db.Model(cl).
		OnConflict("(uuid) DO UPDATE").
		Insert()
	if err != nil {
		return err
	}
	return nil
}

// SetDeleted godoc
func (cl *ClientCRM) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var client ClientCRM
	err := CheckExistsUUID(&client, cl.UUID)
	if err != nil {
		return err
	}
	cl.Deleted = true
	_, err = db.Model(cl).Where("uuid = ?uuid").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}

// ClientsByRegion godoc
func ClientsByRegion(uuid string) ([]ClientCRM, error) {
	var clients []ClientCRM
	err := db.Model(&clients).
		Where("region_uuid = ? AND deleted is not true", uuid).
		Select()
	if err != nil {
		return clients, err
	}
	return clients, nil
}

// IsTheCustomerBlacklisted -
func (cl *ClientCRM) IsTheCustomerBlacklisted(phone string) (bool, error) {
	var client ClientCRM
	var isBlocked bool
	err := db.Model(&client).
		Where("main_phone = ?", phone).
		Select()
	if err != nil {
		return isBlocked, errpath.Err(err)
	}

	if client.Blocked {
		return true, nil
	}
	return false, nil
}
