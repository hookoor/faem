package models

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/gofrs/uuid"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

const (
	// DefaulDriverGroup - все новые (и прочие по обстоятельствам) водители будут принадлежать этой группе.
	// meta: в базе должна быть запись с этой группой
	DefaulDriverGroup string = "defaul_driver_group"
)

// driverGroupKey - ключ для синхронизатора (DriverGroup.updateDrivers)
type driverGroupKey string

// набор ключей для синхронизатора (DriverGroup.updateDrivers)
const (
	addDrivers         driverGroupKey = "add_drivers_to_group"
	dropDrivers        driverGroupKey = "add_drivers_from_group"
	updateDriversGroup driverGroupKey = "update_drivers_group"
	deleteDriversGroup driverGroupKey = "delete_drivers_group"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// DriverGroup - сущность группа водителей для записи в базу
type DriverGroup struct {
	tableName struct{} `sql:"crm_driver_groups,alias:dg"`

	structures.DriverGroup

	CreatedAt time.Time `sql:"default:now()" json:"-" description:"Дата создания"`
	UpdatedAt time.Time `json:"-" `
	Deleted   bool      `json:"-" sql:"default:false"`
}

// DriverGroupFilter -
type DriverGroupFilter struct {
	UUID *string `json:"uuid"`
	Name *string `json:"name"`

	TaxiParkUUID *string `json:"taxi_park_uuid"`
	RegionIDs    []int   `json:"region_ids"`

	MinDate time.Time `json:"min_date"`
	MaxDate time.Time `json:"max_date"`

	tool.Paginator
}

// -------------------------------------------------

type IDriverGroup interface {
	// GetServicesForDriver - получение водителем списка доступных ему сервисов
	GetServicesForDriver(ctx context.Context, taxiparkuuid string) ([]ServiceCRM, error)

	// GetGroup - получение группы водителей по uuid
	GetGroup(ctx context.Context, uuid string) (*DriverGroup, error)
}

type ModelDriverGroup struct {
	db orm.DB
}

func NewModelDriverGroup(db orm.DB) *ModelDriverGroup {
	return &ModelDriverGroup{db: db}
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

func (ModelDriverGroup) GetServicesForDriver(context.Context, string) ([]ServiceCRM, error) {
	return nil, nil
}

// GetGroup - получение группы водителей по uuid
func (mdg *ModelDriverGroup) GetGroup(ctx context.Context, uuid string) (*DriverGroup, error) {
	if uuid == "" {
		return nil, errpath.Errorf("empty uuid")
	}
	var dg DriverGroup
	err := mdg.db.ModelContext(ctx, &dg).
		Where("deleted is not true").
		Where("uuid = ?", uuid).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}
	return &dg, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Create (<->) - создание новой группы водителей
func (dg *DriverGroup) Create() error {
	if dg.DefaultTariffUUID == "" {
		return errpath.Errorf("empty default_tariff_uuid")
	}
	if dg.TaxiParkUUID == "" {
		return errpath.Errorf("empty taxi_park_uuid")
	}
	if dg.DefaultTariffOfflineUUID == "" {
		return errpath.Errorf("empty default_tariff_offline_uuid")
	}
	if dg.RegionID == 0 {
		return errpath.Errorf("empty region_id")
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return errpath.Err(err)
	}
	dg.UUID = uu.String()

	if _, err = db.Model(dg).Insert(); err != nil {
		return errpath.Err(err)
	}
	return nil
}

// GetGroup (<-) - получение группы водителей по uuid
func (dg *DriverGroup) GetGroup(uuid string) error {
	if uuid == "" {
		return errpath.Errorf("empty uuid")
	}
	err := db.Model(dg).
		Where("deleted is not true AND uuid = ?", uuid).
		Select()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// GetGroupsList - получаем список всеx групп водителей
func (DriverGroup) GetGroupsList(allowedRegions ...int) ([]DriverGroup, error) {
	dgArr := new([]DriverGroup)
	query := db.Model(dgArr).Where("deleted is not true")
	if len(allowedRegions) != 0 {
		query.WhereIn("region_id IN (?)", allowedRegions)
	}

	if err := query.Select(); err != nil {
		return nil, errpath.Err(err)
	}

	return *dgArr, nil
}

// GetGroupsListByFilter -
func (DriverGroup) GetGroupsListByFilter(ctx context.Context, filter *DriverGroupFilter) (*tool.PaginatorResult, error) {
	if err := filter.Fasten(); err != nil {
		return nil, errpath.Err(err)
	}

	rowspace := structures.RowSpaceData{
		TaxiParkUUID: filter.TaxiParkUUID,

		Deleted: boolPointer(true),
	}

	wheregroup := func(q *orm.Query) (*orm.Query, error) {
		if filter.UUID != nil {
			q.Where("uuid = ?", *filter.UUID)
		}
		if filter.TaxiParkUUID != nil {
			q.Where("taxi_park_uuid = ?", *filter.TaxiParkUUID)
		}
		if filter.Name != nil {
			q.Where("name ILIKE ?", "%"+*filter.Name+"%")
		}
		q.WhereIn("region_id in (?)", filter.RegionIDs)

		if !filter.MinDate.IsZero() {
			q.Where("created_at >= ?", filter.MinDate)
		}
		if !filter.MaxDate.IsZero() {
			q.Where("created_at <  ?", filter.MaxDate)
		}
		return q, nil
	}

	dgArr := new([]DriverGroup)
	query := db.ModelContext(ctx, dgArr).
		Apply(rowspace.RowSpace()).
		WhereGroup(wheregroup)

	count, err := query.Count()
	if err != nil {
		return nil, errpath.Err(err)
	} else if count == 0 {
		return &tool.PaginatorResult{}, nil
	}

	query.Apply(filter.Pager.Pagination)
	if err := query.Select(); err != nil {
		return nil, errpath.Err(err)
	}

	res, err := tool.GetPaginatorResult(dgArr, count)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return res, nil
}

// GetByName (<-) - получить группу по ее имени
func (dg *DriverGroup) GetByName(name string) error {

	err := db.Model(dg).
		Where("deleted is not true").
		Where("name = ?", name).
		Select()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// AddDrivers (<->) - добавить uuid водителей в группу и возвращает uuid которые уже есть в группе
func (dg *DriverGroup) AddDrivers(uuids ...string) ([]structures.DriversDatForGroup, error) {

	if err := GetByUUID(dg.UUID, dg); err != nil {
		return nil, errpath.Err(err, "get by uuid error")
	}

	var drivers []DriverCRM
	err := db.Model(&drivers).Where("deleted is not true AND uuid in (?)", pg.In(uuids)).Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	uuidsExist := []structures.DriversDatForGroup{}
	uuidsNotExist := []structures.DriversDatForGroup{}
	for _, add := range drivers {
		existCheck := false
		for _, bd := range dg.BelongingDrivers {
			if bd.UUID == add.UUID {
				existCheck = true
				uuidsExist = append(uuidsExist, structures.DriversDatForGroup{Name: add.Name, UUID: add.UUID})
				break
			}
		}
		if !existCheck {
			uuidsNotExist = append(uuidsExist, structures.DriversDatForGroup{Name: add.Name, UUID: add.UUID})
		}
	}

	dg.BelongingDrivers = append(dg.BelongingDrivers, uuidsNotExist...)
	_, err = db.Model(dg).Where("uuid=?uuid").Set("belonging_drivers = ?", dg.BelongingDrivers).Update()
	if err != nil {
		return nil, errpath.Err(err, "update error")
	}

	if len(dg.PhotocontrolTemplates) > 0 { // автоназначение фотоконтролей

		for _, druuid := range uuids {
			var dr DriverCRM

			err = dr.GetDriverByUUID(druuid)
			if err != nil {
				return nil, errpath.Err(err, "GetDriverByUUID error")
			}

			drv := PhotocontrolForDriver{
				Name:  dr.Name,
				Alias: dr.Alias,
				UUID:  dr.UUID,
			}
			for _, pt := range dg.PhotocontrolTemplates {
				payload := PayloadForCreateByDriverGroupTemplate{
					DriverGroupUUID:   dg.UUID,
					GroupTemplateUUID: pt,
					Driver:            drv,
				}

				url := config.St.DriverService.Host + "/photocontrol/template/createbydrvgroup"
				err = tool.SendRequest(http.MethodPost, url, nil, payload, nil)
				if err != nil {
					return nil, errpath.Err(err)
				}
			}
		}
	}

	_ = dg.updateDrivers(addDrivers, uuidsNotExist)

	return uuidsExist, nil
}

// DropDrivers (<->) - убрать uuid водителей из группы и возвращает uuid которых нет в группе
func (dg *DriverGroup) DropDrivers(uuids ...string) ([]structures.DriversDatForGroup, error) {

	err := db.Model(dg).Where("deleted is not true AND uuid = ?", dg.UUID).Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	var drivers []DriverCRM
	err = db.Model(&drivers).Where("deleted is not true AND uuid in (?)", pg.In(uuids)).Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	uuidsExist := []structures.DriversDatForGroup{}
	uuidsNotExist := []structures.DriversDatForGroup{}
	for _, drop := range drivers {
		existCheck := false
		for i, bd := range dg.BelongingDrivers {
			if bd.UUID == drop.UUID {
				existCheck = true
				uuidsExist = append(uuidsExist, structures.DriversDatForGroup{Name: drop.Name, UUID: drop.UUID})
				dg.BelongingDrivers = append(dg.BelongingDrivers[:i], dg.BelongingDrivers[i+1:]...)

				{ // удаление у водителя фотоконтролей которые были назначены группой из которой его удаляют
					url := config.St.DriverService.Host + "/photocontrol/template/deletebydrvgroup/" + drop.UUID
					err = tool.SendRequest(http.MethodGet, url, nil, nil, nil)
					if err != nil {
						return nil, errpath.Err(err)
					}
				}
				break
			}
		}
		if !existCheck {
			uuidsNotExist = append(uuidsExist, structures.DriversDatForGroup{Name: drop.Name, UUID: drop.UUID})
		}
	}

	_, err = db.Model(dg).Where("uuid=?uuid").Set("belonging_drivers = ?", dg.BelongingDrivers).Update()
	if err != nil {
		return nil, errpath.Err(err, "update error")
	}

	_ = dg.updateDrivers(dropDrivers, uuidsExist)

	return uuidsNotExist, nil
}

// DragToGroup -
func (dg *DriverGroup) DragToGroup(_ string, uuids ...string) error {

	var drivers []DriverCRM
	err := db.Model(&drivers).Where("deleted is not true AND uuid in (?)", pg.In(uuids)).Select()
	if err != nil {
		return errpath.Err(err)
	}

	var groupsUUIDForDrop []string
	for _, drv := range drivers {
		groupsUUIDForDrop = append(groupsUUIDForDrop, drv.Group.UUID)
	}
	if len(groupsUUIDForDrop) == 0 {
		return nil
	}

	var groupsForDrop []DriverGroup
	err = db.Model(&groupsForDrop).Where("uuid in (?)", pg.In(groupsUUIDForDrop)).Select()
	if err != nil {
		return errpath.Err(err)
	}

	// удаление
	for _, gfd := range groupsForDrop {
		_, err = gfd.DropDrivers(uuids...)
		if err != nil {
			return errpath.Err(err)
		}
	}

	// добавление
	_, err = dg.AddDrivers(uuids...)
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// Update (->) - обновляет группу водителей
func (dg *DriverGroup) Update(uuid string) error {
	var flag bool
	if dg.Name != "" {
		flag = true
	}
	if dg.RegionID == 0 {
		return errpath.Errorf("empty regionID field")
	}
	if !flag {
		return errpath.Errorf("Required fields are empty")
	}
	dg.UpdatedAt = time.Now()

	res, err := db.Model(dg).Where("uuid = ? AND deleted is not true", uuid).UpdateNotNull()
	if err != nil {
		return errpath.Err(err)
	} else if res.RowsAffected() == 0 {
		return errpath.Errorf("Запись UUID=%s не найдена или удалена", uuid)
	}

	if err := dg.updateDrivers(updateDriversGroup, nil); err != nil {
		return errpath.Err(err)
	}

	return nil
}

func GetDriversGroupsByUUID(uuid []string) ([]DriverGroup, error) {
	if len(uuid) < 1 {
		return nil, nil
	}
	var res []DriverGroup
	err := db.Model(&res).
		WhereIn("uuid in (?)", uuid).
		Where("deleted is not true").
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}
	return res, nil
}

// SetDeleted (-) - помечает группу водителей как удаленную
func (dg DriverGroup) SetDeleted(uuid string) error {
	err := db.Model(&dg).Column("uuid").Where("uuid = ? AND deleted is not true", uuid).Select()
	if err != nil {
		return errpath.Err(err, fmt.Sprintf("Запись UUID=%s не найдена или удалена", uuid))
	}

	_, err = db.Model(&dg).Where("uuid=?", uuid).Set("deleted = true").Update()
	if err != nil {
		return errpath.Err(err)
	}

	_ = dg.updateDrivers(deleteDriversGroup, nil)

	return nil
}

// updateDrivers - синхронизатор мутации группы у водителей во всех сервисах
func (dg DriverGroup) updateDrivers(key driverGroupKey, selectedDrivers []structures.DriversDatForGroup) error {

	switch key {
	case addDrivers:
		var selectedDriversUUIDS []string
		for _, sd := range selectedDrivers {
			selectedDriversUUIDS = append(selectedDriversUUIDS, sd.UUID)
		}
		dg.BelongingDrivers = []structures.DriversDatForGroup{} // дабы не отображать (в контексте водителя) всех водителей принадлежаших группе
		_, err := db.Model(&DriverCRM{}).Where("uuid in (?)", pg.In(selectedDriversUUIDS)).Set("driver_group = ?", dg).UpdateNotNull()
		if err != nil {
			return errpath.Err(err, "update error")
		}

		data := structures.UpdateDriverGroup{
			Drivers:       selectedDrivers,
			DriversGroups: dg.DriverGroup,
		}
		err = rabsender.SendJSONByAction(rabsender.AcDriverData, data, rabbit.UpdateGroupInDrivers)
		if err != nil {
			return errpath.Err(err, "error send driver_group data to broker")
		}

	case dropDrivers:
		defaultGroup, err := getDefaultDriversGroup()
		if err != nil {
			return errpath.Err(err)
		}
		var selectedDriversUUIDS []string
		for _, sd := range selectedDrivers {
			selectedDriversUUIDS = append(selectedDriversUUIDS, sd.UUID)
		}
		defaultGroup.BelongingDrivers = []structures.DriversDatForGroup{}
		_, err = db.Model(&DriverCRM{}).Where("uuid in (?)", pg.In(selectedDriversUUIDS)).Set("driver_group = ?", defaultGroup).UpdateNotNull()
		if err != nil {
			return errpath.Err(err, "update error")
		}

		data := structures.UpdateDriverGroup{
			Drivers:       selectedDrivers,
			DriversGroups: defaultGroup.DriverGroup,
		}
		err = rabsender.SendJSONByAction(rabsender.AcDriverData, data, rabbit.UpdateGroupInDrivers)
		if err != nil {
			return errpath.Err(err, "error send driver_group data to broker")
		}

	case updateDriversGroup:
		dg.BelongingDrivers = []structures.DriversDatForGroup{}
		_, err := db.Model(&DriverCRM{}).Where("driver_group ->> 'uuid' = ?", dg.UUID).Set("driver_group = ?", dg).UpdateNotNull()
		if err != nil {
			return errpath.Err(err, "update error")
		}

		var drivers []DriverCRM
		err = db.Model(&drivers).Where("driver_group ->> 'uuid' = ?", dg.UUID).Select()
		if err != nil {
			return errpath.Err(err)
		}

		var driversUUID []structures.DriversDatForGroup
		for _, drv := range drivers {
			driversUUID = append(driversUUID, structures.DriversDatForGroup{Name: drv.Name, UUID: drv.UUID})
		}
		data := structures.UpdateDriverGroup{
			Drivers:       driversUUID,
			DriversGroups: dg.DriverGroup,
		}
		err = rabsender.SendJSONByAction(rabsender.AcDriverData, data, rabbit.UpdateGroupInDrivers)
		if err != nil {
			return errpath.Err(err, "error send driver_group data to broker")
		}

	case deleteDriversGroup:
		// TODO: с делитом какаято хрень происходит, надо разобраться и допилить
		// defaultGroup, err := getDefaultDriversGroup()
		// if err != nil {
		// 	return errpath.Err(err)
		// }
		// dye.Next(defaultGroup.Name)
		// dye.Next(dg.UUID)
		// defaultGroup.BelongingDrivers = []structures.DriversDatForGroup{}

		// var drivers []DriverCRM
		// err = db.Model(&drivers).Where("driver_group ->> 'uuid' = ?", dg.UUID).Select()
		// if err != nil {
		// 	return errpath.Err(err)
		// }
		// var driversUUID []structures.DriversDatForGroup
		// for _, drv := range drivers {
		// 	driversUUID = append(driversUUID, structures.DriversDatForGroup{Name: drv.Name, UUID: drv.UUID})
		// }
		// dye.Next(driversUUID)

		// var driverCRM DriverCRM
		// _, err = db.Model(&driverCRM).Where("driver_group ->> 'uuid' = ?", dg.UUID).Set("driver_group = ?", defaultGroup).UpdateNotNull()
		// if err != nil {
		// 	return errpath.Err(err, "update error")
		// }
		// dye.Next(driversUUID)

		// data := structures.UpdateDriverGroup{
		// 	Drivers:       driversUUID,
		// 	DriversGroups: defaultGroup.DriverGroup,
		// }
		// err = rabsender.SendJSONByAction(rabsender.AcDriverData, data, rabbit.UpdateGroupInDrivers)
		// if err != nil {
		// 	return errpath.Err(err, "error send driver_group data to broker")
		// }

	default:
	}

	return nil
}

func getDefaultDriversGroup() (DriverGroup, error) {
	var dg DriverGroup
	err := db.Model(&dg).Where("name = ?", DefaulDriverGroup).Select()
	if err != nil {
		return dg, errpath.Err(err)
	}

	return dg, nil
}

// ===============

// GetDrvGroupServices -
func GetDrvGroupServices(uuid string) ([]ShortService, error) {
	dg := new(DriverGroup)
	if err := db.Model(dg).Where("uuid = ?", uuid).Where("deleted is not true").Select(); err != nil {
		return nil, err
	}

	services := make([]ShortService, 0)
	query := db.Model((*ServiceCRM)(nil)).Column("uuid", "name").WhereIn("uuid IN (?)", dg.ServicesUUID)
	if err := query.Select(&services); err != nil {
		return nil, err
	}

	return services, nil
}

// GetDrvGroupServices -
func GetDrvGroupTariffs(uuid string) ([]ShortService, error) {
	tariffs := make([]ShortService, 0)
	query := db.Model((*DriverTariffCrm)(nil)).Column("uuid", "name").Where("drivers_groups_uuid && ?", pg.Array([]string{uuid}))
	if err := query.Select(&tariffs); err != nil {
		return nil, err
	}

	return tariffs, nil
}

func GetDriverGroupWeights(groupUUIDs []string) (map[string]int, error) {
	var groups []DriverGroup

	err := db.Model(&groups).
		WhereIn("uuid in (?)", groupUUIDs).
		Column("uuid", "distribution_weight").
		Select()
	if err != nil {
		return nil, err
	}

	result := make(map[string]int)
	for _, group := range groups {
		result[group.UUID] = group.DistributionWeight
	}

	return result, nil
}
