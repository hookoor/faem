package models

import (
	"context"
	"errors"
	"math"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
)

type ClientRegion struct {
	DeviceID    string                      `json:"device_id"`
	Coordinates *structures.PureCoordinates `json:"coordinates"`
	Region      *RegionCRM                  `json:"region_data"`
	Distance    float64                     `json:"_"`
}

func (cr *ClientRegion) WhereAmI(ctx context.Context) error {
	regions, err := GetRegions(ctx)
	if err != nil {
		return err
	}

	cr.Distance = math.MaxFloat64

	for i, reg := range regions {
		distance := gis.Distance(cr.Coordinates.Lat, cr.Coordinates.Long, reg.Origin.Lat, reg.Origin.Long)
		if distance < cr.Distance {
			cr.Region = &regions[i]
			cr.Distance = distance
		}
	}

	if cr.Region == nil {
		return errors.New("closest region not found")
	}

	return nil
}
