package models

import (
	"github.com/prometheus/common/log"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

type (
	EventItem struct {
		tableName struct{} `sql:"crm_events"`
		structures.EventItem
		DriverCoordinates []LocationDataCRM
	}
)

//PublishEvent отправка в брокер ивента
func PublishEvent(event EventItem) {
	err := rabsender.SendJSONByAction(rabsender.AcPublishEvent, event, event.Event)
	if err != nil {
		log.Errorf("Cant publish event. %s", err)
	}
}
