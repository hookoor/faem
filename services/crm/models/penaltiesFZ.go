package models

import (
	"fmt"

	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type ZonesPenalty struct {
	tableName      struct{} `sql:"crm_penalties_for_zones"`
	ID             int      `json:"-"`
	UUID           string   `json:"uuid"`
	Caption        string   `json:"caption"`
	RuleID         int      `json:"rule_id"`
	Priority       int      `json:"priority"`
	StartZoneUUID  string   `json:"start_zone_uuid"`
	StartZoneName  string   `json:"start_zone_name" sql:"-"`
	FinishZoneUUID string   `json:"finish_zone_uuid"`
	FinishZoneName string   `json:"finish_zone_name" sql:"-"`
	Penalty        float64  `json:"penalty"`
	Deleted        bool     `json:"-"`
}

type ZonesPenaltyFilterCriteria struct {
	StartZoneUUID  string          `json:"start_zone_uuid"`
	FinishZoneUUID string          `json:"finish_zone_uuid"`
	Anywhere       string          `json:"anywhere"`
	OrderBy        string          `json:"orderby"`
	OrderType      string          `json:"order_type"`
	Pager          urlvalues.Pager `json:"-"`
}

func (zp *ZonesPenalty) swapFinishAndStartZones() {
	zp.StartZoneUUID, zp.FinishZoneUUID = zp.FinishZoneUUID, zp.StartZoneUUID
}

func (zp *ZonesPenalty) Create(duplicate bool) *structures.ErrorWithLevel {
	errWL := zp.valid()
	if errWL != nil {
		return errWL
	}
	var data []ZonesPenalty
	data = append(data, *zp)
	if duplicate {
		zp.swapFinishAndStartZones()
		data = append(data, *zp)
	}
	for _, item := range data {
		_, err := db.Model(zp).
			Where("finish_zone_uuid = ? and start_zone_uuid = ?", item.FinishZoneUUID, item.StartZoneUUID).
			Set("deleted = true").
			Update()
		if err != nil {
			return structures.Error(err)
		}
	}

	_, err := db.Model(&data).
		Insert()
	if err != nil {
		return structures.Error(err)
	}
	return nil
}
func GetZones(pager urlvalues.Pager) ([]CrmZone, int, error) {
	var result []CrmZone
	count, err := db.Model(&result).
		Count()
	if err != nil || count == 0 {
		return result, count, err
	}
	err = db.Model(&result).
		Apply(pager.Pagination).
		Select()
	return result, count, err
}
func FillPenaltiesZonesNames(data []ZonesPenalty) ([]ZonesPenalty, error) {
	var neededZonesUUID []string
	if len(data) == 0 {
		return nil, nil
	}
	for _, pen := range data {
		neededZonesUUID = append(neededZonesUUID, pen.FinishZoneUUID, pen.StartZoneUUID)
	}
	zones, err := GetZonesByUUID(neededZonesUUID)
	if err != nil {
		return data, errors.Errorf("error getting zones by uuid,%s", err)
	}
	for i := range data {
		for _, zone := range zones {
			if zone.UUID == data[i].FinishZoneUUID {
				data[i].FinishZoneName = zone.Name
			}
			if zone.UUID == data[i].StartZoneUUID {
				data[i].StartZoneName = zone.Name
			}
		}
	}
	return data, nil
}
func (cr *ZonesPenaltyFilterCriteria) GetPenalties() ([]ZonesPenalty, int, error) {
	var result []ZonesPenalty
	query := db.Model(&result).
		Where("deleted is not true")
	if cr.Anywhere != "" {
		query.Where("start_zone_uuid = ? OR finish_zone_uuid = ?", cr.Anywhere, cr.Anywhere)
		cr.StartZoneUUID = ""
		cr.FinishZoneUUID = ""
	}
	if cr.StartZoneUUID != "" {
		query.Where("start_zone_uuid = ?", cr.StartZoneUUID)
	}
	if cr.FinishZoneUUID != "" {
		query.Where("finish_zone_uuid = ?", cr.FinishZoneUUID)
	}
	if cr.OrderBy != "" {
		if cr.OrderType == "" {
			cr.OrderType = "ASC"
		}
		query.Order(fmt.Sprintf("%s %s", cr.OrderBy, cr.OrderType))
	}
	count, err := query.
		Count()
	if err != nil || count == 0 {
		return result, count, err
	}
	err = query.
		Apply(cr.Pager.Pagination).
		Select()
	return result, count, err
}
func (zp *ZonesPenalty) valid() *structures.ErrorWithLevel {
	if zp.StartZoneUUID == "" || zp.FinishZoneUUID == "" {
		return structures.Warning(errors.Errorf("Укажите начальную и конечную зону"))
	}
	if zp.Penalty == 0 {
		return structures.Warning(errors.Errorf("Укажите наценку"))
	}
	zp.Deleted = false
	return nil
}

func ZenPenaltyDelete(uuid string) error {
	_, err := db.Model(&ZonesPenalty{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}
