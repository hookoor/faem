package models

import (
	"context"
	"encoding/json"

	"github.com/go-pg/pg"
	"github.com/hashicorp/go-version"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
)

// ConfigReinitialization -
func ConfigReinitialization() error {
	var err error

	err = errpath.Errorf("logic not compleate")

	return err
}

// GetConfigValueByKey -
func GetConfigValueByKey(key structures.DBConfigKey) (map[string]interface{}, error) {
	var confg RemoteConfig
	err := db.Model(&confg).
		Where("key = ?", key).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return confg.Value, nil
}

// GetReferralSystemParams -
func GetReferralSystemParams() (structures.ReferralSystemParams, error) {
	var err error
	var res structures.ReferralSystemParams

	var confg RemoteConfig
	err = db.Model(&confg).
		Where("key = ?", structures.DBConfigReferralSystemParams).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	b, err := json.Marshal(confg.Value)
	if err != nil {
		return res, errpath.Err(err)
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil

}
func UpdateReferralSystemParams(ctx context.Context, config *structures.ReferralSystemParams) error {
	var invalues map[string]interface{}
	b, err := json.Marshal(config)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &invalues)
	if err != nil {
		return err
	}
	_, err = db.ModelContext(ctx, (*RemoteConfig)(nil)).Where("key = ?", "referral_system_params").Set("value = ?", invalues).Update()
	if err != nil {
		return err
	}
	return nil
}

// ---------
// ---------
// ---------

// GetDistributionParams -
func GetDistributionParams() (structures.DistributionParams, error) {
	var err error
	var res structures.DistributionParams

	var confg RemoteConfig
	err = db.Model(&confg).
		Where("key = ?", structures.DBConfigDistributionParams).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	b, err := json.Marshal(confg.Value)
	if err != nil {
		return res, errpath.Err(err)
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

// ---------
// ---------
// ---------

// IGodMode -
type IGodMode interface {
	GetGodModeParams() (structures.GodMode, error)
	SetGodModeParams(g structures.GodMode) error
}

// GodModeModel -
type GodModeModel struct {
	db           *pg.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// InitGodMode -
func InitGodMode(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *GodModeModel {
	godModeInst := GodModeModel{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
	return &godModeInst
}

// GetGodModeParams -
func (gm GodModeModel) GetGodModeParams() (structures.GodMode, error) {
	var err error
	var res structures.GodMode

	var confg RemoteConfig
	err = gm.db.Model(&confg).
		Where("key = ?", structures.DBConfigGodModeParams).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	b, err := json.Marshal(confg.Value)
	if err != nil {
		return res, errpath.Err(err)
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return res, errpath.Err(err)
	}

	GodModeInst = res

	return GodModeInst, nil
}

// SetGodModeParams -
func (gm GodModeModel) SetGodModeParams(g structures.GodMode) error {
	var confg RemoteConfig
	_, err := gm.db.Model(&confg).
		Where("key = ?", structures.DBConfigGodModeParams).
		Set("value = ?", g).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// GetGodModeParamsByTicker -
func GetGodModeParamsByTicker() (structures.GodMode, error) {
	var err error
	var res structures.GodMode

	var confg RemoteConfig
	err = db.Model(&confg).
		Where("key = ?", structures.DBConfigGodModeParams).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	b, err := json.Marshal(confg.Value)
	if err != nil {
		return res, errpath.Err(err)
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return res, errpath.Err(err)
	}

	GodModeInst = res

	return GodModeInst, nil
}

// ---------
// ---------
// ---------

func getMinVersionFromDB(service, OS structures.DBConfigKey) (string, error) {
	val, err := GetConfigValueByKey(structures.DBConfigMinAppVersion)
	if err != nil {
		return "", err
	}

	verIface, ok := val[string(service)]
	if !ok {
		return "", errors.Errorf("cannot find min app ver config value for service %s", service)
	}

	data, err := json.Marshal(verIface)
	if err != nil {
		return "", err
	}

	verStruct := make([]structures.AppVersion, 0, 2)
	if err := json.Unmarshal(data, &verStruct); err != nil {
		return "", err
	}

	verStr := ""
	for _, ver := range verStruct {
		if ver.OS == string(OS) {
			verStr = ver.Version
			break
		}
	}

	if verStr == "" {
		return "", errors.Errorf("version for service %s and OS %s not found", service, OS)
	}

	return verStr, nil
}

func isVersionActual(minver, ver string) (bool, error) {
	_minver, err := version.NewVersion(minver)
	if err != nil {
		return false, err
	}

	_ver, err := version.NewVersion(ver)
	if err != nil {
		return false, err
	}

	return _ver.GreaterThanOrEqual(_minver), nil
}

func GetInsuranceParams() (structures.InsuranceParams, error) {
	var err error
	var res structures.InsuranceParams

	var confg RemoteConfig
	err = db.Model(&confg).
		Where("key = ?", structures.DBConfigInsuranceParams).
		Select()
	if err != nil {
		return res, errpath.Err(err)
	}

	b, err := json.Marshal(confg.Value)
	if err != nil {
		return res, errpath.Err(err)
	}

	err = json.Unmarshal(b, &res)
	if err != nil {
		return res, errpath.Err(err)
	}

	return res, nil
}

func UpdateInsuranceParams(ctx context.Context, config *structures.InsuranceParams) error {
	var invalues map[string]interface{}
	b, err := json.Marshal(config)
	if err != nil {
		return err
	}
	err = json.Unmarshal(b, &invalues)
	if err != nil {
		return err
	}
	_, err = db.ModelContext(ctx, (*RemoteConfig)(nil)).Where("key = ?", structures.DBConfigInsuranceParams).Set("value = ?", invalues).Update()
	if err != nil {
		return err
	}
	return nil
}

// ---------
// ---------
// ---------
