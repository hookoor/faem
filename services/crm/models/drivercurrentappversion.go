package models

import (
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type DriverCurrentAPPVersion struct {
	tableName      struct{} `sql:"crm_driver_current_app_version"`
	DriverUUID     string   `json:"driver_uuid"`
	Version        string   `json:"version_name"`
	DeviceModel    string   `json:"device_model"`
	AndroidVersion string   `json:"android_ver"`
	ormStruct
}

func (drvAppVer *DriverCurrentAPPVersion) SaveDriverCAPPVIfNeed() error {
	drvAppVer.UpdatedAt = time.Now()
	_, err := db.Model(drvAppVer).
		OnConflict("(driver_uuid) DO UPDATE").
		Insert()
	return err
}

func FillDriversCurrentAppVersion(drivers []DriverCRM) ([]DriverCRM, error) {
	if len(drivers) == 0 {
		return drivers, nil
	}
	var driversuuid []string
	for _, drv := range drivers {
		driversuuid = append(driversuuid, drv.UUID)
	}
	currVersions, err := getDriversAppCurrentVersion(driversuuid)
	if err != nil {
		return drivers, err
	}
	for _, cVer := range currVersions {
		for i := range drivers {
			if drivers[i].UUID == cVer.DriverUUID {
				drivers[i].DeviceData.CurrentAppVersion = cVer.Version
				drivers[i].DeviceData.DeviceModel = cVer.DeviceModel
				drivers[i].DeviceData.AndroidVersion = cVer.AndroidVersion
				break
			}
		}
	}
	return drivers, nil
}
func getDriversAppCurrentVersion(driversUUID []string) ([]DriverCurrentAPPVersion, error) {
	var versions []DriverCurrentAPPVersion
	err := db.Model(&versions).
		Where("driver_uuid in (?)", pg.In(driversUUID)).
		Select()

	return versions, err
}

// IsDriverAppActual - и так понятно
func IsDriverAppActual(drvApp *DriverCurrentAPPVersion) (bool, error) {
	minVer, err := getMinVersionFromDB(structures.MinVerDriverApp, structures.MinVerAndroidOS) // FIXME: если будет приложение водительское на айос - передалайте структуру
	if err != nil {
		return false, err
	}

	return isVersionActual(minVer, drvApp.Version)
}
