package models

import (
	"context"
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// IRegion -
type IRegion interface {
	// Create -
	// GetByID - получение по id
	GetByID(ctx context.Context, id int) (*RegionCRM, error)
	// GetList -
	// GetRegions -
	GetRegions(ctx context.Context, allowedRegions ...int) ([]RegionCRM, error)
	// GetFilteredList -
	// GetRegionByTaxiparkUUID -
	GetRegionByTaxiparkUUID(ctx context.Context, taxiparkUUID string) (*RegionCRM, error)
	// Update -
	// Delete -
	// ---
	GetByFeatureSetId(ctx context.Context, id int) ([]RegionCRM, error)
	// ---
	// GetTariffs - получение всех тарифов доступных для этого региона
	GetTariffs(ctx context.Context, regionID int) ([]DriverTariffCrm, error)
	// GetRegionalDrivers - получение водителей по регионам через таксопарки
	GetRegionalDrivers(ctx context.Context, regionsIDs []int) ([]DriverCRM, error)
}

// RegionModel -
type RegionModel struct {
	db           orm.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// InitRegionModel -
func InitRegionModel(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *RegionModel {
	regInst := RegionModel{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
	return &regInst
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// RegionCRM - order region structure
type RegionCRM struct {
	tableName struct{} `sql:"crm_regions,alias:region_crm" pg:",discard_unknown_columns"`

	structures.Region

	CreatedAt time.Time `json:"-" sql:"default:now()"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt time.Time `json:"-" pg:",soft_delete"`
}

// ---

// DriverTariffsSetsCRM -
type DriverTariffsSetsCRM struct {
	tableName struct{} `sql:"crm_driver_tariffs_sets"`

	structures.DriverTariffsSets

	CreatedAt time.Time `json:"-" sql:"default:now()"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt time.Time `json:"-"`
}

// AddressesSetsCRM -
type AddressesSetsCRM struct {
	tableName struct{} `sql:"crm_addresses_sets"`

	structures.AddressesSets

	CreatedAt time.Time `json:"-" sql:"default:now()"`
	UpdatedAt time.Time `json:"-"`
	DeletedAt time.Time `json:"-"`
}

// SetToDriverTariff -
type SetToDriverTariff struct {
	tableName struct{} `sql:"crm_set_to_driver_tariff"`

	SetID            int
	DriverTariffUUID string
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// GetByID -
func (rm *RegionModel) GetByID(ctx context.Context, id int) (*RegionCRM, error) {
	var region RegionCRM
	err := rm.db.ModelContext(ctx, &region).
		Where("id = ?", id).
		Where("deleted_at is null").
		Limit(1).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}
	return &region, nil
}

func (rm *RegionModel) GetRegions(ctx context.Context, allowedRegions ...int) ([]RegionCRM, error) {
	regions := new([]RegionCRM)

	if allowedRegions != nil {
		if err := rm.db.ModelContext(ctx, regions).
			WhereIn("id IN (?)", allowedRegions).
			Select(); err != nil {
			return nil, errpath.Err(err)
		}
	}

	return *regions, nil
}

// GetTariffs - go to interface
func (rm *RegionModel) GetTariffs(ctx context.Context, regionID int) ([]DriverTariffCrm, error) {

	queryString := `
	SELECT trf.* FROM crm_regions AS "region_crm" 
	INNER JOIN crm_set_to_driver_tariff as sset on region_crm.services_set_id = sset.set_id 
	INNER JOIN crm_driver_tariff as trf on sset.driver_tariff_uuid = trf.uuid 
	WHERE (region_crm.driver_tariffs_set_id = ?)`

	var tariffs []DriverTariffCrm
	_, err := rm.db.QueryContext(ctx, &tariffs, queryString, regionID)
	if err != nil {
		return nil, errpath.Err(err)
	}

	// лежит в качестве примера попытки реализации, переиспользовать или удалить
	// var tariffs []DriverTariffCrm
	// err := rm.db.ModelContext(ctx, (*RegionCRM)(nil), &tariffs).
	// 	Join("INNER JOIN crm_set_to_driver_tariff as sset on region_crm.services_set_id = sset.set_id").
	// 	Join("INNER JOIN crm_driver_tariff as trf on sset.driver_tariff_uuid = trf.uuid").
	// 	Where("region_crm.driver_tariffs_set_id = = ?", regionID).
	// 	Select(&tariffs)

	return tariffs, nil
}

func (rm *RegionModel) GetRegionByTaxiparkUUID(ctx context.Context, taxiparkUUID string) (*RegionCRM, error) {
	var res RegionCRM

	// лежит в качестве примера попытки реализации, переиспользовать или удалить
	// err := rm.db.ModelContext(ctx, (*TaxiParkCRM)(nil)).
	// 	Join("join crm_regions as reg on taxipark.region_id = reg.id").
	// 	Where("taxipark.uuid = ?", taxiparkUUID).
	// 	Select(&res)
	// if err != nil {
	// 	return nil, errpath.Err(err)
	// }

	queryString := `
	SELECT reg.* FROM crm_taxi_parks AS taxipark 
	INNER JOIN crm_regions AS reg ON taxipark.region_id = reg.id 
	WHERE (taxipark.uuid = ?)`

	_, err := rm.db.QueryContext(ctx, &res, queryString, taxiparkUUID)
	if err != nil {
		return nil, errpath.Err(err)
	}

	return &res, nil
}

// GetByFeatureSetId -
func (rm *RegionModel) GetByFeatureSetId(ctx context.Context, id int) ([]RegionCRM, error) {
	var regions []RegionCRM
	err := rm.db.ModelContext(ctx, &regions).
		Where("features_set_id = ?", id).
		Where("deleted_at is null").
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}
	return regions, nil
}

// GetRegionalDrivers - получние всех региональных водителей
func (rm *RegionModel) GetRegionalDrivers(ctx context.Context, regionsIDs []int) ([]DriverCRM, error) {
	var drivers []DriverCRM

	err := rm.db.ModelContext(ctx, &drivers).
		Join("join crm_taxi_parks as parks on drivers.taxi_park_uuid = parks.uuid").
		Where("drivers.deleted is not true").
		Where("parks.deleted is not true").
		Where("drivers.taxi_park_uuid is not null").
		WhereIn("parks.region_id IN (?)", regionsIDs).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return drivers, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// Create - save structure to database
func (rg *RegionCRM) Create(creatorUUID string) error {
	if err := rg.validCreation(); err != nil {
		return errors.Wrap(err, "data validation error")
	}

	return db.RunInTransaction(func(tx *pg.Tx) error {
		set, err := CreateServicesSet(tx, fmt.Sprintf("Набор услуг региона \"%s\"", rg.Name))
		if err != nil {
			return err
		}
		rg.ServicesSetID = set.ID

		rule := NewDistributionRule(RuleTypeRegion)
		q := tx.Model(&rule).Returning("id")
		if _, err := q.Insert(); err != nil {
			return err
		}
		rg.DistributionRuleID = rule.ID

		if _, err := tx.Model(rg).Returning("*").Insert(); err != nil {
			return err
		}

		query := tx.Model((*UsersCRM)(nil)).
			Where("uuid = ?", creatorUUID).
			Set("region_ids = array_append(region_ids, ?)", rg.ID)

		if _, err := query.Update(); err != nil {
			return err
		}

		return nil
	})
}

func (rg *RegionCRM) validCreation() error {
	if rg.Name == "" {
		return errors.New("Field name is required")
	}

	if ok, err := db.Model(rg).Where("name=?name").Exists(); err != nil {
		return err
	} else if ok {
		return errors.Errorf("region with name %s already exists", rg.Name)
	}

	return nil
}

// Update - save structure to database
func (rg *RegionCRM) Update() error {
	if res, err := db.Model(rg).WherePK().UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("region with id %d doesn't exists", rg.ID)
	}

	return nil
}

// SetDeleted godoc
func (rg *RegionCRM) SetDeleted() error {
	return db.RunInTransaction(func(tx *pg.Tx) error {
		query1 := db.Model(rg).WherePK().Returning("*")
		if res, err := query1.Delete(); err != nil {
			return err
		} else if res.RowsAffected() == 0 {
			return errors.Errorf("region with id %d was not found", rg.ID)
		}

		query2 := db.Model(&ServicesSetCRM{}).
			Where("id = ?", rg.ServicesSetID)
		if _, err := query2.Delete(); err != nil {
			return err
		}

		query3 := db.Model((*ServiceSetItem)(nil)).
			Where("set_id = ?", rg.ServicesSetID)
		if _, err := query3.Delete(); err != nil {
			return err
		}

		query4 := db.Model(&DistributionRule{}).
			Where("id = ?", rg.DistributionRuleID)
		if _, err := query4.Delete(); err != nil {
			return err
		}

		return nil
	})
}

func GetRegions(ctx context.Context, allowedRegions ...int) ([]RegionCRM, error) {
	regions := new([]RegionCRM)
	query := db.ModelContext(ctx, regions)

	if allowedRegions != nil {
		query.WhereIn("id IN (?)", allowedRegions)
	}

	if err := query.Select(); err != nil {
		return nil, err
	}

	return *regions, nil
}

func GetRegionsForFront(allowedRegions []int, allowedTaxiParks []string) ([]RegionForFront, error) {
	var result []RegionForFront
	query := db.Model((*RegionCRM)(nil)).
		Column("id", "name").
		WhereIn("id IN (?)", allowedRegions)
	if err := query.Select(&result); err != nil {
		return nil, err
	}

	for i, region := range result {
		query1 := db.Model((*TaxiParkCRM)(nil)).
			Column("uuid", "name").
			WhereIn("uuid IN (?)", allowedTaxiParks).
			Where("region_id = ?", region.ID).
			Where("deleted IS NOT true")
		if err := query1.Select(&result[i].TaxiParks); err != nil {
			return nil, err
		}

		query2 := db.Model((*ServiceSetItem)(nil)).
			Column("uuid", "s.name").
			Join("JOIN crm_regions r ON set_id = services_set_id").
			Join("JOIN crm_services s ON service_uuid = uuid").
			Where("r.id = ?", region.ID).
			Where("active").
			Order("sts.display_priority DESC")
		if err := query2.Select(&result[i].Services); err != nil {
			return nil, err
		}
	}

	return result, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
// Старая реализация
// по возможности не использовать !!!

// GetCityListByRegionID -
func (RegionCRM) GetCityListByRegionID(regID ...int) ([]string, error) {

	var bind []struct {
		City   string `sql:"city_name"`
		Weight int    `sql:"weight"`
	}
	err := db.Model((*RegionCRM)(nil)).
		WhereIn("region_crm.id in (?)", regID).
		Where("deleted_at is null").
		Join("INNER JOIN crm_set_to_address as st ON addresses_set_id = st.set_id").
		Column("st.city_name", "st.weight").
		Select(&bind)
	if err != nil {
		return nil, errpath.Err(err)
	}

	sort.Slice(bind, func(i, j int) bool {
		return bind[i].Weight > bind[j].Weight
	})

	var result []string = make([]string, 0, len(bind))
	for _, el := range bind {
		result = append(result, el.City)
	}

	return result, nil
}

func (RegionCRM) GetCityWeights(regID int) (map[string]float64, error) {

	var bind []struct {
		City   string  `sql:"city_name"`
		Weight float64 `sql:"weight"`
	}
	err := db.Model((*RegionCRM)(nil)).
		Where("region_crm.id = ?", regID).
		Where("deleted_at is null").
		Join("INNER JOIN crm_set_to_address as st ON addresses_set_id = st.set_id").
		Column("st.city_name", "st.weight").
		Select(&bind)
	if err != nil {
		return nil, errpath.Err(err)
	}

	result := make(map[string]float64)
	for _, b := range bind {
		result[strings.ToLower(b.City)] = b.Weight
	}

	return result, nil
}

// GetRegionByTaxiParkUUID - old
func (RegionCRM) GetRegionByTaxiParkUUID(ctx context.Context, tpuuid string) (*RegionCRM, error) {

	var region RegionCRM
	err := db.ModelContext(ctx, &region).
		Where("belonging_taxi_parks @> ARRAY[?]", tpuuid).
		Where("deleted_at is null").
		Limit(1).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return &region, nil
}

// GetRegionIDByTaxiParkUUID - old
func (RegionCRM) GetRegionIDByTaxiParkUUID(ctx context.Context, tpuuid string) (int, error) {
	var taxipark TaxiParkCRM

	err := db.ModelContext(ctx, &taxipark).
		Column("region_id").
		Where("uuid = ?", tpuuid).
		Select()
	if err != nil {
		return 0, errpath.Err(err)
	}

	return taxipark.RegionID, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
