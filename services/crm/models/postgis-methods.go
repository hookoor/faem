package models

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
)

// ZonesByPoints -
func ZonesByPoints(coords []structures.PureCoordinates) ([]structures.ZoneWithPoints, error) {
	result := []structures.ZoneWithPoints{}

	gisres := []struct {
		CrmZone
		gis.AllPointsBelongingToAllZonesResultItems
	}{}
	gisCrmZonesData.AllPointsBelongingToAllZones(coords, &gisres)

	for _, item := range gisres {
		r, err := gis.WKTParse(item.Intr)
		if err != nil {
			return nil, errors.Errorf("(ZonesByPoints)wkt parse error")
		}
		points := []structures.PureCoordinates{}
		for _, pt := range r.Points {
			points = append(points, pt[0])
		}
		result = append(result, structures.ZoneWithPoints{
			ZoneUUID: item.UUID,
			ZoneName: item.Name,
			Points:   points,
		})
	}

	return result, nil
}
