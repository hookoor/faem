package models

import (
	"context"
	"image"
	"image/jpeg"
	"image/png"
	"path"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

const (
	uploadToCloudTimeout = 60 * time.Second

	// PictureFolderName - папка в бакете гуглклауда
	PictureFolderName string = "pictures"

	// PictureTargetKey - ключ для указание сущности которой предназначена картинка (stores, products, ...)
	PictureTargetKey string = "target"
	// PictureTargetUUID - ключ для UUID сущности
	PictureTargetUUID string = "uuid"
)

// PictureTargetType -
type PictureTargetType string

// PictureTarget -
type PictureTarget struct {
	Target PictureTargetType
	UUID   string
}

const (
	// StorePicture - картинка для заведения
	StorePicture PictureTargetType = "store"
	// ProductPicture - картинка для продукта
	ProductPicture PictureTargetType = "product"
	// ServicePicture - картинка для сервиса
	ServicePicture PictureTargetType = "service"
)

const (
	ImageFullFileName   string = "full"
	ImageSmallFileName  string = "small_format"
	ImageSmallWidth     int    = 750
	ImageSmallHeight    int    = 454
	ImageMediumFileName string = "medium_format"
	ImageMediumWidth    int    = 900
	ImageMediumHeight   int    = 600
	ImageHighFileName   string = "high_format"
	ImageHighWidth      int    = 0
	ImageHighHeight     int    = 0
)

// SaveImage -
func (s *Server) SaveImage(ctx context.Context, upload cloudstorage.Upload, target PictureTarget) (structures.ImagesSet, error) {
	var err error
	var imagesSet structures.ImagesSet
	var img image.Image

	if upload.Filetype == "image/jpeg" {
		img, err = jpeg.Decode(upload.File)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}
	}
	if upload.Filetype == "image/png" {
		img, err = png.Decode(upload.File)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}
	}

	upload.Filename = strings.Replace(upload.Filename, " ", "_", -1)

	folderName := PictureFolderName + "/" + string(target.Target) + "/" + strings.Split(upload.Filename, ".")[0]
	uploadImage := cloudstorage.UploadImage{
		FolderName: folderName,
		Filename:   ImageFullFileName + path.Ext(upload.Filename),
		Filesize:   upload.Filesize,
		Filetype:   upload.Filetype,
		Image:      img,
	}

	imagesSet.FullFormat, err = s.CloudStorage.UploadImage(ctx, &uploadImage)
	if err != nil {
		return imagesSet, errpath.Err(err)
	}

	// upload.File.Seek(0, 0) // ставит указатель считывания файла на начало

	{ // сохранение изображения в small_format формате
		smallSize := tool.PictureSize{
			Width:  ImageSmallWidth,
			Height: ImageSmallHeight,
		}
		cutingImg, err := tool.ResizeAndCutPicture(img, smallSize)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}

		uploadImage.Filename = ImageSmallFileName + path.Ext(upload.Filename)
		uploadImage.Image = cutingImg

		imagesSet.SmallFormat, err = s.CloudStorage.UploadImage(ctx, &uploadImage)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}
	}
	{ // сохранение изображения в medium_format формате
		mediumSize := tool.PictureSize{
			Width:  ImageMediumWidth,
			Height: ImageMediumHeight,
		}
		cutingImg, err := tool.ResizeAndCutPicture(img, mediumSize)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}

		uploadImage.Filename = ImageMediumFileName + path.Ext(upload.Filename)
		uploadImage.Image = cutingImg

		imagesSet.MediumFormat, err = s.CloudStorage.UploadImage(ctx, &uploadImage)
		if err != nil {
			return imagesSet, errpath.Err(err)
		}
	}

	return imagesSet, nil
}
