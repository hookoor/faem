package models

import (
	"sort"
	"sync"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/gis"
	"gitlab.com/faemproject/backend/faem/pkg/structures/osrm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
)

const (
	maxNearestDriversCount int = 25
	distRadius             int = 1500
	// если водила проехал больше этого значения между тем как прислал координаты,
	// то надо бы проверить его скорость. Если она больше чем maxValidSpeed, а с момента последнего
	// обновления координат прошло не больше locationMaxLifetime секунд, то считаем координаты
	// поломанными и не учитываем их
	suspiciousDistance  = 1000
	maxValidSpeed       = 200
	locationMaxLifetime = 60
)

type DriverLocations struct {
	m  map[string]LocationDataCRM
	mx sync.RWMutex
}
type disDataStruct struct {
	structures.Coordinates
	Dist float64
}

func NewDriverLocations() *DriverLocations {
	return &DriverLocations{m: make(map[string]LocationDataCRM)}
}

func (d *DriverLocations) Load(driverUUID string) (LocationDataCRM, bool) {
	d.mx.RLock()
	val, found := d.m[driverUUID]
	d.mx.RUnlock()
	return val, found
}

func (d *DriverLocations) Purge(driverUUID string) {
	d.mx.Lock()
	delete(d.m, driverUUID)
	d.mx.Unlock()
}

func GetNearestDrivers(coord structures.Coordinates, limit int, inRadius, forTest bool) ([]structures.CoordinatesWithAdditionalData, error) {
	if coord.Lat == 0 || coord.Long == 0 || limit == 0 {
		return nil, errors.Errorf("invalid input data")
	}
	if limit > maxNearestDriversCount {
		limit = maxNearestDriversCount
	}
	neededData := make(map[string]LocationDataCRM)
	currentDriverLocations.mx.RLock()
	for key, data := range currentDriverLocations.m {
		neededData[key] = data
	}
	currentDriverLocations.mx.RUnlock()

	distanseData := getDisData(neededData, forTest)
	var distanseFiltData []disDataStruct
	for i := range distanseData {
		dis := gis.Distance(distanseData[i].Lat, distanseData[i].Long, coord.Lat, coord.Long)
		distanseData[i].Dist = dis
		if dis > float64(distRadius) && inRadius {
			continue
		}
		distanseFiltData = append(distanseFiltData, distanseData[i])
	}
	sort.Slice(distanseFiltData, func(i, j int) bool { return distanseFiltData[i].Dist < distanseFiltData[j].Dist })
	if len(distanseFiltData) > limit {
		distanseFiltData = distanseFiltData[:limit-1]
	}
	result, err := FilterByState(distanseFiltData)
	if err != nil {
		return result, err
	}
	return result, nil
}
func getDisData(input map[string]LocationDataCRM, forTest bool) []disDataStruct {
	var result []disDataStruct
	drvLocTime := config.St.Application.DrvLocShelfLifeSec
	if forTest {
		drvLocTime = 100000000
	}
	for key, data := range input {
		if (time.Now().Unix() - data.CreatedAt.Unix()) > int64(drvLocTime) {
			continue
		}
		input[key] = data
		var dis disDataStruct
		dis.Lat = data.Latitude
		dis.Long = data.Longitude
		dis.DriverUUID = data.DriverUUID
		result = append(result, dis)
	}
	return result
}

func GetItinerary(coords []structures.Coordinates) (osrm.RouteData, error) {
	var toolCoords []tool.Coordinates
	for _, coord := range coords {
		if coord.DriverUUID != "" {
			locData, check := currentDriverLocations.Load(coord.DriverUUID)
			if !check {
				return osrm.RouteData{}, errors.Errorf("actual locations for driver with uuid (%s) not found", coord.DriverUUID)
			}
			coord.Lat = locData.Latitude
			coord.Long = locData.Longitude
		}
		toolCoords = append(toolCoords, tool.Coordinates{
			Lat:  coord.Lat,
			Long: coord.Long,
		})
	}
	return osrmka.GetItinerary(toolCoords)
}

func FilterByState(arr []disDataStruct) ([]structures.CoordinatesWithAdditionalData, error) {
	var neededUUID []string
	var result []structures.CoordinatesWithAdditionalData
	for _, data := range arr {
		neededUUID = append(neededUUID, data.DriverUUID)
	}
	coordByUUID := func(uuid string) disDataStruct {
		var res disDataStruct
		for _, val := range arr {
			if val.DriverUUID == uuid {
				return val
			}
		}
		return res
	}
	if len(neededUUID) < 1 {
		return result, nil
	}
	var drivers []DriverCRM
	err := db.Model(&drivers).
		Where("uuid in (?)", pg.In(neededUUID)).
		Where("deleted is not true").
		Returning("status,car,uuid,alias").
		Select()
	if err != nil {
		return result, err
	}
	for _, data := range drivers {
		var coorWithAddData structures.CoordinatesWithAdditionalData
		coorWithAddData.Car = data.Car
		coorWithAddData.DriverState = data.Status
		coorWithAddData.DriverStateTrans = constants.DriverStateRU[data.Status]
		disData := coordByUUID(data.UUID)
		coorWithAddData.Distance = disData.Dist
		coorWithAddData.Alias = data.Alias
		coorWithAddData.Coordinates = disData.Coordinates
		result = append(result, coorWithAddData)
	}
	return result, nil
}

func (d *DriverLocations) Store(driverUUID string, location LocationDataCRM) {
	d.mx.Lock()
	d.m[driverUUID] = location
	d.mx.Unlock()
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

var (
	currentDriverLocations = NewDriverLocations()
)

func InitCurrentDriverLocations() error {
	var drvLocations []LocationDataCRM
	err := db.Model(&drvLocations).
		Order("created_at").
		Select()
	if err != nil {
		return errors.Wrap(err, "failed to load driver locations")
	}

	for _, loc := range drvLocations {
		if loc.DriverUUID == "" {
			continue
		}
		currentDriverLocations.Store(loc.DriverUUID, loc)
	}
	return nil
}

func CurrentDriverLocationsByUUIDs(uuids []string) map[string]LocationDataCRM {
	result := make(map[string]LocationDataCRM)
	for _, uuid := range uuids {
		if loc, found := currentDriverLocations.Load(uuid); found {
			result[loc.DriverUUID] = loc
		}
	}
	return result
}

func SetDriverLocation(driverUUID string, location LocationDataCRM) error {
	if location.CreatedAt.IsZero() {
		location.CreatedAt = time.Now()
	}
	actualLoc, check := currentDriverLocations.Load(driverUUID)
	if check {
		if err := validateNewLocation(location, actualLoc); err != nil {
			return err
		}
	}
	currentDriverLocations.Store(driverUUID, location)
	return nil
}

func validateNewLocation(new, old LocationDataCRM) error {
	if new.Timestamp < old.Timestamp {
		return errors.Errorf("new coords are older then old ones. old = %v, new = %v", old.Timestamp, new.Timestamp)
	}

	dis := gis.Distance(
		new.Latitude,
		new.Longitude,
		old.Latitude,
		old.Longitude,
	)
	if dis < suspiciousDistance {
		return nil
	}

	timeDelta := new.CreatedAt.Unix() - old.CreatedAt.Unix()

	speed := (dis / float64(timeDelta)) * 3.6 //in km/h
	if speed > maxValidSpeed && timeDelta < locationMaxLifetime {
		return errors.Errorf("incredible speed (%v km/h). Max valid speed = %v", speed, maxValidSpeed)
	}
	return nil
}
