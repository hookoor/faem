package models

import (
	"strconv"
	"strings"
	"time"

	"fmt"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type TicketsCRM struct {
	tableName struct{} `sql:"crm_tickets" pg:",discard_unknown_columns"`
	structures.Ticket
	CreatedAt     time.Time `json:"-"`
	CreatedAtUnix int64     `json:"created_at_unix" sql:"-"`
}
type TicketsFilterCriteria struct {
	OperatorUUID     string                    `query:"operator_uuid"`
	DriverUUID       string                    `query:"driver_uuid"`
	DriverAlias      int                       `query:"driver_alias"`
	OrderID          int                       `query:"order_id"`
	OrderUUID        string                    `query:"order_uuid"`
	StoreUUID        string                    `query:"store_uuid"`
	ClientPhone      string                    `query:"client_phone"`
	SourceType       structures.ChatMembers    `query:"source_type"`
	Status           structures.TicketStatuses `query:"status"`
	Title            string                    `query:"title"`
	Description      string                    `query:"description"`
	Pager            urlvalues.Pager
	TaxiParkUUID     string   `query:"taxi_park_uuid"`
	AllowedTaxiParks []string `json:"-"`
}

func GetTicketByUUID(uuid string) (TicketsCRM, error) {
	var result TicketsCRM
	err := db.Model(&result).
		Where("uuid = ?", uuid).
		Select()
	result.CreatedAtUnix = result.CreatedAt.Unix()
	return result, err
}
func fillSenderName(comment *structures.TicketComment) error {
	var err error
	switch comment.SenderType {
	case structures.UserCRMMember:
		var user UsersCRM
		err = GetByUUID(comment.SenderUUID, &user)
		comment.SenderName = user.Name
	case structures.DriverMember:
		var driver DriverCRM
		err = GetByUUID(comment.SenderUUID, &driver)
		comment.SenderName = fmt.Sprintf("%s (%v)", driver.Name, driver.Alias)
	case structures.ClientMember:
		// если это клиент, то оставляем все как есть
	default:
		err = errors.Errorf("invalid source for comment")
	}
	return err
}
func AddCommentToTicket(uuid string, comment structures.TicketComment) (TicketsCRM, error) {
	var result TicketsCRM
	err := fillSenderName(&comment)
	if err != nil {
		return result, err
	}
	comment.CreatedAtUnix = time.Now().Unix()
	_, err = db.Model(&result).
		Set("comments = COALESCE(comments, '[]'::JSONB) || ?::jsonb", []structures.TicketComment{comment}).
		Where("uuid = ?", uuid).
		Returning("*").
		Update()
	return result, err
}

func (tck *TicketsCRM) Create(sourceData string) error {
	if err := tck.validForCreate(); err != nil {
		return err
	}

	if err := tck.fillNeededData(sourceData); err != nil {
		return err
	}

	if _, err := db.Model(tck).Returning("*").Insert(); err != nil {
		return err
	}

	return nil
}
func (tck *TicketsCRM) validForCreate() error {
	if tck.Title == "" || tck.Description == "" {
		return errors.Errorf("Заголовок и описание обязательны")
	}

	if tck.OrderData.UUID != "" {
		_, err := db.Query(&tck.TaxiParksUUID, `SELECT taxi_park_uuid FROM crm_orders WHERE "uuid"=?`, tck.OrderData.UUID)
		if err != nil {
			return err
		}
	}

	if len(tck.TaxiParksUUID) == 0 {
		query := db.Model((*TaxiParkCRM)(nil)).Column("uuid").Where("name='Стандартный таксопарк'")
		if err := query.Select(&tck.TaxiParksUUID); err != nil {
			return err
		}
	}

	return nil
}

func (tck *TicketsCRM) fillNeededData(sourceData string) error {
	switch tck.SourceType {
	case structures.ClientMember:
		tck.ClientData.Phone = sourceData
	case structures.UserCRMMember:
		tck.OperatorData.UUID = sourceData
	case structures.DriverMember:
		tck.DriverData.UUID = sourceData
	default:
		return errors.Errorf("invalid source")
	}

	if err := tck.fillOperatorData(); err != nil {
		return errors.Wrap(err, "error fill operator data")
	}

	if err := tck.fillDriverData(); err != nil {
		return errors.Wrap(err, "error fill driver data")
	}

	if err := tck.fillStoreData(); err != nil {
		return errors.Wrap(err, "error fill store data")
	}

	tck.UUID = structures.GenerateUUID()
	tck.Status = structures.NewStatus

	return nil
}

// func (tck *TicketsCRM) fillClientData(clientPhone string) error {
// 	tck.ClientData.Phone = clientPhone
// 	return nil
// }

func (tck *TicketsCRM) fillOperatorData() error {
	if tck.OperatorData.UUID == "" {
		return nil
	}
	var user UsersCRM
	err := GetByUUID(tck.OperatorData.UUID, &user)
	tck.OperatorData.Name = user.Name
	return err
}
func (tck *TicketsCRM) fillStoreData() error {
	if tck.StoreData.UUID == "" {
		return nil
	}
	var store StoreCRM
	err := GetByUUID(tck.StoreData.UUID, &store)
	tck.StoreData.Name = store.Name
	return err
}
func ChangeTicketStatus(ticketUUID string, status structures.TicketStatuses) (*TicketsCRM, error) {
	var result TicketsCRM
	_, err := db.Model(&result).
		Where("uuid = ?", ticketUUID).
		Set("status = ?", status).
		Returning("*").
		Update()
	return &result, err
}
func (tck *TicketsCRM) fillDriverData() error {
	if tck.DriverData.UUID == "" {
		return nil
	}
	var driver DriverCRM
	err := GetByUUID(tck.DriverData.UUID, &driver)
	tck.DriverData.Name = driver.Name
	tck.DriverData.Alias = driver.Alias
	return err
}

func (cr *TicketsFilterCriteria) GetTicketsByOptions() (result []TicketsCRM, count int, err error) {
	cr.ClientPhone = removeAllSpaces(cr.ClientPhone)
	query := db.Model(&result)

	if cr.Title != "" {
		query.Where("title ILIKE ?", "%"+cr.Title+"%")
	}
	if cr.DriverUUID != "" {
		query.Where("driver_data ->> 'uuid' = ?", cr.DriverUUID)
	}
	if cr.Description != "" {
		query.Where("description ILIKE ?", "%"+cr.Description+"%")
	}
	if cr.ClientPhone != "" {
		query.Where("client_data ->> 'phone' ILIKE ?", "%"+cr.ClientPhone+"%")
	}
	if cr.OperatorUUID != "" {
		query.Where("operator_data ->> 'uuid' = ?", cr.OperatorUUID)
	}
	if cr.SourceType != "" {
		query.Where("source_type = ?", cr.SourceType)
	}
	if cr.Status != "" {
		query.Where("status = ?", cr.Status)
	}
	if cr.OrderUUID != "" {
		query.Where("order_data ->> 'uuid' = ?", cr.OrderUUID)
	}
	if cr.StoreUUID != "" {
		query.Where("store_data ->> 'uuid' = ?", cr.StoreUUID)
	}
	if cr.DriverAlias != 0 {
		query.Where("driver_data ->> 'alias'::text = ?", strconv.Itoa(cr.DriverAlias))
	}
	if cr.OrderID != 0 {
		query.Where("order_data ->> 'id'::text = ?", strconv.Itoa(cr.OrderID))
	}
	if len(cr.AllowedTaxiParks) != 0 {
		query.Where("taxi_parks_uuid && (?)", pg.Array(cr.AllowedTaxiParks))
	}

	count, err = query.Count()
	if err != nil {
		return
	} else if count == 0 {
		return
	}

	err = query.
		Apply(cr.Pager.Pagination).
		Order("created_at desc").
		Select()
	for i := range result {
		result[i].CreatedAtUnix = result[i].CreatedAt.Unix()
	}
	return
}

func removeAllSpaces(s string) string {
	return strings.Replace(s, " ", "", -1)
}
