package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/config"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// TODO: вынести в конфиг
// const tokenExpiredTime = 1440
// const refreshTokenExpiredMinutes = 201600

// Sessions godoc
type Sessions struct {
	tableName           struct{}  `sql:"crm_sessions"`
	ID                  int       `json:"id" sql:",pk"`
	UserID              int       `json:"user_id"`
	RefreshToken        string    `json:"refresh_token" description:"Токен для обновления"`
	SessionEnd          time.Time `json:"session_end" description:"Дата когда токен отозван"`
	RefreshTokenUsed    time.Time `json:"refresh_token_used" description:"Дата использования токена"`
	RefreshTokenExpired time.Time `json:"refrash_expired" description:"Дата протухания токена"`
	CreatedAt           time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
}

type (
	// LoginRequest requested data when logging in
	LoginRequest struct {
		Login          string `json:"login"`
		Password       string `json:"password"`
		TopSecretField bool   `json:"-" sql:"-"`
	}

	// LoginRefreshRequest godoc
	LoginRefreshRequest struct {
		RefreshToken string `json:"refresh"`
	}

	// TokenClaim JWT token structure
	TokenClaim struct {
		Role          string   `json:"role"`
		UserID        int      `json:"user_id"`
		Login         string   `json:"login"`
		UserUUID      string   `json:"user_uuid"`
		TaxiParksUUID []string `json:"taxi_parks_uuid"`
		RegionsID     []int    `json:"regions_id"`
		StoreUUID     string   `json:"store_uuid"`
		jwt.StandardClaims
	}

	// LoginResponse responsed when requesting token
	LoginResponse struct {
		UserID                     int       `json:"user_id"`
		StoreUUID                  string    `json:"store_uuid"`
		UserUUID                   string    `json:"user_uuid"`
		Token                      string    `json:"token"`
		RefreshToken               string    `json:"refresh_token"`
		RefreshTokenExpiration     time.Time `json:"-"`
		RefreshTokenExpirationUnix int64     `json:"refresh_expiration"`

		TopSecretField bool `json:"-" sql:"-"`
	}
)

// NewRefrashToken generate new session ID
func (logResp *LoginResponse) newRefreshToken(userID int) error {

	newToken, err := uuid.NewV4()
	if err != nil {
		return err
	}

	logResp.UserID = userID
	logResp.RefreshToken = newToken.String()

	dur := time.Minute * time.Duration(config.St.Application.RefreshTokenExpiredM)
	logResp.RefreshTokenExpirationUnix = time.Now().Add(dur).Unix()
	logResp.RefreshTokenExpiration = time.Now().Add(dur)
	err = logResp.saveTokenData(newToken.String())
	if err != nil {
		return err
	}
	return nil
}

// saveTokenData expired existing and create new token for user
func (logResp *LoginResponse) saveTokenData(uuid string) error {

	var sessNew Sessions

	sessNew.UserID = logResp.UserID
	sessNew.RefreshToken = uuid
	sessNew.RefreshTokenExpired = logResp.RefreshTokenExpiration

	_, err := db.Model(&sessNew).Returning("*").Insert()
	if err != nil {
		return errors.Errorf("Ошибка сохранения новой сессии, [%s]", err)
	}

	return nil
}

// GenerateJWT generates new token
func (logResp *LoginResponse) GenerateJWT(user *UsersCRM) error {

	jwtSec := config.JWTSecret()
	mySigningKey := []byte(jwtSec)

	if err := user.attachTaxiParksIfEmpty(); err != nil {
		return err
	}

	dur := time.Minute * time.Duration(config.St.Application.TokenExpiredM)
	if logResp.TopSecretField {
		dur = time.Hour * 24 * 365 * 5
	}
	claims := TokenClaim{
		RegionsID:     user.RegionIDs,
		TaxiParksUUID: user.GetTaxiParksUUID(),
		Role:          user.Role,
		UserID:        user.ID,
		UserUUID:      user.UUID,
		StoreUUID:     user.GetStoreUUID(),
		Login:         user.Login,

		StandardClaims: jwt.StandardClaims{
			IssuedAt:  time.Now().Unix(),
			ExpiresAt: time.Now().Add(dur).Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		return err
	}
	logResp.Token = ss

	return nil
}

// By Maurdekhai, Bagasl, PASPARTUUU 03.03.2021
func (user *UsersCRM) attachTaxiParksIfEmpty() error {
	if len(user.RegionIDs) == 0 {
		return errors.New("Пустое поле списка регионов у пользователя")
	}

	if len(user.TaxiParksUUID) != 0 {
		return nil
	}

	query := db.Model((*TaxiParkCRM)(nil)).Column("uuid").WhereIn("region_id IN (?)", user.RegionIDs)
	if err := query.Select(&user.TaxiParksUUID); err != nil {
		return err
	}

	return nil
}

// AuthenticateUser godoc
func AuthenticateUser(data *LoginRequest) (*LoginResponse, error) {
	user := new(UsersCRM)
	query := db.Model(user).Where("deleted is not true AND login = ?", data.Login)
	if err := query.Select(); err != nil {
		if err == pg.ErrNoRows {
			return nil, errors.Wrapf(err, "Пользователь %s не найден", data.Login)
		}
		return nil, err
	}

	// Comparing the password with the hash
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(data.Password))
	if err != nil {
		return nil, errors.Wrap(err, "Неверный логин или пароль")
	}

	if err := expireUserTokens(user.ID); err != nil {
		return nil, err
	}

	login := new(LoginResponse)
	if err := login.newRefreshToken(user.ID); err != nil {
		return nil, err
	}

	login.TopSecretField = data.TopSecretField
	if err := login.GenerateJWT(user); err != nil {
		return nil, err
	}
	login.UserUUID = user.UUID
	login.StoreUUID = user.GetStoreUUID()

	return login, nil
}

func expireUserTokens(userID int) error {

	var sessOld Sessions

	_, err := db.Model(&sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("user_id = ?", userID).
		Update()

	if err != nil {
		return err
	}
	return nil
}

func expireToken(token string) (UsersCRM, *structures.ErrorWithLevel) {

	var oper UsersCRM
	var sessOld Sessions

	check, err := db.Model(&sessOld).
		Where("refresh_token = ? AND CURRENT_TIMESTAMP < refresh_token_expired AND refresh_token_used is NULL", token).
		Exists()

	if err != nil {
		return oper, structures.Error(err)
	}
	if !check {
		return oper, structures.Warning(errors.Errorf("refresh token not found"))
	}
	_, err = db.Model(&sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("refresh_token = ? AND CURRENT_TIMESTAMP < refresh_token_expired AND refresh_token_used is NULL", token).
		Returning("*").
		Update()
	if err != nil {
		return oper, structures.Error(errors.Errorf("error updating refresh_token_used field,%s", err))
	}
	err = db.Model(&oper).
		Where("deleted is not true AND ID = ?", sessOld.UserID).
		First()
	if err != nil {
		return oper, structures.Error(errors.Errorf("error getting operator,%s", err))
	}
	return oper, nil
}

// RefreshJWTToken godoc
func RefreshJWTToken(token string) (LoginResponse, *structures.ErrorWithLevel) {

	var newLogin LoginResponse

	User, errWL := expireToken(token)
	if errWL != nil {
		return newLogin, errWL
	}

	if err := newLogin.newRefreshToken(User.ID); err != nil {
		return newLogin, structures.Error(err)
	}

	if err := newLogin.GenerateJWT(&User); err != nil {
		return newLogin, structures.Error(err)
	}
	newLogin.UserUUID = User.UUID
	return newLogin, nil
}
