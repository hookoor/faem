package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
)

// DistrictCRM - order district structure
type DistrictCRM struct {
	tableName struct{} `sql:"crm_district"`
	ormStruct
	structures.District
}

// CrmZone - для хранения в базе
type CrmZone struct {
	tableName struct{} `sql:"crm_zones"`
	ID        int      `json:"-"`
	structures.Zone
	CreatedAt time.Time `json:"-" sql:"created_at"`
}

// Create - save structure to database
func (ds *DistrictCRM) Create() (DistrictCRM, error) {
	nDistrict, err := ds.validCreation()
	if err != nil {
		return nDistrict, errors.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nDistrict, err
	}

	nDistrict.UUID = uu.String()
	_, err = db.Model(&nDistrict).Returning("*").Insert()
	if err != nil {
		return nDistrict, errors.Errorf("DB error. %v ", err)
	}
	return nDistrict, nil
}

func (ds *DistrictCRM) validCreation() (DistrictCRM, error) {
	var newDistrict DistrictCRM
	if ds.Name == "" {
		return newDistrict, errors.Errorf("Field name is required")
	}
	newDistrict.Name = ds.Name
	newDistrict.Comment = ds.Comment
	newDistrict.Region = ds.Region
	newDistrict.RegionID = ds.RegionID
	newDistrict.Order = ds.Order

	return newDistrict, nil
}
func GetZonesByUUID(uuid []string) ([]CrmZone, error) {
	if len(uuid) == 0 {
		return nil, nil
	}
	var result []CrmZone
	err := db.Model(&result).
		Where("uuid in (?)", pg.In(uuid)).
		Select()
	return result, err
}

// DistrictByID - return District structure based on ID
func DistrictByID(id int) (DistrictCRM, error) {
	var district DistrictCRM
	district.ID = id
	err := db.Model(&district).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return district, err
	}
	return district, nil
}

// DistrictsList - return all Districts
func DistrictsList() ([]DistrictCRM, error) {
	var districts []DistrictCRM
	err := db.Model(&districts).
		Where("deleted is not true ").
		Select()
	if err != nil {
		return districts, err
	}
	return districts, nil
}

// Update godoc
func (ds *DistrictCRM) Update(uuid string) (DistrictCRM, error) {

	uDistrict, err := ds.validUpdate(uuid)
	if err != nil {
		return uDistrict, errors.Errorf("Data validation error. %v", err)
	}

	err = UpdateByPK(&uDistrict)
	if err != nil {
		return uDistrict, errors.Errorf("DB error. %v", err)
	}
	return uDistrict, nil
}

//
func (ds *DistrictCRM) validUpdate(uuid string) (DistrictCRM, error) {

	flag := false
	var district DistrictCRM

	if ds.Name != "" {
		district.Name = ds.Name
		flag = true
	}
	if ds.Comment != "" {
		district.Comment = ds.Comment
		flag = true
	}
	if ds.RegionID != 0 {
		district.RegionID = ds.RegionID
		flag = true
	}
	if ds.Order != 0 {
		district.Order = ds.Order
		flag = true
	}

	if !flag {
		return district, errors.Errorf("Required fields are empty")
	}
	district.UpdatedAt = time.Now()
	district.UUID = uuid
	err := CheckExistsUUID(&district, uuid)
	if err != nil {
		return district, err
	}
	district.Deleted = false
	return district, nil
}

// // CheckExists return nil if ds.ID not exists or deleted
// // TODO: вынести в общие методы и сделать его универсальным
// func (ds *DistrictCRM) CheckExists(id int) error {
// 	exs, _ := db.Model(ds).Column("id").Where("id = ? AND deleted is not true", id).Exists()
// 	if !exs {
// 		return errors.Errorf("Not found ID=%v", id)
// 	}
// 	return nil
// }

// // BeforeUpdate godoc
// // TODO: вынести в общие методы и сделать его универсальным
// func (ds *DistrictCRM) BeforeUpdate(db orm.DB) error {
// 	ds.UpdatedAt = time.Nds()
// 	return nil
// }

// SetDeleted godoc
func (ds *DistrictCRM) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var district DistrictCRM
	err := CheckExistsUUID(&district, ds.UUID)
	if err != nil {
		return err
	}
	ds.Deleted = true
	_, err = db.Model(ds).WherePK().UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}

// DistrictsByRegion godoc
func DistrictsByRegion(uuid string) ([]DistrictCRM, error) {
	var districts []DistrictCRM
	err := db.Model(&districts).
		Where("region_uuid = ? AND deleted is not true", uuid).
		Select()
	if err != nil {
		return districts, err
	}
	return districts, nil
}
