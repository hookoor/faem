package models

import (
	"time"
	"unicode/utf8"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type PushMailingCRM struct {
	tableName struct{} `sql:"crm_push_mailing"`
	structures.PushMailing
	ID            int       `json:"-"`
	UUID          string    `json:"uuid"`
	CreatorUUID   string    `json:"creator_uuid"`
	CreatedAt     time.Time `json:"-"`
	CreatedAtUnix int64     `json:"created_at_unix" sql:"-"`
}

const (
	maxPushBodySize  int = 110
	maxPushTitleSize int = 40
)

func PushMailingList() ([]PushMailingCRM, error) {
	var result []PushMailingCRM
	query := db.Model(&result).Order("created_at desc")
	if err := query.Select(); err != nil {
		return nil, err
	}

	return result, nil
}
func (pm *PushMailingCRM) Save() error {
	pm.CreatedAt = time.Now()
	err := pm.validate()
	if err != nil {
		return err
	}
	uu, err := uuid.NewV4()
	if err != nil {
		return err
	}
	pm.UUID = uu.String()
	_, err = db.Model(pm).Insert()
	return err
}
func (pm *PushMailingCRM) validate() error {
	if pm.Message == "" || pm.Title == "" {
		return errors.New("Введите заголовок и текст сообщения")
	}
	check := false
	for _, typ := range structures.AllMailingTypes() {
		if pm.Type == typ {
			check = true
			break
		}
	}
	if !check {
		return errors.New("Указан неверный тип рассылки")
	}
	if pm.Type == structures.MailingDriversGroupType && len(pm.TargetsUUIDs) == 0 {
		return errors.New("Для этого типа рассылки необходимо указать водителей")
	}
	if utf8.RuneCountInString(pm.Message) > maxPushBodySize {
		return errors.Errorf("Символов в сообщении не должно быть больше %v", maxPushBodySize)
	}
	if utf8.RuneCountInString(pm.Title) > maxPushTitleSize {
		return errors.Errorf("Символов в заголовке не должно быть больше %v", maxPushTitleSize)
	}
	return nil
}

func FillCreatedAt(data ...PushMailingCRM) {
	for i := range data {
		data[i].CreatedAtUnix = data[i].CreatedAt.Unix()
	}
}

// RestrictPushRecepients -  ограничивает получателей пуша по таксопаркам
// FIXME: проверка на owner'a - костыль, нужно разделить клиентов по таксопаркам
func RestrictPushRecepients(push *PushMailingCRM, userRole string) (*PushMailingCRM, error) {
	switch push.Type {
	case structures.MailingAllClientsType, structures.MailingForFewClientType:
		if userRole != "owner" {
			return nil, errors.Errorf("Только владелец может делать рассылку по клиентам")
		}
		return push, nil
	}

	var (
		drivers []DriverCRM
		err     error
	)

	// Если выбрали регион, но не выбрали таксопарк
	allowedTaxiParks := make([]string, 0)
	query := db.Model((*TaxiParkCRM)(nil)).Column("uuid").
		WhereIn("uuid IN (?)", push.TargetsTaxiparks).
		WhereIn("region_id IN (?)", push.TargetsRegions)
	if err := query.Select(&allowedTaxiParks); err != nil {
		return nil, err
	}

	switch push.Type {
	case structures.MailingAllDriversType:
		drivers, err = GetAllDrivers(allowedTaxiParks...)

	case structures.MailingOfflineDriversType:
		drivers, err = GetDriversUUIDSliceByState(allowedTaxiParks, constants.DriverStateOffline)

	case structures.MailingOnlineDriversType:
		drivers, err = GetDriversUUIDSliceByState(allowedTaxiParks, constants.DriverStateOnline)

	case structures.MailingDriversGroupType:
		return push, nil

	default:
		err = errors.Errorf("unknown push mailing type")
	}
	if err != nil {
		return nil, err
	}

	push.Type = structures.MailingDriversGroupType
	for _, drv := range drivers {
		push.TargetsUUIDs = append(push.TargetsUUIDs, drv.UUID)
	}

	return push, nil
}
