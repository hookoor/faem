package models

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/validation"
)

// SourceOfOrdersCRM - order phoneLine structure
type SourceOfOrdersCRM struct {
	tableName struct{} `sql:"crm_sources_of_orders"`
	ormStructV2

	structures.SourceOfOrders
	Services []ShortService `json:"services,omitempty" sql:"-"`
}

type SourceOfOrdersFilter struct {
	Name          string   `json:"name"`
	SourceTypes   []string `json:"source_types"`
	TaxiparkUUIDs []string `json:"taxipark_uuids"`
	RegionIDs     []int    `json:"region_ids"`
}

// Create - save structure to database
func (s *SourceOfOrdersCRM) Create() error {
	if err := validation.Create(s); err != nil {
		return errors.Wrap(err, "data validation error")
	}
	s.UUID = structures.GenerateUUID()

	if _, err := db.Model(s).Returning("*").Insert(); err != nil {
		return err
	}

	return nil
}

// GetByUUID -
func (s SourceOfOrdersCRM) GetByUUID(uuid string) (SourceOfOrdersCRM, error) {
	err := db.Model(&s).
		Where("uuid = ?", uuid).
		Select()
	if err != nil {
		return SourceOfOrdersCRM{}, errpath.Err(err)
	}
	return s, nil
}

// PhoneLinesList - return all PhoneLines
func PhoneLinesList(taxiParksUUID ...string) ([]SourceOfOrdersCRM, error) {
	res := make([]SourceOfOrdersCRM, 0)
	query := db.Model(&res)

	if len(taxiParksUUID) != 0 {
		query.WhereIn("taxi_park_uuid IN (?)", taxiParksUUID)
	}
	err := query.Select()

	if err != nil {
		return res, err
	}
	return res, nil
}

// SourceOfOrdersList - return all sources of orders
func (flt *SourceOfOrdersFilter) SourceOfOrdersList() ([]SourceOfOrdersCRM, error) {
	res := make([]SourceOfOrdersCRM, 0)
	query := db.Model(&res).Where("name ILIKE ?", "%"+flt.Name+"%")

	if len(flt.TaxiparkUUIDs) == 0 {
		return nil, errors.New("empty taxipark_uuids filter")
	}
	query.WhereIn("taxi_park_uuid IN (?)", flt.TaxiparkUUIDs)

	if len(flt.SourceTypes) != 0 {
		query.WhereIn("source_type IN (?)", flt.SourceTypes)
	}
	if len(flt.RegionIDs) != 0 {
		query.WhereIn("region_id IN (?)", flt.RegionIDs)
	}

	if err := query.Select(); err != nil {
		return nil, err
	}

	return res, nil
}

func GetSourceOfOrdersByUUID(uuid string) (*SourceOfOrdersCRM, error) {
	source := new(SourceOfOrdersCRM)
	if err := db.Model(source).Where("uuid = ?", uuid).Select(); err != nil {
		return nil, err
	}

	return source, nil
}

func GetAsteriskHelloBySourceName(name string) (string, error) {
	var result string
	query := db.Model((*SourceOfOrdersCRM)(nil)).
		Column("asterisk_hello").
		Where("name = ?", name)
	err := query.Select(&result)

	return result, err
}

// Update godoc
func (s *SourceOfOrdersCRM) Update() error {
	query := db.Model(s).WherePK().Returning("*")
	if res, err := query.UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("source with uuid %s was not found", s.UUID)
	}

	return nil
}

// SetDeleted godoc
func (s *SourceOfOrdersCRM) SetDeleted() error {
	if res, err := db.Model(s).WherePK().Delete(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		return errors.Errorf("source with uuid %s was not found", s.UUID)
	}

	return nil
}

func GetPhoneLinesByUUID(uuids []string) (map[string]SourceOfOrdersCRM, error) {
	phoneLinesMap := make(map[string]SourceOfOrdersCRM)
	if len(uuids) < 1 {
		return phoneLinesMap, nil
	}
	var phoneLines []SourceOfOrdersCRM
	err := db.Model(&phoneLines).
		WhereIn("uuid in (?)", uuids).
		Order("created_at desc").
		Select()
	if err != nil {
		return phoneLinesMap, err
	}
	for _, phoneLine := range phoneLines {
		phoneLinesMap[phoneLine.UUID] = phoneLine
	}
	return phoneLinesMap, nil
}

type SrcOfOrdersForwarding struct {
	tableName          struct{} `sql:"crm_sources_of_orders_forwarding,alias:srcf"  pg:",discard_unknown_columns"`
	SourceKey          string   `json:"source_key"`
	SourceOfOrdersUUID string   `json:"source_of_orders_uuid"`
	RegionID           int      `json:"region_id" sql:"-"`
}

func (srcf *SrcOfOrdersForwarding) FillSourceOfOrdersUUID() error {
	query := db.Model(srcf).Column("srcf.source_of_orders_uuid").
		Join("JOIN crm_sources_of_orders AS src ON src.uuid = srcf.source_of_orders_uuid").
		Where("srcf.source_key = ?", srcf.SourceKey).
		Where("src.region_id = ?", srcf.RegionID)
	if err := query.Select(); err != nil {
		return err
	}

	return nil
}
