package models

import (
	"context"
	"fmt"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// ServicesSetCRM -
type ServicesSetCRM struct {
	tableName struct{} `sql:"crm_services_sets" pg:",discard_unknown_columns"`

	structures.ServicesSet

	ormStructV2
}

type ServiceSetItem struct {
	tableName struct{} `sql:"crm_set_to_service,alias:sts" pg:",discard_unknown_columns"`

	SetID            int    `json:"-" sql:",notnull"`
	ServiceUUID      string `json:"service_uuid"`
	ServiceName      string `json:"service_name" sql:"-"`
	Active           bool   `json:"active"` // для отображения клиентам
	// EnableForDriver - активна ли для выбора.
	// условно водитель может видеть неактивную опцию в сером цвете
	EnableForDriver  bool   `json:"enable_for_driver" sql:",notnull"`
	// VisibleForDriver - скрыта для отображения у клиента (логика на фронте)
	VisibleForDriver bool   `json:"visible_for_driver" sql:",notnull"`
	DisplayPriority  int    `json:"display_priority"`
	SurgeRuleID      int    `json:"surge_rule_id" sql:",notnull"`
}

func ServicesListBySourceOfOrdersUUID(uuid string) ([]ServiceCRM, error) {
	var services []ServiceCRM

	query := db.Model((*SourceOfOrdersCRM)(nil)).
		Join("JOIN crm_taxi_parks tp ON taxi_park_uuid = tp.uuid").
		Join("JOIN crm_regions r ON tp.region_id = r.id").
		Join("JOIN crm_set_to_service sts ON r.services_set_id = sts.set_id").
		Join("JOIN crm_services s ON sts.service_uuid = s.uuid").
		Where("source_of_orders_crm.uuid = ?", uuid).
		Where("sts.active").
		Order("sts.display_priority DESC", "s.display_priority DESC").
		Column("s.*")
	if err := query.Select(&services); err != nil {
		return nil, err
	}

	return services, nil
}

func CreateServicesSet(tx *pg.Tx, name string) (*ServicesSetCRM, error) {
	var set ServicesSetCRM
	set.Name = name

	// инсертим набор
	query := tx.Model(&set).
		Column("name").
		Returning("*")
	if _, err := query.Insert(); err != nil {
		return nil, err
	}

	// получаем UUID всех неудаленных услуг
	var services []ShortService
	query = tx.Model((*ServiceCRM)(nil)).
		Column("uuid", "name").
		Where("deleted IS NOT true")
	if err := query.Select(&services); err != nil {
		return nil, err
	}

	// создаем правило повыш. спроса для каждой услуги в новом наборе
	surgeRules := make([]SurgeRuleCRM, len(services))
	for i := range surgeRules {
		surgeRules[i] = SurgeRuleCRM{
			Name: fmt.Sprintf("%s: %s", name, services[i].Name),
		}
	}
	query = tx.Model(&surgeRules).
		Column("name").
		Returning("*")
	if _, err := query.Insert(); err != nil {
		return nil, err
	}

	// привязываем услуги к новому набору
	setItems := make([]ServiceSetItem, len(services))
	for i, service := range services {
		setItems[i] = ServiceSetItem{
			SetID:       set.ID,
			ServiceUUID: service.UUID,
			// TODO: surge_rules
			SurgeRuleID: surgeRules[i].ID,
		}
	}
	query = tx.Model(&setItems)
	if _, err := query.Insert(); err != nil {
		return nil, err
	}

	return &set, nil
}

func GetServiceSet(ctx context.Context, setID int) ([]ServiceSetItem, error) {
	items := make([]ServiceSetItem, 0)
	query := db.ModelContext(ctx, &items).Column("sts.*").ColumnExpr("s.name AS service_name").
		Join("JOIN crm_services AS s ON sts.service_uuid = s.uuid").
		Where("s.deleted is not true").
		Where("set_id = ?", setID).
		Order("display_priority DESC")
	if err := query.Select(); err != nil {
		return nil, err
	}

	return items, nil
}

func UpdateServiceSet(ctx context.Context, setID int, items []ServiceSetItem) error {
	// перезаписываем приоритет
	for i := range items {
		items[i].DisplayPriority = len(items) - i
	}

	query := db.ModelContext(ctx, &items).
		ExcludeColumn("set_id").
		Where("set_id = ?", setID).
		Where("sts.service_uuid = ?service_uuid")
	if _, err := query.Update(); err != nil {
		return err
	}

	return nil
}

type ShortService struct {
	UUID string `json:"uuid" sql:",nopk"`
	Name string `json:"name"`
}

func (s *SourceOfOrdersCRM) FillServices() error {
	query := db.Model((*RegionCRM)(nil)).
		Column("s.uuid", "s.name").
		Join("JOIN crm_set_to_service sts ON services_set_id = sts.set_id").
		Join("JOIN crm_services s ON sts.service_uuid = s.uuid").
		Where("region_crm.id = ?", s.RegionID).
		Where("sts.active IS true").
		Order("sts.display_priority DESC")
	if err := query.Select(&s.Services); err != nil {
		return err
	}

	return nil
}
