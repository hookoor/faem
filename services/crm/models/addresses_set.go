package models

import (
	"context"
	"fmt"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/postgre"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
)

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// ISetToAddress -
type ISetToAddress interface {
	// Create -
	// GetByID - получение по id
	GetByID(ctx context.Context, id int) (*SetToAddress, error)
	// GetFilteredList -
	GetFilteredList(ctx context.Context, filter *SetToAddressFilter) (*tool.PaginatorResult, error)
	// Update -
	Update(ctx context.Context, sta *SetToAddress, id int) error
	// Delete -
	Delete(ctx context.Context, id int) error

	ISetToAddressTemporary
}

// ISetToAddressTemporary - временные методы, заменить на более правильное взаимодействие с сущностью
type ISetToAddressTemporary interface {
	// CreateCityRelation -
	// временное решение
	CreateCityRelation(ctx context.Context, setID *int, cityName *string, weight *int) (*SetToAddress, error)
}

// SetToAddressModel -
type SetToAddressModel struct {
	db           orm.DB
	dbQueryTrace *postgre.DBQueryTraceHook
}

// NewSetToAddressModel -
func NewSetToAddressModel(conn *pg.DB, dbQueryTrace *postgre.DBQueryTraceHook) *SetToAddressModel {
	regInst := SetToAddressModel{
		db:           conn,
		dbQueryTrace: dbQueryTrace,
	}
	return &regInst
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// SetToAddress - таблица сопоставляющая регион и приоритетные адреса (по городу)
type SetToAddress struct {
	tableName struct{} `sql:"crm_set_to_address"`

	ID        int          `json:"id"`
	SetID     int          `json:"set_id"`
	AddressID *postgre.Int `json:"-"`
	// Weight - вес для расчета приоритета выдачи адреса при автокомплите
	Weight   int    `json:"weight"`
	CityName string `json:"city_name"`
}

// SetToAddressFilter -
type SetToAddressFilter struct {
	ID    *int `json:"id" query:"id"`
	SetID *int `json:"set_id" query:"set_id"`

	Pager tool.Paginator `json:"pager"`
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------

// GetByID -
func (sm *SetToAddressModel) GetByID(ctx context.Context, id int) (*SetToAddress, error) {
	var sets SetToAddress
	err := sm.db.ModelContext(ctx, &sets).
		Where("id = ?", id).
		Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return &sets, nil
}

// GetFilteredList -
func (sm *SetToAddressModel) GetFilteredList(ctx context.Context, filter *SetToAddressFilter) (*tool.PaginatorResult, error) {
	err := filter.Pager.Fasten()
	if err != nil {
		return nil, errpath.Err(err)
	}

	var sets []SetToAddress
	query := sm.db.ModelContext(ctx, &sets).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			if filter.ID != nil {
				q.Where("id = ?", filter.ID)
			}
			if filter.SetID != nil {
				q.Where("set_id = ?", filter.SetID)
			}

			return q, nil
		})

	rowsCount, err := query.Count()
	if err != nil {
		return nil, errpath.Err(err)
	}
	if rowsCount != 0 {
		if err = query.Apply(filter.Pager.Pager.Pagination).Select(); err != nil {
			return nil, errpath.Err(err)
		}
	}

	res, err := tool.GetPaginatorResult(sets, rowsCount)
	if err != nil {
		return nil, errpath.Err(err)
	}
	return res, nil
}

// Update -
// TODO - нельзя давать полностью редачить, только город и вес для 
func (sm *SetToAddressModel) Update(ctx context.Context, sta *SetToAddress, id int) error {
	query := sm.db.ModelContext(ctx, sta).
		Where("id = ?", id).
		Returning("*")
	if res, err := query.UpdateNotNull(); err != nil {
		return errpath.Err(err)
	} else if res.RowsAffected() == 0 {
		return errpath.Err(err, fmt.Sprintf("set to addresses with id=%d doesn't exist", sta.ID))
	}

	return nil
}

// Delete -
func (sm *SetToAddressModel) Delete(ctx context.Context, id int) error {
	_, err := sm.db.ModelContext(ctx, (*SetToAddress)(nil)).
		Where("id = ?", id).
		Delete()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// ---

// CreateCityRelation -
func (sm *SetToAddressModel) CreateCityRelation(ctx context.Context, setID *int, cityName *string, weight *int) (*SetToAddress, error) {

	{ // validate
		if setID == nil {
			return nil, errpath.Errorf("set id is empty")
		}
		if cityName == nil {
			return nil, errpath.Errorf("city name is empty")
		}
		if weight == nil {
			*weight = 0
		}
	}

	var n postgre.Int = 0

	setTo := SetToAddress{
		SetID:     *setID,
		CityName:  *cityName,
		AddressID: &n,
		Weight:    *weight,
	}

	_, err := sm.db.ModelContext(ctx, &setTo).Returning("*").Insert()
	if err != nil {
		return nil, errpath.Err(err)
	}

	return &setTo, nil
}

// -------------------------------------------------
// -------------------------------------------------
// -------------------------------------------------
