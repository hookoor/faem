package models

import (
	"time"

	"github.com/pkg/errors"

	"github.com/gofrs/uuid"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type QuickMessages struct {
	tableName struct{}               `sql:"crm_quick_messages"`
	UUID      string                 `json:"uuid"`
	ID        int                    `json:"-" sql:",pk"`
	Message   string                 `json:"message"`
	Receiver  structures.ChatMembers `json:"receiver"`
	CreatedAt time.Time              `sql:"default:now()" json:"-" description:"Дата создания"`
	Deleted   bool                   `json:"-" sql:"default:false"`
}

func QuickMessagesListByRequesting(requestingType structures.ChatMembers) ([]QuickMessages, error) {
	var result []QuickMessages
	query := db.Model(&result).
		Order("created_at desc").
		Where("deleted is not true")
	if requestingType == structures.DriverMember {
		query = query.
			Where("receiver = ? or receiver = ?", structures.UserCRMMember, structures.ClientMember)
	}
	if requestingType == structures.ClientMember {
		query = query.
			Where("receiver = ?", structures.DriverMember)
	}

	err := query.
		Select()
	return result, err
}
func (qm *QuickMessages) Save() error {
	qm.CreatedAt = time.Now()
	err := qm.validate()
	if err != nil {
		return err
	}
	uu, err := uuid.NewV4()
	if err != nil {
		return err
	}
	qm.UUID = uu.String()
	_, err = db.Model(qm).Insert()
	return err
}
func (qm *QuickMessages) validate() error {
	check := false
	for _, chm := range structures.AllChatMembers() {
		if chm == qm.Receiver {
			check = true
			break
		}
	}
	if !check {
		return errors.New("invalid receiver")
	}
	if qm.Message == "" {
		return errors.New("Сообщение не может быть пустым")
	}
	return nil
}

func SetQuickMessageDeleted(uuid string) error {
	_, err := db.Model(&QuickMessages{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}
func (qm *QuickMessages) Update(uuid string) error {

	err := qm.validate()
	if err != nil {
		return err
	}
	qm.UUID = uuid
	err = UpdateByPK(qm)
	if err != nil {
		return errors.Errorf("DB error. %v", err)
	}
	return nil
}
