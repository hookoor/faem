package models

import "testing"

var distanceTest int

func BenchmarkDistance(b *testing.B) {
	var dist int
	for i := 0; i < b.N; i++ {
		dist = LevenshteinDistance("Хаджи Мамсу", "Хаджи Мамсурова, 114")
	}
	distanceTest = dist
}
