// +build ignore

package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
)

func main() {
	// Parse flags
	configPath := flag.String("config", "", "configuration file path")
	importPath := flag.String("import", "", "import file path")
	flag.Parse()

	cfg, err := config.ParseConfig(*configPath)
	if err != nil {
		log.Fatalf("failed to parse the config file: %v", err)
	}
	log.Printf("%+v", cfg)

	// Open the file
	csvFile, err := os.Open(*importPath)
	if err != nil {
		log.Fatalf("failed to open the csv file: %v", err)
	}

	// Parse the CSV
	log.Println("reading csv file...")

	var products []crmModels.ProductCRM
	trueVAl := true
	csvReader := csv.NewReader(csvFile)
	for {
		line, err := csvReader.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatalf("failed to read from the csv file: %v", err)
		}
		// for i := range line {
		// 	fmt.Println(line[i], "===", i)
		// }
		// log.Fatalf("")
		var product crmModels.ProductCRM
		storeUUID := "ae4cb22a-e0ac-455d-8b27-d3be1226b189"
		product.Name = line[0]
		product.Category = structures.ProductCategory(line[41])
		product.StoreUUID = storeUUID
		product.Image = line[37]
		product.Comment = line[3]
		product.Price, _ = strconv.Atoi(line[14])
		product.Available = &trueVAl
		product.Weight = 300
		products = append(products, product)
	}

	log.Println("done")

	if len(products) < 1 {
		log.Fatal("no rows in import file parsed")
	}

	addr := fmt.Sprintf("%s:%v", "", "5432")
	crmDB := pg.Connect(&pg.Options{
		Addr:     addr,
		User:     "admin",
		Password: "pass",
		Database: "name",
	})
	if err != nil {
		log.Fatalf("failed to create a crm db instance: %v", err)
	}
	defer crmDB.Close()

	// Upsert records to the databases one-by-one
	log.Println("inserting records to the crm db...")

	_, err = crmDB.Model(&products).Insert()
	if err != nil {
		log.Fatalf("failed to insert data to the crm db: %v", err)
	}
	crmModels.ConnectDB(crmDB)
	log.Println("inserting categories to crm db...")
	for _, prod := range products {
		crmModels.CreatNewProductCategoryIfNeed(prod.Category, prod.StoreUUID)
		if err != nil {
			log.Fatalf("failed to insert new category: %v", err)
		}
	}

	log.Println("done")
}
