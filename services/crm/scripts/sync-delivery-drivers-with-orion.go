// +build ignore

package main

//import (
//	"bytes"
//	"encoding/csv"
//	"encoding/json"
//	"flag"
//	"io"
//	"io/ioutil"
//	"log"
//	"net/http"
//	"os"
//	"strconv"
//	"strings"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//
//	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/crm/models"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//)
//
//const (
//	colID           = 1
//	colNameAndPhone = 2
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	importPath := flag.String("import", "", "import file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Open the file
//	csvFile, err := os.Open(*importPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Connect to the databases and remember to close them
//	crmDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
//		User:     cfg.Databases.CRM.User,
//		Password: cfg.Databases.CRM.Password,
//		Database: cfg.Databases.CRM.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer crmDB.Close()
//	models.ConnectDB(crmDB)
//
//	// Get default tariff
//	var deliveryTariff models.DriverTariffCrm
//	if err = crmDB.Model(&deliveryTariff).
//		Column("uuid").
//		Where("name = ?", "Доставка").
//		Limit(1).
//		Select(); err != nil {
//		log.Fatalf("failed to get the delivery tariff: %v", err)
//	}
//
//	// Get default service
//	var deliveryService models.ServiceCRM
//	if err = crmDB.Model(&deliveryService).
//		Column("uuid").
//		Where("tag = ARRAY['delivery']").
//		Limit(1).
//		Select(); err != nil {
//		log.Fatalf("failed to get the delivery service: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading csv file...")
//
//	deliveryDrivers := make(map[string]*models.DriverCRM)
//	var driverPhones []string
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[colID])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		name, phone := nameAndPhone(line[colNameAndPhone])
//		formattedPhone, err := phoneverify.NumberVerify(phone)
//		if err != nil {
//			log.Printf("[ERROR] invalid phone: %s for driver %s \n", phone, name)
//			continue
//		}
//		maxServiceLevel := 0
//
//		driver := models.DriverCRM{
//			Driver: structures.Driver{
//				MaxServiceLevel: &maxServiceLevel,
//				Tag:             []string{"delivery"},
//			},
//			TariffUUID: deliveryTariff.UUID,
//		}
//		deliveryDrivers[formattedPhone] = &driver
//		driverPhones = append(driverPhones, formattedPhone)
//	}
//
//	log.Println("done")
//
//	if len(driverPhones) < 1 {
//		log.Fatal("no drivers to update")
//	}
//
//	// Get driver's uuids
//	var existingDrivers []*models.DriverCRM
//	if err = crmDB.Model(&existingDrivers).
//		Column("uuid", "phone").
//		WhereIn("phone IN (?)", driverPhones).
//		Select(); err != nil {
//		log.Fatalf("failed to get the delivery drivers: %v", err)
//	}
//
//	if len(existingDrivers) < 1 {
//		log.Fatal("no drivers loaded to update")
//	}
//
//	for i, drv := range existingDrivers {
//		delDrv := deliveryDrivers[drv.Phone]
//		if delDrv == nil {
//			log.Printf("[ERROR] no driver with phone %s", drv.Phone)
//		}
//		if err := request("PUT", cfg.CRM.Host+"/drivers/"+drv.UUID, cfg.CRM.JWT, delDrv, nil); err != nil {
//			log.Printf("[ERROR] failed to update a driver: %v", err)
//		}
//		log.Println(i)
//	}
//}
//
//func nameAndPhone(str string) (name string, phone string) {
//	str = strings.TrimSpace(str)
//	split := strings.Split(str, " (")
//	if len(split) < 2 {
//		name = str
//		return
//	}
//	name = strings.TrimSpace(split[0])
//	phone = formatPhone(split[1])
//	return
//}
//
//func formatPhone(str string) string {
//	phone := strings.TrimSpace(str)
//	phone = strings.TrimSuffix(phone, ")")
//	if strings.HasPrefix(phone, "8") {
//		phone = "+7" + phone[1:]
//	}
//	return phone
//}
//
//func request(method, url, jwt string, payload, response interface{}) error {
//	body, err := json.Marshal(payload)
//	if err != nil {
//		return errors.Wrap(err, "failed to marshal a payload")
//	}
//
//	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
//	if err != nil {
//		return errors.Wrap(err, "failed to create an http request")
//	}
//	req.Header.Set("Content-Type", "application/json")
//	req.Header.Set("Authorization", "Bearer "+jwt)
//
//	resp, err := http.DefaultClient.Do(req)
//	if err != nil {
//		return errors.Wrap(err, "failed to make a post request")
//	}
//	defer resp.Body.Close()
//
//	if resp.StatusCode >= http.StatusBadRequest {
//		body, err := ioutil.ReadAll(resp.Body)
//		if err != nil {
//			return errors.Errorf("request to %s failed with status: %d", url, resp.StatusCode)
//		}
//		return errors.Errorf(
//			"request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body),
//		)
//	}
//	if response != nil {
//		return json.NewDecoder(resp.Body).Decode(response)
//	}
//	return nil
//}
