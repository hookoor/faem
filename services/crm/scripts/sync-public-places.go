// +build ignore

package main

//import (
//	"encoding/csv"
//	"flag"
//	"io"
//	"log"
//	"os"
//	"strconv"
//	"strings"
//
//	"github.com/go-pg/pg"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	importPath := flag.String("import", "", "import file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Open the file
//	csvFile, err := os.Open(*importPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading csv file...")
//
//	var crmPlaces []*crmModels.PublicPlace
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[0])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		lat, _ := strconv.ParseFloat(strings.ReplaceAll(line[8], ",", "."), 64)
//		lon, _ := strconv.ParseFloat(strings.ReplaceAll(line[9], ",", "."), 64)
//		priority, _ := strconv.Atoi(line[14])
//
//		crmPlace := crmModels.PublicPlace{
//			ID:           id,
//			FullName:     strings.TrimSpace(line[1]),
//			Category:     strings.TrimSpace(line[2]),
//			Street:       strings.TrimSpace(line[3]),
//			Building:     strings.TrimSpace(line[4]),
//			Comment:      strings.TrimSpace(line[5]),
//			SpecialOrder: strings.TrimSpace(line[6]),
//			OutOfTown:    strings.TrimSpace(line[7]),
//			Latitude:     float32(lat),
//			Longitude:    float32(lon),
//			HideAddress:  strings.TrimSpace(line[10]),
//			Region:       strings.TrimSpace(line[11]),
//			Synonym:      strings.TrimSpace(line[12]),
//			Name:         strings.TrimSpace(line[13]),
//			Priority:     priority,
//		}
//		crmPlaces = append(crmPlaces, &crmPlace)
//	}
//
//	log.Println("done")
//
//	if len(crmPlaces) < 1 {
//		log.Fatal("no rows in import file parsed")
//	}
//
//	// Connect to the databases and remember to close them
//	crmDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
//		User:     cfg.Databases.CRM.User,
//		Password: cfg.Databases.CRM.Password,
//		Database: cfg.Databases.CRM.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a crm db instance: %v", err)
//	}
//	defer crmDB.Close()
//
//	// Upsert records to the databases one-by-one
//	log.Println("inserting records to the crm db...")
//
//	var iPlaces []interface{}
//	for _, place := range crmPlaces {
//		iPlaces = append(iPlaces, place)
//	}
//	_, err = crmDB.Model(iPlaces...).Insert()
//	if err != nil {
//		log.Fatalf("failed to insert places to the crm db: %v", err)
//	}
//
//	log.Println("done")
//}
