package config

import (
	"github.com/pkg/errors"
	"github.com/spf13/viper"
)

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

type Databases struct {
	CRM    Database
	Driver Database
}

type CRM struct {
	Host string
	JWT  string
}

type Config struct {
	Databases Databases
	CRM       CRM
}

func ParseConfig(filepath string) (Config, error) {
	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return Config{}, errors.Wrap(err, "failed to read the config file")
	}

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return Config{}, errors.Wrap(err, "failed to unmarshal the configuration")
	}
	return cfg, nil
}
