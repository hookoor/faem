// +build ignore

package main

//import (
//	"encoding/csv"
//	"flag"
//	"fmt"
//	"io"
//	"log"
//	"os"
//	"strconv"
//	"strings"
//
//	"github.com/go-pg/pg"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//)
//
//const (
//	defaultCity = "Владикавказ"
//)
//
//var (
//	splitCities = map[string]struct{}{
//		defaultCity: {},
//		"Нальчик":   {},
//	}
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	importPath := flag.String("import", "", "import file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Open the file
//	csvFile, err := os.Open(*importPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading csv file...")
//
//	var crmAddresses []*crmModels.Address
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[0])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		city, street := extractCityAndStreet(line[1])
//		house := line[2]
//		lat, _ := strconv.ParseFloat(strings.ReplaceAll(line[3], ",", "."), 64)
//		lon, _ := strconv.ParseFloat(strings.ReplaceAll(line[4], ",", "."), 64)
//
//		crmAddress := crmModels.Address{
//			City:        city,
//			Street:      street,
//			House:       house,
//			FullAddress: fmt.Sprintf("%s %s", street, house),
//			Lat:         float32(lat),
//			Long:        float32(lon),
//		}
//		crmAddresses = append(crmAddresses, &crmAddress)
//	}
//
//	log.Println("done")
//
//	if len(crmAddresses) < 1 {
//		log.Fatal("no rows in import file parsed")
//	}
//
//	// Connect to the databases and remember to close them
//	crmDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
//		User:     cfg.Databases.CRM.User,
//		Password: cfg.Databases.CRM.Password,
//		Database: cfg.Databases.CRM.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a crm db instance: %v", err)
//	}
//	defer crmDB.Close()
//
//	// Upsert records to the databases one-by-one
//	log.Println("inserting records to the crm db...")
//
//	var iAddresses []interface{}
//	for _, addr := range crmAddresses {
//		iAddresses = append(iAddresses, addr)
//	}
//	_, err = crmDB.Model(iAddresses...).Insert()
//	if err != nil {
//		log.Fatalf("failed to insert addresses to the crm db: %v", err)
//	}
//
//	log.Println("done")
//}
//
//func extractCityAndStreet(str string) (city string, street string) {
//	str = strings.TrimSpace(str)
//	split := strings.SplitN(str, ", ", 2)
//	if len(split) < 2 {
//		city = defaultCity
//		street = strings.TrimSpace(split[0])
//		return
//	}
//	city = strings.TrimSpace(split[0])
//	street = str
//	if _, found := splitCities[city]; found {
//		street = strings.TrimSpace(split[1])
//	}
//	return
//}
