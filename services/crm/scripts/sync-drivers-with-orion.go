// +build ignore

package main

//import (
//	"bytes"
//	"encoding/csv"
//	"encoding/json"
//	"flag"
//	"io"
//	"io/ioutil"
//	"log"
//	"net/http"
//	"os"
//	"strconv"
//	"strings"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//
//	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/pkg/constants"
//	"gitlab.com/faemproject/backend/faem/services/crm/models"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	importPath := flag.String("import", "", "import file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Open the file
//	csvFile, err := os.Open(*importPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Connect to the databases and remember to close them
//	crmDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
//		User:     cfg.Databases.CRM.User,
//		Password: cfg.Databases.CRM.Password,
//		Database: cfg.Databases.CRM.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer crmDB.Close()
//	models.ConnectDB(crmDB)
//
//	// Get default tariff
//	var defaultTariff models.DriverTariffCrm
//	if err = crmDB.Model(&defaultTariff).
//		Column("uuid").
//		Where("\"default\" is TRUE").
//		Limit(1).
//		Select(); err != nil {
//		log.Fatalf("failed to get the default tariff: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading csv file...")
//
//	var orionDrivers []*models.DriverCRM
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[3])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		name, phone := nameAndPhone(line[2])
//		formattedPhone, err := phoneverify.NumberVerify(phone)
//		if err != nil {
//			log.Printf("[ERROR] invalid phone: %s for driver %s \n", phone, name)
//			continue
//		}
//
//		blocked, blockedTemporary, reason := isBlocked(line[7]), isBlocked(line[8]), strings.TrimSpace(line[16])
//		autoColor, autoName, autoNumber := autoInfo(line[10])
//		registrationAddress := strings.TrimSpace(line[11])
//		driverLicense := strings.TrimSpace(line[12])
//		passport := strings.TrimSpace(line[13])
//		comment := "Импортирован из ORION"
//		status := variables.DriverStateAvailable
//		if blockedTemporary {
//			comment += ", не прошел фотоосмотр"
//		}
//		if blocked {
//			comment += ", был заблокирован"
//			status = variables.DriverStateBlocked
//		}
//		if reason != "" {
//			comment += ", комментарий: " + reason
//		}
//		driver := models.DriverCRM{
//			Driver: structures.Driver{
//				Name:      name,
//				Phone:     formattedPhone,
//				Comment:   comment,
//				Car:       autoName,
//				Color:     autoColor,
//				RegNumber: autoNumber,
//				Tag:       []string{"standart"},
//				Status:    status,
//			},
//			DrivingLicenseNumber: driverLicense,
//			PasportSeriesNumber:  passport,
//			RegistrationAddress:  registrationAddress,
//			LivingAddress:        registrationAddress,
//			TariffUUID:           defaultTariff.UUID,
//		}
//
//		orionDrivers = append(orionDrivers, &driver)
//	}
//
//	log.Println("done")
//
//	// Load all the drivers
//	var drivers []*models.DriverCRM
//	if err = crmDB.Model(&drivers).
//		Column("phone", "pasport_series_number").
//		Select(); err != nil {
//		log.Fatalf("failed to select all drivers: %v", err)
//	}
//
//	// Make two sets to identify known drivers - by phone and by passport
//	knownPhones, knownPassports := make(map[string]struct{}), make(map[string]struct{})
//	for _, drv := range drivers {
//		knownPhones[drv.Phone] = struct{}{}
//		knownPassports[drv.PasportSeriesNumber] = struct{}{}
//	}
//
//	// Filter the unknown drivers
//	var (
//		knownDrivers   []*models.DriverCRM
//		unknownDrivers []*models.DriverCRM
//	)
//	for _, drv := range orionDrivers {
//		if _, ok := knownPhones[drv.Phone]; ok {
//			knownDrivers = append(knownDrivers, drv)
//			continue
//		}
//		if _, ok := knownPassports[drv.PasportSeriesNumber]; ok {
//			knownDrivers = append(knownDrivers, drv)
//			continue
//		}
//		unknownDrivers = append(unknownDrivers, drv)
//	}
//
//	//insertDrivers(cfg, unknownDrivers)
//	//updateEmptyDrivers(crmDB, cfg, knownDrivers)
//	updateEmptyComments(crmDB, cfg, orionDrivers)
//}
//
//func insertDrivers(cfg config.Config, unknownDrivers []*models.DriverCRM) {
//	if len(unknownDrivers) < 0 {
//		log.Println("no new drivers")
//		return
//	}
//
//	log.Print("importing drivers...")
//
//	// Iterate through the unknown drivers and save them to the db
//	for i := 0; i < 500 && i < len(unknownDrivers); i++ {
//		drv := unknownDrivers[i]
//		if err := request("POST", cfg.CRM.Host+"/drivers", "", drv, nil); err != nil {
//			log.Printf("[ERROR] failed to import a driver: %v", err)
//		}
//		log.Println(i)
//	}
//
//	log.Print("done")
//}
//
//func updateEmptyDrivers(crmDB *pg.DB, cfg config.Config, knownDrivers []*models.DriverCRM) {
//	if len(knownDrivers) < 0 {
//		log.Println("no drivers to update")
//		return
//	}
//
//	getKnownDriver := func(phone, passport string) *models.DriverCRM {
//		for _, drv := range knownDrivers {
//			if phone != "" && drv.Phone == phone {
//				return drv
//			}
//			if passport != "" && drv.PasportSeriesNumber == passport {
//				return drv
//			}
//		}
//		return nil
//	}
//
//	// Get empty drivers
//	var emptyDrivers []*models.DriverCRM
//	if err := crmDB.Model(&emptyDrivers).
//		Where("name is null").
//		Select(); err != nil {
//		log.Fatalf("failed to select empty drivers: %v", err)
//	}
//
//	log.Print("updating drivers...")
//
//	for i := 0; i < 500 && i < len(emptyDrivers); i++ {
//		emptyDrv := emptyDrivers[i]
//		knownDrv := getKnownDriver(emptyDrv.Phone, emptyDrv.PasportSeriesNumber)
//		if knownDrv == nil {
//			log.Println(i)
//			continue
//		}
//
//		updateDrv := models.DriverCRM{
//			Driver: structures.Driver{
//				Name:      knownDrv.Name,
//				Comment:   knownDrv.Comment,
//				Car:       knownDrv.Car,
//				Color:     knownDrv.Color,
//				RegNumber: knownDrv.RegNumber,
//				Tag:       knownDrv.Tag,
//			},
//			DrivingLicenseNumber: knownDrv.DrivingLicenseNumber,
//			PasportSeriesNumber:  knownDrv.PasportSeriesNumber,
//			RegistrationAddress:  knownDrv.RegistrationAddress,
//			LivingAddress:        knownDrv.LivingAddress,
//		}
//		if err := request("PUT", cfg.CRM.Host+"/drivers/"+emptyDrv.UUID, cfg.CRM.JWT, &updateDrv, nil); err != nil {
//			log.Printf("[ERROR] failed to update a driver: %v", err)
//		}
//		log.Println(i)
//	}
//
//	log.Print("done")
//}
//
//func updateEmptyComments(crmDB *pg.DB, cfg config.Config, orionDrivers []*models.DriverCRM) {
//	if len(orionDrivers) < 0 {
//		log.Println("no drivers to update a comment")
//		return
//	}
//
//	var driversWithComments []*models.DriverCRM
//	for _, drv := range orionDrivers {
//		if drv.Comment != "" {
//			driversWithComments = append(driversWithComments, drv)
//		}
//	}
//
//	getKnownDriver := func(phone, passport string) *models.DriverCRM {
//		for _, drv := range driversWithComments {
//			if phone != "" && drv.Phone == phone {
//				return drv
//			}
//			if passport != "" && drv.PasportSeriesNumber == passport {
//				return drv
//			}
//		}
//		return nil
//	}
//
//	// Get empty drivers
//	var driversWithEmptyComments []*models.DriverCRM
//	if err := crmDB.Model(&driversWithEmptyComments).
//		Where("comment is null or comment = ''").
//		Select(); err != nil {
//		log.Fatalf("failed to select drivers with empty comments: %v", err)
//	}
//
//	log.Print("updating drivers...")
//
//	for i := 0; i < 500 && i < len(driversWithEmptyComments); i++ {
//		drvWithEmptyComment := driversWithEmptyComments[i]
//		knownDrv := getKnownDriver(drvWithEmptyComment.Phone, drvWithEmptyComment.PasportSeriesNumber)
//		if knownDrv == nil {
//			log.Println(i)
//			continue
//		}
//
//		updateDrv := models.DriverCRM{
//			Driver: structures.Driver{Comment: knownDrv.Comment},
//		}
//		if knownDrv.Status == variables.DriverStateBlocked {
//			updateDrv.Status = knownDrv.Status
//		}
//		if err := request("PUT", cfg.CRM.Host+"/drivers/"+drvWithEmptyComment.UUID, cfg.CRM.JWT, &updateDrv, nil); err != nil {
//			log.Printf("[ERROR] failed to update a driver: %v", err)
//		}
//		log.Println(i)
//	}
//
//	log.Print("done")
//}
//
//func nameAndPhone(str string) (name string, phone string) {
//	str = strings.TrimSpace(str)
//	split := strings.Split(str, " (")
//	if len(split) < 2 {
//		name = str
//		return
//	}
//	name = strings.TrimSpace(split[0])
//	phone = strings.TrimSpace(split[1])
//	phone = strings.TrimSuffix(phone, ")")
//	if strings.HasPrefix(phone, "8") {
//		phone = "+7" + phone[1:]
//	}
//	return
//}
//
//func isBlocked(str string) bool {
//	return strings.TrimSpace(str) != "Нет"
//}
//
//func autoInfo(str string) (color, name, number string) {
//	str = strings.TrimSpace(str)
//	split := strings.Split(str, ", ")
//	if len(split) < 2 {
//		name = str
//		return
//	}
//
//	// Extract color and name
//	name = strings.TrimSpace(split[0])
//	nameSplit := strings.Split(name, " ")
//	if len(nameSplit) > 1 {
//		color = nameSplit[0]
//		name = nameSplit[1]
//	}
//
//	// Extract number
//	number = strings.TrimSpace(split[1])
//	number = strings.TrimPrefix(number, "номер ")
//	return
//}
//
//func request(method, url, jwt string, payload, response interface{}) error {
//	body, err := json.Marshal(payload)
//	if err != nil {
//		return errors.Wrap(err, "failed to marshal a payload")
//	}
//
//	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
//	if err != nil {
//		return errors.Wrap(err, "failed to create an http request")
//	}
//	req.Header.Set("Content-Type", "application/json")
//	req.Header.Set("Authorization", "Bearer "+jwt)
//
//	resp, err := http.DefaultClient.Do(req)
//	if err != nil {
//		return errors.Wrap(err, "failed to make a post request")
//	}
//	defer resp.Body.Close()
//
//	if resp.StatusCode >= http.StatusBadRequest {
//		body, err := ioutil.ReadAll(resp.Body)
//		if err != nil {
//			return errors.Errorf("request to %s failed with status: %d", url, resp.StatusCode)
//		}
//		return errors.Errorf(
//			"request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body),
//		)
//	}
//	if response != nil {
//		return json.NewDecoder(resp.Body).Decode(response)
//	}
//	return nil
//}
