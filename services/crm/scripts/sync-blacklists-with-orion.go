// +build ignore

package main

//import (
//	"encoding/csv"
//	"flag"
//	"io"
//	"log"
//	"os"
//	"strconv"
//	"strings"
//	"unicode"
//
//	"github.com/go-pg/pg"
//
//	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//	"gitlab.com/faemproject/backend/faem/services/driver/models"
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	autoPath := flag.String("auto", "", "import auto file path")
//	blacklistPath := flag.String("blacklist", "", "import auto file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	driverDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Driver.Host, cfg.Databases.Driver.Port),
//		User:     cfg.Databases.Driver.User,
//		Password: cfg.Databases.Driver.Password,
//		Database: cfg.Databases.Driver.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer driverDB.Close()
//
//	// Open the file
//	csvFile, err := os.Open(*autoPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading auto csv file...")
//
//	autos := make(map[int]string) // map id to phone
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[19])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		name, phone := nameAndPhone(line[2])
//		driverPhone, err := phoneverify.NumberVerify(phone)
//		if err != nil {
//			log.Printf("[ERROR] invalid phone: %s for driver %s \n", phone, name)
//			continue
//		}
//
//		autos[id] = driverPhone
//	}
//
//	log.Println("done")
//
//	// Read blacklist file
//	csvFile, err = os.Open(*blacklistPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	log.Println("reading blacklist csv file...")
//
//	blacklists := make(map[string][]string) // map driver phone to client phones
//	csvReader = csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[0])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		autoID := cleanNumber(line[1])
//		if autoID == 0 {
//			log.Printf("[ERROR] invalid autoid: %s\n", line[1])
//			continue
//		}
//
//		phone := formatPhone(line[2])
//		clientPhone, err := phoneverify.NumberVerify(phone)
//		if err != nil {
//			log.Printf("[ERROR] invalid phone: %s for auto_id %d\n", phone, autoID)
//			continue
//		}
//
//		driverPhone, ok := autos[autoID]
//		if !ok {
//			log.Printf("[ERROR] no auto_id found %d\n", autoID)
//			continue
//		}
//
//		blacklists[driverPhone] = append(blacklists[driverPhone], clientPhone)
//	}
//
//	log.Println("done")
//
//	log.Println("updating blacklists...")
//
//	for drvPhone, cltPhones := range blacklists {
//		drv := models.DriversApps{
//			Driver: structures.Driver{Blacklist: cltPhones},
//		}
//		_, err = driverDB.Model(&drv).
//			Where("phone = ?", drvPhone).
//			Set("blacklist = ?blacklist").
//			Update()
//		if err != nil {
//			log.Printf("[ERROR] for driver in db with phone %s: %s\n", drvPhone, err)
//		}
//	}
//
//	log.Println("done")
//}
//
//func nameAndPhone(str string) (name string, phone string) {
//	str = strings.TrimSpace(str)
//	split := strings.Split(str, " (")
//	if len(split) < 2 {
//		name = str
//		return
//	}
//	name = strings.TrimSpace(split[0])
//	phone = formatPhone(split[1])
//	return
//}
//
//func formatPhone(str string) string {
//	phone := strings.TrimSpace(str)
//	phone = strings.TrimSuffix(phone, ")")
//	if strings.HasPrefix(phone, "8") {
//		phone = "+7" + phone[1:]
//	}
//	return phone
//}
//
//func cleanNumber(str string) int {
//	str = strings.TrimSpace(str)
//	str = removeWhitespaces(str)
//	id, _ := strconv.Atoi(str)
//	return id
//}
//
//func removeWhitespaces(str string) string {
//	return strings.Map(func(r rune) rune {
//		if unicode.IsSpace(r) {
//			return -1
//		}
//		return r
//	}, str)
//}
