// +build ignore

package main

//import (
//	"flag"
//	"log"
//
//	"github.com/go-pg/pg"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
//	"gitlab.com/faemproject/backend/faem/services/crm/scripts/config"
//	driverModels "gitlab.com/faemproject/backend/faem/services/driver/models"
//)
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	flag.Parse()
//
//	cfg, err := config.ParseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	crmDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.CRM.Host, cfg.Databases.CRM.Port),
//		User:     cfg.Databases.CRM.User,
//		Password: cfg.Databases.CRM.Password,
//		Database: cfg.Databases.CRM.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer crmDB.Close()
//	crmModels.ConnectDB(crmDB)
//
//	driverDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Driver.Host, cfg.Databases.Driver.Port),
//		User:     cfg.Databases.Driver.User,
//		Password: cfg.Databases.Driver.Password,
//		Database: cfg.Databases.Driver.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer driverDB.Close()
//	driverModels.ConnectDB(driverDB)
//
//	// Get drivers with empty balances from CRM
//	var emptyBalances []*crmModels.DriverCRM
//	if err = crmDB.Model(&emptyBalances).
//		Column("uuid").
//		Where("balance IS NULL").
//		Select(); err != nil {
//		log.Fatalf("can't get drivers with empty balances")
//	}
//	if len(emptyBalances) < 1 {
//		log.Fatal("no empty balances")
//	}
//
//	var emptyBalancesUUIDs []string
//	for _, drv := range emptyBalances {
//		emptyBalancesUUIDs = append(emptyBalancesUUIDs, drv.UUID)
//	}
//
//	var knownBalances []*driverModels.DriversApps
//	if err = driverDB.Model(&knownBalances).
//		WhereIn("uuid IN (?)", emptyBalancesUUIDs).
//		Select(); err != nil {
//		log.Fatal("can't get known drivers")
//	}
//	if len(knownBalances) < 1 {
//		log.Fatal("no known drivers")
//	}
//
//	for i, drv := range knownBalances {
//		_, err = crmDB.Model((*crmModels.DriverCRM)(nil)).
//			Where("uuid = ?", drv.UUID).
//			Set("balance = ?", drv.Balance).
//			Update()
//		if err != nil {
//			log.Printf("[ERROR] can't update driver uuid %s balance", drv.UUID)
//		}
//		log.Println(i)
//	}
//}
