package tickers

import (
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

const (
	InsuranceConfigUpdateInterval = 15 * time.Second
)

func InitInsuranceConfigUpdater() {
	wg.Add(1)
	go updateInsuranceConfig()
}

func updateInsuranceConfig() {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "ticker updateInsuranceConfig",
	})

	// to update immediately
	interval := 0 * time.Second
	for {
		select {
		case <-closed:
			return
		case <-time.After(interval):
			interval = InsuranceConfigUpdateInterval
			cfg, err := models.GetInsuranceParams()
			if err != nil {
				log.WithError(err).Error("failed to update insurance config")
				continue
			}
			orders.CurrentInsurance.UpdateConfig(cfg)
		}
	}
}
