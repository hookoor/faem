package tickers

import (
	"context"
	"github.com/sirupsen/logrus"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/surge"
)

const (
	defaultThreshold  = 1 // 1 is a power of any base equal to zero
	surgePastInterval = -time.Hour
)

func InitDynamicTariffTicker() {
	wg.Add(1)
	go checkDynamicTariffExpiration()
}

func checkDynamicTariffExpiration() {
	defer wg.Done()

	interval := time.Duration(0)
	for {
		select {
		case <-closed:
			return
		case <-time.After(interval): // run once immediately, run periodically further
			log := logs.Eloger.WithField("event", "check dynamic tariff")

			remoteConfig, err := models.LoadRemoteConfig(context.Background(), models.SurgeConfigKey)
			if err != nil {
				log.Error(err)
			}
			surgeConfig := models.NewSurgeConfig(remoteConfig.GetValue())
			interval = surgeConfig.Interval
			orders.CurrentServiceCorrection.SetSurgeConfig(surgeConfig)

			surgeRulesMap, err := models.GetSurgeRulesMap(context.Background())
			if err != nil {
				log.Error(err)
			}
			orders.CurrentServiceCorrection.SetRulesMap(surgeRulesMap)

			// Get undispatched orders
			begin := time.Now().Add(surgePastInterval)
			undispatchedOrders, err := orders.GetUndispatchedOrders(context.Background(), begin)
			if err != nil {
				log.WithField("reason", "can't get undispatched orders").Error(err)
				continue
			}

			freeDrivers, err := orders.GetFreeDrivers(context.Background(), begin)
			if err != nil {
				log.WithField("reason", "can't get free drivers").Error(err)
				continue
			}

			orders.CurrentServiceCorrection.Clear()
			surgeByRegion := orders.SplitOrdersAnDriversByRegion(undispatchedOrders, freeDrivers)
			for regionID, surgeData := range surgeByRegion {
				newGeoMap := orders.MapOrdersToDriversByZones(surgeData.Orders, surgeData.Drivers)

				for _, zonesRow := range newGeoMap {
					for _, geoZone := range zonesRow {
						row, col := geoZone.Row, geoZone.Col
						for serviceUUID, surgeMeta := range geoZone.ServicesMeta {
							table := orders.CurrentServiceCorrection.GetTable(regionID, serviceUUID)
							surgeItem := table.GetSurgeItem(surgeMeta.OrderCount, surgeMeta.DriverCount)
							orders.CurrentServiceCorrection.Store(regionID, serviceUUID, row, col, surgeItem)
						}
					}
				}
			}

			log = logs.Eloger.WithFields(logrus.Fields{
				"event": "calc surge zones",
			})
			start := time.Now()
			ctx := context.Background()

			validOrderStates := []string{
				constants.OrderStateDistributing,
				constants.OrderStateFree,
				constants.OrderStateCreated,
				constants.OrderStateOffered,
			}
			activeOrders, err := orders.GetOrdersByStates(ctx, begin, validOrderStates)

			validDriverStates := []string{
				constants.DriverStateAvailable,
				constants.DriverStateOnline,
				constants.DriverStateWorking,
			}
			activeDrivers, err := orders.GetDriversByStates(ctx, begin, validDriverStates)
			surgeMap := orders.BuildSurgeMap(activeOrders, activeDrivers)
			err = orders.CurrentServiceCorrection.CalcSurgeZoneStats(surgeMap)
			if err != nil {
				log.WithField("reason", "can't insert new surge zones/history").Error(err)
				continue
			}
			log.WithFields(logrus.Fields{
				"elapsed":      time.Now().Sub(start),
				"driver_count": len(activeDrivers),
				"order_count":  len(activeOrders),
				"zone_count":   len(surgeMap),
			}).Info("surge stats saved")
		}
	}
}

func calcZoneSurge(table surge.Table, orderCount, driverCount int) structures.SurgeItem {
	return table.GetSurgeItem(orderCount, driverCount)
}
