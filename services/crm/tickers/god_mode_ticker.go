package tickers

import (
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

const (
	godModeRefreshTime = time.Minute
)

// InitRefreshGodMode -
func InitRefreshGodMode() {
	wg.Add(1)
	go refreshConfig()
}

func refreshConfig() {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "ticker refreshGodModeParams",
	})

	for {
		select {
		case <-closed:
			return
		case <-time.After(structures.RefreshGodModeParamsPeriod):

			gm, err := models.GetGodModeParamsByTicker()
			if err != nil {
				log.WithField("reason", "refreshGodModeParams").Error(errpath.Err(err).Error())
			}

			if gm.OSRM.Address != "" {
				config.St.OSRM.Host = gm.OSRM.Address
			}

		}
	}
}
