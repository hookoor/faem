package tickers

import (
	"context"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

const (
	checkDeliveryEstimationInterval   = time.Minute
	checkSelfPickupEstimationInterval = time.Second * 63
)

func InitDeliveryEstimationTicker() {
	wg.Add(1)
	go checkDeliveryEstimation()

	wg.Add(1)
	go checkSelfPickupEstimation()
}

func checkDeliveryEstimation() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkDeliveryEstimationInterval):
			log := logs.Eloger.WithField("event", "check orders delivery estimation expiration")

			if err := orders.PayExpiredDeliveryOrders(context.Background()); err != nil {
				log.WithField("reason", "can't pay expired delivery orders").Error(err)
			}

			if err := orders.FinishExpiredPayedDeliveryOrders(context.Background()); err != nil {
				log.WithField("reason", "can't finish expired payed delivery orders").Error(err)
			}
		}
	}
}

func checkSelfPickupEstimation() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkSelfPickupEstimationInterval):
			log := logs.Eloger.WithField("event", "check orders self pickup estimation expiration")

			if err := orders.PayExpiredSelfPickupOrders(context.Background()); err != nil {
				log.WithField("reason", "can't pay expired self pickup orders").Error(err)
			}

			if err := orders.FinishExpiredSelfPickupOrders(context.Background()); err != nil {
				log.WithField("reason", "can't finish expired payed self pickup orders").Error(err)
			}
		}
	}
}
