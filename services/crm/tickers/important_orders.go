package tickers

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

const (
	checkLateDriversPeriod = time.Second * 20
)

func InitImportantDriversTickers() {
	wg.Add(1)
	go checkLateDriversTicker()
}

func checkLateDriversTicker() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkLateDriversPeriod):
			log := logs.Eloger.WithField("event", "checking late drivers")

			uuids, err := orders.GetOrdersWithLateDriversUUIDs()
			if err != nil {
				log.WithField("reason", "can't get orders with late drivers").Error(err)
				continue
			}
			if len(uuids) < 1 {
				log.Debug("no orders with late driver")
				continue
			}
			err = orders.AddImportantReasonToOrder(orders.LateDriver, uuids...)
			if err != nil {
				log.WithField("reason", "can't add important reason to order").Error(err)
				continue
			}
			log.Infof("%v orders marked as important", len(uuids))
		}
	}
}
