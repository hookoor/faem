package tickers

import (
	"context"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

const (
	checkProdDelExpirationInterval    = time.Second * 10
	checkProdDelFindingDriverInterval = time.Second * 20
)

func InitProdDeliveryTickers() {
	wg.Add(3)
	go checkProdDelOrderCances()
	go checkCookedOrders()
	go checkingTheNeedToFindDriver()
}
func checkCookedOrders() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkProdDelExpirationInterval):
			log := logs.Eloger.WithField("event", "checking cooked orders")

			ors, err := orders.GetCookedOrdersWithOwnDelivery(context.Background())
			if err != nil {
				log.WithField("reason", "can't get cooked orders with own delivery").Error(err)
				continue
			}

			if len(ors) < 1 {
				log.Debug("no cooked orders with own delivery")
				continue
			}

			for _, or := range ors {
				ctxLog := log.WithField("order uuid", or.UUID)
				err := orders.TransToStateAndSetDeliveryTime(or.UUID, constants.OrderStateOnTheWay)
				if err != nil {
					ctxLog.WithField("reason", "can't trans to state order").Error(err)
					continue
				}
			}
		}
	}
}
func checkProdDelOrderCances() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkProdDelExpirationInterval):
			log := logs.Eloger.WithField("event", "checking the need to cancel the products delivery order")

			uuids, err := orders.GetOrdersWithExpiredConfirmationTime()
			if err != nil {
				log.WithField("reason", "can't get orders with expired confirmation time").Error(err)
				continue
			}

			if len(uuids) < 1 {
				log.Debug("no orders with expired confirmation time")
				continue
			}

			for _, uuid := range uuids {
				ctxLog := log.WithField("order uuid", uuid)
				orState, err := orders.CancelOrder(constants.CancelReasonExpiredConfTime, uuid)
				if err != nil {
					ctxLog.WithField("reason", "can't cancel order").Error(err)
					continue
				}

				go func() {
					if err = rabsender.SendJSONByAction(rabsender.AcOrderState, orState); err != nil {
						ctxLog.WithField("reason", "error sending new order state to broker").Error(err)
						return
					}
					ctxLog.Infof("order confirmation time is expired and order was cancel")
				}()
			}
		}
	}
}

func checkingTheNeedToFindDriver() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkProdDelExpirationInterval):
			log := logs.Eloger.WithField("event", "checking the need to find a driver for delivery order")

			neededOrders, err := orders.GetOrdersForFindDriver()
			if err != nil {
				log.WithField("reason", "can't get orders to find a driver").Error(err)
				continue
			}

			if len(neededOrders) < 1 {
				log.Debug("no orders to find a driver")
				continue
			}
			dur, _ := time.ParseDuration(config.St.Application.DistributingTime)

			for _, or := range neededOrders {
				ctxLog := log.WithField("order uuid", or.UUID)

				err = orders.UpdateOrderCancelTime(or.UUID, dur)
				if err != nil {
					ctxLog.WithField("reason", "can't update order cancel time").Error(err)
					continue
				}

				or.CancelTime = time.Now().Add(dur)
				err = rabsender.SendJSONByAction(rabsender.AcOrderToDriver, or)
				if err != nil {
					ctxLog.WithField("reason", "can't send order to broker").Error(err)
					continue
				}
				ctxLog.Infof("order sended to broker")
			}
		}
	}
}
