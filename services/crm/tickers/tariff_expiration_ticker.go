package tickers

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
)

const (
	checkTariffExpirationInterval = time.Second * 30
)

func InitTariffCheckerTicker() {
	wg.Add(1)
	go checkDriverTariffExpiration()
}

func checkDriverTariffExpiration() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkTariffExpirationInterval):
			log := logs.Eloger.WithField("event", "check driver tariff expiration")

			driversWithExpiredTariff, err := models.GetDriversWithExpiredPeriodTariff()
			if err != nil {
				log.WithField("reason", "can't get drivers with expired tariff").Error(err)
				continue
			}

			if len(driversWithExpiredTariff) < 1 {
				log.Debug("no drivers with expired tariff")
				continue
			}

			for _, drv := range driversWithExpiredTariff {
				ctxLog := log.WithField("driver uuid", drv.UUID)

				defaultTariff, err := drv.GetDefaultDriverTariffByGroup()
				if err != nil {
					ctxLog.WithField("reason", "can't get the default driver tariff").Error(err)
					continue
				}

				selectedDefaultTariff := structures.SelectedDriverTariff{DriverTariff: defaultTariff.DriverTariff}
				driver, err := models.UpdateTariffOfDriver(drv.UUID, selectedDefaultTariff)
				if err != nil {
					ctxLog.WithField("reason", "can't set the default tariff for the driver").Error(err)
					continue
				}

				go func() {
					if err = rabsender.SendJSONByAction(rabsender.AcNewDriverDataToDriver, driver); err != nil {
						ctxLog.WithField("reason", "error sending the updated driver with new tariff to broker").Error(err)
						return
					}
					ctxLog.Infof("driver's tariff is expired and set to the default")
				}()
			}
		}
	}
}
