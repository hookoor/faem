package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/cloudstorage"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	xos "gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/crm/config"
	"gitlab.com/faemproject/backend/faem/services/crm/db"
	"gitlab.com/faemproject/backend/faem/services/crm/handlers"
	"gitlab.com/faemproject/backend/faem/services/crm/locationsaver"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
	"gitlab.com/faemproject/backend/faem/services/crm/rabhandler"
	"gitlab.com/faemproject/backend/faem/services/crm/rabsender"
	"gitlab.com/faemproject/backend/faem/services/crm/router"
	"gitlab.com/faemproject/backend/faem/services/crm/tickers"
)

const (
	version               = "0.2.1"
	serverShutdownTimeout = 30 * time.Second
	brokerShutdownTimeout = 30 * time.Second
	tickerShutdownTimeout = 30 * time.Second
)

func main() {
	bVersion := flag.Bool("ver", false, "Output current app vesion and exit")
	bEnvVars := flag.Bool("env", false, "Output envieroinment vars and exit")
	configPath := flag.String("config", "config/crm.toml", "Default config filepath")

	flag.Parse()

	config.InitConfig(*configPath)

	if *bVersion {
		fmt.Println("Version: ", version)
		os.Exit(0)

	}

	if *bEnvVars {
		config.PrintVars()
		os.Exit(0)
	}

	if err := logs.SetLogLevel(config.St.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(config.St.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	eloger := logs.Eloger

	conn, err := db.Connect()
	if err != nil {
		es := fmt.Sprintf("Error connecting to DB. %s", err)
		// logs.OutputError(es)
		fmt.Println(es)
		os.Exit(4)
	}
	defer db.CloseDbConnection(conn)
	fmt.Println("Connecting to Postgres successfully", conn)
	// logs.OutputInfo("Connecting to Postgres successfully")

	dbHook := models.ConnectDB(conn)
	orders.ConnectDB(conn)

	cloudStorage, err := cloudstorage.NewGCStorage(context.Background(), cloudstorage.Config{
		ProjectID:  config.St.CloudStorage.ProjectID,
		BucketName: config.St.CloudStorage.BucketName,
	})
	if err != nil {
		fmt.Println(fmt.Sprintf("Error Init cloudStorage. %s", err))
		//os.Exit(5)
	}

	hndl := handlers.New(cloudStorage, conn, dbHook)

	// Запуск сервиса для сохранения координат, вынесен для дальнейшего переноса

	rb := rabbit.New()
	rb.Credits.User = config.St.Broker.UserCredits
	rb.Credits.URL = config.St.Broker.UserURL
	err = rb.Init(config.St.Broker.ExchagePrefix, config.St.Broker.ExchagePostfix)
	if err != nil {
		fmt.Println(fmt.Sprintf("Error Init RabbitMQ. %s", err))
		// logs.OutputError(fmt.Sprintf("Error Init RabbitMQ. %s", err))
		os.Exit(5)
	}
	fmt.Println("Connecting to RabbitMQ successfully")
	defer rb.CloseRabbit()
	var wg sync.WaitGroup
	rabsender.ItitVars(rb, &wg)
	rabhandler.InitVars(rb, conn, &wg)
	err = rabhandler.StartService()
	if err != nil {
		fmt.Println(fmt.Sprintf("Error starting RabbitMQ sevices. %s", err))
		// logs.OutputError(fmt.Sprintf("Error starting RabbitMQ sevices. %s", err))
		os.Exit(5)
	}
	defer rabhandler.Wait(brokerShutdownTimeout)

	err = locationsaver.Init(rb, conn)
	if err != nil {
		fmt.Printf("Error init location saver,%s", err)
		os.Exit(5)
	}

	e := router.Init(eloger, hndl)
	//orderMetrics := prometrics.CRMMetrics()
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
	//if err = prometrics.InitPrometheus(p); err != nil {
	//	eloger.Errorf("Error init prometheus metrics: %v", err)
	//}
	// Инифцифализация адресов - в отдельном потоке чтобы сервер запускался шустрее
	models.AllRoutesMap = make(models.RouteMap)
	go models.InitAddress()

	tickers.Init()

	err = models.InitCurrentTaxiParks()
	if err != nil {
		fmt.Printf("Error init current taxi parks,%s", err)
		os.Exit(5)
	}
	err = models.InitCurrentDriverTariffs()
	if err != nil {
		fmt.Printf("Error init current driver tariffs,%s", err)
		os.Exit(5)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event": "debug ENV",
	}).Info(config.St.Application.DrvLocShelfLifeSec)
	tickers.InitProdDeliveryTickers()
	defer tickers.Wait(tickerShutdownTimeout)
	// Start an http server and remember to shut it down
	go web.Start(e, config.St.Application.Port)
	defer web.Stop(e, serverShutdownTimeout)

	// Wait for program exit
	<-xos.NotifyAboutExit()
}
