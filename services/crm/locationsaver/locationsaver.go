package locationsaver

import (
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/crm/models"
)

// DriverConnState setting driver apps status
type DriverConnState struct {
	tableName  struct{}  `sql:"drv_conn_states"`
	ID         int       `json:"id" sql:",pk"`
	CreatedAt  time.Time `sql:"default:now()" json:"created_at"`
	DriverUUID string    `json:"driver_uuid"`
	State      string    `json:"state" description:"Status" required:"true"`
	Comment    string    `json:"comment" description:"Comment"`
	StartedAt  time.Time `json:"started_at"`
}

var (
	db *pg.DB
)

// Init создает все необходимое для получения координат
// Вынесен отдельно для дальнешейго более легкого переноса на микросервис
func Init(rb *rabbit.Rabbit, conn *pg.DB) error {
	// Ensure db connection exists first
	db = conn

	if err := models.InitCurrentDriverLocations(); err != nil {
		return errors.Wrap(err, "failed to init current driver locations")
	}

	receiverChannel, err := rb.GetReceiver(rabbit.DriverLocationQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь
	q, err := receiverChannel.QueueDeclare(
		rabbit.DriverLocationQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.LocationKey,    // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannel.Consume(
		q.Name,                           // queue
		rabbit.DriverLocationCRMConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return err
	}

	go handleLocations(msgs)
	return nil
}

// handleLocations сохраняет полученные координаты в базу
func handleLocations(msgs <-chan amqp.Delivery) {
	for d := range msgs {
		var ld []models.LocationDataCRM
		if err := json.Unmarshal(d.Body, &ld); err != nil {
			es := fmt.Sprintf("\nError unmarshal location data. DriverID=%s. %s", d.ConsumerTag, err)
			logs.OutputError(es)
			continue
		}

		// Удаляем все записи с неуникальными DriverUUID для корректной работы OnConflict
		locationMapToUpsert := make(map[string]models.LocationDataCRM)
		for _, loc := range ld {
			if loc.DriverUUID == "" {
				continue
			}

			// если координаты пришли из далекого будущего... (например из 08.2023 года вместо 05.2021)
			if loc.Timestamp-time.Now().Unix() > 60 {
				continue
			}

			// Take the most recent location
			locationMapToUpsert[loc.DriverUUID] = loc
			err := models.SetDriverLocation(loc.DriverUUID, loc)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":   "save driver locations",
					"reason":  "error set drv location",
					"drvUUID": loc.DriverUUID,
				}).Error(err)
			}
		}
		locationsToUpsert := make([]models.LocationDataCRM, 0, len(locationMapToUpsert))
		for _, loc := range locationMapToUpsert {
			locationsToUpsert = append(locationsToUpsert, loc)
		}

		// Upsert
		_, err := db.Model(&locationsToUpsert).
			OnConflict("(driver_uuid) DO UPDATE").
			Set("latitude = EXCLUDED.latitude").
			Set("longitude = EXCLUDED.longitude").
			Set("satelites = EXCLUDED.satelites").
			Set("timestamp = EXCLUDED.timestamp").
			Set("created_at = EXCLUDED.created_at").
			Insert()
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "save driver locations",
				"reason": "error saving location to DB",
			}).Error(err)
		}
	}
}
