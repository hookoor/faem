package surge

import "gitlab.com/faemproject/backend/faem/pkg/structures"

type Table []structures.SurgeItem

var (
	DefaultSurgeTable = Table{}
	DefaultSurgeItem  = structures.SurgeItem{SurgeType: structures.SurgeTypeAbsolute}
)

const (
	DefaultAllowedOutOfTown = false
)

func (t Table) GetSurgeItem(orderCount, driverCount int) structures.SurgeItem {
	if len(t) < 1 {
		return DefaultSurgeItem
	}

	delta := float64(orderCount-driverCount) + 1 // add an intention
	for i := 0; i < len(t)-1; i++ {
		nextItem := t[i+1]
		if nextItem.Delta > delta {
			result := t[i]
			result.CurDelta = delta
			result.CurOrderCount = orderCount
			result.CurDriverCount = driverCount
			return result
		}
	}

	result := t[len(t)-1]
	result.CurDelta = delta
	result.CurOrderCount = orderCount
	result.CurDriverCount = driverCount
	return result
}

func (t Table) GetSurgeItemForDelta(delta float64) structures.SurgeItem {
	if len(t) < 1 {
		return DefaultSurgeItem
	}

	delta++ // add an intention
	for i := 0; i < len(t)-1; i++ {
		nextItem := t[i+1]
		if nextItem.Delta > delta {
			result := t[i]
			result.CurDelta = delta
			return result
		}
	}

	result := t[len(t)-1]
	result.CurDelta = delta
	return result
}
