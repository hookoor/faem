// Начинаю делать новые штуки в отдельном пакете,
// т.к все равно собираемся вынести повышенный спрос в отдельный микросервис
package surge

type Rule struct {
	SurgeTable     Table `json:"surge_table"`
	AllowOutOfTown bool  `json:"allow_out_of_town"`
}

var DefaultSurgeRule = Rule{
	SurgeTable:     DefaultSurgeTable,
	AllowOutOfTown: DefaultAllowedOutOfTown,
}

type rulesMapKey struct {
	RegionID    int
	ServiceUUID string
}

type RulesMap map[rulesMapKey]Rule

func (r RulesMap) GetTable(regionID int, serviceUUID string) Table {
	if rule, ok := r.GetRule(regionID, serviceUUID); ok {
		return rule.SurgeTable
	}

	return DefaultSurgeTable
}

func (r RulesMap) GetAllowedOutOfTown(regionID int, serviceUUID string) bool {
	if rule, ok := r.GetRule(regionID, serviceUUID); ok {
		return rule.AllowOutOfTown
	}

	return DefaultAllowedOutOfTown
}

func (r RulesMap) GetRule(regionID int, serviceUUID string) (Rule, bool) {
	rule, ok := r[key(regionID, serviceUUID)]
	return rule, ok
}

func (r RulesMap) SetRule(regionID int, serviceUUID string, value Rule) {
	r[key(regionID, serviceUUID)] = value
}

func key(regionID int, serviceUUID string) rulesMapKey {
	return rulesMapKey{
		RegionID:    regionID,
		ServiceUUID: serviceUUID,
	}
}
