package config

import (
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "analytic"
)

type Application struct {
	Env       string
	Port      string
	LogLevel  string
	LogFormat string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

func (d *Database) validate() error {
	if d.Host == "" {
		return errors.New("empty db host provided")
	}
	if d.Port == 0 {
		return errors.New("empty db port provided")
	}
	if d.User == "" {
		return errors.New("empty db user provided")
	}
	if d.Password == "" {
		return errors.New("empty db password provided")
	}
	if d.Db == "" {
		return errors.New("empty db name provided")
	}
	return nil
}

type Databases struct {
	CRM     Database
	Billing Database
}

func (d *Databases) validate() error {
	return multierr.Append(
		errors.Wrap(d.CRM.validate(), "crm database"),
		errors.Wrap(d.Billing.validate(), "billing database"),
	)
}

type Broker struct {
	UserURL         string
	UserCredits     string
	ExchangePrefix  string
	ExchangePostfix string
}

func (b *Broker) validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

type BigQuery struct {
	ProjectID            string
	EventsDataset        string
	OrdersEventsTable    string
	DriversEventsTable   string
	TelephonyEventsTable string
	OrdersDataset        string
	OrdersTable          string
	FullOrdersTable      string
	DriversDataset       string
	DriversTable         string
	AggrTimeTable        string
	OrdersAggrDataset    string
	OrdersSourceAggr     string
	OperatorsDataAggr    string
	ClientTariffDataAggr string
	DriverTariffDataAggr string
	CancelDataAggr       string

	BillingEventsDataset     string
	BillingAccountTable      string
	BillingBankTransferTable string
	BillingEntrieTable       string
	BillingTransactionTable  string
	BillingTransferTable     string
}

type CloudStorage struct {
	BucketName string
}

// Config -
type Config struct {
	Application  Application
	Database     Databases
	Broker       Broker
	BigQuery     BigQuery
	CloudStorage CloudStorage
}

func (c *Config) validate() error {
	return c.Database.validate()
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

func setDefaults() {
	viper.SetDefault("Application.Env", "production")
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")
	viper.SetDefault("Application.Port", "8080")

	viper.SetDefault("Database.CRM.Host", "")
	viper.SetDefault("Database.CRM.User", "")
	viper.SetDefault("Database.CRM.Password", "")
	viper.SetDefault("Database.CRM.Db", "")
	viper.SetDefault("Database.CRM.Port", 0)

	viper.SetDefault("Database.Billing.Host", "")
	viper.SetDefault("Database.Billing.User", "")
	viper.SetDefault("Database.Billing.Password", "")
	viper.SetDefault("Database.Billing.Db", "")
	viper.SetDefault("Database.Billing.Port", 0)

	viper.SetDefault("broker.userurl", "localhost")
	viper.SetDefault("broker.usercredits", "login:pass")

	viper.SetDefault("BigQuery.Projectid", "faem-staging-01")

	viper.SetDefault("BigQuery.EventsDataset", "faem_dataset")
	viper.SetDefault("BigQuery.OrdersEventsTable", "orders_events_table00")
	viper.SetDefault("BigQuery.DriversEventsTable", "drivers_events_table00")
	viper.SetDefault("BigQuery.TelephonyEventsTable", "telephony_events_table00")

	viper.SetDefault("BigQuery.OrdersDataset", "faem_dataset")
	viper.SetDefault("BigQuery.OrdersTable", "orders_table00")
	viper.SetDefault("BigQuery.FullOrdersTable", "full_orders")
	viper.SetDefault("BigQuery.DriversDataset", "faem_dataset")
	viper.SetDefault("BigQuery.DriversTable", "drivers_table00")
	viper.SetDefault("BigQuery.AggrTimeTable", "aggr_time")

	viper.SetDefault("BigQuery.OrdersAggrDataset", "faem_dataset")
	viper.SetDefault("BigQuery.OrdersSourceAggr", "orders_source_aggr00")
	viper.SetDefault("BigQuery.OperatorsDataAggr", "operators_aggr00")
	viper.SetDefault("BigQuery.ClientTariffDataAggr", "client_tariff_aggr00")
	viper.SetDefault("BigQuery.DriverTariffDataAggr", "driver_tariff_aggr00")
	viper.SetDefault("BigQuery.CancelDataAggr", "canceldata_aggr00")

	viper.SetDefault("BigQuery.BillingEventsDataset", "faem_dataset")
	viper.SetDefault("BigQuery.BillingAccountTable", "billing_account_table00")
	viper.SetDefault("BigQuery.BillingBankTransferTable", "billing_bank_transfer_table00")
	viper.SetDefault("BigQuery.BillingEntrieTable", "billing_entrie_table00")
	viper.SetDefault("BigQuery.BillingTransactionTable", "billing_transaction_table00")
	viper.SetDefault("BigQuery.BillingTransferTable", "billing_transfer_table00")

	viper.SetDefault("CloudStorage.BucketName", "faem-staging-data-backup")
}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}
