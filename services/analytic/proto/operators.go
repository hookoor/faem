package proto

import (
	"fmt"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
)

type OperatorFilter struct {
	OperatorUUID string         `json:"operator_uuid" query:"operator_uuid"`
	PeriodStart  int64          `json:"period_start" query:"period_start"`
	PeriodEnd    int64          `json:"period_end" query:"period_end"`
	DayStart     string         `json:"day_start" query:"day_start"`
	DayEnd       string         `json:"day_end" query:"day_end"`
	Sort         string         `json:"sort" query:"sort"`
	Dir          models.SortDir `json:"dir" query:"dir"`
}

func (f *OperatorFilter) Prepare() error {
	if f.DayStart == "" {
		return errors.New("empty day start provided")
	}
	f.DayStart = fmt.Sprintf("%s %s", f.DayStart, models.TimeZoneShort)

	if f.DayEnd == "" {
		return errors.New("empty day end provided")
	}
	f.DayEnd = fmt.Sprintf("%s %s", f.DayEnd, models.TimeZoneShort)

	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *OperatorFilter) ToModel() models.OperatorFilter {
	result := models.OperatorFilter{
		OperatorUUID: f.OperatorUUID,
		DayStart:     f.DayStart,
		DayEnd:       f.DayEnd,
	}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}
