package proto

import (
	"time"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
)

type CallFilter struct {
	Source      string         `json:"source" query:"source"`
	PeriodStart int64          `json:"period_start" query:"period_start"`
	PeriodEnd   int64          `json:"period_end" query:"period_end"`
	Sort        string         `json:"sort" query:"sort"`
	Dir         models.SortDir `json:"dir" query:"dir"`
}

func (f *CallFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *CallFilter) ToModel() models.CallFilter {
	result := models.CallFilter{Source: f.Source}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}
