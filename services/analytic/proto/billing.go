package proto

import (
	"time"

	"github.com/go-pg/pg/urlvalues"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
)

type TransferFilter struct {
	Description string         `json:"description" query:"description"`
	PeriodStart int64          `json:"period_start" query:"period_start"`
	PeriodEnd   int64          `json:"period_end" query:"period_end"`
	Sort        string         `json:"sort" query:"sort"`
	Dir         models.SortDir `json:"dir" query:"dir"`

	TransferType structures.BillingTransferType `json:"-" query:"-"` // inner field
}

func (f *TransferFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *TransferFilter) ToModel() models.TransferFilter {
	result := models.TransferFilter{
		Description:  f.Description,
		TransferType: f.TransferType,
	}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type SeparateTransferFilter struct {
	OwnerName    string                         `json:"owner_name" query:"owner_name"`
	DriverAlias  string                         `json:"driver_alias" query:"driver_alias"`
	TransferType structures.BillingTransferType `json:"transfer_type" query:"transfer_type"`
	EntryType    billingModels.EntryType        `json:"entry_type" query:"entry_type"`
	Description  string                         `json:"description" query:"description"`
	UserUUID     string                         `json:"user_uuid" query:"user_uuid"`
	Phone        string                         `json:"phone" query:"phone"`
	PeriodStart  int64                          `json:"period_start" query:"period_start"`
	PeriodEnd    int64                          `json:"period_end" query:"period_end"`
	Sort         string                         `json:"sort" query:"sort"`
	Dir          models.SortDir                 `json:"dir" query:"dir"`
	Pager        urlvalues.Pager                `json:"-" query:"-"`
}

func (f *SeparateTransferFilter) ToModel() models.SeparateTransferFilter {
	result := models.SeparateTransferFilter{
		OwnerName:    f.OwnerName,
		DriverAlias:  f.DriverAlias,
		TransferType: f.TransferType,
		EntryType:    f.EntryType,
		Description:  f.Description,
		Phone:        f.Phone,
		UserUUID:     f.UserUUID,
		Pager:        f.Pager,
	}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}

func (f *SeparateTransferFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir, models.SortDirDesc)
	return nil
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type TransferLogFilter struct {
	Account     string          `json:"account" query:"account"`
	PeriodStart int64           `json:"period_start" query:"period_start"`
	PeriodEnd   int64           `json:"period_end" query:"period_end"`
	Sort        string          `json:"sort" query:"sort"`
	Dir         models.SortDir  `json:"dir" query:"dir"`
	Pager       urlvalues.Pager `json:"-" query:"-"`
}

func (f *TransferLogFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *TransferLogFilter) ToModel() models.TransferLogFilter {
	result := models.TransferLogFilter{
		Account: f.Account,
		Pager:   f.Pager,
	}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type TariffFilter struct {
	PeriodStart int64 `json:"period_start" query:"period_start"`
	PeriodEnd   int64 `json:"period_end" query:"period_end"`
}

func (f *TariffFilter) ToModel() models.TariffFilter {
	var result models.TariffFilter
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}
