package proto

import (
	"time"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
)

type DriverFilter struct {
	DriverUUID  string         `json:"driver_uuid" query:"driver_uuid"`
	PeriodStart int64          `json:"period_start" query:"period_start"`
	PeriodEnd   int64          `json:"period_end" query:"period_end"`
	Sort        string         `json:"sort" query:"sort"`
	Dir         models.SortDir `json:"dir" query:"dir"`
}

func (f *DriverFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *DriverFilter) ToModel() models.DriverFilter {
	result := models.DriverFilter{DriverUUID: f.DriverUUID}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}

type DriverFullInfo struct {
	Driver *models.BQDriver          `json:"driver"`
	Orders *[]models.BQOrderRaw      `json:"orders"`
	Events *[]models.DriverEventData `json:"events"`
}
