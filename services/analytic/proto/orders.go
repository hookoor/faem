package proto

import (
	"time"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
)

type OrderFilter struct {
	Source      string         `json:"source" query:"source"`
	PeriodStart int64          `json:"period_start" query:"period_start"`
	PeriodEnd   int64          `json:"period_end" query:"period_end"`
	Sort        string         `json:"sort" query:"sort"`
	Dir         models.SortDir `json:"dir" query:"dir"`
}

type BQOrderFilter struct {
	ID                 int       `json:"id" query:"id"`
	StartDate          time.Time `json:"start_date" query:"start_date"`
	EndDate            time.Time `json:"end_date" query:"end_date"`
	ClientPhone        string    `json:"client_phone" query:"client_phone"`
	Operator           string    `json:"operator" query:"operator"`
	DriverUUID         string    `json:"driver_uuid"`           //id водителя
	DriverCar          string    `json:"driver_car"`            //машина водителя
	DriverCarRegNumber string    `json:"driver_car_reg_number"` //гос. номер
	DriverCarColor     string    `json:"driver_car_color"`      //цвет машины
	DriverTarrif       string    `json:"driver_tarrif"`         //тариф водителя
	DriverName         string    `json:"driver_name"`
	DriverAlias        int       `json:"driver_alias"` //позывной
	TaxiPool           string    `json:"taxi_pool" query:"taxi_pool"`
	Service            string    `json:"service" query:"service"`
	Source             string    `json:"source" query:"source"`
	Options            string    `json:"options" query:"options"`
	From               string    `json:"from" query:"from"`
	To                 string    `json:"to" query:"to"`
	Page               int       `json:"page" query:"to"`
	Count              int       `json:"count" query:"count"`
}

type BQOrderRawWithCount struct {
	Orders []models.BQOrderRaw `json:"orders"`
	Count  int                 `json:"records_count"`
}

func (f *OrderFilter) Prepare() error {
	f.Dir = models.DefineSortDir(f.Dir)
	return nil
}

func (f *OrderFilter) ToModel() models.OrderFilter {
	result := models.OrderFilter{Source: f.Source}
	result.Sort.Field = f.Sort
	result.Sort.Dir = f.Dir
	if f.PeriodStart != 0 {
		result.PeriodStart = time.Unix(f.PeriodStart, 0)
	}
	if f.PeriodEnd != 0 {
		result.PeriodEnd = time.Unix(f.PeriodEnd, 0)
	}
	return result
}
