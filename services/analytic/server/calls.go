package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

func (r *Rest) Calls(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "calls report")

	var filter proto.CallFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.Calls(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting calls report").Error(err)
		res := logs.OutputRestError("can't get calls report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}
