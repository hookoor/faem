package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/analytic/helpers"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

func (r *Rest) Topups(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "topups report")

	var filter proto.TransferFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	filter.TransferType = structures.TransferTypeTopUp

	out, err := r.Handler.Transfers(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting topups report").Error(err)
		res := logs.OutputRestError("can't get topups report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) Withdraws(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "withdraws report")

	var filter proto.TransferFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	filter.TransferType = structures.TransferTypeWithdraw

	out, err := r.Handler.Transfers(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting withdraws report").Error(err)
		res := logs.OutputRestError("can't get withdraws report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) SeparateTransfers(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "separate transfers report")

	var filter proto.SeparateTransferFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	pager, err := helpers.GetPager(c)
	if err != nil {
		log.WithField("reason", "binding pager arguments").Error(err)
		res := logs.OutputRestError("can't bind pager arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	filter.Pager = pager

	out, err := r.Handler.SeparateTransfers(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting separate transfers report").Error(err)
		res := logs.OutputRestError("can't get separate transfers report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) TransferLogs(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "transfer logs report")

	var filter proto.TransferLogFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	pager, err := helpers.GetPager(c)
	if err != nil {
		log.WithField("reason", "binding pager arguments").Error(err)
		res := logs.OutputRestError("can't bind pager arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	filter.Pager = pager

	out, err := r.Handler.TransferLogs(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting transfer logs report").Error(err)
		res := logs.OutputRestError("can't get transfer logs report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) Tariffs(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "tariffs report")

	var filter proto.TariffFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.Tariffs(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting tariffs report").Error(err)
		res := logs.OutputRestError("can't get tariffs report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}
