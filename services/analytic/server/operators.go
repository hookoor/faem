package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

func (r *Rest) Operators(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "operators report")

	var filter proto.OperatorFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.Operators(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting operators report").Error(err)
		res := logs.OutputRestError("can't get operators report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}
