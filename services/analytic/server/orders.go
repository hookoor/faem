package server

import (
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

const defaultPageLimit = 50

func (r *Rest) Orders(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "orders report")

	var filter proto.OrderFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.Orders(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting orders report").Error(err)
		res := logs.OutputRestError("can't get orders report", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, out)
}

func (r *Rest) GetOrderByUUID(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "order report")

	uuid := c.Param("uuid")
	out, err := r.Handler.GetOrderByUUID(c.Request().Context(), uuid)
	if err != nil {
		log.WithField("reason", "getting order by uuid report").Error(err)
		res := logs.OutputRestError("can't get order by uuid report", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, out)
}

func (r *Rest) OrdersFiler(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "orders filtering report")

	var err error
	var filter proto.BQOrderFilter

	filter.ID, err = strconv.Atoi(c.QueryParam("id"))
	if err != nil {
		filter.ID = 0
	}

	filter.StartDate, err = parseDate(c.QueryParam("start_date"))
	if err != nil {
		filter.StartDate = time.Now().AddDate(0, -1, 0)
	}

	filter.EndDate, err = parseDate(c.QueryParam("end_date"))
	if err != nil {
		filter.EndDate = time.Now()
	}

	filter.ClientPhone = c.QueryParam("client_phone")
	if filter.ClientPhone != "" {
		filter.ClientPhone = "+" + strings.TrimSpace(filter.ClientPhone)
	}

	filter.Operator = c.QueryParam("operator")

	filter.DriverName = c.QueryParam("driver_name")

	filter.DriverTarrif = c.QueryParam("driver_tarrif")

	filter.DriverCar = c.QueryParam("driver_car")

	filter.DriverCarRegNumber = c.QueryParam("driver_car_reg_number")

	filter.DriverCarColor = c.QueryParam("driver_car_color")

	filter.DriverAlias, err = strconv.Atoi(c.QueryParam("driver_alias"))
	if err != nil {
		filter.DriverAlias = 0
	}

	filter.TaxiPool = c.QueryParam("taxi_pool")

	filter.Service = c.QueryParam("service")

	filter.Source = c.QueryParam("source")

	filter.Options = c.QueryParam("options")

	filter.From = c.QueryParam("from")

	filter.To = c.QueryParam("to")

	filter.Page, err = strconv.Atoi(c.QueryParam("page"))
	if err != nil || filter.Page < 1 {
		filter.Page = 1
	}

	filter.Count, err = strconv.Atoi(c.QueryParam("count"))
	if err != nil || filter.Count < 1 || filter.Count > 1000 {
		filter.Count = defaultPageLimit
	}

	out, err := r.Handler.GetFilteredOrders(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting orders report").Error(err)
		res := logs.OutputRestError("can't get filtered orders report", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, out)
}

func parseDate(s string) (time.Time, error) {
	parts := strings.Split(s, "-")

	year, err := strconv.Atoi(parts[0])
	if err != nil {
		return time.Now(), err
	}

	month, err := strconv.Atoi(parts[1])
	if err != nil {
		return time.Now(), err
	}

	day, err := strconv.Atoi(parts[2])
	if err != nil {
		return time.Now(), err
	}

	l, err := time.LoadLocation("Europe/Vienna")
	if err != nil {
		return time.Now(), err
	}

	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, l), nil
}
