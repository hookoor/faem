package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/analytic/config"
	"gitlab.com/faemproject/backend/faem/services/analytic/handler"
)

const (
	apiPrefix = "/api/v2"
)

type Rest struct {
	Router  *echo.Echo
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route() {
	web.UseHealthCheck(r.Router)

	protected := r.Router.Group(apiPrefix)
	protected.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(config.JWTSecret()),
	}))
	protected.GET("/operators", r.Operators)
	protected.GET("/calls", r.Calls)
	protected.GET("/billing/topups", r.Topups)
	protected.GET("/billing/withdraws", r.Withdraws)
	protected.GET("/billing/separate", r.SeparateTransfers)
	protected.GET("/billing/logs", r.TransferLogs)
	protected.GET("/billing/tariffs", r.Tariffs)

	protected.GET("/drivers", r.Drivers)
	protected.GET("/driver/:uuid", r.GetDriverInfo)

	protected.GET("/orders", r.Orders)
	protected.GET("/order/:uuid", r.GetOrderByUUID)
	protected.GET("/orders/filter", r.OrdersFiler)

	temporary := r.Router.Group(apiPrefix + "/temp")
	temporary.POST("/start_grabbing", r.StartGrabbing)
	temporary.GET("/stop_grabbing", r.StopGrabbing)
}
