package server

import (
	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/services/analytic/handler"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"net/http"
)

// StartGrabbing temporary endpoint

func (r *Rest) StopGrabbing(c echo.Context) error {
	r.Handler.CS.SetStopFlag()
	return nil
}

func (r *Rest) StartGrabbing(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "finding addresses",
	})
	var (
		req handler.GrabbingStruct
	)
	err := c.Bind(&req)
	if err != nil {
		log.WithField("reason", "failed to bind data").Error("error binding data")
		res := logs.OutputRestError("cant bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	go r.Handler.StartGrabbing(&req)

	log.Infof("start grabbing %s data", req.DataType)

	return c.JSON(http.StatusOK, "Grabbing Started")
}
