package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

func (r *Rest) Drivers(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "drivers report")

	var filter proto.DriverFilter
	if err := c.Bind(&filter); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.Drivers(c.Request().Context(), &filter)
	if err != nil {
		log.WithField("reason", "getting drivers report").Error(err)
		res := logs.OutputRestError("can't get drivers report", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) GetDriverInfo(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "drivers report")

	uuid := c.Param("uuid")

	out, err := r.Handler.GetDriverInfo(c.Request().Context(), uuid)
	if err != nil {
		log.WithField("reason", "getting drivers report").Error(err)
		res := logs.OutputRestError("can't get drivers report", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, out)
}
