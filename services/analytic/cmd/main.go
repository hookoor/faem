package main

import (
	"context"
	"flag"
	"log"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/services/analytic/cloudstorage"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/store"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	mymware "gitlab.com/faemproject/backend/faem/pkg/web/middleware"
	subscriber "gitlab.com/faemproject/backend/faem/services/analytic/broker/rabsubscriber"
	"gitlab.com/faemproject/backend/faem/services/analytic/config"
	"gitlab.com/faemproject/backend/faem/services/analytic/handler"
	"gitlab.com/faemproject/backend/faem/services/analytic/repository"
	"gitlab.com/faemproject/backend/faem/services/analytic/server"
)

const (
	defaultConfigPath     = "config/analytic.toml"
	maxRequestsAllowed    = 300
	serverShutdownTimeout = 30 * time.Second
	brokerShutdownTimeout = 30 * time.Second
)

func main() {
	// Parse flags
	configPath := flag.String("config", defaultConfigPath, "configuration file path")
	flag.Parse()

	cfg, err := config.Parse(*configPath)
	if err != nil {
		log.Fatalf("failed to parse the config file: %v", err)
	}

	if err := logs.SetLogLevel(cfg.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(cfg.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	logger := logs.Eloger

	// Connect to the dbs and remember to close them
	crmDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Database.CRM.Host, cfg.Database.CRM.Port),
		User:     cfg.Database.CRM.User,
		Password: cfg.Database.CRM.Password,
		Database: cfg.Database.CRM.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create a crm db instance: %v", err)
	}
	defer crmDB.Close()

	billingDB, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Database.Billing.Host, cfg.Database.Billing.Port),
		User:     cfg.Database.Billing.User,
		Password: cfg.Database.Billing.Password,
		Database: cfg.Database.Billing.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create a billing db instance: %v", err)
	}
	defer billingDB.Close()

	// Connect to the broker and remember to close it
	rmq := &rabbit.Rabbit{
		Credits: rabbit.ConnCredits{
			URL:  cfg.Broker.UserURL,
			User: cfg.Broker.UserCredits,
		},
	}
	if err = rmq.Init(cfg.Broker.ExchangePrefix, cfg.Broker.ExchangePostfix); err != nil {
		logger.Fatalf("failed to connect to RabbitMQ: %v", err)
	}
	defer rmq.CloseRabbit()

	bq, err := repository.BigQueryInit(context.Background(), cfg.BigQuery)
	if err != nil {
		logger.Fatalf("failed to create a Bigquery instance: %v", err)
	}
	//defer bq.Client.Close()
	defer bq.Close()

	cs, err := cloudstorage.InitCloudStorage(cfg.CloudStorage.BucketName)
	if err != nil {
		logger.Fatalf("failed to create a CloudStorage client: %v", err)
	}
	defer cs.Close()

	// Create a service object
	hdlr := handler.Handler{
		DB: &repository.Pg{
			CrmDB:     crmDB,
			BillingDB: billingDB,
		},
		DL: &repository.BigQuery{
			OrdersEventInserter:          bq.OrdersEventInserter,
			DriversEventInserter:         bq.DriversEventInserter,
			TelephonyEventInserter:       bq.TelephonyEventInserter,
			OrderInserter:                bq.OrderInserter,
			FullOrderInserter:            bq.FullOrderInserter,
			DriverInserter:               bq.DriverInserter,
			OrdersTable:                  bq.OrdersTable,
			FullOrdersTable:              bq.FullOrdersTable,
			DriversTable:                 bq.DriversTable,
			DriversEventsTable:           bq.DriversEventsTable,
			OrdersEventsTable:            bq.OrdersEventsTable,
			Client:                       bq.Client,
			BillingAccountsInserter:      bq.BillingAccountsInserter,
			BillingBankTransfersInserter: bq.BillingBankTransfersInserter,
			BillingEntriesInserter:       bq.BillingEntriesInserter,
			BillingTransactionsInserter:  bq.BillingTransactionsInserter,
			BillingTransfersInserter:     bq.BillingTransfersInserter,
			BillingEventsTables:          bq.BillingEventsTables,
		},
		CS: &cs,
	}

	hdlr.InitSaverTicker()

	hdlr.InitBillingEventsSaverTicker()

	sub := subscriber.Subscriber{
		Rabbit:  rmq,
		Encoder: &rabbit.JsonEncoder{},
		Handler: &hdlr,
	}
	if err = sub.Init(); err != nil {
		logger.Fatalf("failed to start the subscriber: %v", err)
	}
	defer sub.Wait(brokerShutdownTimeout)

	// Create a rest gateway and handle http requests
	router := web.NewRouter(
		loggerOption(logger),
		cors,
		throttler,
		prometheusmetric,
	)
	rest := server.Rest{
		Router:  router,
		Handler: &hdlr,
	}
	rest.Route()

	// Start an http server and remember to shut it down
	go web.Start(router, cfg.Application.Port)
	defer web.Stop(router, serverShutdownTimeout)

	// Wait for program exit
	<-os.NotifyAboutExit()
}

func loggerOption(logger *logrus.Logger) web.Option {
	return func(e *echo.Echo) {
		e.Logger = &mymware.Logger{Logger: logger} // replace the original echo.Logger with the logrus one
		// Log the requests
		e.Use(mymware.LoggerWithSkipper(
			func(c echo.Context) bool {
				return strings.Contains(c.Request().RequestURI, "/metrics")
			},
		))
	}
}

func cors(e *echo.Echo) {
	e.Use(middleware.CORS()) // allow CORS
}

func throttler(e *echo.Echo) {
	e.Use(mymware.Throttle(maxRequestsAllowed))
}

func prometheusmetric(e *echo.Echo) {
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
}
