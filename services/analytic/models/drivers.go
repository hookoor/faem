package models

import (
	"gitlab.com/faemproject/backend/faem/services/crm/models"
	"strings"
	"time"
)

type DriverReportSort string

const (
	DriverReportSortName DriverReportSort = "name" // default

	DriverReportSortOffered  DriverReportSort = "offered"
	DriverReportSortAccepted DriverReportSort = "accepted"
	DriverReportSortFinished DriverReportSort = "finished"
	DriverReportSortIncome   DriverReportSort = "income"
)

type DriverFilter struct {
	DriverUUID  string
	PeriodStart time.Time
	PeriodEnd   time.Time
	Sort        struct {
		Field string
		Dir   SortDir
	}
}

type (
	BQDriver struct {
		UUID                   string    `json:"uuid"`
		Name                   string    `json:"name" required:"true"`
		PaymentTypes           string    `json:"payment_types"`
		Phone                  string    `json:"phone" description:"Телефон" required:"true"`
		Comment                string    `json:"comment" description:"Comment"`
		Status                 string    `json:"state_name" sql:"status" description:"статус водителя"`
		Car                    string    `json:"car" description:"Машина"`
		Balance                float64   `json:"balance"`
		CardBalance            float64   `json:"card_balance"`
		MaxServiceLevel        int       `json:"max_service_level,omitempty"`
		Karma                  float64   `json:"karma" description:"Рейтинг"`
		Color                  string    `json:"color"`
		DrvTariffUUID          string    `json:"drv_tariff_uuid"`
		Tag                    string    `json:"tag" sql:",notnull"`
		AvailableServiceUUIDs  string    `json:"available_service_uui_ds"`
		AvailableFeaturesUUIDs string    `json:"available_features_uui_ds"`
		Alias                  int       `json:"alias" description:"уникальный код водителя"`
		RegNumber              string    `json:"reg_number" description:"Номера машины"`
		Activity               int       `json:"activity" sql:",notnull"`
		FullRegistrationNumber string    `json:"full_registration_number,omitempty"`
		GroupUUID              string    `json:"group" sql:"driver_group"`
		Blacklist              string    `json:"blacklist"`
		TaxiParkUUID           string    `json:"taxi_park_uuid"`
		CreatedAt              time.Time `sql:"default:now()" json:"-" description:"Дата создания"`
		UpdatedAt              time.Time `json:"-" `
	}

	Driver struct {
		tableName struct{} `pg:",discard_unknown_columns"`

		UUID     string `json:"uuid"`
		Name     string `json:"name"`
		Offered  int    `json:"offered"`
		Accepted int    `json:"accepted"`
		Finished int    `json:"finished"`
		Income   int    `json:"income"`
	}

	DriverReport struct {
		Drivers       []*Driver `json:"drivers"`
		TotalOffered  int       `json:"total_offered"`
		TotalAccepted int       `json:"total_accepted"`
		TotalFinished int       `json:"total_finished"`
		TotalIncome   float64   `json:"total_income"`
	}
)

func DriverToBQDriver(driver models.DriverCRM) BQDriver {
	var paymentTypes []string
	for _, v := range driver.PaymentTypes {
		paymentTypes = append(paymentTypes, string(v))
	}

	var serviceUUIDs []string
	for _, v := range driver.AvailableServices {
		serviceUUIDs = append(serviceUUIDs, v.UUID)
	}

	var featuresUUIDs []string
	for _, v := range driver.AvailableFeatures {
		featuresUUIDs = append(featuresUUIDs, v.UUID)
	}

	maxServiceLevel := 0
	if driver.MaxServiceLevel != nil {
		maxServiceLevel = *driver.MaxServiceLevel
	}

	return BQDriver{
		UUID:                   driver.UUID,
		Name:                   driver.Name,
		PaymentTypes:           strings.Join(paymentTypes, ", "),
		Phone:                  driver.Phone,
		Comment:                driver.Comment,
		Status:                 driver.Status,
		Car:                    driver.Car,
		Balance:                driver.Balance,
		CardBalance:            driver.CardBalance,
		MaxServiceLevel:        maxServiceLevel,
		Karma:                  driver.Karma,
		Color:                  driver.Color,
		DrvTariffUUID:          driver.DrvTariff.UUID,
		Tag:                    strings.Join(driver.Tag, ", "),
		AvailableServiceUUIDs:  strings.Join(serviceUUIDs, ", "),
		AvailableFeaturesUUIDs: strings.Join(featuresUUIDs, ", "),
		Alias:                  driver.Alias,
		RegNumber:              driver.RegNumber,
		Activity:               driver.Activity,
		FullRegistrationNumber: driver.FullRegistrationNumber,
		GroupUUID:              driver.Group.UUID,
		Blacklist:              strings.Join(driver.Blacklist, ", "),
		TaxiParkUUID:           driver.TaxiParkShortFields.TaxiParkData.UUID,
		CreatedAt:              driver.CreatedAt,
		UpdatedAt:              driver.UpdatedAt,
	}
}
