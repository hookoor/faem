package models

import (
	"time"
)

const (
	TimeZoneShort = "MSK"
)

type OperatorReportSort string

const (
	OperatorReportSortName OperatorReportSort = "name" // default

	OperatorReportSortCallsTotal OperatorReportSort = "calls.total"
	OperatorReportSortCallsDay   OperatorReportSort = "calls.day"
	OperatorReportSortCallsNight OperatorReportSort = "calls.night"

	OperatorReportSortOrdersCreatedTotal OperatorReportSort = "orders_created.total"
	OperatorReportSortOrdersCreatedDay   OperatorReportSort = "orders_created.day"
	OperatorReportSortOrdersCreatedNight OperatorReportSort = "orders_created.night"

	OperatorReportSortOrdersSuccessfulTotal OperatorReportSort = "orders_successful.total"
	OperatorReportSortOrdersSuccessfulDay   OperatorReportSort = "orders_successful.day"
	OperatorReportSortOrdersSuccessfulNight OperatorReportSort = "orders_successful.night"

	OperatorReportSortOrdersCancelledTotal OperatorReportSort = "orders_cancelled.total"
	OperatorReportSortOrdersCancelledDay   OperatorReportSort = "orders_cancelled.day"
	OperatorReportSortOrdersCancelledNight OperatorReportSort = "orders_cancelled.night"
)

type OperatorChange string

const (
	OperatorChangeDay   OperatorChange = "day"
	OperatorChangeNight OperatorChange = "night"
)

type OperatorFilter struct {
	OperatorUUID string
	PeriodStart  time.Time
	PeriodEnd    time.Time
	DayStart     string
	DayEnd       string
	Sort         struct {
		Field string
		Dir   SortDir
	}
}

type (
	OperatorItems struct {
		Day   int `json:"day"`
		Night int `json:"night"`
		Total int `json:"total"`
	}

	Operator struct {
		UUID             string        `json:"uuid"`
		Name             string        `json:"name"`
		Calls            OperatorItems `json:"calls"`
		OrdersCreated    OperatorItems `json:"orders_created"`
		OrdersSuccessful OperatorItems `json:"orders_successful"`
		OrdersCancelled  OperatorItems `json:"orders_cancelled"`
	}

	OperatorReport struct {
		Operators             []*Operator   `json:"operators"`
		TotalCalls            OperatorItems `json:"total_calls"`
		TotalOrdersCreated    OperatorItems `json:"total_orders_created"`
		TotalOrdersSuccessful OperatorItems `json:"total_orders_successful"`
		TotalOrdersCancelled  OperatorItems `json:"total_orders_cancelled"`
	}
)

type OperatorDBItem struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	UUID   string
	Name   string
	Change OperatorChange
	Aggr   int
}
