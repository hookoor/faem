package models

import (
	"encoding/json"
	"time"

	"cloud.google.com/go/bigquery"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

const (
	OrderSourceCRM   = "crm"
	DefaultOrderSort = "source"
)

type OrderFilter struct {
	Source      string
	PeriodStart time.Time
	PeriodEnd   time.Time
	Sort        struct {
		Field string
		Dir   SortDir
	}
}

type (
	BQOrderRaw struct {
		//Common data
		ID                 bigquery.NullInt64 `json:"id"`
		UUID               string             `json:"uuid"`
		RoutesCount        int                `json:"rotes_count"`          //количество точек
		ServiceName        string             `json:"service_name"`         //название услуги
		Features           string             `json:"features"`             //фичи
		CreatedDatetime    time.Time          `json:"created_datetime"`     //вермя создания
		Source             string             `json:"source"`               //источник заказа
		OrderState         string             `json:"order_state"`          //тип завершения заказа
		CancelReason       string             `json:"cancel_reason"`        //причина отмены
		OrderTakenTime     time.Time          `json:"order_taken_time"`     //время когда заказ был взят
		ArrivalTimeReal    time.Time          `json:"arrival_real_time"`    //время прибытия по факту
		ArrivalTimePromise time.Time          `json:"arrival_promise_time"` //обещанное время (сек)
		DistanceToClient   float64            `json:"distance_to_client"`   //растояние до клиента
		TripDistance       float64            `json:"trip_distance"`        //растояние поездки
		CancelTime         time.Time          `json:"cancel_time"`          //
		OwnerUUID          string             `json:"owner_uuid"`           //идентификатор оунера
		UserStartName      string             `json:"user_start_name"`      //какой пользователь запустил заказа
		UserStartUUID      string             `json:"user_start_uuid"`      //тип оплаты

		//pickup data
		PickupLon      float32   `json:"pickup_lon"`
		PickupLat      float32   `json:"pickup_lat"`
		PickupDatetime time.Time `json:"pickup_datetime"` // во сколько началась поездка
		PickupArea     string    `json:"pickup_area"`     // район откуда забрали
		PickupAddress  string    `json:"pickup_address"`  //адрес

		//dropoff data
		DropoffLon      float32   `json:"dropoff_lon"`
		DropoffLat      float32   `json:"dropoff_lat"`
		DropoffDatetime time.Time `json:"dropoff_datetime"` // откуда
		DropoffArea     string    `json:"dropoff_area"`
		DropoffAddress  string    `json:"dropoff_address"`

		//tariff data
		TariffName       string `json:"tariff_name"`       //выбранный тариф
		TariffPrice      int    `json:"tariff_price"`      //цена по тарифу
		RealPrice        int    `json:"real_price"`        //цена по факту
		WaitingTime      int    `json:"waiting_time"`      //ожидание (сек)
		WaitingPrice     int    `json:"waiting_price"`     //стоимость ожидания
		BonusPayment     int    `json:"bonus_payment"`     //оплата бонусами
		GuaranteedIncome int    `json:"guaranteed_income"` //гарантированный доход
		ClientAllowance  int    `json:"client_allowance"`  //клиентская надбавка

		//driver data
		DriverUUID         string              `json:"driver_uuid"`           //id водителя
		DriverCar          string              `json:"driver_car"`            //машина водителя
		DriverCarRegNumber bigquery.NullString `json:"driver_car_reg_number"` //гос. номер
		DriverCarColor     bigquery.NullString `json:"driver_car_color"`      //цвет машины
		DriverTarrif       string              `json:"driver_tarrif"`         //тариф водителя
		DriverName         bigquery.NullString `json:"driver_name"`
		DriverAlias        bigquery.NullInt64  `json:"driver_alias"` //позывной

		//client data
		ClientPhone string `json:"client_phone"` //номер клиента
		ClientUUID  string `json:"client_uuid"`  //
		PaymentType string `json:"payment_type"` //тип оплаты

		//products data
		StoreUUID     string `json:"store_uuid"`
		ProductsSum   int    `json:"product_sum"`
		ProductsCount int    `json:"product_count"`
		ProductsData  string `json:"product_data"`

		//insert datetime
		InsertDateTime time.Time `json:"insert_datetime"` // время вставки

		//order events list
		Events []OrderEventData `json:"events"`
	}
	BQFullOrder struct {
		tableName                 struct{}               `sql:"crm_orders" pg:",discard_unknown_columns"`
		UUID                      bigquery.NullString    `json:"uuid"`
		Comment                   bigquery.NullString    `json:"comment"`
		Routes                    bigquery.NullString    `json:"routes"`
		Features                  bigquery.NullString    `json:"features"`
		Driver                    bigquery.NullString    `json:"driver"`
		Tariff                    bigquery.NullString    `json:"tariff"`
		Service                   bigquery.NullString    `json:"service"`
		Owner                     bigquery.NullString    `json:"owner"`
		Client                    bigquery.NullString    `json:"client"`
		ClUUID                    bigquery.NullString    `json:"cl_uuid"`
		SrUUID                    bigquery.NullString    `json:"sr_uuid"`
		FeaturesUuids             bigquery.NullString    `json:"features_uuids"`
		StartUserUUID             bigquery.NullString    `json:"start_user_uuid"`
		LastUserUUID              bigquery.NullString    `json:"last_user_uuid"`
		FinishUserUUID            bigquery.NullString    `json:"finish_user_uuid"`
		AppointmentTime           bigquery.NullTimestamp `json:"appointment_time"`
		CompleteTime              bigquery.NullTimestamp `json:"complete_time"`
		PickupTime                bigquery.NullTimestamp `json:"pickup_time"`
		CreatedAt                 bigquery.NullTimestamp `json:"created_at"`
		UpdatedAt                 bigquery.NullTimestamp `json:"updated_at"`
		Deleted                   bigquery.NullBool      `json:"deleted"`
		OrderState                bigquery.NullString    `json:"order_state"`
		CancelTime                bigquery.NullTimestamp `json:"cancel_time"`
		OrderStart                bigquery.NullTimestamp `json:"order_start"`
		ID                        bigquery.NullInt64     `json:"id"`
		RouteWayData              bigquery.NullString    `json:"route_way_data"`
		StartUserName             bigquery.NullString    `json:"start_user_name"`
		IncreasedFare             bigquery.NullFloat64   `json:"increased_fare"`
		Source                    bigquery.NullString    `json:"source"`
		DriverRating              bigquery.NullString    `json:"driver_rating"`
		ClientRating              bigquery.NullString    `json:"client_rating"`
		Promotion                 bigquery.NullString    `json:"promotion"`
		CallbackPhone             bigquery.NullString    `json:"callback_phone"`
		ArrivalTime               bigquery.NullInt64     `json:"arrival_time"`
		IsOptional                bigquery.NullBool      `json:"is_optional"`
		ReportedAppointment       bigquery.NullBool      `json:"reported_appointment"`
		ReportedArrival           bigquery.NullBool      `json:"reported_arrival"`
		ReasonOfCancellation      bigquery.NullString    `json:"reason_of_cancellation"`
		ReasonOfCancellationTitle bigquery.NullString    `json:"reason_of_cancellation_title"`
		DriverArrivalTimeData     bigquery.NullString    `json:"driver_arrival_time_data"`
		StateTransferTime         bigquery.NullTimestamp `json:"state_transfer_time"`
		PaymentType               bigquery.NullString    `json:"payment_type"`
		StartUserLoginTelephony   bigquery.NullString    `json:"start_user_login_telephony"`
		ImportanceReasons         bigquery.NullString    `json:"importance_reasons"`
		SendSms                   bigquery.NullBool      `json:"send_sms"`
		WithoutDelivery           bigquery.NullBool      `json:"without_delivery"`
		PaymentMeta               bigquery.NullString    `json:"payment_meta"`
		DeliveredToDriver         bigquery.NullBool      `json:"delivered_to_driver"`
		TaxiParkUUID              bigquery.NullString    `json:"taxi_park_uuid"`
		DistributionByTaxiPark    bigquery.NullBool      `json:"distribution_by_taxi_park"`
		OwnDelivery               bigquery.NullBool      `json:"own_delivery"`
		EstimatedDeliveryTime     bigquery.NullTimestamp `json:"estimated_delivery_time"`
		ProductsData              bigquery.NullString    `json:"products_data"`
		CounterOrderMarker        bigquery.NullBool      `json:"counter_order_marker"`
		RegionID                  bigquery.NullInt64     `json:"region_id"`
		SourceOfOrdersUUID        bigquery.NullString    `json:"source_of_orders_uuid"`
		InsertTime                bigquery.NullTimestamp `json:"insert_time"`
	}

	Order struct {
		tableName        struct{} `pg:",discard_unknown_columns"`
		Source           string   `json:"source"`
		Count            int      `json:"count"`
		Finished         int      `json:"finished"`
		Cancelled        int      `json:"cancelled"`
		FinishedPercent  float64  `json:"finished_percent"`
		CancelledPercent float64  `json:"cancelled_percent"`
	}

	OrderReport struct {
		Orders                  []*Order `json:"orders"`
		TotalCount              int      `json:"total_count"`
		TotalFinished           int      `json:"total_finished"`
		TotalCancelled          int      `json:"total_cancelled"`
		AverageFinishedPercent  float64  `json:"average_finished_percent"`
		AverageCancelledPercent float64  `json:"average_cancelled_percent"`
	}
	// OrderCRM main orders CRM struct
	OrderFromCRM struct {
		tableName struct{} `sql:"crm_orders" pg:",discard_unknown_columns"`
		orders.OrderCRM
	}
)

func convertSingle(order OrderFromCRM) BQOrderRaw {
	var bqOrder BQOrderRaw
	loc, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		panic(err)
	}
	if len(order.FeaturesUUIDS) > 0 {
		for _, v := range order.FeaturesUUIDS {
			bqOrder.Features = bqOrder.Features + v + ", "
		}
		bqOrder.Features = bqOrder.Features[:len(bqOrder.Features)-2]
	}
	bqOrder.OrderTakenTime = time.Unix(order.DriverArrivalTimeData.IndicatedArrivalTime-int64(order.DriverArrivalTimeData.IndicatedTime), 0)
	bqOrder.ID = bigquery.NullInt64{Int64: int64(order.ID), Valid: true}
	bqOrder.UUID = order.UUID
	bqOrder.RoutesCount = len(order.Routes)
	bqOrder.ServiceName = order.Service.Name
	bqOrder.CreatedDatetime = order.CreatedAt.In(loc)
	if !order.CancelTime.IsZero() {
		bqOrder.CancelTime = order.CompleteTime.In(loc)
	}
	bqOrder.Source = order.Source
	bqOrder.OrderState = order.OrderState
	bqOrder.CancelReason = order.ReasonOfCancellation
	bqOrder.ArrivalTimeReal = time.Unix(order.DriverArrivalTimeData.ArrivedInFact, 0)
	bqOrder.ArrivalTimePromise = time.Unix(order.DriverArrivalTimeData.IndicatedArrivalTime, 0)
	bqOrder.DistanceToClient = order.RouteWayData.RouteFromDriverToClient.Proper.Distance
	bqOrder.TripDistance = order.RouteWayData.Geometry.Proper.Distance

	if len(order.Routes) > 1 {
		bqOrder.PickupLon = order.Routes[0].Lon
		bqOrder.PickupLat = order.Routes[0].Lat
		if order.Tariff.OrderStartTime != 0 {
			bqOrder.PickupDatetime = time.Unix(order.Tariff.OrderStartTime, 0)
		}
		//bqOrder.PickupArea =
		bqOrder.PickupAddress = order.Routes[0].UnrestrictedValue + "$" + order.Routes[0].City

		dpi := len(order.Routes) - 1
		bqOrder.DropoffLon = order.Routes[dpi].Lon
		bqOrder.DropoffLat = order.Routes[dpi].Lat

		if !order.CompleteTime.IsZero() {
			bqOrder.DropoffDatetime = order.CompleteTime.In(loc)
		}
		//bqOrder.DropoffArea =
		bqOrder.DropoffAddress = order.Routes[dpi].UnrestrictedValue + "$" + order.Routes[dpi].City

	}
	bqOrder.TariffName = order.Tariff.Name
	bqOrder.TariffPrice = order.Tariff.TotalPrice - order.Tariff.WaitingPrice
	bqOrder.RealPrice = order.Tariff.TotalPrice
	bqOrder.WaitingPrice = order.Tariff.WaitingPrice
	bqOrder.DriverUUID = order.Driver.UUID
	bqOrder.DriverCar = order.Driver.Car
	bqOrder.DriverTarrif = order.Driver.DrvTariff.Name
	bqOrder.DriverCarColor = bigquery.NullString{StringVal: order.Driver.Color, Valid: true}
	bqOrder.DriverCarRegNumber = bigquery.NullString{StringVal: order.Driver.FullRegistrationNumber, Valid: true}
	bqOrder.DriverAlias = bigquery.NullInt64{Int64: int64(order.Driver.Alias), Valid: true}
	bqOrder.DriverName = bigquery.NullString{StringVal: order.Driver.Name, Valid: true}
	bqOrder.OwnerUUID = order.GetTaxiParkUUID()
	bqOrder.ClientPhone = order.Client.MainPhone
	bqOrder.ClientUUID = order.Client.UUID

	bqOrder.UserStartName = order.StartUserName
	bqOrder.UserStartUUID = order.StartUserUUID

	bqOrder.PaymentType = order.Tariff.PaymentType
	bqOrder.BonusPayment = order.Tariff.BonusPayment
	bqOrder.ClientAllowance = int(order.IncreasedFare)
	bqOrder.GuaranteedIncome = getGuranteed(order.Tariff)

	if order.ProductsData != nil {
		bqOrder.StoreUUID = order.ProductsData.StoreData.UUID
		if len(order.ProductsData.Pruducts) > 0 {
			var sum int
			var cnt int
			for _, v := range order.ProductsData.Pruducts {
				sum += v.Price
				cnt++
			}
			bqOrder.ProductsSum = sum
			bqOrder.ProductsCount = cnt
		}
		bt, err := json.Marshal(order.ProductsData)
		if err != nil {
			bqOrder.ProductsData = "error marshaling product data"
		}
		bqOrder.ProductsData = string(bt)
	}

	if order.ID != int(bqOrder.ID.Int64) ||
		order.Driver.Color != bqOrder.DriverCarColor.String() ||
		order.Driver.FullRegistrationNumber != bqOrder.DriverCarRegNumber.String() ||
		order.Driver.Name != bqOrder.DriverName.String() ||
		order.Driver.Alias != int(bqOrder.DriverAlias.Int64) {
		logs.Eloger.WithFields(logrus.Fields{
			"uuid":                bqOrder.UUID,
			"id":                  order.ID,
			"driver_car_color":    order.Driver.Color,
			"driver_car_num":      order.Driver.RegNumber,
			"driver_name":         order.Driver.Name,
			"driver_alias":        order.Driver.Alias,
			"bq-id":               bqOrder.ID,
			"bq-driver_car_color": bqOrder.DriverCarColor.String(),
			"bq-driver_car_num":   bqOrder.DriverCarRegNumber.String(),
			"bq-driver_name":      bqOrder.DriverName.String(),
			"bq-driver_alias":     bqOrder.DriverAlias.String(),
		}).Error("unsaved data")
	}

	//bqOrder.timeToMsk()
	return bqOrder
}

//ConvertToBQ converts to BigQuery orders structures
func ConvertToBQ(orders *[]OrderFromCRM) []*BQOrderRaw {
	var cnBQ []*BQOrderRaw
	insertTime := time.Now().UTC()
	if len(*orders) > 0 {
		for _, v := range *orders {
			b := convertSingle(v)
			b.InsertDateTime = insertTime
			cnBQ = append(cnBQ, &b)
		}
	}
	return cnBQ
}

func ConvertToBQFullOrders(orders *[]OrderFromCRM) []BQFullOrder {
	var convertedFullOrders []BQFullOrder
	insertTime := time.Now().UTC()
	if len(*orders) > 0 {
		for _, order := range *orders {
			convertedFullOrder := convertSingleFullOrder(order)
			convertedFullOrder.InsertTime.Timestamp = insertTime
			convertedFullOrders = append(convertedFullOrders, convertedFullOrder)
		}
	}
	return convertedFullOrders
}

// конвертирует заказ из базы в заказ для bigquery
func convertSingleFullOrder(order OrderFromCRM) BQFullOrder {
	var (
		bqFullOrder BQFullOrder
		err         error
	)
	loc, err := time.LoadLocation("Europe/Moscow")
	if err != nil {
		panic(err.Error())
	}
	bqFullOrder.UUID = bigquery.NullString{StringVal: order.UUID, Valid: true}
	if order.Comment != nil {
		bqFullOrder.Comment = bigquery.NullString{StringVal: *order.Comment, Valid: true}
	}
	bqFullOrder.Routes = bigquery.NullString{StringVal: structToString(order.Routes), Valid: true}
	bqFullOrder.Features = bigquery.NullString{StringVal: structToString(order.Features), Valid: true}
	bqFullOrder.Driver = bigquery.NullString{StringVal: structToString(order.Driver), Valid: true}
	bqFullOrder.Tariff = bigquery.NullString{StringVal: structToString(order.Tariff), Valid: true}
	bqFullOrder.Service = bigquery.NullString{StringVal: structToString(order.Service), Valid: true}
	bqFullOrder.Owner = bigquery.NullString{StringVal: structToString(order.SourceOfOrders), Valid: true}
	bqFullOrder.Client = bigquery.NullString{StringVal: structToString(order.Client), Valid: true}
	bqFullOrder.ClUUID = bigquery.NullString{StringVal: order.ClUUID, Valid: true}
	bqFullOrder.SrUUID = bigquery.NullString{StringVal: order.SrUUID, Valid: true}
	bqFullOrder.FeaturesUuids = bigquery.NullString{StringVal: structToString(order.FeaturesUUIDS), Valid: true}
	bqFullOrder.StartUserUUID = bigquery.NullString{StringVal: order.StartUserUUID, Valid: true}
	bqFullOrder.LastUserUUID = bigquery.NullString{StringVal: order.LastUserUUID, Valid: true}
	bqFullOrder.FinishUserUUID = bigquery.NullString{StringVal: order.FinishUserUUID, Valid: true}
	bqFullOrder.AppointmentTime = bigquery.NullTimestamp{Timestamp: order.AppointmentTime.In(loc), Valid: true}
	bqFullOrder.CompleteTime = bigquery.NullTimestamp{Timestamp: order.CompleteTime.In(loc), Valid: true}
	bqFullOrder.PickupTime = bigquery.NullTimestamp{Timestamp: order.PickupTime.In(loc), Valid: true}
	bqFullOrder.CreatedAt = bigquery.NullTimestamp{Timestamp: order.CreatedAt.In(loc), Valid: true}
	bqFullOrder.UpdatedAt = bigquery.NullTimestamp{Timestamp: order.UpdatedAt.In(loc), Valid: true}
	bqFullOrder.Deleted = bigquery.NullBool{Bool: order.Deleted, Valid: true}
	bqFullOrder.OrderState = bigquery.NullString{StringVal: order.OrderState, Valid: true}
	bqFullOrder.OrderStart = bigquery.NullTimestamp{Timestamp: order.OrderStart, Valid: true}
	bqFullOrder.ID = bigquery.NullInt64{Int64: int64(order.ID), Valid: true}
	bqFullOrder.RouteWayData = bigquery.NullString{StringVal: structToString(order.RouteWayData), Valid: true}
	bqFullOrder.StartUserName = bigquery.NullString{StringVal: order.StartUserName, Valid: true}
	bqFullOrder.IncreasedFare = bigquery.NullFloat64{Float64: float64(order.IncreasedFare), Valid: true}
	bqFullOrder.Source = bigquery.NullString{StringVal: order.Source, Valid: true}
	bqFullOrder.DriverRating = bigquery.NullString{StringVal: structToString(order.DriverRating), Valid: true}
	bqFullOrder.ClientRating = bigquery.NullString{StringVal: structToString(order.ClientRating), Valid: true}
	bqFullOrder.Promotion = bigquery.NullString{StringVal: structToString(order.Promotion), Valid: true}
	bqFullOrder.CallbackPhone = bigquery.NullString{StringVal: order.CallbackPhone, Valid: true}
	bqFullOrder.ArrivalTime = bigquery.NullInt64{Int64: order.ArrivalTime, Valid: true}
	if order.IsOptional != nil {
		bqFullOrder.IsOptional = bigquery.NullBool{Bool: *order.IsOptional, Valid: true}
	}
	bqFullOrder.ReportedAppointment = bigquery.NullBool{Bool: order.ReportedAppointment, Valid: true}
	bqFullOrder.ReportedArrival = bigquery.NullBool{Bool: order.ReportedArrival, Valid: true}
	bqFullOrder.ReasonOfCancellation = bigquery.NullString{StringVal: order.ReasonOfCancellation, Valid: true}
	bqFullOrder.ReasonOfCancellationTitle = bigquery.NullString{StringVal: order.ReasonOfCancellationTitle, Valid: true}
	bqFullOrder.DriverArrivalTimeData = bigquery.NullString{StringVal: structToString(order.DriverArrivalTimeData), Valid: true}
	bqFullOrder.StateTransferTime = bigquery.NullTimestamp{Timestamp: order.StateTransferTime, Valid: true}
	bqFullOrder.PaymentType = bigquery.NullString{StringVal: structToString(order.PaymentType), Valid: true}
	bqFullOrder.StartUserLoginTelephony = bigquery.NullString{StringVal: order.StartUserLoginTelethony, Valid: true}
	bqFullOrder.ImportanceReasons = bigquery.NullString{StringVal: structToString(order.ImportanceReasons), Valid: true}
	bqFullOrder.SendSms = bigquery.NullBool{Bool: order.SendSMS, Valid: true}
	bqFullOrder.WithoutDelivery = bigquery.NullBool{Bool: order.WithoutDelivery, Valid: true}
	bqFullOrder.PaymentMeta = bigquery.NullString{StringVal: structToString(order.PaymentMeta), Valid: true}
	bqFullOrder.DeliveredToDriver = bigquery.NullBool{Bool: order.DeliveredToDriver, Valid: true}
	if order.TaxiParkUUID != nil {
		bqFullOrder.TaxiParkUUID = bigquery.NullString{StringVal: *order.TaxiParkUUID, Valid: true}
	}
	if order.DistributionByTaxiPark != nil {
		bqFullOrder.DistributionByTaxiPark = bigquery.NullBool{Bool: *order.DistributionByTaxiPark, Valid: true}
	}
	bqFullOrder.OwnDelivery = bigquery.NullBool{Bool: order.OwnDelivery, Valid: true}
	bqFullOrder.EstimatedDeliveryTime = bigquery.NullTimestamp{Timestamp: order.EstimatedDeliveryTime, Valid: true}
	if order.ProductsData != nil {
		bqFullOrder.ProductsData = bigquery.NullString{StringVal: structToString(order.ProductsData), Valid: true}
	}
	bqFullOrder.CounterOrderMarker = bigquery.NullBool{Bool: order.CounterOrderMarker, Valid: true}
	bqFullOrder.RegionID = bigquery.NullInt64{Int64: int64(order.RegionID), Valid: true}
	bqFullOrder.SourceOfOrdersUUID = bigquery.NullString{StringVal: order.SourceOfOrdersUUID, Valid: true}
	if !order.CancelTime.IsZero() {
		bqFullOrder.CancelTime = bigquery.NullTimestamp{Timestamp: order.CompleteTime.In(loc)}
	}
	return bqFullOrder
}

func getGuranteed(tariff structures.Tariff) int {
	for _, v := range tariff.Items {
		if v.Name == "Гарантированный доход водителя" {
			return v.Price
		}
	}
	return 0
}

func structToString(s interface{}) string {
	bytes, err := json.Marshal(s)
	if err != nil {
		panic(err.Error())
	}
	return string(bytes)
}

//func getString(o *string) string {
//	if o == nil {
//		return ""
//	}
//	return *o
//}
//func getInt(o *int) int {
//	if o == nil {
//		return 0
//	}
//	return *o
//}
//func getBool(o *bool) bool {
//	if o == nil {
//		return false
//	}
//	return *o
//}

//func ConvertOrderToBQ(ofc OrderFromCRM) BQOrder {
//	var bqOrder BQOrder
//	bqOrder.UUID = ofc.UUID
//	bqOrder.Comment = getString(ofc.Comment)
//	bqOrder.Routes = routesToString(ofc.Routes)
//	bqOrder.Features = toJSON(ofc.Features)
//	bqOrder.Tariff = toJSON(ofc.Tariff)
//	bqOrder.FixedPrice = ofc.FixedPrice
//	bqOrder.Service = toJSON(ofc.Service)
//	bqOrder.IncreasedFare = ofc.IncreasedFare
//	bqOrder.Driver = toJSON(ofc.Driver)
//	bqOrder.Owner = toJSON(ofc.Owner)
//	bqOrder.Client = toJSON(ofc.Client)
//	bqOrder.Source = ofc.Source
//	bqOrder.ProductsInput = productDataToString(ofc.ProductsInput)
//	bqOrder.DriverRating = toJSON(ofc.DriverRating)
//	bqOrder.ClientRating = toJSON(ofc.ClientRating)
//	bqOrder.IsOptional = getBool(ofc.IsOptional)
//	bqOrder.WithoutDelivery = ofc.WithoutDelivery
//	bqOrder.OrderStart = ofc.OrderStart
//	bqOrder.CancelTime = ofc.CancelTime
//	bqOrder.CreatedAt = ofc.CreatedAt
//	bqOrder.Promotion = toJSON(ofc.Promotion)
//	bqOrder.ArrivalTime = ofc.ArrivalTime
//	bqOrder.ProductsData = toJSON(ofc.ProductsData)
//	bqOrder.CallbackPhone = ofc.CallbackPhone
//	bqOrder.PaymentType = string(ofc.PaymentType)
//	bqOrder.ClUUID = ofc.ClUUID
//	bqOrder.SrUUID = ofc.SrUUID
//	bqOrder.FeaturesUUIDS = ofc.FeaturesUUIDS
//	bqOrder.StartUserUUID = ofc.StartUserUUID
//	bqOrder.OrderNumberInStore = ofc.OrderNumberInStore
//	bqOrder.StartUserName = ofc.StartUserName
//	bqOrder.ProductsPrice = ofc.ProductsPrice
//	bqOrder.StoreUUID = ofc.StoreUUID
//	bqOrder.StartUserLoginTelethony = ofc.StartUserLoginTelethony
//	bqOrder.LastUserUUID = ofc.LastUserUUID
//	bqOrder.DriverArrivalTimeData = toJSON(ofc.DriverArrivalTimeData)
//	bqOrder.ReasonOfCancellation = ofc.ReasonOfCancellation
//	bqOrder.ReasonOfCancellationTitle = ofc.ReasonOfCancellationTitle
//	bqOrder.FinishUserUUID = ofc.FinishUserUUID
//	bqOrder.CancelTimeUnix = ofc.CancelTimeUnix
//	bqOrder.AppointmentTime = ofc.AppointmentTime
//	bqOrder.CompleteTime = ofc.CompleteTime
//	bqOrder.CompleteTimeUnix = ofc.CompleteTimeUnix
//	bqOrder.PreparationTime = getInt(ofc.PreparationTime)
//	bqOrder.PickupTime = ofc.PickupTime
//	bqOrder.OwUUID = ofc.OwUUID
//	bqOrder.CreatedAtUnix = ofc.CreatedAtUnix
//	bqOrder.UpdatedAt = ofc.UpdatedAt
//	bqOrder.Deleted = ofc.Deleted
//	bqOrder.OrderState = ofc.OrderState
//	bqOrder.StateTitle = ofc.StateTitle
//	bqOrder.TripTime = ofc.TripTime
//	bqOrder.ReportedAppointment = ofc.ReportedAppointment
//	bqOrder.StateTransferTime = ofc.StateTransferTime
//	bqOrder.ImportanceReasons = importanseReasonsToString(ofc.ImportanceReasons)
//	bqOrder.ReportedArrival = ofc.ReportedArrival
//	bqOrder.SendSMS = ofc.SendSMS
//	bqOrder.Unpaid = getBool(ofc.Unpaid)
//	bqOrder.UnreadMessagesCount = ofc.UnreadMessagesCount
//	return bqOrder
//}

//func routesToString(r []structures.Route) []string {
//	var result []string
//	for _, v := range r {
//		a := toJSON(v)
//		result = append(result, a)
//	}
//	return result
//}
//
//func productDataToString(r []structures.ProductInputData) []string {
//	var result []string
//	for _, v := range r {
//		a := toJSON(v)
//		result = append(result, a)
//	}
//	return result
//}
//
//func importanseReasonsToString(r []orders.ImportanceReason) []string {
//	var result []string
//	for _, v := range r {
//		a := toJSON(v)
//		result = append(result, a)
//	}
//	return result
//}

//-------------//

//type (
//BQOrder struct {
//	UUID            string    `json:"uuid"`
//	Comment         string    `json:"comment"`
//	Routes          []string  `json:"routes"`
//	Features        string    `json:"features"`
//	Tariff          string    `json:"tariff!"`
//	FixedPrice      int       `json:"fixed_price" sql:"-"`
//	Service         string    `json:"service"`
//	IncreasedFare   float32   `json:"increased_fare"`
//	Driver          string    `json:"driver"`
//	Owner           string    `json:"owner"`
//	Client          string    `json:"client"`
//	Source          string    `json:"source"`
//	ProductsInput   []string  `json:"products_input,omitempty" sql:"-"`
//	DriverRating    string    `json:"driver_rating"`
//	ClientRating    string    `json:"client_rating"`
//	IsOptional      bool      `json:"is_optional,omitempty"`
//	WithoutDelivery bool      `json:"without_delivery"`
//	OrderStart      time.Time `json:"order_start"`
//	CancelTime      time.Time `json:"cancel_time"`
//	CreatedAt       time.Time `json:"created_at"`
//	Promotion       string    `json:"promotion"`
//	ArrivalTime     int64     `json:"arrival_time"`
//	ProductsData    string    `json:"products_data"`
//	CallbackPhone   string    `json:"callback_phone"`
//	PaymentType     string    `json:"payment_type"`
//	//OrderCRM struct {
//	ID                        int       `json:"id" sql:",pk"`
//	ClUUID                    string    `json:"client_uuid"`
//	SrUUID                    string    `json:"service_uuid"`
//	FeaturesUUIDS             []string  `json:"features_uuids"`
//	StartUserUUID             string    `json:"user_uuid"`
//	OrderNumberInStore        string    `json:"order_number_in_store,omitempty" sql:"-"`
//	StartUserName             string    `json:"user_name" sql:"start_user_name"`
//	ProductsPrice             int       `json:"products_price,omitempty" sql:"-"`
//	StoreUUID                 string    `json:"-" sql:"-"`
//	StartUserLoginTelethony   string    `json:"user_alias" sql:"start_user_login_telephony"`
//	LastUserUUID              string    `json:"-"`
//	DriverArrivalTimeData     string    `json:"driver_arrival_time_data" sql:"driver_arrival_time_data"`
//	ReasonOfCancellation      string    `json:"reason_of_cancellation"`
//	ReasonOfCancellationTitle string    `json:"reason_of_cancellation_title"`
//	FinishUserUUID            string    `json:"-"`
//	CancelTimeUnix            int64     `json:"cancel_time_unix" sql:"-"`
//	AppointmentTime           time.Time `json:"-" description:"Время назначения авто на заказ"`
//	CompleteTime              time.Time `json:"-" description:"Врем завершения заказа"`
//	CompleteTimeUnix          int64     `json:"complete_time_unix,omitempty" sql:"-"`
//	PreparationTime           int       `json:"preparation_time,omitempty" sql:"-"`
//	PickupTime                time.Time `json:"-" description:"Прибытие авто"`
//	OwUUID                    string    `json:"owner_uuid"`
//	CreatedAtUnix             int64     `json:"created_at_unix" description:"Дата создания в unix" sql:"-"`
//	UpdatedAt                 time.Time `json:"updated_at" `
//	Deleted                   bool      `json:"-" sql:"default:false"`
//	OrderState                string    `json:"state_name" `
//	StateTitle                string    `json:"state_title" sql:"-"`
//	TripTime                  int64     `json:"trip_time" sql:"-"`
//	ReportedAppointment       bool      `json:"reported_appointment"`
//	StateTransferTime         time.Time `json:"state_transfer_time"`
//	ImportanceReasons         []string  `json:"importance_reason" sql:",type:text[]"`
//	ReportedArrival           bool      `json:"reported_arrival"`
//	SendSMS                   bool      `json:"send_sms"`
//	Unpaid                    bool      `json:"unpaid" sql:"-"`
//	UnreadMessagesCount       int       `json:"unread_messages_count" sql:"-"` // кол не прочитанных сообщений
//}
//)
