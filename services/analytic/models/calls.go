package models

import (
	"time"
)

const (
	DefaultCallSort = "source"

	VoipServiceName = "voip"
	CallIncoming    = "incoming"
	CallOutgoing    = "outgoing"
	CallAnswer      = "answer"
)

type CallFilter struct {
	Source      string
	PeriodStart time.Time
	PeriodEnd   time.Time
	Sort        struct {
		Field string
		Dir   SortDir
	}
}

type (
	Call struct {
		tableName struct{} `pg:",discard_unknown_columns"`

		Source   string `json:"source"`
		Incoming int    `json:"incoming"`
		Outgoing int    `json:"outgoing"`
		Missed   int    `json:"missed"`
	}

	CallReport struct {
		Calls         []*Call `json:"calls"`
		TotalIncoming int     `json:"total_incoming"`
		TotalOutgoing int     `json:"total_outgoing"`
		TotalMissed   int     `json:"total_missed"`
	}
)
