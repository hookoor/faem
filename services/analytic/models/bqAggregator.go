package models

import (
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"strings"
	"time"
)

type (
	//OrdersSourceAggr базовая агрегация заказов по услуге и источнику
	OrdersSourceAggr struct {
		TimestampMSK time.Time `json:"timestamp_msk"` //время заказов по МСК
		Source       string    `json:"source"`        //источник заказа (тел/моб)
		Service      string    `json:"service"`       //услуга
		OrderType    string    `json:"order_type"`    //тип завершения заказов
		OrdersCount  int       `json:"orders_count"`  //их количество
		OwnerUUID    string    `json:"owner"`         //owner (линия)
	}
	OperatorDataAggr struct {
		TimestampMSK time.Time `json:"timestamp_msk"` //время заказов по МСК
		Operator     string    `json:"operator"`      //источник заказа (тел/моб)
		Total        int       `json:"total"`         //всего создано заказов
		Success      int       `json:"success"`       //успешных заказов
	}
	ClientTariffAggr struct {
		TimestampMSK time.Time `json:"timestamp_msk"` //время заказов по МСК
		PaymentType  string    `json:"payment_type"`  //тип оплаты
		PaymentSum   int       `json:"payment_sum"`   //сумма оплат
		Tariff       string    `json:"tariff"`        //клиентский тариф
		BonusesSum   int       `json:"bonuses_sum"`   //сумма оплат бонусами
		PriceChanges int       `json:"price_changes"` //сумма изменения цены клиентом
	}
	DriverTariffAggr struct {
		TimestampMSK    time.Time `json:"timestamp_msk"`    //время заказов по МСК
		DriverTariff    string    `json:"driver_tariff"`    //водительский тариф
		OrdersCompleted int       `json:"orders_completed"` //заказов завершено
		OrdersSum       int       `json:"orders_sum"`       //сумма заказов
		GuranteeSum     int       `json:"guarantee_sum"`    //сумма доплат до мин.
		BonusesSum      int       `json:"guarantee_sum"`    //сумма оплат бонусами
	}
	CancelOrdersAggr struct {
		TimestampMSK time.Time `json:"timestamp_msk"` //время заказов по МСК
		Service      string    `json:"service"`       //услуга
		Source       string    `json:"source"`        //источник заказ
		CancelType   string    `json:"cancel_type"`   //тип отказа
		Reason       string    `json:"reason"`        //причина отказа
		Count        int       `json:"count"`         //количество заказов
		AvgTime      int       `json:"average_time"`  //среднее время жизни заказа
	}

	AggrTimeTable struct {
		AggrName   string    `json:"aggr_name"`
		LastInsert time.Time `json:"last_insert"`
		AggrTime   time.Time `json:"aggr_time"`
		LastUUID   string    `json:"last_uuid"`
	}
)

//функция принимает массив заказов и шаг, и возвращает минимальное, максимальное и шаг
func minMaxTime(orders *[]BQOrderRaw, minuteStep int) (time.Time, time.Time, time.Time) {
	var startTime, finishTime, stepTime time.Time
	for k, v := range *orders {
		if k == 0 || v.CreatedDatetime.Before(startTime) {
			startTime = v.CreatedDatetime
		}
		if k == 0 || v.CreatedDatetime.After(finishTime) {
			finishTime = v.CreatedDatetime
		}
	}

	startTime = startTime.Truncate(60 * time.Second)
	stepTime = startTime.Add(time.Minute * time.Duration(minuteStep))
	if stepTime.After(finishTime) {
		stepTime = finishTime
	}
	return startTime, finishTime, stepTime
}

func OrderSourceAggregate(orders []BQOrderRaw, minuteStep int) []OrdersSourceAggr {
	var result []OrdersSourceAggr

	if len(orders) < 1 {
		return result
	}

	startTime, finishTime, stepTime := minMaxTime(&orders, minuteStep)

	for {
		tempMap := make(map[string]int)

		//отбираем заказы из диапазона
		for _, v := range orders {
			if v.CreatedDatetime.After(startTime) && v.CreatedDatetime.Before(stepTime.Add(time.Millisecond)) {
				mapKey := fmt.Sprintf("%s|%s|%s|%s", v.Source, v.ServiceName, v.OrderState, v.OwnerUUID)
				tempMap[mapKey]++
			}
		}

		//агрегируем заказы
		for k, v := range tempMap {
			bqelem := getSourceAggrElement(k, v, startTime)
			result = append(result, bqelem)
		}

		// проверка на последний круг
		if stepTime == finishTime {
			break
		}

		//теперь надо проверить остался ли "хвост"
		if stepTime.Add(time.Minute * time.Duration(minuteStep)).After(finishTime) {
			startTime = stepTime
			stepTime = finishTime
		} else {
			startTime = stepTime
			stepTime = stepTime.Add(time.Minute * time.Duration(minuteStep))
		}
	}

	return result
}

func getSourceAggrElement(key string, val int, t time.Time) OrdersSourceAggr {
	parts := strings.Split(key, "|")
	if len(parts) != 4 {
		return OrdersSourceAggr{}
	}

	return OrdersSourceAggr{
		TimestampMSK: t.Add(3 * time.Hour),
		Source:       parts[0],
		Service:      parts[1],
		OrderType:    parts[2],
		OwnerUUID:    parts[3],
		OrdersCount:  val,
	}
}

func OperatorDataAggregate(orders []BQOrderRaw, minuteStep int) []OperatorDataAggr {
	var result []OperatorDataAggr

	if len(orders) < 1 {
		return result
	}

	startTime, finishTime, stepTime := minMaxTime(&orders, minuteStep)

	for {
		type operOrders struct {
			finished int
			failed   int
		}
		tempMap := make(map[string]operOrders)

		//отбираем заказы из диапазона
		for _, v := range orders {
			if v.CreatedDatetime.After(startTime) && v.CreatedDatetime.Before(stepTime.Add(time.Millisecond)) && v.Source == "crm" {
				if v.OrderState == constants.OrderStateFinished {
					if val, ok := tempMap[v.UserStartUUID]; ok {
						val.finished++
						tempMap[v.UserStartUUID] = val
					} else {
						tempMap[v.UserStartUUID] = operOrders{finished: 1}
					}
				} else {
					if val, ok := tempMap[v.UserStartUUID]; ok {
						val.failed++
						tempMap[v.UserStartUUID] = val
					} else {
						tempMap[v.UserStartUUID] = operOrders{failed: 1}
					}
				}
			}
		}
		// проверка на последний круг
		if stepTime == finishTime {
			break
		}

		//агрегируем заказы
		for k, v := range tempMap {
			operator := OperatorDataAggr{
				TimestampMSK: startTime.Add(3 * time.Hour),
				Operator:     k,
				Total:        v.failed + v.finished,
				Success:      v.finished,
			}
			result = append(result, operator)
		}

		//теперь надо проверить остался ли "хвост"
		if stepTime.Add(time.Minute * time.Duration(minuteStep)).After(finishTime) {
			startTime = stepTime
			stepTime = finishTime
		} else {
			startTime = stepTime
			stepTime = stepTime.Add(time.Minute * time.Duration(minuteStep))
		}
	}
	return result
}

func ClientTariffAggregate(orders []BQOrderRaw, minuteStep int) []ClientTariffAggr {
	var result []ClientTariffAggr

	if len(orders) < 1 {
		return result
	}

	startTime, finishTime, stepTime := minMaxTime(&orders, minuteStep)

	for {
		type clientTariff struct {
			paymentSum  int
			bonusesSum  int
			priceChange int
		}
		tempMap := make(map[string]clientTariff)

		//отбираем заказы из диапазона
		for _, v := range orders {
			if v.CreatedDatetime.After(startTime) && v.CreatedDatetime.Before(stepTime.Add(time.Millisecond)) && v.OrderState == constants.OrderStateFinished {

				mapKey := fmt.Sprintf("%s|%s", v.PaymentType, v.TariffName)
				if val, ok := tempMap[mapKey]; ok {
					val.paymentSum = val.paymentSum + v.RealPrice
					val.bonusesSum = val.bonusesSum + v.BonusPayment
					val.priceChange = val.priceChange + v.ClientAllowance
					tempMap[mapKey] = val
				} else {
					tempMap[mapKey] = clientTariff{
						paymentSum:  v.RealPrice,
						bonusesSum:  v.BonusPayment,
						priceChange: v.ClientAllowance,
					}
				}
			}
		}

		// проверка на последний круг
		if stepTime == finishTime {
			break
		}

		//агрегируем заказы
		for k, v := range tempMap {
			ttype, ttariff := paymentTypeAndTariff(k)
			tariff := ClientTariffAggr{
				TimestampMSK: startTime.Add(3 * time.Hour),
				PaymentType:  ttype,
				PaymentSum:   v.paymentSum,
				Tariff:       ttariff,
				BonusesSum:   v.bonusesSum,
				PriceChanges: v.priceChange,
			}
			result = append(result, tariff)
		}

		//теперь надо проверить остался ли "хвост"
		if stepTime.Add(time.Minute * time.Duration(minuteStep)).After(finishTime) {
			startTime = stepTime
			stepTime = finishTime
		} else {
			startTime = stepTime
			stepTime = stepTime.Add(time.Minute * time.Duration(minuteStep))
		}

	}
	return result
}

func paymentTypeAndTariff(key string) (string, string) {
	parts := strings.Split(key, "|")
	if len(parts) != 2 {
		return "", ""
	}
	return parts[0], parts[1]
}

func DriverTariffAggregate(orders []BQOrderRaw, minuteStep int) []DriverTariffAggr {
	var result []DriverTariffAggr

	if len(orders) < 1 {
		return result
	}

	startTime, finishTime, stepTime := minMaxTime(&orders, minuteStep)

	for {
		type driverTariff struct {
			ordersComplete int
			ordersSum      int
			guranteeSum    int
			bonusesSum     int
		}
		tempMap := make(map[string]driverTariff)

		//отбираем заказы из диапазона
		for _, v := range orders {
			if v.CreatedDatetime.After(startTime) && v.CreatedDatetime.Before(stepTime.Add(time.Millisecond)) && v.OrderState == constants.OrderStateFinished {
				if val, ok := tempMap[v.DriverTarrif]; ok {
					val.ordersComplete++
					val.ordersSum = val.ordersSum + v.RealPrice
					if v.RealPrice < v.GuaranteedIncome {
						val.guranteeSum = val.guranteeSum + (v.GuaranteedIncome - v.RealPrice)
					}
					val.bonusesSum = val.bonusesSum + v.BonusPayment
				} else {
					dt := driverTariff{
						ordersComplete: 1,
						ordersSum:      v.RealPrice,
						bonusesSum:     v.BonusPayment,
					}
					if v.RealPrice < v.GuaranteedIncome {
						dt.guranteeSum = v.GuaranteedIncome - v.RealPrice
					}
					tempMap[v.DriverTarrif] = dt
				}
			}
		}

		// проверка на последний круг
		if stepTime == finishTime {
			break
		}

		//агрегируем заказы
		for k, v := range tempMap {

			tariff := DriverTariffAggr{
				TimestampMSK:    startTime.Add(3 * time.Hour),
				DriverTariff:    k,
				OrdersCompleted: v.ordersComplete,
				OrdersSum:       v.ordersSum,
				GuranteeSum:     v.guranteeSum,
				BonusesSum:      v.bonusesSum,
			}
			result = append(result, tariff)
		}

		//теперь надо проверить остался ли "хвост"
		if stepTime.Add(time.Minute * time.Duration(minuteStep)).After(finishTime) {
			startTime = stepTime
			stepTime = finishTime
		} else {
			startTime = stepTime
			stepTime = stepTime.Add(time.Minute * time.Duration(minuteStep))
		}

	}
	return result
}

func CancelAggregate(orders []BQOrderRaw, minuteStep int) []CancelOrdersAggr {
	var result []CancelOrdersAggr

	if len(orders) < 1 {
		return result
	}

	startTime, finishTime, stepTime := minMaxTime(&orders, minuteStep)

	for {
		type cancelAggr struct {
			count   int
			avgtime int64
		}
		tempMap := make(map[string]cancelAggr)

		//отбираем заказы из диапазона
		for _, v := range orders {
			if v.CreatedDatetime.After(startTime) && v.CreatedDatetime.Before(stepTime.Add(time.Millisecond)) && (v.OrderState == constants.OrderStateDriverNotFound || v.OrderState == constants.OrderStateCancelled) {
				mapKey := fmt.Sprintf("%s|%s|%s|%s", v.ServiceName, v.Source, v.OrderState, v.CancelReason)
				if val, ok := tempMap[mapKey]; ok {
					val.count++
					val.avgtime = val.avgtime + v.CancelTime.Unix() - v.CreatedDatetime.Unix()
					tempMap[mapKey] = val
				} else {
					avg := v.CancelTime.Unix() - v.CreatedDatetime.Unix()
					tm := cancelAggr{
						count:   1,
						avgtime: avg,
					}
					tempMap[mapKey] = tm
				}
			}
		}

		// проверка на последний круг
		if stepTime == finishTime {
			break
		}

		//агрегируем заказы
		for k, v := range tempMap {
			count := v.count
			avg := v.avgtime
			cncl := getCancelKeys(k, count, int(avg), startTime)
			result = append(result, cncl)
		}

		//теперь надо проверить остался ли "хвост"
		if stepTime.Add(time.Minute * time.Duration(minuteStep)).After(finishTime) {
			startTime = stepTime
			stepTime = finishTime
		} else {
			startTime = stepTime
			stepTime = stepTime.Add(time.Minute * time.Duration(minuteStep))
		}

	}
	return result
}

func getCancelKeys(key string, count, totaltime int, t time.Time) CancelOrdersAggr {
	parts := strings.Split(key, "|")
	if len(parts) != 4 {
		return CancelOrdersAggr{}
	}
	avgTime := int(totaltime / count)
	return CancelOrdersAggr{
		TimestampMSK: t.Add(3 * time.Hour),
		Service:      parts[0],
		Source:       parts[1],
		CancelType:   parts[2],
		Reason:       parts[3],
		Count:        count,
		AvgTime:      avgTime,
	}
}
