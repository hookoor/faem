package models

import (
	"time"

	"github.com/go-pg/pg/urlvalues"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
)

const (
	DefaultTransferSort         = "description"
	DefaultSeparateTransferSort = "tr_created_at"
	DefaultTransferLogSort      = "created_at"
)

// BQAccountRaw -
type BQAccountRaw struct {
	ID          string
	AccountType JSONField
	UserUUID    string
	UserType    JSONField
	Phone       string
	Meta        JSONField
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   time.Time
}

// BQBankTransferRaw -
type BQBankTransferRaw struct {
	ID            string
	UserUUID      string
	TransactionID string
	PaymentReq    string
	AcsURL        string
	Amount        float64
	SubjectUUID   string
	PrepaidAt     time.Time
	ConfirmedAt   time.Time
	CreatedAt     time.Time
	RefundedAt    time.Time
}

// BQEntryRaw -
type BQEntryRaw struct {
	ID        string
	EntryType JSONField
	AccountID string
	Amount    float64
	Balance   float64
	Force     bool
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

// BQTransactionRaw -
type BQTransactionRaw struct {
	ID        string
	CreditID  string
	DebitID   string
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}

// BQTransferRaw -
type BQTransferRaw struct {
	ID             string
	TransferType   JSONField
	Amount         float64
	TransactionIds string
	Description    string
	ExternalID     string
	Meta           JSONField
	CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      time.Time
}

// ConvertBillingAccountsToBQ -
func ConvertBillingAccountsToBQ(accounts []billingModels.Account) []BQAccountRaw {
	var res []BQAccountRaw
	if len(accounts) > 0 {
		for _, v := range accounts {
			res = append(res, BQAccountRaw{
				ID:          v.ID,
				AccountType: JSONField(toJSON(v.AccountType)),
				UserUUID:    v.UserUUID,
				UserType:    JSONField(toJSON(v.UserType)),
				Phone:       v.Phone,
				Meta:        JSONField(toJSON(v.Meta)),
				CreatedAt:   v.CreatedAt,
				UpdatedAt:   v.UpdatedAt,
				DeletedAt:   v.GetDeletedAt(),
			})
		}
	}
	return res
}

// ConvertBillingBankTransfersToBQ -
func ConvertBillingBankTransfersToBQ(bancTransfers []billingModels.BankTransfer) []BQBankTransferRaw {
	var res []BQBankTransferRaw
	if len(bancTransfers) > 0 {
		for _, v := range bancTransfers {
			res = append(res, BQBankTransferRaw{
				ID:            v.ID,
				UserUUID:      v.UserUUID,
				TransactionID: v.TransactionId,
				PaymentReq:    v.PaymentReq,
				AcsURL:        v.AcsUrl,
				Amount:        v.Amount,
				SubjectUUID:   v.SubjectUUID,
				PrepaidAt:     v.PrepaidAt,
				ConfirmedAt:   v.ConfirmedAt,
				CreatedAt:     v.CreatedAt,
				RefundedAt:    v.RefundedAt,
			})
		}
	}
	return res
}

// ConvertBillingEntriesToBQ -
func ConvertBillingEntriesToBQ(entries []billingModels.Entry) []BQEntryRaw {
	var res []BQEntryRaw
	if len(entries) > 0 {
		for _, v := range entries {
			res = append(res, BQEntryRaw{
				ID:        v.ID,
				EntryType: JSONField(toJSON(v.EntryType)),
				AccountID: v.AccountID,
				Amount:    v.Amount,
				Balance:   v.Balance,
				Force:     v.Force,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				DeletedAt: v.GetDeletedAt(),
			})
		}
	}
	return res
}

// ConvertBillingTransactionsToBQ -
func ConvertBillingTransactionsToBQ(transactions []billingModels.Transaction) []BQTransactionRaw {
	var res []BQTransactionRaw
	if len(transactions) > 0 {
		for _, v := range transactions {
			res = append(res, BQTransactionRaw{
				ID:        v.ID,
				CreditID:  v.CreditID,
				DebitID:   v.DebitID,
				CreatedAt: v.CreatedAt,
				UpdatedAt: v.UpdatedAt,
				DeletedAt: v.GetDeletedAt(),
			})
		}
	}
	return res
}

// ConvertBillingTransfersToBQ -
func ConvertBillingTransfersToBQ(transfers []billingModels.Transfer) []BQTransferRaw {
	var res []BQTransferRaw

	if len(transfers) > 0 {
		for _, v := range transfers {
			var ids string
			for k, v := range v.TransactionIds {
				if k == 0 {
					ids = v
				} else {
					ids = "," + v
				}
			}
			res = append(res, BQTransferRaw{
				ID:             v.ID,
				TransferType:   JSONField(toJSON(v.TransferType)),
				Amount:         v.Amount,
				TransactionIds: ids,
				Description:    v.Description,
				ExternalID:     v.ExternalID,
				Meta:           JSONField(toJSON(v.Meta)),
				CreatedAt:      v.CreatedAt,
				UpdatedAt:      v.UpdatedAt,
				DeletedAt:      v.GetDeletedAt(),
			})
		}
	}
	return res
}

type TransferFilter struct {
	Description  string
	PeriodStart  time.Time
	PeriodEnd    time.Time
	TransferType structures.BillingTransferType
	Sort         struct {
		Field string
		Dir   SortDir
	}
}

type (
	Transfer struct {
		tableName struct{} `pg:",discard_unknown_columns"`

		Description string  `json:"description"`
		Count       int     `json:"count"`
		Sum         float64 `json:"sum"`
	}

	TransferReport struct {
		Transfers  []*Transfer `json:"transfers"`
		TotalCount int         `json:"total_count"`
		TotalSum   float64     `json:"total_sum"`
	}
)

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type SeparateTransferFilter struct {
	OwnerName    string
	DriverAlias  string
	TransferType structures.BillingTransferType
	EntryType    billingModels.EntryType
	Description  string
	Phone        string
	UserUUID     string
	PeriodStart  time.Time
	PeriodEnd    time.Time
	Sort         struct {
		Field string
		Dir   SortDir
	}
	Pager urlvalues.Pager
}

type (
	SeparateTransfer struct {
		tableName struct{} `sql:"reports_v2" pg:",discard_unknown_columns"`

		ID           string                         `json:"id"`
		OwnerName    string                         `json:"owner_name"`
		DriverAlias  int                            `json:"driver_alias"`
		TransferType structures.BillingTransferType `json:"transfer_type"`
		EntryType    billingModels.EntryType        `json:"entry_type"`
		AccountType  structures.BillingAccountType  `json:"account_type"`
		Amount       float64                        `json:"amount"`
		Balance      float64                        `json:"balance"`
		Description  string                         `json:"description"`
		Phone        string                         `json:"phone"`
		// для оптимизации огромных JOIN'ов пришлось добавить во вью reports_v2 отдельно поля
		// transactions.created_at (tx_created_at) и transfers.created_at (tr_created_at) -- чтобы можно было оптимально фильтровать по дате
		TrCreatedAt time.Time `json:"-"`
		TxCreatedAt time.Time `json:"-"`
		Created     int64     `json:"created_at" sql:"-"`
	}

	SeparateTransferReport struct {
		Transfers  []*SeparateTransfer `json:"transfers"`
		TotalCount int                 `json:"total_count"`
	}
)

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type TransferLogFilter struct {
	Account     string
	PeriodStart time.Time
	PeriodEnd   time.Time
	Sort        struct {
		Field string
		Dir   SortDir
	}
	Pager urlvalues.Pager
}

type (
	TransferLog struct {
		tableName struct{} `sql:"logs" pg:",discard_unknown_columns"`

		ID        int                    `json:"id"`
		Data      map[string]interface{} `json:"data"`
		CreatedAt time.Time              `json:"-"`
		Created   int64                  `json:"created_at" sql:"-"`
	}

	TransferLogReport struct {
		Logs       []*TransferLog `json:"logs"`
		TotalCount int            `json:"total_count"`
	}
)

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type TariffFilter struct {
	PeriodStart time.Time
	PeriodEnd   time.Time
}

type (
	Tariff struct {
		tableName struct{} `sql:"transfers" pg:",discard_unknown_columns"`

		UUID           string  `json:"uuid"`
		Tariff         string  `json:"tariff"`
		TotalOrders    int     `json:"total_orders"`
		TotalIncome    float64 `json:"total_income"`
		AverageIncome  float64 `json:"average_income"`
		PurchaseCount  int     `json:"purchase_count"`
		TotalPenalty   float64 `json:"total_penalty"`
		PenaltyCount   int     `json:"penalty_count"`
		AveragePenalty float64 `json:"average_penalty"`
	}

	TariffReport struct {
		Tariffs []*Tariff `json:"tariffs"`
	}
)
