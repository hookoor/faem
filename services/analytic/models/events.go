package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"time"
)

//EventProto структру внутри сервиса
type Event struct {
	tableName struct{} `sql:"crm_events"  pg:",discard_unknown_columns"`
	structures.EventItem
	DriverCoordinates []structures.LocationData `json:"driver_coordinates,omitempty" sql:"driver_coordinates"`
}

type BQEvent struct {
	EventTime         time.Time `json:"event_time"`
	TicketUUID        string    `json:"ticket_uuid"`
	MessagesUUID      string    `json:"messages_uuid"`
	EventTimeUnix     int64     `json:"event_time_unix" `
	DriverUUID        string    `json:"driver_uuid" `
	OrderUUID         string    `json:"order_uuid" `
	ClientUUID        string    `json:"client_uuid" `
	OperatorUUID      string    `json:"operator_uuid"`
	Publisher         string    `json:"publisher"`
	Event             string    `json:"event_name"`
	EventTrans        string    `json:"event_title"`
	Reason            string    `json:"reason"`
	Value             string    `json:"value"`
	Payload           string    `json:"payload"`
	Comment           string    `json:"comment"`
	ValueTitle        string    `json:"value_title,omitempty"`
	DriverCoordinates string    `json:"driver_coordinates" `
}

//ConvertEventToBQ преобразование в струтуру для хранения
//func ConvertEventToBQ(event Event) BQEvent {
//	var bqe BQEvent
//	bqe.MessagesUUID = arrayToString(event.MessagesUUID)
//	bqe.DriverUUID = arrayToString(event.DriverUUID)
//	bqe.OrderUUID = arrayToString(event.OrderUUID)
//	bqe.ClientUUID = arrayToString(event.ClientUUID)
//	bqe.OperatorUUID = arrayToString(event.OperatorUUID)
//	bqe.Payload = toJSON(event.Payload)
//	if len(event.DriverCoordinates) > 0 {
//		bqe.DriverCoordinates = toJSON(event.DriverCoordinates[0])
//	}
//	bqe.TicketUUID = event.TicketUUID
//	bqe.EventTime = event.EventTime
//	bqe.TicketUUID = event.TicketUUID
//	bqe.EventTimeUnix = event.EventTimeUnix
//	bqe.Publisher = event.Publisher
//	bqe.Event = event.Event
//	bqe.EventTrans = event.EventTrans
//	bqe.Reason = event.Reason
//	bqe.Value = event.Value
//	bqe.Comment = event.Comment
//	bqe.ValueTitle = event.ValueTitle
//
//	return bqe
//}

type TelephonyEventData struct {
	EventTime    time.Time `json:"event_time"`
	Event        string    `json:"event"`
	Publisher    string    `json:"publisher"`
	Type         string    `json:"type"`
	IncomingLine string    `json:"incoming_line"`
	Operator     string    `json:"operator"`
	Caller       string    `json:"caller"`
	OrderUUID    string    `json:"order"`

	//insert datetime
	InsertDateTime time.Time `json:"insert_datetime"` // время вставки
}

type DriverEventData struct {
	EventTime  time.Time `json:"event_time"`
	DriverUUID string    `json:"driver_uuid"`
	State      string    `json:"state"`
	Publisher  string    `json:"publisher"`
	Comment    string    `json:"comment"`
	Lat        float32   `json:"lat"`
	Lon        float32   `json:"lon"`

	//insert datetime
	InsertDateTime time.Time `json:"insert_datetime"` // время вставки
}

type OrderEventData struct {
	EventTime    time.Time `json:"event_time"`
	DriverUUID   string    `json:"driver_uuid"`
	OrderUUID    string    `json:"order_uuid"`
	Publisher    string    `json:"publisher"`
	OperatorUUID string    `json:"operator_uuid"`
	State        string    `json:"state"`
	Comment      string    `json:"comment"`

	//insert datetime
	InsertDateTime time.Time `json:"insert_datetime"` // время вставки
}
