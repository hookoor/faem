package models

type SortDir string

const (
	SortDirAsc  SortDir = "asc"
	SortDirDesc SortDir = "desc"
)

func DefineSortDir(dir SortDir, defaultDir ...SortDir) SortDir {
	if dir != "" { // if a direction specified, then use the provided one
		return dir
	}
	sortDir := SortDirAsc // default sort direction
	if len(defaultDir) > 0 {
		sortDir = defaultDir[0]
	}
	return sortDir
}
