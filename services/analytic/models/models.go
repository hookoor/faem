package models

import (
	"encoding/json"
	"fmt"
	"strings"
)

// JSONField -
type JSONField string

// SavingDataType - тип сохраняемых данных
type SavingDataType string

const (
	EventsType SavingDataType = "events"
	OrdersType SavingDataType = "orders"

	BillingsAccountsType      SavingDataType = "billings_accounts"
	BillingsBankTransfersType SavingDataType = "billings_bank_transfers"
	BillingsEntriesType       SavingDataType = "billings_entries"
	BillingsTransactionsType  SavingDataType = "billings_transactions"
	BillingsTransfersType     SavingDataType = "billings_transfers"
	UnknownType               SavingDataType = "unknown"
)

func arrayToString(i interface{}) string {
	return strings.Trim(strings.Replace(fmt.Sprint(i), " ", ",", -1), "[]")
}

func toJSON(i interface{}) string {
	if i == nil {
		return ""
	}
	b, _ := json.Marshal(i)
	return string(b)
}
