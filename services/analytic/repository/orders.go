package repository

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/crm/orders"
)

func (p *Pg) GetOrders(ctx context.Context, filter *models.OrderFilter) ([]*models.Order, error) {
	var result []*models.Order
	q := p.CrmDB.Model((*orders.OrderCRM)(nil)).
		ColumnExpr("CASE WHEN source = ? THEN owner ->> 'name' ELSE source END AS source", models.OrderSourceCRM).
		ColumnExpr("CASE WHEN order_state = ? THEN 1 END AS finished", constants.OrderStateFinished).
		ColumnExpr(
			"CASE WHEN order_state IN (?) THEN 1 END AS cancelled",
			pg.InMulti(constants.OrderStateCancelled, constants.OrderStateDriverNotFound),
		)
	if filter.Source != "" {
		q = q.Where("source = ?", filter.Source)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("created_at <= ?", filter.PeriodEnd)
	}

	sort := filter.Sort.Field
	if sort == "" {
		sort = models.DefaultOrderSort
	}

	err := p.CrmDB.ModelContext(ctx).
		Column("q.source").
		ColumnExpr("COUNT(*) AS count").
		ColumnExpr("COUNT(q.finished) AS finished").
		ColumnExpr("COUNT(q.cancelled) AS cancelled").
		ColumnExpr("ROUND(100 * count(q.finished)::decimal / GREATEST(count(*), 1), 2)  as finished_percent").
		ColumnExpr("ROUND(100 * count(q.cancelled)::decimal / GREATEST(count(*), 1), 2) as cancelled_percent").
		TableExpr("(?) AS q", q).
		Group("q.source").
		Order(fmt.Sprintf("%s %s", sort, filter.Sort.Dir)).
		Select(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

// GetCRMOrderByUUID - getting order by UUID
func (p *Pg) GetCRMOrderByUUID(ctx context.Context, uuid string) (models.OrderFromCRM, error) {
	var order models.OrderFromCRM
	err := p.CrmDB.ModelContext(ctx, &order).
		Where("uuid = ?", uuid).
		Select()
	if err != nil {
		return order, errors.Wrap(err, "failed to get order from DB")
	}

	return order, nil
}

// GetCRMOrderByUUID - getting list of orders by period from crm
func (p *Pg) GetOrdersByPeriod(ctx context.Context, startTime, endTime time.Time, created bool) ([]models.OrderFromCRM, error) {
	var ordersFromCrm []models.OrderFromCRM
	var err error
	if created {
		err = p.CrmDB.ModelContext(ctx, &ordersFromCrm).
			Where("created_at > ? AND created_at < ?", startTime, endTime).Select()
	} else {
		err = p.CrmDB.ModelContext(ctx, &ordersFromCrm).
			Where("complete_time > ? AND complete_time < ?", startTime, endTime).
			Select()
	}
	if err != nil {
		return ordersFromCrm, errors.Wrap(err, "failed to get orders from DB")
	}
	return ordersFromCrm, nil
}

// GetFullOrdersByPeriod - getting list of full orders by period from crm
func (p *Pg) GetFullOrdersByPeriod(ctx context.Context, startTime time.Time, endTime time.Time, created bool) ([]models.OrderFromCRM, error) {
	var (
		ordersFromCrm []models.OrderFromCRM
		err           error
	)
	if created {
		err = p.CrmDB.ModelContext(ctx, &ordersFromCrm).
			Where("created_at BETWEEN ? AND ?", startTime, endTime).Select()
	} else {
		err = p.CrmDB.ModelContext(ctx, &ordersFromCrm).
			Where("complete_time BETWEEN ? AND ?", startTime, endTime).Select()
	}
	if err != nil {
		return []models.OrderFromCRM{}, errors.Wrap(err, "failed to get fullOrders list  from crm DB ")
	}
	return ordersFromCrm, nil
}
