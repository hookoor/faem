package repository

import (
	"context"
	"fmt"

	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
)

const (
	msgBuyTariff = "Покупка тарифа" // should correspond to CRM's const

	// Should correspond to driver's const
	msgPayAction      = "Оплата"
	msgPenaltyAction  = "Списание"
	msgCompleteAction = "Оплата за выполненный заказ"
)

// GetAccountsItems -
func (p *Pg) GetAccountsItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Account, error) {
	var accounts []billingModels.Account
	err := p.BillingDB.ModelContext(ctx, &accounts).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return accounts, errors.Wrap(err, "failed to get accounts from DB")
	}

	return accounts, nil
}

// GetBankTransfersItems -
func (p *Pg) GetBankTransfersItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.BankTransfer, error) {
	var bankTransfers []billingModels.BankTransfer
	err := p.BillingDB.ModelContext(ctx, &bankTransfers).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return bankTransfers, errors.Wrap(err, "failed to get bank transfers from DB")
	}

	return bankTransfers, nil
}

// GetEntriesItems -
func (p *Pg) GetEntriesItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Entry, error) {
	var entrires []billingModels.Entry
	err := p.BillingDB.ModelContext(ctx, &entrires).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return entrires, errors.Wrap(err, "failed to get entries from DB")
	}

	return entrires, nil
}

// GetTransactionsItems -
func (p *Pg) GetTransactionsItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Transaction, error) {
	var transactions []billingModels.Transaction
	err := p.BillingDB.ModelContext(ctx, &transactions).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return transactions, errors.Wrap(err, "failed to get transactions from DB")
	}

	return transactions, nil
}

// GetTransfersItems -
func (p *Pg) GetTransfersItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Transfer, error) {
	var transfers []billingModels.Transfer
	err := p.BillingDB.ModelContext(ctx, &transfers).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return transfers, errors.Wrap(err, "failed to get transfer from DB")
	}

	return transfers, nil
}

// GetBillingEventsByPeriod -
func (p *Pg) GetBillingEventsByPeriod(ctx context.Context, startTime, endTime time.Time) ([]interface{}, error) {
	var err error
	var data []interface{}

	accounts, err := p.GetAccountsItems(ctx, startTime, endTime)
	if err != nil {
		return data, errpath.Err(err)
	}
	for _, element := range accounts {
		data = append(data, element)
	}
	bankTransfers, err := p.GetBankTransfersItems(ctx, startTime, endTime)
	if err != nil {
		return data, errpath.Err(err)
	}
	for _, element := range bankTransfers {
		data = append(data, element)
	}
	entries, err := p.GetEntriesItems(ctx, startTime, endTime)
	if err != nil {
		return data, errpath.Err(err)
	}
	for _, element := range entries {
		data = append(data, element)
	}
	transactions, err := p.GetTransactionsItems(ctx, startTime, endTime)
	if err != nil {
		return data, errpath.Err(err)
	}
	for _, element := range transactions {
		data = append(data, element)
	}
	transfers, err := p.GetTransfersItems(ctx, startTime, endTime)
	if err != nil {
		return data, errpath.Err(err)
	}
	for _, element := range transfers {
		data = append(data, element)
	}

	return data, nil
}

func (p *Pg) GetTransfers(ctx context.Context, filter *models.TransferFilter) ([]*models.Transfer, error) {
	sort := filter.Sort.Field
	if sort == "" {
		sort = models.DefaultTransferSort
	}

	var result []*models.Transfer
	q := p.BillingDB.ModelContext(ctx, (*billingModels.Transfer)(nil)).
		Column("transfer.description").
		ColumnExpr("COUNT(*) AS count").
		ColumnExpr("SUM(e.amount) AS sum").
		Join("INNER JOIN transactions AS tx ON tx.id = transfer.transaction_ids[1]").
		Join(
			"INNER JOIN entries AS e ON tx.debit_id = e.id OR tx.credit_id = e.id AND e.entry_type = ?",
			billingModels.EntryTypeDebit,
		).
		Where("transfer.transfer_type = ?", filter.TransferType).
		Group("transfer.description").
		Order(fmt.Sprintf("%s %s", sort, filter.Sort.Dir))
	if filter.Description != "" {
		q = q.Where("transfer.description = ?", filter.Description)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("transfer.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("transfer.created_at <= ?", filter.PeriodEnd)
	}
	if err := q.Select(&result); err != nil {
		return nil, err
	}
	return result, nil
}

func (p *Pg) GetSeparateTransfers(ctx context.Context, filter *models.SeparateTransferFilter) (
	[]*models.SeparateTransfer, int, error,
) {
	sort := filter.Sort.Field
	if sort == "" {
		sort = models.DefaultSeparateTransferSort
	}

	var result []*models.SeparateTransfer
	q := p.BillingDB.ModelContext(ctx, (*models.SeparateTransfer)(nil)).
		Column("id",
			"transfer_type",
			"entry_type",
			"account_type",
			"amount",
			"balance",
			"description",
			"phone",
			"user_uuid",
			"tr_created_at").
		ColumnExpr("transfer_meta ->> 'owner_name' as owner_name").
		ColumnExpr("(account_meta ->> 'driver_alias')::integer as driver_alias").
		WhereInMulti("account_type IN (?)", structures.AccountTypeCard, structures.AccountTypeBonus).
		Where("user_type = ?", structures.UserTypeDriver).
		Order(fmt.Sprintf("%s %s", sort, filter.Sort.Dir)).
		Apply(filter.Pager.Pagination)
	if filter.OwnerName != "" {
		q.WhereIn("transfer_meta ->> 'owner_name' = ?", filter.OwnerName)
	}
	if filter.DriverAlias != "" {
		q.Where("account_meta ->> 'driver_alias' = ?", filter.DriverAlias)
	}
	if filter.TransferType != "" {
		q.Where("transfer_type = ?", filter.TransferType)
	}
	if filter.EntryType != "" {
		q.Where("entry_type = ?", filter.EntryType)
	}
	if filter.Description != "" {
		q.Where("description = ?", filter.Description)
	}
	if filter.Phone != "" {
		q.Where("phone = ?", filter.Phone)
	}
	if filter.UserUUID != "" {
		q.Where("user_uuid = ?", filter.UserUUID)
	}
	if filter.PeriodStart.IsZero() {
		// жесткое ограничение если с фронта не прислали дату, чтобы запрос не длился вечность
		// почему два поля created_at -- см. структуру models.SeparateTransfer
		q.Where("tx_created_at >= now() - interval'7 days'")
		q.Where("tr_created_at >= now() - interval'7 days'")
	} else {
		q.Where("tx_created_at >= ?", filter.PeriodStart)
		q.Where("tr_created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q.Where("tx_created_at <= ?", filter.PeriodEnd)
		q.Where("tr_created_at <= ?", filter.PeriodEnd)
	}
	count, err := q.SelectAndCount(&result)
	if err != nil {
		return nil, 0, err
	}

	// Transform time
	for _, t := range result {
		t.EntryType = billingModels.EntryType(t.EntryType.GetTranslatedName())
		t.TransferType = structures.BillingTransferType(t.TransferType.GetTranslatedNameFor(t.AccountType))
		t.Created = t.TrCreatedAt.Unix()
	}
	return result, count, nil
}

func (p *Pg) GetTransferLogs(ctx context.Context, filter *models.TransferLogFilter) (
	[]*models.TransferLog, int, error,
) {
	sort := filter.Sort.Field
	if sort == "" {
		sort = models.DefaultTransferLogSort
	}

	var result []*models.TransferLog
	q := p.BillingDB.ModelContext(ctx, &result).
		Order(fmt.Sprintf("%s %s", sort, filter.Sort.Dir)).
		Apply(filter.Pager.Pagination)
	if filter.Account != "" {
		q = q.Where("data ->> 'account' like ?", "%"+filter.Account+"%")
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("created_at <= ?", filter.PeriodEnd)
	}
	count, err := q.SelectAndCount(&result)
	if err != nil {
		return nil, 0, err
	}

	// Transform time
	for _, t := range result {
		t.Created = t.CreatedAt.Unix()
	}
	return result, count, nil
}

func (p *Pg) GetTariffs(ctx context.Context, filter *models.TariffFilter) ([]*models.Tariff, error) {
	var result []*models.Tariff
	subq := p.BillingDB.ModelContext(ctx, (*models.Tariff)(nil)).
		ColumnExpr("meta -> 'driver_tariff' ->> 'uuid' AS uuid").
		ColumnExpr("meta -> 'driver_tariff' ->> 'name' AS tariff").
		ColumnExpr("CASE WHEN description ILIKE ? THEN e.amount END AS amount", msgPayAction+"%").
		ColumnExpr("CASE WHEN description = ? THEN 1 END AS orders", msgCompleteAction).
		ColumnExpr("CASE WHEN description ILIKE ? THEN e.amount END AS penalty", msgPenaltyAction+"%").
		Join("INNER JOIN transactions AS tx ON tx.id = tariff.transaction_ids[1]").
		Join(
			"INNER JOIN entries AS e ON tx.debit_id = e.id OR tx.credit_id = e.id AND e.entry_type = ?",
			billingModels.EntryTypeDebit,
		).
		Where("tariff.transfer_type = ?", structures.TransferTypeWithdraw).
		Where("meta -> 'driver_tariff' ->> 'tariff_type' = ?", structures.DriverTariffPercent)
	if !filter.PeriodStart.IsZero() {
		subq = subq.Where("tariff.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		subq = subq.Where("tariff.created_at <= ?", filter.PeriodEnd)
	}
	q := p.BillingDB.ModelContext(ctx).
		Column("uuid", "tariff").
		ColumnExpr("COUNT(orders) AS total_orders").
		ColumnExpr("SUM(amount) AS total_income").
		ColumnExpr("SUM(amount) / GREATEST(COUNT(orders), 1) AS average_income").
		ColumnExpr("0 AS purchase_count").
		ColumnExpr("SUM(penalty) AS total_penalty").
		ColumnExpr("COUNT(penalty) AS penalty_count").
		ColumnExpr("SUM(penalty) / GREATEST(COUNT(penalty), 1) AS average_penalty").
		TableExpr("(?) AS q", subq).
		Group("uuid", "tariff").
		Order("tariff")
	if err := q.Select(&result); err != nil {
		return nil, err
	}

	// Union period tariffs
	var union []*models.Tariff
	subq = p.BillingDB.ModelContext(ctx, (*models.Tariff)(nil)).
		ColumnExpr("meta -> 'driver_tariff' ->> 'uuid' AS uuid").
		ColumnExpr("meta -> 'driver_tariff' ->> 'name' AS tariff").
		ColumnExpr("CASE WHEN transfer_type = ? THEN e.amount END AS amount", structures.TransferTypeWithdraw).
		ColumnExpr("CASE WHEN description ILIKE ? THEN 1 END AS purchase", msgBuyTariff+"%").
		ColumnExpr("CASE WHEN description ILIKE ? THEN e.amount END AS penalty", msgPenaltyAction+"%").
		Join("INNER JOIN transactions AS tx ON tx.id = tariff.transaction_ids[1]").
		Join(
			"INNER JOIN entries AS e ON tx.debit_id = e.id OR tx.credit_id = e.id AND e.entry_type = ?",
			billingModels.EntryTypeDebit,
		).
		WhereInMulti("tariff.transfer_type IN (?)", structures.TransferTypeWithdraw, structures.TransferTypePayment).
		Where("meta -> 'driver_tariff' ->> 'tariff_type' = ?", structures.DriverTariffPeriod)
	if !filter.PeriodStart.IsZero() {
		subq = subq.Where("tariff.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		subq = subq.Where("tariff.created_at <= ?", filter.PeriodEnd)
	}
	q = p.BillingDB.ModelContext(ctx).
		Column("uuid", "tariff").
		ColumnExpr("COUNT(*) - COUNT(amount) AS total_orders").
		ColumnExpr("SUM(amount) AS total_income").
		ColumnExpr("SUM(amount) / GREATEST((COUNT(*) - COUNT(amount)), 1) AS average_income").
		ColumnExpr("COUNT(purchase) AS purchase_count").
		ColumnExpr("SUM(penalty) AS total_penalty").
		ColumnExpr("COUNT(penalty) AS penalty_count").
		ColumnExpr("SUM(penalty) / GREATEST(COUNT(penalty), 1) AS average_penalty").
		TableExpr("(?) AS q", subq).
		Group("uuid", "tariff").
		Order("tariff")
	if err := q.Select(&union); err != nil {
		return nil, err
	}
	result = append(result, union...)
	return result, nil
}

// InsertAccountsItem -
func (bq *BigQuery) InsertAccountsItem(ctx context.Context, item billingModels.Account) error {
	bq.BufferOfBillingAccounts = append(bq.BufferOfBillingAccounts, item)
	if len(bq.BufferOfBillingAccounts) >= flushEventCount {
		return bq.FlushBillingAccountsEvents(ctx)
	}
	return nil
}

// InsertBankTransfersItem -
func (bq *BigQuery) InsertBankTransfersItem(ctx context.Context, item billingModels.BankTransfer) error {
	bq.BufferOfBillingBankTransfers = append(bq.BufferOfBillingBankTransfers, item)
	if len(bq.BufferOfBillingBankTransfers) >= flushEventCount {
		return bq.FlushBillingBankTransfersEvents(ctx)
	}
	return nil
}

// InsertEntriesItem -
func (bq *BigQuery) InsertEntriesItem(ctx context.Context, item billingModels.Entry) error {
	bq.BufferOfBillingEntries = append(bq.BufferOfBillingEntries, item)
	if len(bq.BufferOfBillingEntries) >= flushEventCount {
		return bq.FlushBillingEntriesEvents(ctx)
	}
	return nil
}

// InsertTransactionsItem -
func (bq *BigQuery) InsertTransactionsItem(ctx context.Context, item billingModels.Transaction) error {
	bq.BufferOfBillingTransactions = append(bq.BufferOfBillingTransactions, item)
	if len(bq.BufferOfBillingTransactions) >= flushEventCount {
		return bq.FlushBillingTransactionsEvents(ctx)
	}
	return nil
}

// InsertTransfersItem -
func (bq *BigQuery) InsertTransfersItem(ctx context.Context, item billingModels.Transfer) error {
	bq.BufferOfBillingTransfers = append(bq.BufferOfBillingTransfers, item)
	if len(bq.BufferOfBillingTransfers) >= flushEventCount {
		return bq.FlushBillingTransfersEvents(ctx)
	}
	return nil
}
