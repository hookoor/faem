package repository

import (
	"github.com/go-pg/pg"
)

type Pg struct {
	CrmDB     *pg.DB
	BillingDB *pg.DB
}
