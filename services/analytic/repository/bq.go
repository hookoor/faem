package repository

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
	"google.golang.org/api/iterator"

	"time"

	"cloud.google.com/go/bigquery"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/analytic/config"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
	"go.uber.org/multierr"
)

const (
	flushEventCount = 25
)

type BigQuery struct {
	BillingAccountsInserter      *bigquery.Inserter
	BillingBankTransfersInserter *bigquery.Inserter
	BillingEntriesInserter       *bigquery.Inserter
	BillingTransactionsInserter  *bigquery.Inserter
	BillingTransfersInserter     *bigquery.Inserter
	BufferOfBillingAccounts      []billingModels.Account
	BufferOfBillingBankTransfers []billingModels.BankTransfer
	BufferOfBillingEntries       []billingModels.Entry
	BufferOfBillingTransactions  []billingModels.Transaction
	BufferOfBillingTransfers     []billingModels.Transfer
	OrdersEventInserter          *bigquery.Inserter
	FullOrdersEventInserter      *bigquery.Inserter
	DriversEventInserter         *bigquery.Inserter
	TelephonyEventInserter       *bigquery.Inserter
	OrderInserter                *bigquery.Inserter
	FullOrderInserter            *bigquery.Inserter
	DriverInserter               *bigquery.Inserter
	Client                       *bigquery.Client
	OrderEvents                  []models.OrderEventData
	DriverEvents                 []models.DriverEventData
	TelephonyEvents              []models.TelephonyEventData

	OrdersTable     string
	FullOrdersTable string
	DriversTable    string

	BillingEventsTables []string

	OrdersEventsTable  string
	DriversEventsTable string
}

//InsertOrdersEvent инсертим событие в BigQuery
func (bq *BigQuery) InsertOrdersEvent(ctx context.Context, event models.OrderEventData) error {
	bq.OrderEvents = append(bq.OrderEvents, event)
	if len(bq.OrderEvents) >= flushEventCount {
		return bq.FlushOrdersEvent(ctx)
	}
	return nil
}

//InsertDriversEvent инсертим событие в BigQuery
func (bq *BigQuery) InsertDriversEvent(ctx context.Context, event models.DriverEventData) error {
	bq.DriverEvents = append(bq.DriverEvents, event)
	if len(bq.DriverEvents) >= flushEventCount {
		return bq.FlushDriversEvent(ctx)
	}
	return nil
}

//InsertTelephonyEvent инсертим событие в BigQuery
func (bq *BigQuery) InsertTelephonyEvent(ctx context.Context, event models.TelephonyEventData) error {
	bq.TelephonyEvents = append(bq.TelephonyEvents, event)
	if len(bq.TelephonyEvents) >= flushEventCount {
		return bq.FlushTelephonyEvents(ctx)
	}
	return nil
}

func (bq *BigQuery) FlushOrdersEvent(ctx context.Context) error {
	err := bq.OrdersEventInserter.Put(ctx, bq.OrderEvents)
	if err != nil {
		return errors.Wrap(err, "failed to put orders event to BQ")
	}
	bq.OrderEvents = nil
	return nil
}

func (bq *BigQuery) FlushDriversEvent(ctx context.Context) error {
	insertTime := time.Now().UTC()
	events := bq.DriverEvents
	for k := range events {
		events[k].InsertDateTime = insertTime
	}
	err := bq.DriversEventInserter.Put(ctx, events)
	if err != nil {
		return errors.Wrap(err, "failed to put driver event to BQ")
	}
	bq.DriverEvents = nil
	return nil
}

func (bq *BigQuery) FlushTelephonyEvents(ctx context.Context) error {
	err := bq.TelephonyEventInserter.Put(ctx, bq.TelephonyEvents)
	if err != nil {
		return errors.Wrap(err, "failed to put telephony event to BQ")
	}
	bq.TelephonyEvents = nil
	return nil
}

// Flush billing events: ---

// FlushBillingAccountsEvents -
func (bq *BigQuery) FlushBillingAccountsEvents(ctx context.Context) error {
	data := models.ConvertBillingAccountsToBQ(bq.BufferOfBillingAccounts)
	if len(data) == 0 {
		return nil
	}
	err := bq.BillingAccountsInserter.Put(ctx, data)
	if err != nil {
		return errpath.Err(err, "failed to put BillingAccounts event to BQ")
	}
	bq.BufferOfBillingAccounts = nil
	return nil
}

// FlushBillingBankTransfersEvents -
func (bq *BigQuery) FlushBillingBankTransfersEvents(ctx context.Context) error {
	data := models.ConvertBillingBankTransfersToBQ(bq.BufferOfBillingBankTransfers)
	err := bq.BillingBankTransfersInserter.Put(ctx, data)
	if err != nil {
		return errpath.Err(err, "failed to put BillingBankTransfers event to BQ")
	}
	bq.BufferOfBillingBankTransfers = nil
	return nil
}

// FlushBillingEntriesEvents -
func (bq *BigQuery) FlushBillingEntriesEvents(ctx context.Context) error {
	data := models.ConvertBillingEntriesToBQ(bq.BufferOfBillingEntries)
	err := bq.BillingEntriesInserter.Put(ctx, data)
	if err != nil {
		return errpath.Err(err, "failed to put BillingEntries event to BQ")
	}
	bq.BufferOfBillingEntries = nil
	return nil
}

// FlushBillingTransactionsEvents -
func (bq *BigQuery) FlushBillingTransactionsEvents(ctx context.Context) error {
	data := models.ConvertBillingTransactionsToBQ(bq.BufferOfBillingTransactions)
	err := bq.BillingTransactionsInserter.Put(ctx, data)
	if err != nil {
		return errpath.Err(err, "failed to put BillingTransactions event to BQ")
	}
	bq.BufferOfBillingTransactions = nil
	return nil
}

// FlushBillingTransfersEvents -
func (bq *BigQuery) FlushBillingTransfersEvents(ctx context.Context) error {
	data := models.ConvertBillingTransfersToBQ(bq.BufferOfBillingTransfers)
	err := bq.BillingTransfersInserter.Put(ctx, data)
	if err != nil {
		return errpath.Err(err, "failed to put BillingTransfers event to BQ")
	}
	bq.BufferOfBillingTransfers = nil
	return nil
}

func (bq *BigQuery) Close() error {
	ctx := context.Background()
	return multierr.Combine(
		errors.Wrap(bq.FlushOrdersEvent(ctx), "error while flushing BQ telephony event buffer"),
		errors.Wrap(bq.FlushDriversEvent(ctx), "error while flushing BQ driver event buffer"),
		errors.Wrap(bq.FlushOrdersEvent(ctx), "error while flushing BQ order events buffer"),
		errors.Wrap(bq.Client.Close(), "error while closing cloud client"),
	)
}

//GetLastOrderTime возвращает время последней записи в BigQuery
func (bq *BigQuery) GetLastOrderTime(ctx context.Context) (time.Time, error) {
	year, month, day := time.Now().Add(24 * time.Hour).Date()
	//day++
	dt := fmt.Sprintf("%d-%d-%d", year, int(month), day)
	qstr := "SELECT CancelTime FROM `" + bq.OrdersTable + "` WHERE DATE(CreatedDatetime) < '" + dt + "' ORDER BY CancelTime DESC LIMIT 1"
	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return time.Time{}, err
	}

	type resulter struct {
		Tm time.Time `bigquery:"CancelTime"`
	}

	var result resulter
	err = row.Next(&result)
	if err != nil {
		return time.Time{}, err
	}

	return result.Tm.Add(1 * time.Millisecond), nil
}
func (bq *BigQuery) GetLastOrderTimeFullOrders(ctx context.Context) (time.Time, error) {
	year, month, day := time.Now().Add(24 * time.Hour).Date()
	//day++
	dt := fmt.Sprintf("%d-%d-%d", year, int(month), day)
	qstr := "SELECT CancelTime FROM `" + bq.FullOrdersTable + "` WHERE DATE(CreatedAt) < '" + dt + "' ORDER BY CancelTime DESC LIMIT 1"
	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return time.Time{}, err
	}

	type resulter struct {
		Tm time.Time `bigquery:"CancelTime"`
	}

	var result resulter
	err = row.Next(&result)
	if err != nil {
		return time.Time{}, err
	}

	return result.Tm.Add(1 * time.Millisecond), nil
}

// GetLastBillingEventTime - возврящает время последней записи из таблиц по билинговым ивентам
func (bq *BigQuery) GetLastBillingEventTime(ctx context.Context) (time.Time, error) {
	var result time.Time
	year, month, day := time.Now().Add(24 * time.Hour).Date()
	//day++
	dt := fmt.Sprintf("%d-%d-%d", year, int(month), day)

	for _, tbl := range bq.BillingEventsTables {
		qstr := "SELECT CreatedAt FROM `" + tbl + "` WHERE DATE(CreatedAt) < '" + dt + "' ORDER BY CreatedAt DESC LIMIT 1"
		query := bq.Client.Query(qstr)
		row, err := query.Read(ctx)
		if err != nil {
			return time.Time{}, err
		}

		var rowBind struct {
			Tm time.Time `bigquery:"CreatedAt"`
		}

		err = row.Next(&rowBind)
		if err != nil {
			continue // дабы пройтись по всем таблица
		}

		if result.Unix() < rowBind.Tm.Add(1*time.Millisecond).Unix() {
			result = rowBind.Tm.Add(1 * time.Millisecond)
		}
	}
	return result, nil
}

func (bq *BigQuery) InsertOrders(ctx context.Context, orders *[]models.OrderFromCRM) error {
	//convertedOrder := models.ConvertOrderToBQ(order)
	convertedOrders := models.ConvertToBQ(orders)
	//sourceAggr := models.OrderSourceAggregate(convertedOrder, 2)
	//operatorAggr := models.OperatorDataAggregate(convertedOrder, 2)
	//clientAggr := models.ClientTariffAggregate(convertedOrder, 5)
	//driverTariffAggr := models.DriverTariffAggregate(convertedOrder, 5)
	//cancelAggr := models.CancelAggregate(convertedOrder, 5)

	err := bq.OrderInserter.Put(ctx, convertedOrders)
	resErr := err
	if err != nil {
		resErr = errors.New("")
		if multiError, ok := err.(bigquery.PutMultiError); ok {
			for _, err1 := range multiError {
				for _, err2 := range err1.Errors {
					resErr = errors.Wrap(resErr, err2.Error())
				}
			}
		} else {
			resErr = errors.Wrap(resErr, err.Error())
		}
	}
	return multierr.Combine(
		errors.Wrap(resErr, "failed to stream order data to DataLake"),
		//errors.Wrap(bq.OrderSourceAggrInserter.Put(ctx, sourceAggr), "failed to stream source agregate data"),
		//errors.Wrap(bq.OperatorAggrInserter.Put(ctx, operatorAggr), "failed to stream operators agregate data"),
		//errors.Wrap(bq.ClientTariffAggrInserter.Put(ctx, clientAggr), "failed to stream client tariff agregate data"),
		//errors.Wrap(bq.DriverTariffAggrInserter.Put(ctx, driverTariffAggr), "failed to stream driver tariff agregate data"),
		//errors.Wrap(bq.CancelAggrInserter.Put(ctx, cancelAggr), "failed to stream cancel aggregate data"),
	)
}

func (bq *BigQuery) InsertFullOrders(ctx context.Context, orders *[]models.OrderFromCRM) error {
	err := bq.FullOrderInserter.Put(ctx, models.ConvertToBQFullOrders(orders))
	resErr := err
	if err != nil {
		resErr = errors.New("")
		if multiError, ok := err.(bigquery.PutMultiError); ok {
			for _, err1 := range multiError {
				for _, err2 := range err1.Errors {
					resErr = errors.Wrap(resErr, err2.Error())
				}
			}
		} else {
			resErr = errors.Wrap(resErr, err.Error())
		}
	}
	return multierr.Combine(
		errors.Wrap(resErr, "failed to stream order data to DataLake"),
	)
}

func (bq *BigQuery) InsertDrivers(ctx context.Context, drivers *[]crmModels.DriverCRM) error {
	var convertedDrivers []models.BQDriver
	for _, v := range *drivers {
		convertedDrivers = append(convertedDrivers, models.DriverToBQDriver(v))
	}

	return multierr.Combine(
		errors.Wrap(bq.DriverInserter.Put(ctx, convertedDrivers), "failed to stream drivers data to DataLake"),
	)
}

// BigQueryInit инициализации клиента BigQuery
func BigQueryInit(ctx context.Context, cfg config.BigQuery) (BigQuery, error) {
	var bq BigQuery
	client, err := bigquery.NewClient(ctx, cfg.ProjectID)
	if err != nil {
		return bq, errors.Wrap(err, "Cant get Client")
	}

	bq.Client = client

	//Статусы заказа
	err = bq.createTableIfNotExists(ctx, cfg.EventsDataset, cfg.OrdersEventsTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.OrdersEventInserter = bq.Client.Dataset(cfg.EventsDataset).Table(cfg.OrdersEventsTable).Inserter()

	//Статусы водителя
	err = bq.createTableIfNotExists(ctx, cfg.EventsDataset, cfg.DriversEventsTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.DriversEventInserter = bq.Client.Dataset(cfg.EventsDataset).Table(cfg.DriversEventsTable).Inserter()

	//События телефония
	err = bq.createTableIfNotExists(ctx, cfg.EventsDataset, cfg.TelephonyEventsTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.TelephonyEventInserter = bq.Client.Dataset(cfg.EventsDataset).Table(cfg.TelephonyEventsTable).Inserter()

	//Заказы
	err = bq.createTableIfNotExists(ctx, cfg.OrdersDataset, cfg.OrdersTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.OrderInserter = bq.Client.Dataset(cfg.OrdersDataset).Table(cfg.OrdersTable).Inserter()

	//Заказы полные
	err = bq.createTableIfNotExists(ctx, cfg.OrdersDataset, cfg.FullOrdersTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.FullOrderInserter = bq.Client.Dataset(cfg.OrdersDataset).Table(cfg.FullOrdersTable).Inserter()

	//Водители
	err = bq.createTableIfNotExists(ctx, cfg.DriversDataset, cfg.DriversTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	}
	bq.DriverInserter = bq.Client.Dataset(cfg.DriversDataset).Table(cfg.DriversTable).Inserter()

	bq.OrdersTable = fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.OrdersDataset, cfg.OrdersTable)
	bq.FullOrdersTable = fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.OrdersDataset, cfg.FullOrdersTable)
	bq.OrdersEventsTable = fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.EventsDataset, cfg.OrdersEventsTable)
	bq.DriversTable = fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.DriversDataset, cfg.DriversTable)
	bq.DriversEventsTable = fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.DriversDataset, cfg.DriversEventsTable)

	bq.BillingEventsTables = append(bq.BillingEventsTables,
		fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.BillingEventsDataset, cfg.BillingAccountTable),
		fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.BillingEventsDataset, cfg.BillingBankTransferTable),
		fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.BillingEventsDataset, cfg.BillingEntrieTable),
		fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.BillingEventsDataset, cfg.BillingTransactionTable),
		fmt.Sprintf("%s.%s.%s", cfg.ProjectID, cfg.BillingEventsDataset, cfg.BillingTransferTable),
	)

	////Агрегированные данные по заказам в срезе статуса завершения, услуги, источника и оунера
	//err = bq.createTableIfNotExists(ctx, cfg.OrdersAggrDataset, cfg.OrdersSourceAggr, cfg)
	//if err != nil {
	//	return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	//}
	//bq.OrderSourceAggrInserter = client.Dataset(cfg.OrdersAggrDataset).Table(cfg.OrdersSourceAggr).Inserter()

	////Агрегированные данные по операторам
	//err = bq.createTableIfNotExists(ctx, cfg.OrdersAggrDataset, cfg.OperatorsDataAggr, cfg)
	//if err != nil {
	//	return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	//}
	//bq.OperatorAggrInserter = client.Dataset(cfg.OrdersAggrDataset).Table(cfg.OperatorsDataAggr).Inserter()

	////Агрегированные данные по клиентским тарифам
	//err = bq.createTableIfNotExists(ctx, cfg.OrdersAggrDataset, cfg.ClientTariffDataAggr, cfg)
	//if err != nil {
	//	return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	//}
	//bq.ClientTariffAggrInserter = client.Dataset(cfg.OrdersAggrDataset).Table(cfg.ClientTariffDataAggr).Inserter()

	////Агрегированные данные по водительским тарифам
	//err = bq.createTableIfNotExists(ctx, cfg.OrdersAggrDataset, cfg.DriverTariffDataAggr, cfg)
	//if err != nil {
	//	return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	//}
	//bq.DriverTariffAggrInserter = client.Dataset(cfg.OrdersAggrDataset).Table(cfg.DriverTariffDataAggr).Inserter()

	////Агрегированные данные по отмененным заказам
	//err = bq.createTableIfNotExists(ctx, cfg.OrdersAggrDataset, cfg.CancelDataAggr, cfg)
	//if err != nil {
	//	return bq, errors.Wrap(err, "failed to check or create BigQuery table")
	//}
	//bq.CancelAggrInserter = client.Dataset(cfg.OrdersAggrDataset).Table(cfg.CancelDataAggr).Inserter()

	// Billing events tables: ---
	err = bq.createTableIfNotExists(ctx, cfg.BillingEventsDataset, cfg.BillingAccountTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table BillingAccount")
	}
	bq.BillingAccountsInserter = bq.Client.Dataset(cfg.BillingEventsDataset).Table(cfg.BillingAccountTable).Inserter()
	err = bq.createTableIfNotExists(ctx, cfg.BillingEventsDataset, cfg.BillingBankTransferTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table BillingBankTransfer")
	}
	bq.BillingBankTransfersInserter = bq.Client.Dataset(cfg.BillingEventsDataset).Table(cfg.BillingBankTransferTable).Inserter()
	err = bq.createTableIfNotExists(ctx, cfg.BillingEventsDataset, cfg.BillingEntrieTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table BillingEntrie")
	}
	bq.BillingEntriesInserter = bq.Client.Dataset(cfg.BillingEventsDataset).Table(cfg.BillingEntrieTable).Inserter()
	err = bq.createTableIfNotExists(ctx, cfg.BillingEventsDataset, cfg.BillingTransactionTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table BillingTransaction")
	}
	bq.BillingTransactionsInserter = bq.Client.Dataset(cfg.BillingEventsDataset).Table(cfg.BillingTransactionTable).Inserter()
	err = bq.createTableIfNotExists(ctx, cfg.BillingEventsDataset, cfg.BillingTransferTable, cfg)
	if err != nil {
		return bq, errors.Wrap(err, "failed to check or create BigQuery table BillingTransaction")
	}
	bq.BillingTransfersInserter = bq.Client.Dataset(cfg.BillingEventsDataset).Table(cfg.BillingTransferTable).Inserter()

	return bq, nil
}

func (bq *BigQuery) createTableIfNotExists(ctx context.Context, dataset, table string, cfg config.BigQuery) error {
	log := logs.LoggerForContext(ctx).
		WithField("event", "checking BQ tables exists")
	md, _ := bq.Client.Dataset(dataset).Table(table).Metadata(ctx)
	if md != nil {
		log.WithFields(logrus.Fields{
			"table":   table,
			"dataset": dataset,
		}).Info("Table exists")
		return nil
	}
	log.WithFields(logrus.Fields{
		"table":   table,
		"dataset": dataset,
	}).Warn("Not found")

	//сопоставляем таблицы в BQ и структуры для их создания
	var err error
	var bqMeta bigquery.TableMetadata

	bqMeta.RequirePartitionFilter = true
	switch table {
	case cfg.OrdersEventsTable:
		bqMeta.Name = table
		bqMeta.Description = "orders events data"
		bqMeta.Schema, err = bigquery.InferSchema(models.OrderEventData{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "EventTime",
		}
	case cfg.DriversEventsTable:
		bqMeta.Name = table
		bqMeta.Description = "driver events data"
		bqMeta.Schema, err = bigquery.InferSchema(models.DriverEventData{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "EventTime",
		}
	case cfg.TelephonyEventsTable:
		bqMeta.Name = table
		bqMeta.Description = "telephony events data"
		bqMeta.Schema, err = bigquery.InferSchema(models.TelephonyEventData{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "EventTime",
		}
	case cfg.OrdersTable:
		bqMeta.Name = table
		bqMeta.Description = "orders table"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQOrderRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedDatetime",
		}
	case cfg.FullOrdersTable:
		bqMeta.Name = table
		bqMeta.Description = "full orders table"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQFullOrder{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}
	case cfg.DriversTable:
		bqMeta.Name = table
		bqMeta.Description = "drivers table"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQDriver{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
	case cfg.OrdersSourceAggr:
		bqMeta.Name = table
		bqMeta.Description = "orders source service and owner aggregation table"
		bqMeta.Schema, err = bigquery.InferSchema(models.OrdersSourceAggr{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "TimestampMSK",
		}
	case cfg.OperatorsDataAggr:
		bqMeta.Name = table
		bqMeta.Description = "operators aggregation data"
		bqMeta.Schema, err = bigquery.InferSchema(models.OperatorDataAggr{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "TimestampMSK",
		}
	case cfg.ClientTariffDataAggr:
		bqMeta.Name = table
		bqMeta.Description = "client tariffs aggregation data"
		bqMeta.Schema, err = bigquery.InferSchema(models.ClientTariffAggr{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "TimestampMSK",
		}
	case cfg.DriverTariffDataAggr:
		bqMeta.Name = table
		bqMeta.Description = "driver tariffs aggregation data"
		bqMeta.Schema, err = bigquery.InferSchema(models.DriverTariffAggr{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "TimestampMSK",
		}

	case cfg.CancelDataAggr:
		bqMeta.Name = table
		bqMeta.Description = "cancel aggregation data"
		bqMeta.Schema, err = bigquery.InferSchema(models.CancelOrdersAggr{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "TimestampMSK",
		}

	case cfg.BillingAccountTable:
		bqMeta.Name = table
		bqMeta.Description = "billing Account data"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQAccountRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}
	case cfg.BillingBankTransferTable:
		bqMeta.Name = table
		bqMeta.Description = "billing BankTransfer data"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQBankTransferRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}
	case cfg.BillingEntrieTable:
		bqMeta.Name = table
		bqMeta.Description = "billing Entry data"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQEntryRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}
	case cfg.BillingTransactionTable:
		bqMeta.Name = table
		bqMeta.Description = "billing Transaction data"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQTransactionRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}
	case cfg.BillingTransferTable:
		bqMeta.Name = table
		bqMeta.Description = "billing Transfer data"
		bqMeta.Schema, err = bigquery.InferSchema(models.BQTransferRaw{})
		if err != nil {
			return errors.Wrap(err, "Error creating BQ schema")
		}
		bqMeta.TimePartitioning = &bigquery.TimePartitioning{
			Field: "CreatedAt",
		}

	default:
		errText := fmt.Sprintf("BigQuery table not exists and cant find table %s assosiated struct.", table)
		return errors.New(errText)
	}
	err = bq.Client.Dataset(dataset).Table(table).Create(ctx, &bqMeta)
	if err != nil {
		return errors.Wrapf(err, "Cant create BQ table %s", table)
	}

	log.WithFields(logrus.Fields{
		"table":   table,
		"dataset": dataset,
	}).Info("Table created")

	return nil
}

func (bq *BigQuery) GetOrderByUUID(ctx context.Context, uuid string) (*models.BQOrderRaw, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting order by uuid from bigquery",
	})

	qstr := "SELECT UUID, CreatedDatetime FROM `" + bq.OrdersTable + "` WHERE (DATE(CreatedDatetime) > '2020-01-07') AND (UUID = '" + uuid + "')"
	log.WithFields(logrus.Fields{
		"query": qstr,
	}).Trace()

	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		log.Error(err)
		return nil, err
	}

	var order models.BQOrderRaw

	if row.TotalRows > 0 {
		err = row.Next(&order)
		if err != nil {
			log.Errorf("models.BQOrderRaw binding 1 error: %s", err)
			return nil, err
		}

		qstr = "SELECT o.*, d.Name, d.Alias, d.Phone, d.Comment " +
			"FROM `" + bq.OrdersTable + "` AS o " +
			"LEFT JOIN `" + bq.DriversTable + "` AS d ON o.DriverUUID = d.UUID " +
			"WHERE (DATE(CreatedDatetime) = '" + timeToStr(&order.CreatedDatetime) + "') AND (o.UUID = '" + uuid + "')"
		log.WithFields(logrus.Fields{
			"query": qstr,
		}).Trace()

		query := bq.Client.Query(qstr)
		row, err := query.Read(ctx)
		if err != nil {
			log.Error(err)
			return nil, err
		}

		if row.TotalRows > 0 {
			err = row.Next(&order)
			if err != nil {
				log.Errorf("models.BQOrderRaw binding 2 error: %s", err)
				return nil, err
			}
		}
	}

	orderEvents, err := bq.GetOrderEvents(ctx, uuid, &order.CreatedDatetime)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get order events by order_uuid")
	}
	order.Events = *orderEvents

	return &order, nil
}

func (bq *BigQuery) GetFilteredOrders(ctx context.Context, filter *proto.BQOrderFilter) (*proto.BQOrderRawWithCount, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting filtered orders from bigquery",
	})

	qstr := "FROM `" + bq.OrdersTable + "` " +
		"WHERE '" + timeToStr(&filter.StartDate) + "' <= DATE(CreatedDatetime) AND DATE(CreatedDatetime) <= '" + timeToStr(&filter.EndDate) + "'"

	if filter.ID != 0 {
		qstr += " AND ID = " + strconv.Itoa(filter.ID) + ""
	}

	if filter.ClientPhone != "" {
		qstr += " AND ClientPhone LIKE '%" + strings.ReplaceAll(filter.ClientPhone, "+", "") + "%'"
	}

	if filter.Operator != "" {
		qstr += " AND UserStartUUID = '" + filter.Operator + "'"
	}

	if filter.TaxiPool != "" {
		qstr += " AND OwnerUUID = '" + filter.TaxiPool + "'"
	}

	if filter.Service != "" {
		qstr += " AND ServiceName = '" + filter.Service + "'"
	}

	if filter.Source != "" {
		qstr += " AND Source = '" + filter.Source + "'"
	}

	if filter.Options != "" {
		qstr += " AND Features = '" + filter.Options + "'"
	}

	if filter.From != "" {
		qstr += " AND LOWER(PickupAddress) LIKE LOWER('%" + filter.From + "%')"
	}

	if filter.To != "" {
		qstr += " AND LOWER(DropoffAddress) LIKE LOWER('%" + filter.To + "%')"
	}

	if filter.DriverAlias != 0 {
		qstr += " AND DriverAlias = " + strconv.Itoa(filter.DriverAlias) + ""
	}

	if filter.DriverName != "" {
		qstr += " AND LOWER(DriverName) LIKE LOWER('%" + filter.DriverName + "%')"
	}

	if filter.DriverCar != "" {
		qstr += " AND DriverCar = '" + filter.DriverCar + "'"
	}

	if filter.DriverCarColor != "" {
		qstr += " AND DriverCarColor = '" + filter.DriverCarColor + "'"
	}

	if filter.DriverCarRegNumber != "" {
		qstr += " AND DriverCarRegNumber = '" + filter.DriverCarRegNumber + "'"
	}

	if filter.DriverTarrif != "" {
		qstr += " AND DriverTarrif = '" + filter.DriverTarrif + "'"
	}

	// Подсчёт кол-ва заказов для пагинатора
	countQuery, countIface := "SELECT COUNT(uuid) "+qstr, make([]bigquery.Value, 0)
	rowCount, err := bq.Client.Query(countQuery).Read(ctx)
	if err != nil {
		log.WithField("query", countQuery).Errorf("error executing query: %s", err)
		return nil, err
	}
	if err := rowCount.Next(&countIface); err != nil {
		log.WithField("query", countQuery).Errorf("error executing query: %s", err)
		return nil, err
	}
	count := int(countIface[0].(int64))

	qstr = fmt.Sprintf("SELECT * %s ORDER BY CreatedDatetime DESC LIMIT %d OFFSET %d", qstr, filter.Count, (filter.Page-1)*filter.Count)
	log.WithField("query", qstr).Trace()

	query := bq.Client.Query(qstr)
	rows, err := query.Read(ctx)
	if err != nil {
		log.WithField("query", qstr).Errorf("error executing query: %s", err)
		return nil, err
	}

	var orders []models.BQOrderRaw
	if rows.TotalRows > 0 {
		var t models.BQOrderRaw
		for {
			err := rows.Next(&t)
			if err == iterator.Done {
				break
			}
			if err != nil {
				log.Errorf("models.BQOrderRaw binding error: %s", err)
				continue
			}
			orders = append(orders, t)
		}
	}

	result := &proto.BQOrderRawWithCount{
		Orders: orders,
		Count:  count,
	}

	return result, nil
}

func (bq *BigQuery) GetOrderEvents(ctx context.Context, uuid string, tt *time.Time) (*[]models.OrderEventData, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting order events by order_uuid from bigquery",
	})

	dt := timeToStr(tt)
	qstr := "SELECT * FROM `" + bq.OrdersEventsTable + "` WHERE DATE(EventTime) = '" + dt + "' AND OrderUUID = '" + uuid + "'"
	log.WithFields(logrus.Fields{
		"query": qstr,
	}).Trace()

	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return nil, err
	}

	var orderEvents []models.OrderEventData
	var t models.OrderEventData
	for {
		err := row.Next(&t)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Errorf("models.OrderEventData binding error: %s", err)
		}
		orderEvents = append(orderEvents, t)
	}

	return &orderEvents, nil
}

func timeToStr(tt *time.Time) string {
	var year, day int
	var month time.Month
	if tt == nil {
		year, month, day = time.Now().Date()
	} else {
		year, month, day = tt.Date()
	}
	return fmt.Sprintf("%d-%02d-%02d", year, int(month), day)
}

func (bq *BigQuery) GetDriver(ctx context.Context, uuid string) (*models.BQDriver, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting driver by uuid from bigquery",
	})

	println("--- ", bq.DriversTable)
	qstr := "SELECT * FROM `" + bq.DriversTable + "` WHERE UUID = '" + uuid + "'"
	log.WithFields(logrus.Fields{
		"query": qstr,
	}).Trace()

	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return nil, err
	}

	var driver models.BQDriver
	if row.TotalRows > 0 {
		err = row.Next(&driver)
		if err != nil {
			return nil, err
		}
	}

	return &driver, nil
}

func (bq *BigQuery) GetDriverEvents(ctx context.Context, uuid string, tt *time.Time) (*[]models.DriverEventData, error) {
	log := logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"event": "getting driver events by driver_uuid from bigquery",
	})

	if tt == nil {
		temp := time.Date(2020, time.January, 1, 1, 1, 1, 1, time.Local)
		tt = &temp
	}

	qstr := "SELECT * FROM `" + bq.DriversEventsTable + "` WHERE DATE(EventTime) = '" + timeToStr(tt) + "' AND DriverUUID = '" + uuid + "'"
	log.WithFields(logrus.Fields{
		"query": qstr,
	}).Trace()

	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return nil, err
	}

	var driverEvents []models.DriverEventData
	var t models.DriverEventData
	for {
		err := row.Next(&t)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Errorf("models.DriverEventData binding error: %s", err)
		}
		driverEvents = append(driverEvents, t)
	}

	return &driverEvents, nil
}

func (bq *BigQuery) GetDriverOrders(ctx context.Context, uuid string, tt *time.Time) (*[]models.BQOrderRaw, error) {
	log := logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"event": "getting driver orders by driver_uuid from bigquery",
	})

	if tt == nil {
		temp := time.Date(2020, time.January, 1, 1, 1, 1, 1, time.Local)
		tt = &temp
	}

	qstr := "SELECT * FROM `" + bq.OrdersTable + "` WHERE DATE(CreatedDatetime) > '" + timeToStr(tt) + "' AND DriverUUID = '" + uuid + "'"
	log.WithFields(logrus.Fields{
		"query": qstr,
	}).Trace()

	query := bq.Client.Query(qstr)
	row, err := query.Read(ctx)
	if err != nil {
		return nil, err
	}

	var driverOrders []models.BQOrderRaw
	var t models.BQOrderRaw
	for {
		err := row.Next(&t)
		if err == iterator.Done {
			break
		}
		if err != nil {
			log.Errorf("models.BQOrderRaw binding error: %s", err)
			continue
		}
		driverOrders = append(driverOrders, t)
	}

	return &driverOrders, nil
}
