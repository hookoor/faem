package repository

import (
	"context"
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
)

func (p *Pg) GetOperatorCreatedOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error) {
	var fetch []*models.OperatorDBItem
	q := p.CrmDB.ModelContext(ctx, (*crmModels.UsersCRM)(nil)).
		Column("users_crm.uuid", "users_crm.name", "o.change").
		ColumnExpr("count(o.*) AS aggr").
		Join(
			fmt.Sprintf(`
INNER JOIN (
	SELECT *,
		(CASE
			WHEN created_at::timetz AT TIME ZONE '%s' BETWEEN ? AND ? THEN '%s'
			ELSE '%s'
		END) AS change
	FROM crm_orders
) AS o ON users_crm.uuid = o.start_user_uuid`, models.TimeZoneShort, models.OperatorChangeDay, models.OperatorChangeNight),
			filter.DayStart, filter.DayEnd).
		Group("users_crm.uuid", "users_crm.name", "o.change").
		Order("users_crm.name", "o.change")
	if filter.OperatorUUID != "" {
		q = q.Where("users_crm.uuid = ?", filter.OperatorUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("o.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("o.created_at <= ?", filter.PeriodEnd)
	}
	if err := q.Select(&fetch); err != nil {
		return nil, err
	}

	// Transform to the map
	operatorMap := make(map[string]*models.Operator)
	for _, op := range fetch {
		operator, found := operatorMap[op.UUID]
		if !found {
			operator = &models.Operator{
				UUID: op.UUID,
				Name: op.Name,
			}
			if op.Change == models.OperatorChangeDay {
				operator.OrdersCreated.Day = op.Aggr
			} else {
				operator.OrdersCreated.Night = op.Aggr
			}
			operatorMap[op.UUID] = operator
			continue
		}
		if op.Change == models.OperatorChangeDay {
			operator.OrdersCreated.Day = op.Aggr
		} else {
			operator.OrdersCreated.Night = op.Aggr
		}
	}

	// Aggregate and return the result
	result := make([]*models.Operator, 0, len(operatorMap))
	for _, op := range operatorMap {
		op.OrdersCreated.Total = op.OrdersCreated.Day + op.OrdersCreated.Night
		result = append(result, op)
	}
	return result, nil
}

func (p *Pg) GetOperatorSuccessfulOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error) {
	var fetch []*models.OperatorDBItem
	q := p.CrmDB.ModelContext(ctx, (*crmModels.UsersCRM)(nil)).
		Column("users_crm.uuid", "users_crm.name", "o.change").
		ColumnExpr("count(o.*) AS aggr").
		Join(
			fmt.Sprintf(`
INNER JOIN (
	SELECT *,
		(CASE
			WHEN created_at::timetz AT TIME ZONE '%s' BETWEEN ? AND ? THEN '%s'
			ELSE '%s'
		END) AS change
	FROM crm_orders
) AS o ON users_crm.uuid = o.start_user_uuid`, models.TimeZoneShort, models.OperatorChangeDay, models.OperatorChangeNight),
			filter.DayStart, filter.DayEnd).
		Where("o.order_state = ?", constants.OrderStateFinished).
		Group("users_crm.uuid", "users_crm.name", "o.change").
		Order("users_crm.name", "o.change")
	if filter.OperatorUUID != "" {
		q = q.Where("users_crm.uuid = ?", filter.OperatorUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("o.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("o.created_at <= ?", filter.PeriodEnd)
	}
	if err := q.Select(&fetch); err != nil {
		return nil, err
	}

	// Transform to the map
	operatorMap := make(map[string]*models.Operator)
	for _, op := range fetch {
		operator, found := operatorMap[op.UUID]
		if !found {
			operator = &models.Operator{
				UUID: op.UUID,
				Name: op.Name,
			}
			if op.Change == models.OperatorChangeDay {
				operator.OrdersSuccessful.Day = op.Aggr
			} else {
				operator.OrdersSuccessful.Night = op.Aggr
			}
			operatorMap[op.UUID] = operator
			continue
		}
		if op.Change == models.OperatorChangeDay {
			operator.OrdersSuccessful.Day = op.Aggr
		} else {
			operator.OrdersSuccessful.Night = op.Aggr
		}
	}

	// Aggregate and return the result
	result := make([]*models.Operator, 0, len(operatorMap))
	for _, op := range operatorMap {
		op.OrdersSuccessful.Total = op.OrdersSuccessful.Day + op.OrdersSuccessful.Night
		result = append(result, op)
	}
	return result, nil
}

func (p *Pg) GetOperatorCancelledOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error) {
	var fetch []*models.OperatorDBItem
	q := p.CrmDB.ModelContext(ctx, (*crmModels.UsersCRM)(nil)).
		Column("users_crm.uuid", "users_crm.name", "o.change").
		ColumnExpr("count(o.*) AS aggr").
		Join(
			fmt.Sprintf(`
INNER JOIN (
	SELECT *,
		(CASE
			WHEN created_at::timetz AT TIME ZONE '%s' BETWEEN ? AND ? THEN '%s'
			ELSE '%s'
		END) AS change
	FROM crm_orders
) AS o ON users_crm.uuid = o.start_user_uuid`, models.TimeZoneShort, models.OperatorChangeDay, models.OperatorChangeNight),
			filter.DayStart, filter.DayEnd).
		WhereInMulti("o.order_state IN (?)", constants.OrderStateCancelled, constants.OrderStateDriverNotFound).
		Group("users_crm.uuid", "users_crm.name", "o.change").
		Order("users_crm.name", "o.change")
	if filter.OperatorUUID != "" {
		q = q.Where("users_crm.uuid = ?", filter.OperatorUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("o.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("o.created_at <= ?", filter.PeriodEnd)
	}
	if err := q.Select(&fetch); err != nil {
		return nil, err
	}

	// Transform to the map
	operatorMap := make(map[string]*models.Operator)
	for _, op := range fetch {
		operator, found := operatorMap[op.UUID]
		if !found {
			operator = &models.Operator{
				UUID: op.UUID,
				Name: op.Name,
			}
			if op.Change == models.OperatorChangeDay {
				operator.OrdersCancelled.Day = op.Aggr
			} else {
				operator.OrdersCancelled.Night = op.Aggr
			}
			operatorMap[op.UUID] = operator
			continue
		}
		if op.Change == models.OperatorChangeDay {
			operator.OrdersCancelled.Day = op.Aggr
		} else {
			operator.OrdersCancelled.Night = op.Aggr
		}
	}

	// Aggregate and return the result
	result := make([]*models.Operator, 0, len(operatorMap))
	for _, op := range operatorMap {
		op.OrdersCancelled.Total = op.OrdersCancelled.Day + op.OrdersCancelled.Night
		result = append(result, op)
	}
	return result, nil
}

func (p *Pg) GetOperatorCalls(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error) {
	var fetch []*models.OperatorDBItem
	q := p.CrmDB.Model((*crmModels.UsersCRM)(nil)).
		ColumnExpr("DISTINCT ON (c.id) users_crm.uuid, users_crm.name, c.*").
		Join(
			fmt.Sprintf(`
INNER JOIN (
	SELECT *,
		(CASE
			WHEN event_time::timetz AT TIME ZONE '%s' BETWEEN ? AND ? THEN '%s'
			ELSE '%s'
		END) AS change
	FROM crm_events
) AS c on users_crm.login_telephony = c.payload ->> 'operator'
`, models.TimeZoneShort, models.OperatorChangeDay, models.OperatorChangeNight),
			filter.DayStart, filter.DayEnd).
		Where("LOWER(c.value) = ?", models.CallAnswer)
	if filter.OperatorUUID != "" {
		q = q.Where("users_crm.uuid = ?", filter.OperatorUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("c.event_time >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("c.event_time <= ?", filter.PeriodEnd)
	}

	err := p.CrmDB.ModelContext(ctx).
		Column("q.uuid", "q.name", "q.change").
		ColumnExpr("count(q.*) AS aggr").
		TableExpr("(?) AS q", q).
		Group("q.uuid", "q.name", "q.change").
		Order("q.name", "q.change").
		Select(&fetch)
	if err != nil {
		return nil, err
	}

	// Transform to the map
	operatorMap := make(map[string]*models.Operator)
	for _, op := range fetch {
		operator, found := operatorMap[op.UUID]
		if !found {
			operator = &models.Operator{
				UUID: op.UUID,
				Name: op.Name,
			}
			if op.Change == models.OperatorChangeDay {
				operator.Calls.Day = op.Aggr
			} else {
				operator.Calls.Night = op.Aggr
			}
			operatorMap[op.UUID] = operator
			continue
		}
		if op.Change == models.OperatorChangeDay {
			operator.Calls.Day = op.Aggr
		} else {
			operator.Calls.Night = op.Aggr
		}
	}

	// Aggregate and return the result
	result := make([]*models.Operator, 0, len(operatorMap))
	for _, op := range operatorMap {
		op.Calls.Total = op.Calls.Day + op.Calls.Night
		result = append(result, op)
	}
	return result, nil
}
