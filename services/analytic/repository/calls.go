package repository

import (
	"context"
	"fmt"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/crm/rabhandler"
)

func (p *Pg) GetCalls(ctx context.Context, filter *models.CallFilter) ([]*models.Call, error) {
	var result []*models.Call
	q := p.CrmDB.Model((*rabhandler.Events)(nil)).
		ColumnExpr("payload ->> 'exten' AS source").
		ColumnExpr("CASE WHEN LOWER(payload ->> 'type') = ? THEN 1 END AS incoming", models.CallIncoming).
		ColumnExpr("CASE WHEN LOWER(payload ->> 'type') = ? THEN 1 END AS outgoing", models.CallOutgoing).
		ColumnExpr("CASE WHEN LOWER(payload ->> 'type') = ? AND LOWER(value) <> ? THEN 1 END AS missed", models.CallIncoming, models.CallAnswer).
		Where("publisher = ?", models.VoipServiceName)
	if filter.Source != "" {
		q = q.Where("payload ->> 'exten' = ?", filter.Source)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("event_time >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("event_time <= ?", filter.PeriodEnd)
	}

	sort := filter.Sort.Field
	if sort == "" {
		sort = models.DefaultCallSort
	}

	err := p.CrmDB.ModelContext(ctx).
		Column("q.source").
		ColumnExpr("COUNT(q.incoming) AS incoming").
		ColumnExpr("COUNT(q.outgoing) AS outgoing").
		ColumnExpr("COUNT(q.missed) AS missed").
		TableExpr("(?) AS q", q).
		Group("q.source").
		Order(fmt.Sprintf("%s %s", sort, filter.Sort.Dir)).
		Select(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}
