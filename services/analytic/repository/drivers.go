package repository

import (
	"context"
	"github.com/pkg/errors"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
)

func (p *Pg) GetDriverOrders(ctx context.Context, filter *models.DriverFilter) ([]*models.Driver, error) {
	var result []*models.Driver
	q := p.CrmDB.Model((*crmModels.DriverCRM)(nil)).
		Column("driver_crm.uuid", "driver_crm.name").
		ColumnExpr("CASE WHEN os.state = ? THEN 1 END AS offered", constants.OrderStateOffered).
		ColumnExpr("CASE WHEN os.state = ? THEN 1 END AS accepted", constants.OrderStateAccepted).
		ColumnExpr("CASE WHEN os.state = ? THEN 1 END AS finished", constants.OrderStateFinished).
		Join("INNER JOIN crm_orderstates AS os ON driver_crm.uuid = os.driver_uuid")
	if filter.DriverUUID != "" {
		q = q.Where("driver_crm.alias = ?", filter.DriverUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("os.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("os.created_at <= ?", filter.PeriodEnd)
	}

	err := p.CrmDB.ModelContext(ctx).
		Column("q.uuid", "q.name").
		ColumnExpr("COUNT(q.offered) AS offered").
		ColumnExpr("COUNT(q.accepted) AS accepted").
		ColumnExpr("COUNT(q.finished) AS finished").
		TableExpr("(?) AS q", q).
		Group("q.uuid", "q.name").
		Order("q.name").
		Select(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (p *Pg) GetDriverIncome(ctx context.Context, filter *models.DriverFilter) ([]*models.Driver, error) {
	var result []*models.Driver
	q := p.CrmDB.ModelContext(ctx, (*crmModels.DriverCRM)(nil)).
		Column("driver_crm.uuid", "driver_crm.name").
		ColumnExpr("SUM((o.tariff ->> 'total_price')::integer) AS income").
		Join(
			"INNER JOIN crm_orderstates AS os ON driver_crm.uuid = os.driver_uuid AND os.state = ?",
			constants.OrderStateFinished,
		).
		Join("INNER JOIN crm_orders AS o ON os.order_uuid = o.uuid").
		Group("driver_crm.uuid", "driver_crm.name").
		Order("driver_crm.name")
	if filter.DriverUUID != "" {
		q = q.Where("driver_crm.alias = ?", filter.DriverUUID)
	}
	if !filter.PeriodStart.IsZero() {
		q = q.Where("os.created_at >= ?", filter.PeriodStart)
	}
	if !filter.PeriodEnd.IsZero() {
		q = q.Where("os.created_at <= ?", filter.PeriodEnd)
	}

	if err := q.Select(&result); err != nil {
		return nil, err
	}
	return result, nil
}

func (p *Pg) GetDriversByPeriod(ctx context.Context, startTime, endTime time.Time) ([]crmModels.DriverCRM, error) {
	var drivers []crmModels.DriverCRM
	err := p.CrmDB.ModelContext(ctx, &drivers).
		Where("created_at > ? AND created_at < ?", startTime, endTime).
		Select()
	if err != nil {
		return drivers, errors.Wrap(err, "failed to get drivers from DB")
	}

	return drivers, nil
}
