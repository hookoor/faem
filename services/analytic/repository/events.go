package repository

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"time"
)

// GetCRMOrderByUUID - getting order by UUID
func (p *Pg) GetEventByPeriod(ctx context.Context, startTime, endTime time.Time) ([]models.Event, error) {
	var events []models.Event
	err := p.CrmDB.ModelContext(ctx, &events).
		Where("event_time > ? AND event_time < ?", startTime, endTime).
		Select()
	if err != nil {
		return events, errors.Wrap(err, "failed to get events from DB")
	}

	return events, nil
}
