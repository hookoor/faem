package rabsubscriber

import (
	"context"
	"encoding/json"
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"go.uber.org/multierr"
)

func (s *Subscriber) handleEvent(ctx context.Context, event models.Event) error {
	return multierr.Combine(
		s.Handler.SaveEvent(ctx, event),
		s.Handler.IncomingEventHandler(ctx, event),
	)
}

func (s *Subscriber) initEventsReciever() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.AnalyticEventsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.AnalyticEventsQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		"*",                  // routing key
		rabbit.EventExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                        // queue
		rabbit.AnalyticEventsConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return err
	}

	s.wg.Add(1)
	go s.handleIncomingEvents(msgs)
	return nil
}

func (s *Subscriber) handleIncomingEvents(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewUsersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case d := <-messages:
			ctx := context.Background()

			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "handling events",
			})

			var event models.Event
			err := json.Unmarshal([]byte(d.Body), &event)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error binding new order",
				}).Error(errpath.Err(err))
				continue
			}

			err = s.handleEvent(ctx, event)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error handle new event",
				}).Error(errpath.Err(err))
				continue
			}
			log.WithFields(logrus.Fields{
				"event type":  event.Event,
				"event value": event.Value,
			}).Debug("Event Saved")

		}
	}
}
