BigQuery

записать в таблицу(insert)
https://cloud.google.com/bigquery/streaming-data-into-bigquery


создание таблицы с помощью кода
https://cloud.google.com/bigquery/docs/nested-repeated?hl=ru


как записывать jsonb формат? {
	(придется парсить стринговый json)
	данные типа jsonb записываются в поле типа string
	оперировать значениями придется с помощью json_extract()
	https://www.youtube.com/watch?v=KyEU8VQb3sQ

	(json должен иметь постоянный формат)
	Инструкция по вложенным таблицам
	https://cloud.google.com/bigquery/docs/nested-repeated?hl=ru
}


JSON_EXTRACT
https://database.guide/json_extract-return-data-from-a-json-document-in-mysql/
Examples
SELECT JSON_EXTRACT(full_name, '$.b') AS Result from SomeData.item
SELECT JSON_EXTRACT(full_name, '$.routes.geometry.coordinates[0][0]') AS Result from SomeData.item
SELECT JSON_EXTRACT('{"routes":{"type":"Feature","geometry":{"type":"LineString","coordinates":[[44.669358,43.024426],[44.666203,43.029587]]}}}', '$.routes.geometry.coordinates[0][0]') AS Result
SELECT JSON_EXTRACT('[1, 2, 3]', '$[2]') AS 'Result';


каст (приведение) типов
https://cloud.google.com/bigquery/docs/reference/standard-sql/conversion_rules?hl=ru


режим repeated - массив значений
тип record - вложенные поля (как в json {})


как записать tags []string в колонку с вложением(не json)?
колонка tags должна быть массивом объектов (tags record repeated) и иметь поле-значение массива(пусть оно называется arrvalue)


вложенные типы:
1. если поле пустое то не отправлять значение карты 
2. можно закинуть массив-структуры (если в ней нет вложений(но это не точно))
3. как сделать запись в колонку вида: column "type": "string", "mode": "repeated" ??? я хз
