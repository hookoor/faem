package cloudstorage

import (
	"context"
	"encoding/json"
	"fmt"
	"hash/crc32"
	"math/rand"
	"strings"
	"time"

	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"

	"cloud.google.com/go/storage"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"go.uber.org/multierr"
)

const (
	writerTimeOutSec = 60
)

type CloudStorage struct {
	Client              *storage.Client
	Bucket              *storage.BucketHandle
	EventsBufferStream  []models.Event
	EventsBufferGrabber []models.Event

	// BillingEventsBufferStream  []models.DataUnit
	// BillingEventsBufferGrabber []models.DataUnit

	BillingAccountsBufferStream       []billingModels.Account
	BillingAccountsBufferGrabber      []billingModels.Account
	BillingBankTransfersBufferStream  []billingModels.BankTransfer
	BillingBankTransfersBufferGrabber []billingModels.BankTransfer
	BillingEntriesBufferStream        []billingModels.Entry
	BillingEntriesBufferGrabber       []billingModels.Entry
	BillingTransactionsBufferStream   []billingModels.Transaction
	BillingTransactionsBufferGrabber  []billingModels.Transaction
	BillingTransfersBufferStream      []billingModels.Transfer
	BillingTransfersBufferGrabber     []billingModels.Transfer

	StopGabbing bool
}

func InitCloudStorage(bucketName string) (CloudStorage, error) {
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	bucket := client.Bucket(bucketName)
	if err != nil {
		return CloudStorage{}, errors.Wrap(err, "failed to init GCP storage client")
	}
	cs := CloudStorage{
		Client: client,
		Bucket: bucket,
	}

	return cs, nil
}

func (cs *CloudStorage) SetStopFlag() { cs.StopGabbing = true }

func (cs *CloudStorage) SetStartFlag() { cs.StopGabbing = false }

func (cs *CloudStorage) GetStopFlag() bool { return cs.StopGabbing }

func (cs *CloudStorage) Close() error {
	ctx := context.Background()
	return multierr.Combine(
		errors.Wrap(cs.FlushAndSaveEventBuffer(ctx, "stream"), "error while flushing event buffer"),
		errors.Wrap(cs.FlushAndSaveEventBuffer(ctx, "grab"), "error while flushing event buffer"),
		errors.Wrap(cs.Client.Close(), "error while closing cloud client"),
	)
}

func (cs *CloudStorage) WriteToStorage(ctx context.Context, byteData *[]byte, filename string) error {
	dur := time.Second * time.Duration(writerTimeOutSec)
	tctx, cancel := context.WithTimeout(ctx, dur)
	defer cancel()
	_, err := cs.Bucket.Object(filename).Attrs(ctx)

	//а вдруг такой файл уже есть - создаим случайный
	if err != storage.ErrObjectNotExist {
		s1 := rand.NewSource(time.Now().UnixNano())
		r1 := rand.New(s1)
		f := strings.Split(filename, ".")
		if len(f) > 0 {
			filename = fmt.Sprintf("%s-%d.%s", f[0], r1.Intn(100), f[1])
		}
		return nil
	}

	wr := cs.Bucket.Object(filename).NewWriter(tctx)

	wr.CRC32C = crc32.Checksum(*byteData, crc32.MakeTable(crc32.Castagnoli))
	wr.SendCRC32C = true

	if _, err := wr.Write(*byteData); err != nil {
		return errors.Wrap(err, "failed to write data to storage")
	}

	if err := wr.Close(); err != nil {
		return errors.Wrap(err, "error while closing writer to storage")
	}

	return nil
}

//SaveEventToStorage сохраняем ивент в облако, с по-минутным буфером
func (cs *CloudStorage) SaveEventToStorage(ctx context.Context, event models.Event, t string) error {

	//собираем ивенты по минутно, если имя файла разное значет и время
	//разное, значит можно очищать буффер
	switch t {
	case "stream":
		if len(cs.EventsBufferStream) == 0 {
			cs.EventsBufferStream = append(cs.EventsBufferStream, event)
			return nil
		}
		bufFile := getEventsFilename(cs.EventsBufferStream[0].EventTime, models.EventsType)
		curFile := getEventsFilename(event.EventTime, models.EventsType)
		cs.EventsBufferStream = append(cs.EventsBufferStream, event)
		if bufFile == curFile {
			return nil
		}
	case "grab":
		if len(cs.EventsBufferGrabber) == 0 {
			cs.EventsBufferGrabber = append(cs.EventsBufferGrabber, event)
			return nil
		}
		bufFile := getEventsFilename(cs.EventsBufferGrabber[0].EventTime, models.EventsType)
		curFile := getEventsFilename(event.EventTime, models.EventsType)
		cs.EventsBufferGrabber = append(cs.EventsBufferGrabber, event)
		if bufFile == curFile {
			return nil
		}
	}

	//если нет, то очищаем буфер
	err := cs.FlushAndSaveEventBuffer(ctx, t)
	if err != nil {
		return err
	}

	return nil
}

// FlushAndSaveEventBuffer - записывает данные с буфера в хранилище и очищает буфер
func (cs *CloudStorage) FlushAndSaveEventBuffer(ctx context.Context, t string) error {
	switch t {
	case "stream":
		if len(cs.EventsBufferStream) == 0 {
			return nil
		}
		data, err := json.Marshal(&cs.EventsBufferStream)
		if err != nil {
			return errors.Wrap(err, "failed to marshal stream buffer")
		}
		err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.EventsBufferStream[0].EventTime, models.EventsType))
		if err != nil {
			return errors.Wrap(err, "failed to send event to storage")
		}
		cs.EventsBufferStream = nil
		return nil
	case "grab":
		if len(cs.EventsBufferGrabber) == 0 {
			return nil
		}
		data, err := json.Marshal(&cs.EventsBufferGrabber)
		if err != nil {
			return errors.Wrap(err, "failed to marshal graber buffer")
		}
		err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.EventsBufferGrabber[0].EventTime, models.EventsType))
		if err != nil {
			return errors.Wrap(err, "failed to send event to storage")
		}
		cs.EventsBufferGrabber = nil
		return nil
	}
	return nil
}

// SaveBillingEventToStorage -
func (cs *CloudStorage) SaveBillingEventToStorage(ctx context.Context, billingEvent interface{}, savingMethod string) error {

	switch event := billingEvent.(type) {
	case billingModels.Account:

		switch savingMethod {
		case "stream":
			if len(cs.BillingAccountsBufferStream) == 0 {
				cs.BillingAccountsBufferStream = append(cs.BillingAccountsBufferStream, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingAccountsBufferStream[0].CreatedAt, models.BillingsAccountsType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsAccountsType)
			cs.BillingAccountsBufferStream = append(cs.BillingAccountsBufferStream, event)

			if bufFile == curFile {
				return nil
			}

		case "grab":
			if len(cs.BillingAccountsBufferGrabber) == 0 {
				cs.BillingAccountsBufferGrabber = append(cs.BillingAccountsBufferGrabber, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingAccountsBufferGrabber[0].CreatedAt, models.BillingsAccountsType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsAccountsType)
			cs.BillingAccountsBufferGrabber = append(cs.BillingAccountsBufferGrabber, event)
			if bufFile == curFile {
				return nil
			}
		}

	case billingModels.BankTransfer:
		switch savingMethod {
		case "stream":
			if len(cs.BillingBankTransfersBufferStream) == 0 {
				cs.BillingBankTransfersBufferStream = append(cs.BillingBankTransfersBufferStream, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingBankTransfersBufferStream[0].CreatedAt, models.BillingsBankTransfersType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsBankTransfersType)
			cs.BillingBankTransfersBufferStream = append(cs.BillingBankTransfersBufferStream, event)
			if bufFile == curFile {
				return nil
			}

		case "grab":
			if len(cs.BillingBankTransfersBufferGrabber) == 0 {
				cs.BillingBankTransfersBufferGrabber = append(cs.BillingBankTransfersBufferGrabber, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingBankTransfersBufferGrabber[0].CreatedAt, models.BillingsBankTransfersType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsBankTransfersType)
			cs.BillingBankTransfersBufferGrabber = append(cs.BillingBankTransfersBufferGrabber, event)
			if bufFile == curFile {
				return nil
			}
		}

	case billingModels.Entry:

		switch savingMethod {
		case "stream":
			if len(cs.BillingEntriesBufferStream) == 0 {
				cs.BillingEntriesBufferStream = append(cs.BillingEntriesBufferStream, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingEntriesBufferStream[0].CreatedAt, models.BillingsEntriesType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsEntriesType)
			cs.BillingEntriesBufferStream = append(cs.BillingEntriesBufferStream, event)
			if bufFile == curFile {
				return nil
			}
		case "grab":
			if len(cs.BillingEntriesBufferGrabber) == 0 {
				cs.BillingEntriesBufferGrabber = append(cs.BillingEntriesBufferGrabber, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingEntriesBufferGrabber[0].CreatedAt, models.BillingsEntriesType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsEntriesType)
			cs.BillingEntriesBufferGrabber = append(cs.BillingEntriesBufferGrabber, event)
			if bufFile == curFile {
				return nil
			}
		}

	case billingModels.Transaction:
		switch savingMethod {
		case "stream":
			if len(cs.BillingTransactionsBufferStream) == 0 {
				cs.BillingTransactionsBufferStream = append(cs.BillingTransactionsBufferStream, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingTransactionsBufferStream[0].CreatedAt, models.BillingsTransactionsType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsTransactionsType)
			cs.BillingTransactionsBufferStream = append(cs.BillingTransactionsBufferStream, event)
			if bufFile == curFile {
				return nil
			}
		case "grab":
			if len(cs.BillingTransactionsBufferGrabber) == 0 {
				cs.BillingTransactionsBufferGrabber = append(cs.BillingTransactionsBufferGrabber, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingTransactionsBufferGrabber[0].CreatedAt, models.BillingsTransactionsType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsTransactionsType)
			cs.BillingTransactionsBufferGrabber = append(cs.BillingTransactionsBufferGrabber, event)
			if bufFile == curFile {
				return nil
			}
		}

	case billingModels.Transfer:
		switch savingMethod {
		case "stream":
			if len(cs.BillingTransfersBufferStream) == 0 {
				cs.BillingTransfersBufferStream = append(cs.BillingTransfersBufferStream, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingTransfersBufferStream[0].CreatedAt, models.BillingsTransfersType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsTransfersType)
			cs.BillingTransfersBufferStream = append(cs.BillingTransfersBufferStream, event)
			if bufFile == curFile {
				return nil
			}
		case "grab":
			if len(cs.BillingTransfersBufferGrabber) == 0 {
				cs.BillingTransfersBufferGrabber = append(cs.BillingTransfersBufferGrabber, event)
				return nil
			}
			bufFile := getEventsFilename(cs.BillingTransfersBufferGrabber[0].CreatedAt, models.BillingsTransfersType)
			curFile := getEventsFilename(event.CreatedAt, models.BillingsTransfersType)
			cs.BillingTransfersBufferGrabber = append(cs.BillingTransfersBufferGrabber, event)
			if bufFile == curFile {
				return nil
			}
		}

	default:
		return errpath.Errorf("invalid type of billing event")
	}

	//если нет, то очищаем буфер
	err := cs.FlushAndSaveBillingEventBuffer(ctx, savingMethod)
	if err != nil {
		return err
	}

	return nil
}

// FlushAndSaveBillingEventBuffer - записывает данные с буфера билинговых ивентов в хранилище и очищает буфер
func (cs *CloudStorage) FlushAndSaveBillingEventBuffer(ctx context.Context, savingMethod string) error {
	switch savingMethod {
	case "stream":
		if len(cs.BillingAccountsBufferStream) != 0 {
			data, err := json.Marshal(&cs.BillingAccountsBufferStream)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingAccountsBufferStream[0].CreatedAt, models.BillingsAccountsType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingAccountsBufferStream = nil
		}
		if len(cs.BillingBankTransfersBufferStream) != 0 {
			data, err := json.Marshal(&cs.BillingBankTransfersBufferStream)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingBankTransfersBufferStream[0].CreatedAt, models.BillingsBankTransfersType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingBankTransfersBufferStream = nil
		}
		if len(cs.BillingEntriesBufferStream) != 0 {
			data, err := json.Marshal(&cs.BillingEntriesBufferStream)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingEntriesBufferStream[0].CreatedAt, models.BillingsEntriesType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingEntriesBufferStream = nil
		}
		if len(cs.BillingTransactionsBufferStream) != 0 {
			data, err := json.Marshal(&cs.BillingTransactionsBufferStream)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingTransactionsBufferStream[0].CreatedAt, models.BillingsTransactionsType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingTransactionsBufferStream = nil
		}
		if len(cs.BillingTransfersBufferStream) != 0 {
			data, err := json.Marshal(&cs.BillingTransfersBufferStream)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingTransfersBufferStream[0].CreatedAt, models.BillingsTransfersType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingTransfersBufferStream = nil
		}

	case "grab":
		if len(cs.BillingAccountsBufferGrabber) != 0 {
			data, err := json.Marshal(&cs.BillingAccountsBufferGrabber)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingAccountsBufferGrabber[0].CreatedAt, models.BillingsAccountsType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingAccountsBufferGrabber = nil
		}
		if len(cs.BillingBankTransfersBufferGrabber) != 0 {
			data, err := json.Marshal(&cs.BillingBankTransfersBufferGrabber)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingBankTransfersBufferGrabber[0].CreatedAt, models.BillingsBankTransfersType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingBankTransfersBufferGrabber = nil
		}
		if len(cs.BillingEntriesBufferGrabber) != 0 {
			data, err := json.Marshal(&cs.BillingEntriesBufferGrabber)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingEntriesBufferGrabber[0].CreatedAt, models.BillingsEntriesType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingEntriesBufferGrabber = nil
		}
		if len(cs.BillingTransactionsBufferGrabber) != 0 {
			data, err := json.Marshal(&cs.BillingTransactionsBufferGrabber)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingTransactionsBufferGrabber[0].CreatedAt, models.BillingsTransactionsType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingTransactionsBufferGrabber = nil
		}
		if len(cs.BillingTransfersBufferGrabber) != 0 {
			data, err := json.Marshal(&cs.BillingTransfersBufferGrabber)
			if err != nil {
				return errors.Wrap(err, "failed to marshal stream buffer")
			}
			err = cs.WriteToStorage(ctx, &data, getEventsFilename(cs.BillingTransfersBufferGrabber[0].CreatedAt, models.BillingsTransfersType))
			if err != nil {
				return errors.Wrap(err, "failed to send event to storage")
			}
			cs.BillingTransfersBufferGrabber = nil
		}

	}
	return nil
}

func getEventsFilename(createdTime time.Time, dataType models.SavingDataType) string {
	roundTime := createdTime.Truncate(10 * time.Minute).Unix()
	var dataName string
	y, m, d := createdTime.Date()

	switch dataType {
	case models.EventsType:
		dataName = string(models.EventsType)
	case models.BillingsAccountsType:

		dataName = "billing/" + string(models.BillingsAccountsType)
	case models.BillingsBankTransfersType:
		dataName = "billing/" + string(models.BillingsBankTransfersType)
	case models.BillingsEntriesType:
		dataName = "billing/" + string(models.BillingsEntriesType)
	case models.BillingsTransactionsType:
		dataName = "billing/" + string(models.BillingsTransactionsType)
	case models.BillingsTransfersType:
		dataName = "billing/" + string(models.BillingsTransfersType)
	// case models.OrdersType:

	default:
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "geting events file name",
			"data type": string(dataType),
		}).Warnln("unknown data type")
		dataName = string(models.UnknownType)
	}

	return fmt.Sprintf("%s/%d/%02d/%02d/%d.json", dataName, y, m, d, roundTime)
}
