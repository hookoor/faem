package helpers

import (
	"strconv"

	"github.com/go-pg/pg/urlvalues"
	"github.com/labstack/echo/v4"
)

func GetPager(c echo.Context) (urlvalues.Pager, error) {
	var pager urlvalues.Pager
	limit, _ := strconv.Atoi(c.QueryParam("limit"))

	page, _ := strconv.Atoi(c.QueryParam("page"))
	if page < 1 {
		page = 1
	}
	pager.Offset = (page - 1) * limit
	pager.Limit = limit
	return pager, nil
}
