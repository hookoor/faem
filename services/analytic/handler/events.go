package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"go.uber.org/multierr"
	"time"
)

//IncomingEventHandler - когда нужно навесить какую то логику на входящее событие
func (h *Handler) IncomingEventHandler(_ context.Context, _ models.Event) error {

	//Событие завершение заказа и статус неактивный и источник CRM, во избежании дублей
	//if (event.Event == variables.EventOrderState) && (variables.InactiveOrderStates(event.Value)) && (event.Publisher != "driver") {
	//	if len(event.OrderUUID) == 0 {
	//		return errors.New("failed to save finished order, orderUUID array are empty")
	//	}
	//	order, err := h.DB.GetCRMOrderByUUID(ctx, event.OrderUUID[0])
	//	if err != nil {
	//		return errors.Wrap(err, "failed to get order by UUID")
	//	}
	//	err = h.SendOrderToStorage(ctx, &order)
	//	if err != nil {
	//		return errors.Wrap(err, "failed to save order in storage")
	//	}
	//	var ord []models.OrderFromCRM
	//	ord = append(ord, order)
	//	if err := h.DL.InsertOrders(ctx, &ord); err != nil {
	//		return errors.Wrap(err, "Cant insert event to BigQuery")
	//	}
	//}
	return nil
}

func (h *Handler) SendOrderToStorage(ctx context.Context, order *models.OrderFromCRM) error {
	filename := generateOrderFilename(order.CreatedAt, order.UUID)
	data, err := json.Marshal(order)
	if err != nil {
		return errors.Wrap(err, "failed to marshal event")
	}
	err = h.CS.WriteToStorage(ctx, &data, filename)
	if err != nil {
		return errors.Wrap(err, "failed to write in storage ")
	}
	return nil
}

//SaveEvent - сохраняет событие в DataLake, вынесено что бы можно было проще сделать BULK SAVE
func (h *Handler) SaveEvent(ctx context.Context, event models.Event) error {
	return multierr.Append(
		errors.Wrap(h.CS.SaveEventToStorage(ctx, event, "stream"), "failed to save event in storage"),
		errors.Wrap(h.saveEventInDL(ctx, event), "failed to save event in data lake"),
	)
}
func (h *Handler) SendFullOrderToStorage(ctx context.Context, order *models.OrderFromCRM) error {
	filename := generateOrderFilename(order.CreatedAt, order.UUID)
	data, err := json.Marshal(order)
	if err != nil {
		return errors.Wrap(err, "failed to marshal event")
	}
	err = h.CS.WriteToStorage(ctx, &data, filename)
	if err != nil {
		return errors.Wrap(err, "failed to write in storage ")
	}
	return nil
}

func (h *Handler) saveEventInDL(ctx context.Context, event models.Event) error {
	switch event.Event {
	case constants.EventOrderState:
		ordEvent := newOrderEvent(event)
		return h.DL.InsertOrdersEvent(ctx, ordEvent)
	case constants.EventDriverState:
		drvEvent := newDriverEvent(event)
		return h.DL.InsertDriversEvent(ctx, drvEvent)
	case constants.EventTelephony:
		telEvent, err := newTelEvent(event)
		if err != nil {
			return err
		}
		return h.DL.InsertTelephonyEvent(ctx, telEvent)
	default:
		//return h.DL.InsertEvent(ctx, event)
		return nil
	}
}

func newTelEvent(event models.Event) (models.TelephonyEventData, error) {
	var tlEv models.TelephonyEventData
	tlEv.Event = event.Value
	tlEv.EventTime = event.EventTime
	tlEv.Publisher = event.Publisher
	if len(event.OrderUUID) > 0 {
		tlEv.OrderUUID = event.OrderUUID[0]
	}

	for k, v := range event.Payload.(map[string]interface{}) {
		switch k {
		case "type":
			tlEv.Type = v.(string)
		case "exten":
			tlEv.IncomingLine = v.(string)
		case "operator":
			tlEv.Operator = v.(string)
		case "caller_id":
			tlEv.Caller = v.(string)
		}
	}
	return tlEv, nil
}

type Telephony struct {
	structures.TelephonyCall
	Datetime time.Time `json:"datetime"`
}

func newDriverEvent(event models.Event) models.DriverEventData {
	var drEv models.DriverEventData
	drEv.EventTime = event.EventTime
	if len(event.DriverUUID) > 0 {
		drEv.DriverUUID = event.DriverUUID[0]
	}
	drEv.State = event.Value
	drEv.Publisher = event.Publisher
	drEv.Comment = event.Comment
	if len(event.DriverCoordinates) > 0 {
		drEv.Lat = float32(event.DriverCoordinates[0].Latitude)
		drEv.Lon = float32(event.DriverCoordinates[0].Longitude)
	}
	return drEv
}

func newOrderEvent(event models.Event) models.OrderEventData {
	var orEv models.OrderEventData
	orEv.EventTime = event.EventTime
	if len(event.DriverUUID) > 0 {
		orEv.DriverUUID = event.DriverUUID[0]
	}
	if len(event.OrderUUID) > 0 {
		orEv.OrderUUID = event.OrderUUID[0]
	}
	if len(event.OperatorUUID) > 0 {
		orEv.OperatorUUID = event.OperatorUUID[0]
	}
	orEv.Publisher = event.Publisher
	orEv.Comment = event.Comment
	orEv.State = event.Value

	return orEv
}

func generateOrderFilename(createdTime time.Time, uuid string) string {
	y, m, d := createdTime.Date()
	return fmt.Sprintf("orders/%d/%02d/%02d/%s-%d.json", y, m, d, uuid[:8], createdTime.Unix())
}
