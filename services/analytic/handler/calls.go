package handler

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

type CallRepository interface {
	GetCalls(ctx context.Context, filter *models.CallFilter) ([]*models.Call, error)
}

func (h *Handler) Calls(ctx context.Context, in *proto.CallFilter) (*models.CallReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate call filter")
	}

	filter := in.ToModel()
	calls, err := h.DB.GetCalls(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get calls")
	}

	// Aggregate
	var totalIncoming, totalOutgoing, totalMissed int
	for _, c := range calls {
		totalIncoming += c.Incoming
		totalOutgoing += c.Outgoing
		totalMissed += c.Missed
	}

	return &models.CallReport{
		Calls:         calls,
		TotalIncoming: totalIncoming,
		TotalOutgoing: totalOutgoing,
		TotalMissed:   totalMissed,
	}, nil
}
