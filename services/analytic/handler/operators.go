package handler

import (
	"context"
	"sort"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

type OperatorRepository interface {
	GetOperatorCreatedOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error)
	GetOperatorSuccessfulOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error)
	GetOperatorCancelledOrders(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error)
	GetOperatorCalls(ctx context.Context, filter *models.OperatorFilter) ([]*models.Operator, error)
}

func (h *Handler) Operators(ctx context.Context, in *proto.OperatorFilter) (*models.OperatorReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate operator filter")
	}

	filter := in.ToModel()
	// Get created orders first
	operators, err := h.DB.GetOperatorCreatedOrders(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get created operator orders")
	}
	// Create a global map
	operatorMap := make(map[string]*models.Operator)
	for _, op := range operators {
		operatorMap[op.UUID] = op
	}

	// Get successful orders
	operators, err = h.DB.GetOperatorSuccessfulOrders(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get successful operator orders")
	}
	// Add them to the map
	for _, op := range operators {
		operator, found := operatorMap[op.UUID]
		if !found { // just add a new operator if we don't know him yet
			operatorMap[op.UUID] = op
			continue
		}
		operator.OrdersSuccessful = op.OrdersSuccessful
	}

	// Get cancelled orders
	operators, err = h.DB.GetOperatorCancelledOrders(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get cancelled operator orders")
	}
	// Add them to the map
	for _, op := range operators {
		operator, found := operatorMap[op.UUID]
		if !found { // just add a new operator if we don't know him yet
			operatorMap[op.UUID] = op
			continue
		}
		operator.OrdersCancelled = op.OrdersCancelled
	}

	// Get calls
	operators, err = h.DB.GetOperatorCalls(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get operator calls")
	}
	// Add them to the map
	for _, op := range operators {
		operator, found := operatorMap[op.UUID]
		if !found { // just add a new operator if we don't know him yet
			operatorMap[op.UUID] = op
			continue
		}
		operator.Calls = op.Calls
	}

	// Transform map to the slice and aggregate
	operators = make([]*models.Operator, 0, len(operatorMap))
	var totalCalls, totalOrdersCreated, totalOrdersSuccessful, totalOrdersCancelled models.OperatorItems
	for _, op := range operatorMap {
		operators = append(operators, op)

		totalCalls.Total += op.Calls.Total
		totalCalls.Day += op.Calls.Day
		totalCalls.Night += op.Calls.Night

		totalOrdersCreated.Total += op.OrdersCreated.Total
		totalOrdersCreated.Day += op.OrdersCreated.Day
		totalOrdersCreated.Night += op.OrdersCreated.Night

		totalOrdersSuccessful.Total += op.OrdersSuccessful.Total
		totalOrdersSuccessful.Day += op.OrdersSuccessful.Day
		totalOrdersSuccessful.Night += op.OrdersSuccessful.Night

		totalOrdersCancelled.Total += op.OrdersCancelled.Total
		totalOrdersCancelled.Day += op.OrdersCancelled.Day
		totalOrdersCancelled.Night += op.OrdersCancelled.Night
	}
	// Then sort the slice
	sort.Slice(operators, func(i, j int) bool {
		switch models.OperatorReportSort(filter.Sort.Field) {
		case models.OperatorReportSortCallsTotal:
			return compareIntFields(operators[i].Calls.Total, operators[j].Calls.Total, filter.Sort.Dir)
		case models.OperatorReportSortCallsDay:
			return compareIntFields(operators[i].Calls.Day, operators[j].Calls.Day, filter.Sort.Dir)
		case models.OperatorReportSortCallsNight:
			return compareIntFields(operators[i].Calls.Night, operators[j].Calls.Night, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCreatedTotal:
			return compareIntFields(operators[i].OrdersCreated.Total, operators[j].OrdersCreated.Total, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCreatedDay:
			return compareIntFields(operators[i].OrdersCreated.Day, operators[j].OrdersCreated.Day, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCreatedNight:
			return compareIntFields(operators[i].OrdersCreated.Night, operators[j].OrdersCreated.Night, filter.Sort.Dir)
		case models.OperatorReportSortOrdersSuccessfulTotal:
			return compareIntFields(operators[i].OrdersSuccessful.Total, operators[j].OrdersSuccessful.Total, filter.Sort.Dir)
		case models.OperatorReportSortOrdersSuccessfulDay:
			return compareIntFields(operators[i].OrdersSuccessful.Day, operators[j].OrdersSuccessful.Day, filter.Sort.Dir)
		case models.OperatorReportSortOrdersSuccessfulNight:
			return compareIntFields(operators[i].OrdersSuccessful.Night, operators[j].OrdersSuccessful.Night, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCancelledTotal:
			return compareIntFields(operators[i].OrdersCancelled.Total, operators[j].OrdersCancelled.Total, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCancelledDay:
			return compareIntFields(operators[i].OrdersCancelled.Day, operators[j].OrdersCancelled.Day, filter.Sort.Dir)
		case models.OperatorReportSortOrdersCancelledNight:
			return compareIntFields(operators[i].OrdersCancelled.Night, operators[j].OrdersCancelled.Night, filter.Sort.Dir)
		}
		// default - by name
		return compareStringFields(operators[i].Name, operators[j].Name, filter.Sort.Dir)
	})
	return &models.OperatorReport{
		Operators:             operators,
		TotalCalls:            totalCalls,
		TotalOrdersCreated:    totalOrdersCreated,
		TotalOrdersSuccessful: totalOrdersSuccessful,
		TotalOrdersCancelled:  totalOrdersCancelled,
	}, nil
}
