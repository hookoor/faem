package handler

import (
	"context"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

const (
	//ordersSyncPeriod периодичность синхронизации заказов
	ordersSyncPeriod  = time.Minute * 15
	billingSyncPeriod = time.Minute * 20
)

var (
	wg     sync.WaitGroup
	closed = make(chan struct{})
	// startTime time.Time
)

func (h *Handler) InitSaverTicker() {
	//вся конструкция создана для того что бы таймер запускался в
	//кратные ordersSyncPeriod промежутки времени
	ctx := context.Background()
	var err error
	//возвращает время последнего заказа из таблицы BigQuery
	lastOrderTime, err := h.DL.GetLastOrderTime(ctx)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting last order time from bq",
			"reason": err,
		}).Error(err)
	}
	//возвращает время последнего заказа из BigQuery таблицы full_orders
	//lastFullOrderTime, err := h.DL.GetLastOrderTimeFullOrders(ctx)
	//if err != nil {
	//	logs.Eloger.WithFields(logrus.Fields{
	//		"event":  "getting last full order time from bq",
	//		"reason": err,
	//	}).Error(err)
	//}
	startTicker := time.Now().Truncate(ordersSyncPeriod).Add(ordersSyncPeriod)

	tickerStart := time.Now().Sub(startTicker)
	logs.Eloger.WithFields(logrus.Fields{
		"event":           "Starting order ticker",
		"last order time": lastOrderTime,
		//"last full order time": lastFullOrderTime,
		"ticker start": tickerStart,
	}).Info("Init ticker")

	wg.Add(1)
	go h.syncOrders(ordersSyncPeriod, lastOrderTime)
	//go h.syncFullOrders(15*time.Second, lastFullOrderTime)
}

// каждые timeout секунд забирает заказы из постгреса и заливает в BigQuery
func (h *Handler) syncOrders(timeout time.Duration, startTime time.Time) {
	defer wg.Done()
	for {
		select {
		case <-closed:
			return
		case <-time.After(timeout):
			ctx := context.Background()
			tn := time.Now()
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":        "Saving orders ticker",
				"startPeriod":  startTime,
				"finishPeriod": tn,
			})
			//берем список заказов
			orders, err := h.DB.GetOrdersByPeriod(ctx, startTime, tn, false)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "failed to get orders list",
					"from":   startTime,
				}).Error(err)
				continue
			}
			//sending orders to cloud storage
			for _, val := range orders {
				err := h.SendOrderToStorage(ctx, &val)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "failed to save in cloud storage",
						"from":   startTime,
					}).Error(err)
					continue
				}
			}

			err = h.DL.InsertOrders(ctx, &orders)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "failed to save orders in DL",
					"from":   startTime,
				}).Error(err)
				continue
			}
			log.WithFields(logrus.Fields{
				"rows":    len(orders),
				"latency": time.Now().Sub(tn),
			}).Info("Orders uploaded to data lake")
			startTime = tn
		}
	}
}

// каждые timeout секунд забирает заказы из постгреса и заливает в BigQuery full_orders
func (h *Handler) syncFullOrders(timeout time.Duration, startTime time.Time) {
	defer wg.Done()
	for {
		select {
		case <-closed:
			return
		case <-time.After(timeout):
			ctx := context.Background()
			tn := time.Now()
			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "trying to push full orders to CS and DL",
			})
			tickerLog := logs.Eloger.WithFields(logrus.Fields{
				"event":      "full orders sync",
				"start_time": startTime,
			})
			tickerLog.Info("new iteration")

			//берем список заказов
			orders, err := h.DB.GetFullOrdersByPeriod(ctx, startTime, tn, false)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "failed to get orders list from crm",
				}).Error(err)
				continue
			}
			log.Info("got orders from crm_db", len(orders))
			//sending orders to cloud storage
			for _, val := range orders {
				err := h.SendFullOrderToStorage(ctx, &val)
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "failed to save in cloud storage",
						"from":   startTime,
					}).Error(err)
					continue
				}
			}
			log.WithFields(logrus.Fields{
				"rows":    len(orders),
				"latency": time.Now().Sub(tn),
			}).Info("FullOrders uploaded to cloud storage")

			//sending orders to data lake
			err = h.DL.InsertFullOrders(ctx, &orders)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "failed to save orders in DL",
					"from":   startTime,
				}).Error(err)
				continue
			}
			log.WithFields(logrus.Fields{
				"rows":    len(orders),
				"latency": time.Now().Sub(tn),
			}).Info("FullOrders uploaded to data lake")
			startTime = tn
		}
	}
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("tickers stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("tickers stopped forcibly")
	}
}
