package handler

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

type OrderRepository interface {
	GetOrders(ctx context.Context, filter *models.OrderFilter) ([]*models.Order, error)
	GetCRMOrderByUUID(ctx context.Context, uuid string) (models.OrderFromCRM, error)
}

func (h *Handler) Orders(ctx context.Context, in *proto.OrderFilter) (*models.OrderReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate order filter")
	}

	filter := in.ToModel()
	orders, err := h.DB.GetOrders(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get orders")
	}

	// Aggregate
	var totalCount, totalFinished, totalCancelled int
	var averageFinished, averageCancelled float64
	for _, ord := range orders {
		totalCount += ord.Count
		totalFinished += ord.Finished
		totalCancelled += ord.Cancelled
		averageFinished += ord.FinishedPercent
		averageCancelled += ord.CancelledPercent
	}
	averageFinished /= float64(len(orders))
	averageCancelled /= float64(len(orders))

	return &models.OrderReport{
		Orders:                  orders,
		TotalCount:              totalCount,
		TotalFinished:           totalFinished,
		TotalCancelled:          totalCancelled,
		AverageFinishedPercent:  averageFinished,
		AverageCancelledPercent: averageCancelled,
	}, nil
}

func (h *Handler) GetOrderByUUID(ctx context.Context, uuid string) (*models.BQOrderRaw, error) {
	order, err := h.DL.GetOrderByUUID(ctx, uuid)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get order by uuid")
	}
	return order, nil
}

func (h *Handler) GetFilteredOrders(ctx context.Context, filter *proto.BQOrderFilter) (*proto.BQOrderRawWithCount, error) {
	orders, err := h.DL.GetFilteredOrders(ctx, filter)

	if err != nil {
		return nil, errors.Wrap(err, "failed to get filtered orders")
	}

	return orders, nil
}
