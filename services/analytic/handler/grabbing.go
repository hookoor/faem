package handler

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
)

type EventsRepository interface {
	GetEventByPeriod(ctx context.Context, startTime, endTime time.Time) ([]models.Event, error)
	//GetOrdersByPeriod возвращает список заказов, если created true то выборка будет по времени создание
	//иначе по времени завершения
	GetOrdersByPeriod(ctx context.Context, startTime, endTime time.Time, created bool) ([]models.OrderFromCRM, error)
	// Возвращает список водителей за указанный период
	GetDriversByPeriod(ctx context.Context, startTime, endTime time.Time) ([]crmModels.DriverCRM, error)
	// возвращает список полных (на 17/04/2021) заказов
	GetFullOrdersByPeriod(ctx context.Context, startTime time.Time, endTime time.Time, created bool) ([]models.OrderFromCRM, error)
}

type GrabbingStruct struct {
	StartPeriod  int64  `json:"start_period"`
	FinishPeriod int64  `json:"finish_period"`
	Step         int64  `json:"step"`
	Timeout      int64  `json:"timeout"`
	DataType     string `json:"data_type"`
}

func (h *Handler) StartGrabbing(grab *GrabbingStruct) {

	h.CS.SetStartFlag()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "start grabbing to cloud",
		"data type": grab.DataType,
	})
	ctx := context.Background()
	var count int

	startTime := time.Unix(grab.StartPeriod, 0)
	finishTime := time.Unix(grab.FinishPeriod, 0)
	curTime := startTime

	log.WithFields(logrus.Fields{
		"from": startTime,
		"to":   finishTime,
	}).Info("Start quering data")

	for curTime.Before(finishTime) {
		endTime := curTime.Add(time.Duration(grab.Step) * time.Second)
		log.WithFields(logrus.Fields{
			"from": curTime,
			"to":   endTime,
		}).Debug("Quering events")

		//startQ := time.Now()
		switch grab.DataType {
		case "billings":
			startDB := time.Now()
			var data []interface{}

			data, err := h.DB.GetBillingEventsByPeriod(ctx, curTime, endTime)
			if err != nil {
				log.WithFields(logrus.Fields{
					"from": curTime,
					"step": grab.Step,
				}).Errorf("failed to get bank transfers: %s", err)
				return
			}

			count += len(data)
			startCLoudUpload := time.Now()
			log.WithFields(logrus.Fields{
				"from":     curTime,
				"step":     grab.Step,
				"duration": startCLoudUpload.Sub(startDB).Seconds(),
				"rows":     len(data),
			}).Info("Data requested from DB. Starting uploading to cloud...")

			for _, val := range data {
				err := h.CS.SaveBillingEventToStorage(ctx, val, "grab")
				if err != nil {
					log.WithFields(logrus.Fields{
						"from": curTime,
						"step": grab.Step,
					}).Errorf("failed to save in cloud: %s", errpath.Err(err))
					return
				}

				switch val.(type) {
				case billingModels.Account:
					item := val.(billingModels.Account)
					err = h.DL.InsertAccountsItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing account in DL",
							"from":   startTime,
						}).Error(errpath.Err(err))
						continue
					}
				case billingModels.BankTransfer:
					item := val.(billingModels.BankTransfer)
					err = h.DL.InsertBankTransfersItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing BankTransfer in DL",
							"from":   startTime,
						}).Error(errpath.Err(err))
						continue
					}
				case billingModels.Entry:
					item := val.(billingModels.Entry)
					err = h.DL.InsertEntriesItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Entry in DL",
							"from":   startTime,
						}).Error(errpath.Err(err))
						continue
					}
				case billingModels.Transaction:
					item := val.(billingModels.Transaction)
					err = h.DL.InsertTransactionsItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Transaction in DL",
							"from":   startTime,
						}).Error(errpath.Err(err))
						continue
					}
				case billingModels.Transfer:
					item := val.(billingModels.Transfer)
					err = h.DL.InsertTransfersItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Transfer in DL",
							"from":   startTime,
						}).Error(errpath.Err(err))
						continue
					}
				default:
					log.WithFields(logrus.Fields{
						"reason": "failed to save billing event in DL",
						"from":   startTime,
					}).Warnln("invalid billing event type")
				}
			}

			log.WithFields(logrus.Fields{
				"duration": time.Now().Sub(startCLoudUpload).Seconds(),
				"rows":     len(data),
			}).Info("Events uploaded successfuly")

		case "events":
			startDB := time.Now()
			events, err := h.DB.GetEventByPeriod(ctx, curTime, endTime)
			if err != nil {
				log.WithFields(logrus.Fields{
					"from": curTime,
					"step": grab.Step,
				}).Errorf("failed to get data from db: %s", err)
				return
			}
			count += len(events)
			startCLoudUpload := time.Now()
			log.WithFields(logrus.Fields{
				"from":     curTime,
				"step":     grab.Step,
				"duration": startCLoudUpload.Sub(startDB).Seconds(),
				"rows":     len(events),
			}).Info("Data requested from DB. Starting uploading to cloud...")
			for _, val := range events {
				err := h.CS.SaveEventToStorage(ctx, val, "grab")
				if err != nil {
					log.WithFields(logrus.Fields{
						"from": curTime,
						"step": grab.Step,
					}).Errorf("failed to save in cloud: %s", err)
					return
				}
				err = h.saveEventInDL(ctx, val)
				if err != nil {
					log.Errorf("failed to save in BQ: %s", err)
					return
				}
			}
			log.WithFields(logrus.Fields{
				"duration": time.Now().Sub(startCLoudUpload).Seconds(),
				"rows":     len(events),
			}).Info("Events uploaded to DL")

		case "orders":
			startDB := time.Now()
			orders, err := h.DB.GetOrdersByPeriod(ctx, curTime, endTime, true)
			if err != nil {
				log.Errorf("failed to get data from db: %s", err)
				return
			}
			count += len(orders)
			startCloud := time.Now()
			log.WithFields(logrus.Fields{
				"from":     curTime,
				"step":     grab.Step,
				"duration": startCloud.Sub(startDB).Seconds(),
				"rows":     len(orders),
			}).Info("Data requested from DB")

			// временно отключаем сохранение стриминговых данных в облачное хранилище
			//for _, val := range orders {
			//	err := h.SendOrderToStorage(ctx, &val)
			//	if err != nil {
			//		log.Errorf("failed to save in cloud: %s", err)
			//		return
			//	}
			//}

			var ordersPointers []*models.OrderFromCRM
			for _, v := range orders {
				ordersPointers = append(ordersPointers, &v)
			}
			err = h.DL.InsertOrders(ctx, &orders)

			if err != nil {
				log.Errorf("failed to save in DL: %s", err)
				return
			}
			log.WithFields(logrus.Fields{
				"duration": time.Now().Sub(startCloud).Seconds(),
				"rows":     len(orders),
			}).Info("Orders uploaded to data lake")

		case "drivers":
			startDB := time.Now()
			drivers, err := h.DB.GetDriversByPeriod(ctx, curTime, endTime)
			if err != nil {
				log.Errorf("failed to get data from db: %s", err)
				return
			}
			count += len(drivers)
			startCloud := time.Now()
			log.WithFields(logrus.Fields{
				"from":     curTime,
				"step":     grab.Step,
				"duration": startCloud.Sub(startDB).Seconds(),
				"rows":     len(drivers),
			}).Info("Data requested from DB")

			err = h.DL.InsertDrivers(ctx, &drivers)

			if err != nil {
				log.Errorf("failed to save in DL: %s", err)
				return
			}
			log.WithFields(logrus.Fields{
				"duration": time.Now().Sub(startCloud).Seconds(),
				"rows":     len(drivers),
			}).Info("Drivers uploaded to data lake")

		default:
			log.WithFields(logrus.Fields{
				"command": grab.DataType,
			}).Warn("Unknown grabbing command")
			return
		}

		curTime = endTime
		log.WithFields(logrus.Fields{
			"duration": grab.Timeout,
		}).Debug("Sleeping...")
		if h.CS.GetStopFlag() {
			log.WithFields(logrus.Fields{
				"from": curTime,
			}).Info("called stop")
			return
		}
		time.Sleep(time.Duration(grab.Timeout) * time.Second)
	}
	log.WithFields(logrus.Fields{
		"from":           startTime,
		"to":             finishTime,
		"rows converted": count,
	}).Info("FINISHED!")

}
