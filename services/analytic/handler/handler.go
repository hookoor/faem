package handler

import (
	"context"
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
	crmModels "gitlab.com/faemproject/backend/faem/services/crm/models"
	"time"
)

type Handler struct {
	DB Repository
	DL DataLake
	CS CloudStorage
}

type Repository interface {
	OperatorRepository
	DriverRepository
	OrderRepository
	BillingRepository
	CallRepository
	EventsRepository
}

// DataLake -
type DataLake interface {
	InsertOrdersEvent(ctx context.Context, events models.OrderEventData) error
	InsertDriversEvent(ctx context.Context, events models.DriverEventData) error
	InsertTelephonyEvent(ctx context.Context, events models.TelephonyEventData) error
	InsertOrders(ctx context.Context, orders *[]models.OrderFromCRM) error
	InsertFullOrders(ctx context.Context, orders *[]models.OrderFromCRM) error
	InsertDrivers(ctx context.Context, order *[]crmModels.DriverCRM) error

	InsertAccountsItem(ctx context.Context, item billingModels.Account) error
	InsertBankTransfersItem(ctx context.Context, item billingModels.BankTransfer) error
	InsertEntriesItem(ctx context.Context, item billingModels.Entry) error
	InsertTransactionsItem(ctx context.Context, item billingModels.Transaction) error
	InsertTransfersItem(ctx context.Context, item billingModels.Transfer) error
	GetLastOrderTime(ctx context.Context) (time.Time, error)
	GetLastOrderTimeFullOrders(ctx context.Context) (time.Time, error)

	GetOrderByUUID(ctx context.Context, uuid string) (*models.BQOrderRaw, error)
	GetFilteredOrders(ctx context.Context, filter *proto.BQOrderFilter) (*proto.BQOrderRawWithCount, error)
	GetOrderEvents(ctx context.Context, uuid string, tt *time.Time) (*[]models.OrderEventData, error)
	GetLastBillingEventTime(ctx context.Context) (time.Time, error)

	GetDriver(ctx context.Context, uuid string) (*models.BQDriver, error)
	GetDriverOrders(ctx context.Context, uuid string, tt *time.Time) (*[]models.BQOrderRaw, error)
	GetDriverEvents(ctx context.Context, uuid string, tt *time.Time) (*[]models.DriverEventData, error)
}

// CloudStorage interface for saving data to data lake
type CloudStorage interface {
	WriteToStorage(ctx context.Context, byteData *[]byte, filename string) error
	SaveEventToStorage(ctx context.Context, event models.Event, t string) error

	SaveBillingEventToStorage(ctx context.Context, billingEvent interface{}, savingMethod string) error

	// SaveBillingAccountsToStorage(ctx context.Context, account billingModels.Account, savingMethod string) error
	// SaveBillingBankTransfersToStorage(ctx context.Context, bankTransfer billingModels.BankTransfer, savingMethod string) error
	// SaveBillingEntriesToStorage(ctx context.Context, entry billingModels.Entry, savingMethod string) error
	// SaveBillingTransactionsToStorage(ctx context.Context, transaction billingModels.Transaction, savingMethod string) error
	// SaveBillingTransfersToStorage(ctx context.Context, transfer billingModels.Transfer, savingMethod string) error

	Close() error
	SetStopFlag()
	SetStartFlag()
	GetStopFlag() bool
}
