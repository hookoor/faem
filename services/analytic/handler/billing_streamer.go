package handler

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"
)

// GetAccountsItems  + []billingModels.Account
// GetBankTransfersItems  +  []billingModels.BankTransfer
// GetEntriesItems  +  []billingModels.Entry
// GetTransactionsItems  +  []billingModels.Transaction
// GetTransfersItems  +  []billingModels.Transfer

// InitBillingEventsSaverTicker -
func (h *Handler) InitBillingEventsSaverTicker() {

	//вся конструкция создана для того что бы таймер запускался в
	//кратные (в данном случае 10) промежутки времени
	ctx := context.Background()
	var err error
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "Starting billing events ticker",
	})
	startTime, err := h.DL.GetLastBillingEventTime(ctx)
	if err != nil {
		log.WithField("reason", "cant get last order time").Error(err)
		return
	}

	if startTime.Add(12 * time.Hour).Before(time.Now()) {
		log.WithFields(logrus.Fields{
			"start time": startTime,
		}).Error("Billing data streaming aborted. Last entrie more then 12h to now, please grab data or call fixiks")
		return
	}

	startTicker := time.Now().Truncate(billingSyncPeriod).Add(billingSyncPeriod)

	log.WithFields(logrus.Fields{
		"start time":   startTime,
		"ticker start": startTicker,
	}).Info("Init ticker")

	tickerStart := time.Now().Sub(startTicker)

	wg.Add(1)
	time.AfterFunc(tickerStart, func() {
		go h.syncBillingEvents(billingSyncPeriod, startTime)
	})
}

// каждые timeout секунд забирает заказы из постгреса и заливает в BigQuery
func (h *Handler) syncBillingEvents(timeout time.Duration, startTime time.Time) {
	defer wg.Done()
	for {
		select {
		case <-closed:
			return
		case <-time.After(timeout):
			var err error
			ctx := context.Background()
			tn := time.Now()
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":        "Saving orders ticker",
				"startPeriod":  startTime,
				"finishPeriod": tn,
			})

			data, err := h.DB.GetBillingEventsByPeriod(ctx, startTime, tn)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "failed to get billing events list",
					"from":   startTime,
				}).Error(errpath.Err(err))
				continue
			}

			for _, val := range data {
				err := h.CS.SaveBillingEventToStorage(ctx, val, "stream")
				if err != nil {
					log.WithFields(logrus.Fields{
						"reason": "failed to save in cloud storage",
						"from":   startTime,
					}).Error(err)
					continue
				}

				switch val.(type) {
				case billingModels.Account:
					item := val.(billingModels.Account)
					err = h.DL.InsertAccountsItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing account in DL",
							"from":   startTime,
						}).Error(err)
						continue
					}
				case billingModels.BankTransfer:
					item := val.(billingModels.BankTransfer)
					err = h.DL.InsertBankTransfersItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing BankTransfer in DL",
							"from":   startTime,
						}).Error(err)
						continue
					}
				case billingModels.Entry:
					item := val.(billingModels.Entry)
					err = h.DL.InsertEntriesItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Entry in DL",
							"from":   startTime,
						}).Error(err)
						continue
					}
				case billingModels.Transaction:
					item := val.(billingModels.Transaction)
					err = h.DL.InsertTransactionsItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Transaction in DL",
							"from":   startTime,
						}).Error(err)
						continue
					}
				case billingModels.Transfer:
					item := val.(billingModels.Transfer)
					err = h.DL.InsertTransfersItem(ctx, item)
					if err != nil {
						log.WithFields(logrus.Fields{
							"reason": "failed to save billing Transfer in DL",
							"from":   startTime,
						}).Error(err)
						continue
					}
				default:
					log.WithFields(logrus.Fields{
						"reason": "failed to save billing event in DL",
						"from":   startTime,
					}).Warnln("invalid billing event type")
				}
			}

			log.WithFields(logrus.Fields{
				"rows":    len(data),
				"latency": time.Now().Sub(tn),
			}).Info("biling events saved in cloud storage")
			startTime = tn
		}
	}
}
