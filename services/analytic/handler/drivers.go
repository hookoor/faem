package handler

import (
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"sort"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

type DriverRepository interface {
	GetDriverOrders(ctx context.Context, filter *models.DriverFilter) ([]*models.Driver, error)
	GetDriverIncome(ctx context.Context, filter *models.DriverFilter) ([]*models.Driver, error)
}

func (h *Handler) Drivers(ctx context.Context, in *proto.DriverFilter) (*models.DriverReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate driver filter")
	}

	filter := in.ToModel()
	// Get created orders first
	drivers, err := h.DB.GetDriverOrders(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get created driver orders")
	}
	// Create a global map
	driverMap := make(map[string]*models.Driver)
	for _, drv := range drivers {
		driverMap[drv.UUID] = drv
	}

	// Get calls
	drivers, err = h.DB.GetDriverIncome(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get driver calls")
	}
	// Add them to the map
	for _, drv := range drivers {
		driver, found := driverMap[drv.UUID]
		if !found { // just add a new driver if we don't know him yet
			driverMap[drv.UUID] = drv
			continue
		}
		driver.Income = drv.Income
	}

	// Transform map to the slice and aggregate
	drivers = make([]*models.Driver, 0, len(driverMap))
	var totalOffered, totalAccepted, totalFinished int
	var totalIncome float64
	for _, drv := range driverMap {
		drivers = append(drivers, drv)

		totalOffered += drv.Offered
		totalAccepted += drv.Accepted
		totalFinished += drv.Finished
		totalIncome += float64(drv.Income)
	}
	// Then sort the slice
	sort.Slice(drivers, func(i, j int) bool {
		switch models.DriverReportSort(filter.Sort.Field) {
		case models.DriverReportSortOffered:
			return compareIntFields(drivers[i].Offered, drivers[j].Offered, filter.Sort.Dir)
		case models.DriverReportSortAccepted:
			return compareIntFields(drivers[i].Accepted, drivers[j].Accepted, filter.Sort.Dir)
		case models.DriverReportSortFinished:
			return compareIntFields(drivers[i].Finished, drivers[j].Finished, filter.Sort.Dir)
		case models.DriverReportSortIncome:
			return compareIntFields(drivers[i].Income, drivers[j].Income, filter.Sort.Dir)
		}
		// default - by name
		return compareStringFields(drivers[i].Name, drivers[j].Name, filter.Sort.Dir)
	})
	return &models.DriverReport{
		Drivers:       drivers,
		TotalOffered:  totalOffered,
		TotalAccepted: totalAccepted,
		TotalFinished: totalFinished,
		TotalIncome:   totalIncome,
	}, nil
}

func (h *Handler) GetDriverInfo(ctx context.Context, uuid string) (*proto.DriverFullInfo, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "getting driver events by driver_uuid from bigquery",
	})

	var err error
	var driverInfo proto.DriverFullInfo

	driverInfo.Driver, err = h.DL.GetDriver(ctx, uuid)
	if err != nil {
		log.WithFields(logrus.Fields{
			"msg":         "getting driver by driver_uuid",
			"driver_uuid": uuid,
		}).Error(err)
	}

	driverInfo.Orders, err = h.DL.GetDriverOrders(ctx, uuid, nil)
	if err != nil {
		log.WithFields(logrus.Fields{
			"msg":         "getting driver orders by driver_uuid",
			"driver_uuid": uuid,
		}).Error(err)
	}

	driverInfo.Events, err = h.DL.GetDriverEvents(ctx, uuid, nil)
	if err != nil {
		log.WithFields(logrus.Fields{
			"msg":         "getting driver events by driver_uuid",
			"driver_uuid": uuid,
		}).Error(err)
	}

	return &driverInfo, nil
}
