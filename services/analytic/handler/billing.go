package handler

import (
	"context"
	"time"

	"github.com/pkg/errors"
	billingModels "gitlab.com/faemproject/backend/faem/services/billing/models"

	"gitlab.com/faemproject/backend/faem/services/analytic/models"
	"gitlab.com/faemproject/backend/faem/services/analytic/proto"
)

type BillingRepository interface {
	GetTransfers(ctx context.Context, filter *models.TransferFilter) ([]*models.Transfer, error)
	GetSeparateTransfers(ctx context.Context, filter *models.SeparateTransferFilter) ([]*models.SeparateTransfer, int, error)
	GetTransferLogs(ctx context.Context, filter *models.TransferLogFilter) ([]*models.TransferLog, int, error)
	GetTariffs(ctx context.Context, filter *models.TariffFilter) ([]*models.Tariff, error)

	GetAccountsItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Account, error)
	GetBankTransfersItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.BankTransfer, error)
	GetEntriesItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Entry, error)
	GetTransactionsItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Transaction, error)
	GetTransfersItems(ctx context.Context, startTime, endTime time.Time) ([]billingModels.Transfer, error)

	GetBillingEventsByPeriod(ctx context.Context, startTime, endTime time.Time) ([]interface{}, error)
}

func (h *Handler) Transfers(ctx context.Context, in *proto.TransferFilter) (*models.TransferReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate transfer filter")
	}

	filter := in.ToModel()
	transfers, err := h.DB.GetTransfers(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get transfers")
	}

	// Aggregate
	var totalCount int
	var totalSum float64
	for _, top := range transfers {
		totalCount += top.Count
		totalSum += top.Sum
	}

	return &models.TransferReport{
		Transfers:  transfers,
		TotalCount: totalCount,
		TotalSum:   totalSum,
	}, nil
}

func (h *Handler) SeparateTransfers(ctx context.Context, in *proto.SeparateTransferFilter) (*models.SeparateTransferReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate separate transfer filter")
	}

	filter := in.ToModel()
	transfers, count, err := h.DB.GetSeparateTransfers(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get separate transfers")
	}

	return &models.SeparateTransferReport{
		Transfers:  transfers,
		TotalCount: count,
	}, nil
}

func (h *Handler) TransferLogs(ctx context.Context, in *proto.TransferLogFilter) (*models.TransferLogReport, error) {
	if err := in.Prepare(); err != nil {
		return nil, errors.Wrap(err, "failed to validate transfer log filter")
	}

	filter := in.ToModel()
	logs, count, err := h.DB.GetTransferLogs(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get transfer logs")
	}

	return &models.TransferLogReport{
		Logs:       logs,
		TotalCount: count,
	}, nil
}

func (h *Handler) Tariffs(ctx context.Context, in *proto.TariffFilter) (*models.TariffReport, error) {
	filter := in.ToModel()
	tariffs, err := h.DB.GetTariffs(ctx, &filter)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get transfers")
	}

	return &models.TariffReport{Tariffs: tariffs}, nil
}
