package handler

import (
	"gitlab.com/faemproject/backend/faem/services/analytic/models"
)

func compareIntFields(a, b int, dir models.SortDir) bool {
	if dir == models.SortDirDesc {
		return a > b
	}
	return a < b
}

func compareStringFields(a, b string, dir models.SortDir) bool {
	if dir == models.SortDirDesc {
		return a > b
	}
	return a < b
}
