package speech

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSpellMinutes(t *testing.T) {
	expect := map[int]string{
		1:   "1 минуту",
		2:   "2 минуты",
		7:   "7 минут",
		12:  "12 минут",
		15:  "15 минут",
		24:  "24 минуты",
		96:  "96 минут",
		118: "118 минут",
	}

	for min, val := range expect {
		spell := SpellMinutes(min)
		assert.Equal(t, val, spell)
	}
}
