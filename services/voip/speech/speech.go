package speech

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"math"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

type SpeechData struct {
	CarColor  string
	CarNumber string
	CarBrand  string
	ArriveIn  int64
}

const (
	willArrivePhrase       = "Здравствуйте, это такси. Через %s к вам подъедет $ %s цвет %s номер %s $ Пожалуйста ожидайте"
	willArrivePhraseNoTime = "Здравствуйте, это такси. К вам подъедет $ %s цвет %s номер %s $ Пожалуйста ожидайте"
	arrivedPhrase          = "Здравствуйте, вас ожидает $ %s цвет %s номер %s $ Пожалуйста выходите"
)

func TextToNotify(order models.CRMOrder, orderState string) (string, string, error) {
	var response string
	var cType string
	switch orderState {
	case constants.OrderStateOnPlace:
		var err error
		response, err = CarArrived(SpeechData{
			CarColor:  order.RawOrder.Driver.Color,
			CarNumber: order.RawOrder.Driver.RegNumber,
			CarBrand:  order.RawOrder.Driver.Car,
		})
		cType = structures.OnplaceAutoCall
		if err != nil {
			return "", "", err
		}
	}
	return response, cType, nil
}

func CarArrived(sp SpeechData) (string, error) {
	if sp.CarBrand == "" {
		//return "", errors.New("Car brand is empty")
	}
	if sp.CarNumber == "" {
		//return "", errors.New("Car number is empty")
	}
	sp.CarNumber = removeLetters(sp.CarNumber)

	return fmt.Sprintf(arrivedPhrase, sp.CarBrand, sp.CarColor, sp.CarNumber), nil
}

func CarWillArrive(sp SpeechData) (string, error) {
	if sp.CarNumber == "" {
		//return "", errors.New("car number is empty")
	}
	sp.CarNumber = removeLetters(sp.CarNumber)
	nowTime := time.Now()
	endTime := time.Unix(sp.ArriveIn, 0)

	//if endTime.Before(nowTime) {
	//	return "", errors.New("arriving time is end")
	//}
	var res string
	minutes := int(math.Ceil(endTime.Sub(nowTime).Minutes()))
	if minutes < 1 {
		res = fmt.Sprintf(willArrivePhraseNoTime, sp.CarBrand, sp.CarColor, sp.CarNumber)
	} else {
		// Склоняем числительное
		spellMinutes := SpellMinutes(minutes)
		// Формируем строку
		res = fmt.Sprintf(willArrivePhrase, spellMinutes, sp.CarBrand, sp.CarColor, sp.CarNumber)
	}
	return res, nil
}

func SpellMinutes(minutes int) string {
	// Process special cases
	div := minutes % 100
	if div >= 10 && div <= 20 {
		return fmt.Sprintf("%v минут", minutes)
	}

	div = minutes % 10
	if div == 1 {
		return fmt.Sprintf("%v минуту", minutes)
	}
	if div >= 2 && div <= 4 {
		return fmt.Sprintf("%v минуты", minutes)
	}
	return fmt.Sprintf("%v минут", minutes)
}

func PreparePhrases(phrase string) proto.SplittedPhrase {
	var res proto.SplittedPhrase
	parts := strings.Split(phrase, " $ ")
	emptyphrase := " ,, "
	res.Prefix = parts[0]
	res.PrefixHash = getHash(res.Prefix)

	switch len(parts) {
	case 1:
		res.Phrase = emptyphrase
		res.PrefixHash = getHash(emptyphrase)
		res.Postfix = emptyphrase
		res.PostfixHash = getHash(emptyphrase)
	case 2:
		res.Phrase = parts[1]
		res.PrefixHash = getHash(parts[1])
		res.Postfix = emptyphrase
		res.PostfixHash = getHash(emptyphrase)
	case 3:
		res.Phrase = parts[1]
		res.PhraseHash = getHash(res.Phrase)
		res.Postfix = parts[2]
		res.PostfixHash = getHash(res.Postfix)
	case 4:
		res.Phrase = parts[1]
		res.PhraseHash = getHash(res.Phrase)
		res.Postfix = parts[2]
		res.PostfixHash = getHash(res.Postfix)
		res.ExtraPhrase1 = parts[3]
		res.ExtraHash1 = getHash(res.ExtraPhrase1)
	case 5:
		res.Phrase = parts[1]
		res.PhraseHash = getHash(res.Phrase)
		res.Postfix = parts[2]
		res.PostfixHash = getHash(res.Postfix)
		res.ExtraPhrase1 = parts[3]
		res.ExtraHash1 = getHash(res.ExtraPhrase1)
		res.ExtraPhrase2 = parts[4]
		res.ExtraHash2 = getHash(res.ExtraPhrase2)
	case 6:
		res.Phrase = parts[1]
		res.PhraseHash = getHash(res.Phrase)
		res.Postfix = parts[2]
		res.PostfixHash = getHash(res.Postfix)
		res.ExtraPhrase1 = parts[3]
		res.ExtraHash1 = getHash(res.ExtraPhrase1)
		res.ExtraPhrase2 = parts[4]
		res.ExtraHash2 = getHash(res.ExtraPhrase2)
		res.ExtraPhrase3 = parts[5]
		res.ExtraHash3 = getHash(res.ExtraPhrase3)
	}
	return res
}

func getHash(text string) string {
	preHash := sha1.New()
	preHash.Write([]byte(text))
	res := hex.EncodeToString(preHash.Sum(nil))
	return res
}

func removeLetters(text string) string {
	var res string
	for i := range text {
		if text[i] >= 48 && text[i] <= 57 {
			res += string(text[i])
		}
	}
	return res
}
