package proto

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"regexp"
)

type AutoOutCallResponse struct {
	Status string `json:"status"`
	Voice  string `json:"voice,omitempty"`
	Phone  string `json:"phone"`
	Desc   string `json:"desc,omitempty"`
}
type AutoOutCallRequest struct {
	structures.AutoOutCallRequest
}

//DelayedTask structure that sends to RMQ for reCall
type DelayedRecallTask struct {
	Hash       string `json:"hash"`
	ReCallTime int64  `json:"recall_time"`
}

// Validate text
func (h *AutoOutCallRequest) ValidateSpeech() error {
	if h.Text == "" {
		return errors.New("Text to synth are empty")
	}
	return nil
}

func (h *AutoOutCallRequest) ValidatePhone() error {
	return ValidatePhone(h.Phone)
}

func ValidatePhone(phone string) error {
	if phone == "" {
		return errors.New("Phone number are empty")
	}
	if !isPhone(phone) {
		return errors.New("Phone number not valid")
	}
	return nil
}

func isPhone(phone string) bool {
	re := regexp.MustCompile(`^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$`)
	return re.MatchString(phone)
}
