package proto

type ActionRequest struct {
	Operator string `json:"oper"`
	Action   string `json:"action"`
}

type ActionResponse struct {
	Action string `json:"action"`
	Status string `json:"status"`
	Desc   string `json:"desc"`
}
