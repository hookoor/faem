package proto

type ConferenceRequest struct {
	Operator       string `json:"operator" query:"operator"`
	Number         string `json:"number" query:"number"`
	SecondOperator string `json:"second_operator" query:"second_operator"`
}

type ConferenceData struct {
	Channel        string // внешний канал
	Exten          string // номер конференции с 9
	ExtraChannel   string //
	Number         string // номер телефона который мы хотим подключить
	Operator       string // номер оператора
	ConfID         string // идентификатор конференции
	SecondOperator string // идентификатор второго оператора
}

type ResponseStruct struct {
	Status string `json:"status"`
}

type OperatorLine struct {
	Operator string `json:"status"`
	Line     string `json:"line"`
}
