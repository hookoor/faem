package proto

import (
	//"gitlab.com/faemproject/backend/faem/pkg/structures"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Call input data
type Call struct {
	AsterEvent
}

type AsterEvent struct {
	Status     string `json:"status" query:"status"`
	CallerID   string `json:"caller_id" query:"callerid"`
	Exten      string `json:"exten" query:"exten"`
	Uid        string `json:"uid" query:"uid"`
	OrderUUID  string `json:"order_uuid" query:"order_uuid"`
	Date       string `json:"date" query:"date"`
	ID         string `json:"id" query:"id"`
	Operator   string `json:"operator" query:"operator"`
	Record     string `json:"rec" query:"rec"`
	DialStatus string `json:"dial-status" query:"dial-status"`
	Channel    string `json:"channel" query:"channel"`
	ConfID     string `json:"channel" query:"confid"` //conference id
	Press      string `json:"press" query:"press"`    //IVR menu button
	CRMAction  string `json:"crmaction" query:"crmaction"`
}

func (ae *AsterEvent) ReplaceSpaceWithPlus() {
	ae.CallerID = strings.Replace(ae.CallerID, " ", "+", -1)
	ae.Record = strings.Replace(ae.Record, " ", "+", -1)

}

// ToCallEvent convert Call to CallEvent
//func (ae *AsterEvent) ToCallEvent() CallEvent {
//	var cEvent CallEvent
//	cEvent.Call = *ae
//	cEvent.DateTime = time.Now()
//	return cEvent
//}

// Validate input data
func (ae *AsterEvent) Validate() error {
	if ae.Status == "" {
		return errors.New("Empty status")
	}
	if ae.CallerID == "" {
		return errors.New("Unknows CallerID")
	}

	return nil
}

// CallOut returns answer (group name)
type GroupNameResponse struct {
	OperatorGroup string `json:"operator_group"`
	Message       string `json:"message"`
}

// CallEvent looks like Call struct but have Golang time format
// also in future we can modify it
type CallEvent struct {
	Call     AsterEvent
	DateTime time.Time `json:"datetime"`
}

type CRMCallEvent struct {
	Status    string    `json:"status" query:"status"`
	CallerID  string    `json:"caller_id" query:"callerid"`
	Exten     string    `json:"exten" query:"exten"`
	OrderUUID string    `json:"order_uuid" query:"order_uuid"`
	Date      string    `json:"date" query:"date"`
	ID        string    `json:"id" query:"id"`
	Record    string    `json:"rec" query:"rec"`
	CallTime  time.Time `json:"call_time"`
}
