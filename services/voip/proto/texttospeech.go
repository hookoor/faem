package proto

// TextPhrase input structure to synthes speec
// TODO: по хорошему это надо убрать, дублируются поля. Оставил что бы глубже ничего не менять
type TextPhrase struct {
	Text    string `json:"text"`
	Ssml    string `json:"ssml"`
	Voice   string `json:"voice"`
	Emotion string `json:"emotion"`
}

// SynthPhrase resulted phrase
type SynthPhrase struct {
	Url string `json:"url"`
}

type SplittedPhrase struct {
	Prefix       string
	PrefixHash   string
	PrefixURL    string
	Phrase       string
	PhraseHash   string
	PhraseURL    string
	Postfix      string
	PostfixHash  string
	PostfixURL   string
	ExtraPhrase1 string
	ExtraHash1   string
	ExtraURL1    string
	ExtraPhrase2 string
	ExtraHash2   string
	ExtraURL2    string
	ExtraPhrase3 string
	ExtraHash3   string
	ExtraURL3    string
}
