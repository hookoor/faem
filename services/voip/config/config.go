package config

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "VOIP" // TODO: change me
)

type Application struct {
	Env       string
	Port      string
	Secret    string
	LogLevel  string
	LogFormat string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

func (a *Application) validate() error {
	// if a.Addr == "" {
	// 	return errors.New("empty address provided for an http server to start on")
	// }
	return nil
}

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

func (d *Database) validate() error {
	if d.Host == "" {
		return errors.New("empty db host provided")
	}
	if d.Port == 0 {
		return errors.New("empty db port provided")
	}
	if d.User == "" {
		return errors.New("empty db user provided")
	}
	if d.Password == "" {
		return errors.New("empty db password provided")
	}
	if d.Db == "" {
		return errors.New("empty db name provided")
	}
	return nil
}

type Broker struct {
	UserURL         string
	UserCredits     string
	ExchangePrefix  string
	ExchangePostfix string
}

func (b *Broker) validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

// Voip настройки телефонии
type VoipCfg struct {
	//  Группа распределения по дефолту
	DefaultGroup string
	// Количество попыток дозвона
	ReCallingAttempts int
	// Интервал между перезвонами
	ReCallingInterval int
}

type TextToSpeech struct {
	BaseURL    string
	OAuth      string
	OAuthURL   string
	Timeout    time.Duration
	FolderID   string
	BucketName string
	ApiKey     string
}

func (t *TextToSpeech) validate() error {
	if t.OAuth == "" && t.ApiKey == "" {
		return errors.New("IAM token or ApiKey must be not empty")
	}
	if t.FolderID == "" {
		return errors.New("empty GC FolderID")
	}
	return nil
}

type Config struct {
	Application Application
	Database    Database
	Broker      Broker
	Voip        VoipCfg
	Tts         TextToSpeech
	Aster       AsteriskConfig
	Config      HandlerConfig
}

// HandlerConfig - конфиги доступные из handler-a
type HandlerConfig struct {
	Endpoints Endpoints
}

type AsteriskConfig struct {
	Username string
	Password string
	Url      string
}

func (a *AsteriskConfig) validate() error {
	if a.Url == "" {
		return errors.New("Empty Asterisk Base URL. Please provide...")
	}
	return nil
}
func (c *Config) validate() error {
	return multierr.Combine(
		c.Application.validate(),
		c.Database.validate(),
		c.Broker.validate(),
		c.Tts.validate(),
		c.Aster.validate(),
	)
}

// Endpoints -
type Endpoints struct {
	CRM          string
	CRMEndpoints struct {
		IsTheClientBlacklisted string
	}
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

func (c *Config) Print() {
	if c.Application.IsProduction() {
		return
	}
	inspected := *c // get a copy of an actual object
	// Hide sensitive data
	inspected.Application.Secret = ""
	inspected.Database.User = ""
	inspected.Database.Password = ""
	inspected.Broker.UserCredits = ""
	fmt.Printf("%+v\n", inspected)
}

// TODO: set the default and ENV values here
func setDefaults() {
	viper.SetDefault("application.env", "development")
	viper.SetDefault("application.loglevel", "debug")
	viper.SetDefault("application.logformat", "text")
	viper.SetDefault("application.port", "1326")

	viper.SetDefault("database.host", "localhost")
	viper.SetDefault("database.user", "postgres")
	viper.SetDefault("database.password", "pass")
	viper.SetDefault("database.db", "faem")
	viper.SetDefault("database.port", "5432")

	viper.SetDefault("broker.userurl", "localhost")
	viper.SetDefault("broker.usercredits", "login:pass")

	viper.SetDefault("voip.defaultgroup", "operator")
	viper.SetDefault("voip.recallingattempts", 5)
	viper.SetDefault("voip.recallinginterval", 7)

	viper.SetDefault("tts.baseurl", "https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize")
	viper.SetDefault("tts.timeout", 30*time.Second)
	viper.SetDefault("tts.oauth", "")
	viper.SetDefault("tts.oauthurl", "https://iam.api.cloud.yandex.net/iam/v1/tokens")
	viper.SetDefault("tts.folderID", "")
	viper.SetDefault("tts.iam", "")
	viper.SetDefault("tts.apikey", "")
	viper.SetDefault("tts.bucketname", "faem-staging-synth-speech")

	viper.SetDefault("aster.url", "")
	viper.SetDefault("aster.username", "")
	viper.SetDefault("aster.password", "")

	viper.SetDefault("config.endpoints.CRM", "http://faem-backend-crm.faem.svc.cluster.local/api/v2")
	viper.SetDefault("config.endpoints.CRMEndpoints.IsTheClientBlacklisted", "/clients/isblacklisted/")
}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}
