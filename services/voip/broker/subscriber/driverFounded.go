package subscriber

import (
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/speech"
)

const (
	channelDriverFounded = "driverFoundedChannel"
	//maxOrderStateChangesAllowed = 10
)

// вся эта консрукция по большому счету костыль на отловление обновление водителя в заказе
func (s *Subscriber) handleDriverFoundedMsg(msg amqp.Delivery) error {

	var newOrder structures.Order

	if err := s.Encoder.Decode(msg.Body, &newOrder); err != nil {
		return errors.Wrap(err, "failed to decode an newOrder request")
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling driver found msg",
		"orderUUID":  newOrder.UUID,
		"driverUUID": newOrder.Driver.UUID,
	})

	// Создан ли этот заказ в CRM, не уведомляем если через прилагу
	if newOrder.Source != "crm" {
		return nil
	}

	// Проверяем есть ли тег о назначении водителя
	state, ok := msg.Headers["tag"].(string)
	if !ok || state != "offer_accepted" {
		return nil
	}

	log.Info("Saving to DB")
	//state = "order_start"
	state = constants.OrderStateAccepted

	err := s.Handler.DB.SaveDriverData(&newOrder)
	if err != nil {
		return errors.Wrap(err, "failed to Save Driver Data from CRM")
	}

	sayText, err := speech.CarWillArrive(speech.SpeechData{
		CarColor:  newOrder.Driver.Color,
		CarNumber: newOrder.Driver.RegNumber,
		CarBrand:  newOrder.Driver.Car,
		ArriveIn:  newOrder.ArrivalTime,
	})

	if err != nil {
		return errors.Wrap(err, "failed to construct text")
	}

	////DUMMY CHECK +7 => 8
	//if newOrder.CallbackPhone[:2] == "+7" {
	//	newOrder.CallbackPhone = "8" + newOrder.CallbackPhone[2:]
	//}
	log.Infof("Prepare to synth and call: %s, %s, %v", newOrder.Driver.RegNumber, newOrder.Driver.Car, newOrder.ArrivalTime)
	err = s.Handler.SynthAndCall(proto.AutoOutCallRequest{
		AutoOutCallRequest: structures.AutoOutCallRequest{
			Text:       sayText,
			Phone:      newOrder.CallbackPhone,
			OrderUUID:  newOrder.UUID,
			OrderState: state,
			Type:       structures.OnTheWayAutoCall,
		}})
	if err != nil {
		return errors.Wrap(err, "failed to synthSpeech and call")
	}
	return nil
}

func (s *Subscriber) initDriverFounded() error {
	autoCallChannel, err := s.Rabbit.GetReceiver(channelDriverFounded)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelDriverFounded)
	}

	// Declare an exchange first
	err = autoCallChannel.ExchangeDeclare(
		rabbit.OrderExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := autoCallChannel.QueueDeclare(
		rabbit.VoipDriverFoundedQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = autoCallChannel.QueueBind(
		queue.Name, // queue name
		rabbit.OrderUpdateFromCRMKey,
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind [DriverAccepted] queue")
	}

	msgs, err := autoCallChannel.Consume(
		queue.Name,                       // queue
		rabbit.VoipDriverFoundedConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleDriverFounded(msgs) // handle incoming messages

	return nil
}

func (s *Subscriber) handleDriverFounded(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					logs.Eloger.Info("Get NewOrder - lets handle it")
					if err := s.handleDriverFoundedMsg(msg); err != nil {
						logs.Eloger.Errorf("Failed to handle driver found request: %v", err)
					}
				},
			))
		}
	}
}
