package subscriber

import (
	"context"
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	channelDriverClientCall = "driverToClientChannel"
	maxCallReuqestAllowed   = 10
)

func (s *Subscriber) makeDriverToClientCall(ctx context.Context, msg amqp.Delivery) error {

	var callRequest structures.NewConferenceData
	if err := s.Encoder.Decode(msg.Body, &callRequest); err != nil {
		return errors.Wrap(err, "failed to decode an driver to client call request")
	}
	if err := s.Handler.AsterClient.DriverClientCall(&callRequest); err != nil {
		return errors.Wrap(err, "failed to Synth speech and Call")
	}

	return nil

}

func (s *Subscriber) handleDriverClientCall(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxCallReuqestAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.makeDriverToClientCall(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle driver to client call: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initDriverToClientCall() error {
	autoCallChannel, err := s.Rabbit.GetReceiver(channelDriverClientCall)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelAutoCall)
	}

	// Declare an exchange first
	err = autoCallChannel.ExchangeDeclare(
		rabbit.VoipExchange, // name
		"topic",             // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := autoCallChannel.QueueDeclare(
		rabbit.VoipDriverClientQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = autoCallChannel.QueueBind(
		queue.Name,              // queue name
		rabbit.NewConferenceKey, // routing key
		rabbit.VoipExchange,     // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := autoCallChannel.Consume(
		queue.Name,                      // queue
		rabbit.VoipDriverClientConsumer, // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleDriverClientCall(msgs) // handle incoming messages
	return nil
}
