package subscriber

import (
	"time"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/speech"
)

const (
	channelOrdersStateUpdate = "orderStateUpdateChannel"
	//maxOrderStateChangesAllowed = 10
)

type orderState struct {
	OrderUUID  string    `json:"order_uuid"`
	DriverUUID string    `json:"driver_uuid"`
	State      string    `sql:",notnull" json:"state"`
	Comment    string    `json:"comment"`
	StartState time.Time `sql:",notnull" json:"start_state"`
}

//handleOrderStateUpdateMsg handling new order state message
func (s *Subscriber) handleOrderStateUpdateMsg(msg amqp.Delivery) error {
	var newState orderState
	if err := s.Encoder.Decode(msg.Body, &newState); err != nil {
		return errors.Wrap(err, "failed to decode an newOrderState request")
	}

	//проверяем буфер для нотификаций
	if _, ok := s.Handler.TickerNotificator[newState.OrderUUID]; ok {
		if (newState.State != constants.OrderStateConfirmation) && (newState.State != constants.OrderStateCreated) {
			delete(s.Handler.TickerNotificator, newState.State)
		}
	}

	updatedOrder, err := s.Handler.DB.UpdateOrderState(newState.OrderUUID, newState.State)
	if err != nil {
		return errors.Wrap(err, "failed to Update Order State")
	}

	// Проверяем бы ли обновлен заказ
	if updatedOrder.OrderUUID == "" {
		logs.Eloger.Infof("Order UUID=%s, not found to update state", newState.OrderUUID)
		return nil
	}
	//Проверяем есть ли тригеры для нотификаций
	sayText, callType, err := speech.TextToNotify(updatedOrder, updatedOrder.OrderState)
	if err != nil {
		return errors.Wrap(err, "error getting text to speech")
	}
	if sayText == "" {
		logs.Eloger.Info("Triggers to notify customers not found", newState.OrderUUID)
		return nil
	}
	if updatedOrder.CallBackPhone == "" {
		return errors.Wrap(err, "CallBackPhone are empty")
	}

	err = s.Handler.SynthAndCall(proto.AutoOutCallRequest{
		AutoOutCallRequest: structures.AutoOutCallRequest{
			Text:       sayText,
			Phone:      updatedOrder.CallBackPhone,
			OrderUUID:  updatedOrder.OrderUUID,
			OrderState: updatedOrder.OrderState,
			Type:       callType,
		}})
	if updatedOrder.CallBackPhone == "" {
		return errors.Wrap(err, "Error synthing or outcome calling")
	}

	return nil
}

func (s *Subscriber) initOrdersStateUpdate() error {
	autoCallChannel, err := s.Rabbit.GetReceiver(channelOrdersStateUpdate)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelNewOrders)
	}

	// Declare an exchange first
	err = autoCallChannel.ExchangeDeclare(
		rabbit.OrderExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := autoCallChannel.QueueDeclare(
		rabbit.VoipOrdersStateUpdate, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = autoCallChannel.QueueBind(
		queue.Name,           // queue name
		"state.*",            // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind [DriverAccepted] queue")
	}

	msgs, err := autoCallChannel.Consume(
		queue.Name,                     // queue
		rabbit.VoipOrderUpdateConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleOrderStateUpdateRequest(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleOrderStateUpdateRequest(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.handleOrderStateUpdateMsg(msg); err != nil {
						logs.Eloger.Errorf("Failed to handle Update Order request: %v", err)
					}
				},
			))
		}
	}
}
