//Здесь мы отслеживание новые заказы из CRM-ки
package subscriber

import (
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/handler"
	"time"
)

const (
	channelNewOrders    = "newOrdersChannel"
	notificatorTimerSec = 60
)

//handleNewOrderMsg сохраняет новый заказ
func (s *Subscriber) handleNewOrderMsg(msg amqp.Delivery) error {

	var newOrder structures.Order

	//// Если паблишер не crm игнорим
	//publisher, ok := msg.Headers["publisher"].(string)
	//if !ok || publisher != "crm" {
	//	return nil
	//}

	if err := s.Encoder.Decode(msg.Body, &newOrder); err != nil {
		return errors.Wrap(err, "failed to decode an newOrder request")
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "handling NewOrder msg",
		"orderUUID": newOrder.UUID,
	})

	if newOrder.ProductsData != nil {
		callTime := newOrder.CreatedAt.Add(notificatorTimerSec * time.Second)
		s.Handler.TickerNotificator[newOrder.UUID] = handler.NotificatorData{
			CallTime:    callTime,
			PhoneNumber: newOrder.ProductsData.StoreData.Phone,
		}
	}
	// Создан ли этот заказ в CRM, не уведомляем если через прилагу
	// проверяем на всякий случай, ибо эта проверка есть выше
	if newOrder.Source != "crm" {
		return nil
	}

	log.Infof("Saving to New Order to DB: UUID: %s", newOrder.UUID)
	err := s.Handler.DB.SaveCRMOrder(newOrder, constants.OrderStateCreated)
	if err != nil {
		return errors.Wrap(err, "failed to Save Order from CRM")
	}

	return nil
}

func (s *Subscriber) initNewOrders() error {
	autoCallChannel, err := s.Rabbit.GetReceiver(channelNewOrders)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelNewOrders)
	}

	// Declare an exchange first
	err = autoCallChannel.ExchangeDeclare(
		rabbit.OrderExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := autoCallChannel.QueueDeclare(
		rabbit.VoipNewOrders, // name
		true,                 // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = autoCallChannel.QueueBind(
		queue.Name, // queue name
		rabbit.NewKey,
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind [NewOrder] queue")
	}

	msgs, err := autoCallChannel.Consume(
		queue.Name,                  // queue
		rabbit.VoipNewOrderConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewOrderRequest(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewOrderRequest(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					logs.Eloger.Info("Get NewOrder Created - lets handle it")
					if err := s.handleNewOrderMsg(msg); err != nil {
						logs.Eloger.Errorf("Failed to handle New Order request: %v", err)
					}
				},
			))
		}
	}
}
