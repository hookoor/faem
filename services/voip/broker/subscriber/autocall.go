package subscriber

import (
	"context"
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

const (
	channelAutoCall             = "autoCallChannel"
	maxOrderStateChangesAllowed = 10
)

// Вызвать метод дозвона. Создать уникальный идентификатор звонка содержащий
// текст для синтеза, номер телефона и UUID заказа, а также хэш полученный от всего этого
// мы отправляем запрос и ждем результата, запись - сохраняем в  таблицу для отслеживания
// исходящих звонков. При получении успешного ответа, отмечаем запись - как успешную. Если
// пришла ошибка - отправляем себе задание на перезвон, - отложенное сообщение в брокер.
// Мы отмечаем запись звонка как неуспешное, инкрементируем номер попытки. Когда это задание придет

func (s *Subscriber) handleAutoCallMsg(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var call proto.AutoOutCallRequest
	if err := s.Encoder.Decode(msg.Body, &call); err != nil {
		return errors.Wrap(err, "failed to decode an autoCall request")
	}

	if err := s.Handler.SynthAndCall(call); err != nil {
		return errors.Wrap(err, "failed to Synth speech and Call")
	}

	return nil
}

func (s *Subscriber) handleAutoCallRequest(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.handleAutoCallMsg(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle auto call message: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initAutoCall() error {
	autoCallChannel, err := s.Rabbit.GetReceiver(channelAutoCall)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelAutoCall)
	}

	// Declare an exchange first
	err = autoCallChannel.ExchangeDeclare(
		rabbit.VoipExchange, // name
		"topic",             // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := autoCallChannel.QueueDeclare(
		rabbit.VoipAutoCallQueue, // name
		true,                     // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = autoCallChannel.QueueBind(
		queue.Name,          // queue name
		rabbit.AutoCallKey,  // routing key
		rabbit.VoipExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := autoCallChannel.Consume(
		queue.Name,                  // queue
		rabbit.VoipAutoCallConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleAutoCallRequest(msgs) // handle incoming messages
	return nil
}
