package subscriber

import (
	"fmt"
	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

const (
	channelDelayedRecallSub = "delayedRecallSub"
	//channelAutoCall             = "autoCallChannel"
	//maxOrderStateChangesAllowed = 10
)

func (s *Subscriber) handleAutoReCallMsg(msg amqp.Delivery) error {
	// начала надо проверить нужно ли вообще делать этот звонок? Например, проверить
	// статус этого звонка, может уже дозвонились или статус заказа поменялся.
	// Затем можно инкрементировать попытку и сделать новый звонок

	var reCallData proto.DelayedRecallTask
	if err := s.Encoder.Decode(msg.Body, &reCallData); err != nil {
		return errors.Wrap(err, "failed to decode an autoCall request")
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling ReCall request",
		"hash":  reCallData.Hash,
	})

	autoCall, err := s.Handler.DB.GetAutoCall(reCallData.Hash)
	if err != nil {
		return errors.Wrap(err, "failed to get reCall data from DB")
	}
	log.Infof("Autocall is found. Phone=%s", autoCall.Phone)
	log.Infof("Attempts = %v, maxAttempts = %v", autoCall.Attempt, s.Handler.VoipCfg.ReCallingAttempts)
	// Checking is reCall actual state. Error nil - actual, not nil - reason why not to do it
	err = s.Handler.DB.ReCallActualState(autoCall, s.Handler.VoipCfg.ReCallingAttempts)
	if err != nil {
		_ = s.Handler.DB.SetCancelStateAC(autoCall.Hash, fmt.Sprint(err))
		log.Infof("ReCall not actual. %s", err)
		return nil
	}

	//making outgoing call
	log.Infof("Call will be schedulled. Phone=%s", autoCall.Phone)

	err = s.Handler.CallerHandler(autoCall.Phone, autoCall.Hash, &autoCall.SplitPhrases, autoCall.Type)
	if err != nil {
		log.Errorf("failed to make Re-Call: %v", err)
		return nil
	}

	//saving call state
	err = s.Handler.DB.SetCallingStateAC(autoCall.Hash)
	if err != nil {
		log.Errorf("failed to make call: %v", err)
		return nil
	}

	return nil
}

func (s *Subscriber) handleAutoReCallRequest(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					//fmt.Printf("Got New delayed message - %s", msg)
					if err := s.handleAutoReCallMsg(msg); err != nil {
						logs.Eloger.Errorf("failed to handle auto ReCall message: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initDelayedRecallSub() error {
	channel, err := s.Rabbit.GetReceiver(channelDelayedRecallSub)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel %s", channelAutoCall)
	}

	// Declare an exchange first
	args := make(amqp.Table)
	args["x-delayed-type"] = "direct"
	err = channel.ExchangeDeclare(
		rabbit.DelayedReCallExchande, // name
		"x-delayed-message",          // type
		true,                         // durable
		false,                        // auto-deleted
		false,                        // internal
		false,                        // no-wait
		args,                         // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := channel.QueueDeclare(
		rabbit.VoipAutoReCallQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = channel.QueueBind(
		queue.Name,                   // queue name
		"",                           // routing key
		rabbit.DelayedReCallExchande, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := channel.Consume(
		queue.Name,                    // queue
		rabbit.VoipAutoReCallConsumer, // consumer
		true,                          // auto-ack
		false,                         // exclusive
		false,                         // no-local
		false,                         // no-wait
		nil,                           // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleAutoReCallRequest(msgs) // handle incoming messages
	return nil
}
