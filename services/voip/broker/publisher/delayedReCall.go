package publisher

import (
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"time"
)

const (
	channelDelayedReCall = "voipDelayedRecall"
)

//getReCallTime
func getReCallTime(delay int64) int64 {
	return time.Now().Add(time.Duration(delay) * time.Millisecond).Unix()
}
func (p *Publisher) SendDelayedReCall(call *proto.AsterEvent) error {
	var delayTime int64
	delayTime = 15000

	channel, err := p.Rabbit.GetSender(channelDelayedReCall)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelDelayedReCall)
	}
	dTask := proto.DelayedRecallTask{
		Hash:       call.Uid,
		ReCallTime: getReCallTime(delayTime),
	}
	//TODO: перенести delay time в настройки системы
	return p.Publish(channel, rabbit.DelayedReCallExchande, "", delayTime, dTask)
}

func (p *Publisher) initDelayedReCall() error {
	channel, err := p.Rabbit.GetSender(channelDelayedReCall)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelDelayedReCall)
	}
	args := make(amqp.Table)
	args["x-delayed-type"] = "direct"
	err = channel.ExchangeDeclare(
		rabbit.DelayedReCallExchande, // name
		"x-delayed-message",          // type

		true,  // durable
		false, // auto-deleted
		false, // internal
		false, // no-wait
		args,  // arguments
	)
	return errors.Wrapf(err, "failed to create an exchange %s", rabbit.DelayedReCallExchande)
}
