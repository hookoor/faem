package publisher

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (p *Publisher) SendOrderCancel(orderUUID, orderState string) error {
	var newState string
	channel, err := p.Rabbit.GetSender(channelEvent)

	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelEvent)
	}
	switch orderState {
	case constants.OrderStateFree:
		newState = constants.OrderStateDriverNotFound
	default:
		newState = constants.OrderStateCancelled
	}
	rkey := fmt.Sprintf("state.%s", constants.OrderStateDriverNotFound)
	return p.Publish(channel, rabbit.OrderExchange, rkey, 0, structures.OfferStates{
		State:     newState,
		OrderUUID: orderUUID,
	})
}

func (p *Publisher) initCancellEvents() error {

	channel, err := p.Rabbit.GetSender(channelEvent)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelEvent)
	}

	err = channel.ExchangeDeclare(
		rabbit.OrderExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	return errors.Wrapf(err, "failed to create an exchange %s", rabbit.OrderExchange)
}
