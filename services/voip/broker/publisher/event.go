package publisher

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"strings"
)

const (
	channelEvent = "voipEventsChannel"
)

func (p *Publisher) SendEvent(call *structures.TelephonyCall) error {
	channel, err := p.Rabbit.GetSender(channelEvent)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelEvent)
	}
	return p.Publish(channel, rabbit.VoipExchange, strings.ToLower(call.Status), 0, call)
}

func (p *Publisher) initVoipEvents() error {

	channel, err := p.Rabbit.GetSender(channelEvent)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel %s", channelEvent)
	}

	err = channel.ExchangeDeclare(
		rabbit.VoipExchange, // name
		"topic",             // type
		true,                // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	return errors.Wrapf(err, "failed to create an exchange %s", rabbit.VoipExchange)
}
