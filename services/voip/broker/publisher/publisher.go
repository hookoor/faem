package publisher

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"go.uber.org/multierr"
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/bootstrap/broker"
)

const (
	headerPublisher = "voip" // TODO: change me
)

type Publisher struct {
	Rabbit  *rabbit.Rabbit
	Encoder broker.Encoder

	wg sync.WaitGroup
}

func (p *Publisher) Init() error {
	// call all the initializers here, multierr package might be useful
	return multierr.Combine(
		p.initVoipEvents(),
		p.initDelayedReCall(),
		p.initCancellEvents(),
	)
}

func (p *Publisher) Wait() {
	p.wg.Wait()
}

// Publish payload to exchange with routing key and delay
func (p *Publisher) Publish(channel *rabbit.Channel, exchange, routingKey string, delay int64, payload interface{}) error {
	p.wg.Add(1)
	defer p.wg.Done()

	headers := make(amqp.Table)
	headers["publisher"] = headerPublisher

	if delay != 0 {
		headers["x-delay"] = delay
	}

	body, err := p.Encoder.Encode(payload)
	if err != nil {
		return errors.Wrap(err, "failed to encode the message")
	}

	err = channel.Publish(
		exchange,   // exchange
		routingKey, // routing key
		false,      // mandatory
		false,      // immediate
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         body,
			Headers:      headers,
			Timestamp:    time.Now(),
			DeliveryMode: amqp.Persistent,
		})
	if err != nil {
		return errors.Wrapf(err, "failed to send a message, exchange = %s, routing key = %s", exchange, routingKey)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":      "Send JSON to RabbitMQ",
		"exchange":   exchange,
		"routingKey": routingKey,
	}).Debug("Sent to RabbitMQ")
	return nil
}

//func (p *Publisher) PublishDelayed(channel *rabbit.Channel, exchange, routingKey string, delay int64, payload interface{}) error {
//	p.wg.Add(1)
//	defer p.wg.Done()
//
//	headers := make(amqp.Table)
//	headers["publisher"] = headerPublisher
//
//	if delay != 0 {
//		headers["x-delay"] = delay
//	}
//
//	body, err := p.Encoder.Encode(payload)
//	if err != nil {
//		return errors.Wrap(err, "failed to encode the message")
//	}
//
//	err = channel.Publish(
//		exchange,   // exchange
//		routingKey, // routing key
//		false,      // mandatory
//		false,      // immediate
//		amqp.Publishing{
//			ContentType: "application/json",
//			Body:        body,
//			Headers:     headers,
//			Timestamp:   time.Now(),
//		}) // TODO: use persistent mode if required
//	if err != nil {
//		return errors.Wrapf(err, "failed to send a message, exchange = %s, routing key = %s", exchange, routingKey)
//	}
//
//	logs.Eloger.WithFields(logrus.Fields{
//		"event": "Delayed Send to RabbitMQ",
//		"value": fmt.Sprintf("exchange = %s; key = %s", exchange, routingKey),
//	}).Debug("Sent to RabbitMQ (delayed)")
//	return nil
//}
