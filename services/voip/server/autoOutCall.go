package server

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// AutoOutCall handler request to make autocall with synthesized speech
func (r *Rest) SynthAndUpload(c echo.Context) error {
	//var autoCall proto.AutoOutCallRequest
	//if err := c.Bind(&autoCall); err != nil {
	//	logs.LoggerForContext(c.Request().Context()).
	//		WithFields(logrus.Fields{
	//			"event":  "binding autoCall data",
	//			"in":     autoCall,
	//			"reason": errBind,
	//		}).
	//		Error(err)
	//	res := logs.OutputRestError(errBind, err)
	//	return c.JSON(http.StatusBadRequest, res)
	//}
	//
	//speechURL, err := r.Handler.SpeechSynth(c.Request().Context(), autoCall.Text)
	//if err != nil {
	//	logs.LoggerForContext(c.Request().Context()).
	//		WithFields(logrus.Fields{
	//			"event": "handling auto call",
	//			"in":    autoCall,
	//		}).
	//		Error(err) // you may add additional fields here
	//	res := logs.OutputRestError(errHandling, err)
	//	return c.JSON(http.StatusBadRequest, res)
	//}
	//out := proto.AutoOutCallResponse{
	//	Status: "Ok. Call schedulled.",
	//	Voice:  speechURL,
	//	Phone:  autoCall.Phone,
	//}
	//return c.JSON(http.StatusOK, out)
	return c.JSON(http.StatusBadRequest, "Manual Temporary Unavailable ")
}
