package server

import (
	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/voip/handler"
)

const (
	apiPrefix = "/api/v2"
)

type Rest struct {
	Router  *echo.Echo
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route() {
	web.UseHealthCheck(r.Router)

	g := r.Router.Group(apiPrefix)
	g.GET("/call", r.Call)
	g.POST("/synthandupload", r.SynthAndUpload)
	g.GET("/conference", r.Conference)
	//TODO: ALARM открыто наружу
	g.GET("/last_line", r.LastLine)
	g.POST("/oper_action", r.OperAction)
}
