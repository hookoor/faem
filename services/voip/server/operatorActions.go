package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// Call binding data from request
func (r *Rest) OperAction(c echo.Context) error {

	var operAction proto.ActionRequest
	if err := c.Bind(&operAction); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "handling operator action",
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.OperatorAction(operAction); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "handling operator action",
				"reason": "error in hangling request",
			}).Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}
