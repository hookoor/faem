package server

import (
	"errors"
	"net/http"

	"github.com/labstack/echo/v4"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// Call binding data from request
func (r *Rest) LastLine(c echo.Context) error {
	operator := c.QueryParam("operator")

	if operator == "" {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event": "getting current line",
			}).Error("operator line is empty")
		res := logs.OutputRestError(errBind, errors.New("can't provide line number, operator field is empty"))
		return c.JSON(http.StatusBadRequest, res)
	}

	line, err := r.Handler.DB.GetOperatorLine(operator)
	if err != nil {
		res := logs.OutputRestError("error getting operator line", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, proto.OperatorLine{Operator: operator, Line: line})
}
