package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// Call binding data from request
func (r *Rest) Call(c echo.Context) error {
	var call proto.AsterEvent
	if err := c.Bind(&call); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "binding call data",
				"in":     call,
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	call.ReplaceSpaceWithPlus()
	out, err := r.Handler.Call(c.Request().Context(), &call)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			Error(err) // you may add additional fields here
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}
