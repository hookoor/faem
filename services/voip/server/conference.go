package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// Call binding data from request
func (r *Rest) Conference(c echo.Context) error {
	var confRequest proto.ConferenceRequest

	if err := c.Bind(&confRequest); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "conference request",
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	//валидируем полученный номер
	if err := validatePhone(confRequest.Number); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "conference request",
				"reason": "error validating phone",
			}).Error(err) // you may add additional fields here
		res := logs.OutputRestError("Wrong phone Number", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	//получаем данные по каналу
	confData, err := r.Handler.GetCallChannel(c.Request().Context(), &confRequest)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "conference request",
				"reason": "open channel for operator not found",
			}).Error(err) // you may add additional fields here
		res := logs.OutputRestError("Can't find call for operator", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event":    "conference request",
			"operator": confData.Operator,
			"number":   confData.Number,
		}).Info("Trying to create conference call")
	//создаем конференцию
	go r.Handler.AsterClient.MakeConference(&confData)
	return c.JSON(http.StatusOK, proto.ResponseStruct{Status: "Запрос на создание конференц-звонка принят"})
}

func validatePhone(number string) error {
	//TODO: написать валидацию телефонного номера
	return nil
}
