package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"time"
)

//CRMOrder ... that store Order data
type CRMOrder struct {
	tableName     struct{}         `sql:"crm_orders"`
	ID            int64            `json:"id"`
	OrderUUID     string           `json:"order_uuid"`
	ArrivalTime   int64            `json:"arrival_time"`
	CallBackPhone string           `json:"callback_phone" sql:"callback_phone"`
	Car           string           `json:"car"`
	OrderState    string           `json:"order_state"`
	Updated_at    time.Time        `json:"updated_at"`
	Created_at    time.Time        `json:"created_at"`
	RawOrder      structures.Order `json:"raw_order"`
}
