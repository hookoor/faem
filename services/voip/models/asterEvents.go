package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/models/voip"
)

// Client table in PG
type AsterEvent struct {
	tableName struct{} `sql:"aster_events"`
	voip.AsterEvent
}
