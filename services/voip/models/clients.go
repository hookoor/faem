package models

// Client table in PG
type Client struct {
	tableName struct{} `sql:"clients"`
	ID        int64    `json:"id"`
	Phone     string   `json:"phone"`
	CrmUUID   string   `json:"crm_clientUUID"`
	Group     int      `json:"group"`
}
