package models

// OperatorGroups table in PG
type OperatorGroups struct {
	tableName struct{} `sql:"operator_groups"`
	ID        int64    `json:"id"`
	Name      string   `json:"name"`
	Desc      string   `json:"desc"`
}
