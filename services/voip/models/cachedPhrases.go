package models

import "time"

// Client table in PG
type CachedPhrases struct {
	tableName struct{}  `sql:"cached_phrases"`
	Hash      string    `json:"hash"`
	Phrase    string    `json:"phrase"`
	Url       string    `json:"url"`
	CreatedAt time.Time `json:"created_at"`
}
