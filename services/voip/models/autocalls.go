package models

import (
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"time"
)

// Client table in PG
type AutoCall struct {
	tableName    struct{}                 `sql:"autocalls"`
	ID           int64                    `json:"id"`
	Hash         string                   `json:"hash"`
	Payload      proto.AutoOutCallRequest `json:"payload"`
	Phone        string                   `json:"phone"`
	OrderUUID    string                   `json:"order_uuid"`
	OrderState   string                   `json:"order_state"`
	Attempt      int                      `json:"attempt"`
	State        string                   `json:"state"`
	SpeechURL    string                   `json:"speech_url"`
	Comment      string                   `json:"comment"`
	SplitPhrases proto.SplittedPhrase     `json:"split_phrases"`
	SuccessfulAt int64                    `json:"successful_at"`
	Type         string                   `json:"type"`
	CreatedAt    time.Time                `json:"created_at"`
	UpdatedAt    time.Time                `json:"created_at"`
}
