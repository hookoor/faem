package repository

import (
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

const (
	createdState    = "created"
	callingState    = "calling"
	missedState     = "missed"
	schedulledState = "schedulled"
	okState         = "ok"
	cancelledState  = "cancelled"
)

var (
	workStates   = []string{createdState, callingState, missedState, schedulledState}
	finishStates = []string{okState, cancelledState}
	allStates    = []string{okState, cancelledState, createdState, callingState, missedState, schedulledState}
)

func callHash(call *models.AutoCall) string {
	toHash := call.Phone + call.OrderUUID + call.OrderState + call.SplitPhrases.PrefixHash + call.CreatedAt.String()
	preHash := sha1.New()
	preHash.Write([]byte(toHash))
	return hex.EncodeToString(preHash.Sum(nil))
}

// CreateAutoCall calculate hash of call and save to DB
func (p *Pg) CreateAutoCall(call *proto.AutoOutCallRequest, splitedPhrases *proto.SplittedPhrase) (string, error) {
	autocall := models.AutoCall{
		Payload:      *call,
		Phone:        call.Phone,
		OrderUUID:    call.OrderUUID,
		OrderState:   call.OrderState,
		Attempt:      0,
		SplitPhrases: *splitedPhrases,
		CreatedAt:    time.Now(),
	}
	switch string(call.Type) {
	case structures.LongDistibNotificAutoCall:
		autocall.Type = string(structures.LongDistibNotificAutoCall)
		autocall.State = constants.OrderStateFree
	case structures.OnTheWayAutoCall:
		autocall.Type = structures.OnTheWayAutoCall
		autocall.State = constants.OrderStateStarted
	case structures.OnplaceAutoCall:
		autocall.Type = structures.OnplaceAutoCall
		autocall.State = constants.OrderStateOnPlace
	default:
		autocall.Type = string(call.Type)
		autocall.State = createdState
	}

	hash := callHash(&autocall)
	autocall.Hash = hash
	err := p.Db.Insert(&autocall)
	if err != nil {
		return "", errors.Wrap(err, "failed to insert a record")
	}
	return hash, nil
}

// SetSchedulledStateAC setting calling state
func (p *Pg) SetSchedulledStateAC(hash string) error {
	ac := models.AutoCall{
		Hash: hash,
	}
	_, err := p.Db.Model(&ac).
		Where("hash = ?", hash).
		Set("state = ?", schedulledState).
		Set("updated_at = ?", time.Now()).
		UpdateNotNull()
	if err != nil {
		return errors.Wrap(err, "error updating order state to schedulled")
	}
	return nil
}

// SetCallingStateAC setting calling state
func (p *Pg) SetCallingStateAC(hash string) error {
	ac := models.AutoCall{
		Hash: hash,
	}
	_, err := p.Db.Model(&ac).
		Where("hash = ?", hash).
		Set("state = ?", callingState).
		Set("attempt = attempt + 1").
		Set("updated_at = ?", time.Now()).
		UpdateNotNull()
	if err != nil {
		return errors.Wrap(err, "error updating order state")
	}
	return nil
}

// GetOrderUUIDByHash returns order uuid by ac hash
func (p *Pg) GetOrderUUIDByHash(hash string) (string, error) {
	var ac models.AutoCall
	err := p.Db.Model(&ac).
		Where("hash = ?", hash).
		Select()
	return ac.OrderUUID, err
}

// SetCallingStateAC setts cancel state for auto call
func (p *Pg) SetCancelStateAC(hash, comment string) error {
	ac := models.AutoCall{
		Hash: hash,
	}
	_, err := p.Db.Model(&ac).
		Where("hash = ?", hash).
		Set("state = ?", cancelledState).
		Set("updated_at = ?", time.Now()).
		Set("comment = ?", comment).
		UpdateNotNull()
	if err != nil {
		return errors.Wrap(err, "error updating order state")
	}
	return nil
}

// ReCallActualState checking is state of autoCall is actual for Re-Calling
func (p *Pg) ReCallActualState(autoCall models.AutoCall, maxAttempts int) error {
	currentState, err := p.orderInState(autoCall.OrderUUID)
	if err != nil {
		return errors.Wrap(err, "error checking current order state")
	}
	switch autoCall.Type {
	case structures.LongDistibNotificAutoCall:
		if currentState != constants.OrderStateFree {
			errPrint := fmt.Sprintf("finding driver status changed - %s != %s", currentState, constants.OrderStateFree)
			return errors.New(errPrint)
		}
		if autoCall.Attempt > 3 {
			return errors.New("Max calling attemts are reached.")
		}
	case structures.OnTheWayAutoCall:
		errPrint := fmt.Sprintf("finding driver status changed - %s != %s || %s", currentState, constants.OrderStateAccepted, constants.OrderStateStarted)
		if currentState != constants.OrderStateAccepted && currentState != constants.OrderStateStarted {
			return errors.New(errPrint)
		}
	case structures.OnplaceAutoCall:
		errPrint := fmt.Sprintf("onplace autocall type - %s != %s", currentState, constants.OrderStateOnPlace)
		if currentState != constants.OrderStateOnPlace {
			return errors.New(errPrint)
		}

	default:
		errPrint := fmt.Sprintf("default state checker - %s != %s", currentState, autoCall.OrderState)
		if currentState != autoCall.OrderState {
			return errors.New(errPrint)
		}
	}
	if autoCall.State != schedulledState {
		return errors.Errorf("Inactual state for ReCalling. State is %s", autoCall.State)
	}
	if autoCall.Attempt > maxAttempts {
		return errors.New("Max calling attemts are reached.")
	}
	return nil
}

// orderInState checking does order are in right state to make call. During autocall delay order state can be updated
func (p *Pg) orderInState(orderUUID string) (string, error) {
	orderState := models.CRMOrder{
		OrderUUID: orderUUID,
	}

	err := p.Db.Model(&orderState).Column("order_state").
		Where("order_uuid = ?order_uuid").Limit(1).Select()
	if err != nil {
		return "", err
	}
	return orderState.OrderState, nil
}
