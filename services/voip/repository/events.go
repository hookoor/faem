package repository

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

const (
	hangupAsterStatus    = "hangup"
	startcallAsterStatus = "startcall"
	answerAsterStatus    = "answer"
	playendAsterStatus   = "playend"
	cancelledAsterStatus = "cancelled"
)

// CheckAutoCallTriggers returns auto action, model, or error
func (p *Pg) CheckAutoCallTriggers(call *proto.AsterEvent) (string, models.AutoCall, error) {

	//валидация, является или автозвонком
	if call.Uid == "" {
		return "ignore", models.AutoCall{}, nil
	}

	//берем информацию о автозвонке
	autocall, err := p.GetAutoCall(call.Uid)
	if err != nil {
		return "", autocall, errors.Wrap(err, "error getting autocall trigger entry")
	}

	//игнорируем если автозвонок уже завершен
	if autocall.State == okState || autocall.State == cancelledState {
		return "ignore", autocall, nil
	}

	//проверяем текущий статус звонка
	callState, err := p.AutoCallState(call.Uid)
	if err != nil {
		return "", autocall, errors.Wrap(err, "error getting autocall latest state")
	}

	switch callState {
	case answerAsterStatus:
		//если ответили - значит все ок
		return "setOk", autocall, err
	case cancelledAsterStatus:
		//если не ответили - запланируем презвон
		return "schedule", autocall, nil
	}

	//ошибка если дошли до этого места
	errStr := fmt.Sprintf("Unknown AutoCall state. CallState = %v, AutoCall = %v", callState, autocall.State)
	return "ignore", autocall, errors.New(errStr)
}

func (p *Pg) AutoCallState(uid string) (string, error) {
	var uidCalls []models.AsterEvent
	err := p.Db.Model(&uidCalls).
		Where("uid = ?", uid).Select()
	if err != nil {
		return "", err
	}
	for _, val := range uidCalls {
		if val.Status == answerAsterStatus {
			return answerAsterStatus, nil
		}
	}
	return cancelledAsterStatus, nil
}

func (p *Pg) GetAutoCall(hash string) (models.AutoCall, error) {
	var autocall models.AutoCall
	err := p.Db.Model(&autocall).
		Where("hash = ?", hash).
		Select()
	return autocall, err
}

func (p *Pg) SaveOKStatusAC(call *proto.AsterEvent, autoCall models.AutoCall) error {
	autoCall.State = okState
	autoCall.SuccessfulAt = time.Now().Unix()
	comment := fmt.Sprintf("Call, ok because get status:%s, requestID: %v", call.Status, call.ID)
	autoCall.Comment = comment
	autoCall.UpdatedAt = time.Now()
	err := p.Db.Update(&autoCall)
	if err != nil {
		return err
	}
	return nil
}

func (p *Pg) SaveCall(call *proto.AsterEvent) error {
	var event models.AsterEvent
	event.CallerID = call.ID
	event.CallID = call.ID
	event.CallerID = call.CallerID
	event.Status = call.Status
	event.Uid = call.Uid
	event.Operator = call.Exten
	event.Record = call.Record
	event.DialStatus = call.DialStatus
	event.Channel = call.Channel
	event.Exten = call.Exten
	event.ConfID = call.ConfID
	event.CRMAction = call.CRMAction

	layout := "2006-01-02-15:04:05 GMT-0700"
	t, err := time.Parse(layout, call.Date+" GMT+0300")
	if err == nil {
		event.CallTimestamp = t
	}

	err = p.Db.Insert(&event)
	if err != nil {
		return err
	}

	return nil
}

func (p *Pg) CallEventsByID(id string) ([]models.AsterEvent, error) {
	var events []models.AsterEvent
	err := p.Db.Model(&events).
		Where("call_id = ?", id).
		Order("id DESC").
		Select()
	if err != nil {
		return events, err
	}
	return events, nil
}

func (p *Pg) CallEventsByUID(uid string) ([]models.AsterEvent, error) {
	var events []models.AsterEvent
	err := p.Db.Model(&events).
		Where("uid = ?", uid).
		Order("id DESC").
		Select()
	if err != nil {
		return events, err
	}
	return events, nil
}
