package repository

import (
	"github.com/go-pg/pg/orm"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
)

func (p *Pg) OrderByPhone(phone string) (models.CRMOrder, error) {

	var crmOrder models.CRMOrder
	var phone2 string
	if phone[:2] == "+7" {
		phone2 = "8" + phone[2:]
	} else {
		phone2 = "+7" + phone[1:]
	}

	err := p.Db.Model(&crmOrder).
		WhereGroup(func(q *orm.Query) (*orm.Query, error) {
			q = q.WhereOr("callback_phone = ?", phone).
				WhereOr("callback_phone = ?", phone2)
			return q, nil
		}).
		Where("order_state <> ?", constants.OrderStateFinished).
		Where("order_state <> ?", constants.OrderStateCancelled).
		Where("order_state <> ?", constants.OrderStateDriverNotFound).
		Where("created_at > now()-interval '1h'").
		Limit(1).Select()

	if err != nil {
		return models.CRMOrder{}, errors.Wrap(err, "failed to get current order")
	}

	return crmOrder, nil
}
