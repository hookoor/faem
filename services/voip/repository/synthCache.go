package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

//GetCachedPhrases запрашивает из базы хэши фраз
func (p *Pg) CachedPhrases(phrase *proto.SplittedPhrase) error {

	var returnError error

	resp, err := p.requestUrl(phrase.PrefixHash)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.PrefixHash)
	}
	phrase.PrefixURL = resp

	resp, err = p.requestUrl(phrase.PhraseHash)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.PhraseHash)
	}
	phrase.PhraseURL = resp

	resp, err = p.requestUrl(phrase.PostfixHash)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.PostfixHash)
	}
	phrase.PostfixURL = resp

	resp, err = p.requestUrl(phrase.ExtraHash1)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.ExtraHash1)
	}
	phrase.ExtraURL1 = resp

	resp, err = p.requestUrl(phrase.ExtraHash2)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.ExtraHash2)
	}
	phrase.ExtraURL2 = resp

	resp, err = p.requestUrl(phrase.ExtraHash3)
	if err != nil {
		returnError = errors.Wrapf(returnError, "error getting hash %v", phrase.ExtraHash3)
	}
	phrase.ExtraURL3 = resp

	return returnError
}

func (p *Pg) requestUrl(hash string) (string, error) {
	var cachePhrases models.CachedPhrases
	if hash == "" {
		return "", nil
	}
	err := p.Db.Model(&cachePhrases).
		Where("hash = ?", hash).
		Limit(1).
		Select()
	if err != nil {
		return cachePhrases.Phrase, err
	}
	return cachePhrases.Url, nil
}

func (p *Pg) SavePhraseHash(phrase, hash, url string) error {
	cachePhrases := models.CachedPhrases{
		Hash:   hash,
		Phrase: phrase,
		Url:    url,
	}

	err := p.Db.Insert(&cachePhrases)
	if err != nil {
		return err
	}
	return nil
}
