package repository

import (
	"fmt"

	"github.com/go-pg/pg"
)

type Pg struct {
	Db *pg.DB
}

// DBLogger logger interface
type DBLogger struct{}

// BeforeQuery for implementing interface
func (d DBLogger) BeforeQuery(q *pg.QueryEvent) {
}

// AfterQuery output PG SQL just for debugging
func (d DBLogger) AfterQuery(q *pg.QueryEvent) {
	fmt.Println(q.FormattedQuery())
}
