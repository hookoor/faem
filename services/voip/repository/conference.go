package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
)

func (p *Pg) GetOperatorChannel(operator string) (string, string, string, error) {

	var asterEvent models.AsterEvent
	err := p.Db.Model(&asterEvent).
		Where("caller_id = ? AND status='answer'", operator).
		Order("id DESC").
		Limit(1).
		Select()
	if err != nil {
		return "", "", "", errors.Wrap(err, "error getting operator channel")
	}

	var asterEvent2 models.AsterEvent
	err = p.Db.Model(&asterEvent2).
		Where("call_id = ? AND status='incoming'", asterEvent.CallID).
		Limit(1).
		Select()
	if err != nil {
		return "", "", "", errors.Wrap(err, "error getting caller channel")
	}

	return asterEvent.Channel, asterEvent2.Channel, asterEvent.CallID, nil
}
