package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// GetClientGroup returns dispatch group for number
func (p *Pg) GetClientGroup(call *proto.AsterEvent) (string, error) {
	var (
		group models.OperatorGroups
	)

	err := p.Db.Model(&group).
		ColumnExpr("name").
		Join("JOIN clients cl ON operator_groups.id=cl.group").
		Where("cl.phone = ?", call.CallerID).
		Limit(1).
		Select()
	if err != nil {
		return "", errors.Wrap(err, "error getting client group")
	}
	return group.Name, nil
}
