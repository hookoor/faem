package repository

import (
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
)

func (p *Pg) SaveCRMOrder(order structures.Order, state string) error {

	crmOrder := models.CRMOrder{
		OrderUUID:  order.UUID,
		RawOrder:   order,
		OrderState: state,
		Updated_at: time.Now(),
		Created_at: time.Now(),
	}
	if order.CallbackPhone == "" {
		crmOrder.CallBackPhone = crmOrder.RawOrder.Client.MainPhone
	} else {
		crmOrder.CallBackPhone = order.CallbackPhone
	}

	err := p.Db.Insert(&crmOrder)
	if err != nil {
		return errors.Wrap(err, "failed to insert a record")
	}

	return nil
}

func (p *Pg) UpdateOrderState(UUID, state string) (models.CRMOrder, error) {
	newOrderState := models.CRMOrder{
		OrderUUID:  UUID,
		OrderState: state,
	}
	exist, err := p.Db.Model(&newOrderState).
		Where("order_uuid = ?order_uuid").
		Exists()
	if err != nil {
		return models.CRMOrder{}, errors.Wrap(err, "failed to check existing order")
	}

	if !exist {
		return models.CRMOrder{}, nil
	}

	_, err = p.Db.Model(&newOrderState).
		Set("order_state = ?order_state").
		Where("order_uuid = ?order_uuid").Returning("*").
		UpdateNotNull()

	if err != nil {
		return models.CRMOrder{}, errors.Wrapf(err, "failed to update order state UUID = %s", UUID)
	}

	return newOrderState, nil
}

func (p *Pg) SaveDriverData(order *structures.Order) error {
	carDesc := order.Driver.Color + " " + order.Driver.Car + " " + order.Driver.RegNumber
	crmOrder := models.CRMOrder{
		Car:         carDesc,
		RawOrder:    *order,
		OrderState:  constants.OrderStateAccepted,
		ArrivalTime: order.ArrivalTime,
		Updated_at:  time.Now(),
	}
	_, err := p.Db.Model(&crmOrder).
		Where("order_uuid = ?", order.UUID).
		UpdateNotNull()

	if err != nil {
		return err
	}

	return nil

}
