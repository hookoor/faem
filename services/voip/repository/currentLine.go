package repository

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
)

func (p *Pg) GetOperatorLine(operator string) (string, error) {

	var operatorChannel models.AsterEvent
	err := p.Db.Model(&operatorChannel).
		Where("caller_id = ? AND status='answer'", operator).
		Order("id DESC").
		Limit(1).
		Select()
	if err != nil {
		return "", errors.Wrap(err, "error getting operator channel")
	}

	var eventLine models.AsterEvent
	err = p.Db.Model(&eventLine).
		Where("call_id = ? AND status='incoming'", operatorChannel.CallID).
		Limit(1).
		Select()
	if err != nil {
		return "", errors.Wrap(err, "error getting incoming number")
	}

	return eventLine.Exten, nil
}
