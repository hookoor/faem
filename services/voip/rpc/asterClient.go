package rpc

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/voip/digest"
)

const (
	httpTimeout = time.Second * 60
)

type (
	AsterClient struct {
		*http.Client
		Config AsterConfig
	}
	AsterConfig struct {
		AsterURL string
		Username string
		Password string
	}
)

func NewAsterClient(config AsterConfig) *AsterClient {
	t := digest.Transport{
		Username: config.Username,
		Password: config.Password,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}
	httpClient := http.Client{
		Transport: &t,
		Timeout:   httpTimeout,
	}
	return &AsterClient{
		Config: config,
		Client: &httpClient,
	}
}

func (a *AsterClient) Logoff() error {
	req, err := http.NewRequest("GET", a.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "creating aster logoff request failed")
	}

	q := req.URL.Query()
	q.Add("action", "Logoff")
	req.URL.RawQuery = q.Encode()

	resp, err := a.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to logoff from aster")
	}
	resp.Body.Close()
	return nil
}
