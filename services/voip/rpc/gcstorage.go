package rpc

import (
	"bytes"
	"cloud.google.com/go/storage"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"io"
)

func (s *SynthClient) UploadToCloud(byteData *[]byte, fname ...string) (string, error) {
	//TODO Переделать на функцию пэкеджа общую для всех случаев
	ctx := context.Background()
	client, err := storage.NewClient(ctx)
	if err != nil {
		fmt.Printf("ERRROR creating client %s", err)
	}
	r := bytes.NewReader(*byteData)
	var fileName string
	if (len(fname) > 0) && (fname[0] != "") {
		fileName = fmt.Sprintf("%s.%s", fname[0], fileExt)
	} else {
		fileName = randFileName()
	}
	wc := client.Bucket(s.Config.BucketName).Object(fileName).NewWriter(ctx)

	if _, err = io.Copy(wc, r); err != nil {
		return "", errors.Wrap(err, "Cant copy to bucket")
	}
	if err := wc.Close(); err != nil {
		return "", errors.Wrap(err, "Cant close bucket")
	}
	resp := fmt.Sprintf("%s/%s/%s", buckerURL, s.Config.BucketName, fileName)
	return resp, nil
}
