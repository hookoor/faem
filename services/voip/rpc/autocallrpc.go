package rpc

import (
	"fmt"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

//MakeCall(phone, uuid string, sp *proto.SplittedPhrase)

func (a *AsterClient) MakeCall(phone, uuid string, sp *proto.SplittedPhrase, callType ...string) error {

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Sending to Aster Call command",
		"phone": phone,
	}).Info("Preparing data to make call")

	//DUMMY CHECK +7 => 8
	if len(phone) > 0 && phone[:2] == "+7" {
		phone = "8" + phone[2:]
	}
	req, err := http.NewRequest("GET", a.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster New Request failed")
	}
	q := req.URL.Query()

	if len(callType) > 0 {
		switch callType[0] {
		case structures.LongDistibNotificAutoCall:
			q.Add("Context", "callout-carnotfound")
			q.Add("Variable", fmt.Sprintf("svar1=%s", sp.PrefixHash))
			q.Add("Variable", fmt.Sprintf("svar2=%s", sp.PhraseHash))
		case structures.OnTheWayAutoCall:
			q.Add("Context", "callout-car-found")
			q.Add("Variable", fmt.Sprintf("svar1=%s", cutHttp(sp.PrefixHash)))
			q.Add("Variable", fmt.Sprintf("svar2=%s", cutHttp(sp.PhraseHash)))
			q.Add("Variable", fmt.Sprintf("svar3=%s", cutHttp(sp.PostfixHash)))
			q.Add("Variable", fmt.Sprintf("svar4=%s", cutHttp(sp.ExtraHash1)))
			q.Add("Variable", fmt.Sprintf("svar5=%s", cutHttp(sp.ExtraHash2)))
			q.Add("Variable", fmt.Sprintf("svar6=%s", cutHttp(sp.ExtraHash3)))
			if len(callType) > 1 {
				q.Add("Variable", fmt.Sprintf("ivar5=%s", callType[1]))
			}
		case structures.OnplaceAutoCall:
			q.Add("Context", "callout-car-waiting")
			q.Add("Variable", fmt.Sprintf("svar1=%s", cutHttp(sp.PrefixHash)))
			q.Add("Variable", fmt.Sprintf("svar2=%s", cutHttp(sp.PhraseHash)))
			q.Add("Variable", fmt.Sprintf("svar3=%s", cutHttp(sp.PostfixHash)))
			q.Add("Variable", fmt.Sprintf("svar4=%s", cutHttp(sp.ExtraHash1)))
			q.Add("Variable", fmt.Sprintf("svar5=%s", cutHttp(sp.ExtraHash2)))
			if len(callType) > 1 {
				q.Add("Variable", fmt.Sprintf("ivar5=%s", callType[1]))
			}
		default:
			q.Add("Context", "robot")
			q.Add("Variable", fmt.Sprintf("rec=%s", cutHttp(sp.PhraseURL)))
			q.Add("Variable", fmt.Sprintf("recname=%s", sp.PhraseHash))
			q.Add("Variable", fmt.Sprintf("recbeg=%s", cutHttp(sp.PrefixURL)))
			q.Add("Variable", fmt.Sprintf("recnamebeg=%s", sp.PrefixHash))
			q.Add("Variable", fmt.Sprintf("recend=%s", cutHttp(sp.PostfixURL)))
			q.Add("Variable", fmt.Sprintf("recnameend=%s", sp.PostfixHash))
		}
	} else {
		q.Add("Context", "robot")
		q.Add("Variable", fmt.Sprintf("rec=%s", cutHttp(sp.PhraseURL)))
		q.Add("Variable", fmt.Sprintf("recname=%s", sp.PhraseHash))
		q.Add("Variable", fmt.Sprintf("recbeg=%s", cutHttp(sp.PrefixURL)))
		q.Add("Variable", fmt.Sprintf("recnamebeg=%s", sp.PrefixHash))
		q.Add("Variable", fmt.Sprintf("recend=%s", cutHttp(sp.PostfixURL)))
		q.Add("Variable", fmt.Sprintf("recnameend=%s", sp.PostfixHash))
	}

	q.Add("ActionID", uuid)
	q.Add("action", "originate")
	q.Add("channel", fmt.Sprintf("LOCAL/%s@from-crm", phone))
	q.Add("Callerid", phone)
	q.Add("Priority", "1")
	q.Add("Async", "True")
	q.Add("Exten", phone)
	q.Add("Variable", fmt.Sprintf("uid=%s", uuid))

	req.URL.RawQuery = q.Encode()

	fmt.Printf("\n\n REQ = %s \n\n", req.URL.RawQuery)

	resp, err := a.Do(req)
	if err != nil {
		return errors.Wrap(err, "Failed Roundtrip to Aster")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return errors.Errorf("requesting Asterisk to %s failed with status: %d", a.Config.AsterURL, resp.StatusCode)
		}
		return errors.Errorf("requesting Asterisk to %s failed with status: %d and body: %s", a.Config.AsterURL, resp.StatusCode, string(bodyStr))
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Sending to Aster Call command",
		"code":  resp.StatusCode,
		"body":  string(bodyStr),
	}).Info("Request to Asterisk sended.")

	return nil
}

func cutHttp(url string) string {
	var cutter string
	if strings.Contains(url, "http://") {
		cutter = "http://"
	}
	if strings.Contains(url, "https://") {
		cutter = "https://"
	}
	return strings.Replace(url, cutter, "", -1)
}
