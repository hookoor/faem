package rpc

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"reflect"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

type (
	SynthOptions struct {
		Emotion  string `json:"emotion,omitempty"` // эмоция - [good, evil, neutral]
		Voice    string `json:"voice,omitempty"`   // голос [alyss, jane, oksana, omazh], необязательные
		Ssml     string `json:"ssml"`
		Filename string `json:"-"`
	}
	// Config for speech synthesiser
	SpeechConfig struct {
		BaseURL    string
		IAM        string
		FolderID   string
		BucketName string
		ApiKey     string
	}

	// SynthClient for text to speech
	SynthClient struct {
		HTTPClient *http.Client
		Config     SpeechConfig
	}

	//Yandex Payload
	speechKitBody struct {
		Text            string `json:"text"`
		Ssml            string `json:"ssml"`
		Lang            string `json:"lang"`
		Voice           string `json:"voice"`
		Emotion         string `json:"emotion"`
		Speed           string `json:"speed"`
		Format          string `json:"format"`
		SampleRateHertz string `json:"sampleRateHertz"`
		FolderID        string `json:"folderId"`
	}

	yskIAM struct {
		IamToken  string    `json:"iamToken"`
		ExpiresAt time.Time `json:"expiresAt"`
	}
	yskOAuth struct {
		OAuthToken string `json:"yandexPassportOauthToken"`
	}
)

const (
	kitLanguage  = "ru-RU"
	kitSpeed     = "1.0"
	kitFormat    = "oggopus"
	kitRareHertz = "48000"
	kitVoice     = "alena"
	kitEmotion   = "good"
	fileExt      = "ogg"
	fileNameLen  = 8

	//TODO: Move bucketName to ENV VARS
	//bucketName = "faem-speech"
	buckerURL = "https://storage.googleapis.com"

	checkIAMinterval = 30
	errorIAMinterval = 5
)

func (sc *SynthClient) SetIAM(oauth, url string) error {

	checkInterval := checkIAMinterval * time.Minute
	go func() {
		for {
			err := sc.requestIAM(oauth, url)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event": "updating Yandex Speech IAM token ",
				}).Error(err)
				checkInterval = errorIAMinterval * time.Minute
			} else {
				logs.Eloger.WithFields(logrus.Fields{
					"event": "updating Yandex Speech IAM",
				}).Info("IAM token updated")
				checkInterval = checkIAMinterval * time.Minute
			}
			<-time.After(checkInterval)
		}
	}()
	return nil
}

func (sc *SynthClient) requestIAM(oauth, url string) error {

	yOauth := yskOAuth{
		OAuthToken: oauth,
	}
	body, err := json.Marshal(yOauth)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an POST request for JWT token")
	}
	resp, err := sc.HTTPClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to make a GET request (SKT JWT)")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("requesting JWT to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf("requesting JWT to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}
	var token yskIAM
	err = json.NewDecoder(resp.Body).Decode(&token)
	if err != nil {
		return errors.Wrap(err, "failed to decode JWT token response")
	}
	sc.Config.IAM = token.IamToken
	return nil
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func randFileName() string {
	b := make([]byte, fileNameLen)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return fmt.Sprintf("%s.%s", string(b), fileExt)
}

// GetSpeech gets text and return binary speech
func (s *SynthClient) GetSpeech(phrase proto.TextPhrase) ([]byte, error) {
	skb, err := newSKB(phrase, s.Config.FolderID)
	if err != nil {
		return nil, errors.Wrap(err, "error validating speech phrase")
	}

	urlKeys := skb.urlValues()
	req, err := http.NewRequest("POST", s.Config.BaseURL, strings.NewReader(urlKeys.Encode()))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create an http request to YandexSpeechKit")
	}
	//req.Header.Set("Authorization", "Bearer "+s.Config.IAM)
	//req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	//oauth or apikey authorisation
	if s.Config.IAM != "" {
		req.Header.Set("Authorization", "Bearer "+s.Config.IAM)
	} else {
		req.Header.Set("Authorization", "Api-Key "+s.Config.ApiKey)
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")

	resp, err := s.HTTPClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "Error requesting YandexSpeechKit")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, errors.Errorf("post request failed with status: %d", resp.StatusCode)
		}
		return nil, errors.Errorf("post request failed with status: %d and body: %s", resp.StatusCode, string(body))
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, errors.Wrap(err, "Error converting result to byte")
	}
	return body, nil
}

// New Speech Kit Body
func newSKB(ph proto.TextPhrase, folderID string) (speechKitBody, error) {
	var skb speechKitBody
	if ph.Text == "" && ph.Ssml == "" {
		return skb, fmt.Errorf("Text or Ssml filed must be not empty")
	}
	if ph.Text != "" {
		skb.Text = ph.Text
	} else {
		skb.Ssml = ph.Ssml
	}
	if ph.Voice == "" {
		skb.Voice = kitVoice
	} else {
		skb.Voice = ph.Voice
	}
	if ph.Emotion == "" {
		skb.Emotion = kitEmotion
	} else {
		skb.Voice = ph.Emotion
	}

	skb.FolderID = folderID
	skb.Lang = kitLanguage
	skb.Speed = kitSpeed
	skb.Format = kitFormat
	skb.SampleRateHertz = kitRareHertz

	return skb, nil
}

func (skb *speechKitBody) urlValues() url.Values {
	values := url.Values{}
	iVal := reflect.ValueOf(skb).Elem()
	typ := iVal.Type()
	for i := 0; i < iVal.NumField(); i++ {
		if fmt.Sprint(iVal.Field(i)) == "" {
			continue
		}
		values.Set(typ.Field(i).Tag.Get("json"), fmt.Sprint(iVal.Field(i)))
	}
	return values
}
