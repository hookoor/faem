package rpc

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

func (a *AsterClient) MakeConference(callData *proto.ConferenceData) {

	logs.Eloger.WithFields(logrus.Fields{
		"event":           "Creating conference call",
		"operator":        callData.Operator,
		"number":          callData.Number,
		"second operator": callData.SecondOperator,
	}).Info("Preparing data to make conference")

	//DUMMY CHECK +7 => 8
	if callData.Number != "" {
		if callData.Number[:2] == "+7" {
			callData.Number = "8" + callData.Number[2:]
		}
	}

	//Если поле оператора пустое - соединение с внешней линией
	if callData.SecondOperator == "" {
		if err := connectCurrentCall(callData, a); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":    "Creating external conference call. Adding current call",
				"operator": callData.Operator,
				"number":   callData.Number,
			}).Error(err)
		}

		if err := connectNewCall(callData, a); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":    "Creating external conference call. Calling to new number",
				"operator": callData.Operator,
				"number":   callData.Number,
			}).Error(err)
		}

		//if err := a.Logoff(); err != nil {
		//	logs.Eloger.WithFields(logrus.Fields{
		//		"event":  "Creating external conference call. Calling to new number",
		//		"reason": "Logging OFF",
		//	}).Error(err)
		//}

	} else {
		//иначе соединение с оператором
		if err := connectOperator(callData, a); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":           "Creating internal conference call. Calling to new number",
				"operator":        callData.Operator,
				"second operator": callData.SecondOperator,
			}).Error(err)
		}
		//if err := a.Logoff(); err != nil {
		//	logs.Eloger.WithFields(logrus.Fields{
		//		"event":  "Creating external conference call. Calling to new number",
		//		"reason": "Logging OFF",
		//	}).Error(err)
		//}

	}
}

//перевод на оператора
func connectOperator(callData *proto.ConferenceData, client *AsterClient) error {
	req, err := http.NewRequest("GET", client.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster conference request failed")
	}

	q := req.URL.Query()
	q.Add("action", "redirect")
	q.Add("channel", callData.Channel)
	q.Add("Context", "from-users")
	q.Add("Exten", callData.SecondOperator)
	q.Add("priority", "1")
	q.Add("Variable", fmt.Sprintf("confid=%s", callData.ConfID))

	err = makeRequest(req, q, client)
	if err != nil {
		return err
	}

	return nil
}

func connectNewCall(callData *proto.ConferenceData, client *AsterClient) error {
	req, err := http.NewRequest("GET", client.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster conference request failed")
	}

	q := req.URL.Query()
	q.Add("action", "originate")
	q.Add("channel", fmt.Sprintf("LOCAL/%v@from-crm-conf", callData.Number))
	q.Add("Callerid", callData.Number)
	q.Add("priority", "1")
	q.Add("Async", "False")
	q.Add("Context", "conference-client")
	q.Add("Exten", callData.Exten)
	q.Add("Variable", fmt.Sprintf("confid=%s", callData.ConfID))

	err = makeRequest(req, q, client)
	if err != nil {
		return err
	}

	//req.URL.RawQuery = q.Encode()
	//
	//t := digest.NewTransport(config.Username, config.Password)
	//tlsConfig := tls.Config{}
	//tlsConfig.InsecureSkipVerify = true
	//t.Transport = &http.Transport{
	//	TLSClientConfig: &tlsConfig,
	//}
	//
	//logs.Eloger.WithFields(logrus.Fields{
	//	"event":  "Creating conference call",
	//	"reason": "connecting new call",
	//}).Info(req.URL.RawQuery)
	//
	//resp, err := t.RoundTrip(req)
	//if err != nil {
	//	return errors.Wrap(err, "Failed Roundtrip to Aster Conference")
	//}
	//
	//bodyStr, err := ioutil.ReadAll(resp.Body)
	//if resp.StatusCode >= http.StatusBadRequest {
	//	if err != nil {
	//		return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d", config.AsterURL, resp.StatusCode)
	//	}
	//	return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d and body: %s", config.AsterURL, resp.StatusCode, string(bodyStr))
	//}
	//
	//logs.Eloger.WithFields(logrus.Fields{
	//	"event": "Creating conference call",
	//	"code":  resp.StatusCode,
	//	"body":  string(bodyStr),
	//}).Info("Request to connect new number to conference ... OK.")

	return nil
}

func connectCurrentCall(callData *proto.ConferenceData, client *AsterClient) error {
	req, err := http.NewRequest("GET", client.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster conference request failed")
	}

	q := req.URL.Query()
	q.Add("action", "redirect")
	q.Add("channel", callData.Channel)
	q.Add("Context", "conference")
	q.Add("Exten", callData.Exten)
	q.Add("priority", "1")
	q.Add("ExtraChannel", callData.ExtraChannel)
	q.Add("ExtraContext", "conference")
	q.Add("ExtraExten", callData.Exten)
	q.Add("Extrapriority", "1")
	q.Add("Variable", fmt.Sprintf("confid=%s", callData.ConfID))

	err = makeRequest(req, q, client)
	if err != nil {
		return err
	}

	//req.URL.RawQuery = q.Encode()
	//
	//t := digest.NewTransport(config.Username, config.Password)
	//tlsConfig := tls.Config{}
	//tlsConfig.InsecureSkipVerify = true
	//t.Transport = &http.Transport{
	//	TLSClientConfig: &tlsConfig,
	//}
	//
	//logs.Eloger.WithFields(logrus.Fields{
	//	"event":  "Creating conference call",
	//	"reason": "connecting current call",
	//}).Info(req.URL.RawQuery)
	//
	//resp, err := t.RoundTrip(req)
	//if err != nil {
	//	return errors.Wrap(err, "Failed Roundtrip to Aster Conference")
	//}
	//
	//bodyStr, err := ioutil.ReadAll(resp.Body)
	//if resp.StatusCode >= http.StatusBadRequest {
	//	if err != nil {
	//		return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d", config.AsterURL, resp.StatusCode)
	//	}
	//	return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d and body: %s", config.AsterURL, resp.StatusCode, string(bodyStr))
	//}
	//
	//logs.Eloger.WithFields(logrus.Fields{
	//	"event": "Creating conference call",
	//	"code":  resp.StatusCode,
	//	"body":  string(bodyStr),
	//}).Info("Request to connect current call to conference ... OK.")
	//

	return nil
}

func makeRequest(req *http.Request, q url.Values, client *AsterClient) error {
	req.URL.RawQuery = q.Encode()

	logs.Eloger.WithFields(logrus.Fields{
		"event":  "Creating conference call",
		"reason": "connecting current call",
	}).Info(req.URL.RawQuery)

	resp, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "Failed Roundtrip to Aster Conference")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d", client.Config.AsterURL, resp.StatusCode)
		}
		return errors.Errorf("requesting Asterisk ConfCall to %s failed with status: %d and body: %s", client.Config.AsterURL, resp.StatusCode, string(bodyStr))
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Creating conference call",
		"code":  resp.StatusCode,
		"body":  string(bodyStr),
	}).Info("Request to connect current call to conference ... OK.")

	return nil
}
