package rpc

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (a *AsterClient) DriverClientCall(callRequest *structures.NewConferenceData) error {

	logs.Eloger.WithFields(logrus.Fields{
		"event":  "Driver calling to client",
		"driver": callRequest.DriverPhone,
		"client": callRequest.ClientPhone,
	}).Info("Preparing data to make call")

	//DUMMY CHECK +7 => 8
	if callRequest.DriverPhone[:2] == "+7" {
		callRequest.DriverPhone = "8" + callRequest.DriverPhone[2:]
	}
	if callRequest.ClientPhone[:2] == "+7" {
		callRequest.ClientPhone = "8" + callRequest.ClientPhone[2:]
	}

	req, err := http.NewRequest("GET", a.Config.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster conference request failed")
	}

	q := req.URL.Query()
	q.Add("action", "originate")
	q.Add("channel", fmt.Sprintf("LOCAL/%s@from-crm", callRequest.DriverPhone))
	q.Add("Callerid", callRequest.DriverPhone)
	q.Add("priority", "1")
	q.Add("Async", "False")
	q.Add("Context", "from-users")
	q.Add("Exten", callRequest.ClientPhone)

	req.URL.RawQuery = q.Encode()

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Creating driver to client call",
	}).Info(req.URL.RawQuery)

	resp, err := a.Do(req)
	if err != nil {
		return errors.Wrap(err, "Failed Roundtrip to Aster Driver to Client call")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return errors.Errorf("requesting Asterisk Driver to Client Call to %s failed with status: %d", a.Config.AsterURL, resp.StatusCode)
		}
		return errors.Errorf("requesting Asterisk Driver to Client Call failed with status: %d and body: %s", resp.StatusCode, string(bodyStr))
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "Creating driver to client call",
		"code":  resp.StatusCode,
		"body":  string(bodyStr),
	}).Info("... OK.")

	//err = a.Logoff()
	//logs.Eloger.WithFields(logrus.Fields{
	//	"event": "Creating driver to client call",
	//	"Event": "Logging off",
	//}).Error(err)

	return nil
}
