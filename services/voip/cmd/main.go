package main

import (
	"flag"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/go-pg/pg"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/store"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	mymware "gitlab.com/faemproject/backend/faem/pkg/web/middleware"
	"gitlab.com/faemproject/backend/faem/services/voip/broker/publisher"
	"gitlab.com/faemproject/backend/faem/services/voip/broker/subscriber"
	"gitlab.com/faemproject/backend/faem/services/voip/config"
	"gitlab.com/faemproject/backend/faem/services/voip/handler"
	"gitlab.com/faemproject/backend/faem/services/voip/metrics"
	"gitlab.com/faemproject/backend/faem/services/voip/repository"
	"gitlab.com/faemproject/backend/faem/services/voip/rpc"
	"gitlab.com/faemproject/backend/faem/services/voip/server"
)

const (
	defaultConfigPath     = "config/voip.toml" // TODO: change me
	maxRequestsAllowed    = 3000
	serverShutdownTimeout = 30 * time.Second
	shutdownTimeout       = 30 * time.Second
)

func main() {
	// Parse flags
	configPath := flag.String("config", defaultConfigPath, "configuration file path")
	flag.Parse()

	cfg, err := config.Parse(*configPath)
	if err != nil {
		log.Fatalf("failed to parse the config file: %v", err)
	}

	cfg.Print() // just for debugging

	if err := logs.SetLogLevel(cfg.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(cfg.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	logger := logs.Eloger

	// Connect to the db and remember to close it
	db, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Database.Host, cfg.Database.Port),
		User:     cfg.Database.User,
		Password: cfg.Database.Password,
		Database: cfg.Database.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create a db instance: %v", err)
	}
	// Hook for logging SQL
	db.AddQueryHook(repository.DBLogger{})
	defer db.Close()

	synthClient := rpc.SynthClient{
		HTTPClient: &http.Client{
			Timeout: cfg.Tts.Timeout,
		},
		Config: rpc.SpeechConfig{
			BaseURL:    cfg.Tts.BaseURL,
			FolderID:   cfg.Tts.FolderID,
			BucketName: cfg.Tts.BucketName,
			ApiKey:     cfg.Tts.ApiKey,
		},
	}

	if cfg.Tts.ApiKey == "" {
		if err = synthClient.SetIAM(cfg.Tts.OAuth, cfg.Tts.OAuthURL); err != nil {
			logger.Fatalf("failed to init synthClient: %v", err)
		}
	}
	asterClient := rpc.NewAsterClient(rpc.AsterConfig{
		AsterURL: cfg.Aster.Url,
		Username: cfg.Aster.Username,
		Password: cfg.Aster.Password,
	})
	defer asterClient.Logoff() // remember to logoff from Asterisk

	// Connect to the broker and remember to close it
	rmq := &rabbit.Rabbit{
		Credits: rabbit.ConnCredits{
			URL:  cfg.Broker.UserURL,
			User: cfg.Broker.UserCredits,
		},
	}
	if err = rmq.Init(cfg.Broker.ExchangePrefix, cfg.Broker.ExchangePostfix); err != nil {
		logger.Fatalf("failed to connect to RabbitMQ: %v", err)
	}
	defer rmq.CloseRabbit()

	// Create a publisher
	pub := publisher.Publisher{
		Rabbit:  rmq,
		Encoder: &rabbit.JsonEncoder{},
	}
	if err = pub.Init(); err != nil {
		logger.Fatalf("failed to init the broker: %v", err)
	}
	defer pub.Wait()

	// Create a service object
	hdlr := handler.Handler{
		DB:           &repository.Pg{Db: db},
		Pub:          &pub,
		VoipCfg:      cfg.Voip,
		SynthClient:  &synthClient,
		AsterClient:  asterClient,
		AsterConfigs: asterClient.Config,
		Config:       cfg.Config,
	}

	// Create a subscriber
	sub := subscriber.Subscriber{
		Rabbit:  rmq,
		Encoder: &rabbit.JsonEncoder{},
		Handler: &hdlr,
	}
	if err = sub.Init(); err != nil {
		logger.Fatalf("failed to start the subscriber: %v", err)
	}
	defer sub.Wait(shutdownTimeout)

	// Create a rest gateway and handle http requests
	router := web.NewRouter(
		loggerOption(logger),
		throttler,
		cors,
	)

	//Init Prometheus
	promMetrics := prometheusmetric(router)
	hdlr.Metrics = promMetrics

	//Init statistic collector
	hdlr.InitStasCollector()

	//Init notificator handler
	hdlr.InitNotificatorTicker()

	rest := server.Rest{
		Router:  router,
		Handler: &hdlr,
	}
	rest.Route()

	// Start an http server and remember to shut it down
	go web.Start(router, cfg.Application.Port)
	defer web.Stop(router, serverShutdownTimeout)

	// Wait for program exit
	<-os.NotifyAboutExit()
}

func loggerOption(logger *logrus.Logger) web.Option {
	return func(e *echo.Echo) {
		e.Logger = &mymware.Logger{Logger: logger} // replace the original echo.Logger with the logrus one
		// Log the requests
		e.Use(mymware.LoggerWithSkipper(
			func(c echo.Context) bool {
				return strings.Contains(c.Request().RequestURI, "/metrics")
			}),
		)
	}
}

func throttler(e *echo.Echo) {
	e.Use(mymware.Throttle(maxRequestsAllowed))
}

func prometheusmetric(e *echo.Echo) metrics.PrometheusData {
	voipMetrics := metrics.BusinessMetrics()
	p := prometheus.NewPrometheus("echo", nil, voipMetrics)
	p.Use(e)

	promData, err := metrics.InitPrometheus(p)
	if err != nil {
		log.Fatalf("Error init prometheus metrics: %v", err)
	}

	return promData
}

func cors(e *echo.Echo) {
	//e.Use(middleware.CORS()) // allow CORS
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins:     []string{"*"},
		ExposeHeaders:    []string{"*"},
		MaxAge:           3600,
		AllowHeaders:     []string{"*"},
		AllowCredentials: true,
		AllowMethods:     []string{http.MethodGet},
	}))
}
