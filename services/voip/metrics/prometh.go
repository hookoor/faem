package metrics

import (
	promethGo "github.com/prometheus/client_golang/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
)

type PrometheusData struct {
	Prometh                                                                                                   *prometheus.Prometheus
	CallsStatesCnt                                                                                            *promethGo.CounterVec
	OperatorsOnLine                                                                                           *promethGo.GaugeVec
	AsterLoggedIn, AsterAvailable, AsterCallers, AsterHoldTime, AsterTalkTime, AsterCompleted, AsterAbandoned promethGo.Gauge
}

// InitPrometheus initialize Prometheues instance
func InitPrometheus(prometh *prometheus.Prometheus) (PrometheusData, error) {
	var pt PrometheusData
	//setting up prometheusData metrics variables
	for _, metricDef := range prometh.MetricsList {
		switch metricDef.ID {
		case "faemOperatorsOnline":
			pt.OperatorsOnLine = metricDef.MetricCollector.(*promethGo.GaugeVec)
		case "feamCallsStates":
			pt.CallsStatesCnt = metricDef.MetricCollector.(*promethGo.CounterVec)
		case "faemAsterLoggedIn":
			pt.AsterLoggedIn = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterAvailable":
			pt.AsterAvailable = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterCallers":
			pt.AsterCallers = metricDef.MetricCollector.(promethGo.Gauge)

		case "faemAsterCalls":
			pt.AsterCallers = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterHoldTime":
			pt.AsterHoldTime = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterTalkTime":
			pt.AsterTalkTime = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterCompleted":
			pt.AsterCompleted = metricDef.MetricCollector.(promethGo.Gauge)
		case "faemAsterAbandoned":
			pt.AsterAbandoned = metricDef.MetricCollector.(promethGo.Gauge)

		}
	}

	return pt, nil
}

func BusinessMetrics() []*prometheus.Metric {
	var (
		callsStatesCnt = &prometheus.Metric{
			ID:          "feamCallsStates",
			Name:        "calls_states_count",
			Description: "How many calls was made",
			Type:        "counter_vec",
			Args:        []string{"type", "status", "line"},
		}

		OperatorsOnLine = &prometheus.Metric{
			ID:          "faemOperatorsOnline",
			Name:        "operators_online",
			Description: "Операторы онлайн",
			Type:        "gauge_vec",
			Args:        []string{"operator"},
		}

		AsterLoggedIn = &prometheus.Metric{
			ID:          "faemAsterLoggedIn",
			Name:        "aster_loggedin",
			Description: "Всего зарегистрировано операторов",
			Type:        "gauge",
		}

		AsterAvailable = &prometheus.Metric{
			ID:          "faemAsterAvailable",
			Name:        "aster_available",
			Description: "Операторов готовых принять заказ",
			Type:        "gauge",
		}

		AsterCallers = &prometheus.Metric{
			ID:          "faemAsterCallers",
			Name:        "aster_callers",
			Description: "Очередь клиентов",
			Type:        "gauge",
		}

		// Operators
		AsterCalls = &prometheus.Metric{
			ID:          "faemAsterCalls",
			Name:        "aster_calls",
			Description: "эпидерсия",
			Type:        "gauge",
		}
		AsterHoldTime = &prometheus.Metric{
			ID:          "faemAsterHoldTime",
			Name:        "aster_holdtime",
			Description: "среднее время ответа на звонок",
			Type:        "gauge",
		}
		AsterTalkTime = &prometheus.Metric{
			ID:          "faemAsterTalkTime",
			Name:        "aster_talktime",
			Description: "среднее время разговора",
			Type:        "gauge",
		}
		AsterCompleted = &prometheus.Metric{
			ID:          "faemAsterCompleted",
			Name:        "aster_completed",
			Description: "отвеченных",
			Type:        "gauge",
		}
		AsterAbandoned = &prometheus.Metric{
			ID:          "faemAsterAbandoned",
			Name:        "aster_abandoned",
			Description: "пропущенных",
			Type:        "gauge",
		}
	)
	return []*prometheus.Metric{
		callsStatesCnt,
		OperatorsOnLine,
		AsterLoggedIn,
		AsterAvailable,
		AsterCallers,
		AsterCalls,
		AsterHoldTime,
		AsterTalkTime,
		AsterCompleted,
		AsterAbandoned,
	}
}
