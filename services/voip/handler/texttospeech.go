package handler

import (
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/rpc"
)

// SynthClient interface
type SynthClient interface {
	GetSpeech(proto.TextPhrase) ([]byte, error)
	UploadToCloud(byteData *[]byte, fName ...string) (string, error)
}

// Call handle incoming call event from Asterisk
func (h *Handler) SpeechSynth(ctx context.Context, syntText string, opts ...rpc.SynthOptions) (string, error) {

	if syntText == "" {
		return "", errors.New("error validating text")
	}

	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"event": "SpeechSynth",
		"text":  syntText,
	}).Info("start synthesizing speech") // just a logging sample

	textPhrase := proto.TextPhrase{
		Text: syntText,
	}
	if len(opts) > 0 {
		textPhrase.Emotion = opts[0].Emotion
		textPhrase.Voice = opts[0].Voice
	}

	speech, err := h.SynthClient.GetSpeech(textPhrase)
	if err != nil {
		return "", err
	}

	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"event": "uploading to cloud storage",
	}).Info("Waiting....") // just a logging sample
	var fname string
	if len(opts) > 0 {
		fname = opts[0].Filename
	}
	url, err := h.SynthClient.UploadToCloud(&speech, fname)
	if err != nil {
		return "", err
	}

	return url, nil
}

// get proto.TextPhrase from proto.AutoOutCallRequest
func newTextPhrase(callReq *proto.AutoOutCallRequest) proto.TextPhrase {
	return proto.TextPhrase{
		Text:    callReq.Text,
		Voice:   callReq.Voice,
		Emotion: callReq.Emotion,
	}
}
