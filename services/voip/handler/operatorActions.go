package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

func (h *Handler) OperatorAction(action proto.ActionRequest) error {

	var err error
	switch action.Action {
	case "enter_queue":
		err = h.enterQueue(action.Operator)
	case "exit_queue":
		err = h.exitQueue(action.Operator)
	}
	return err
}

func (h *Handler) enterQueue(oper string) error {
	req, err := http.NewRequest("GET", h.AsterConfigs.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster New Request failed")
	}
	q := req.URL.Query()
	q.Add("action", "QueueAdd")
	q.Add("Queue", "opers")
	q.Add("Interface", fmt.Sprintf("Local/%s@from-queue", oper))
	q.Add("MemberName", fmt.Sprintf("Dynamic Operator %s", oper))
	q.Add("StateInterface", fmt.Sprintf("PJSIP/%s", oper))

	req.URL.RawQuery = q.Encode()
	resp, err := h.AsterClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "Failed Roundtrip to Aster")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return errors.Errorf("requesting Asterisk. Failed with status: %d", resp.StatusCode)
		}
		return errors.Errorf("requesting Asterisk. Failed with status: %d and body: %s", resp.StatusCode, string(bodyStr))
	}

	return nil
}

func (h *Handler) exitQueue(oper string) error {
	req, err := http.NewRequest("GET", h.AsterConfigs.AsterURL, nil)
	if err != nil {
		return errors.Wrap(err, "Creating Aster New Request failed")
	}

	q := req.URL.Query()
	q.Add("action", "QueueRemove")
	q.Add("Queue", "opers")
	q.Add("Interface", fmt.Sprintf("Local/%s@from-queue", oper))

	req.URL.RawQuery = q.Encode()

	resp, err := h.AsterClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "Failed Roundtrip to Aster")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return errors.Errorf("requesting Asterisk. Failed with status: %d", resp.StatusCode)
		}
		return errors.Errorf("requesting Asterisk. Failed with status: %d and body: %s", resp.StatusCode, string(bodyStr))
	}

	return nil
}
