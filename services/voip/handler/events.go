package handler

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/speech"
)

const (
	orderCancelledPhrase = "Ваш заказ отменен, - будем рады видеть вас в следующий раз"
	connectWithDriver    = "Соединяем Вас с водителем"
	connectWithOperator  = "Соединяем Вас с оператором"
	menuPhraseFindingCar = "По вашему заказу идет поиск автомобиля. - Для отмены - нажмите 2, для соединения с оператором - нажмите 0"
	menuPhraseCarFound   = "Для отмены - нажмите 2, для соединения с водителем - нажмите 8, для соединения с оператором - нажмите 0"
	menuPhraseCarWaiting = "Для соединения с водителем - нажмите 8. Для соединения с оператором - нажмите 0"
)

type (
	Publisher interface {
		//SendEvent sends all events from Asterisk to Broker
		SendEvent(call *structures.TelephonyCall) error
		SendDelayedReCall(call *proto.AsterEvent) error
		SendOrderCancel(orderUUID, orderState string) error
	}
	CallEventsRepository interface {
		//CheckAutoCallTriggers returns [ignore | setOk | schedule]. Ignore - do nothing
		//setOk - means that autocall trigger set to Ok state. Call is reached.
		//schedule - need to schedulle new call
		CheckAutoCallTriggers(call *proto.AsterEvent) (string, models.AutoCall, error)

		//SaveOKStatusAC setting AutoCall status to OK
		SaveOKStatusAC(call *proto.AsterEvent, autocall models.AutoCall) error

		//SavesCall to Aster Events table
		SaveCall(call *proto.AsterEvent) error

		//CallEvents return all events by ID
		CallEventsByID(id string) ([]models.AsterEvent, error)
		//CallEvents return all events by UID
		CallEventsByUID(uid string) ([]models.AsterEvent, error)
	}

	DefaultResp struct {
		Status string `json:"status"`
	}
	ivrRespone struct {
		RecNameIVR string `json:"recnameivr"`
		RecIVR     string `json:"recivr"`
	}
)

// Call handle incoming call event from Asterisk
func (h *Handler) Call(ctx context.Context, call *proto.AsterEvent) (interface{}, error) {
	logger := logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"event":  "handling call event from Asterisk",
		"callID": call.ID,
		"status": call.Status,
		"uid":    call.Uid,
	})
	var response interface{}
	response = DefaultResp{Status: "Ok"}

	err := h.DB.SaveCall(call)
	if err != nil {
		logger.Errorf("Error saving event. %v", err)
	}
	switch call.Status {
	case rabbit.IVRAsterStatus:
		logger.Info("handling ivr menu")
		ivrResp, err := h.handleIVRMenu(call)
		if err != nil {
			logger.WithField("context", "error getting orderUUID from hash").Error(err)
		}
		return ivrResp, nil

	case rabbit.IncomingAsterStatus:
		logger.Info("handling incoming call")
		response = h.handleIncomingCall(call)
	case rabbit.ConfHangupAsterStatus:
		crmEvent, err := h.handleConferenceEnd(call)
		if err != nil {
			logger.WithField("context", "error handling crmEvent in conference end").Error(err)
			return response, nil
		}

		logs.Eloger.WithFields(logrus.Fields{
			"event":  "handling end of conference call",
			"callID": call.ID,
		}).Info("event will send to broker")

		h.sendToBroker(crmEvent)

	// Завершение звонка и начало конференции
	case rabbit.ConferenceAsterStatus:
		//таким костылем ловим второе уведомление, перед конференц звонокм
		if len(call.CallerID) < 5 {
			return response, nil
		}

		crmEvent, err := h.handleConference(call)
		if err != nil {
			logger.WithField("context", "error handling crmEvent in conference").Error(err)
			return response, nil
		}

		logs.Eloger.WithFields(logrus.Fields{
			"event":  "handling switching to conference",
			"callID": call.ID,
		}).Info("event will send to broker")

		h.sendToBroker(crmEvent)

	//	Обрабатываем событие только когда звонок завершился
	case rabbit.HangupAsterStatus:

		//обработка конф кола
		//TODO: если конф колл, берем его ID и находим запись кидаем ее в ребит как первый звонок

		//если просто внутреннее событие hangup - игнорим
		if isInternal(call.Channel) && call.Uid == "" {
			return response, nil
		}
		//специальная заплатка-костыль которая скипает второе уведомление об авто-звонке
		if isInternal(call.Channel) && call.Uid != "" && call.DialStatus == "" {
			return response, nil
		}

		//завершения автоматического звонка в каком либо статусе обрабатываем тут
		if call.DialStatus != "" && call.Uid != "" {
			action, orderUUID, err := h.handleAutoCall(call)
			if err != nil {
				logger.WithField("context", "error handling autocall").Error(err)
			} else {
				logger.Infof("Autocall (%v) action is %v. ", call.Uid, action)
			}
			if call.Uid != "" {
				call.OrderUUID = orderUUID
			}
		}

		crmEvent, err := h.handleHangup(call)
		if err != nil {
			logger.WithField("context", "error handling crmEvent").Error(err)
			return response, nil
		}
		logs.Eloger.WithFields(logrus.Fields{
			"event":    "handling hangup",
			"type":     crmEvent.Type,
			"status":   crmEvent.Status,
			"crmEvent": crmEvent,
		}).Info("event will send to broker")

		h.sendToBroker(crmEvent)
	}

	return response, nil
}

func (h *Handler) handleIVRMenu(call *proto.AsterEvent) (string, error) {
	switch call.CRMAction {
	case "cancel":
		ord, err := h.DB.OrderByPhone(call.CallerID)
		if err != nil {
			return "", err
		}
		h.sendCancel(ord.OrderUUID, ord.OrderState)
	case "getivar":
		var res string
		ordState, drvPhone := h.orderStateAndDriver(call.CallerID)

		var asteriskHello string
		helloUrl := fmt.Sprintf("%s/sources/%s/asterisk_hello", h.Config.Endpoints.CRM, call.Exten)
		err := tool.SendRequest(http.MethodGet, helloUrl, nil, nil, &asteriskHello)
		if err != nil {
			logs.Eloger.WithError(err).WithFields(logrus.Fields{
				"url": helloUrl,
			}).Warnf("failed to fetch asterisk_hello")
		}
		if asteriskHello == "" {
			asteriskHello = "NULL"
		}

		var clientIsBlacklisted bool
		url := h.Config.Endpoints.CRM + h.Config.Endpoints.CRMEndpoints.IsTheClientBlacklisted + call.CallerID
		logs.Eloger.WithField("url", url).Infoln("request for check client blacklisted")
		err = tool.SendRequest(http.MethodGet, url, nil, nil, &clientIsBlacklisted)
		if err != nil {
			// нельзя возвращать в астер ошибку, если номера нет
			logs.Eloger.Warnln("client with this number not found")
		}
		clientBlacklistedFlag := 0
		if clientIsBlacklisted {
			clientBlacklistedFlag = 1
		}
		res = fmt.Sprintf("%s,NULL,%d,%s,%s", ordState, clientBlacklistedFlag, asteriskHello, drvPhone)

		return res, nil
	case "getsvar":
		splitPhrases, err := h.prepareVars(call.CallerID)
		if err != nil {
			return "NULL,NULL,NULL,NULL,NULL", err
		}
		resp := fmt.Sprintf("%s,%s,%s,%s,%s,%s", splitPhrases.PrefixHash, splitPhrases.PhraseHash, splitPhrases.PostfixHash, splitPhrases.ExtraHash1, splitPhrases.ExtraHash2, splitPhrases.ExtraHash3)
		return resp, nil
		//return "de7eb8392332f2a530e16983a4b346d7f50b7570,df80414f9a6dbf4eddcd19fc1727e0335a7f5e7e,d71ec32b2b9568122df4901de7be826d0a9a05ba,c330c0d4c2891538056466efa0bc640f9958bf7c,c330c0d4c2891538056466efa0bc640f9958bf7c,c330c0d4c2891538056466efa0bc640f9958bf7c", nil
	}
	return "UNKNOWN ACTION", nil
}

//1 - de7eb8392332f2a530e16983a4b346d7f50b7570 (Здравствуйте, это такси. Через 5 минут к вам подъедет)
//2 - df80414f9a6dbf4eddcd19fc1727e0335a7f5e7e (белый Nissan номер 333)
//3 - d71ec32b2b9568122df4901de7be826d0a9a05ba (пожалуйста ожидайте)
//4 - c330c0d4c2891538056466efa0bc640f9958bf7c (Ваш заказ отменен, спасибо)
//5 - c330c0d4c2891538056466efa0bc640f9958bf7c (Ваш заказ отменен, спасибо - должен быть: соединяю с водителем)
//6 - c330c0d4c2891538056466efa0bc640f9958bf7c (Ваш заказ отменен, спасибо - должен быть: Соединяем Вас с оператором)

func (h *Handler) prepareVars(phone string) (proto.SplittedPhrase, error) {
	order, err := h.DB.OrderByPhone(phone)
	if err != nil {
		return proto.SplittedPhrase{}, err
	}

	var splitedPhrase proto.SplittedPhrase
	var sayText string

	switch order.OrderState {
	case constants.OrderStateDistributing, constants.OrderStateFree, constants.OrderStateOffered:
		sayText = menuPhraseFindingCar
		sayText = fmt.Sprintf("%s - $ - %s - $ - %s", sayText, orderCancelledPhrase, connectWithOperator)
	case constants.OrderStateAccepted, constants.OrderStateStarted, constants.OrderStateOnTheWay:
		sayText, err = speech.CarWillArrive(speech.SpeechData{
			CarColor:  order.RawOrder.Driver.Color,
			CarNumber: order.RawOrder.Driver.RegNumber,
			CarBrand:  order.RawOrder.Driver.Car,
			ArriveIn:  order.RawOrder.ArrivalTime,
		})
		sayText = fmt.Sprintf("%s - %s $ %s $ %s $ %s", sayText, menuPhraseCarFound, orderCancelledPhrase, connectWithDriver, connectWithOperator)
	case constants.OrderStateOnPlace:
		sayText, err = speech.CarArrived(speech.SpeechData{
			CarColor:  order.RawOrder.Driver.Color,
			CarNumber: order.RawOrder.Driver.RegNumber,
			CarBrand:  order.RawOrder.Driver.Car,
			ArriveIn:  order.RawOrder.ArrivalTime,
		})
		sayText = fmt.Sprintf("%s - %s $ %s $ %s", sayText, menuPhraseCarWaiting, connectWithDriver, connectWithOperator)
	default:
		return splitedPhrase, errors.New("Unhandlerd state")
	}

	splitedPhrase = speech.PreparePhrases(sayText)

	//проверям что есть в кэше
	err = h.DB.CachedPhrases(&splitedPhrase)
	if err != nil {
		err = errors.Wrap(err, "Error during requesting phrases")
	}

	//синтезируем и сохраняем
	err = h.synthAndSaveCache(&splitedPhrase)
	if err != nil {
		return proto.SplittedPhrase{}, err
	}

	return splitedPhrase, nil
}

func (h *Handler) orderStateAndDriver(phone string) (string, string) {
	//1) Статус заказа. 0 - нет заказа, 1 - поиск авто, 2 выехал, 3 - жду, 4 выполняется
	//5) Номер водителя

	order, err := h.DB.OrderByPhone(phone)
	if err != nil {
		return "0", "NULL"
	}
	drvPhone := order.RawOrder.Driver.Phone

	//DUMMY CHECK +7 => 8
	if len(drvPhone) > 0 && drvPhone[:2] == "+7" {
		drvPhone = "8" + drvPhone[2:]
	}
	switch order.OrderState {

	case constants.OrderStateDistributing, constants.OrderStateFree, constants.OrderStateOffered, constants.OrderStateRejected:
		return "1", "NULL"
	case constants.OrderStateAccepted, constants.OrderStateStarted:
		return "2", drvPhone
	case constants.OrderStateOnPlace:
		return "3", drvPhone
	case constants.OrderStateOnTheWay, constants.OrderStateWaiting, constants.OrderStatePayment:
		return "4", drvPhone
	}

	return "0", "NULL"
}

func (h *Handler) sendToBroker(crmEvent structures.TelephonyCall) {
	h.Metrics.CallsStatesCnt.WithLabelValues(crmEvent.Type, crmEvent.Status, crmEvent.Exten).Inc()

	go func() {
		if err := h.Pub.SendEvent(&crmEvent); err != nil {
			logs.Eloger.Error(err) // just a sample error handling
		}
	}()
}

func (h *Handler) sendCancel(orderUUID, orderState string) {
	go func() {
		if err := h.Pub.SendOrderCancel(orderUUID, orderState); err != nil {
			logs.Eloger.Error(err)
		}
	}()
}

//handleHangup обрабатывает событие завершения звонка
func (h *Handler) handleHangup(call *proto.AsterEvent) (structures.TelephonyCall, error) {
	var crmEvent structures.TelephonyCall
	var callEventsList []models.AsterEvent
	var eventType string
	var err error

	if call.Uid != "" {
		callEventsList, err = h.DB.CallEventsByUID(call.Uid)
		autoCall, _ := h.DB.GetAutoCall(call.Uid)
		eventType = autoCall.Type
	} else if call.ID != "" {
		callEventsList, err = h.DB.CallEventsByID(call.ID)
	} else {
		return crmEvent, errors.New("Call ID and UID are empty.")
	}

	if err != nil {
		return crmEvent, errors.Wrap(err, "error getting list of events")
	}

	//собираем все ивенты по данному звонке
	crmEvent, err = h.callEventForCRM(callEventsList, call.OrderUUID, eventType)
	if err != nil {
		return crmEvent, err
	}

	return crmEvent, nil
}

//handleConferenceEnd обрабатываем завершение конференц звонка
func (h *Handler) handleConferenceEnd(call *proto.AsterEvent) (structures.TelephonyCall, error) {

	callEventsList, err := h.DB.CallEventsByID(call.ConfID)
	if err != nil {
		return structures.TelephonyCall{}, errors.Wrap(err, "error getting list of events for conference end")
	}

	return structures.TelephonyCall{
		Record:    call.Record,
		CallerID:  callerID(callEventsList),
		Status:    call.DialStatus,
		Type:      "Conference",
		CallID:    call.ID,
		Operator:  lastOperator(callEventsList),
		StartTime: time.Now(),
		Exten:     callToNumber(callEventsList),
	}, nil
}

func (h *Handler) handleConference(call *proto.AsterEvent) (structures.TelephonyCall, error) {
	callEventsList, err := h.DB.CallEventsByID(call.ID)
	if err != nil {
		return structures.TelephonyCall{}, errors.Wrap(err, "error getting list of events")
	}

	st := callEventsList[len(callEventsList)-1].Status
	return structures.TelephonyCall{
		Record:    callRecord(callEventsList),
		CallerID:  call.CallerID,
		Status:    structures.VOIPCallSatusANSWER,
		Type:      strings.ToUpper(st[:1]) + st[1:],
		CallID:    call.ID,
		Operator:  lastOperator(callEventsList),
		EndTime:   callEventsList[0].CallTimestamp,
		StartTime: callEventsList[len(callEventsList)-1].CallTimestamp,
		Exten:     callToNumber(callEventsList),
	}, nil
}

func (h *Handler) callEventForCRM(asterEvents []models.AsterEvent, OrderUUID string, acType ...string) (structures.TelephonyCall, error) {
	if len(asterEvents) < 2 {
		return structures.TelephonyCall{}, errors.New("Less then 2 asterisk events")
	}
	switch callType(asterEvents) {
	case "Outgoing":
		return handelOutCall(asterEvents), nil
	case "AutoCall":
		var autoCallType string
		if len(acType) > 0 && acType[0] != "" {
			autoCallType = acType[0]
		} else {
			autoCallType = structures.BasicAutoCall
		}
		return handelAutoCall(asterEvents, OrderUUID, autoCallType), nil
	case "Incoming":
		return handelIncomingCall(asterEvents), nil
	default:
		return structures.TelephonyCall{}, errors.New("Unknown call type")
	}
}

func handelOutCall(asterEvents []models.AsterEvent) structures.TelephonyCall {
	return structures.TelephonyCall{
		Type:      "Outgoing",
		CallID:    asterEvents[0].CallID,                         // id звонка
		Record:    asterEvents[0].Record,                         // запись
		EndTime:   asterEvents[0].CallTimestamp,                  // время завершения звонка
		StartTime: asterEvents[len(asterEvents)-1].CallTimestamp, // время начала звонка
		CallerID:  asterEvents[1].Exten,                          // кто звонил
		Exten:     "UNKNOWN",                                     // c какого номера
		Operator:  asterEvents[0].CallerID,                       // оператор
		Status:    asterEvents[0].DialStatus,                     // чем закончился вызов
	}
}

func handelIncomingCall(asterEvents []models.AsterEvent) structures.TelephonyCall {
	return structures.TelephonyCall{
		Type:      "Incoming",
		CallID:    asterEvents[0].CallID,                         // id звонка
		EndTime:   asterEvents[0].CallTimestamp,                  // время завершения звонка
		StartTime: asterEvents[len(asterEvents)-1].CallTimestamp, // время начала звонка
		Record:    callRecord(asterEvents),                       // запись
		CallerID:  asterEvents[0].CallerID,                       // кто звонил
		Exten:     callToNumber(asterEvents),                     // на какой номер
		Operator:  lastOperator(asterEvents),                     // оператор
		Status:    lastDialStatus(asterEvents),                   // чем закончился вызов
	}
}

func handelAutoCall(asterEvents []models.AsterEvent, orderUUID, acType string) structures.TelephonyCall {
	return structures.TelephonyCall{
		Type:      acType,
		Uid:       asterEvents[0].Uid,                            // UID если этот вызов был инициирован нами
		EndTime:   asterEvents[0].CallTimestamp,                  // время завершения звонка
		StartTime: asterEvents[len(asterEvents)-1].CallTimestamp, // время начала звонка
		Record:    callRecord(asterEvents),                       // запись
		CallerID:  asterEvents[0].CallerID,                       // кто звонил
		Exten:     callToNumber(asterEvents),                     // на какой номер
		Operator:  lastOperator(asterEvents),                     // оператор
		Status:    lastDialStatus(asterEvents),                   // чем закончился вызов
		OrderUUID: orderUUID,
	}
}

func callType(asterEvents []models.AsterEvent) string {
	lastidx := len(asterEvents) - 1
	for i := lastidx; i >= 0; i-- {
		switch asterEvents[i].Status {
		case "startcall":
			return "AutoCall"
		case "out":
			return "Outgoing"
		case "incoming":
			return "Incoming"
		}
	}
	return "unknown"
}

func callRecord(asterEvents []models.AsterEvent) string {
	for _, val := range asterEvents {
		if val.Record != "" {
			return val.Record
		}
	}
	return ""
}

func callerID(asterEvents []models.AsterEvent) string {
	for _, val := range asterEvents {
		if val.Status == "ringing" {
			return val.CallerID
		}
	}
	return ""
}

func lastDialStatus(asterEvents []models.AsterEvent) string {
	var lastStateAt time.Time
	state := "Unknown"
	for _, val := range asterEvents {
		if val.CallTimestamp.After(lastStateAt) {
			lastStateAt = val.CallTimestamp
			state = val.DialStatus
		}
	}
	return state
}

// возвращает последнего оператора который принял звонок
// 0 если такого не найдено
func lastOperator(asterEvents []models.AsterEvent) string {
	var lastOperatorTime time.Time
	lastIndex := -1
	for i, val := range asterEvents {
		if !isInternal(val.Channel) {
			continue
		}
		if val.CallTimestamp.After(lastOperatorTime) {
			lastIndex = i
		}
	}
	if lastIndex == -1 {
		return "0"
	}
	return asterEvents[lastIndex].Exten
}

//возвращает номер на который звонили
func callToNumber(asterEvents []models.AsterEvent) string {
	for k := range asterEvents {
		cur := len(asterEvents) - 1 - k
		if !isInternal(asterEvents[cur].Channel) {
			return asterEvents[cur].Exten
		}
	}
	return "0"
}

//является ли канал внутренним или внешним
func isInternal(chanel string) bool {
	if len(chanel) > 0 && chanel[:5] == "Local" {
		return true
	}
	return false
}

// функция проверяет относиться ли вызов к автоколам,
// если да - обрабатывает его и возращает действие и номер заказа
func (h *Handler) handleAutoCall(call *proto.AsterEvent) (string, string, error) {
	var err error
	var orderUUID string
	event, autocall, err := h.DB.CheckAutoCallTriggers(call)
	if err != nil {
		return "errror checking autocall trigger", orderUUID, err
	}
	if len(autocall.OrderUUID) > 1 {
		orderUUID = autocall.OrderUUID
	}
	switch event {
	case "setOk":
		err = h.DB.SaveOKStatusAC(call, autocall)
		return "Saving OK status for AutoCall", orderUUID, err
	case "schedule":
		err = h.scheduleAC(call, autocall)
		return "Schedulling AutoCall", orderUUID, err
	case "ignore":
		return "ignore", orderUUID, nil
	}

	errStr := fmt.Sprintf("Unknown AutoCall state. Event = %v", event)

	return "Unknown autocall event", "", errors.New(errStr)
}

func (h *Handler) scheduleAC(call *proto.AsterEvent, autoCall models.AutoCall) error {
	err := h.DB.SetSchedulledStateAC(autoCall.Hash)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "schedulleing autocall",
		}).Error(err)
	}

	err = h.Pub.SendDelayedReCall(call)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "schedulleing autocall",
		}).Error(err)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "schedulling autocall",
		"phone": call.CallerID,
	}).Info("AutoCall schedulled")
	return nil
}

func (h *Handler) handleIncomingCall(call *proto.AsterEvent) proto.GroupNameResponse {
	var resp proto.GroupNameResponse
	group, err := h.DB.GetClientGroup(call)
	if err != nil || group == "" {
		resp.OperatorGroup = h.VoipCfg.DefaultGroup
	} else {
		resp.OperatorGroup = group
	}
	return resp
}
