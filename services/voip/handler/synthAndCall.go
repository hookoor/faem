package handler

import (
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/rpc"
	"gitlab.com/faemproject/backend/faem/services/voip/speech"
)

// SynthAndCall extend handler and combine all other events to synth speech and make call
//
func (h *Handler) SynthAndCall(call proto.AutoOutCallRequest) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling auto call request",
		"phone": call.Phone,
	})
	var splitedPhrase proto.SplittedPhrase
	var err error
	//возвращается фразу разбитую на части с хэшем
	switch call.Type {
	case structures.OnplaceAutoCall, structures.OnTheWayAutoCall:
		splitedPhrase, err = h.prepareVars(call.Phone)
	default:
		splitedPhrase = speech.PreparePhrases(call.Text)
	}

	//проверям что есть в кэше
	err = h.DB.CachedPhrases(&splitedPhrase)
	if err != nil {
		return errors.Wrap(err, "Error during requesting phrases")
	}
	//синтезируем и сохраняем
	err = h.synthAndSaveCache(&splitedPhrase)
	if err != nil {
		return errors.Wrap(err, "Synthing or saving requests")
	}

	// saving call to DB
	hash, err := h.NewAutoCall(&call, &splitedPhrase)
	if err != nil {
		return errors.Wrap(err, "failed to save new auto call")
	}

	//making outgoing call
	cType := string(call.Type)
	switch call.Type {
	case structures.LongDistibNotificAutoCall, structures.OnplaceAutoCall, structures.OnTheWayAutoCall:
		err = h.CallerHandler(call.Phone, hash, &splitedPhrase, cType)
	default:
		err = h.CallerHandler(call.Phone, hash, &splitedPhrase)
	}

	if err != nil {
		return errors.Wrap(err, "failed to make call")
	}
	log.Infof("caller handler is OK")

	//saving call state
	err = h.DB.SetCallingStateAC(hash)
	if err != nil {
		return errors.Wrap(err, "failed to make call")
	}

	log.Infof("caller handler is OK")

	return nil
}

// синтезируем, загружаем и возращаем фразы
func (h *Handler) synthAndReturnURL(text string) (proto.SplittedPhrase, error) {
	var splitedPhrase proto.SplittedPhrase
	//возвращается фразу разбитую на части с хэшем
	splitedPhrase = speech.PreparePhrases(text)
	//проверям что есть в кэше
	err := h.DB.CachedPhrases(&splitedPhrase)
	if err != nil {
		return splitedPhrase, errors.Wrap(err, "Error during requesting phrases")
	}
	//синтезируем и сохраняем
	err = h.synthAndSaveCache(&splitedPhrase)
	if err != nil {
		return splitedPhrase, errors.Wrap(err, "Synthing or saving requests")
	}
	return splitedPhrase, nil
}

func (h *Handler) synthAndSaveCache(splitedPhrase *proto.SplittedPhrase) error {
	var err error
	if splitedPhrase.PrefixURL == "" {
		splitedPhrase.PrefixURL, err = h.SpeechSynth(context.Background(), splitedPhrase.Prefix, rpc.SynthOptions{Filename: splitedPhrase.PrefixHash})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.Prefix, splitedPhrase.PrefixHash, splitedPhrase.PrefixURL)
			if err != nil {
				err = errors.Wrap(err, "Error Saving Prefix Cache")
			}
		}
	}
	if splitedPhrase.PhraseURL == "" {
		splitedPhrase.PhraseURL, err = h.SpeechSynth(context.Background(), splitedPhrase.Phrase, rpc.SynthOptions{Filename: splitedPhrase.PhraseHash})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.Phrase, splitedPhrase.PhraseHash, splitedPhrase.PhraseURL)
			if err != nil {
				err = errors.Wrap(err, "Error Saving Phrase Cache")
			}
		}
	}
	if splitedPhrase.PostfixURL == "" {
		splitedPhrase.PostfixURL, err = h.SpeechSynth(context.Background(), splitedPhrase.Postfix, rpc.SynthOptions{Filename: splitedPhrase.PostfixHash})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.Postfix, splitedPhrase.PostfixHash, splitedPhrase.PostfixURL)
			if err != nil {
				err = errors.Wrap(err, "Error Saving Postfix Cache")
			}
		}
	}

	if splitedPhrase.ExtraURL1 == "" && splitedPhrase.ExtraPhrase1 != "" {
		splitedPhrase.ExtraURL1, err = h.SpeechSynth(context.Background(), splitedPhrase.ExtraPhrase1, rpc.SynthOptions{Filename: splitedPhrase.ExtraHash1})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.ExtraPhrase1, splitedPhrase.ExtraHash1, splitedPhrase.ExtraURL1)
			if err != nil {
				err = errors.Wrap(err, "Error Saving ExtraPhrase1 Cache")
			}
		}
	}

	if splitedPhrase.ExtraURL2 == "" && splitedPhrase.ExtraPhrase2 != "" {
		splitedPhrase.ExtraURL2, err = h.SpeechSynth(context.Background(), splitedPhrase.ExtraPhrase2, rpc.SynthOptions{Filename: splitedPhrase.ExtraHash2})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.ExtraPhrase2, splitedPhrase.ExtraHash2, splitedPhrase.ExtraURL2)
			if err != nil {
				err = errors.Wrap(err, "Error Saving ExtraPhrase1 Cache")
			}
		}
	}

	if splitedPhrase.ExtraURL3 == "" && splitedPhrase.ExtraPhrase3 != "" {
		splitedPhrase.ExtraURL3, err = h.SpeechSynth(context.Background(), splitedPhrase.ExtraPhrase3, rpc.SynthOptions{Filename: splitedPhrase.ExtraHash3})
		if err != nil {
			err = errors.Wrap(err, "failed to synth speech")
		} else {
			err = h.DB.SavePhraseHash(splitedPhrase.ExtraPhrase3, splitedPhrase.ExtraHash3, splitedPhrase.ExtraURL3)
			if err != nil {
				err = errors.Wrap(err, "Error Saving ExtraPhrase1 Cache")
			}
		}
	}
	if err != nil {
		return err
	}
	return nil
}

//if splitedPhrase.PhraseURL == "" && splitedPhrase.Phrase != "" {
//	splitedPhrase.PhraseURL, err = h.SpeechSynth(context.Background(), call.Text, rpc.SynthOptions{Filename: splitedPhrase.PhraseHash})
//	if err != nil {
//		return errors.Wrap(err, "failed to synth speech")
//	}
//
//	err = h.DB.SavePhraseHash(splitedPhrase.Phrase, splitedPhrase.PhraseHash, splitedPhrase.PhraseURL)
//	if err != nil {
//		log.Errorf("Error saving hash: %s", err)
//	}
//}
//
//if splitedPhrase.PostfixHash == "" && splitedPhrase.Postfix != "" {
//	splitedPhrase.PostfixHash, err = h.SpeechSynth(context.Background(), call.Text, rpc.SynthOptions{Filename: splitedPhrase.PostfixHash})
//	if err != nil {
//		return errors.Wrap(err, "failed to synth speech")
//	}
//	err = h.DB.SavePhraseHash(splitedPhrase.Postfix, splitedPhrase.PostfixHash, splitedPhrase.PostfixURL)
//	if err != nil {
//		log.Errorf("Error saving hash: %s", err)
//	}
//}
