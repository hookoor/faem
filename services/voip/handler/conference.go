package handler

import (
	"context"
	"fmt"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

type ConferenceRepository interface {
	// GetOperatorChannel возвращает каналы оператора
	GetOperatorChannel(operator string) (string, string, string, error)
}

func (h *Handler) GetCallChannel(ctx context.Context, callData *proto.ConferenceRequest) (proto.ConferenceData, error) {
	extraChanel, chanel, callId, err := h.DB.GetOperatorChannel(callData.Operator)
	if err != nil {
		return proto.ConferenceData{}, err
	}

	return proto.ConferenceData{
		Channel:        chanel,
		Exten:          "9" + callData.Operator,
		ExtraChannel:   extraChanel,
		Number:         callData.Number,
		Operator:       callData.Operator,
		ConfID:         fmt.Sprintf(callId),
		SecondOperator: callData.SecondOperator,
	}, nil
}
