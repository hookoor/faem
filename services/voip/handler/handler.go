package handler

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/voip/config"
	"gitlab.com/faemproject/backend/faem/services/voip/metrics"
	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
	"gitlab.com/faemproject/backend/faem/services/voip/rpc"
	"sync"
	"time"
)

var (
	wg sync.WaitGroup
)

type Handler struct {
	SynthClient  SynthClient
	AsterClient  *rpc.AsterClient
	DB           Repository
	Pub          Publisher
	VoipCfg      config.VoipCfg
	Metrics      metrics.PrometheusData
	AsterConfigs rpc.AsterConfig
	Config       config.HandlerConfig
	//init statisctic collector to prometh
	MemeberBuffer     []string
	TickerNotificator map[string]NotificatorData
}

type NotificatorData struct {
	CallTime    time.Time
	PhoneNumber string
	SayText     string
}

type Repository interface {
	GetClientGroup(call *proto.AsterEvent) (string, error)
	CallEventsRepository
	AutoCallerRepository
	ConferenceRepository
	// SaveCRMOrder saving order data from CRM
	SaveCRMOrder(order structures.Order, state string) error
	//UpdateOrderState update, if exists order state. If order not found will not return error
	UpdateOrderState(UUID, state string) (models.CRMOrder, error)
	// GetOperatorLine returns line number of provided operator number
	GetOperatorLine(operator string) (string, error)
	//CachedPhrases return list of phrases that are cached
	CachedPhrases(phrase *proto.SplittedPhrase) error
	SavePhraseHash(phrase, hash, url string) error
	SaveDriverData(order *structures.Order) error
	OrderByPhone(phone string) (models.CRMOrder, error)
}

func (h *Handler) InitNotificatorTicker() {
	h.TickerNotificator = make(map[string]NotificatorData)
	go h.syncOrders(10 * time.Second)
}

// каждые timeout секунд забирает заказы из постгреса и заливает в BigQuery
func (h *Handler) syncOrders(timeout time.Duration) {
	defer wg.Done()
	for {
		select {
		case <-time.After(timeout):
			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "Notificator ticker",
			})

			tn := time.Now()
			for k, v := range h.TickerNotificator {
				if v.CallTime.After(tn) {
					call := proto.AutoOutCallRequest{}
					call.Text = "Здравствуйте! $ Вам поступил новый заказ в систему ФАЕМ Еда. $ Пожалуйста проверьте новые заказы"
					call.Phone = v.PhoneNumber
					err := h.SynthAndCall(call)
					if err != nil {
						log.WithField("reason", "failed to syth and make call").Error(err)
						continue
					}
					log.WithFields(logrus.Fields{
						"reason": "making product notification call",
						"phone":  v.PhoneNumber,
					})
					delete(h.TickerNotificator, k)
				}
			}
		}

	}
}
