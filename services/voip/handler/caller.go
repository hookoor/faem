package handler

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/voip/models"
	"gitlab.com/faemproject/backend/faem/services/voip/proto"
)

// FIXME: possibly, remove
//type AsterClient interface {
//	MakeCall(phone, url, uuid string) error
//	MakeConference(callData *proto.ConferenceData)
//	DriverClientCall(callRequest *structures.NewConferenceData) error
//	Logoff() error
//}

type AutoCallerRepository interface {
	// Creating auto call entry
	CreateAutoCall(call *proto.AutoOutCallRequest, splitedPhrases *proto.SplittedPhrase) (string, error)
	// Setting AutoCall State - Calling
	SetCallingStateAC(hash string) error
	// Get AutoCall by hash
	GetAutoCall(hash string) (models.AutoCall, error)
	// ReCallActualState checking is state of autoCall is actual for Re-Calling
	ReCallActualState(autoCall models.AutoCall, maxAttempts int) error
	// Setting cancel state for auto call
	SetCancelStateAC(hash, comment string) error
	GetOrderUUIDByHash(hash string) (string, error)
	SetSchedulledStateAC(hash string) error
}

// NewAutoCall saving new AutoCall to DB
func (h *Handler) NewAutoCall(call *proto.AutoOutCallRequest, splitedPhrases *proto.SplittedPhrase) (string, error) {
	if err := call.ValidatePhone(); err != nil {
		return "", errors.Wrap(err, "failed validate phone number")
	}
	hash, err := h.DB.CreateAutoCall(call, splitedPhrases)
	if err != nil {
		return "", errors.Wrap(err, "failed to create new call entry")
	}
	return hash, nil
}

// CallerHandler Makes call
func (h *Handler) CallerHandler(phone, hash string, sp *proto.SplittedPhrase, callType ...string) error {
	if err := proto.ValidatePhone(phone); err != nil {
		return errors.Wrap(err, "failed validate phone number")
	}
	var err error
	_, drvPhone := h.orderStateAndDriver(phone)
	if len(callType) > 0 {
		err = h.AsterClient.MakeCall(phone, hash, sp, callType[0], drvPhone)
	} else {
		err = h.AsterClient.MakeCall(phone, hash, sp)
	}
	if err != nil {
		return errors.Wrap(err, "failed to make new autocall")
	}
	return nil
}
