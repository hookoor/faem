package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/voip/rpc"
)

const stasCollectorPerionSec = 3

func (h *Handler) InitStasCollector() {
	go h.startStatsCollector()
}

func (h *Handler) startStatsCollector() {
	//dur := time.Duration(int64(stasCollectorPerionSec))
	for {
		data, err := getQeueStat(h.AsterClient)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event": "Collecting Aster Stats",
			}).Error(err)
		}
		r := parseSummaryResponse(data)
		h.Metrics.AsterLoggedIn.Set(float64(r.AsterLoggedIn))
		h.Metrics.AsterCallers.Set(float64(r.AsterCallers))
		h.Metrics.AsterAvailable.Set(float64(r.AsterAvailable))

		data, err = getQueueStatus(h.AsterClient)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event": "Collecting Aster Stats",
			}).Error(err)
		}
		rr, _ := asterQueueParamsParse(string(data))
		h.Metrics.AsterCallers.Set(float64(rr.QueueParams.Calls))
		h.Metrics.AsterHoldTime.Set(float64(rr.QueueParams.HoldTime))
		h.Metrics.AsterTalkTime.Set(float64(rr.QueueParams.TalkTime))
		h.Metrics.AsterCompleted.Set(float64(rr.QueueParams.Completed))
		h.Metrics.AsterAbandoned.Set(float64(rr.QueueParams.Abandoned))

		// есть три сценария:
		// 1) участник был в очереди и удалился (есть в буфере нет в новом)
		// 2) участник был в очереди и остается (есть и в буфере и новом)
		// 3) учасника не было в очерди и он появился (нет в буфере но есть в новом)
		// мы можем буфер закинуть в мапу

		act := make(map[string]string)

		for _, i := range h.MemeberBuffer {
			act[i] = "remove"
		}

		//очищаем буфер
		h.MemeberBuffer = nil
		for _, i := range rr.QueueMember {
			if _, ok := act[i.Name]; !ok {
				act[i.Name] = "add"
			} else {
				act[i.Name] = "keep"
			}
			h.MemeberBuffer = append(h.MemeberBuffer, i.Name)
		}

		for k, v := range act {
			switch v {
			case "add":
				h.Metrics.OperatorsOnLine.WithLabelValues(k).Set(1)
			case "remove":
				h.Metrics.OperatorsOnLine.WithLabelValues(k).Set(0)
			}
		}

		time.Sleep(stasCollectorPerionSec * time.Second)

	}
}

type asterSumData struct {
	AsterLoggedIn  int
	AsterAvailable int
	AsterCallers   int
}

func parseSummaryResponse(data []byte) asterSumData {
	var result asterSumData
	txtData := string(data)
	splText := strings.Split(txtData, "\r\n")
	for _, v := range splText {
		pos := strings.Index(v, ": ")
		if pos == -1 {
			continue
		}
		val := v[pos+2:]
		arg := v[:pos]

		switch arg {
		case "LoggedIn":
			result.AsterLoggedIn, _ = strconv.Atoi(val)
		case "Available":
			result.AsterAvailable, _ = strconv.Atoi(val)
		case "Callers":
			result.AsterCallers, _ = strconv.Atoi(val)
		}
	}
	return result
}

func getQeueStat(client *rpc.AsterClient) ([]byte, error) {
	req, err := http.NewRequest("GET", client.Config.AsterURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Creating Aster New Request failed")
	}

	q := req.URL.Query()
	q.Add("action", "QueueSummary")
	q.Add("Queue", "opers")

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "Failed Roundtrip to Aster")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return nil, errors.Errorf("requesting Asterisk to %s failed with status: %d", client.Config.AsterURL, resp.StatusCode)
		}
		return nil, errors.Errorf("requesting Asterisk to %s failed with status: %d and body: %s", client.Config.AsterURL, resp.StatusCode, string(bodyStr))
	}

	return bodyStr, nil
}

// ---
type queueParams struct {
	Event             string
	Queue             string
	Max               int
	Strategy          string
	Calls             int //
	HoldTime          int // среднее время ответа на звонок
	TalkTime          int // среднее время разговора
	Completed         int // отвеченных
	Abandoned         int // пропущенных
	ServiceLevel      int
	ServicelevelPerf  float64
	ServicelevelPerf2 float64
	Weight            int
}
type queueMember struct {
	Event          string
	Queue          string
	Name           string
	Location       string
	StateInterface string
	Membership     string
	Penalty        int
	CallsTaken     int // сколько звонков ответил
	LastCall       int
	LastPause      int
	InCall         int // если занят то 1
	Status         int // 0 - UNKNOWN; 1 - NOT_INUSE; 2 - INUSE; 3 - BUSY; 4 - INVALID; 5 - UNAVAILABLE; 6 - RINGING; 7 - RINGINUSE; 8 - ONHOLD
	// 0 - неизвестный; 1 - не используется; 2 - в использовании; 3 - занят; 4 - инвалид; 5 - недоступен; 6 - вызов; 7 - использование звонка; 8 - на удержании
	Paused       int // на паузе или нет
	PausedReason string
	Wrapuptime   int
}
type queueStatusComplete struct {
	Event     string
	EventList string
	ListItems int
}
type asterQueueStatus struct {
	Response            string
	EventList           string
	Message             string
	QueueParams         queueParams
	QueueMember         []queueMember
	QueueStatusComplete queueStatusComplete
}

//q.Add("action", "QueueStatus")

func getQueueStatus(client *rpc.AsterClient) ([]byte, error) {

	req, err := http.NewRequest("GET", client.Config.AsterURL, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Creating Aster New Request failed")
	}

	q := req.URL.Query()
	q.Add("action", "QueueStatus")
	q.Add("Queue", "opers")

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "Failed Roundtrip to Aster")
	}
	defer resp.Body.Close()

	bodyStr, err := ioutil.ReadAll(resp.Body)
	if resp.StatusCode >= http.StatusBadRequest {
		if err != nil {
			return nil, errors.Errorf("requesting Asterisk to %s failed with status: %d", client.Config.AsterURL, resp.StatusCode)
		}
		return nil, errors.Errorf("requesting Asterisk to %s failed with status: %d and body: %s", client.Config.AsterURL, resp.StatusCode, string(bodyStr))
	}

	return bodyStr, nil
}

func asterQueueParamsParse(text string) (asterQueueStatus, error) {
	var res asterQueueStatus
	objects := []map[string]string{}
	filds := map[string]string{}
	var unit int = 0

	fl := strings.Split(text, "\r\n")
	for _, item := range fl {
		f := strings.Split(item, ": ")
		if len(f) < 2 {
			unit++
			objects = append(objects, filds)
			filds = map[string]string{}
			continue
		}
		if len(f) > 2 {
			return res, fmt.Errorf("invalid param text")
		}
		filds[f[0]] = f[1]
	}

	for _, item := range objects {
		if item["Response"] == "Success" {
			res.Response = item["Response"]
			res.EventList = item["EventList"]
			res.Message = item["Message"]
		}
		if item["Event"] == "QueueParams" {
			res.fillQueueParams(item)
		}
		if item["Event"] == "QueueMember" {
			var help queueMember
			help.fillQueueMember(item)
			res.QueueMember = append(res.QueueMember, help)
		}
		if item["Event"] == "QueueStatusComplete" {
			res.fillQueueStatusComplete(item)
		}
	}

	return res, nil
}

func (aqs *asterQueueStatus) fillQueueParams(params map[string]string) {
	aqs.QueueParams.Event = params["Event"]
	aqs.QueueParams.Queue = params["Queue"]
	aqs.QueueParams.Max = intconv(params["Max"])
	aqs.QueueParams.Strategy = params["Strategy"]
	aqs.QueueParams.Calls = intconv(params["Calls"])
	aqs.QueueParams.HoldTime = intconv(params["Holdtime"])
	aqs.QueueParams.TalkTime = intconv(params["TalkTime"])
	aqs.QueueParams.Completed = intconv(params["Completed"])
	aqs.QueueParams.Abandoned = intconv(params["Abandoned"])
	aqs.QueueParams.ServiceLevel = intconv(params["ServiceLevel"])
	aqs.QueueParams.ServicelevelPerf = floatconv(params["ServicelevelPerf"])
	aqs.QueueParams.ServicelevelPerf2 = floatconv(params["ServicelevelPerf2"])
	aqs.QueueParams.Weight = intconv(params["Weight"])

}
func (aqs *queueMember) fillQueueMember(params map[string]string) {
	aqs.Event = params["Event"]
	aqs.Queue = params["Queue"]
	aqs.Name = params["Name"]
	aqs.Location = params["Location"]
	aqs.StateInterface = params["StateInterface"]
	aqs.Membership = params["Membership"]
	aqs.PausedReason = params["PausedReason"]
	aqs.Penalty = intconv(params["Penalty"])
	aqs.CallsTaken = intconv(params["CallsTaken"])
	aqs.LastCall = intconv(params["LastCall"])
	aqs.LastPause = intconv(params["LastPause"])
	aqs.InCall = intconv(params["InCall"])
	aqs.Status = intconv(params["Status"])
	aqs.Paused = intconv(params["Paused"])
	aqs.Wrapuptime = intconv(params["Wrapuptime"])
}
func (aqs *asterQueueStatus) fillQueueStatusComplete(params map[string]string) {
	aqs.QueueStatusComplete.Event = params["Event"]
	aqs.QueueStatusComplete.EventList = params["EventList"]
	aqs.QueueStatusComplete.ListItems = intconv(params["ListItems"])
}

func intconv(el string) int {
	i, _ := strconv.ParseInt(el, 10, 64)
	return int(i)
}
func floatconv(el string) float64 {
	i, _ := strconv.ParseFloat(el, 64)
	return i
}
