package proto

import (
	"context"
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

const (
	CallbackPost3DS = "/api/v2/client/payments/post3ds"
)

type NewBankCard struct {
	Name       string `json:"name"`
	CardSuffix string `json:"card_suffix"`
	Cryptogram string `json:"cryptogram"`
	UserUUID   string `json:"-"`
	IpAddress  string `json:"-"`
}

type GatewayCurrency string

const (
	GatewayCurrencyRUB GatewayCurrency = "RUB"
)

type ApproveBankTransaction struct {
	TransactionID int    `json:"transaction_id"`
	PaymentReq    string `json:"payment_req"`
	AcsUrl        string `json:"acs_url"`
	CallbackUrl   string `json:"callback_url"`
}

type (
	GatewayNewBankCard struct {
		Amount               float64         `json:"Amount"`
		Currency             GatewayCurrency `json:"Currency"`
		IpAddress            string          `json:"IpAddress"`
		Name                 string          `json:"Name"`
		CardCryptogramPacket string          `json:"CardCryptogramPacket"`
		AccountId            string          `json:"AccountId"`
		Description          string          `json:"Description"`

		CardSuffix string `json:"-"`
	}

	GatewayNewBankCardResponse struct {
		Card    *models.BankCard        `json:"card,omitempty"`
		Approve *ApproveBankTransaction `json:"approve,omitempty"`
	}
)

func (c *GatewayNewBankCard) IdempotencyKey() string {
	return fmt.Sprintf("%s.%s.%s", rpc.PaymentGatewayOpRegisterCard, c.AccountId, c.CardSuffix)
}

func (c *GatewayNewBankCard) Create(ctx context.Context, client *rpc.PaymentGatewayClient) (*GatewayNewBankCardResponse, error) {
	resp, err := client.CallMethod(rpc.ChargePaymentURL, c.IdempotencyKey(), c)
	if err != nil {
		return nil, errors.Wrap(err, "failed to call a method")
	}

	// In case of successful transaction
	var res GatewayNewBankCardResponse
	if resp.Success {
		card := models.BankCard{UserUUID: c.AccountId}
		card.CardType, _ = resp.Model["CardType"].(string)
		card.CardSuffix, _ = resp.Model["CardLastFour"].(string)
		card.PaymentToken, _ = resp.Model["Token"].(string)
		res.Card = &card
		return &res, nil
	}

	// In case of failed transaction
	if resp.HasMessage() {
		return nil, errors.New(resp.Message)
	}
	if resp.Need3DS() {
		approve := ApproveBankTransaction{CallbackUrl: CallbackPost3DS}
		txId, _ := resp.Model["TransactionId"].(float64)
		approve.TransactionID = int(txId)
		approve.PaymentReq, _ = resp.Model["PaReq"].(string)
		approve.AcsUrl, _ = resp.Model["AcsUrl"].(string)
		res.Approve = &approve
		return &res, nil
	}

	reason, _ := resp.Model["Reason"].(string)
	code, _ := resp.Model["ReasonCode"].(float64)
	logs.LoggerForContext(ctx).
		WithFields(logrus.Fields{
			"reason":      reason,
			"reason_code": code,
		}).Warn("can't create bank card in gateway")
	msg, _ := resp.Model["CardHolderMessage"].(string)
	return nil, errors.New(msg)
}

func (r *GatewayNewBankCardResponse) Need3DS() bool {
	return r.Approve != nil
}
