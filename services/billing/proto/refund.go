package proto

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

type Refund struct {
	TransactionId int     `json:"TransactionId"`
	Amount        float64 `json:"Amount"`
}

func (r *Refund) IdempotencyKey() string {
	return fmt.Sprintf("%s.%d", rpc.PaymentGatewayOpRefund, r.TransactionId)
}

func (r *Refund) Refund(_ context.Context, client *rpc.PaymentGatewayClient) error {
	resp, err := client.CallMethod(rpc.RefundURL, r.IdempotencyKey(), r)
	if err != nil {
		return errors.Wrap(err, "failed to call a method")
	}

	// In case of successful transaction
	if resp.Success {
		return nil
	}

	// In case of failed transaction
	if resp.HasMessage() {
		return errors.New(resp.Message)
	}
	return errors.New("failed to refund")
}
