package proto

import "github.com/pkg/errors"

type ForceGroup string

const (
	ForceGroupAll ForceGroup = "all"
	ForceGroupID  ForceGroup = "id"
)

func (g ForceGroup) Validate() error {
	if g == ForceGroupAll || g == ForceGroupID {
		return nil
	}
	return errors.New("invalid group type")
}

func (g ForceGroup) RequireID() bool {
	return g == ForceGroupID
}

type ForceArgs struct {
	Group  ForceGroup `json:"group"`
	ID     []string   `json:"id"`
	Amount float64    `json:"amount"`
}

func (a ForceArgs) Validate() error {
	if err := a.Group.Validate(); err != nil {
		return err
	}
	if a.Group.RequireID() && len(a.ID) < 1 {
		return errors.New("ids array must present")
	}
	return nil
}
