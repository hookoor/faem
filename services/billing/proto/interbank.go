package proto

import (
	"errors"
	"time"

	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

type InterbankDocumentRequest struct {
	TaxiParkUUID string `json:"taxi_park_uuid"`
	PeriodStart  int64  `json:"period_start"`
	PeriodEnd    int64  `json:"period_end"`
}

func (r *InterbankDocumentRequest) ToModel() (models.InterbankDocumentRequest, error) {
	var result models.InterbankDocumentRequest

	if r.TaxiParkUUID == "" {
		return models.InterbankDocumentRequest{}, errors.New("Выберите таксопарк")
	}
	result.TaxiParkUUID = r.TaxiParkUUID

	if r.PeriodStart == 0 {
		return models.InterbankDocumentRequest{}, errors.New("Вы должны указать начало периода")
	}
	result.PeriodStart = time.Unix(r.PeriodStart, 0)

	if r.PeriodEnd == 0 {
		return models.InterbankDocumentRequest{}, errors.New("Вы должны указать конец периода")
	}
	result.PeriodEnd = time.Unix(r.PeriodEnd, 0)

	return result, nil
}
