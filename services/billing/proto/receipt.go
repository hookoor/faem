package proto

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

type ReceiptData struct {
	structures.ReceiptData
}

func (r ReceiptData) Validate() error {
	if r.OrderUUID == "" {
		return errors.New("empty order uuid provided")
	}
	if r.ClientUUID == "" {
		return errors.New("emprt client uuid provided")
	}
	if r.Amount < 0 {
		return errors.Errorf("amount must be a non-negative number: %f", r.Amount)
	}
	return nil
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type GatewayReceiptType string

const (
	GatewayReceiptTypeIncome GatewayReceiptType = "Income"
)

type GatewayTaxationSystem int

const (
	GatewayTaxationSystemUNS6 GatewayTaxationSystem = 1
)

type GatewayReceiptObject int

const (
	GatewayReceiptObjectService GatewayReceiptObject = 4
)

type GatewayReceiptMethod int

const (
	GatewayReceiptMethodFullPay GatewayReceiptMethod = 4
)

type GatewayReceiptItem struct {
	Label    string               `json:"Label"`
	Price    float64              `json:"Price"`
	Quantity float64              `json:"Quantity"`
	Amount   float64              `json:"Amount"`
	Vat      *int                 `json:"Vat"`
	Object   GatewayReceiptObject `json:"Object"`
	Method   GatewayReceiptMethod `json:"Method"`
}

type GatewayReceiptAmounts struct {
	Electronic float64 `json:"Electronic"`
}

type GatewayCustomerReceipt struct {
	Items          []GatewayReceiptItem  `json:"Items"`
	Amounts        GatewayReceiptAmounts `json:"Amounts"`
	TaxationSystem GatewayTaxationSystem `json:"TaxationSystem"`
}

type GatewayReceiptData struct {
	Inn             string                 `json:"Inn"`
	Type            GatewayReceiptType     `json:"Type"`
	CustomerReceipt GatewayCustomerReceipt `json:"CustomerReceipt"`
	InvoiceId       string                 `json:"InvoiceId"`
	AccountId       string                 `json:"AccountId"`

	IdKey string `json:"-"`
}

func (r *GatewayReceiptData) IdempotencyKey() string {
	return fmt.Sprintf(
		"%s.%s.%s.%s", rpc.PaymentGatewayOpGenerateReceipt, r.AccountId, r.InvoiceId, r.IdKey,
	)
}

func (r *GatewayReceiptData) Generate(ctx context.Context, client *rpc.PaymentGatewayClient) error {
	return r.request(ctx, client, rpc.GenerateReceiptURL, r.IdempotencyKey())
}

func (r *GatewayReceiptData) request(
	ctx context.Context,
	client *rpc.PaymentGatewayClient,
	url,
	idempotencyKey string,
) error {
	resp, err := client.CallMethod(url, idempotencyKey, r)
	if err != nil {
		return errors.Wrap(err, "failed to call a method")
	}

	// In case of successful transaction
	if resp.Success {
		return nil
	}

	// In case of failed transaction
	if resp.HasMessage() {
		return errors.New(resp.Message)
	}
	return errors.New("failed to generate a receipt")
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type ReceiptHook struct {
	InvoiceId string `json:"InvoiceId" form:"InvoiceId" query:"InvoiceId"` // OrderUUID
	Url       string `json:"Url" form:"Url" query:"Url"`
	QrCodeUrl string `json:"QrCodeUrl" form:"QrCodeUrl" query:"QrCodeUrl"`
}
