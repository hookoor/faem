package proto

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

type NewTransfer struct {
	structures.NewTransfer

	// Inner service fields
	ExternalID string `json:"-"`
}

func (t NewTransfer) Validate() error {
	if t.IdempotencyKey == "" {
		return errors.New("empty idempotency key provided")
	}

	if err := t.PayerAccountType.Validate(); err != nil {
		return errors.Wrap(err, "payer account")
	}

	if t.PayeeAccountType != "" {
		if err := t.PayeeAccountType.Validate(); err != nil {
			return errors.Wrap(err, "payee account")
		}
	}

	if err := t.TransferType.Validate(); err != nil {
		return err
	}

	if t.TransferType.RequiresPayee() {
		if t.PayeeUUID == "" {
			return errors.New("empty payee id provided")
		}
		if err := t.PayeeType.Validate(); err != nil {
			return err
		}
	}

	if t.TransferType.RequiresPayer() {
		if t.PayerUUID == "" {
			return errors.New("empty payer id provided")
		}
		if err := t.PayerType.Validate(); err != nil {
			return err
		}
	}

	if t.Amount < 0 {
		return errors.Errorf("amount must be a non-negative number: %f", t.Amount)
	}
	if t.Amount > models.MaxTransferAmount {
		return errors.New("amount is absurdly high")
	}

	if t.HasFee() && t.Fee.Amount < 0 {
		return errors.Errorf("fee amount must be a non-negative number: %f", t.Fee.Amount)
	}
	if t.HasFee() && t.Fee.Amount > models.MaxTransferAmount {
		return errors.New("fee amount is absurdly high")
	}
	return nil
}

func (t NewTransfer) HasFee() bool {
	return t.Fee.AccountType != ""
}

func (t NewTransfer) GetPayeeAccountType() structures.BillingAccountType {
	if t.PayeeAccountType != "" {
		return t.PayeeAccountType
	}
	return t.PayerAccountType
}
