package proto

import (
	"context"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

type PaymentApproveArg struct {
	MD     int    `json:"MD" form:"MD" query:"MD"`
	PayRes string `json:"PaRes" form:"PaRes" query:"PaRes"`
}

type PaymentApprove struct {
	TransactionId int    `json:"TransactionId"`
	PayRes        string `json:"PaRes"`
}

func (a *PaymentApprove) IdempotencyKey() string {
	return fmt.Sprintf("%s.%d", rpc.PaymentGatewayOpApproveCard, a.TransactionId)
}

func (a *PaymentApprove) Approve(ctx context.Context, client *rpc.PaymentGatewayClient) (*models.BankCard, error) {
	resp, err := client.CallMethod(rpc.Post3DSURL, a.IdempotencyKey(), a)
	if err != nil {
		return nil, errors.Wrap(err, "failed to call a method")
	}

	// In case of successful transaction
	var res models.BankCard
	if resp.Success {
		res.UserUUID, _ = resp.Model["AccountId"].(string)
		res.CardType, _ = resp.Model["CardType"].(string)
		res.CardSuffix, _ = resp.Model["CardLastFour"].(string)
		res.PaymentToken, _ = resp.Model["Token"].(string)
		return &res, nil
	}

	// In case of failed transaction
	if resp.HasMessage() {
		return nil, errors.New(resp.Message)
	}
	// If we need to approve a transaction
	if resp.Need3DS() {
		return nil, errors.New("Ошибка подтверждения 3DS. Обратитесь в тех. поддержку")
	}

	reason, _ := resp.Model["Reason"].(string)
	code, _ := resp.Model["ReasonCode"].(float64)
	logs.LoggerForContext(ctx).
		WithFields(logrus.Fields{
			"reason":      reason,
			"reason_code": code,
		}).Warn("can't approve bank card in gateway")
	msg, _ := resp.Model["CardHolderMessage"].(string)
	return nil, errors.New(msg)
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type CardTransferCompleted struct {
	PayerUUID   string  `json:"payer_uuid"`
	SubjectUUID string  `json:"subject_uuid"`
	Amount      float64 `json:"amount"`
}
