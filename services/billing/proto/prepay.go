package proto

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

const (
	MaxPrepayAmount = 999999
)

type PrepayOrder struct {
	structures.PrepayOrder
}

func (p PrepayOrder) Validate() error {
	if p.IdempotencyKey == "" {
		return errors.New("empty idempotency key provided")
	}
	if p.PayerUUID == "" {
		return errors.New("empty payer id provided")
	}
	if p.SubjectUUID == "" {
		return errors.New("empty subject id provided")
	}
	if p.Amount <= 0 {
		return errors.Errorf("amount must be a positive number: %f", p.Amount)
	}
	if p.Amount > MaxPrepayAmount {
		return errors.New("amount is absurdly high")
	}
	return nil
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type GatewayPayment struct {
	Amount      float64           `json:"Amount"`
	Currency    GatewayCurrency   `json:"Currency"`
	AccountId   string            `json:"AccountId"`
	Token       string            `json:"Token"`
	InvoiceId   string            `json:"InvoiceId"`
	Description string            `json:"Description"`
	JsonData    map[string]string `json:"JsonData"`

	IdKey string `json:"-"`
}

func (p *GatewayPayment) PrepayIdempotencyKey() string {
	return fmt.Sprintf("%s.%s.%s.%s", rpc.PaymentGatewayOpPrepay, p.AccountId, p.InvoiceId, p.IdKey)
}

func (p *GatewayPayment) PayIdempotencyKey() string {
	return fmt.Sprintf("%s.%s.%s.%s", rpc.PaymentGatewayOpPayment, p.AccountId, p.InvoiceId, p.IdKey)
}

func (p *GatewayPayment) Prepay(ctx context.Context, client *rpc.PaymentGatewayClient) (*SuccessfulGatewayPrepay, error) {
	return p.request(ctx, client, rpc.PrepayTokenURL, p.PrepayIdempotencyKey())
}

func (p *GatewayPayment) Pay(ctx context.Context, client *rpc.PaymentGatewayClient) (*SuccessfulGatewayPrepay, error) {
	return p.request(ctx, client, rpc.ChargeTokenURL, p.PayIdempotencyKey())
}

func (p *GatewayPayment) request(
	ctx context.Context,
	client *rpc.PaymentGatewayClient,
	url,
	idempotencyKey string,
) (*SuccessfulGatewayPrepay, error) {
	resp, err := client.CallMethod(url, idempotencyKey, p)
	if err != nil {
		return nil, errors.Wrap(err, "failed to call a method")
	}

	// In case of successful transaction
	var res SuccessfulGatewayPrepay
	if resp.Success {
		txId, _ := resp.Model["TransactionId"].(float64)
		res.TransactionID = int(txId)
		return &res, nil
	}

	// In case of failed transaction
	if resp.HasMessage() {
		return nil, errors.New(resp.Message)
	}
	// If we need to approve a transaction
	if resp.Need3DS() {
		return nil, errors.New("Требуется подтверждение 3DS. Обратитесь в тех. поддержку")
	}

	reason, _ := resp.Model["Reason"].(string)
	code, _ := resp.Model["ReasonCode"].(float64)
	logs.LoggerForContext(ctx).
		WithFields(logrus.Fields{
			"reason":      reason,
			"reason_code": code,
		}).Warn("can't perform a prepay in gateway")
	msg, _ := resp.Model["CardHolderMessage"].(string)
	return nil, errors.New(msg)
}

type SuccessfulGatewayPrepay struct {
	TransactionID int `json:"transaction_id"`
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

const (
	JsonDataPayeeUUID = "payee_uuid"
)

type GatewayConfirm struct {
	TransactionId int               `json:"TransactionId"`
	Amount        float64           `json:"Amount"`
	JsonData      map[string]string `json:"JsonData"`
}

func (c *GatewayConfirm) ConfirmIdempotencyKey() string {
	return fmt.Sprintf("%s.%d", rpc.PaymentGatewayOpConfirm, c.TransactionId)
}

func (c *GatewayConfirm) CancelIdempotencyKey() string {
	return fmt.Sprintf("%s.%d", rpc.PaymentGatewayOpCancel, c.TransactionId)
}

func (c *GatewayConfirm) Confirm(ctx context.Context, client *rpc.PaymentGatewayClient) error {
	return c.request(ctx, client, rpc.ConfirmURL, c.ConfirmIdempotencyKey())
}

func (c *GatewayConfirm) Cancel(ctx context.Context, client *rpc.PaymentGatewayClient) error {
	return c.request(ctx, client, rpc.CancelURL, c.CancelIdempotencyKey())
}

// Cloudpayments errors
const (
	CPFormatError     = 5030
	CPSystemError     = 5096
	CPUnableToProcess = 5204
)

const (
	retryInterval = time.Second * 15
	retryTimeout  = time.Minute * 5
)

func (c *GatewayConfirm) request(ctx context.Context, client *rpc.PaymentGatewayClient, url, idempotencyKey string) error {
	var (
		log = logs.Eloger.WithFields(logrus.Fields{
			"idempotency_key": idempotencyKey,
			"url":             url,
		})

		resp *rpc.GatewayResponse
		err  error

		code int
		ok   bool

		ticker  = time.NewTicker(retryInterval)
		timeout = time.NewTimer(retryTimeout)
	)
	defer func() {
		ticker.Stop()
		timeout.Stop()
	}()

	for {
		resp, err = client.CallMethod(url, idempotencyKey, c)
		if err != nil {
			return errors.Wrap(err, "failed to call a method")
		}

		// In case of successful transaction
		if resp.Success {
			return nil
		}

		code, ok = resp.Model["ReasonCode"].(int)
		if !ok {
			data, _ := json.Marshal(resp)
			log.WithField("response", string(data)).Error("assert failure")
			return errors.New("cannot assert ReasonCode to int")
		}

		switch code {
		case CPFormatError, CPSystemError, CPUnableToProcess:
			log.Warnf("cannot dial %s, retrying in %v\n", url, retryInterval)

			select {
			case <-timeout.C:
				return errors.Errorf("dial [%s] timeout (%v) error: %s code: %d", url, retryTimeout, resp.Message, code)
			case <-ticker.C:
				continue
			}
		}

		break
	}

	if resp.HasMessage() {
		return errors.Errorf("failed to confirm transaction. error: %s code: %v idempotency_key: %s", resp.Message, code, idempotencyKey)
	}

	// If we need to approve a transaction
	if resp.Need3DS() {
		return errors.New("Требуется подтверждение 3DS. Обратитесь в тех. поддержку")
	}

	msg, _ := resp.Model["CardHolderMessage"].(string)
	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"reason":      resp.Model["Reason"],
		"reason_code": resp.Model["ReasonCode"],
	}).Warnf("can't perform a prepay in gateway. msg: %s", msg)

	return errors.New(msg)
}
