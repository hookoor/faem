package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type AccountMetadata struct {
	DriverAlias int `json:"driver_alias,omitempty"`
	ImportID    int `json:"import_id,omitempty"`
}

type Account struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID          string                        `json:"id"`
	AccountType structures.BillingAccountType `json:"account_type"`
	UserUUID    string                        `json:"user_uuid"`
	UserType    structures.BillingUserType    `json:"user_type"`
	Phone       string                        `json:"phone"`
	Meta        AccountMetadata               `json:"meta,omitempty"`
	CreatedAt   time.Time                     `json:"-"`
	UpdatedAt   time.Time                     `json:"-"`
	DeletedAt   *time.Time                    `json:"-" pg:",soft_delete"`

	Entry *Entry `json:"-" sql:"-"` // belongs to
}

// GetDeletedAt -
func (ac *Account) GetDeletedAt() time.Time {
	if ac.DeletedAt == nil {
		return time.Time{}
	}
	return *ac.DeletedAt
}
