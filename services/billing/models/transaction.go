package models

import "time"

type Transaction struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID        string     `json:"id"`
	CreditID  string     `json:"credit_id"`
	DebitID   string     `json:"debit_id"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" pg:",soft_delete"`
}

// GetDeletedAt -
func (t *Transaction) GetDeletedAt() time.Time {
	if t.DeletedAt == nil {
		return time.Time{}
	}
	return *t.DeletedAt
}
