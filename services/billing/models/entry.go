package models

import (
	"time"
)

type EntryType string

const (
	EntryTypeCredit EntryType = "credit"
	EntryTypeDebit  EntryType = "debit"
)

func (t EntryType) GetTranslatedName() string {
	switch t {
	case EntryTypeCredit:
		return "кредит"
	case EntryTypeDebit:
		return "дебет"
	}
	return "неизвестный"
}

type Entry struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID        string     `json:"id"`
	EntryType EntryType  `json:"entry_type"`
	AccountID string     `json:"account_id"`
	Amount    float64    `json:"amount"`
	Balance   float64    `json:"balance"`
	Force     bool       `json:"force"`
	CreatedAt time.Time  `json:"-"`
	UpdatedAt time.Time  `json:"-"`
	DeletedAt *time.Time `json:"-" pg:",soft_delete"`
}

func (e *Entry) GetBalance() float64 {
	if e == nil {
		return 0
	}
	return e.Balance
}

// GetDeletedAt -
func (e *Entry) GetDeletedAt() time.Time {
	if e.DeletedAt == nil {
		return time.Time{}
	}
	return *e.DeletedAt
}
