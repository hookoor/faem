package models

import "time"

type BankTransfer struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID            string
	UserUUID      string
	TransactionId string `sql:",notnull"`
	PaymentReq    string
	AcsUrl        string
	Amount        float64
	SubjectUUID   string
	PrepaidAt     time.Time
	ConfirmedAt   time.Time
	CreatedAt     time.Time
	RefundedAt    time.Time
}
