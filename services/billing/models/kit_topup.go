package models

import (
	"encoding/xml"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
)

const (
	kitPrefix = "kit-"
)

type KitCommand string

const (
	KitCommandCheck KitCommand = "check"
	KitCommandPay   KitCommand = "pay"
)

func (c KitCommand) validate() error {
	if c == KitCommandCheck || c == KitCommandPay {
		return nil
	}
	return errors.Errorf("invalid kit command provided: %s", c)
}

type TopupKit struct {
	Command       KitCommand `json:"command"`
	TransactionID string     `json:"transaction_id"`
	Account       string     `json:"account"`
	Sum           float64    `json:"sum"`
	TxnDate       string     `json:"txn_date"`

	prefixedID string
	phone      string
}

func (t *TopupKit) Prepare() error {
	if err := t.Command.validate(); err != nil {
		return KitTopupResult{
			error:  err,
			Result: TopUpCodeUnknown,
		}
	}
	if t.TransactionID == "" {
		return KitTopupResult{
			error:  errors.New("empty transaction id provided"),
			Result: TopUpCodeUnknown,
		}
	}
	t.prefixedID = kitPrefix + t.TransactionID
	// Verify phone
	phone := FormatExternalPhone(t.Account)
	phone, err := phoneverify.NumberVerify("+7" + phone)
	if err != nil {
		return KitTopupResult{
			error:  err,
			Result: TopUpCodeInvalidAccountFormat,
		}
	}
	t.phone = phone
	// Validate sum
	if t.Sum <= 0 {
		return KitTopupResult{
			error:  errors.New("sum must be a positive value"),
			Result: TopUpCodeSumIsTooSmall,
		}
	}
	if t.Sum > MaxTransferAmount {
		return KitTopupResult{
			error:  errors.New("sum is absurdly high"),
			Result: TopUpCodeSumIsTooBig,
		}
	}
	return nil
}

func (t *TopupKit) PrefixedID() string {
	return t.prefixedID
}

func (t *TopupKit) Phone() string {
	return t.phone
}

type KitTopupResult struct {
	XMLName xml.Name `xml:"response" json:"-"`

	error              `xml:"-" json:"-"`
	KitTransactionID   string    `xml:"kit_txn_id" json:"-"`
	InnerTransactionID string    `xml:"prv_txn,omitempty" json:"-"`
	Amount             float64   `xml:"sum,omitempty" json:"-"`
	Result             TopUpCode `xml:"result" json:"result"`
}

func KitTopupResultFromError(err error) KitTopupResult {
	if err == nil {
		return KitTopupResult{Result: TopUpCodeOK}
	}

	tr, ok := err.(KitTopupResult)
	if ok {
		return tr
	}
	return KitTopupResult{
		error:  err,
		Result: TopUpCodeUnknown,
	}
}
