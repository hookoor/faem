package models

const (
	MaxTransferAmount = 999999
)

type TopUpCode int

const (
	TopUpCodeOK                   TopUpCode = 0
	TopUpCodeTmpError             TopUpCode = 1
	TopUpCodeInvalidAccountFormat TopUpCode = 4
	TopUpCodeAccountNotFound      TopUpCode = 5
	TopUpCodeForbidden            TopUpCode = 7
	TopUpCodeTechForbidden        TopUpCode = 8
	TopUpCodeAccountInactive      TopUpCode = 79
	TopUpCodePaymentInterrupted   TopUpCode = 90
	TopUpCodeSumIsTooSmall        TopUpCode = 241
	TopUpCodeSumIsTooBig          TopUpCode = 242
	TopUpCodeFailedToCheckAccount TopUpCode = 243
	TopUpCodeUnknown              TopUpCode = 300
)
