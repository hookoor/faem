package models

import (
	"time"
)

type Log struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID        int         `json:"id"`
	Data      interface{} `json:"data"`
	CreatedAt time.Time   `json:"-"`
}

type LogItem struct {
	Event string `json:"event"`
}

type TopupKitLog struct {
	LogItem
	TopupKit
	Result interface{} `json:"result"`
}

type TopupQiwiLog struct {
	LogItem
	TopupQiwi
	Result interface{} `json:"result"`
}
