package models

import (
	"time"
)

type InterbankDocumentRequest struct {
	TaxiParkUUID string
	PeriodStart  time.Time
	PeriodEnd    time.Time
}

type InterbankTransactions struct {
	AmountTotal float64 `json:"amount_total"`
	AmountFee   float64 `json:"amount_fee"`
}

func (t InterbankTransactions) GetAmountContragent() float64 {
	return t.AmountTotal - t.AmountFee
}
