package models

import "time"

type BankCard struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID           string     `json:"id"`
	UserUUID     string     `json:"user_uuid"`
	CardType     string     `json:"card_type"`
	CardSuffix   string     `json:"card_suffix"`
	PaymentToken string     `json:"-"`
	CreatedAt    time.Time  `json:"-"`
	DeletedAt    *time.Time `json:"-" pg:",soft_delete"`
}
