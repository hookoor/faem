package models

const (
	maxExternalAliasLength = 7
)

func NeedToProxy(account string) bool {
	return len(account) <= maxExternalAliasLength
}
