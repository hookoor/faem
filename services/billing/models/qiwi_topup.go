package models

import (
	"encoding/xml"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
)

const (
	qiwiPrefix = "qiwi-"
)

type QiwiCommand string

const (
	QiwiCommandCheck QiwiCommand = "check"
)

func (c QiwiCommand) validate() error {
	if c == QiwiCommandCheck {
		return nil
	}
	return errors.Errorf("invalid qiwi command provided: %s", c)
}

type TopupQiwi struct {
	Command       QiwiCommand `json:"command"`
	TransactionID string      `json:"transaction_id"`
	Account       string      `json:"account"`
	Sum           float64     `json:"sum"`

	prefixedID string
	phone      string
}

func (t *TopupQiwi) Prepare() error {
	if err := t.Command.validate(); err != nil {
		return QiwiTopupResult{
			error:  err,
			Result: TopUpCodeUnknown,
		}
	}
	if t.TransactionID == "" {
		return QiwiTopupResult{
			error:  errors.New("empty transaction id provided"),
			Result: TopUpCodeUnknown,
		}
	}
	t.prefixedID = qiwiPrefix + t.TransactionID
	// Verify phone
	phone := FormatExternalPhone(t.Account)
	phone, err := phoneverify.NumberVerify("+7" + phone)
	if err != nil {
		return QiwiTopupResult{
			error:  err,
			Result: TopUpCodeInvalidAccountFormat,
		}
	}
	t.phone = phone
	// Validate sum
	if t.Sum <= 0 {
		return QiwiTopupResult{
			error:  errors.New("sum must be a positive value"),
			Result: TopUpCodeSumIsTooSmall,
		}
	}
	if t.Sum > MaxTransferAmount {
		return QiwiTopupResult{
			error:  errors.New("sum is absurdly high"),
			Result: TopUpCodeSumIsTooBig,
		}
	}
	return nil
}

func (t *TopupQiwi) PrefixedID() string {
	return t.prefixedID
}

func (t *TopupQiwi) Phone() string {
	return t.phone
}

type QiwiTopupResult struct {
	XMLName xml.Name `xml:"response" json:"-"`

	error             `xml:"-" json:"-"`
	QiwiTransactionID string    `xml:"osmp_txn_id" json:"-"`
	Result            TopUpCode `xml:"result" json:"result"`
}

func QiwiTopupResultFromError(err error) QiwiTopupResult {
	if err == nil {
		return QiwiTopupResult{Result: TopUpCodeOK}
	}

	tr, ok := err.(QiwiTopupResult)
	if ok {
		return tr
	}
	return QiwiTopupResult{
		error:  err,
		Result: TopUpCodeUnknown,
	}
}
