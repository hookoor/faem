package models

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
)

const (
	qiwi1Prefix = "qiwi1-"
)

type Qiwi1Command string

const (
	Qiwi1CommandCheck Qiwi1Command = "check"
	Qiwi1CommandPay   Qiwi1Command = "pay"
)

func (c Qiwi1Command) validate() error {
	if c == Qiwi1CommandCheck || c == Qiwi1CommandPay {
		return nil
	}
	return errors.Errorf("invalid qiwi1 command provided: %s", c)
}

type TopupQiwi1 struct {
	Command       Qiwi1Command `json:"command"`
	TransactionID string       `json:"transaction_id"`
	Account       string       `json:"account"`
	Sum           float64      `json:"sum"`

	prefixedID string
	phone      string
}

func (t *TopupQiwi1) Prepare() error {
	if err := t.Command.validate(); err != nil {
		return QiwiTopupResult{
			error:  err,
			Result: TopUpCodeUnknown,
		}
	}
	if t.TransactionID == "" {
		return QiwiTopupResult{
			error:  errors.New("empty transaction id provided"),
			Result: TopUpCodeUnknown,
		}
	}
	t.prefixedID = qiwi1Prefix + t.TransactionID
	// Verify phone
	phone := FormatExternalPhone(t.Account)
	phone, err := phoneverify.NumberVerify("+7" + phone)
	if err != nil {
		return QiwiTopupResult{
			error:  err,
			Result: TopUpCodeInvalidAccountFormat,
		}
	}
	t.phone = phone
	// Validate sum
	if t.Sum <= 0 {
		return QiwiTopupResult{
			error:  errors.New("sum must be a positive value"),
			Result: TopUpCodeSumIsTooSmall,
		}
	}
	if t.Sum > MaxTransferAmount {
		return QiwiTopupResult{
			error:  errors.New("sum is absurdly high"),
			Result: TopUpCodeSumIsTooBig,
		}
	}
	return nil
}

func (t *TopupQiwi1) PrefixedID() string {
	return t.prefixedID
}

func (t *TopupQiwi1) Phone() string {
	return t.phone
}
