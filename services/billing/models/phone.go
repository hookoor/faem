package models

import (
	"strings"
)

// FormatExternalPhone returns a phone if its length is less or equal then E164 format without leading code
func FormatExternalPhone(phone string) string {
	phone = strings.TrimSpace(phone)
	if strings.HasPrefix(phone, "+7") {
		return phone[2:]
	}
	if strings.HasPrefix(phone, "7") || strings.HasPrefix(phone, "8") {
		return phone[1:]
	}
	return phone
}
