package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type Transfer struct {
	tableName struct{} `pg:",discard_unknown_columns"`

	ID             string                         `json:"id"`
	TransferType   structures.BillingTransferType `json:"transfer_type"`
	Amount         float64                        `json:"amount"`
	TransactionIds []string                       `json:"transaction_ids" sql:",array"`
	Description    string                         `json:"description"`
	ExternalID     string                         `json:"external_id" sql:"external_id"`
	Meta           structures.TransferMetadata    `json:"meta"`
	CreatedAt      time.Time                      `json:"-"`
	UpdatedAt      time.Time                      `json:"-"`
	DeletedAt      *time.Time                     `json:"-" pg:",soft_delete"`
}

// GetDeletedAt -
func (t *Transfer) GetDeletedAt() time.Time {
	if t.DeletedAt == nil {
		return time.Time{}
	}
	return *t.DeletedAt
}
