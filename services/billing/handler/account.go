package handler

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	initialDriverBalance        = 100
	initialClientBonusesBalance = 0
	msgInitialReward            = "Вознаграждение за регистрацию в приложении"
)

type AccountRepository interface {
	AccountExistsByPhoneAndUserType(ctx context.Context, phone string, userType structures.BillingUserType) (bool, error)
	CreateAccounts(ctx context.Context, accounts ...*models.Account) error
	UpdateDriverAccountPhone(ctx context.Context, driver *structures.Driver) error
	CloseDriverAccount(ctx context.Context, driver structures.DeletedObject) error
	UpdateClientAccountPhone(ctx context.Context, client *structures.Client) error
	CloseClientAccount(ctx context.Context, client structures.DeletedObject) error
	UpdateUserUUIDByPhoneAndUserType(
		ctx context.Context, userUUID, phone string, userType structures.BillingUserType,
	) ([]models.Account, error)
	GetAccountsByPhoneAndUserType(
		ctx context.Context, phone string, userType structures.BillingUserType,
	) ([]models.Account, error)
	GetAccountsWithEntryByIDsAndType(
		ctx context.Context, userIDs []string, userType structures.BillingUserType,
	) (map[string][]*models.Account, error)
}

func (h *Handler) OpenDriverAccount(ctx context.Context, driver *structures.Driver) (
	cashAcc models.Account, cardAcc models.Account, bonusAcc models.Account, err error,
) {
	if driver.UUID == "" {
		err = errors.New("empty driver uuid provided")
		return
	}
	if driver.Phone == "" {
		err = errors.New("empty driver phone provided")
		return
	}

	cashAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeCash,
		UserUUID:    driver.UUID,
		UserType:    structures.UserTypeDriver,
		Phone:       driver.Phone,
		Meta: models.AccountMetadata{
			DriverAlias: driver.Alias,
		},
	}
	cardAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeCard,
		UserUUID:    driver.UUID,
		UserType:    structures.UserTypeDriver,
		Phone:       driver.Phone,
		Meta: models.AccountMetadata{
			DriverAlias: driver.Alias,
		},
	}
	bonusAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeBonus,
		UserUUID:    driver.UUID,
		UserType:    structures.UserTypeDriver,
		Phone:       driver.Phone,
		Meta: models.AccountMetadata{
			DriverAlias: driver.Alias,
		},
	}
	if err = h.DB.CreateAccounts(ctx, &cashAcc, &cardAcc, &bonusAcc); err != nil {
		err = errors.Wrap(err, "failed to create driver accounts")
		return
	}
	return

	// Do not reward a new driver
	//err = h.createTopUpTransfer(ctx, proto.NewTransfer{
	//	NewTransfer: structures.NewTransfer{
	//		IdempotencyKey:   structures.GenerateUUID(),
	//		PayerAccountType: structures.AccountTypeBonus,
	//		TransferType:     structures.TransferTypeTopUp,
	//		PayeeUUID:        driver.UUID,
	//		PayeeType:        structures.UserTypeDriver,
	//		Amount:           initialDriverBalance,
	//		Description:      msgInitialReward,
	//	},
	//})
	//err = errors.Wrap(err, "failed to reward a new driver")
	//return
}

func (h *Handler) UpdateDriverAccount(ctx context.Context, driver *structures.Driver) error {
	if driver == nil {
		return errors.New("nil driver provided")
	}
	if driver.UUID == "" {
		return errors.New("empty driver uuid provided")
	}
	err := h.DB.UpdateDriverAccountPhone(ctx, driver)
	return errors.Wrap(err, "failed to update driver account")
}

func (h *Handler) DeleteDriverAccount(ctx context.Context, driver structures.DeletedObject) error {
	if driver.UUID == "" {
		return errors.New("empty driver uuid provided")
	}
	err := h.DB.CloseDriverAccount(ctx, driver)
	return errors.Wrap(err, "failed to close driver account")
}

// GetTaxiParkAccount calls func OpenOrGetTaxiParkAccount and return needed account type
func (h *Handler) GetTaxiParkAccount(ctx context.Context, tpUUID string, accType structures.BillingAccountType) (models.Account, error) {
	var neededAcc models.Account
	cash, card, bon, err := h.OpenOrGetTaxiParkAccount(ctx, tpUUID)
	switch accType {
	case structures.AccountTypeBonus:
		neededAcc = bon
	case structures.AccountTypeCard:
		neededAcc = card
	case structures.AccountTypeCash:
		neededAcc = cash
	default:
		neededAcc = cash
	}
	return neededAcc, err
}

func (h *Handler) OpenOrGetTaxiParkAccount(ctx context.Context, tpUUID string) (
	cashAcc models.Account, cardAcc models.Account, bonusAcc models.Account, err error,
) {
	job := h.jobs.GetJobQueue(openClientAccJob, maxParallelOpenClientAccAllowed)
	err = job.Execute(clientIDHash(tpUUID), func() error {
		tpAccExists, _ := h.DB.AccountExistsByPhoneAndUserType(ctx, tpUUID, structures.UserTypeTaxiPark) // don't care about errors
		if tpAccExists {
			var accs []models.Account
			accs, err = h.DB.GetAccountsByPhoneAndUserType(ctx, tpUUID, structures.UserTypeTaxiPark)
			if err != nil {
				err = errors.Wrap(err, "failed to get taxipark acc")
				return err
			}
			// Map accounts
			for _, acc := range accs {
				switch acc.AccountType {
				case structures.AccountTypeBonus:
					bonusAcc = acc
				case structures.AccountTypeCard:
					cardAcc = acc
				default:
					cashAcc = acc
				}
			}
			return nil
		}

		// We don't have an account yet, create one
		cashAcc = models.Account{
			ID:          structures.GenerateUUID(),
			AccountType: structures.AccountTypeCash,
			UserUUID:    tpUUID,
			UserType:    structures.UserTypeTaxiPark,
			Phone:       tpUUID,
		}
		cardAcc = models.Account{
			ID:          structures.GenerateUUID(),
			AccountType: structures.AccountTypeCard,
			UserUUID:    tpUUID,
			UserType:    structures.UserTypeTaxiPark,
			Phone:       tpUUID,
		}
		bonusAcc = models.Account{
			ID:          structures.GenerateUUID(),
			AccountType: structures.AccountTypeBonus,
			UserUUID:    tpUUID,
			UserType:    structures.UserTypeTaxiPark,
			Phone:       tpUUID,
		}
		err = h.DB.CreateAccounts(ctx, &cashAcc, &cardAcc, &bonusAcc)
		err = errors.Wrap(err, "failed to create taxi park accounts")
		return err
	})
	return
}

func (h *Handler) OpenClientAccount(ctx context.Context, client *structures.Client) (
	cashAcc models.Account, cardAcc models.Account, bonusAcc models.Account, err error,
) {
	// If a client with a provided phone already exists (already has called to us), just update the uuid
	clientAccExists, _ := h.DB.AccountExistsByPhoneAndUserType(ctx, client.MainPhone, structures.UserTypeClient) // don't care about errors
	if clientAccExists {
		// We already know this client
		var accs []models.Account
		accs, err = h.DB.UpdateUserUUIDByPhoneAndUserType(ctx, client.UUID, client.MainPhone, structures.UserTypeClient)
		if err != nil {
			err = errors.Wrap(err, "failed to update client uuid")
			return
		}
		// Map accounts
		for _, acc := range accs {
			switch acc.AccountType {
			case structures.AccountTypeBonus:
				bonusAcc = acc
			case structures.AccountTypeCard:
				cardAcc = acc
			default:
				cashAcc = acc
			}
		}
		return
	}

	// We don't have an account yet, create one
	cashAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeCash,
		UserUUID:    client.UUID,
		UserType:    structures.UserTypeClient,
		Phone:       client.MainPhone,
	}
	cardAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeCard,
		UserUUID:    client.UUID,
		UserType:    structures.UserTypeClient,
		Phone:       client.MainPhone,
	}
	bonusAcc = models.Account{
		ID:          structures.GenerateUUID(),
		AccountType: structures.AccountTypeBonus,
		UserUUID:    client.UUID,
		UserType:    structures.UserTypeClient,
		Phone:       client.MainPhone,
	}
	err = h.DB.CreateAccounts(ctx, &cashAcc, &cardAcc, &bonusAcc)
	err = errors.Wrap(err, "failed to create clients accounts")
	return
}

func (h *Handler) UpdateClientAccount(ctx context.Context, client *structures.Client) error {
	if client == nil {
		return errors.New("nil client provided")
	}
	if client.UUID == "" {
		return errors.New("empty client uuid provided")
	}
	err := h.DB.UpdateClientAccountPhone(ctx, client)
	return errors.Wrap(err, "failed to update client account")
}

func (h *Handler) DeleteClientAccount(ctx context.Context, client structures.DeletedObject) error {
	if client.UUID == "" {
		return errors.New("empty client uuid provided")
	}
	err := h.DB.CloseClientAccount(ctx, client)
	return errors.Wrap(err, "failed to close client account")
}

func (h *Handler) GetAccountsInfo(
	ctx context.Context, userIDs []string, userType structures.BillingUserType,
) (structures.BalancesInfo, error) {
	if len(userIDs) < 1 {
		return structures.BalancesInfo{}, errors.New("empty user ids provided")
	}

	var (
		accounts map[string][]*models.Account
		err      error
	)
	accounts, err = h.DB.GetAccountsWithEntryByIDsAndType(ctx, userIDs, userType)
	if err != nil {
		return structures.BalancesInfo{}, errors.Wrap(err, "failed to get user accounts")
	}

	result := structures.BalancesInfo{UserAccounts: make(map[string]structures.BillingAccountsInfo)}
	for key, val := range accounts {
		accInfo := structures.BillingAccountsInfo{
			Accounts: make(map[structures.BillingAccountType]structures.BillingAccountInfo),
		}
		for _, acc := range val {
			protoAcc := structures.BillingAccountInfo{Balance: acc.Entry.GetBalance()}
			accInfo.Accounts[acc.AccountType] = protoAcc
		}
		result.UserAccounts[key] = accInfo
	}
	return result, nil
}

func (h *Handler) RewardRegisteredClient(ctx context.Context, client *structures.Client) error {
	if initialClientBonusesBalance <= 0 {
		return nil
	}
	err := h.createTopUpTransfer(ctx, proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     structures.TransferTypeTopUp,
			PayeeUUID:        client.MainPhone,
			PayeeType:        structures.UserTypeClient,
			Amount:           initialClientBonusesBalance,
			Description:      msgInitialReward,
		},
	})
	if err != nil {
		return errors.Wrap(err, "failed to reward a new client")
	}
	return nil
}
