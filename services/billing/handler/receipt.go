package handler

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

func (h *Handler) GenerateReceipt(ctx context.Context, receipt proto.ReceiptData) error {
	if err := receipt.Validate(); err != nil {
		return err
	}

	totalPrice := receipt.Amount
	gatewayReceipt := proto.GatewayReceiptData{
		Inn:       h.gateClient.Inn,
		Type:      proto.GatewayReceiptTypeIncome,
		InvoiceId: receipt.OrderUUID,
		AccountId: receipt.ClientUUID,
		CustomerReceipt: proto.GatewayCustomerReceipt{
			Items: []proto.GatewayReceiptItem{
				{
					Label:    receipt.Label,
					Price:    totalPrice,
					Quantity: 1,
					Amount:   totalPrice,
					Object:   proto.GatewayReceiptObjectService,
					Method:   proto.GatewayReceiptMethodFullPay,
				},
			},
			Amounts: proto.GatewayReceiptAmounts{
				Electronic: totalPrice,
			},
			TaxationSystem: proto.GatewayTaxationSystemUNS6,
		},

		IdKey: string(receipt.GatewayKey),
	}
	if err := gatewayReceipt.Generate(ctx, h.gateClient); err != nil {
		return errors.Wrap(err, "failed to generate a receipt in a gateway")
	}
	return nil
}

func (h *Handler) ReceiptGenerated(ctx context.Context, receipt proto.ReceiptHook) error {
	receiptHook := structures.ReceiptHook{
		OrderUUID: receipt.InvoiceId,
		Url:       receipt.Url,
		QrCodeUrl: receipt.QrCodeUrl,
	}
	return h.Pub.ReceiptGenerated(receiptHook)
}
