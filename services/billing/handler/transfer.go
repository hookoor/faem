package handler

import (
	"context"
	"math"
	"math/rand"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.uber.org/multierr"

	"gitlab.com/faemproject/backend/faem/pkg/crypto"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	transferJobs                    = "transfers"
	openClientAccJob                = "open_client_acc"
	maxParallelTransfersAllowed     = 97
	maxParallelOpenClientAccAllowed = 97
)

type TransferRepository interface {
	GetAccountByUserIDAndTypeAndUserType(
		ctx context.Context, userUUID string, accountType structures.BillingAccountType, userType structures.BillingUserType,
	) (models.Account, error)
	GetAccountByUserIDOrPhoneAndTypeAndUserType(
		ctx context.Context, userUUIDOrPhone string, accountType structures.BillingAccountType, userType structures.BillingUserType,
	) (models.Account, error)
	CreateEntries(ctx context.Context, tx *pg.Tx, entries ...*models.Entry) error
	CreateTransactions(ctx context.Context, tx *pg.Tx, transaction ...*models.Transaction) error
	CreateTransfer(ctx context.Context, tx *pg.Tx, transfer models.Transfer) error
	TransferExists(ctx context.Context, id string) (bool, error)
}

func (h *Handler) CreateTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	// Validate a request first
	if err := newTransfer.Validate(); err != nil {
		return err
	}

	// Process a transfer by its type
	switch newTransfer.TransferType {
	case structures.TransferTypeForceSet, structures.TransferTypeTopUp:
		err := h.createTopUpTransfer(ctx, newTransfer)
		return errors.Wrap(err, "failed to create a top up transfer")
	case structures.TransferTypeWithdraw:
		err := h.createWithdrawTransfer(ctx, newTransfer)
		return errors.Wrap(err, "failed to create a withdraw transfer")
	case structures.TransferTypePayment:
		if newTransfer.PayerAccountType == structures.AccountTypeCard && newTransfer.PayerType == structures.UserTypeClient {
			err := h.CreateCardPaymentTransfer(ctx, newTransfer)
			return errors.Wrap(err, "failed to create a card payment transfer")
		}
		err := h.createPaymentTransfer(ctx, newTransfer)
		return errors.Wrap(err, "failed to create a payment transfer")
	default:
		return errors.New("unknown transfer type")
	}
}

func (h *Handler) createTopUpTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	// Check if a transfer already exists
	exists, err := h.DB.TransferExists(ctx, newTransfer.IdempotencyKey)
	if err != nil {
		return err
	}
	if exists {
		return errors.New("a transfer with such idempotency key already exists")
	}

	// Get payee account ID
	var payeeAcc models.Account
	switch newTransfer.PayeeType {
	case structures.UserTypeClient:
		payeeAcc, err = h.getOrOpenClientAccount(
			ctx, newTransfer.PayeeUUID, newTransfer.Meta.ClientPhone, newTransfer.GetPayeeAccountType(), newTransfer.PayeeType,
		)
	case structures.UserTypeTaxiPark:
		payeeAcc, err = h.GetTaxiParkAccount(ctx, newTransfer.PayeeUUID, newTransfer.GetPayeeAccountType())
	default:
		payeeAcc, err = h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayeeUUID, newTransfer.GetPayeeAccountType(), newTransfer.PayeeType,
		)
	}
	if err != nil {
		return errors.Wrap(err, "failed to get a payee account")
	}

	// Get payer account ID
	payerID := h.Accounts.Gateway.GetAccountByType(newTransfer.PayerAccountType)

	return h.createTransferInTransaction(ctx, newTransfer, payeeAcc.ID, payerID)
}

func (h *Handler) createWithdrawTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	// Check if a transfer already exists
	exists, err := h.DB.TransferExists(ctx, newTransfer.IdempotencyKey)
	if err != nil {
		return err
	}
	if exists {
		return errors.New("a transfer with such idempotency key already exists")
	}

	// Get payee account ID
	payeeID := h.Accounts.Owner.GetAccountByType(newTransfer.GetPayeeAccountType())

	// Get payer account ID
	var payerAcc models.Account
	switch newTransfer.PayerType {
	case structures.UserTypeClient:
		payerAcc, err = h.getOrOpenClientAccount(
			ctx, newTransfer.PayerUUID, newTransfer.Meta.ClientPhone, newTransfer.PayerAccountType, newTransfer.PayerType,
		)
	case structures.UserTypeTaxiPark:
		payerAcc, err = h.GetTaxiParkAccount(ctx, newTransfer.PayerUUID, newTransfer.PayerAccountType)
	default:
		payerAcc, err = h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayerUUID, newTransfer.PayerAccountType, newTransfer.PayerType,
		)
	}
	if err != nil {
		return errors.Wrap(err, "failed to get a payer account")
	}

	// Allow overdraft for driver bonus account
	if newTransfer.PayerType == structures.UserTypeDriver &&
		newTransfer.PayerAccountType == structures.AccountTypeBonus &&
		(newTransfer.Meta.CurrentDriverBalance != nil && newTransfer.Amount > *newTransfer.Meta.CurrentDriverBalance) {
		payerOverdraftAcc, err := h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayerUUID, structures.AccountTypeCard, newTransfer.PayerType,
		)
		if err != nil {
			return errors.Wrap(err, "failed to get a payer account for overdraft")
		}

		corDriverBalance := math.Max(*newTransfer.Meta.CurrentDriverBalance, 0)
		diffAmount := newTransfer.Amount - corDriverBalance
		newTransfer.Amount = corDriverBalance

		// prevent card balance overdraft
		if newTransfer.Meta.CurrentDriverCardBalance != nil {
			cardBalance := math.Max(*newTransfer.Meta.CurrentDriverCardBalance, 0)
			if cardOverdraft := diffAmount - cardBalance; cardOverdraft > 0 {
				diffAmount = cardBalance
				newTransfer.Amount += cardOverdraft
			}
		}

		var bonusErr error
		if newTransfer.Amount > 0 {
			bonusErr = h.createTransferInTransaction(ctx, newTransfer, payeeID, payerAcc.ID)
		}

		overdraftTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				TransferType:     structures.TransferTypeWithdraw,
				PayerUUID:        newTransfer.PayerUUID,
				PayerType:        newTransfer.PayerType,
				PayerAccountType: structures.AccountTypeCard,
				Amount:           diffAmount,
				Description:      newTransfer.Description,
				Meta:             newTransfer.Meta,
			},
		}
		overdraftPayeeID := h.Accounts.Owner.GetAccountByType(overdraftTransfer.GetPayeeAccountType())
		overdraftErr := h.createTransferInTransaction(ctx, overdraftTransfer, overdraftPayeeID, payerOverdraftAcc.ID)
		return multierr.Append(bonusErr, overdraftErr)
	}

	return h.createTransferInTransaction(ctx, newTransfer, payeeID, payerAcc.ID)
}

type transferToPerform struct {
	transfer proto.NewTransfer
	payeeID  string
	payerID  string
}

func (h *Handler) createPaymentTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	// Check if a transfer already exists
	exists, err := h.DB.TransferExists(ctx, newTransfer.IdempotencyKey)
	if err != nil {
		return err
	}
	if exists {
		return errors.New("a transfer with such idempotency key already exists")
	}

	// Get payer account ID
	var payerAcc models.Account
	switch newTransfer.PayerType {
	case structures.UserTypeClient:
		payerAcc, err = h.getOrOpenClientAccount(
			ctx, newTransfer.PayerUUID, newTransfer.Meta.ClientPhone, newTransfer.PayerAccountType, newTransfer.PayerType,
		)
	case structures.UserTypeTaxiPark:
		var cash, card, bonus models.Account
		cash, card, bonus, err = h.OpenOrGetTaxiParkAccount(ctx, newTransfer.PayerUUID)
		switch newTransfer.PayerAccountType {
		case structures.AccountTypeBonus:
			payerAcc = bonus
		case structures.AccountTypeCard:
			payerAcc = card
		case structures.AccountTypeCash:
			payerAcc = cash
		default:
		}
	default:
		payerAcc, err = h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayerUUID, newTransfer.PayerAccountType, newTransfer.PayerType,
		)
	}
	if err != nil {
		return errors.Wrap(err, "failed to get a payer account")
	}

	var payeeAcc models.Account
	switch newTransfer.PayeeType {
	case structures.UserTypeClient:
		payeeAcc, err = h.getOrOpenClientAccount(
			ctx, newTransfer.PayeeUUID, newTransfer.Meta.ClientPhone, newTransfer.GetPayeeAccountType(), newTransfer.PayeeType,
		)
	case structures.UserTypeTaxiPark:
		var cash, card, bonus models.Account
		cash, card, bonus, err = h.OpenOrGetTaxiParkAccount(ctx, newTransfer.PayeeUUID)
		switch newTransfer.GetPayeeAccountType() {
		case structures.AccountTypeBonus:
			payeeAcc = bonus
		case structures.AccountTypeCard:
			payeeAcc = card
		case structures.AccountTypeCash:
			payeeAcc = cash
		default:
		}
	default:
		// Get payee account ID
		payeeAcc, err = h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayeeUUID, newTransfer.GetPayeeAccountType(), newTransfer.PayeeType,
		)
	}
	if err != nil {
		return errors.Wrap(err, "failed to get a payee account by user id and type")
	}

	// ------------------------------------------------------------------------
	// Allow overdraft for driver bonus account
	// ------------------------------------------------------------------------

	var transfersToPerform []transferToPerform
	if newTransfer.PayerType == structures.UserTypeDriver &&
		newTransfer.PayerAccountType == structures.AccountTypeBonus &&
		(newTransfer.Meta.CurrentDriverBalance != nil && newTransfer.Amount > *newTransfer.Meta.CurrentDriverBalance) {
		payerOverdraftAcc, err := h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, newTransfer.PayerUUID, structures.AccountTypeCard, newTransfer.PayerType,
		)
		if err != nil {
			return errors.Wrap(err, "failed to get a payer account for overdraft")
		}

		corDriverBalance := math.Max(*newTransfer.Meta.CurrentDriverBalance, 0)
		diffAmount := newTransfer.Amount - corDriverBalance
		newTransfer.Amount = corDriverBalance

		// prevent card balance overdraft
		if newTransfer.Meta.CurrentDriverCardBalance != nil {
			cardBalance := math.Max(*newTransfer.Meta.CurrentDriverCardBalance, 0)
			if cardOverdraft := diffAmount - cardBalance; cardOverdraft > 0 {
				diffAmount = cardBalance
				newTransfer.Amount += cardOverdraft
			}
		}

		if newTransfer.Amount > 0 {
			transfersToPerform = append(transfersToPerform, transferToPerform{
				transfer: newTransfer,
				payeeID:  payeeAcc.ID,
				payerID:  payerAcc.ID,
			})
		}

		overdraftTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				TransferType:     structures.TransferTypeWithdraw,
				PayerUUID:        newTransfer.PayerUUID,
				PayerType:        newTransfer.PayerType,
				PayerAccountType: structures.AccountTypeCard,
				Amount:           diffAmount,
				Description:      newTransfer.Description,
				Meta:             newTransfer.Meta,
			},
		}
		overdraftPayeeID := h.Accounts.Owner.GetAccountByType(overdraftTransfer.GetPayeeAccountType())
		transfersToPerform = append(transfersToPerform, transferToPerform{
			transfer: overdraftTransfer,
			payeeID:  overdraftPayeeID,
			payerID:  payerOverdraftAcc.ID,
		})
	} else {
		transfersToPerform = append(transfersToPerform, transferToPerform{
			transfer: newTransfer,
			payeeID:  payeeAcc.ID,
			payerID:  payerAcc.ID,
		})
	}

	// If fee is not provided then create only one transfer
	if !newTransfer.HasFee() {
		var err error
		for _, tr := range transfersToPerform {
			err = multierr.Append(err, h.createTransferInTransaction(ctx, tr.transfer, tr.payeeID, tr.payerID))
		}
		return err
	}

	// ------------------------------------------------------------------------
	// Else create double transfers
	// ------------------------------------------------------------------------

	feePayerAcc, err := h.DB.GetAccountByUserIDAndTypeAndUserType(
		ctx, newTransfer.Fee.PayerUUID, newTransfer.Fee.AccountType, newTransfer.Fee.PayerType,
	)
	if err != nil {
		return errors.Wrap(err, "failed to get a payee account by user id and type")
	}

	feePayeeID := h.Accounts.Owner.GetAccountByType(newTransfer.Fee.AccountType)
	feeTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   newTransfer.IdempotencyKey, // fee is more important than a payment itself
			PayerAccountType: newTransfer.Fee.AccountType,
			TransferType:     structures.TransferTypeWithdraw,
			PayerUUID:        feePayerAcc.UserUUID,
			PayerType:        feePayerAcc.UserType,
			Amount:           newTransfer.Fee.Amount,
			Description:      newTransfer.Description,
			Meta:             newTransfer.Meta,
		},
	}
	newTransfer.IdempotencyKey = structures.GenerateUUID() // payment is less important than a fee
	newTransfer.Meta.FeeAmount = feeTransfer.Amount        // super important to set this field to generate interbank reports later

	// ------------------------------------------------------------------------
	// Allow overdraft for driver bonus account
	// ------------------------------------------------------------------------

	if feeTransfer.PayerType == structures.UserTypeDriver &&
		feeTransfer.PayerAccountType == structures.AccountTypeBonus &&
		(feeTransfer.Meta.CurrentDriverBalance != nil && feeTransfer.Amount > *feeTransfer.Meta.CurrentDriverBalance) {
		payerOverdraftAcc, err := h.DB.GetAccountByUserIDAndTypeAndUserType(
			ctx, feeTransfer.PayerUUID, structures.AccountTypeCard, feeTransfer.PayerType,
		)
		if err != nil {
			return errors.Wrap(err, "failed to get a payer account for overdraft")
		}

		corDriverBalance := math.Max(*feeTransfer.Meta.CurrentDriverBalance, 0)
		diffAmount := feeTransfer.Amount - corDriverBalance
		feeTransfer.Amount = corDriverBalance

		// prevent card balance overdraft
		if feeTransfer.Meta.CurrentDriverCardBalance != nil {
			cardBalance := math.Max(*feeTransfer.Meta.CurrentDriverCardBalance, 0)
			if cardOverdraft := diffAmount - cardBalance; cardOverdraft > 0 {
				diffAmount = cardBalance
				feeTransfer.Amount += cardOverdraft
			}
		}

		var bonusErr error
		if feeTransfer.Amount > 0 {
			bonusErr = h.createTransferWithFeeInTransaction(
				ctx,
				feeTransfer, feePayeeID, feePayerAcc.ID,
				newTransfer, payeeAcc.ID, payerAcc.ID,
			)
		} else {
			bonusErr = h.createTransferInTransaction(ctx, newTransfer, payeeAcc.ID, payerAcc.ID)
		}

		overdraftTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				TransferType:     structures.TransferTypeWithdraw,
				PayerUUID:        feeTransfer.PayerUUID,
				PayerType:        feeTransfer.PayerType,
				PayerAccountType: structures.AccountTypeCard,
				Amount:           diffAmount,
				Description:      feeTransfer.Description,
				Meta:             feeTransfer.Meta,
			},
		}
		overdraftPayeeID := h.Accounts.Owner.GetAccountByType(overdraftTransfer.GetPayeeAccountType())
		overdraftErr := h.createTransferInTransaction(ctx, overdraftTransfer, overdraftPayeeID, payerOverdraftAcc.ID)
		return multierr.Append(bonusErr, overdraftErr)
	}

	// Create a payment and a fee transfers
	return h.createTransferWithFeeInTransaction(
		ctx,
		feeTransfer, feePayeeID, feePayerAcc.ID,
		newTransfer, payeeAcc.ID, payerAcc.ID,
	)
}

func (h *Handler) createTransferInTransaction(
	ctx context.Context,
	newTransfer proto.NewTransfer, payeeID, payerID string,
) error {
	// Don't care about zero transfers
	force := newTransfer.TransferType == structures.TransferTypeForceSet
	if newTransfer.Amount <= 0 && !force {
		return errors.Errorf("amount must be a positive number: %f", newTransfer.Amount)
	}

	// Prepare debit and credit entries
	debit := models.Entry{
		ID:        structures.GenerateUUID(),
		EntryType: models.EntryTypeDebit,
		AccountID: payeeID,
		Amount:    newTransfer.Amount,
		Force:     force,
	}
	credit := models.Entry{
		ID:        structures.GenerateUUID(),
		EntryType: models.EntryTypeCredit,
		AccountID: payerID,
		Amount:    -newTransfer.Amount,
	}

	// Avoid concurrency problems by putting jobs for the same objects in a queue
	job := h.jobs.GetJobQueue(transferJobs, maxParallelTransfersAllowed)
	return job.Execute(driverUUIDHash(newTransfer), func() error {
		txErr := h.DB.RunInTransaction(func(tx *pg.Tx) error {
			if err := h.DB.CreateEntries(ctx, tx, &debit, &credit); err != nil {
				return errors.Wrap(err, "failed to create entries")
			}

			// Prepare and create a transaction itself
			transaction := models.Transaction{
				ID:       structures.GenerateUUID(),
				CreditID: credit.ID,
				DebitID:  debit.ID,
			}
			if err := h.DB.CreateTransactions(ctx, tx, &transaction); err != nil {
				return errors.Wrap(err, "failed to create a transaction")
			}

			// Prepare and create a transfer
			transfer := models.Transfer{
				ID:             newTransfer.IdempotencyKey,
				TransferType:   newTransfer.TransferType,
				Amount:         newTransfer.Amount,
				TransactionIds: []string{transaction.ID},
				Description:    newTransfer.Description,
				ExternalID:     newTransfer.ExternalID, // remember to set an external id
				Meta:           newTransfer.Meta,
			}
			if err := h.DB.CreateTransfer(ctx, tx, transfer); err != nil {
				return errors.Wrap(err, "failed to create a transfer")
			}
			return nil
		})

		if txErr != nil {
			return txErr
		}

		go func() {
			completedTransfer := structures.CompletedTransfer{
				ID:               newTransfer.IdempotencyKey,
				TransferType:     newTransfer.TransferType,
				PayerUUID:        newTransfer.PayerUUID,
				PayerType:        newTransfer.PayerType,
				PayerAccountType: newTransfer.PayerAccountType,
				PayerBalance:     credit.GetBalance(),
				PayeeUUID:        newTransfer.PayeeUUID,
				PayeeType:        newTransfer.PayeeType,
				PayeeAccountType: newTransfer.GetPayeeAccountType(),
				PayeeBalance:     debit.GetBalance(),
				Amount:           newTransfer.Amount,
				Description:      newTransfer.Description,
				Meta:             newTransfer.Meta,
			}
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":           "publishing completed transfer to RMQ",
				"idempotency key": completedTransfer.ID,
			})
			if err := h.Pub.TransferCompleted(completedTransfer); err != nil {
				log.Error(err)
				return
			}
			log.Info("OK")
		}()
		return nil
	})
}

func (h *Handler) createTransferWithFeeInTransaction(
	ctx context.Context,
	feeTransfer proto.NewTransfer, feePayeeID, feePayerID string,
	newTransfer proto.NewTransfer, payeeID, payerID string,
) error {
	job := h.jobs.GetJobQueue(transferJobs, maxParallelTransfersAllowed)

	if feeTransfer.Amount > 0 { // don't care about zero transfers
		// Prepare fee transaction - for our case is the most important.
		// We can fail the payment itself, but must provide fee.
		feeDebit := models.Entry{
			ID:        structures.GenerateUUID(),
			EntryType: models.EntryTypeDebit,
			AccountID: feePayeeID,
			Amount:    feeTransfer.Amount,
		}
		feeCredit := models.Entry{
			ID:        structures.GenerateUUID(),
			EntryType: models.EntryTypeCredit,
			AccountID: feePayerID,
			Amount:    -feeTransfer.Amount,
		}

		// Avoid concurrency problems by putting jobs for the same objects in a queue
		jobErr := job.Execute(driverUUIDHash(newTransfer), func() error {
			txErr := h.DB.RunInTransaction(func(tx *pg.Tx) error {
				if err := h.DB.CreateEntries(ctx, tx, &feeDebit, &feeCredit); err != nil {
					return errors.Wrap(err, "failed to create fee entries")
				}

				// Prepare and create a transaction itself
				feeTransaction := models.Transaction{
					ID:       structures.GenerateUUID(),
					CreditID: feeCredit.ID,
					DebitID:  feeDebit.ID,
				}
				if err := h.DB.CreateTransactions(ctx, tx, &feeTransaction); err != nil {
					return errors.Wrap(err, "failed to create a fee transaction")
				}

				// Prepare and create a transfer
				feeTransfer := models.Transfer{
					ID:             feeTransfer.IdempotencyKey,
					TransferType:   feeTransfer.TransferType,
					Amount:         feeTransfer.Amount,
					TransactionIds: []string{feeTransaction.ID},
					Description:    feeTransfer.Description,
					Meta:           feeTransfer.Meta,
				}
				if err := h.DB.CreateTransfer(ctx, tx, feeTransfer); err != nil {
					return errors.Wrap(err, "failed to create a fee transfer")
				}
				return nil
			})

			if txErr != nil {
				return txErr
			}

			// Prepare debit and credit entries
			go func() {
				completedFeeTransfer := structures.CompletedTransfer{
					ID:               feeTransfer.IdempotencyKey,
					TransferType:     feeTransfer.TransferType,
					PayerUUID:        feeTransfer.PayerUUID,
					PayerType:        feeTransfer.PayerType,
					PayerAccountType: feeTransfer.PayerAccountType,
					PayerBalance:     feeCredit.GetBalance(),
					PayeeUUID:        feeTransfer.PayeeUUID,
					PayeeType:        feeTransfer.PayeeType,
					PayeeAccountType: feeTransfer.GetPayeeAccountType(),
					PayeeBalance:     feeDebit.GetBalance(),
					Amount:           feeTransfer.Amount,
					Description:      feeTransfer.Description,
					Meta:             feeTransfer.Meta,
				}
				log := logs.Eloger.WithFields(logrus.Fields{
					"event":               "publishing completed transfer to RMQ",
					"fee idempotency key": completedFeeTransfer.ID,
				})
				if err := h.Pub.TransferCompleted(completedFeeTransfer); err != nil {
					log.Error(err)
					return
				}
				log.Info("OK")
			}()
			return nil
		})

		if jobErr != nil {
			return jobErr
		}
	}

	// ------------------------------------------------------------------------
	// Same for the payment itself
	// ------------------------------------------------------------------------

	force := newTransfer.TransferType == structures.TransferTypeForceSet
	if newTransfer.Amount > 0 || force { // don't care about zero transfers
		debit := models.Entry{
			ID:        structures.GenerateUUID(),
			EntryType: models.EntryTypeDebit,
			AccountID: payeeID,
			Amount:    newTransfer.Amount,
			Force:     force,
		}
		credit := models.Entry{
			ID:        structures.GenerateUUID(),
			EntryType: models.EntryTypeCredit,
			AccountID: payerID,
			Amount:    -newTransfer.Amount,
		}

		// Avoid concurrency problems by putting jobs for the same objects in a queue
		return job.Execute(driverUUIDHash(newTransfer), func() error {
			txErr := h.DB.RunInTransaction(func(tx *pg.Tx) error {
				if err := h.DB.CreateEntries(ctx, tx, &debit, &credit); err != nil {
					return errors.Wrap(err, "failed to create entries")
				}

				// Prepare and create a transaction itself
				transaction := models.Transaction{
					ID:       structures.GenerateUUID(),
					CreditID: credit.ID,
					DebitID:  debit.ID,
				}
				if err := h.DB.CreateTransactions(ctx, tx, &transaction); err != nil {
					return errors.Wrap(err, "failed to create a transaction")
				}

				// Prepare and create a transfer
				transfer := models.Transfer{
					ID:             newTransfer.IdempotencyKey,
					TransferType:   newTransfer.TransferType,
					Amount:         newTransfer.Amount,
					TransactionIds: []string{transaction.ID},
					Description:    newTransfer.Description,
					Meta:           newTransfer.Meta,
				}
				if err := h.DB.CreateTransfer(ctx, tx, transfer); err != nil {
					return errors.Wrap(err, "failed to create a transfer")
				}
				return nil
			})

			if txErr != nil {
				return txErr
			}

			go func() {
				completedTransfer := structures.CompletedTransfer{
					ID:               newTransfer.IdempotencyKey,
					TransferType:     newTransfer.TransferType,
					PayerUUID:        newTransfer.PayerUUID,
					PayerType:        newTransfer.PayerType,
					PayerAccountType: newTransfer.PayerAccountType,
					PayerBalance:     credit.GetBalance(),
					PayeeUUID:        newTransfer.PayeeUUID,
					PayeeType:        newTransfer.PayeeType,
					PayeeAccountType: newTransfer.GetPayeeAccountType(),
					PayeeBalance:     debit.GetBalance(),
					Amount:           newTransfer.Amount,
					Description:      newTransfer.Description,
					Meta:             newTransfer.Meta,
				}
				log := logs.Eloger.WithFields(logrus.Fields{
					"event":           "publishing completed transfer to RMQ",
					"idempotency key": completedTransfer.ID,
				})
				if err := h.Pub.TransferCompleted(completedTransfer); err != nil {
					log.Error(err)
					return
				}
				log.Info("OK")
			}()
			return nil
		})
	}

	return nil
}

func (h *Handler) getOrOpenClientAccount(
	ctx context.Context,
	userUUID, userPhone string,
	accountType structures.BillingAccountType,
	userType structures.BillingUserType,
) (models.Account, error) {
	var clientAcc models.Account
	job := h.jobs.GetJobQueue(openClientAccJob, maxParallelOpenClientAccAllowed)
	err := job.Execute(clientIDHash(userUUID), func() error {
		var err error
		clientAcc, err = h.DB.GetAccountByUserIDOrPhoneAndTypeAndUserType(ctx, userUUID, accountType, userType)
		if err != nil {
			if errors.Cause(err) != pg.ErrNoRows { // expected error
				return errors.Wrap(err, "failed to get account by user id or phone and type")
			}
			// Create an account if a client does not exist
			var client structures.Client
			isUUID := uuid.FromStringOrNil(userUUID) != uuid.Nil
			if isUUID { // detect if uuid or phone provided
				client.UUID = userUUID
				client.MainPhone = userPhone // if exists
			} else {
				client.MainPhone = userUUID
			}
			cashAcc, cardAcc, bonusAcc, err := h.OpenClientAccount(ctx, &client)
			if err != nil {
				return errors.Wrap(err, "failed to open a client account")
			}
			switch accountType {
			case structures.AccountTypeBonus:
				clientAcc = bonusAcc
			case structures.AccountTypeCard:
				clientAcc = cardAcc
			default:
				clientAcc = cashAcc
			}
		}
		return nil
	})
	return clientAcc, err
}

func driverUUIDHash(transfer proto.NewTransfer) int {
	switch {
	case transfer.PayeeType == structures.UserTypeDriver:
		return crypto.FNV(transfer.PayeeUUID)
	case transfer.PayerType == structures.UserTypeDriver:
		return crypto.FNV(transfer.PayerUUID)
	}
	// Seems like a transaction does not affect a driver, use a random hash to avoid reducing parallelism
	return rand.Int()
}

func clientIDHash(id string) int {
	return crypto.FNV(id)
}
