package handler

import (
	"context"
	"math"
	"strconv"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"go.uber.org/multierr"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	msgDiffPayment   = "Доплата разницы между предоплатой и конечной стоимостью поездки"
	msgPenaltyClient = "Компенсация за отказ от выполняемого заказа"
	msgFullPrepay    = "Полная предоплата за заказ"
)

type BankTransferRepository interface {
	CreateBankTransfer(ctx context.Context, transfer *models.BankTransfer) error
	PrepayExists(ctx context.Context, id, subjectUUID string) (bool, error)
	FindPrepayByUserAndSubjectUUIDs(ctx context.Context, userUUID, subjectUUID string) (*models.BankTransfer, error)
	ConfirmPrepay(ctx context.Context, id string) error
	RefundPrepay(ctx context.Context, id string) error
}

func (h *Handler) ApproveClientBankCard(ctx context.Context, approve *proto.PaymentApprove) (*models.BankCard, error) {
	result, err := approve.Approve(ctx, h.gateClient)
	if err != nil {
		return nil, err
	}

	// If we approved successfully, try to refund the client and save a card
	go func() {
		refund := proto.Refund{
			TransactionId: approve.TransactionId,
			Amount:        initialMoneyBlock,
		}
		if err := refund.Refund(ctx, h.gateClient); err != nil {
			logs.LoggerForContext(ctx).Warn(err)
		}
	}()

	if err = h.DB.CreateClientBankCard(ctx, result); err != nil {
		return nil, errors.Wrap(err, "failed to create a client bank card")
	}
	return result, nil
}

func (h *Handler) PrepayOrder(ctx context.Context, prepay proto.PrepayOrder) (err error) {
	defer func() {
		if err != nil {
			go func() {
				log := logs.Eloger.WithFields(logrus.Fields{
					"event":           "publishing transfer type changed to RMQ",
					"idempotency key": prepay.IdempotencyKey,
				})
				if err := h.Pub.PaymentTypeChangedToCash(prepay.PrepayOrder); err != nil {
					log.Error(err)
					return
				}
				log.Debug("OK")
			}()
		}
	}()

	if err := prepay.Validate(); err != nil {
		return err
	}

	// Check if a prepay already exists
	exists, err := h.DB.PrepayExists(ctx, prepay.IdempotencyKey, prepay.SubjectUUID)
	if err != nil {
		return err
	}
	if exists {
		logs.Eloger.WithField("event", "prepay").
			Warn("prepay with such idempotency key already exists")
	}

	// Retrieve the payment token
	bankCard, err := h.DB.FindLatestClientBankCardByUserUUID(ctx, prepay.PayerUUID)
	if err != nil {
		return errors.Wrap(err, "failed to find client bank card for user")
	}

	// Try to block amount on the card
	gatePrepay := proto.GatewayPayment{
		Amount:      prepay.Amount,
		Currency:    proto.GatewayCurrencyRUB,
		AccountId:   prepay.PayerUUID,
		Token:       bankCard.PaymentToken,
		InvoiceId:   prepay.SubjectUUID,
		Description: "Оплата поездки",
		JsonData:    map[string]string{proto.JsonDataPayeeUUID: prepay.PayeeUUID},
	}
	result, err := gatePrepay.Prepay(ctx, h.gateClient)
	if err != nil {
		return errors.Wrap(err, "failed to perform a prepay")
	}

	// Save prepaid info to the database
	bankTransfer := models.BankTransfer{
		ID:            prepay.IdempotencyKey,
		UserUUID:      prepay.PayerUUID,
		TransactionId: strconv.Itoa(result.TransactionID),
		Amount:        prepay.Amount,
		SubjectUUID:   prepay.SubjectUUID,
		PrepaidAt:     time.Now(),
	}
	if err = h.DB.CreateBankTransfer(ctx, &bankTransfer); err != nil { // rare case
		err = errors.Wrap(err, "failed to insert prepayment transfer")
		// Refund client
		cancel := proto.GatewayConfirm{TransactionId: result.TransactionID}
		cerr := cancel.Cancel(ctx, h.gateClient)
		cerr = errors.Wrap(cerr, "failed to cancel prepay")
		return multierr.Append(err, cerr)
	}
	return nil
}

func (h *Handler) CreateCardPaymentTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	// If a transfer has already been prepaid, just confirm the transaction
	if newTransfer.Meta.Prepaid {
		return h.createPrepaidCardPaymentTransfer(ctx, newTransfer)
	}

	// If a completely new transfer - withdraw right now
	return h.createNewCardPaymentTransfer(ctx, newTransfer)
}

func (h *Handler) PenaltyClient(ctx context.Context, penalty proto.PrepayOrder) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "penalty client",
		"payer_uuid":   penalty.PayerUUID,
		"subject_uuid": penalty.SubjectUUID,
	})

	// First, try to find out if an order was prepaid
	prepay, err := h.DB.FindPrepayByUserAndSubjectUUIDs(ctx, penalty.PayerUUID, penalty.SubjectUUID)
	if err != nil {
		log.Debug("no need to penalty a client")
		return nil
	}

	txID, err := strconv.Atoi(prepay.TransactionId)
	if err != nil { // internal error, wrong format in database
		return errors.Wrap(err, "failed to atoi transaction id")
	}

	if penalty.Amount >= prepay.Amount { // confirm the prepay
		confirm := proto.GatewayConfirm{
			TransactionId: txID,
			Amount:        prepay.Amount,
			JsonData:      map[string]string{proto.JsonDataPayeeUUID: penalty.PayeeUUID},
		}
		if err = confirm.Confirm(ctx, h.gateClient); err != nil {
			return errors.Wrap(err, "failed to confirm the transaction")
		}

		if err = h.DB.ConfirmPrepay(ctx, prepay.ID); err != nil {
			log.WithField("event", "confirming prepay").Error(err)
			// continue intentionally
		}

		// Save a transfer to the database
		newTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   penalty.IdempotencyKey,
				PayerAccountType: structures.AccountTypeCard,
				TransferType:     structures.TransferTypePayment,
				PayerUUID:        penalty.PayerUUID,
				PayerType:        structures.UserTypeClient,
				PayeeUUID:        penalty.PayeeUUID,
				PayeeType:        structures.UserTypeDriver,
				Meta: structures.TransferMetadata{
					OfferUUID:  penalty.Meta.OfferUUID,
					OrderUUID:  penalty.SubjectUUID,
					DriverUUID: penalty.PayeeUUID,
					ClientID:   penalty.PayerUUID,
				},
				Amount:      prepay.Amount,
				Description: msgPenaltyClient,
			},
		}
		return h.createPaymentTransfer(ctx, newTransfer)
	}

	// Else if penalty is less than prepaid amount

	// Refund client
	cancel := proto.GatewayConfirm{TransactionId: txID}
	if err = cancel.Cancel(ctx, h.gateClient); err != nil {
		// Leave the prepaid amount, will be auto-confirmed. Do not withdraw anything else
		return errors.Wrap(err, "failed to refund a client")
	}

	if err = h.DB.RefundPrepay(ctx, prepay.ID); err != nil {
		log.WithField("event", "refunding prepay").Error(err)
		// continue intentionally
	}

	// Withdraw
	newTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   penalty.IdempotencyKey,
			PayerAccountType: structures.AccountTypeCard,
			TransferType:     structures.TransferTypePayment,
			PayerUUID:        penalty.PayerUUID,
			PayerType:        structures.UserTypeClient,
			PayeeUUID:        penalty.PayeeUUID,
			PayeeType:        structures.UserTypeDriver,
			Meta: structures.TransferMetadata{
				OfferUUID:  penalty.Meta.OfferUUID,
				OrderUUID:  penalty.SubjectUUID,
				DriverUUID: penalty.PayeeUUID,
				ClientID:   penalty.PayerUUID,
			},
			Amount:      math.Abs(penalty.Amount),
			Description: msgPenaltyClient,
		},
	}
	return h.createNewCardPaymentTransfer(ctx, newTransfer)
}

func (h *Handler) RefundClient(ctx context.Context, refund proto.PrepayOrder) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":        "refund client",
		"payee_uuid":   refund.PayeeUUID,
		"subject_uuid": refund.SubjectUUID,
	})

	// First, try to find out if an order was prepaid
	prepay, err := h.DB.FindPrepayByUserAndSubjectUUIDs(ctx, refund.PayeeUUID, refund.SubjectUUID)
	if err != nil {
		log.Debug("no need to refund a client")
		return nil
	}

	txID, err := strconv.Atoi(prepay.TransactionId)
	if err != nil { // internal error, wrong format in database
		return errors.Wrap(err, "failed to atoi transaction id")
	}

	// Refund client
	cancel := proto.GatewayConfirm{TransactionId: txID}
	if err = cancel.Cancel(ctx, h.gateClient); err != nil {
		// Leave the prepaid amount, must be resolved manually
		return errors.Wrap(err, "failed to refund a client")
	}

	if err = h.DB.RefundPrepay(ctx, prepay.ID); err != nil {
		log.WithField("event", "refunding prepay").Error(err)
		// continue intentionally
	}
	return nil
}

func (h *Handler) ConfirmCardTransfer(ctx context.Context, cardTransfer proto.CardTransferCompleted) error {
	if cardTransfer.Amount <= 0 {
		return errors.Errorf("amount must be a positive number: %f", cardTransfer.Amount)
	}

	newTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			TransferType:     structures.TransferTypeWithdraw,
			PayerUUID:        cardTransfer.PayerUUID,
			PayerType:        structures.UserTypeClient,
			PayerAccountType: structures.AccountTypeCard,
			Amount:           cardTransfer.Amount,
			Description:      msgFullPrepay,
			Meta: structures.TransferMetadata{
				OrderUUID: cardTransfer.SubjectUUID,
				ClientID:  cardTransfer.PayerUUID,
			},
		},
	}

	// Save info to the database
	bankTransfer := models.BankTransfer{
		ID:          newTransfer.IdempotencyKey,
		UserUUID:    newTransfer.PayerUUID,
		Amount:      newTransfer.Amount,
		SubjectUUID: newTransfer.Meta.OrderUUID,
	}
	if err := h.DB.CreateBankTransfer(ctx, &bankTransfer); err != nil {
		return errors.Wrap(err, "failed to insert payment transfer")
	}

	return h.createWithdrawTransfer(ctx, newTransfer)
}

func (h *Handler) createPrepaidCardPaymentTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	prepay, err := h.DB.FindPrepayByUserAndSubjectUUIDs(ctx, newTransfer.PayerUUID, newTransfer.Meta.OrderUUID)
	if err != nil { // not supposed to get here
		return errors.Wrap(err, "failed to find prepay by user and subject uuids")
	}
	txID, err := strconv.Atoi(prepay.TransactionId)
	if err != nil { // internal error, wrong format in database
		return errors.Wrap(err, "failed to atoi transaction id")
	}
	confirm := proto.GatewayConfirm{
		TransactionId: txID,
		Amount:        prepay.Amount,
		JsonData:      map[string]string{proto.JsonDataPayeeUUID: newTransfer.PayeeUUID},
	}
	if err = confirm.Confirm(ctx, h.gateClient); err != nil {
		return errors.Wrap(err, "failed to confirm the transaction")
	}

	if err = h.DB.ConfirmPrepay(ctx, prepay.ID); err != nil {
		logs.Eloger.WithField("event", "confirming prepay").Error(err)
		// continue intentionally
	}

	// If provided amount differs from prepaid one
	if newTransfer.Amount > prepay.Amount {
		// Create an additional transaction
		diffTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				PayerAccountType: structures.AccountTypeCard,
				TransferType:     newTransfer.TransferType,
				PayerUUID:        newTransfer.PayerUUID,
				PayerType:        newTransfer.PayerType,
				PayeeUUID:        newTransfer.PayeeUUID,
				PayeeType:        newTransfer.PayeeType,
				Meta:             newTransfer.Meta,
				Amount:           newTransfer.Amount - prepay.Amount,
				Description:      msgDiffPayment,
				GatewayKey:       structures.TransferKeyPrepayDiff,
			},
		}
		newTransfer.Amount = prepay.Amount // update the source amount
		if err = h.createNewCardPaymentTransfer(ctx, diffTransfer); err != nil {
			logs.Eloger.WithField("event", "paying a diff").
				Errorf("failed to create a diff transfer: %v", err)
			// continue intentionally
		}
	}

	// If provided amount is less than prepaid, do not refund user. Move the diff to bonus account
	if newTransfer.Amount < prepay.Amount {
		diffTransfer := proto.NewTransfer{
			NewTransfer: structures.NewTransfer{
				IdempotencyKey:   structures.GenerateUUID(),
				PayerAccountType: structures.AccountTypeBonus,
				TransferType:     structures.TransferTypeTopUp,
				PayeeUUID:        newTransfer.PayerUUID,
				PayeeType:        newTransfer.PayerType,
				Amount:           prepay.Amount - newTransfer.Amount,
				Description:      msgDiffPayment,
			},
		}
		if err = h.createTopUpTransfer(ctx, diffTransfer); err != nil {
			logs.Eloger.WithField("event", "paying a diff").
				Errorf("failed to create a diff transfer: %v", err)
			// continue intentionally
		}
	}

	return h.createPaymentTransfer(ctx, newTransfer)
}

func (h *Handler) createNewCardPaymentTransfer(ctx context.Context, newTransfer proto.NewTransfer) error {
	if newTransfer.Amount <= 0 {
		return errors.Errorf("amount must be a positive number: %f", newTransfer.Amount)
	}

	// Retrieve the payment token
	bankCard, err := h.DB.FindLatestClientBankCardByUserUUID(ctx, newTransfer.PayerUUID)
	if err != nil {
		return errors.Wrap(err, "failed to find client bank card for user")
	}

	gatePay := proto.GatewayPayment{
		Amount:      newTransfer.Amount,
		Currency:    proto.GatewayCurrencyRUB,
		AccountId:   newTransfer.PayerUUID,
		Token:       bankCard.PaymentToken,
		InvoiceId:   newTransfer.Meta.OrderUUID,
		Description: newTransfer.Description,
		JsonData:    map[string]string{proto.JsonDataPayeeUUID: newTransfer.PayeeUUID},

		IdKey: string(newTransfer.GatewayKey),
	}
	result, err := gatePay.Pay(ctx, h.gateClient)
	if err != nil {
		return errors.Wrap(err, "failed to perform a payment")
	}

	// Save info to the database
	bankTransfer := models.BankTransfer{
		ID:            newTransfer.IdempotencyKey,
		UserUUID:      newTransfer.PayerUUID,
		TransactionId: strconv.Itoa(result.TransactionID),
		Amount:        newTransfer.Amount,
		SubjectUUID:   newTransfer.Meta.OrderUUID,
	}
	if err = h.DB.CreateBankTransfer(ctx, &bankTransfer); err != nil {
		return errors.Wrap(err, "failed to insert payment transfer")
	}

	return h.createPaymentTransfer(ctx, newTransfer)
}
