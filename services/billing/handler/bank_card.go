package handler

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/crypto"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	cardJobs                   = "cards"
	maxParallelCardJobsAllowed = 97

	initialMoneyBlock = 1 // 1 rub
)

type BankCardRepository interface {
	IndexClientBankCards(ctx context.Context, clientUUID string) ([]*models.BankCard, error)
	FindLatestClientBankCardByUserUUID(ctx context.Context, clientUUID string) (*models.BankCard, error)
	CreateClientBankCard(ctx context.Context, card *models.BankCard) error
	BankCardExistsByUserUUIDAndSuffix(ctx context.Context, userUUID, suffix string) (bool, error)
	BankCardExistsByUserUUID(ctx context.Context, userUUID string) (bool, error)
	DeleteClientBankCardByIDAndUserUUID(ctx context.Context, cardID, userUUID string) error
}

func (h *Handler) IndexClientBankCards(ctx context.Context, clientUUID string) ([]*models.BankCard, error) {
	return h.DB.IndexClientBankCards(ctx, clientUUID)
}

func (h *Handler) CreateClientBankCard(ctx context.Context, card *proto.NewBankCard) (*proto.GatewayNewBankCardResponse, error) {
	// Make an argument for a payment gateway
	newCardReq := proto.GatewayNewBankCard{
		Amount:               initialMoneyBlock, // block some money to ensure card is valid
		Currency:             proto.GatewayCurrencyRUB,
		IpAddress:            card.IpAddress,
		Name:                 card.Name,
		CardCryptogramPacket: card.Cryptogram,
		AccountId:            card.UserUUID,
		Description:          "Регистрация новой карты",

		CardSuffix: card.CardSuffix,
	}

	var result *proto.GatewayNewBankCardResponse
	job := h.jobs.GetJobQueue(cardJobs, maxParallelCardJobsAllowed)
	err := job.Execute(crypto.FNV(newCardReq.AccountId), func() error {
		// TODO: for now check for any existing card is used
		//cardExists, err := h.DB.BankCardExistsByUserUUIDAndSuffix(ctx, card.UserUUID, card.CardSuffix)
		cardExists, err := h.DB.BankCardExistsByUserUUID(ctx, card.UserUUID)
		if err != nil {
			return err
		}
		if cardExists {
			//return errors.New("Такая карта уже существует")
			return errors.New("В данный момент Вы можете использовать только одну карту. " +
				"Чтобы привязать новую карту удалите уже существующую.")
		}

		result, err = newCardReq.Create(ctx, h.gateClient)
		if err != nil {
			return err
		}

		// If we need to approve a transaction
		if result.Need3DS() {
			return nil
			// Unused for now, might get handy later
			// Save 3DS information to approve the transaction later
			//return h.DB.CreateBankTransfer(ctx, &models.BankTransfer{
			//	UserUUID:      card.UserUUID,
			//	TransactionId: result.Approve.TransactionId,
			//	PaymentReq:    result.Approve.PaymentReq,
			//	AcsUrl:        result.Approve.AcsUrl,
			//})
		}

		// If there is no need to approve
		return h.DB.CreateClientBankCard(ctx, result.Card)
	})

	if err != nil {
		return nil, err
	}
	return result, nil
}

func (h *Handler) DeleteClientBankCard(ctx context.Context, clientUUID, cardID string) error {
	return h.DB.DeleteClientBankCardByIDAndUserUUID(ctx, cardID, clientUUID)
}
