package handler

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/nguyenthenguyen/docx"
	"github.com/pkg/errors"
	"golang.org/x/text/language"
	"golang.org/x/text/message"

	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/helpers"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	periodLayout = "02.01.2006"
)

type InterbankRepository interface {
	InterbankTransactions(context.Context, models.InterbankDocumentRequest) (models.InterbankTransactions, error)
}

type TaxiParkRepository interface {
	GetTaxiParkByUUID(ctx context.Context, uuid string) (structures.TaxiPark, error)
}

func (h *Handler) GenerateServiceAct(ctx context.Context, preq proto.InterbankDocumentRequest, w http.ResponseWriter) error {
	req, err := preq.ToModel()
	if err != nil {
		return err
	}

	taxiPark, err := h.RemoteDB.GetTaxiParkByUUID(ctx, preq.TaxiParkUUID)
	if err != nil {
		return errors.Wrap(err, "failed to fetch a taxi park by uuid")
	}

	tx, err := h.DB.InterbankTransactions(ctx, req)
	if err != nil {
		return errors.Wrap(err, "failed to fetch interbank transactions")
	}

	r, err := docx.ReadDocxFile(h.Config.InterbankConfig.ServiceActTemplate)
	if err != nil {
		return errors.Wrap(err, "failed to read a doc template")
	}
	defer r.Close()

	doc := r.Editable()
	p := message.NewPrinter(language.Russian)
	now := localtime.TimeInZone(time.Now(), h.Config.InterbankConfig.TimeZone)
	amountFee := int(tx.AmountFee)

	doc.Replace("{date}", fmt.Sprintf("%d", now.Day()), -1)
	doc.Replace("{month}", helpers.MonthRus(now.Month()), -1)
	doc.Replace("{year}", fmt.Sprintf("%d", now.Year()), -1)

	doc.Replace("{contragent_name}", taxiPark.Representative.Name, -1)
	doc.Replace("{contragent_inn}", taxiPark.Representative.INN, -1)

	doc.Replace("{period_start}", localtime.TimeInZone(req.PeriodStart, h.Config.InterbankConfig.TimeZone).Format(periodLayout), -1)
	doc.Replace("{period_end}", localtime.TimeInZone(req.PeriodEnd, h.Config.InterbankConfig.TimeZone).Format(periodLayout), -1)
	doc.Replace("{amount_digital}", p.Sprintf("%d", amountFee), -1)
	doc.Replace("{amount_text}", helpers.Number2Text(amountFee), -1)
	doc.Replace("{amount_rubles}", helpers.SpellRubles(amountFee), -1)

	// Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename=service-act.docx")
	w.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
	if err = doc.Write(w); err != nil {
		return errors.Wrap(err, "failed to write a document into the response")
	}

	return nil
}

func (h *Handler) GenerateAgentReport(ctx context.Context, preq proto.InterbankDocumentRequest, w http.ResponseWriter) error {
	req, err := preq.ToModel()
	if err != nil {
		return err
	}

	taxiPark, err := h.RemoteDB.GetTaxiParkByUUID(ctx, preq.TaxiParkUUID)
	if err != nil {
		return errors.Wrap(err, "failed to fetch a taxi park by uuid")
	}

	tx, err := h.DB.InterbankTransactions(ctx, req)
	if err != nil {
		return errors.Wrap(err, "failed to fetch interbank transactions")
	}

	r, err := docx.ReadDocxFile(h.Config.InterbankConfig.AgentReportTemplate)
	if err != nil {
		return errors.Wrap(err, "failed to read a doc template")
	}
	defer r.Close()

	doc := r.Editable()
	p := message.NewPrinter(language.Russian)
	amountTotal := int(tx.AmountTotal)
	amountContragent := int(tx.GetAmountContragent())

	doc.Replace("{contragent_name}", taxiPark.Representative.Name, -1)
	doc.Replace("{contragent_inn}", taxiPark.Representative.INN, -1)

	doc.Replace("{period_start}", localtime.TimeInZone(req.PeriodStart, h.Config.InterbankConfig.TimeZone).Format(periodLayout), -1)
	doc.Replace("{period_end}", localtime.TimeInZone(req.PeriodEnd, h.Config.InterbankConfig.TimeZone).Format(periodLayout), -1)
	doc.Replace("{amount_digital}", p.Sprintf("%d", amountTotal), -1)
	doc.Replace("{amount_text}", helpers.Number2Text(amountTotal), -1)
	doc.Replace("{amount_rubles}", helpers.SpellRubles(amountTotal), -1)

	doc.Replace("{amount_contragent_digital}", p.Sprintf("%d", amountContragent), -1)
	doc.Replace("{amount_contragent_text}", helpers.Number2Text(amountContragent), -1)
	doc.Replace("{amount_contragent_rubles}", helpers.SpellRubles(amountContragent), -1)

	doc.Replace("{amount_refund_digital}", "0", -1)
	doc.Replace("{amount_refund_text}", "ноль", -1)
	doc.Replace("{amount_refund_rubles}", "рублей", -1)

	doc.Replace("{amount_refund_fee_digital}", "0", -1)
	doc.Replace("{amount_refund_fee_text}", "ноль", -1)
	doc.Replace("{amount_refund_fee_rubles}", "рублей", -1)

	// Send the headers
	w.Header().Set("Content-Disposition", "attachment; filename=agent-report.docx")
	w.Header().Set("Content-Type", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
	if err = doc.Write(w); err != nil {
		return errors.Wrap(err, "failed to write a document into the response")
	}

	return nil
}
