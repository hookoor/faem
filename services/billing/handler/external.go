package handler

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	msgExternalQiwi  = "Пополнение через QIWI"
	msgExternalQiwi1 = "Пополнение через QIWI1"
	msgExternalKit   = "Пополнение через KIT"
)

type ExternalTransferRepository interface {
	GetTransferByExternalID(ctx context.Context, externalID string) (models.Transfer, error)
	GetAccountByPhoneAndTypeAndUserType(
		ctx context.Context, phone string, accountType structures.BillingAccountType, userType structures.BillingUserType,
	) (models.Account, error)
}

func (h *Handler) TopupQiwi(ctx context.Context, topup models.TopupQiwi) models.QiwiTopupResult {
	err := h.topupQiwi(ctx, topup)
	tr := models.QiwiTopupResultFromError(errors.Cause(err)) // transform to a response forcibly
	tr.QiwiTransactionID = topup.TransactionID               // remember to set the transaction id
	return tr
}

func (h *Handler) TopupQiwi1(ctx context.Context, topup models.TopupQiwi1) models.QiwiTopupResult {
	err := h.topupQiwi1(ctx, topup)
	tr := models.QiwiTopupResultFromError(errors.Cause(err)) // transform to a response forcibly
	tr.QiwiTransactionID = topup.TransactionID               // remember to set the transaction id
	return tr
}

func (h *Handler) TopupKit(ctx context.Context, topup models.TopupKit) models.KitTopupResult {
	err := h.topupKit(ctx, topup)
	tr := models.KitTopupResultFromError(errors.Cause(err)) // transform to a response forcibly
	tr.KitTransactionID = topup.TransactionID               // remember to set the transaction id
	return tr
}

func (h *Handler) topupQiwi(ctx context.Context, topup models.TopupQiwi) (err error) {
	defer func() {
		logItm := models.TopupQiwiLog{
			LogItem:   models.LogItem{Event: msgExternalQiwi},
			TopupQiwi: topup,
			Result:    err,
		}
		h.DB.FlushLogs(ctx, models.Log{Data: logItm})
	}()

	log := logs.LoggerForContext(ctx).WithField("provider", "QIWI")

	// Validate incoming args first
	if err = topup.Prepare(); err != nil {
		log.WithField("event", "preparing incoming from QIWI args").Error(err)
		return err
	}

	// Check if a transfer with such id already exists
	_, err = h.DB.GetTransferByExternalID(ctx, topup.PrefixedID())
	if err == nil {
		log.Infof("transfer with external id %s already exists", topup.TransactionID)
		return nil
	}
	if errors.Cause(err) != pg.ErrNoRows { // pg.ErrNoRows is expected
		log.WithField("event", "checking if an external transfer already exists").Error(err)
		return err
	}

	// Get payee account ID
	payeeAcc, err := h.DB.GetAccountByPhoneAndTypeAndUserType(
		ctx, topup.Phone(), structures.AccountTypeBonus, structures.UserTypeDriver,
	)
	if err != nil {
		log.WithFields(logrus.Fields{
			"event": "finding a payee account by phone",
			"phone": topup.Phone(),
		}).Error(err)
		return models.QiwiTopupResult{Result: models.TopUpCodeAccountNotFound}
	}

	// Get payer account ID
	payerID := h.Accounts.Gateway.GetAccountByType(structures.AccountTypeBonus)

	// Prepare a transfer
	newTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     structures.TransferTypeTopUp,
			PayeeUUID:        payeeAcc.UserUUID,
			PayeeType:        structures.UserTypeDriver,
			Amount:           topup.Sum,
			Description:      msgExternalQiwi,
		},
		ExternalID: topup.PrefixedID(),
	}
	if err = h.createTransferInTransaction(ctx, newTransfer, payeeAcc.ID, payerID); err != nil {
		log.WithField("event", "providing a QIWI transfer").Error(err)
		return err
	}
	return nil
}

func (h *Handler) topupQiwi1(ctx context.Context, topup models.TopupQiwi1) (err error) {
	defer func() {
		logItm := models.TopupQiwiLog{
			LogItem: models.LogItem{Event: msgExternalQiwi1},
			TopupQiwi: models.TopupQiwi{
				Command:       models.QiwiCommand(topup.Command),
				TransactionID: topup.TransactionID,
				Account:       topup.Account,
				Sum:           topup.Sum,
			},
			Result: err,
		}
		h.DB.FlushLogs(ctx, models.Log{Data: logItm})
	}()

	log := logs.LoggerForContext(ctx).WithField("provider", "QIWI1")

	// Validate incoming args first
	if err = topup.Prepare(); err != nil {
		log.WithField("event", "preparing incoming from QIWI args").Error(err)
		return err
	}

	// Check if a transfer with such id already exists
	_, err = h.DB.GetTransferByExternalID(ctx, topup.PrefixedID())
	if err == nil {
		log.Infof("transfer with external id %s already exists", topup.TransactionID)
		return nil
	}
	if errors.Cause(err) != pg.ErrNoRows { // pg.ErrNoRows is expected
		log.WithField("event", "checking if an external transfer already exists").Error(err)
		return err
	}

	// Get payee account ID
	payeeAcc, err := h.DB.GetAccountByPhoneAndTypeAndUserType(
		ctx, topup.Phone(), structures.AccountTypeBonus, structures.UserTypeDriver,
	)
	if err != nil {
		log.WithFields(logrus.Fields{
			"event": "finding a payee account by phone",
			"phone": topup.Phone(),
		}).Error(err)
		return models.QiwiTopupResult{Result: models.TopUpCodeAccountNotFound}
	}

	if topup.Command == models.Qiwi1CommandCheck {
		// We have enough info to respond the "check" command
		log.Debugf("successfully checking if a QIWI1 topup possible for transaction: %s", topup.TransactionID)
		return nil
	}

	// Get payer account ID
	payerID := h.Accounts.Gateway.GetAccountByType(structures.AccountTypeBonus)

	// Prepare a transfer
	newTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     structures.TransferTypeTopUp,
			PayeeUUID:        payeeAcc.UserUUID,
			PayeeType:        structures.UserTypeDriver,
			Amount:           topup.Sum,
			Description:      msgExternalQiwi1,
		},
		ExternalID: topup.PrefixedID(),
	}
	if err = h.createTransferInTransaction(ctx, newTransfer, payeeAcc.ID, payerID); err != nil {
		log.WithField("event", "providing a QIWI1 transfer").Error(err)
		return err
	}
	return nil
}

func (h *Handler) topupKit(ctx context.Context, topup models.TopupKit) (err error) {
	defer func() {
		logItm := models.TopupKitLog{
			LogItem:  models.LogItem{Event: msgExternalKit},
			TopupKit: topup,
			Result:   err,
		}
		h.DB.FlushLogs(ctx, models.Log{Data: logItm})
	}()

	log := logs.LoggerForContext(ctx).WithField("provider", "KIT")

	// Validate incoming args first
	if err = topup.Prepare(); err != nil {
		log.WithField("event", "preparing incoming from KIT args").Error(err)
		return err
	}

	// Check if a transfer with such id already exists
	transfer, err := h.DB.GetTransferByExternalID(ctx, topup.PrefixedID())
	if err == nil {
		log.Infof("transfer with external id %s already exists", topup.TransactionID)
		return models.KitTopupResult{
			KitTransactionID:   topup.TransactionID,
			InnerTransactionID: transfer.ID,
			Amount:             transfer.Amount,
			Result:             models.TopUpCodeOK,
		}
	}
	if errors.Cause(err) != pg.ErrNoRows { // pg.ErrNoRows is expected
		log.WithField("event", "checking if an external transfer already exists").Error(err)
		return err
	}

	// Get payee account ID
	payeeAcc, err := h.DB.GetAccountByPhoneAndTypeAndUserType(
		ctx, topup.Phone(), structures.AccountTypeBonus, structures.UserTypeDriver,
	)
	if err != nil {
		log.WithFields(logrus.Fields{
			"event": "finding a payee account by phone",
			"phone": topup.Phone(),
		}).Error(err)
		return models.KitTopupResult{Result: models.TopUpCodeAccountNotFound}
	}

	if topup.Command == models.KitCommandCheck {
		// We have enough info to respond the "check" command
		log.Debugf("successfully checking if a KIT topup possible for transaction: %s", topup.TransactionID)
		return nil
	}

	// Continue if "pay" command provided
	payerID := h.Accounts.Gateway.GetAccountByType(structures.AccountTypeBonus) // get payer account ID

	// Prepare a transfer
	newTransfer := proto.NewTransfer{
		NewTransfer: structures.NewTransfer{
			IdempotencyKey:   structures.GenerateUUID(),
			PayerAccountType: structures.AccountTypeBonus,
			TransferType:     structures.TransferTypeTopUp,
			PayeeUUID:        payeeAcc.UserUUID,
			PayeeType:        structures.UserTypeDriver,
			Amount:           topup.Sum,
			Description:      msgExternalKit,
		},
		ExternalID: topup.PrefixedID(),
	}
	if err = h.createTransferInTransaction(ctx, newTransfer, payeeAcc.ID, payerID); err != nil {
		log.WithField("event", "providing a KIT transfer").Error(err)
		return models.KitTopupResult{
			Amount: topup.Sum,
			Result: models.TopUpCodeUnknown,
		}
	}

	return models.KitTopupResult{
		KitTransactionID:   topup.TransactionID,
		InnerTransactionID: newTransfer.IdempotencyKey,
		Amount:             topup.Sum,
		Result:             models.TopUpCodeOK,
	}
}
