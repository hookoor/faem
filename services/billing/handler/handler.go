package handler

import (
	"context"
	"crypto/tls"
	"net/http"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/jobqueue"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
	"gitlab.com/faemproject/backend/faem/services/billing/rpc"
)

type LogRepository interface {
	FlushLogs(ctx context.Context, logs ...models.Log) error
}

type Repository interface {
	AccountRepository
	TransferRepository
	ExternalTransferRepository
	ForceRepository
	LogRepository
	BankCardRepository
	BankTransferRepository
	InterbankRepository

	RunInTransaction(func(*pg.Tx) error) error
}

type RemoteRepository interface {
	TaxiParkRepository
}

type Publisher interface {
	TransferCompleted(transfer structures.CompletedTransfer) error
	PaymentTypeChangedToCash(prepay structures.PrepayOrder) error
	ReceiptGenerated(receipt structures.ReceiptHook) error
}

type (
	Account struct {
		Cash  string
		Card  string
		Bonus string
	}

	Accounts struct {
		Owner   Account
		Gateway Account
	}
)

func (a *Account) GetAccountByType(accountType structures.BillingAccountType) string {
	switch accountType {
	case structures.AccountTypeCash:
		return a.Cash
	case structures.AccountTypeBonus:
		return a.Bonus
	}
	return a.Card
}

type PaymentConfig struct {
	Host     string
	Username string
	Password string
	Inn      string
}

type InterbankConfig struct {
	TimeZone            string
	ServiceActTemplate  string
	AgentReportTemplate string
}

type CRMConfig struct {
	Host string
}

type Config struct {
	PaymentConfig   PaymentConfig
	InterbankConfig InterbankConfig
	CRMConfig       CRMConfig
}

type Handler struct {
	DB       Repository
	RemoteDB RemoteRepository
	Pub      Publisher
	Accounts Accounts
	Config   Config

	gateClient *rpc.PaymentGatewayClient
	jobs       *jobqueue.JobQueues
}

func NewHandler(db Repository, pub Publisher, accs Accounts, config Config) *Handler {
	crmClient := rpc.CRMClient{
		HttpClient: &http.Client{
			Timeout: 60 * time.Second,
		},
		Config: rpc.CRMConfig{
			Host: config.CRMConfig.Host,
		},
	}

	return &Handler{
		DB:       db,
		RemoteDB: &crmClient,
		Pub:      pub,
		Accounts: accs,
		Config:   config,

		gateClient: &rpc.PaymentGatewayClient{
			HttpClient: &http.Client{
				Timeout: 60 * time.Second,
				Transport: &http.Transport{
					TLSClientConfig: &tls.Config{InsecureSkipVerify: true}, // TODO: use cert if required
				},
			},
			Host:     config.PaymentConfig.Host,
			Username: config.PaymentConfig.Username,
			Password: config.PaymentConfig.Password,
			Inn:      config.PaymentConfig.Inn,
		},
		jobs: jobqueue.NewJobQueues(),
	}
}
