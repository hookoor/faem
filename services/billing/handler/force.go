package handler

import (
	"context"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

type ForceRepository interface {
	GetCardDriverUUIDs(ctx context.Context) ([]string, error)
}

func (h *Handler) ForceSet(ctx context.Context, in proto.ForceArgs) error {
	if err := in.Validate(); err != nil {
		return err
	}

	driverUUIDs := in.ID
	if in.Group == proto.ForceGroupAll {
		var err error
		driverUUIDs, err = h.DB.GetCardDriverUUIDs(ctx)
		if err != nil {
			return err
		}
	}

	go func() {
		logs.Eloger.Debug("setting drivers balance forcibly...")

		for _, drv := range driverUUIDs {
			force := proto.NewTransfer{
				NewTransfer: structures.NewTransfer{
					IdempotencyKey:   structures.GenerateUUID(),
					PayerAccountType: structures.AccountTypeBonus,
					TransferType:     structures.TransferTypeForceSet,
					PayeeUUID:        drv,
					PayeeType:        structures.UserTypeDriver,
					Amount:           in.Amount,
					Description:      "Установка баланса администратором",
				},
			}
			if err := h.CreateTransfer(context.Background(), force); err != nil {
				logs.Eloger.Errorf("failed to set balance %v: %v\n", force, err)
			}
		}

		logs.Eloger.Debug("done")
	}()
	return nil
}
