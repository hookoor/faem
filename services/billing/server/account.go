package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (r *Rest) DriverBalances(c echo.Context) error {
	var in structures.DriverBalancesArgs
	if err := c.Bind(&in); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("event", "driver balances").
			Error(err)
		res := logs.OutputRestError("failed to parse the request", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	balance, err := r.Handler.GetAccountsInfo(c.Request().Context(), in.UserUUIDs, structures.UserTypeDriver)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("user_uuids", in.UserUUIDs).
			Error(err)
		res := logs.OutputRestError("can't find user's balance", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, balance)
}

func (r *Rest) TaxiParkBalances(c echo.Context) error {
	var in structures.TaxiParksBalancesArgs
	if err := c.Bind(&in); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("event", "taxi parks balances").
			Error(err)
		res := logs.OutputRestError("failed to parse the request", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	balance, err := r.Handler.GetAccountsInfo(c.Request().Context(), in.UUIDs, structures.UserTypeTaxiPark)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("uuids", in.UUIDs).
			Error(err)
		res := logs.OutputRestError("can't find taxiparks's balance", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, balance)
}

func (r *Rest) ClientBalances(c echo.Context) error {
	var in structures.ClientBalancesArgs
	if err := c.Bind(&in); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("event", "client balances").
			Error(err)
		res := logs.OutputRestError("failed to parse the request", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	balance, err := r.Handler.GetAccountsInfo(c.Request().Context(), in.Phones, structures.UserTypeClient)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("phones", in.Phones).
			Error(err)
		res := logs.OutputRestError("can't find user balances", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, balance)
}
