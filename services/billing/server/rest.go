package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/billing/config"
	"gitlab.com/faemproject/backend/faem/services/billing/handler"
)

const (
	gatePrefix = "/gate/v1"
	rpcPrefix  = "/rpc/v1"
	apiPrefix  = "/api/v2"
)

type Config struct {
	Auth config.Auth
	//ProxyPass config.ProxyPass
}

type Rest struct {
	Config  Config
	Router  *echo.Echo
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route() {
	web.UseHealthCheck(r.Router)

	// Proxy payment routines. Unused now. (TODO: handle IP whitelist).
	//r.Router.GET("/qiwiterm/int/script.jsp", r.QiwiTopup)
	//r.Router.GET("/int/script.jsp", r.QiwiTopup)
	//r.Router.GET("/kitterm/index.php", r.KitTopup)
	//r.Router.GET("/index.php", r.KitTopup)

	// Our payment routines
	gate := r.Router.Group(gatePrefix)
	gate.Use(basicAuth(r.Config.Auth.Gate.Username, r.Config.Auth.Gate.Password))
	gate.GET("/qiwi", r.QiwiTopup)
	gate.GET("/kit", r.KitTopup)
	gate.GET("/qiwi1", r.Qiwi1Topup) // TODO: temporary

	rpc := r.Router.Group(rpcPrefix)
	rpc.Use(basicAuth(r.Config.Auth.Rpc.Username, r.Config.Auth.Rpc.Password))
	rpc.POST("/driver/balances", r.DriverBalances)
	rpc.POST("/taxipark/balances", r.TaxiParkBalances)
	rpc.POST("/client/balances", r.ClientBalances)

	// Open API group
	open := r.Router.Group(apiPrefix)

	clientOpen := open.Group("/client")
	clientOpen.POST("/bank_cards/approve", r.ApproveClientBankCard)

	// CloudPayments webhook
	open.POST("/receipt", r.ReceiptGenerated)

	// Protected API group
	protected := r.Router.Group(apiPrefix)
	protected.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(config.JWTSecret()),
	}))

	clientProtected := protected.Group("/client")
	clientProtected.GET("/bank_cards", r.IndexClientBankCards)
	clientProtected.POST("/bank_cards", r.CreateClientBankCard)
	clientProtected.DELETE("/bank_cards/:id", r.DeleteClientBankCard)

	interbankProtected := protected.Group("/interbank")
	interbankProtected.POST("/service_act", r.GenerateServiceAct)
	interbankProtected.POST("/agent_report", r.GenerateAgentReport)

	transferProtected := protected.Group("/transfer")
	transferProtected.POST("/cardcompleted", r.CardTransferCompleted)

	// remember to remove after executing
	//force := r.Router.Group("/force")
	//force.Use(basicAuth("MSdMcUrN2UM6QzVa", "Rv3E4q79A34AaQ6A"))
	//force.POST("/bx6bMza63v5ALkdQ", r.Force)
}

func basicAuth(authUser, authPass string) echo.MiddlewareFunc {
	return middleware.BasicAuth(
		func(username, password string, context echo.Context) (bool, error) {
			return username == authUser && password == authPass, nil
		},
	)
}
