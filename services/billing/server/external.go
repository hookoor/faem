package server

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (r *Rest) QiwiTopup(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context())

	command := models.QiwiCommand(c.QueryParam("command"))
	account := c.QueryParam("account")
	transactionID := c.QueryParam("txn_id")

	sum, err := strconv.ParseFloat(c.QueryParam("sum"), 64)
	if err != nil {
		log.WithField("event", "parsing sum from QIWI").Error(err)
		return c.XML(http.StatusOK, models.QiwiTopupResult{
			QiwiTransactionID: transactionID,
			Result:            models.TopUpCodeUnknown,
		})
	}

	topup := models.TopupQiwi{
		Command:       command,
		TransactionID: transactionID,
		Account:       account,
		Sum:           sum,
	}
	topupResult := r.Handler.TopupQiwi(c.Request().Context(), topup)
	log.WithFields(logrus.Fields{
		"event":         "handling new external QIWI top up",
		"transactionID": topupResult.QiwiTransactionID,
	}).Debugf("result: %d", topupResult.Result)
	return c.XML(http.StatusOK, topupResult)
}

func (r *Rest) Qiwi1Topup(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context())

	command := models.Qiwi1Command(c.QueryParam("command"))
	account := c.QueryParam("account")
	transactionID := c.QueryParam("txn_id")

	sum, err := strconv.ParseFloat(c.QueryParam("sum"), 64)
	if err != nil {
		log.WithField("event", "parsing sum from QIWI1").Error(err)
		return c.XML(http.StatusOK, models.QiwiTopupResult{
			QiwiTransactionID: transactionID,
			Result:            models.TopUpCodeUnknown,
		})
	}

	topup := models.TopupQiwi1{
		Command:       command,
		TransactionID: transactionID,
		Account:       account,
		Sum:           sum,
	}
	topupResult := r.Handler.TopupQiwi1(c.Request().Context(), topup)
	log.WithFields(logrus.Fields{
		"event":         "handling new external QIWI top up",
		"transactionID": topupResult.QiwiTransactionID,
	}).Debugf("result: %d", topupResult.Result)
	return c.XML(http.StatusOK, topupResult)
}

func (r *Rest) KitTopup(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context())

	command := models.KitCommand(c.QueryParam("command"))
	account := c.QueryParam("account")
	transactionID := c.QueryParam("txn_id")
	txnDate := c.QueryParam("txn_date")

	sum, err := strconv.ParseFloat(c.QueryParam("sum"), 64)
	if err != nil {
		log.WithField("event", "parsing sum from KIT").Error(err)
		return c.XML(http.StatusOK, models.KitTopupResult{
			KitTransactionID: transactionID,
			Result:           models.TopUpCodeUnknown,
		})
	}

	topup := models.TopupKit{
		Command:       command,
		TransactionID: transactionID,
		Account:       account,
		Sum:           sum,
		TxnDate:       txnDate,
	}
	topupResult := r.Handler.TopupKit(c.Request().Context(), topup)
	log.WithFields(logrus.Fields{
		"event":         "handling new external KIT top up",
		"transactionID": topupResult.KitTransactionID,
	}).Debugf("result: %d", topupResult.Result)
	return c.XML(http.StatusOK, topupResult)
}

// Unused for now
//func (r *Rest) reverseProxy(rw http.ResponseWriter, req *http.Request) {
//	proxyURL, _ := url.Parse(r.Config.ProxyPass.Host)     // parse the proxy url
//	proxy := httputil.NewSingleHostReverseProxy(proxyURL) // create the reverse proxy
//
//	// Update the headers to allow for SSL redirection
//	req.URL.Host = proxyURL.Host
//	req.URL.Scheme = proxyURL.Scheme
//	req.URL.Path = r.Config.ProxyPass.QiwiEndpoint // forget the original path, use the one from the config
//	req.Header.Set("X-Forwarded-Host", req.Header.Get("Host"))
//	req.Host = proxyURL.Host
//	// TODO: set authorization if required
//
//	// ServeHTTP is non blocking
//	proxy.ServeHTTP(rw, req)
//}
