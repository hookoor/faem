package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

// The endpoint forcibly sets driver balances and notifies other services via RMQ.
func (r *Rest) Force(c echo.Context) error {
	var in proto.ForceArgs
	if err := c.Bind(&in); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("event", "setting balances forcibly").
			Error(err)
		res := logs.OutputRestError("failed to parse the request", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.ForceSet(c.Request().Context(), in); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithField("in", in).
			Error(err)
		res := logs.OutputRestError("can't set balance", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, nil)
}
