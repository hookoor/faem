package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/helpers"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

func (r *Rest) GenerateServiceAct(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "generating a service act")

	_, err := helpers.StaffUUIDFromJWT(c)
	if err != nil {
		logger.Error(err)
		res := logs.OutputRestError("unauthorized", err)
		return c.JSON(http.StatusUnauthorized, res)
	}

	var req proto.InterbankDocumentRequest
	if err := c.Bind(&req); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.GenerateServiceAct(c.Request().Context(), req, c.Response().Writer); err != nil {
		logger.WithField("reason", "generating a service act").Error(err)
		res := logs.OutputRestError("Ошибка генерации документа", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return nil
}

func (r *Rest) GenerateAgentReport(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "generating an agent report")

	_, err := helpers.StaffUUIDFromJWT(c)
	if err != nil {
		logger.Error(err)
		res := logs.OutputRestError("unauthorized", err)
		return c.JSON(http.StatusUnauthorized, res)
	}

	var req proto.InterbankDocumentRequest
	if err := c.Bind(&req); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.GenerateAgentReport(c.Request().Context(), req, c.Response().Writer); err != nil {
		logger.WithField("reason", "generating an agent report").Error(err)
		res := logs.OutputRestError("Ошибка генерации документа", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return nil
}
