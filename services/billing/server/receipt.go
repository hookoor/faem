package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

func (r *Rest) ReceiptGenerated(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "receipt webhook")

	// Check for header
	sign := c.Request().Header.Get("Content-HMAC")
	if sign == "" {
		logger.WithField("reason", "validating incoming arguments").Error("empty signature provided")
		res := logs.OutputRestError("something went wrong", nil)
		return c.JSON(http.StatusBadRequest, res)
	}

	var receipt proto.ReceiptHook
	if err := c.Bind(&receipt); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.ReceiptGenerated(c.Request().Context(), receipt); err != nil {
		logger.WithField("reason", "handling generated receipt").Error(err)
		res := logs.OutputRestError("failed to handle generated receipt", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, echo.Map{"code": 0})
}
