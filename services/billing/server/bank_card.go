package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/helpers"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

func (r *Rest) IndexClientBankCards(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "index client bank cards")

	clientUUID, err := helpers.ClientUUIDFromJWT(c)
	if err != nil {
		logger.WithField("reason", "getting client UUID from JWT").Warn(err)
		res := logs.OutputRestError("can't get client UUID", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.IndexClientBankCards(c.Request().Context(), clientUUID)
	if err != nil {
		logger.WithField("reason", "indexing client bank cards").Error(err)
		res := logs.OutputRestError("Ошибка получения списка карт", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) CreateClientBankCard(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "create client bank card")

	clientUUID, err := helpers.ClientUUIDFromJWT(c)
	if err != nil {
		logger.WithField("reason", "getting client UUID from JWT").Warn(err)
		res := logs.OutputRestError("can't get client UUID", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	var card proto.NewBankCard
	if err = c.Bind(&card); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	card.UserUUID = clientUUID
	card.IpAddress = c.RealIP()

	out, err := r.Handler.CreateClientBankCard(c.Request().Context(), &card)
	if err != nil {
		logger.WithField("reason", "creating client's new bank card").Error(err)
		res := logs.OutputRestError("Ошибка создания карты", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) DeleteClientBankCard(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "delete client bank card")

	clientUUID, err := helpers.ClientUUIDFromJWT(c)
	if err != nil {
		logger.WithField("reason", "getting client UUID from JWT").Warn(err)
		res := logs.OutputRestError("can't get client UUID", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	cardId := c.Param("id")
	if err := r.Handler.DeleteClientBankCard(c.Request().Context(), clientUUID, cardId); err != nil {
		logger.WithField("reason", "deleting client bank card").Error(err)
		res := logs.OutputRestError("Ошибка удаления карты", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, out)
}
