package server

import (
	"net/http"

	"github.com/labstack/echo/v4"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

func (r *Rest) ApproveClientBankCard(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "approve client bank card")

	var approve proto.PaymentApproveArg
	if err := c.Bind(&approve); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	out, err := r.Handler.ApproveClientBankCard(c.Request().Context(), &proto.PaymentApprove{
		TransactionId: approve.MD,
		PayRes:        approve.PayRes,
	})
	if err != nil {
		logger.WithField("reason", "approving client bank card").Error(err)
		res := logs.OutputRestError("Ошибка подтверждения регистрации карты", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) CardTransferCompleted(c echo.Context) error {
	logger := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "card transfer completed")

	var cardTransfer proto.CardTransferCompleted
	if err := c.Bind(&cardTransfer); err != nil {
		logger.WithField("reason", "binding incoming arguments").Warn(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err := r.Handler.ConfirmCardTransfer(c.Request().Context(), cardTransfer); err != nil {
		logger.WithField("reason", "completing a card transfer").Error(err)
		res := logs.OutputRestError("Ошибка подтверждения платежа", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, out)
}
