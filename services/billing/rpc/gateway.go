package rpc

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
)

const (
	maxRequestAttempts = 5
	retryInterval      = time.Second * 5

	ChargePaymentURL   = "/payments/cards/charge"
	PrepayTokenURL     = "/payments/tokens/auth"
	ChargeTokenURL     = "/payments/tokens/charge"
	ConfirmURL         = "/payments/confirm"
	CancelURL          = "/payments/void"
	Post3DSURL         = "/payments/cards/post3ds"
	RefundURL          = "/payments/refund"
	GenerateReceiptURL = "/kkt/receipt"
)

type PaymentGatewayOp string

const (
	PaymentGatewayOpRegisterCard    PaymentGatewayOp = "register_card"
	PaymentGatewayOpApproveCard     PaymentGatewayOp = "approve_card"
	PaymentGatewayOpPrepay          PaymentGatewayOp = "prepay"
	PaymentGatewayOpConfirm         PaymentGatewayOp = "confirm"
	PaymentGatewayOpCancel          PaymentGatewayOp = "cancel"
	PaymentGatewayOpPayment         PaymentGatewayOp = "payment"
	PaymentGatewayOpRefund          PaymentGatewayOp = "refund"
	PaymentGatewayOpGenerateReceipt PaymentGatewayOp = "generate_receipt"
)

type GatewayResponse struct {
	Success bool                   `json:"Success"`
	Message string                 `json:"Message"`
	Model   map[string]interface{} `json:"Model"`
}

func (r *GatewayResponse) HasMessage() bool {
	return r.Message != ""
}

func (r *GatewayResponse) Need3DS() bool {
	_, found := r.Model["PaReq"]
	return found
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type PaymentGatewayClient struct {
	HttpClient *http.Client
	Host       string
	Username   string
	Password   string
	Inn        string
}

func (g *PaymentGatewayClient) CallMethod(endpoint, idempotencyKey string, payload interface{}) (*GatewayResponse, error) {
	var resp *GatewayResponse
	err := lang.Retry(maxRequestAttempts, retryInterval, func() (bool, error) {
		var err error
		resp, err = g.Call(g.Host+endpoint, idempotencyKey, payload)
		if !isTemporaryError(err) {
			return true, err
		}
		return false, err
	})
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (g *PaymentGatewayClient) Call(url, idempotencyKey string, payload interface{}) (*GatewayResponse, error) {
	body, err := json.Marshal(payload)
	if err != nil {
		return nil, errors.Wrap(err, "failed to marshal a payload")
	}
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))
	if err != nil {
		return nil, errors.Wrap(err, "failed to create an http request")
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("X-Request-ID", idempotencyKey)
	req.SetBasicAuth(g.Username, g.Password)

	resp, err := g.HttpClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "failed to make a POST request")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return nil, errors.Errorf("POST request to %s failed with status: %d", url, resp.StatusCode)
		}
		return nil, errors.Errorf("POST request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}
	var response GatewayResponse
	if err = json.NewDecoder(resp.Body).Decode(&response); err != nil {
		return nil, errors.Wrap(err, "failed to decode a response")
	}
	return &response, nil
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

type temporary interface {
	Temporary() bool
}

func isTemporaryError(err error) bool {
	if err == nil {
		return false
	}

	cause := errors.Cause(err)
	if te, ok := cause.(temporary); ok {
		return te.Temporary()
	}
	return false
}
