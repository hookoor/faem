package rpc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	GetTaxiParkByUUIDEndpoint = "/taxiparks/%s"
)

type CRMConfig struct {
	Host string
}

type CRMClient struct {
	HttpClient *http.Client
	Config     CRMConfig
}

func (c *CRMClient) GetTaxiParkByUUID(_ context.Context, uuid string) (structures.TaxiPark, error) {
	var result structures.TaxiPark
	if err := c.request(
		http.MethodGet, c.Config.Host+fmt.Sprintf(GetTaxiParkByUUIDEndpoint, uuid), nil, &result,
	); err != nil {
		return structures.TaxiPark{}, errors.Wrap(err, "failed to fetch a taxi park by uuid")
	}
	return result, nil
}

func (c *CRMClient) request(method, url string, payload, response interface{}) error {
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := c.HttpClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to make a post request")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("post request to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf(
			"post request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body),
		)
	}
	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}
