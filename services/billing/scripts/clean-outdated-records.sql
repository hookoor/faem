-- Execute commands one by one

-- TODO: replace '2020-07-01' with the desired date and restore DELETE statement

SELECT COUNT(*) -- DELETE
FROM transfers
WHERE id IN (
    SELECT t.id
    FROM transfers t
        INNER JOIN transactions tx on t.transaction_ids[1] = tx.id
        INNER JOIN entries e on tx.credit_id = e.id OR tx.debit_id = e.id
    WHERE e.created_at < '2020-07-01'
        AND EXISTS(
            SELECT 1
            FROM entries e1
            WHERE e.account_id = e1.account_id AND e1.created_at >= '2020-07-01'
        )
);

--

SELECT COUNT(*) -- DELETE
FROM transactions
WHERE id IN (
    SELECT tx.id
    FROM transactions tx
        INNER JOIN entries e on tx.credit_id = e.id OR tx.debit_id = e.id
    WHERE e.created_at < '2020-07-01'
        AND EXISTS(
            SELECT 1
            FROM entries e1
            WHERE e.account_id = e1.account_id AND e1.created_at >= '2020-07-01'
        )
);

--

SELECT COUNT(*) -- DELETE
FROM entries e
WHERE created_at < '2020-07-01'
    AND EXISTS(
        SELECT 1
        FROM entries e1
        WHERE e.account_id = e1.account_id AND e1.created_at >= '2020-07-01'
    );

--

SELECT COUNT(*) -- DELETE
FROM bank_transfers
WHERE created_at < '2020-07-01';


-- Check script. Run before the execution and after.
-- Compare the outputs, the second one must be equal on greater.

SELECT COUNT(*)
FROM (
    SELECT DISTINCT ON (account_id) *
    FROM accounts
        INNER JOIN entries e on accounts.id = e.account_id
    ORDER BY account_id
) q;
