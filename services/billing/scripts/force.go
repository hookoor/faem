// +build ignore

// The script top ups existing driver accounts and notifies other services via RMQ.

package main

//import (
//	"context"
//	"flag"
//	"log"
//	"time"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//	"github.com/spf13/viper"
//	"go.uber.org/multierr"
//
//	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/billing/broker/publisher"
//	"gitlab.com/faemproject/backend/faem/services/billing/config"
//	"gitlab.com/faemproject/backend/faem/services/billing/handler"
//	"gitlab.com/faemproject/backend/faem/services/billing/models"
//	"gitlab.com/faemproject/backend/faem/services/billing/proto"
//	"gitlab.com/faemproject/backend/faem/services/billing/repository"
//)
//
//type Databases struct {
//	Billing config.Database
//}
//
//func (d Databases) validate() error {
//	return d.Billing.Validate()
//}
//
//type Topup struct {
//	Amount float64
//}
//
//type Config struct {
//	Databases Databases
//	Broker    config.Broker
//	Accounts  config.Accounts
//	Topup     Topup
//}
//
//func (c Config) validate() error {
//	return multierr.Combine(
//		c.Databases.validate(),
//		c.Broker.Validate(),
//		c.Accounts.Validate(),
//	)
//}
//
//func parseConfig(filepath string) (Config, error) {
//	// Parse the file
//	viper.SetConfigFile(filepath)
//	if err := viper.ReadInConfig(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to read the config file")
//	}
//
//	// Unmarshal the config
//	var cfg Config
//	if err := viper.Unmarshal(&cfg); err != nil {
//		return Config{}, errors.Wrap(err, "failed to unmarshal the configuration")
//	}
//
//	// Validate the provided configuration
//	if err := cfg.validate(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to validate the config")
//	}
//	return cfg, nil
//}
//
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	flag.Parse()
//
//	cfg, err := parseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	db, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Billing.Host, cfg.Databases.Billing.Port),
//		User:     cfg.Databases.Billing.User,
//		Password: cfg.Databases.Billing.Password,
//		Database: cfg.Databases.Billing.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer db.Close()
//
//	// Connect to the broker and remember to close it
//	rmq := rabbit.Rabbit{
//		Credits: rabbit.ConnCredits{
//			URL:  cfg.Broker.UserURL,
//			User: cfg.Broker.UserCredits,
//		},
//	}
//	if err = rmq.Init(); err != nil {
//		log.Fatalf("failed to connect to RabbitMQ: %v", err)
//	}
//	defer rmq.CloseRabbit()
//
//	// Create a publisher
//	pub := publisher.Publisher{
//		Rabbit:  &rmq,
//		Encoder: &rabbit.JsonEncoder{},
//	}
//	if err = pub.Init(); err != nil {
//		log.Fatalf("failed to init the publisher: %v", err)
//	}
//	defer pub.Wait(time.Second * 30)
//
//	// Create a service object
//	pgDB, err := repository.NewPg(db, cfg.Accounts)
//	if err != nil {
//		log.Fatalf("failed to init the repository: %v", err)
//	}
//	hdlr := handler.Handler{
//		DB:  pgDB,
//		Pub: &pub,
//		Accounts: handler.Accounts{
//			Owner: handler.Account{
//				Cash: cfg.Accounts.Owner.Cash,
//				Card: cfg.Accounts.Owner.Card,
//			},
//			Gateway: handler.Account{
//				Cash: cfg.Accounts.Gateway.Cash,
//				Card: cfg.Accounts.Gateway.Card,
//			},
//		},
//	}
//
//	// Top up all the existing drivers
//	var drivers []models.Account
//	if err = db.Model(&drivers).
//		Where("account_type = ?", structures.AccountTypeCard).
//		Where("user_type = ?", structures.UserTypeDriver).
//		Select(); err != nil {
//		log.Fatalf("failed to select all driver accounts: %v", err)
//	}
//
//	log.Print("setting drivers balance forcibly...")
//
//	for _, drv := range drivers {
//		topup := proto.NewTransfer{
//			NewTransfer: structures.NewTransfer{
//				IdempotencyKey: structures.GenerateUUID(),
//				PayerAccountType:    structures.AccountTypeCard,
//				TransferType:   structures.TransferTypeForceSet,
//				PayeeUUID:      drv.UserUUID,
//				PayeeType:      structures.UserTypeDriver,
//				Amount:         cfg.Topup.Amount,
//				Description:    "system force set",
//			},
//		}
//		if err := hdlr.CreateTransfer(context.Background(), topup); err != nil {
//			log.Printf("failed to create a top up transfer %v: %v\n", topup, err)
//		}
//	}
//
//	log.Print("done")
//}
