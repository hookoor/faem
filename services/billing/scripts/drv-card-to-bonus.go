// +build ignore

// The script moves existing driver card balance to the bonus account.

package main

//import (
//	"context"
//	"flag"
//	"log"
//	"sync/atomic"
//
//	"github.com/go-pg/pg"
//	"github.com/korovkin/limiter"
//	"github.com/pkg/errors"
//	"github.com/spf13/viper"
//	"go.uber.org/multierr"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/billing/config"
//	"gitlab.com/faemproject/backend/faem/services/billing/handler"
//	"gitlab.com/faemproject/backend/faem/services/billing/models"
//	"gitlab.com/faemproject/backend/faem/services/billing/proto"
//	"gitlab.com/faemproject/backend/faem/services/billing/repository"
//)
//
//type Databases struct {
//	Billing config.Database
//}
//
//func (d Databases) validate() error {
//	return d.Billing.Validate()
//}
//
//type Config struct {
//	Databases Databases
//	Accounts  config.Accounts
//}
//
//func (c Config) validate() error {
//	return multierr.Combine(
//		c.Databases.validate(),
//		c.Accounts.Validate(),
//	)
//}
//
//func parseConfig(filepath string) (Config, error) {
//	// Parse the file
//	viper.SetConfigFile(filepath)
//	if err := viper.ReadInConfig(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to read the config file")
//	}
//
//	// Unmarshal the config
//	var cfg Config
//	if err := viper.Unmarshal(&cfg); err != nil {
//		return Config{}, errors.Wrap(err, "failed to unmarshal the configuration")
//	}
//
//	// Validate the provided configuration
//	if err := cfg.validate(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to validate the config")
//	}
//	return cfg, nil
//}
//
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//
//type PublisherMock struct{}
//
//func (PublisherMock) TransferCompleted(transfer structures.CompletedTransfer) error {
//	return nil
//}
//
//func (PublisherMock) PaymentTypeChangedToCash(prepay structures.PrepayOrder) error {
//	return nil
//}
//
//func (PublisherMock) ReceiptGenerated(receipt structures.ReceiptHook) error {
//	return nil
//}
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	flag.Parse()
//
//	cfg, err := parseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	db, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Billing.Host, cfg.Databases.Billing.Port),
//		User:     cfg.Databases.Billing.User,
//		Password: cfg.Databases.Billing.Password,
//		Database: cfg.Databases.Billing.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer db.Close()
//
//	// Create a service object
//	pgDB, err := repository.NewPg(db, cfg.Accounts)
//	if err != nil {
//		log.Fatalf("failed to init the repository: %v", err)
//	}
//	hdlr := handler.NewHandler(pgDB, PublisherMock{}, handler.Accounts{
//		Owner: handler.Account{
//			Cash:  cfg.Accounts.Owner.Cash,
//			Card:  cfg.Accounts.Owner.Card,
//			Bonus: cfg.Accounts.Owner.Bonus,
//		},
//		Gateway: handler.Account{
//			Cash:  cfg.Accounts.Gateway.Cash,
//			Card:  cfg.Accounts.Gateway.Card,
//			Bonus: cfg.Accounts.Gateway.Bonus,
//		},
//	}, handler.Config{})
//
//	// Move balances for the existing drivers
//
//	log.Println("fetching driver accounts...")
//
//	// Get drivers
//	var drivers []models.Account
//	if err = db.Model(&drivers).
//		ColumnExpr("DISTINCT ON (user_uuid) account.*").
//		Join("INNER JOIN entries AS e ON e.account_id = account.id AND e.balance > 0").
//		Where("user_type = ?", structures.UserTypeDriver).
//		Where("account_type = ?", structures.AccountTypeCard).
//		Where("user_uuid = ?", "afcc761f-7517-4603-a462-c089edffa00f"). // TODO: remove after test
//		Select(); err != nil {
//		log.Fatalf("failed to select all driver accounts: %v", err)
//	}
//
//	log.Printf("fetching driver balances for %d accounts...\n", len(drivers))
//
//	// Get balances
//	driverUUIDs := make([]string, 0, len(drivers))
//	for _, drv := range drivers {
//		driverUUIDs = append(driverUUIDs, drv.UserUUID)
//	}
//
//	balances, err := hdlr.GetAccountsInfo(context.Background(), driverUUIDs, structures.UserTypeDriver)
//	if err != nil {
//		log.Fatalf("failed to get driver balances: %v", err)
//	}
//
//	balancesToMove := make(map[string]structures.BillingAccountsInfo)
//	for drvUUID, balance := range balances.UserAccounts {
//		if balance.Accounts[structures.AccountTypeCard].Balance <= 0 {
//			continue
//		}
//		balancesToMove[drvUUID] = balance
//	}
//
//	log.Printf("moving driver balances, total count: %d...\n", len(balancesToMove))
//
//	limit := limiter.NewConcurrencyLimiter(10)
//	defer limit.Wait()
//
//	var i int32
//	for drvUUID, blnce := range balancesToMove {
//		driverUUID := drvUUID // super important to copy the value
//		balance := blnce      // super important to copy the value
//		limit.Execute(func() {
//			transfer := proto.NewTransfer{
//				NewTransfer: structures.NewTransfer{
//					IdempotencyKey:   structures.GenerateUUID(),
//					TransferType:     structures.TransferTypePayment,
//					PayerUUID:        driverUUID,
//					PayerType:        structures.UserTypeDriver,
//					PayerAccountType: structures.AccountTypeCard,
//					PayeeUUID:        driverUUID,
//					PayeeType:        structures.UserTypeDriver,
//					PayeeAccountType: structures.AccountTypeBonus,
//					Amount:           balance.Accounts[structures.AccountTypeCard].Balance,
//					Description:      "Перевод с безналичного счета на бонусный",
//				},
//			}
//
//			if err := hdlr.CreateTransfer(context.Background(), transfer); err != nil {
//				log.Printf("[ERROR] failed to create a payment transfer %v: %v\n", transfer, err)
//			}
//
//			atomic.AddInt32(&i, 1)
//			if cur := atomic.LoadInt32(&i); cur%10 == 0 {
//				log.Printf("%d done\n", cur)
//			}
//		})
//	}
//
//	log.Println("done")
//}
