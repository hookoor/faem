// +build ignore

package main

//import (
//	"bytes"
//	"encoding/csv"
//	"encoding/json"
//	"flag"
//	"io"
//	"io/ioutil"
//	"log"
//	"net/http"
//	"os"
//	"strconv"
//	"strings"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//	"github.com/spf13/viper"
//
//	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/billing/config"
//	"gitlab.com/faemproject/backend/faem/services/billing/models"
//	"gitlab.com/faemproject/backend/faem/services/billing/proto"
//)
//
//const (
//	colID           = 0
//	colBalance      = 2
//	colNameAndPhone = 1
//
//	minBalance = 100
//)
//
//type databasesConfig struct {
//	Billing config.Database
//}
//
//type billingAuth struct {
//	Username string
//	Password string
//}
//
//type billingConfig struct {
//	Auth billingAuth
//	Host string
//}
//
//type scriptConfig struct {
//	Billing   billingConfig
//	Databases databasesConfig
//}
//
//func parseConfig(filepath string) (scriptConfig, error) {
//	// Parse the file
//	viper.SetConfigFile(filepath)
//	if err := viper.ReadInConfig(); err != nil {
//		return scriptConfig{}, errors.Wrap(err, "failed to read the config file")
//	}
//
//	// Unmarshal the config
//	var cfg scriptConfig
//	if err := viper.Unmarshal(&cfg); err != nil {
//		return scriptConfig{}, errors.Wrap(err, "failed to unmarshal the configuration")
//	}
//	return cfg, nil
//}
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	autoPath := flag.String("auto", "", "import auto file path")
//	flag.Parse()
//
//	cfg, err := parseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//	log.Printf("%+v", cfg)
//
//	// Open the file
//	csvFile, err := os.Open(*autoPath)
//	if err != nil {
//		log.Fatalf("failed to open the csv file: %v", err)
//	}
//
//	// Parse the CSV
//	log.Println("reading auto csv file...")
//
//	importedAccs := make(map[string]*models.Account) // map phone to driver
//	balances := make(map[string]float64)             // map phone to balance
//	csvReader := csv.NewReader(csvFile)
//	for {
//		line, err := csvReader.Read()
//		if err == io.EOF {
//			break
//		}
//		if err != nil {
//			log.Fatalf("failed to read from the csv file: %v", err)
//		}
//
//		id, _ := strconv.Atoi(line[colID])
//		if id == 0 { // skip file header, assuming id field exists for any record
//			continue
//		}
//
//		name, phone := nameAndPhone(line[colNameAndPhone])
//		driverPhone, err := phoneverify.NumberVerify(phone)
//		if err != nil {
//			log.Printf("[ERROR] invalid phone: %s for driver %s \n", phone, name)
//			continue
//		}
//
//		balance, _ := strconv.ParseFloat(strings.ReplaceAll(line[colBalance], ",", "."), 64)
//		acc := models.Account{
//			Phone: driverPhone,
//			Meta:  models.AccountMetadata{ImportID: id},
//		}
//		importedAccs[driverPhone] = &acc
//		balances[driverPhone] = balance
//	}
//
//	log.Println("done")
//
//	// Connect to the databases and remember to close them
//	billingDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Billing.Host, cfg.Databases.Billing.Port),
//		User:     cfg.Databases.Billing.User,
//		Password: cfg.Databases.Billing.Password,
//		Database: cfg.Databases.Billing.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer billingDB.Close()
//
//	// Extract existing accounts
//	var existingAccs []*models.Account
//	if err := billingDB.Model(&existingAccs).
//		Column("id", "user_uuid", "phone").
//		Where("account_type = ?", structures.AccountTypeCard).
//		Select(); err != nil {
//		log.Fatalf("failed to select all accs: %v", err)
//	}
//
//	// Filter known accs
//	var knownAccs []*models.Account
//	for _, exAcc := range existingAccs {
//		if imAcc, ok := importedAccs[exAcc.Phone]; ok {
//			imAcc.ID, imAcc.UserUUID = exAcc.ID, exAcc.UserUUID
//			knownAccs = append(knownAccs, imAcc)
//		}
//	}
//
//	// Update known accs to set the import id and current balance
//	for i, acc := range knownAccs {
//		_, err = billingDB.Model((*models.Account)(nil)).
//			Where("id = ?", acc.ID).
//			Set("meta = jsonb_set(COALESCE(meta, '{}'), '{import_id}', '?')", acc.Meta.ImportID).
//			Update()
//		if err != nil {
//			log.Printf("[ERROR] failed to update an account meta with id %s: %v", acc.ID, err)
//		}
//
//		balance := balances[acc.Phone]
//		if balance < minBalance {
//			continue // TODO: assuming we already set minimum balance for the users
//			balance = minBalance
//		}
//
//		// Update the balance itself
//		updateAcc := proto.ForceArgs{
//			Group:  proto.ForceGroupID,
//			ID:     []string{acc.UserUUID},
//			Amount: balance,
//		}
//		if err := request(
//			"POST",
//			cfg.Billing.Host+"/force/bx6bMza63v5ALkdQ",
//			cfg.Billing.Auth.Username, cfg.Billing.Auth.Password,
//			&updateAcc,
//			nil,
//		); err != nil {
//			log.Printf("[ERROR] failed to update a driver: %v", err)
//		}
//
//		log.Println(i)
//	}
//}
//
//func nameAndPhone(str string) (name string, phone string) {
//	str = strings.TrimSpace(str)
//	split := strings.Split(str, " (")
//	if len(split) < 2 {
//		name = str
//		return
//	}
//	name = strings.TrimSpace(split[0])
//	phone = formatPhone(split[1])
//	return
//}
//
//func formatPhone(str string) string {
//	phone := strings.TrimSpace(str)
//	phone = strings.TrimSuffix(phone, ")")
//	if strings.HasPrefix(phone, "8") {
//		phone = "+7" + phone[1:]
//	}
//	return phone
//}
//
//func request(method, url, username, password string, payload, response interface{}) error {
//	body, err := json.Marshal(payload)
//	if err != nil {
//		return errors.Wrap(err, "failed to marshal a payload")
//	}
//
//	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
//	if err != nil {
//		return errors.Wrap(err, "failed to create an http request")
//	}
//	req.Header.Set("Content-Type", "application/json")
//	req.SetBasicAuth(username, password)
//
//	resp, err := http.DefaultClient.Do(req)
//	if err != nil {
//		return errors.Wrap(err, "failed to make a post request")
//	}
//	defer resp.Body.Close()
//
//	if resp.StatusCode >= http.StatusBadRequest {
//		body, err := ioutil.ReadAll(resp.Body)
//		if err != nil {
//			return errors.Errorf("request to %s failed with status: %d", url, resp.StatusCode)
//		}
//		return errors.Errorf(
//			"request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body),
//		)
//	}
//	if response != nil {
//		return json.NewDecoder(resp.Body).Decode(response)
//	}
//	return nil
//}
