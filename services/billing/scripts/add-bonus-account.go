// +build ignore

// The script syncs drivers between "driver" and "billing" databases.
// It should be executed from a command line by hands.

package main

//import (
//	"flag"
//	"log"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//	"github.com/spf13/viper"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/billing/config"
//	"gitlab.com/faemproject/backend/faem/services/billing/models"
//)
//
//type Databases struct {
//	Billing config.Database
//}
//
//func (d Databases) validate() error {
//	return errors.Wrap(d.Billing.Validate(), "billing database")
//}
//
//type Config struct {
//	Databases Databases
//}
//
//func (c Config) validate() error {
//	return c.Databases.validate()
//}
//
//func parseConfig(filepath string) (Config, error) {
//	// Parse the file
//	viper.SetConfigFile(filepath)
//	if err := viper.ReadInConfig(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to read the config file")
//	}
//
//	// Unmarshal the config
//	var cfg Config
//	if err := viper.Unmarshal(&cfg); err != nil {
//		return Config{}, errors.Wrap(err, "failed to unmarshal the configuration")
//	}
//
//	// Validate the provided configuration
//	if err := cfg.validate(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to validate the config")
//	}
//	return cfg, nil
//}
//
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	flag.Parse()
//
//	cfg, err := parseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	billingDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Billing.Host, cfg.Databases.Billing.Port),
//		User:     cfg.Databases.Billing.User,
//		Password: cfg.Databases.Billing.Password,
//		Database: cfg.Databases.Billing.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer billingDB.Close()
//
//	// Load all the users without bonus accounts
//	var accsWithoutBonus []models.Account
//	err = billingDB.Model(&accsWithoutBonus).
//		ColumnExpr("DISTINCT ON (account.user_uuid, account.user_type, account.phone) " +
//			"account.user_uuid, account.user_type, account.phone, account.meta").
//		Where(`
//NOT EXISTS (
//	SELECT 1
//	FROM accounts AS a2
//	WHERE account.user_uuid = a2.user_uuid
//		AND account.user_type = a2.user_type
//		AND account.phone = a2.phone
//		AND a2.deleted_at IS NULL
//		AND a2.account_type = ?
//)`, structures.AccountTypeBonus).
//		Select()
//	if err != nil {
//		log.Fatalf("failed to select accounts without bonus: %v", err)
//	}
//
//	// Open accounts for unknown drivers
//	log.Print("inserting bonus accounts...")
//
//	for i, acc := range accsWithoutBonus {
//		bonusAcc := models.Account{
//			ID:          structures.GenerateUUID(),
//			PayerAccountType: structures.AccountTypeBonus,
//			UserUUID:    acc.UserUUID,
//			UserType:    acc.UserType,
//			Phone:       acc.Phone,
//			Meta:        acc.Meta,
//		}
//		_, err := billingDB.Model(&bonusAcc).Insert()
//		if err != nil {
//			log.Fatalf("failed to insert bonus accounts: %v", err)
//		}
//		if i % 10 == 0 {
//			log.Println(i)
//		}
//	}
//
//	log.Print("done")
//}
