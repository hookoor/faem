// +build ignore

// The script syncs drivers between "driver" and "billing" databases.
// It should be executed from a command line by hands.

package main

//import (
//	"flag"
//	"log"
//
//	"github.com/go-pg/pg"
//	"github.com/pkg/errors"
//	"github.com/spf13/viper"
//	"go.uber.org/multierr"
//
//	"gitlab.com/faemproject/backend/faem/pkg/store"
//	"gitlab.com/faemproject/backend/faem/pkg/structures"
//	"gitlab.com/faemproject/backend/faem/services/billing/config"
//	"gitlab.com/faemproject/backend/faem/services/billing/models"
//	driverModels "gitlab.com/faemproject/backend/faem/services/driver/models"
//)
//
//type Databases struct {
//	Driver  config.Database
//	Billing config.Database
//}
//
//func (d Databases) validate() error {
//	return multierr.Append(
//		errors.Wrap(d.Driver.Validate(), "driver database"),
//		errors.Wrap(d.Billing.Validate(), "billing database"),
//	)
//}
//
//type Config struct {
//	Databases Databases
//}
//
//func (c Config) validate() error {
//	return c.Databases.validate()
//}
//
//func parseConfig(filepath string) (Config, error) {
//	// Parse the file
//	viper.SetConfigFile(filepath)
//	if err := viper.ReadInConfig(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to read the config file")
//	}
//
//	// Unmarshal the config
//	var cfg Config
//	if err := viper.Unmarshal(&cfg); err != nil {
//		return Config{}, errors.Wrap(err, "failed to unmarshal the configuration")
//	}
//
//	// Validate the provided configuration
//	if err := cfg.validate(); err != nil {
//		return Config{}, errors.Wrap(err, "failed to validate the config")
//	}
//	return cfg, nil
//}
//
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//// ----------------------------------------------------------------------------
//
//func main() {
//	// Parse flags
//	configPath := flag.String("config", "", "configuration file path")
//	flag.Parse()
//
//	cfg, err := parseConfig(*configPath)
//	if err != nil {
//		log.Fatalf("failed to parse the config file: %v", err)
//	}
//
//	log.Printf("%+v", cfg)
//
//	// Connect to the databases and remember to close them
//	driverDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Driver.Host, cfg.Databases.Driver.Port),
//		User:     cfg.Databases.Driver.User,
//		Password: cfg.Databases.Driver.Password,
//		Database: cfg.Databases.Driver.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a driver db instance: %v", err)
//	}
//	defer driverDB.Close()
//
//	// Connect to the databases and remember to close them
//	billingDB, err := store.Connect(&pg.Options{
//		Addr:     store.Addr(cfg.Databases.Billing.Host, cfg.Databases.Billing.Port),
//		User:     cfg.Databases.Billing.User,
//		Password: cfg.Databases.Billing.Password,
//		Database: cfg.Databases.Billing.Db,
//	})
//	if err != nil {
//		log.Fatalf("failed to create a billing db instance: %v", err)
//	}
//	defer billingDB.Close()
//
//	// Load all the drivers from the "drivers" database
//	var drivers []driverModels.DriversApps
//	if err = driverDB.Model(&drivers).
//		Column("uuid", "phone", "alias").
//		Where("deleted IS NOT TRUE").
//		Select(); err != nil {
//		log.Fatalf("failed to select all drivers: %v", err)
//	}
//
//	// Load the existing accounts from the "billing" database and transform them to the map
//	var accounts []models.Account
//	if err = billingDB.Model(&accounts).Column("user_uuid").Select(); err != nil {
//		log.Fatalf("failed to select all accounts: %v", err)
//	}
//	knownUUIDs := make(map[string]struct{})
//	for _, acc := range accounts {
//		knownUUIDs[acc.UserUUID] = struct{}{}
//	}
//
//	// Filter known and unknown drivers
//	var known, unknown []structures.Driver
//	for _, drv := range drivers {
//		if _, found := knownUUIDs[drv.UUID]; found {
//			known = append(known, drv.Driver)
//			continue
//		}
//		unknown = append(unknown, drv.Driver)
//	}
//
//	// Open accounts for unknown drivers
//	log.Print("inserting unknown drivers...")
//	err = billingDB.RunInTransaction(func(tx *pg.Tx) error {
//		for _, drv := range unknown {
//			cashAcc := models.Account{
//				ID:          structures.GenerateUUID(),
//				PayerAccountType: structures.AccountTypeCash,
//				UserUUID:    drv.UUID,
//				UserType:    structures.UserTypeDriver,
//				Phone:       drv.Phone,
//				Meta: models.AccountMetadata{
//					DriverAlias: drv.Alias,
//				},
//			}
//			cardAcc := models.Account{
//				ID:          structures.GenerateUUID(),
//				PayerAccountType: structures.AccountTypeCard,
//				UserUUID:    drv.UUID,
//				UserType:    structures.UserTypeDriver,
//				Phone:       drv.Phone,
//				Meta: models.AccountMetadata{
//					DriverAlias: drv.Alias,
//				},
//			}
//
//			_, err := tx.Model(&cashAcc, &cardAcc).Insert()
//			if err != nil {
//				return err
//			}
//		}
//		return nil
//	})
//	if err != nil {
//		log.Fatalf("failed to insert unknown drivers: %v", err)
//	}
//
//	// Update known accounts
//	log.Print("updating known drivers...")
//	err = billingDB.RunInTransaction(func(tx *pg.Tx) error {
//		for _, drv := range known {
//			var driverAcc models.Account
//			if err := p.DB.ModelContext(ctx, &driverAcc).
//				Where("user_uuid = ?", driver.UUID).
//				Where("user_type = ?", structures.UserTypeDriver).
//				Where("account_type = ?", structures.AccountTypeCard).
//				Limit(1).
//				Select(); err != nil {
//				return errors.Wrap(err, "failed to find an updated driver")
//			}
//
//			updateParams := models.Account{
//				UserUUID: drv.UUID,
//				Phone:    drv.Phone,
//				Meta: models.AccountMetadata{
//					DriverAlias: drv.Alias,
//					ImportID:    driverAcc.Meta.ImportID, // keep the import id
//				},
//			}
//
//			_, err := tx.Model(&updateParams).
//				Where("user_uuid = ?user_uuid").
//				UpdateNotNull()
//			if err != nil {
//				return err
//			}
//		}
//		return nil
//	})
//	if err != nil {
//		log.Fatalf("failed to update known drivers: %v", err)
//	}
//
//	log.Print("done")
//}
