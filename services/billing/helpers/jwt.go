package helpers

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/pkg/errors"
)

func ClientUUIDFromJWT(c echo.Context) (string, error) {
	user, ok := c.Get("user").(*jwt.Token)
	if !ok {
		return "", errors.New("invalid user field")
	}
	claims, ok := user.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid claims")
	}
	userUUID, ok := claims["client_uuid"].(string)
	if !ok {
		return "", errors.New("invalid client claim")
	}
	return userUUID, nil
}

func StaffUUIDFromJWT(c echo.Context) (string, error) {
	user, ok := c.Get("user").(*jwt.Token)
	if !ok {
		return "", errors.New("invalid user field")
	}
	claims, ok := user.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid claims")
	}
	userUUID, ok := claims["user_uuid"].(string)
	if !ok {
		return "", errors.New("invalid user claim")
	}
	return userUUID, nil
}
