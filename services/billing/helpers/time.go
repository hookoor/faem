package helpers

import (
	"fmt"
	"time"
)

var months = [...]string{
	"января",
	"февраля",
	"марта",
	"апреля",
	"мая",
	"июня",
	"июля",
	"августа",
	"сентября",
	"октябра",
	"ноября",
	"декабря",
}

func MonthRus(m time.Month) string {
	if time.January <= m && m <= time.December {
		return months[m-1]
	}
	return fmt.Sprintf("месяц %d", m)
}
