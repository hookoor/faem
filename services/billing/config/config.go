package config

import (
	"fmt"
	"os"
	"strings"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "billing"
)

type Application struct {
	Env       string
	Port      string
	LogLevel  string
	LogFormat string
	TimeZone  string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

func (a *Application) Validate() error {
	//if a.Port == "" {
	//	return errors.New("empty port provided for an http server to start on")
	//}
	return nil
}

type AuthItem struct {
	Username string
	Password string
}

func (a *AuthItem) Validate() error {
	if a.Username == "" {
		return errors.New("empty auth username provided")
	}
	if a.Password == "" {
		return errors.New("empty auth password provided")
	}
	return nil
}

type Auth struct {
	Rpc  AuthItem
	Gate AuthItem
}

func (a *Auth) Validate() error {
	return multierr.Append(
		errors.Wrap(a.Rpc.Validate(), "failed to validate rpc auth"),
		errors.Wrap(a.Rpc.Validate(), "failed to validate gate auth"),
	)
}

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

func (d *Database) Validate() error {
	if d.Host == "" {
		return errors.New("empty db host provided")
	}
	if d.Port == 0 {
		return errors.New("empty db port provided")
	}
	if d.User == "" {
		return errors.New("empty db user provided")
	}
	if d.Password == "" {
		return errors.New("empty db password provided")
	}
	if d.Db == "" {
		return errors.New("empty db name provided")
	}
	return nil
}

type Broker struct {
	UserURL     string
	UserCredits string
}

func (b *Broker) Validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

type (
	Account struct {
		Cash  string
		Card  string
		Bonus string
	}

	Accounts struct {
		Owner   Account
		Gateway Account
	}
)

func (a *Account) Validate() error {
	if a.Cash == "" {
		return errors.New("empty cash account uuid provided")
	}
	if a.Card == "" {
		return errors.New("empty card account uuid provided")
	}
	if a.Bonus == "" {
		return errors.New("empty bonus account uuid provided")
	}
	return nil
}

func (a *Accounts) Validate() error {
	if err := a.Owner.Validate(); err != nil {
		return errors.Wrap(err, "invalid owner account provided")
	}
	if err := a.Gateway.Validate(); err != nil {
		return errors.Wrap(err, "invalid gateway account provided")
	}
	return nil
}

//type ProxyPass struct {
//	Host         string
//	QiwiEndpoint string
//	KitEndpoint  string
//	Username     string
//	Password     string
//}
//
//func (p *ProxyPass) Validate() error {
//	if p.Host == "" {
//		return errors.New("empty reverse proxy host provided")
//	}
//	if p.QiwiEndpoint == "" {
//		return errors.New("empty reverse proxy qiwi endpoint provided")
//	}
//	if p.KitEndpoint == "" {
//		return errors.New("empty reverse proxy kit endpoint provided")
//	}
//	return nil
//}

type PaymentGateway struct {
	Host     string
	Username string
	Password string
	Inn      string
}

func (g *PaymentGateway) Validate() error {
	if g.Host == "" {
		return errors.New("empty payment gateway host provided")
	}
	if g.Username == "" {
		return errors.New("empty payment gateway username provided")
	}
	if g.Password == "" {
		return errors.New("empty payment gateway password provided")
	}
	if g.Inn == "" {
		return errors.New("empty payment gateway inn provided")
	}
	return nil
}

type Interbank struct {
	ServiceActTemplate  string
	AgentReportTemplate string
}

func (r *Interbank) Validate() error {
	if r.ServiceActTemplate == "" {
		return errors.New("empty reports service act template path provided")
	}
	if r.AgentReportTemplate == "" {
		return errors.New("empty reports agent report template path provided")
	}
	return nil
}

type CRM struct {
	Host string
}

func (c *CRM) Validate() error {
	if c.Host == "" {
		return errors.New("empty CRM host provided")
	}
	return nil
}

type Config struct {
	Application    Application
	Auth           Auth
	Database       Database
	Broker         Broker
	Accounts       Accounts
	PaymentGateway PaymentGateway
	Interbank      Interbank
	CRM            CRM
	//ProxyPass   ProxyPass
}

func (c *Config) Validate() error {
	return multierr.Combine(
		c.Application.Validate(),
		c.Auth.Validate(),
		c.Database.Validate(),
		c.Broker.Validate(),
		c.Accounts.Validate(),
		c.PaymentGateway.Validate(),
		c.Interbank.Validate(),
		c.CRM.Validate(),
	)
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.Validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

func (c *Config) Print() {
	if c.Application.IsProduction() {
		return
	}
	inspected := *c // get a copy of an actual object
	// Hide sensitive data
	inspected.Auth = Auth{}
	inspected.Database.User = ""
	inspected.Database.Password = ""
	inspected.Broker.UserCredits = ""
	fmt.Printf("%+v\n", inspected)
}

func setDefaults() {
	viper.SetDefault("Application.Env", "production")
	viper.SetDefault("Application.LogLevel", "debug")
	viper.SetDefault("Application.LogFormat", "text")
	viper.SetDefault("Application.Port", "1326")
	viper.SetDefault("Application.TimeZone", "Europe/Moscow")

	viper.SetDefault("Auth.Rpc.Username", "")
	viper.SetDefault("Auth.Rpc.Password", "")
	viper.SetDefault("Auth.Gate.Username", "")
	viper.SetDefault("Auth.Gate.Password", "")

	viper.SetDefault("Database.Host", "")
	viper.SetDefault("Database.Port", 0)
	viper.SetDefault("Database.User", "")
	viper.SetDefault("Database.Password", "")
	viper.SetDefault("Database.Db", "")

	viper.SetDefault("Broker.UserURL", "")
	viper.SetDefault("Broker.UserCredits", "")

	viper.SetDefault("Accounts.Owner.Cash", "")
	viper.SetDefault("Accounts.Owner.Card", "")
	viper.SetDefault("Accounts.Owner.Bonus", "")
	viper.SetDefault("Accounts.Gateway.Cash", "")
	viper.SetDefault("Accounts.Gateway.Card", "")
	viper.SetDefault("Accounts.Gateway.Bonus", "")

	viper.SetDefault("PaymentGateway.Host", "https://api.cloudpayments.ru")
	viper.SetDefault("PaymentGateway.Username", "")
	viper.SetDefault("PaymentGateway.Password", "")
	viper.SetDefault("PaymentGateway.Inn", "1513077138") // out default INN, not a secret

	viper.SetDefault("Interbank.ServiceActTemplate", "")
	viper.SetDefault("Interbank.AgentReportTemplate", "")

	viper.SetDefault("CRM.Host", "")

	//viper.SetDefault("ProxyPass.Host", "")
	//viper.SetDefault("ProxyPass.QiwiEndpoint", "")
	//viper.SetDefault("ProxyPass.KitEndpoint", "")
	//viper.SetDefault("ProxyPass.Username", "")
	//viper.SetDefault("ProxyPass.Password", "")
}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}
