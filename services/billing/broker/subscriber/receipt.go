package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	maxReceiptsAllowed = 10
)

func (s *Subscriber) HandleGenerateReceipt(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var receipt proto.ReceiptData
	if err := s.Encoder.Decode(msg.Body, &receipt); err != nil {
		return errors.Wrap(err, "failed to decode a receipt")
	}
	log := logs.Eloger.WithField("receipt", receipt)

	// Handle incoming message
	if err := s.Handler.GenerateReceipt(ctx, receipt); err != nil {
		log.Errorf("can't generate a receipt: %s", err)
		return errors.Wrap(err, "failed to generate a receipt")
	}

	log.Debug("receipt generated successfully")
	return nil
}

func (s *Subscriber) initGenerateReceipt() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingGenerateReceiptQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingGenerateReceiptQueue, // name
		true,                               // durable
		false,                              // delete when unused
		false,                              // exclusive
		false,                              // no-wait
		nil,                                // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,             // queue name
		rabbit.GenerateReceipt, // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                            // queue
		rabbit.BillingGenerateReceiptConsumer, // consumer
		false,                                 // auto-ack
		false,                                 // exclusive
		false,                                 // no-local
		false,                                 // no-wait
		nil,                                   // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleGenerateReceipt(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleGenerateReceipt(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxReceiptsAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandleGenerateReceipt(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to generate a receipt: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
