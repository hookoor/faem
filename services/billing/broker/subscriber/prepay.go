package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	maxPrepayOrdersAllowed = 10
)

func (s *Subscriber) HandlePrepayOrder(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var prepay proto.PrepayOrder
	if err := s.Encoder.Decode(msg.Body, &prepay); err != nil {
		return errors.Wrap(err, "failed to decode a prepay")
	}
	log := logs.Eloger.WithField("prepay", prepay)

	// Handle incoming message
	if err := s.Handler.PrepayOrder(ctx, prepay); err != nil {
		log.Errorf("can't prepay an order: %s", err)
		return errors.Wrap(err, "failed to prepay an order")
	}

	log.Debug("order prepayed successfully")
	return nil
}

func (s *Subscriber) initPrepayOrder() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingPrepayOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingPrepayOrderQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,             // queue name
		rabbit.PrepayKey,       // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                        // queue
		rabbit.BillingPrepayOrderConsumer, // consumer
		false,                             // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handlePrepayOrder(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handlePrepayOrder(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxPrepayOrdersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandlePrepayOrder(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to handle a new prepay: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
