package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	maxClientRefundsAllowed = 10
)

func (s *Subscriber) HandleRefundClient(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var refund proto.PrepayOrder
	if err := s.Encoder.Decode(msg.Body, &refund); err != nil {
		return errors.Wrap(err, "failed to decode a refund")
	}
	log := logs.Eloger.WithField("refund", refund)

	// Handle incoming message
	if err := s.Handler.RefundClient(ctx, refund); err != nil {
		log.Errorf("can't refund a client: %s", err)
		return errors.Wrap(err, "failed to refund a client")
	}

	log.Debug("refund client successfully")
	return nil
}

func (s *Subscriber) initRefundClient() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingRefundClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingRefundClientQueue, // name
		true,                            // durable
		false,                           // delete when unused
		false,                           // exclusive
		false,                           // no-wait
		nil,                             // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,             // queue name
		rabbit.RefundClient,    // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                         // queue
		rabbit.BillingRefundClientConsumer, // consumer
		false,                              // auto-ack
		false,                              // exclusive
		false,                              // no-local
		false,                              // no-wait
		nil,                                // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleRefundClient(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleRefundClient(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxClientRefundsAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandleRefundClient(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to handle client refund: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
