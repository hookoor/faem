package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const maxNewTransfersAllowed = 10

func (s *Subscriber) HandleNewTransfer(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var transfer proto.NewTransfer
	if err := s.Encoder.Decode(msg.Body, &transfer); err != nil {
		return errors.Wrap(err, "failed to decode a new transfer")
	}
	log := logs.Eloger.WithField("new_transfer", transfer)

	// Handle incoming message somehow
	if err := s.Handler.CreateTransfer(ctx, transfer); err != nil {
		log.Errorf("can't create a transfer: %s", err)
		return errors.Wrap(err, "failed to create a transfer")
	}

	log.Debug("new transfer created successfully")
	return nil
}

func (s *Subscriber) initTransferCreated() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingNewTransferQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingNewTransferQueue, // name
		true,                           // durable
		false,                          // delete when unused
		false,                          // exclusive
		false,                          // no-wait
		nil,                            // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,             // queue name
		rabbit.NewKey,          // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                        // queue
		rabbit.BillingNewTransferConsumer, // consumer
		false,                             // auto-ack
		false,                             // exclusive
		false,                             // no-local
		false,                             // no-wait
		nil,                               // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewTransfers(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewTransfers(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewTransfersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandleNewTransfer(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to handle a new transfer: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
