package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"go.uber.org/multierr"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	maxNewDriversAllowed     = 10
	maxUpdatedDriversAllowed = 10
	maxDeletedDriversAllowed = 10
	maxNewClientsAllowed     = 10
	maxUpdatedClientsAllowed = 10
	maxDeletedClientsAllowed = 10
)

func (s *Subscriber) HandleNewDriver(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var driver structures.Driver
	if err := s.Encoder.Decode(msg.Body, &driver); err != nil {
		return errors.Wrap(err, "failed to decode a new driver")
	}

	// Handle incoming message somehow
	_, _, _, err := s.Handler.OpenDriverAccount(ctx, &driver)
	if err != nil {
		return errors.Wrap(err, "failed to open a new driver account")
	}

	logs.Eloger.WithField("driverUUID", driver.UUID).Debug("new driver account created successfully")
	return nil
}

func (s *Subscriber) HandleUpdatedDriver(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var driver structures.Driver
	if err := s.Encoder.Decode(msg.Body, &driver); err != nil {
		return errors.Wrap(err, "failed to decode an updated driver")
	}

	// Handle incoming message somehow
	if err := s.Handler.UpdateDriverAccount(ctx, &driver); err != nil {
		return errors.Wrap(err, "failed to update a driver account")
	}

	logs.Eloger.WithField("driverUUID", driver.UUID).Info("driver account updated successfully")
	return nil
}

func (s *Subscriber) HandleDeletedDriver(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var driver structures.DeletedObject
	if err := s.Encoder.Decode(msg.Body, &driver); err != nil {
		return errors.Wrap(err, "failed to decode an deleted driver")
	}

	// Handle incoming message somehow
	if err := s.Handler.DeleteDriverAccount(ctx, driver); err != nil {
		return errors.Wrap(err, "failed to delete a driver account")
	}

	logs.Eloger.WithField("driverUUID", driver.UUID).Info("driver account deleted successfully")
	return nil
}

func (s *Subscriber) HandleNewClient(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var client structures.Client
	if err := s.Encoder.Decode(msg.Body, &client); err != nil {
		return errors.Wrap(err, "failed to decode a new client")
	}

	// Handle incoming message somehow
	_, _, _, err := s.Handler.OpenClientAccount(ctx, &client)
	if err != nil {
		return errors.Wrap(err, "failed to open a new client account")
	}

	// Reward for the registration in the app
	if err = s.Handler.RewardRegisteredClient(ctx, &client); err != nil {
		return errors.Wrap(err, "failed to reward a registered client")
	}

	logs.Eloger.WithField("clientUUID", client.UUID).Info("new client account created successfully")
	return nil
}

func (s *Subscriber) HandleUpdatedClient(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var client structures.Client
	if err := s.Encoder.Decode(msg.Body, &client); err != nil {
		return errors.Wrap(err, "failed to decode an updated client")
	}

	// Handle incoming message somehow
	if err := s.Handler.UpdateClientAccount(ctx, &client); err != nil {
		return errors.Wrap(err, "failed to update a client account")
	}

	logs.Eloger.WithField("clientUUID", client.UUID).Info("client account updated successfully")
	return nil
}

func (s *Subscriber) HandleDeletedClient(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var client structures.DeletedObject
	if err := s.Encoder.Decode(msg.Body, &client); err != nil {
		return errors.Wrap(err, "failed to decode an deleted client")
	}

	// Handle incoming message somehow
	if err := s.Handler.DeleteClientAccount(ctx, client); err != nil {
		return errors.Wrap(err, "failed to delete a client account")
	}

	logs.Eloger.WithField("clientUUID", client.UUID).Info("client account deleted successfully")
	return nil
}

func (s *Subscriber) initAccountCreated() error {
	return multierr.Combine(
		s.initDriverAccountCreated(),
		s.initDriverAccountUpdated(),
		s.initDriverAccountDeleted(),

		s.initClientAccountCreated(),
		s.initClientAccountUpdated(),
		s.initClientAccountDeleted(),
	)
}

func (s *Subscriber) initDriverAccountCreated() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingNewDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingNewDriverQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.NewKey,         // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                      // queue
		rabbit.BillingNewDriverConsumer, // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewDrivers(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewDrivers(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewDriversAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleNewDriver(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle a new driver: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initDriverAccountUpdated() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingUpdatedDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingUpdatedDriverQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                          // queue
		rabbit.BillingUpdatedDriverConsumer, // consumer
		true,                                // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleUpdatedDrivers(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleUpdatedDrivers(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxUpdatedDriversAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Driver updating is allowed from the CRM service only for now
			if msg.Headers["publisher"] != "crm" {
				continue
			}
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleUpdatedDriver(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle an updated driver: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initDriverAccountDeleted() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingDeletedDriverQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.DriverExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingDeletedDriverQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.DeleteKey,      // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                          // queue
		rabbit.BillingDeletedDriverConsumer, // consumer
		true,                                // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleDeletedDrivers(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleDeletedDrivers(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxDeletedDriversAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Driver deletion is allowed from the CRM service only for now
			if msg.Headers["publisher"] != "crm" {
				continue
			}
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleDeletedDriver(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle an deleted driver: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initClientAccountCreated() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingNewClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingNewClientQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.NewKey,         // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                      // queue
		rabbit.BillingNewClientConsumer, // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewClients(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewClients(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewClientsAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleNewClient(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle a new client: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initClientAccountUpdated() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingUpdatedClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingUpdatedClientQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                          // queue
		rabbit.BillingUpdatedClientConsumer, // consumer
		true,                                // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleUpdatedClients(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleUpdatedClients(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxUpdatedClientsAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Client updating is allowed from the CRM service only for now
			if msg.Headers["publisher"] != "crm" {
				continue
			}
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleUpdatedClient(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle an updated driver: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) initClientAccountDeleted() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingDeletedClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingDeletedClientQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,            // queue name
		rabbit.DeleteKey,      // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                          // queue
		rabbit.BillingDeletedClientConsumer, // consumer
		true,                                // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleDeletedClients(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleDeletedClients(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxDeletedClientsAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Client deletion is allowed from the CRM service only for now
			if msg.Headers["publisher"] != "crm" {
				continue
			}
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleDeletedClient(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle an deleted driver: %v", err)
					}
				},
			))
		}
	}
}
