package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/billing/proto"
)

const (
	maxClientPenaltiesAllowed = 10
)

func (s *Subscriber) HandlePenaltyClient(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var penalty proto.PrepayOrder
	if err := s.Encoder.Decode(msg.Body, &penalty); err != nil {
		return errors.Wrap(err, "failed to decode a penalty")
	}
	log := logs.Eloger.WithField("penalty", penalty)

	// Handle incoming message
	if err := s.Handler.PenaltyClient(ctx, penalty); err != nil {
		log.Errorf("can't penalty a client: %s", err)
		return errors.Wrap(err, "failed to penalty a client")
	}

	log.Debug("penalty client successfully")
	return nil
}

func (s *Subscriber) initPenaltyClient() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.BillingPenaltyClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.BillingPenaltyClientQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,             // queue name
		rabbit.PenaltyClient,   // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                          // queue
		rabbit.BillingPenaltyClientConsumer, // consumer
		false,                               // auto-ack
		false,                               // exclusive
		false,                               // no-local
		false,                               // no-wait
		nil,                                 // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handlePenaltyClient(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handlePenaltyClient(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxClientPenaltiesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start a new goroutine to handle multiple requests at the same time
			limit.Execute(func() {
				err := lang.RecoverWithError(func() error {
					return s.HandlePenaltyClient(context.Background(), msg)
				})
				if err != nil {
					logs.Eloger.Errorf("failed to handle client penalty: %v", err)
					msg.Reject(false)
					return
				}
				msg.Ack(false)
			})
		}
	}
}
