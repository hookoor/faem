package publisher

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	channelNamePaymentTypeChanged = "paymentTypeChanged"
)

func (p *Publisher) PaymentTypeChangedToCash(prepay structures.PrepayOrder) error {
	senderChannel, err := p.Rabbit.GetSender(channelNamePaymentTypeChanged)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}
	return p.Publish(senderChannel, rabbit.BillingExchange, rabbit.PaymentTypeChangedToCash, prepay)
}

func (p *Publisher) initPaymentTypeChangedToCash() error {
	senderChannel, err := p.Rabbit.GetSender(channelNamePaymentTypeChanged)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	err = senderChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	return errors.Wrap(err, "failed to create an exchange")
}
