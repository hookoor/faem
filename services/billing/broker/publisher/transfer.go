package publisher

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	channelNameTransferCompleted = "transferCompleted"
)

func (p *Publisher) TransferCompleted(transfer structures.CompletedTransfer) error {
	senderChannel, err := p.Rabbit.GetSender(channelNameTransferCompleted)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}
	return p.Publish(senderChannel, rabbit.BillingExchange, rabbit.CompletedKey, transfer)
}

func (p *Publisher) initTransferCompleted() error {
	senderChannel, err := p.Rabbit.GetSender(channelNameTransferCompleted)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	err = senderChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	return errors.Wrap(err, "failed to create an exchange")
}
