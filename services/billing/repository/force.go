package repository

import (
	"context"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) GetCardDriverUUIDs(ctx context.Context) ([]string, error) {
	var drivers []models.Account
	if err := p.DB.Model(&drivers).
		Column("user_uuid").
		Where("account_type = ?", structures.AccountTypeCard).
		Where("user_type = ?", structures.UserTypeDriver).
		Select(); err != nil {
		return nil, errors.Wrapf(err, "failed to select driver uuids")
	}

	result := make([]string, 0, len(drivers))
	for _, drv := range drivers {
		result = append(result, drv.UserUUID)
	}
	return result, nil
}
