package repository

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) CreateTransfer(ctx context.Context, tx *pg.Tx, transfer models.Transfer) error {
	_, err := tx.ModelContext(ctx, &transfer).Insert()
	return errors.Wrap(err, "failed to insert transfer to the db")
}

func (p *Pg) TransferExists(ctx context.Context, id string) (bool, error) {
	return p.DB.ModelContext(ctx, (*models.Transfer)(nil)).
		Where("id = ?", id).
		Exists()
}

func (p *Pg) GetTransferByExternalID(ctx context.Context, externalID string) (models.Transfer, error) {
	var result models.Transfer
	err := p.DB.ModelContext(ctx, &result).
		Where("external_id = ?", externalID).
		Limit(1).
		Select()
	return result, err
}
