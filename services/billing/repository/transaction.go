package repository

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) CreateTransactions(ctx context.Context, tx *pg.Tx, transactions ...*models.Transaction) error {
	// Transform accounts to pass to go-pg
	txes := make([]interface{}, 0, len(transactions))
	for _, tx := range transactions {
		txes = append(txes, tx)
	}

	_, err := tx.ModelContext(ctx, txes...).Insert()
	return errors.Wrap(err, "failed to insert transactions to the db")
}
