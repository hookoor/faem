package repository

import (
	"context"
	"time"

	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) CreateBankTransfer(ctx context.Context, transfer *models.BankTransfer) error {
	_, err := p.DB.ModelContext(ctx, transfer).
		OnConflict("(id) DO NOTHING").
		Insert()
	return err
}

func (p *Pg) PrepayExists(ctx context.Context, id, subjectUUID string) (bool, error) {
	return p.DB.ModelContext(ctx, (*models.BankTransfer)(nil)).
		Where("id = ?", id).
		Where("subject_uuid = ?", subjectUUID).
		Exists()
}

func (p *Pg) FindPrepayByUserAndSubjectUUIDs(ctx context.Context, userUUID, subjectUUID string) (*models.BankTransfer, error) {
	var result models.BankTransfer
	err := p.DB.ModelContext(ctx, &result).
		Where("user_uuid = ?", userUUID).
		Where("subject_uuid = ?", subjectUUID).
		Where("prepaid_at IS NOT NULL").
		Limit(1).
		Select()
	if err != nil {
		return nil, err
	}
	return &result, nil
}

func (p *Pg) ConfirmPrepay(ctx context.Context, id string) error {
	_, err := p.DB.ModelContext(ctx, (*models.BankTransfer)(nil)).
		Set("confirmed_at = ?", time.Now()).
		Where("id = ?", id).
		Update()
	return err
}

func (p *Pg) RefundPrepay(ctx context.Context, id string) error {
	_, err := p.DB.ModelContext(ctx, (*models.BankTransfer)(nil)).
		Set("refunded_at = ?", time.Now()).
		Where("id = ?", id).
		Update()
	return err
}
