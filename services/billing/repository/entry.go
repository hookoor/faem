package repository

import (
	"context"

	"github.com/pkg/errors"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

// CreateEntries inserts new entries to the db. Balance will be recalculated in the db trigger.
func (p *Pg) CreateEntries(ctx context.Context, tx *pg.Tx, entries ...*models.Entry) error {
	// Transform accounts to pass to go-pg
	iEntries := make([]interface{}, 0, len(entries))
	for _, entry := range entries {
		iEntries = append(iEntries, entry)
	}

	_, err := tx.ModelContext(ctx, iEntries...).Insert()
	return errors.Wrap(err, "failed to insert entries to the db")
}
