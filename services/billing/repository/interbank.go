package repository

import (
	"context"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) InterbankTransactions(ctx context.Context, req models.InterbankDocumentRequest) (models.InterbankTransactions, error) {
	var result models.InterbankTransactions
	err := p.DB.ModelContext(ctx, (*models.Transfer)(nil)).
		ColumnExpr("SUM(transfer.amount) AS amount_total").
		ColumnExpr("SUM((transfer.meta ->> 'fee_amount')::double precision) AS amount_fee").
		Join("INNER JOIN transactions AS tx ON tx.id = transfer.transaction_ids[1]").
		Join(
			"INNER JOIN entries AS e ON tx.credit_id = e.id AND e.entry_type = ?",
			models.EntryTypeCredit,
		).
		Join(
			"INNER JOIN accounts AS a ON e.account_id = a.id AND a.user_type = ? AND a.account_type = ?",
			structures.UserTypeClient, structures.AccountTypeCard,
		).
		Where("transfer.created_at >= ?", req.PeriodStart).
		Where("transfer.created_at <= ?", req.PeriodEnd).
		Where("transfer.transfer_type = ?", structures.TransferTypePayment).
		Where("transfer.meta -> 'driver_taxi_park' ->> 'uuid' = ?", req.TaxiParkUUID).
		Limit(1).
		Select(&result)
	if err != nil {
		return models.InterbankTransactions{}, err
	}
	return result, nil
}
