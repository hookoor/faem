package repository

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

// CreateAccounts create multiple accounts using a database transaction
func (p *Pg) CreateAccounts(ctx context.Context, accounts ...*models.Account) error {
	// Transform accounts to pass to go-pg
	accs := make([]interface{}, 0, len(accounts))
	for _, acc := range accounts {
		accs = append(accs, acc)
	}

	_, err := p.DB.ModelContext(ctx, accs...).Insert()
	return errors.Wrap(err, "failed to insert accounts to the db")
}

func (p *Pg) GetAccountsWithEntryByIDsAndType(
	ctx context.Context, userIDs []string, userType structures.BillingUserType,
) (map[string][]*models.Account, error) {
	idField := "user_uuid"
	if userType == structures.UserTypeClient {
		idField = "phone"
	}

	var accounts []*models.Account
	err := p.DB.ModelContext(ctx, &accounts).
		Column("account.*").
		WhereIn(fmt.Sprintf("%s IN (?)", idField), userIDs).
		Where("user_type = ?", userType).
		Order("created_at DESC").
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to select user accounts")
	}

	if len(accounts) < 1 {
		return nil, nil // no error required
	}

	// Join entries
	accIds := make([]string, 0, len(accounts))
	for _, acc := range accounts {
		accIds = append(accIds, acc.ID)
	}
	with := p.DB.Model((*models.Entry)(nil)).
		Column("account_id").
		ColumnExpr("max(created_at) created_at").
		WhereIn("account_id IN (?)", accIds).
		Group("account_id")
	var entries []models.Entry
	err = p.DB.ModelContext(ctx).
		With("q", with).
		Table("q").
		Column("e.account_id", "e.balance").
		Join("INNER JOIN entries AS e ON e.account_id = q.account_id AND e.created_at = q.created_at").
		Select(&entries)
	if err != nil {
		return nil, errors.Wrap(err, "failed to select entries accounts")
	}

	// map entries to accounts and accounts to user ids
	result := make(map[string][]*models.Account)
	for _, acc := range accounts {
		for _, entry := range entries {
			if acc.ID == entry.AccountID {
				acc.Entry = &entry
				break
			}
		}
		if userType == structures.UserTypeDriver {
			result[acc.UserUUID] = append(result[acc.UserUUID], acc)
		} else { // client
			result[acc.Phone] = append(result[acc.Phone], acc)
		}
	}
	return result, nil
}

func (p *Pg) GetAccountByUserIDAndTypeAndUserType(
	ctx context.Context, userUUID string, accountType structures.BillingAccountType, userType structures.BillingUserType,
) (models.Account, error) {
	var account models.Account
	err := p.DB.ModelContext(ctx, &account).
		Where("user_uuid = ?", userUUID).
		Where("account_type = ?", accountType).
		Where("user_type = ?", userType).
		Limit(1).
		Select()
	if err != nil {
		return models.Account{}, errors.Wrap(err, "failed to select an account from the db")
	}
	return account, nil
}

func (p *Pg) GetAccountByPhoneAndTypeAndUserType(
	ctx context.Context, phone string, accountType structures.BillingAccountType, userType structures.BillingUserType,
) (models.Account, error) {
	var account models.Account
	err := p.DB.ModelContext(ctx, &account).
		Where("phone = ?", phone).
		Where("account_type = ?", accountType).
		Where("user_type = ?", userType).
		Limit(1).
		Select()
	if err != nil {
		return models.Account{}, errors.Wrap(err, "failed to select an account from the db")
	}
	return account, nil
}

func (p *Pg) GetAccountByUserIDOrPhoneAndTypeAndUserType(
	ctx context.Context, userUUIDOrPhone string, accountType structures.BillingAccountType, userType structures.BillingUserType,
) (models.Account, error) {
	var account models.Account
	err := p.DB.ModelContext(ctx, &account).
		Where("user_uuid = ? OR phone = ?", userUUIDOrPhone, userUUIDOrPhone).
		Where("account_type = ?", accountType).
		Where("user_type = ?", userType).
		Limit(1).
		Select()
	if err != nil {
		return models.Account{}, errors.Wrap(err, "failed to select an account from the db")
	}
	return account, nil
}

func (p *Pg) AccountExistsByPhoneAndUserType(ctx context.Context, phone string, userType structures.BillingUserType) (
	bool, error,
) {
	if phone == "" {
		return false, errors.New("empty phone provided")
	}
	exists, err := p.DB.ModelContext(ctx, (*models.Account)(nil)).
		Where("phone = ?", phone).
		Where("user_type = ?", userType).
		Exists()
	if err != nil {
		return false, errors.Wrap(err, "failed to select an account from the db")
	}
	return exists, nil
}

func (p *Pg) GetAccountsByPhoneAndUserType(
	ctx context.Context, phone string, userType structures.BillingUserType,
) ([]models.Account, error) {
	if phone == "" {
		return nil, errors.New("empty user phone provided")
	}
	var result []models.Account
	err := p.DB.ModelContext(ctx, &result).
		Where("phone = ?", phone).
		Where("user_type = ?", userType).
		Select()
	if err != nil {
		return nil, errors.Wrap(err, "failed to get user accounts")
	}
	return result, nil
}

func (p *Pg) UpdateUserUUIDByPhoneAndUserType(
	ctx context.Context, userUUID, phone string, userType structures.BillingUserType,
) ([]models.Account, error) {
	if userUUID == "" {
		return nil, errors.New("empty user uuid provided")
	}
	var result []models.Account
	_, err := p.DB.ModelContext(ctx, &result).
		Where("phone = ?", phone).
		Where("user_type = ?", userType).
		Set("user_uuid = ?", userUUID).
		Returning("*").
		Update()
	if err != nil {
		return nil, errors.Wrap(err, "failed to update user account")
	}
	return result, nil
}

func (p *Pg) UpdateDriverAccountPhone(ctx context.Context, driver *structures.Driver) error {
	if driver == nil {
		return errors.New("nil driver provided")
	}
	if driver.UUID == "" {
		return errors.New("empty client uuid provided")
	}

	// First, load the account itself to keep the meta
	var driverAcc models.Account
	if err := p.DB.ModelContext(ctx, &driverAcc).
		Column("phone", "meta").
		Where("user_uuid = ?", driver.UUID).
		Where("user_type = ?", structures.UserTypeDriver).
		Where("account_type = ?", structures.AccountTypeCard).
		Limit(1).
		Select(); err != nil {
		return errors.Wrap(err, "failed to find an updated driver")
	}

	// Check if need to update
	if driverAcc.Phone == driver.Phone && driverAcc.Meta.DriverAlias == driver.Alias {
		return nil // no need to update
	}

	updateParams := models.Account{
		UserUUID: driver.UUID,
		Phone:    driver.Phone,
		Meta: models.AccountMetadata{
			DriverAlias: driver.Alias,
			ImportID:    driverAcc.Meta.ImportID, // keep the import id
		},
	}
	_, err := p.DB.ModelContext(ctx, &updateParams).
		Where("user_uuid = ?user_uuid").
		UpdateNotNull()
	return err
}

func (p *Pg) CloseDriverAccount(ctx context.Context, driver structures.DeletedObject) error {
	if driver.UUID == "" {
		return errors.New("empty driver uuid provided")
	}
	_, err := p.DB.ModelContext(ctx, (*models.Account)(nil)).
		Where("user_uuid = ?", driver.UUID).
		Where("user_type = ?", structures.UserTypeDriver).
		Set("deleted_at = NOW()").
		Update()
	return err
}

func (p *Pg) UpdateClientAccountPhone(ctx context.Context, client *structures.Client) error {
	if client == nil {
		return errors.New("nil client provided")
	}
	if client.UUID == "" {
		return errors.New("empty client uuid provided")
	}

	// First, load the account itself to keep the meta
	var clientAcc models.Account
	if err := p.DB.ModelContext(ctx, &clientAcc).
		Column("phone").
		Where("user_uuid = ?", client.UUID).
		Where("user_type = ?", structures.UserTypeClient).
		Where("account_type = ?", structures.AccountTypeCard).
		Limit(1).
		Select(); err != nil {
		return errors.Wrap(err, "failed to find an updated client")
	}

	// Check if need to update
	if clientAcc.Phone == client.MainPhone {
		return nil // no need to update
	}

	updateParams := models.Account{
		UserUUID: client.UUID,
		Phone:    client.MainPhone,
	}
	_, err := p.DB.ModelContext(ctx, &updateParams).
		Where("user_uuid = ?user_uuid").
		UpdateNotNull()
	return err
}

func (p *Pg) CloseClientAccount(ctx context.Context, client structures.DeletedObject) error {
	if client.UUID == "" {
		return errors.New("empty driver uuid provided")
	}
	_, err := p.DB.ModelContext(ctx, (*models.Account)(nil)).
		Where("user_uuid = ?", client.UUID).
		Where("user_type = ?", structures.UserTypeClient).
		Set("deleted_at = NOW()").
		Update()
	return err
}

//func (p *Pg) joinLastEntry(ctx context.Context, account models.Account) (models.Account, error) {
//	// Join the last entry
//	var entry models.Entry
//	err := p.DB.ModelContext(ctx, &entry).
//		Where("account_id = ?", account.ID).
//		Order("created_at DESC").
//		Limit(1).
//		Select()
//	if err != nil {
//		return models.Account{}, errors.Wrap(err, "failed to select entries accounts")
//	}
//	account.Entry = &entry
//	return account, nil
//}
