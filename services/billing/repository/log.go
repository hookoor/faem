package repository

import (
	"context"

	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) FlushLogs(ctx context.Context, logs ...models.Log) error {
	_, err := p.DB.ModelContext(ctx, &logs).Insert()
	return err
}
