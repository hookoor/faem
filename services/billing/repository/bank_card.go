package repository

import (
	"context"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

func (p *Pg) IndexClientBankCards(ctx context.Context, clientUUID string) ([]*models.BankCard, error) {
	var cards []*models.BankCard
	err := p.DB.ModelContext(ctx, &cards).
		Where("user_uuid = ?", clientUUID).
		Order("created_at DESC").
		Select()
	return cards, err
}

func (p *Pg) FindLatestClientBankCardByUserUUID(ctx context.Context, clientUUID string) (*models.BankCard, error) {
	var card models.BankCard
	err := p.DB.ModelContext(ctx, &card).
		Where("user_uuid = ?", clientUUID).
		Order("created_at DESC").
		Limit(1).
		Select()
	return &card, err
}

func (p *Pg) CreateClientBankCard(ctx context.Context, card *models.BankCard) error {
	card.ID = structures.GenerateUUID()
	_, err := p.DB.ModelContext(ctx, card).Insert()
	return err
}

func (p *Pg) BankCardExistsByUserUUIDAndSuffix(ctx context.Context, userUUID, suffix string) (bool, error) {
	return p.DB.ModelContext(ctx, (*models.BankCard)(nil)).
		Where("user_uuid = ?", userUUID).
		Where("card_suffix = ?", suffix).
		Exists()
}

func (p *Pg) BankCardExistsByUserUUID(ctx context.Context, userUUID string) (bool, error) {
	return p.DB.ModelContext(ctx, (*models.BankCard)(nil)).
		Where("user_uuid = ?", userUUID).
		Exists()
}

func (p *Pg) DeleteClientBankCardByIDAndUserUUID(ctx context.Context, cardID, userUUID string) error {
	_, err := p.DB.ModelContext(ctx, (*models.BankCard)(nil)).
		Where("id = ?", cardID).
		Where("user_uuid = ?", userUUID).
		Set("deleted_at = NOW()").
		Update()
	return err
}
