package repository

import (
	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/billing/config"
	"gitlab.com/faemproject/backend/faem/services/billing/models"
)

type Pg struct {
	*pg.DB
}

func NewPg(db *pg.DB, initialAccounts config.Accounts) (*Pg, error) {
	if err := createInitialAccounts(db, initialAccounts); err != nil {
		return nil, errors.Wrap(err, "failed to create initial accounts")
	}
	return &Pg{DB: db}, nil
}

func createInitialAccounts(db *pg.DB, initialAccounts config.Accounts) error {
	ownerCash := models.Account{
		ID:          initialAccounts.Owner.Cash,
		AccountType: structures.AccountTypeCash,
		UserType:    structures.UserTypeOwner,
	}
	ownerCard := models.Account{
		ID:          initialAccounts.Owner.Card,
		AccountType: structures.AccountTypeCard,
		UserType:    structures.UserTypeOwner,
	}
	ownerBonus := models.Account{
		ID:          initialAccounts.Owner.Bonus,
		AccountType: structures.AccountTypeBonus,
		UserType:    structures.UserTypeOwner,
	}
	gateCash := models.Account{
		ID:          initialAccounts.Gateway.Cash,
		AccountType: structures.AccountTypeCash,
		UserType:    structures.UserTypeGateway,
	}
	gateCard := models.Account{
		ID:          initialAccounts.Gateway.Card,
		AccountType: structures.AccountTypeCard,
		UserType:    structures.UserTypeGateway,
	}
	gateBonus := models.Account{
		ID:          initialAccounts.Gateway.Bonus,
		AccountType: structures.AccountTypeBonus,
		UserType:    structures.UserTypeGateway,
	}
	_, err := db.Model(&ownerCash, &ownerCard, &ownerBonus, &gateCash, &gateCard, &gateBonus).
		OnConflict("(id) DO NOTHING").
		Insert()
	return errors.Wrap(err, "failed to insert initial accounts")
}
