package main

import (
	"flag"
	"log"
	"net/http"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/store"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/broker/postgres"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/broker/rabbitmq"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/config"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/handler"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/repository/external"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/repository/inner"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/rpc"
)

const (
	defaultConfigPath = "config/limesidecar.toml"
	shutdownTimeout   = 30 * time.Second
)

func main() {
	// Parse flags
	configPath := flag.String("config", defaultConfigPath, "configuration file path")
	flag.Parse()

	cfg, err := config.Parse(*configPath)
	if err != nil {
		log.Fatalf("failed to parse the config file: %v", err)
	}

	cfg.Print() // just for debugging

	if err := logs.SetLogLevel(cfg.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(cfg.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	logger := logs.Eloger

	// Connect to the external db and remember to close it
	edb, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.External.Host, cfg.External.Port),
		User:     cfg.External.User,
		Password: cfg.External.Password,
		Database: cfg.External.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create an external db instance: %v", err)
	}
	defer edb.Close()

	// Connect to the internal db and remember to close it
	idb, err := store.Connect(&pg.Options{
		Addr:     store.Addr(cfg.Database.Host, cfg.Database.Port),
		User:     cfg.Database.User,
		Password: cfg.Database.Password,
		Database: cfg.Database.Db,
	})
	if err != nil {
		logger.Fatalf("failed to create an internal db instance: %v", err)
	}
	defer idb.Close()

	serviceMapper := handler.NewServiceMapper(cfg.Mappings.ServiceMapping)
	remoteClient := rpc.RemoteClient{
		HttpClient: &http.Client{
			Timeout: cfg.Client.Timeout,
		},
		Config: rpc.Config{
			BaseURL:        cfg.Client.BaseURL,
			CreateOrderURL: cfg.Client.CreateOrderURL,
			CancelOrderURL: cfg.Client.CancelOrderURL,
			JWT:            cfg.Client.JWT,
		},
	}
	hdlr := handler.Handler{
		Config: handler.Config{
			DistributionTimeout: cfg.External.DistributionTimeout,
			DataTransferTimeout: cfg.External.DataTransferTimeout,
		},
		Client:        &remoteClient,
		DB:            &inner.Pg{Db: idb},
		ExternalDB:    &external.Pg{Db: edb},
		ServiceMapper: serviceMapper,
	}
	hdlr.Init()
	defer hdlr.Wait(shutdownTimeout)

	esub := postgres.NewSubscriber(
		edb.Listen(cfg.External.CreateChannel),
		edb.Listen(cfg.External.CancelChannel),
		&external.Pg{Db: edb},
		&inner.Pg{Db: idb},
		&hdlr,
		serviceMapper,
	)
	defer esub.Wait(shutdownTimeout)

	// Connect to the broker and remember to close it
	rmq := &rabbit.Rabbit{
		Credits: rabbit.ConnCredits{
			URL:  cfg.Broker.UserURL,
			User: cfg.Broker.UserCredits,
		},
	}
	if err = rmq.Init(cfg.Broker.ExchangePrefix, cfg.Broker.ExchangePostfix); err != nil {
		logger.Fatalf("failed to connect to RabbitMQ: %v", err)
	}
	defer rmq.CloseRabbit()

	// Create a subscriber
	sub := rabbitmq.Subscriber{
		Rabbit:  rmq,
		Encoder: &rabbit.JsonEncoder{},
		Handler: &hdlr,
	}
	if err = sub.Init(); err != nil {
		logger.Fatalf("failed to start the subscriber: %v", err)
	}
	defer sub.Wait(shutdownTimeout)

	logger.Info("Service started")

	// Wait for program exit.
	<-os.NotifyAboutExit()
}

// Trigger CI/CD
