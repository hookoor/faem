package models

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestPhoneToInternationalCode(t *testing.T) {
	phones := map[string]string{
		"89824017432": "+79824017432",
		"":            "+7",
		"1":           "+71",
		"8":           "+7",
		"88":          "+78",
		"88004017432": "+78004017432",
	}
	for p := range phones {
		phone := ToRussianPhoneCode(p)
		assert.Equal(t, phones[p], phone)
	}
}

func TestFormatUnrestrictedValue(t *testing.T) {
	type addr struct {
		short    string
		full     string
		expected string
	}
	addresses := []addr{
		{
			short:    "",
			full:     "full address",
			expected: "full address",
		},
		{
			short:    "short address",
			full:     "full address",
			expected: "short address, full address",
		},
		{
			short:    "short address",
			full:     "",
			expected: "short address",
		},
	}

	for _, a := range addresses {
		formatted := FormatUnrestrictedValue(a.short, a.full)
		assert.Equal(t, a.expected, formatted)
	}
}
