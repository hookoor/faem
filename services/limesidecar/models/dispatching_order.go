package models

const (
	StateFindingDriver  = "finding_driver"
	StateOffering       = "offer_offered"
	StateAccepted       = "offer_accepted"
	StateFinished       = "finished"
	StateDriverNotFound = "driver_not_found"
	StateCancelled      = "offer_cancelled"
)

type DispatchingOrder struct {
	tableName struct{} `sql:"main_oper"`

	Idx        int64
	State      string
	Completeid int64
}
