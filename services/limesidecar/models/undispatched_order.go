package models

import (
	"fmt"
	"strings"
	"time"

	"gitlab.com/faemproject/backend/faem/services/limesidecar/proto"
)

const (
	MainOpenMetadataTable = "main_oper_workinfo"

	LimeStateUndispatched = "FreeSearch"
	LimeStateCancelled    = "EndCancel"
	LimeStateOffering     = "Предложен в FAEM"
	LimeStateAccepted     = "Принят в FAEM"
	LimeStateFinished     = "Завершен в FAEM"

	LimeCompleteIdSuccess = 1
)

type ServiceMapper func(int64) (string, bool)

type Route struct {
	Fulladdress  string
	Shortaddress string
	Latitude     float32
	Longitude    float32
}

func (r Route) ToProto() proto.Route {
	return proto.Route{
		UnrestrictedValue: FormatUnrestrictedValue(r.Shortaddress, r.Fulladdress),
		Value:             r.Shortaddress,
		Lat:               r.Latitude,
		Lon:               r.Longitude,
	}
}

type UndispatchedOrder struct {
	tableName struct{} `sql:"main_oper,alias:undispatched"`

	Idx          int64
	Aclientphone string
	Comment      string
	Adrarr       []Route
	Tariffid     int64
	Completetime time.Time
	//FeaturesUUIDS []string // TODO: map
}

type UndispatchedOrderWithDeadline struct {
	tableName struct{} `sql:"main_oper,alias:undispatched"`

	UndispatchedOrder

	Createtime time.Time
	DeadlineAt time.Time
}

func (u UndispatchedOrder) IsEmpty() bool {
	return u.Idx == 0
}

func (u UndispatchedOrder) ToProto(mapService ServiceMapper) proto.UndispatchedOrder {
	routes := make([]proto.Route, len(u.Adrarr))
	for i, r := range u.Adrarr {
		routes[i] = r.ToProto()
	}

	serviceID, _ := mapService(u.Tariffid)
	return proto.UndispatchedOrder{
		ID:            u.Idx,
		CallbackPhone: ToRussianPhoneCode(u.Aclientphone),
		Comment:       u.Comment,
		Routes:        routes,
		ServiceUUID:   serviceID,
		//FeaturesUUIDS: u.FeaturesUUIDS, // TODO: transform
	}
}

// ToRussianPhoneCode changes phone like "8900..." to "+79000"
func ToRussianPhoneCode(phone string) string {
	phone = strings.TrimSpace(phone)
	if strings.HasPrefix(phone, "+") { // already starts with an international code
		return phone
	}
	if strings.HasPrefix(phone, "8") { // if starts with "8" replace it for +7
		return strings.Replace(phone, "8", "+7", 1)
	}
	// Else just prepend the +7
	return "+7" + phone
}

func FormatUnrestrictedValue(shortAddress, fullAddress string) string {
	if shortAddress == "" {
		return fullAddress
	}
	if fullAddress == "" {
		return shortAddress
	}
	return fmt.Sprintf("%s, %s", shortAddress, fullAddress)
}
