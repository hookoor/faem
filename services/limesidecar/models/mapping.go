package models

import "time"

const (
	MappingStateNew                 = "new"
	MappingStateWatching            = "watching"
	MappingStateSearching           = "searching"
	MappingStateRejected            = "rejected" // end state
	MappingStateProcessing          = "processing"
	MappingStateFinished            = "finished"             // end state
	MappingStateCancelled           = "cancelled"            // end state
	MappingStateExternallyCancelled = "cancelled_externally" // end state
)

func EndStates() []string {
	return []string{
		MappingStateRejected,
		MappingStateFinished,
		MappingStateCancelled,
		MappingStateExternallyCancelled,
	}
}

type Mapping struct {
	ID           string
	LimeObjectID int64
	FaemObjectID string
	ObjectType   string
	State        string
	DeadlineAt   time.Time
	CreatedAt    time.Time
	UpdatedAt    time.Time
}

func (m Mapping) IsAlreadyTransferred() bool {
	return m.State != MappingStateNew
}

func (m Mapping) IsEnded() bool {
	for _, es := range EndStates() {
		if es == m.State {
			return true
		}
	}
	return false
}
