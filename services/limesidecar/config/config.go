package config

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/spf13/viper"
	"go.uber.org/multierr"
)

const (
	envPrefix = "limesidecar"
)

type Application struct {
	Env       string
	LogLevel  string
	LogFormat string
}

func (a *Application) IsProduction() bool {
	return a.Env == "production"
}

type Database struct {
	Host     string
	User     string
	Password string
	Port     int
	Db       string
}

func (d *Database) validate() error {
	if d.Host == "" {
		return errors.New("empty db host provided")
	}
	if d.Port == 0 {
		return errors.New("empty db port provided")
	}
	if d.User == "" {
		return errors.New("empty db user provided")
	}
	if d.Password == "" {
		return errors.New("empty db password provided")
	}
	if d.Db == "" {
		return errors.New("empty db name provided")
	}
	return nil
}

type External struct {
	Host                string
	User                string
	Password            string
	Port                int
	Db                  string
	CreateChannel       string
	CancelChannel       string
	DistributionTimeout time.Duration
	DataTransferTimeout time.Duration
}

func (e *External) validate() error {
	if e.Host == "" {
		return errors.New("empty external host provided")
	}
	if e.Port == 0 {
		return errors.New("empty external port provided")
	}
	if e.User == "" {
		return errors.New("empty external user provided")
	}
	if e.Password == "" {
		return errors.New("empty external password provided")
	}
	if e.Db == "" {
		return errors.New("empty external name provided")
	}
	if e.CreateChannel == "" {
		return errors.New("empty external channel provided to monitor created orders")
	}
	if e.CancelChannel == "" {
		return errors.New("empty external channel provided to monitor cancelled orders")
	}
	if e.DistributionTimeout == 0 {
		return errors.New("empty external distribution timeout provided")
	}
	if e.DataTransferTimeout == 0 {
		return errors.New("empty external data transfer timeout provided")
	}
	return nil
}

type Client struct {
	BaseURL        string
	CreateOrderURL string
	CancelOrderURL string
	Timeout        time.Duration
	JWT            string
}

func (c *Client) validate() error {
	if c.BaseURL == "" {
		return errors.New("empty base url provided for a client service")
	}
	if c.CreateOrderURL == "" {
		return errors.New("empty order creation url provided for a client service")
	}
	if c.CancelOrderURL == "" {
		return errors.New("empty order canceling url provided for a client service")
	}
	if c.JWT == "" {
		return errors.New("empty jwt provided for a client service")
	}
	return nil
}

type Broker struct {
	UserURL         string
	UserCredits     string
	ExchangePrefix  string
	ExchangePostfix string
}

func (b *Broker) validate() error {
	if b.UserURL == "" {
		return errors.New("empty broker url provided")
	}
	if b.UserCredits == "" {
		return errors.New("empty broker credentials provided")
	}
	return nil
}

type ServiceMapping struct {
	LimeID int64
	FaemID string
}

func (s *ServiceMapping) validate() error {
	if s.LimeID == 0 {
		return errors.New("empty lime id for service mapping")
	}
	if s.FaemID == "" {
		return errors.New("empty faem id for service mapping")
	}
	return nil
}

type Mappings struct {
	ServiceMapping []ServiceMapping
}

func (m *Mappings) validate() error {
	if len(m.ServiceMapping) < 1 {
		return errors.New("you must provide at least one service mapping")
	}

	for _, sm := range m.ServiceMapping {
		if err := sm.validate(); err != nil {
			return err
		}
	}
	return nil
}

type Config struct {
	Application Application
	Database    Database
	External    External
	Client      Client
	Broker      Broker
	Mappings    Mappings
}

func (c *Config) validate() error {
	return multierr.Combine(
		c.Database.validate(),
		c.External.validate(),
		c.Client.validate(),
		c.Broker.validate(),
		c.Mappings.validate(),
	)
}

// Parse will parse the configuration from the environment variables and a file with the specified path.
// Environment variables have more priority than ones specified in the file.
func Parse(filepath string) (*Config, error) {
	setDefaults()

	// Parse the file
	viper.SetConfigFile(filepath)
	if err := viper.ReadInConfig(); err != nil {
		return nil, errors.Wrap(err, "failed to read the config file")
	}

	bindEnvVars() // remember to parse the environment variables

	// Unmarshal the config
	var cfg Config
	if err := viper.Unmarshal(&cfg); err != nil {
		return nil, errors.Wrap(err, "failed to unmarshal the configuration")
	}

	// Validate the provided configuration
	if err := cfg.validate(); err != nil {
		return nil, errors.Wrap(err, "failed to validate the config")
	}
	return &cfg, nil
}

func (c *Config) Print() {
	if c.Application.IsProduction() {
		return
	}
	inspected := *c // get a copy of an actual object
	// Hide sensitive data
	inspected.Database.User = ""
	inspected.Database.Password = ""
	inspected.External.User = ""
	inspected.External.Password = ""
	inspected.Broker.UserCredits = ""
	inspected.Client.JWT = ""
	fmt.Printf("%+v\n", inspected)
}

func setDefaults() {
	viper.SetDefault("application.env", "production")
	viper.SetDefault("application.loglevel", "debug")
	viper.SetDefault("application.logformat", "text")

	viper.SetDefault("database.host", "")
	viper.SetDefault("database.port", 0)
	viper.SetDefault("database.user", "")
	viper.SetDefault("database.password", "")
	viper.SetDefault("database.db", "")

	viper.SetDefault("external.host", "")
	viper.SetDefault("external.port", 0)
	viper.SetDefault("external.user", "")
	viper.SetDefault("external.password", "")
	viper.SetDefault("external.db", "")
	viper.SetDefault("external.createchannel", "")
	viper.SetDefault("external.cancelchannel", "")
	viper.SetDefault("external.distributiontimeout", 0)
	viper.SetDefault("external.datatransfertimeout", 0)

	viper.SetDefault("broker.userurl", "")
	viper.SetDefault("broker.usercredits", "")
	viper.SetDefault("broker.exchangeprefix", "")
	viper.SetDefault("broker.exchangepostfix", "")

	viper.SetDefault("client.baseurl", "")
	viper.SetDefault("client.createorderurl", "")
	viper.SetDefault("client.cancelorderurl", "")
	viper.SetDefault("client.timeout", 30*time.Second)
	viper.SetDefault("client.jwt", "")

	viper.SetDefault("mappings.servicemapping", []ServiceMapping{
		{
			LimeID: 0,
			FaemID: "",
		},
	})
}

func bindEnvVars() {
	viper.SetEnvPrefix(envPrefix)
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}
