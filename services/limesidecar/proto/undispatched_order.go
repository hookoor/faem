package proto

type Route struct {
	UnrestrictedValue string  `json:"unrestricted_value"`
	Value             string  `json:"value"`
	Lat               float32 `json:"lat"`
	Lon               float32 `json:"lon"`
}

type UndispatchedOrder struct {
	ID            int64    `json:"-"`
	CallbackPhone string   `json:"callback_phone"`
	Comment       string   `json:"comment"`
	ServiceUUID   string   `json:"service_uuid"`
	Routes        []Route  `json:"routes"`
	FeaturesUUIDS []string `json:"features_uuids"`
}
