package proto

type OrderState struct {
	OrderUUID string `json:"order_uuid"`
	State     string `json:"state"`
}
