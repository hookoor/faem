package inner

import (
	"context"
	"time"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
)

type Pg struct {
	Db *pg.DB
}

func (p *Pg) CreateUndispatchedOrder(_ context.Context, undispatched models.UndispatchedOrderWithDeadline) error {
	mapping := models.Mapping{
		ID:           uuid.Must(uuid.NewV4()).String(),
		LimeObjectID: undispatched.Idx,
		State:        models.MappingStateNew,
		DeadlineAt:   undispatched.DeadlineAt,
		CreatedAt:    undispatched.Createtime,
	}
	err := p.Db.Insert(&mapping)
	return errors.Wrap(err, "failed to insert a record")
}

func (p *Pg) GetWaitingOrders(ctx context.Context, distributionTimeout time.Duration) ([]models.Mapping, error) {
	var mappings []models.Mapping
	if err := p.Db.ModelContext(ctx, &mappings).
		Where("state = ?", models.MappingStateNew).
		Where("created_at <= ?", time.Now().Add(-distributionTimeout)).
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}
	return mappings, nil
}

func (p *Pg) GetOutdatedSearchingMappings(ctx context.Context, dataTransferTimeout time.Duration) ([]models.Mapping, error) {
	var mappings []models.Mapping
	if err := p.Db.ModelContext(ctx, &mappings).
		Where("state = ?", models.MappingStateSearching).
		Where("deadline_at <= ?", time.Now().Add(dataTransferTimeout)).
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}
	return mappings, nil
}

func (p *Pg) MapUndispatchedOrderToCreated(ctx context.Context, mappingID, createdID string) error {
	_, err := p.Db.ModelContext(ctx, (*models.Mapping)(nil)).
		Set("faem_object_id = ?", createdID).
		Set("state = ?", models.MappingStateWatching).
		Where("id = ?", mappingID).
		Update()
	return errors.Wrap(err, "failed to update a record")
}

func (p *Pg) GetMappedOrder(ctx context.Context, orderID string) (models.Mapping, error) {
	var mapping models.Mapping
	if err := p.Db.ModelContext(ctx, &mapping).
		Where("faem_object_id = ?", orderID).
		Select(); err != nil {
		return mapping, errors.Wrap(err, "failed to select a record")
	}
	return mapping, nil
}

func (p *Pg) FilterKnownUndispatchedOrders(ctx context.Context, undispatched []models.UndispatchedOrderWithDeadline) (
	[]models.UndispatchedOrderWithDeadline, error,
) {
	if len(undispatched) < 1 {
		return nil, errors.New("no undispatched orders provided")
	}

	limeIds := make([]int64, len(undispatched))
	for i, u := range undispatched {
		limeIds[i] = u.Idx
	}

	var mappings []models.Mapping
	if err := p.Db.ModelContext(ctx, &mappings).
		WhereIn("lime_object_id IN (?)", limeIds).
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}

	// Filter already known orders
	knownIds := make(map[int64]struct{})
	for _, m := range mappings {
		knownIds[m.LimeObjectID] = struct{}{}
	}

	var filtered []models.UndispatchedOrderWithDeadline
	for _, u := range undispatched {
		if _, ok := knownIds[u.Idx]; !ok {
			filtered = append(filtered, u)
		}
	}
	return filtered, nil
}

func (p *Pg) FilterUnknownAndEndedOrders(ctx context.Context, cancelled []models.UndispatchedOrder) (
	[]models.Mapping, error,
) {
	if len(cancelled) < 1 {
		return nil, errors.New("no cancelled orders provided")
	}

	limeIds := make([]int64, len(cancelled))
	for i, c := range cancelled {
		limeIds[i] = c.Idx
	}

	var mappings []models.Mapping
	if err := p.Db.ModelContext(ctx, &mappings).
		WhereIn("lime_object_id IN (?)", limeIds).
		WhereIn("state NOT IN (?)", models.EndStates()).
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}
	return mappings, nil
}

func (p *Pg) SetRejectedState(ctx context.Context, mappingID string) error {
	return p.setStateForMapping(ctx, mappingID, models.MappingStateRejected)
}

func (p *Pg) SetSearchingState(ctx context.Context, orderID string) error {
	return p.setState(ctx, orderID, models.MappingStateSearching)
}

func (p *Pg) SetProcessingState(ctx context.Context, orderID string) error {
	return p.setState(ctx, orderID, models.MappingStateProcessing)
}

func (p *Pg) SetFinishedState(ctx context.Context, orderID string) error {
	return p.setState(ctx, orderID, models.MappingStateFinished)
}

func (p *Pg) SetCancelledState(ctx context.Context, orderID string) error {
	return p.setState(ctx, orderID, models.MappingStateCancelled)
}

func (p *Pg) SetExternallyCancelledState(ctx context.Context, orderID string) error {
	return p.setState(ctx, orderID, models.MappingStateExternallyCancelled)
}

func (p *Pg) SetExternallyCancelledStateForMapping(ctx context.Context, mappingID string) error {
	return p.setStateForMapping(ctx, mappingID, models.MappingStateExternallyCancelled)
}

func (p *Pg) setState(ctx context.Context, orderID, state string) error {
	_, err := p.Db.ModelContext(ctx, (*models.Mapping)(nil)).
		Set("state = ?", state).
		Where("faem_object_id = ?", orderID).
		WhereIn("state NOT IN (?)", models.EndStates()).
		Update()
	return errors.Wrap(err, "failed to update a record")
}

func (p *Pg) setStateForMapping(ctx context.Context, mappingID, state string) error {
	_, err := p.Db.ModelContext(ctx, (*models.Mapping)(nil)).
		Set("state = ?", state).
		Where("id = ?", mappingID).
		WhereIn("state NOT IN (?)", models.EndStates()).
		Update()
	return errors.Wrap(err, "failed to update a record")
}
