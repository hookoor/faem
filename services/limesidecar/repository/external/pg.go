package external

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
)

const (
	skipCancelledOrdersTimeout = 1 * time.Hour
)

type Pg struct {
	Db *pg.DB
}

func (p *Pg) GetUndispatchedOrders(ctx context.Context) ([]models.UndispatchedOrderWithDeadline, error) {
	var undispatched []models.UndispatchedOrderWithDeadline
	if err := p.Db.ModelContext(ctx, &undispatched).
		Column("undispatched.idx", "undispatched.createtime").
		ColumnExpr("meta.freesearchdeadline AS deadline_at").
		Join(fmt.Sprintf("LEFT JOIN %s AS meta ON meta.mainid = undispatched.idx", models.MainOpenMetadataTable)).
		Where("state = ?", models.LimeStateUndispatched).
		Where("meta.freesearchdeadline > NOW()").
		Order("idx ASC").
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}
	return undispatched, nil
}

func (p *Pg) GetCancelledOrders(ctx context.Context) ([]models.UndispatchedOrder, error) {
	var cancelled []models.UndispatchedOrder
	if err := p.Db.ModelContext(ctx, &cancelled).
		Column("undispatched.idx").
		Where("state = ?", models.LimeStateCancelled).
		Where("completetime > ?", time.Now().Add(-skipCancelledOrdersTimeout)).
		Order("idx DESC").
		Select(); err != nil {
		return nil, errors.Wrap(err, "failed to select records")
	}
	return cancelled, nil
}

func (p *Pg) GetUndispatchedOrder(ctx context.Context, orderID int64) (models.UndispatchedOrder, error) {
	var undispatched models.UndispatchedOrder
	err := p.Db.ModelContext(ctx, &undispatched).
		Where("idx = ?", orderID).
		Where("state = ?", models.LimeStateUndispatched).
		Limit(1).
		Select()
	if err == pg.ErrNoRows {
		return undispatched, nil
	}
	return undispatched, errors.Wrap(err, "failed to get a record")
}

func (p *Pg) SetOfferingState(ctx context.Context, orderID int64) error {
	res, err := p.Db.ModelContext(ctx, (*models.DispatchingOrder)(nil)).
		Set("state = ?", models.LimeStateOffering).
		Where("idx = ?", orderID).
		WhereInMulti("state IN (?)", models.LimeStateUndispatched, models.LimeStateOffering).
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update a record with id %d", orderID)
	}
	if res.RowsAffected() < 1 {
		return errors.Errorf("order %d is not in a free state anymore", orderID)
	}
	return nil
}

func (p *Pg) SetAcceptedState(ctx context.Context, orderID int64) error {
	return p.setState(ctx, orderID, models.LimeStateAccepted)
}

func (p *Pg) SetFinishedState(ctx context.Context, orderID int64) error {
	_, err := p.Db.ModelContext(ctx, (*models.DispatchingOrder)(nil)).
		Set("state = ?", models.LimeStateFinished).
		Set("completeid = ?", models.LimeCompleteIdSuccess).
		Where("idx = ?", orderID).
		Update()
	return errors.Wrap(err, "failed to update a record")
}

func (p *Pg) ReleaseState(ctx context.Context, orderID int64) error {
	return p.setState(ctx, orderID, models.LimeStateUndispatched)
}

func (p *Pg) setState(ctx context.Context, orderID int64, state string) error {
	_, err := p.Db.ModelContext(ctx, (*models.DispatchingOrder)(nil)).
		Set("state = ?", state).
		Where("idx = ?", orderID).
		Update()
	return errors.Wrap(err, "failed to update a record")
}
