package handler

import (
	"context"
	"time"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"go.uber.org/multierr"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/proto"
)

const (
	monitorUndispatchedOrdersInterval = 30 * time.Second
	monitorSearchingOrdersInterval    = 25 * time.Second
	maxNewOrdersAllowed               = 10
)

type RemoteClient interface {
	CreateOrder(context.Context, proto.UndispatchedOrder) (proto.CreatedOrder, error)
	CancelOrder(ctx context.Context, orderID string) error
}

type OrderRepository interface {
	CreateUndispatchedOrder(ctx context.Context, undispatched models.UndispatchedOrderWithDeadline) error
	GetWaitingOrders(ctx context.Context, distributionTimeout time.Duration) ([]models.Mapping, error)
	GetOutdatedSearchingMappings(ctx context.Context, dataTransferTimeout time.Duration) ([]models.Mapping, error)
	MapUndispatchedOrderToCreated(ctx context.Context, mappingID, createdID string) error
	GetMappedOrder(ctx context.Context, orderID string) (models.Mapping, error)
	SetRejectedState(ctx context.Context, mappingID string) error
	SetSearchingState(ctx context.Context, orderID string) error
	SetProcessingState(ctx context.Context, orderID string) error
	SetFinishedState(ctx context.Context, orderID string) error
	SetCancelledState(ctx context.Context, orderID string) error
	SetExternallyCancelledState(ctx context.Context, orderID string) error
	SetExternallyCancelledStateForMapping(ctx context.Context, mappingID string) error
}

type ExternalOrderRepository interface {
	GetUndispatchedOrder(ctx context.Context, orderID int64) (models.UndispatchedOrder, error)
	SetOfferingState(ctx context.Context, orderID int64) error
	SetAcceptedState(ctx context.Context, orderID int64) error
	SetFinishedState(ctx context.Context, orderID int64) error
	ReleaseState(ctx context.Context, orderID int64) error
}

func (h *Handler) RememberUndispatchedOrder(ctx context.Context, undispatched models.UndispatchedOrderWithDeadline) error {
	err := h.DB.CreateUndispatchedOrder(ctx, undispatched)
	return errors.Wrap(err, "failed to create an undispatched order")
}

func (h *Handler) DispatchOrder(ctx context.Context, mappingID string, order proto.UndispatchedOrder) error {
	created, err := h.Client.CreateOrder(ctx, order)
	if err != nil {
		return errors.Wrap(err, "failed to create an order in client repo")
	}
	err = h.DB.MapUndispatchedOrderToCreated(ctx, mappingID, created.UUID)
	return errors.Wrap(err, "failed to map an undispatched order to a created one")
}

func (h *Handler) ExternallyCancelMapping(ctx context.Context, mapping models.Mapping) error {
	if !mapping.IsAlreadyTransferred() {
		return h.SetExternallyCancelledStateForMapping(ctx, mapping.ID)
	}
	return h.ExternallyCancelOrder(ctx, mapping.FaemObjectID)
}

func (h *Handler) ChangeOrderState(ctx context.Context, state proto.OrderState) error {
	switch state.State {
	case models.StateFindingDriver:
		return h.SetFindingDriverState(ctx, state.OrderUUID)
	case models.StateOffering:
		return h.SetOfferingState(ctx, state.OrderUUID)
	case models.StateAccepted:
		return h.SetAcceptedState(ctx, state.OrderUUID)
	case models.StateFinished:
		return h.SetFinishedState(ctx, state.OrderUUID)
	case models.StateDriverNotFound, models.StateCancelled:
		return h.ReleaseOrder(ctx, state.OrderUUID)
	default:
		return nil
	}
}

func (h *Handler) SetFindingDriverState(ctx context.Context, orderID string) error {
	mapping, err := h.DB.GetMappedOrder(ctx, orderID)
	if err != nil {
		return errors.Wrap(err, "failed to get a mapping from the db")
	}

	err = h.DB.SetSearchingState(ctx, orderID)
	err = errors.Wrap(err, "failed to set finding driver state in the external db")

	rerr := h.ExternalDB.ReleaseState(ctx, mapping.LimeObjectID)
	rerr = errors.Wrapf(rerr, "failed to release an order in lime with id: %d", mapping.LimeObjectID)

	return multierr.Append(err, rerr)
}

func (h *Handler) SetOfferingState(ctx context.Context, orderID string) error {
	mapping, err := h.DB.GetMappedOrder(ctx, orderID)
	if err != nil {
		return errors.Wrap(err, "failed to get a mapping from the db")
	}

	if err = h.ExternalDB.SetOfferingState(ctx, mapping.LimeObjectID); err != nil {
		err = errors.Wrap(err, "failed to set offering state in the external db")

		logs.Eloger.WithField("orderID", orderID).Info("cancelling order")
		cerr := h.CancelOrder(ctx, orderID)
		return multierr.Append(err, cerr)
	}
	err = h.DB.SetProcessingState(ctx, orderID)
	return errors.Wrapf(err, "failed to set processing state in the db")
}

func (h *Handler) SetAcceptedState(ctx context.Context, orderID string) error {
	mapping, err := h.DB.GetMappedOrder(ctx, orderID)
	if err != nil {
		return errors.Wrap(err, "failed to get a mapping from the db")
	}
	err = h.ExternalDB.SetAcceptedState(ctx, mapping.LimeObjectID)
	return errors.Wrap(err, "failed to set accepted state in the external db")
}

func (h *Handler) SetFinishedState(ctx context.Context, orderID string) error {
	mapping, err := h.DB.GetMappedOrder(ctx, orderID)
	if err != nil {
		return errors.Wrap(err, "failed to get a mapping from the db")
	}

	err = h.ExternalDB.SetFinishedState(ctx, mapping.LimeObjectID)
	err = errors.Wrap(err, "failed to set finished state in the external db")

	ierr := h.DB.SetFinishedState(ctx, orderID)
	ierr = errors.Wrap(ierr, "failed to set finished state in the db")
	return multierr.Append(err, ierr)
}

func (h *Handler) CancelOrder(ctx context.Context, orderID string) error {
	err := h.Client.CancelOrder(ctx, orderID)
	err = errors.Wrapf(err, "failed to cancel an order with id: %s", orderID)

	ierr := h.DB.SetCancelledState(ctx, orderID)
	ierr = errors.Wrap(ierr, "failed to set cancelled state in the db")
	return multierr.Append(err, ierr)
}

func (h *Handler) ReleaseOrder(ctx context.Context, orderID string) error {
	mapping, err := h.DB.GetMappedOrder(ctx, orderID)
	if err != nil {
		return errors.Wrap(err, "failed to get a mapping from the db")
	}

	if mapping.IsEnded() {
		return nil
	}

	err = h.ExternalDB.ReleaseState(ctx, mapping.LimeObjectID)
	err = errors.Wrapf(err, "failed to release an order in lime with id: %d", mapping.LimeObjectID)

	ierr := h.DB.SetCancelledState(ctx, orderID)
	ierr = errors.Wrap(ierr, "failed to set cancelled state in the db")
	return multierr.Append(err, ierr)
}

func (h *Handler) ExternallyCancelOrder(ctx context.Context, orderID string) error {
	err := h.Client.CancelOrder(ctx, orderID)
	err = errors.Wrapf(err, "failed to cancel an order with id: %s", orderID)

	ierr := h.DB.SetExternallyCancelledState(ctx, orderID)
	ierr = errors.Wrap(ierr, "failed to set externally cancelled state for the order in the db")
	return multierr.Append(err, ierr)
}

func (h *Handler) SetExternallyCancelledStateForMapping(ctx context.Context, mappingID string) error {
	err := h.DB.SetExternallyCancelledStateForMapping(ctx, mappingID)
	return errors.Wrapf(err, "failed to set externally cancelled state for the mapping in the db")
}

func (h *Handler) monitorUndispatchedOrders() {
	h.wg.Add(1)
	defer h.wg.Done()

	for {
		select {
		case <-h.closed:
			return
		case <-time.After(monitorUndispatchedOrdersInterval): // ensure subroutine ends before starting a new timer
			h.tryDispatchOrders(context.Background())
		}
	}
}

func (h *Handler) tryDispatchOrders(ctx context.Context) {
	logs.Eloger.Info("trying to dispatch new orders...")

	newMappings, err := h.DB.GetWaitingOrders(ctx, h.Config.DistributionTimeout)
	if err != nil {
		logs.Eloger.Errorf("failed to get waiting orders: %v", err)
		return
	}

	limit := limiter.NewConcurrencyLimiter(maxNewOrdersAllowed)
	defer limit.Wait()

	// Handle all the orders concurrently
	for _, m := range newMappings {
		mapping := m // super important to copy the value to use in the closure below
		limit.Execute(lang.Recover(
			func() {
				// Check if an order is still undispatched
				undispatched, err := h.ExternalDB.GetUndispatchedOrder(ctx, mapping.LimeObjectID)
				if err != nil {
					logs.Eloger.Errorf(
						"failed to check if a lime order with id %d is still undispatched: %v", mapping.LimeObjectID, err,
					)
					return
				}
				// Reject the order if it's already dispatched in Lime
				if undispatched.IsEmpty() {
					if err := h.DB.SetRejectedState(ctx, mapping.ID); err != nil {
						logs.Eloger.Errorf("failed to set rejected state for mapping id %s: %v", mapping.ID, err)
						return
					}
					logs.Eloger.Infof("lime order %d is already dispatched", mapping.LimeObjectID)
					return
				}

				// Dispatch the order in FAEM
				if err := h.DispatchOrder(ctx, mapping.ID, undispatched.ToProto(h.ServiceMapper.Map)); err != nil {
					logs.Eloger.Errorf("failed to dispatch an order with lime id %d: %v", mapping.LimeObjectID, err)
					return
				}
				logs.Eloger.WithField("limeOrderID", mapping.LimeObjectID).Info("transferred to faem successfully")
			},
		))
	}
}

func (h *Handler) monitorSearchingOrders() {
	h.wg.Add(1)
	defer h.wg.Done()

	for {
		select {
		case <-h.closed:
			return
		case <-time.After(monitorSearchingOrdersInterval): // ensure subroutine ends before starting a new timer
			h.releaseOrdersIfNeeded(context.Background())
		}
	}
}

func (h *Handler) releaseOrdersIfNeeded(ctx context.Context) {
	logs.Eloger.Info("checking if need to release outdated orders...")

	searchingMappings, err := h.DB.GetOutdatedSearchingMappings(ctx, h.Config.DataTransferTimeout)
	if err != nil {
		logs.Eloger.Errorf("failed to get outdated searching orders: %v", err)
		return
	}

	limit := limiter.NewConcurrencyLimiter(maxNewOrdersAllowed)
	defer limit.Wait()

	// Handle all the orders concurrently
	for _, m := range searchingMappings {
		mapping := m // super important to copy the value to use in the closure below
		limit.Execute(lang.Recover(
			func() {
				// Cancel and release the order
				if err := h.CancelOrder(ctx, mapping.FaemObjectID); err != nil {
					logs.Eloger.Errorf("failed to cancel and release an order with mapping id %s: %v", mapping.ID, err)
					return
				}
				logs.Eloger.WithField("mappingID", mapping.ID).Info("cancelled and released successfully")
			},
		))
	}
}
