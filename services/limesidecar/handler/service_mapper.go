package handler

import "gitlab.com/faemproject/backend/faem/services/limesidecar/config"

type ServiceMapper struct {
	Mapping map[int64]string
}

func NewServiceMapper(mappings []config.ServiceMapping) *ServiceMapper {
	mapping := make(map[int64]string)
	for _, m := range mappings {
		mapping[m.LimeID] = m.FaemID
	}
	return &ServiceMapper{Mapping: mapping}
}

func (s *ServiceMapper) Map(limeID int64) (string, bool) {
	res, ok := s.Mapping[limeID]
	return res, ok
}
