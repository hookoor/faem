package handler

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"

	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/proto"
)

type mockRemoteClient struct {
	mock.Mock
}

func (m *mockRemoteClient) CreateOrder(ctx context.Context, order proto.UndispatchedOrder) (proto.CreatedOrder, error) {
	args := m.Called(ctx, order)
	return args.Get(0).(proto.CreatedOrder), args.Error(1)
}

func (m *mockRemoteClient) CancelOrder(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

type mockDB struct {
	mock.Mock
}

func (m *mockDB) CreateUndispatchedOrder(ctx context.Context, undispatched models.UndispatchedOrderWithDeadline) error {
	args := m.Called(ctx, undispatched)
	return args.Error(0)
}

func (m *mockDB) GetWaitingOrders(ctx context.Context, distributionTimeout time.Duration) ([]models.Mapping, error) {
	args := m.Called(ctx, distributionTimeout)
	return args.Get(0).([]models.Mapping), args.Error(1)
}

func (m *mockDB) GetOutdatedSearchingMappings(ctx context.Context, dataTransferTimeout time.Duration) ([]models.Mapping, error) {
	args := m.Called(ctx, dataTransferTimeout)
	return args.Get(0).([]models.Mapping), args.Error(1)
}

func (m *mockDB) MapUndispatchedOrderToCreated(ctx context.Context, mappingID, createdID string) error {
	args := m.Called(ctx, mappingID, createdID)
	return args.Error(0)
}

func (m *mockDB) GetMappedOrder(ctx context.Context, orderID string) (models.Mapping, error) {
	args := m.Called(ctx, orderID)
	return args.Get(0).(models.Mapping), args.Error(1)
}

func (m *mockDB) SetRejectedState(ctx context.Context, mappingID string) error {
	args := m.Called(ctx, mappingID)
	return args.Error(0)
}

func (m *mockDB) SetSearchingState(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockDB) SetProcessingState(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockDB) SetFinishedState(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockDB) SetCancelledState(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockDB) SetExternallyCancelledState(ctx context.Context, orderID string) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockDB) SetExternallyCancelledStateForMapping(ctx context.Context, mappingID string) error {
	args := m.Called(ctx, mappingID)
	return args.Error(0)
}

type mockExternalDB struct {
	mock.Mock
}

func (m *mockExternalDB) GetUndispatchedOrder(ctx context.Context, orderID int64) (models.UndispatchedOrder, error) {
	args := m.Called(ctx, orderID)
	return args.Get(0).(models.UndispatchedOrder), args.Error(1)
}

func (m *mockExternalDB) SetOfferingState(ctx context.Context, orderID int64) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockExternalDB) SetAcceptedState(ctx context.Context, orderID int64) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockExternalDB) SetFinishedState(ctx context.Context, orderID int64) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func (m *mockExternalDB) ReleaseState(ctx context.Context, orderID int64) error {
	args := m.Called(ctx, orderID)
	return args.Error(0)
}

func TestHandler_RememberUndispatchedOrder(t *testing.T) {
	undispatched := models.UndispatchedOrderWithDeadline{
		UndispatchedOrder: models.UndispatchedOrder{Idx: 1},
		DeadlineAt:        time.Now().Add(time.Minute),
	}
	var db mockDB
	hdlr := Handler{DB: &db}

	db.On("CreateUndispatchedOrder", mock.Anything, undispatched).Return(errors.New("testing")).Once()
	err := hdlr.RememberUndispatchedOrder(context.Background(), undispatched)
	assert.Error(t, err)
	db.AssertExpectations(t)

	db.On("CreateUndispatchedOrder", mock.Anything, undispatched).Return(nil)
	err = hdlr.RememberUndispatchedOrder(context.Background(), undispatched)
	assert.NoError(t, err)
	db.AssertExpectations(t)
}

func TestHandler_DispatchOrder(t *testing.T) {
	undispatched := proto.UndispatchedOrder{ID: 1}
	mappingID := "123asd"
	created := proto.CreatedOrder{UUID: "123asd"}
	var (
		client mockRemoteClient
		db     mockDB
	)
	hdlr := Handler{
		Client: &client,
		DB:     &db,
	}

	client.On("CreateOrder", mock.Anything, mock.Anything).Return(created, errors.New("testing")).Once()
	err := hdlr.DispatchOrder(context.Background(), mappingID, undispatched)
	assert.Error(t, err)
	client.AssertExpectations(t)
	db.AssertNotCalled(t, "MapUndispatchedOrderToCreated")

	client.On("CreateOrder", mock.Anything, undispatched).Return(created, nil)
	db.On("MapUndispatchedOrderToCreated", mock.Anything, mappingID, created.UUID).Return(errors.New("testing")).Once()
	err = hdlr.DispatchOrder(context.Background(), mappingID, undispatched)
	assert.Error(t, err)
	client.AssertExpectations(t)
	db.AssertExpectations(t)

	db.On("MapUndispatchedOrderToCreated", mock.Anything, mappingID, created.UUID).Return(nil)
	err = hdlr.DispatchOrder(context.Background(), mappingID, undispatched)
	assert.NoError(t, err)
	client.AssertExpectations(t)
	db.AssertExpectations(t)
}

func TestHandler_SetFindingDriverState(t *testing.T) {
	mapping := models.Mapping{
		LimeObjectID: 6,
		FaemObjectID: "123abc",
	}
	var (
		db         mockDB
		externalDb mockExternalDB
	)
	hdlr := Handler{
		DB:         &db,
		ExternalDB: &externalDb,
	}

	db.On("GetMappedOrder", mock.Anything, mock.Anything).Return(mapping, errors.New("testing")).Once()
	err := hdlr.SetFindingDriverState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	db.AssertNotCalled(t, "SetSearchingState")
	externalDb.AssertNotCalled(t, "SetOfferingState")

	db.On("GetMappedOrder", mock.Anything, mapping.FaemObjectID).Return(mapping, nil)
	db.On("SetSearchingState", mock.Anything, mapping.FaemObjectID).Return(errors.New("testing")).Once()
	externalDb.On("ReleaseState", mock.Anything, mapping.LimeObjectID).Return(nil)
	err = hdlr.SetFindingDriverState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)

	db.On("SetSearchingState", mock.Anything, mapping.FaemObjectID).Return(nil)
	err = hdlr.SetFindingDriverState(context.Background(), mapping.FaemObjectID)
	assert.NoError(t, err)
	db.AssertExpectations(t)
}

func TestHandler_SetOfferingState(t *testing.T) {
	mapping := models.Mapping{
		LimeObjectID: 2,
		FaemObjectID: "123abc",
	}
	var (
		client     mockRemoteClient
		db         mockDB
		externalDb mockExternalDB
	)
	hdlr := Handler{
		Client:     &client,
		DB:         &db,
		ExternalDB: &externalDb,
	}

	db.On("GetMappedOrder", mock.Anything, mock.Anything).Return(mapping, errors.New("testing")).Once()
	err := hdlr.SetOfferingState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertNotCalled(t, "SetOfferingState")
	client.AssertNotCalled(t, "CancelOrder")
	db.AssertNotCalled(t, "SetProcessingState")

	db.On("GetMappedOrder", mock.Anything, mapping.FaemObjectID).Return(mapping, nil)
	externalDb.On("SetOfferingState", mock.Anything, mapping.LimeObjectID).Return(nil).Once()
	db.On("SetProcessingState", mock.Anything, mapping.FaemObjectID).Return(nil)
	err = hdlr.SetOfferingState(context.Background(), mapping.FaemObjectID)
	assert.NoError(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)
	client.AssertNotCalled(t, "CancelOrder")

	externalDb.On("SetOfferingState", mock.Anything, mapping.LimeObjectID).Return(errors.New("testing"))
	client.On("CancelOrder", mock.Anything, mapping.FaemObjectID).Return(nil).Once()
	db.On("SetCancelledState", mock.Anything, mapping.FaemObjectID).Return(nil)
	err = hdlr.SetOfferingState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	externalDb.AssertExpectations(t)
	client.AssertExpectations(t)
	db.AssertNotCalled(t, "SetProcessingState")

	client.On("CancelOrder", mock.Anything, mapping.FaemObjectID).Return(errors.New("testing"))
	err = hdlr.SetOfferingState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)
	client.AssertExpectations(t)
	db.AssertNotCalled(t, "SetProcessingState")
}

func TestHandler_SetAcceptedState(t *testing.T) {
	mapping := models.Mapping{
		LimeObjectID: 3,
		FaemObjectID: "123abc",
	}
	var (
		db         mockDB
		externalDb mockExternalDB
	)
	hdlr := Handler{
		DB:         &db,
		ExternalDB: &externalDb,
	}

	db.On("GetMappedOrder", mock.Anything, mock.Anything).Return(mapping, errors.New("testing")).Once()
	err := hdlr.SetAcceptedState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertNotCalled(t, "SetAcceptedState")

	db.On("GetMappedOrder", mock.Anything, mapping.FaemObjectID).Return(mapping, nil)
	externalDb.On("SetAcceptedState", mock.Anything, mapping.LimeObjectID).Return(errors.New("testing")).Once()
	err = hdlr.SetAcceptedState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)

	externalDb.On("SetAcceptedState", mock.Anything, mapping.LimeObjectID).Return(nil)
	err = hdlr.SetAcceptedState(context.Background(), mapping.FaemObjectID)
	assert.NoError(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)
}

func TestHandler_SetFinishedState(t *testing.T) {
	mapping := models.Mapping{
		LimeObjectID: 4,
		FaemObjectID: "123abc",
	}
	var (
		db         mockDB
		externalDb mockExternalDB
	)
	hdlr := Handler{
		DB:         &db,
		ExternalDB: &externalDb,
	}

	db.On("GetMappedOrder", mock.Anything, mock.Anything).Return(mapping, errors.New("testing")).Once()
	err := hdlr.SetFinishedState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertNotCalled(t, "SetFinishedState")

	db.On("GetMappedOrder", mock.Anything, mapping.FaemObjectID).Return(mapping, nil)
	externalDb.On("SetFinishedState", mock.Anything, mapping.LimeObjectID).Return(errors.New("testing")).Once()
	db.On("SetFinishedState", mock.Anything, mapping.FaemObjectID).Return(nil)
	err = hdlr.SetFinishedState(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)

	externalDb.On("SetFinishedState", mock.Anything, mapping.LimeObjectID).Return(nil)
	err = hdlr.SetFinishedState(context.Background(), mapping.FaemObjectID)
	assert.NoError(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)
}

func TestHandler_CancelOrder(t *testing.T) {
	const faemID = "123asd"
	var (
		client mockRemoteClient
		db     mockDB
	)
	hdlr := Handler{
		Client: &client,
		DB:     &db,
	}

	client.On("CancelOrder", mock.Anything, faemID).Return(errors.New("testing")).Once()
	db.On("SetCancelledState", mock.Anything, faemID).Return(nil)
	err := hdlr.CancelOrder(context.Background(), faemID)
	assert.Error(t, err)
	client.AssertExpectations(t)

	client.On("CancelOrder", mock.Anything, faemID).Return(nil)
	err = hdlr.CancelOrder(context.Background(), faemID)
	assert.NoError(t, err)
	client.AssertExpectations(t)
}

func TestHandler_ReleaseOrder(t *testing.T) {
	mapping := models.Mapping{
		LimeObjectID: 5,
		FaemObjectID: "123abc",
	}
	var (
		db         mockDB
		externalDb mockExternalDB
	)
	hdlr := Handler{
		DB:         &db,
		ExternalDB: &externalDb,
	}

	db.On("GetMappedOrder", mock.Anything, mock.Anything).Return(mapping, errors.New("testing")).Once()
	err := hdlr.ReleaseOrder(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertNotCalled(t, "ReleaseState")

	db.On("GetMappedOrder", mock.Anything, mapping.FaemObjectID).Return(mapping, nil)
	db.On("SetCancelledState", mock.Anything, mapping.FaemObjectID).Return(nil)
	externalDb.On("ReleaseState", mock.Anything, mapping.LimeObjectID).Return(nil).Once()
	err = hdlr.ReleaseOrder(context.Background(), mapping.FaemObjectID)
	assert.NoError(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)

	externalDb.On("ReleaseState", mock.Anything, mapping.LimeObjectID).Return(errors.New("testing"))
	err = hdlr.ReleaseOrder(context.Background(), mapping.FaemObjectID)
	assert.Error(t, err)
	db.AssertExpectations(t)
	externalDb.AssertExpectations(t)
}
