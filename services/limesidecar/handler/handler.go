package handler

import (
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

type Config struct {
	DistributionTimeout time.Duration
	DataTransferTimeout time.Duration
}

type Handler struct {
	Config        Config
	Client        RemoteClient
	DB            OrderRepository
	ExternalDB    ExternalOrderRepository
	ServiceMapper *ServiceMapper

	wg     sync.WaitGroup
	closed chan struct{}
}

func (h *Handler) Init() {
	h.closed = make(chan struct{})

	// call all the initializers here
	go h.monitorUndispatchedOrders()
	go h.monitorSearchingOrders()
}

func (h *Handler) Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		close(h.closed)
		h.wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("handler stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("handler stopped forcibly")
	}
}
