package rabbitmq

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/proto"
)

const (
	channelNameOrderStateChanged = "orderStateChanged"

	maxOrderStateChangesAllowed = 5
)

func (s *Subscriber) HandleOrderStateChanged(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var state proto.OrderState
	if err := s.Encoder.Decode(msg.Body, &state); err != nil {
		return errors.Wrap(err, "failed to decode an order state")
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling order state changed",
		"order_uuid": state.OrderUUID,
		"state":      state.State,
	})

	// Handle incoming message
	if err := s.Handler.ChangeOrderState(context.Background(), state); err != nil {
		log.Errorf("failed to handle order state changing: %v", err)
		return nil
	}

	log.Info("OK")
	return nil
}

func (s *Subscriber) initOrderStateChanged() error {
	orderStateChangedChannel, err := s.Rabbit.GetReceiver(channelNameOrderStateChanged)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = orderStateChangedChannel.ExchangeDeclare(
		rabbit.OrderExchange, // name
		"topic",              // type
		true,                 // durable
		false,                // auto-deleted
		false,                // internal
		false,                // no-wait
		nil,                  // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	queue, err := orderStateChangedChannel.QueueDeclare(
		rabbit.LimeSidecarOrderStateQueue, // name
		true,                              // durable
		false,                             // delete when unused
		false,                             // exclusive
		false,                             // no-wait
		nil,                               // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = orderStateChangedChannel.QueueBind(
		queue.Name,           // queue name
		rabbit.StateKey+".*", // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := orderStateChangedChannel.Consume(
		queue.Name,                           // queue
		rabbit.LimeSidecarOrderStateConsumer, // consumer
		true,                                 // auto-ack
		false,                                // exclusive
		false,                                // no-local
		false,                                // no-wait
		nil,                                  // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	go s.handleOrdersStatesChanged(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleOrdersStatesChanged(messages <-chan amqp.Delivery) {
	s.wg.Add(1)
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxOrderStateChangesAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleOrderStateChanged(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle order state changing: %v", err)
					}
				},
			))
		}
	}
}
