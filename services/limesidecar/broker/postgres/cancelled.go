package postgres

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/korovkin/limiter"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
)

const (
	maxCancelledOrdersAllowed = 5
)

type ExternalCancelledRepository interface {
	GetCancelledOrders(context.Context) ([]models.UndispatchedOrder, error)
}

type CancelledRepository interface {
	FilterUnknownAndEndedOrders(ctx context.Context, cancelled []models.UndispatchedOrder) ([]models.Mapping, error)
}

func (s *Subscriber) HandleCancelledOrder(ctx context.Context, message *pg.Notification) {
	logs.Eloger.WithFields(logrus.Fields{
		"channel": message.Channel,
		"payload": message.Payload,
	}).Info("cancelled orders received")

	orders, err := s.ExternalRepository.GetCancelledOrders(ctx)
	if err != nil {
		logs.Eloger.Errorf("failed to get cancelled orders: %v", err)
		return
	}

	// Remove unknown and ended orders
	mappings, err := s.Repository.FilterUnknownAndEndedOrders(ctx, orders)
	if err != nil {
		logs.Eloger.Errorf("failed to filter already unknown or ended orders: %v", err)
		return
	}

	limit := limiter.NewConcurrencyLimiter(maxCancelledOrdersAllowed)
	defer limit.Wait()

	// Handle all the cancelled orders in separate goroutines
	for _, m := range mappings {
		mapping := m // super important to copy the value to use in the closure below
		limit.Execute(lang.Recover(
			func() {
				if err := s.Handler.ExternallyCancelMapping(ctx, mapping); err != nil {
					logs.Eloger.Errorf("failed to finish or cancel the order mapping %v: %v", mapping, err)
					return
				}
				logs.Eloger.WithField("mappingID", mapping.ID).Info("externally cancelled in faem successfully")
			},
		))
	}
}

func (s *Subscriber) initOrderCancelled() {
	go s.handleCancelledOrders(s.CancelListener.Channel())
}

func (s *Subscriber) handleCancelledOrders(messages <-chan *pg.Notification) {
	s.wg.Add(1)
	defer s.wg.Done()

	// Handle messages sequentially one-by-one
	for msg := range messages {
		s.HandleCancelledOrder(context.Background(), msg)
	}
}
