package postgres

import (
	"context"

	"github.com/go-pg/pg"
	"github.com/korovkin/limiter"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/models"
)

const (
	maxUndispatchedOrdersAllowed = 5
)

type ExternalUndispatchedRepository interface {
	GetUndispatchedOrders(context.Context) ([]models.UndispatchedOrderWithDeadline, error)
}

type UndispatchedRepository interface {
	FilterKnownUndispatchedOrders(ctx context.Context, undispatched []models.UndispatchedOrderWithDeadline) (
		[]models.UndispatchedOrderWithDeadline, error,
	)
}

func (s *Subscriber) HandleUndispatchedOrder(ctx context.Context, message *pg.Notification) {
	logs.Eloger.WithFields(logrus.Fields{
		"channel": message.Channel,
		"payload": message.Payload,
	}).Info("undispatched orders received")

	orders, err := s.ExternalRepository.GetUndispatchedOrders(ctx)
	if err != nil {
		logs.Eloger.Errorf("failed to get undispatched orders: %v", err)
		return
	}

	// Remove orders we already know about
	orders, err = s.Repository.FilterKnownUndispatchedOrders(ctx, orders)
	if err != nil {
		logs.Eloger.Errorf("failed to filter already known undispatched orders: %v", err)
		return
	}

	limit := limiter.NewConcurrencyLimiter(maxUndispatchedOrdersAllowed)
	defer limit.Wait()

	// Handle all the undispatched orders in separate goroutines
	for _, o := range orders {
		order := o // super important to copy the value to use in the closure below
		limit.Execute(lang.Recover(
			func() {
				if err := s.Handler.RememberUndispatchedOrder(ctx, order); err != nil {
					logs.Eloger.Errorf("failed to transfer an undispatched order %v: %v", order, err)
					return
				}
				logs.Eloger.WithField("limeOrderID", order.Idx).Info("remembered in faem successfully")
			},
		))
	}
}

func (s *Subscriber) initOrderUndispatched() {
	go s.handleUndispatchedOrders(s.CreateListener.Channel())
}

func (s *Subscriber) handleUndispatchedOrders(messages <-chan *pg.Notification) {
	s.wg.Add(1)
	defer s.wg.Done()

	// Handle messages sequentially one-by-one
	for msg := range messages {
		s.HandleUndispatchedOrder(context.Background(), msg)
	}
}
