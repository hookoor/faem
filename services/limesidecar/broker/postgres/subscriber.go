package postgres

import (
	"sync"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/handler"
)

type ExternalListener interface {
	Close() error
	Channel() <-chan *pg.Notification
}

type ExternalOrderRepository interface {
	ExternalUndispatchedRepository
	ExternalCancelledRepository
}

type OrderRepository interface {
	UndispatchedRepository
	CancelledRepository
}

type Subscriber struct {
	CreateListener     ExternalListener
	CancelListener     ExternalListener
	ExternalRepository ExternalOrderRepository
	Repository         OrderRepository
	Handler            *handler.Handler
	ServiceMapper      *handler.ServiceMapper

	wg sync.WaitGroup
}

func NewSubscriber(
	createListener, cancelListener ExternalListener,
	externalRepository ExternalOrderRepository,
	repository OrderRepository,
	handler *handler.Handler,
	serviceMapper *handler.ServiceMapper,
) *Subscriber {
	sub := Subscriber{
		CreateListener:     createListener,
		CancelListener:     cancelListener,
		ExternalRepository: externalRepository,
		Repository:         repository,
		Handler:            handler,
		ServiceMapper:      serviceMapper,
	}
	sub.initOrderUndispatched()
	sub.initOrderCancelled()
	return &sub
}

func (s *Subscriber) Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		_ = s.CreateListener.Close()
		_ = s.CancelListener.Close()
		s.wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("pg subscriber stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("pg subscriber stopped forcibly")
	}
}
