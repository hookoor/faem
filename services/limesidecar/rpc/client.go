package rpc

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/services/limesidecar/proto"
)

const (
	cancellationReason = "order_is_not_relevant"

	maxRequestAttempts = 3
	retryInterval      = 1 * time.Second
)

type Config struct {
	BaseURL        string
	CreateOrderURL string
	CancelOrderURL string
	JWT            string
}

type RemoteClient struct {
	HttpClient *http.Client
	Config     Config
}

func (r *RemoteClient) CreateOrder(_ context.Context, order proto.UndispatchedOrder) (proto.CreatedOrder, error) {
	var createdOrder proto.CreatedOrder
	err := lang.Retry(maxRequestAttempts, retryInterval, func() (bool, error) {
		err := r.request("POST", r.Config.BaseURL+r.Config.CreateOrderURL, order, &createdOrder)
		if !isTemporaryError(err) {
			return true, err
		}
		return false, err
	})
	return createdOrder, err
}

func (r *RemoteClient) CancelOrder(_ context.Context, orderID string) error {
	payload := struct {
		Reason string `json:"reason"`
	}{
		Reason: cancellationReason,
	}
	return lang.Retry(maxRequestAttempts, retryInterval, func() (bool, error) {
		err := r.request("PUT", r.Config.BaseURL+r.Config.CancelOrderURL+orderID, payload, nil)
		if !isTemporaryError(err) {
			return true, err
		}
		return false, err
	})
}

func (r *RemoteClient) request(method, url string, payload, response interface{}) error {
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", "Bearer "+r.Config.JWT)

	resp, err := r.HttpClient.Do(req)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("failed to make a %s request", method))
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("post request to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf("post request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}
	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}

type temporary interface {
	Temporary() bool
}

func isTemporaryError(err error) bool {
	if err == nil {
		return false
	}

	cause := errors.Cause(err)
	if te, ok := cause.(temporary); ok {
		return te.Temporary()
	}
	return false
}
