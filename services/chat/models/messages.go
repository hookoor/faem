package models

import (
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	InitMessage = "init_chat_message"
)

// ChatMessages histroy of all chats
type ChatMessages struct {
	structures.ChatMessages
	CreatedAt time.Time `json:"-"`
}

func FillCreatedAtUnix(mess ...*ChatMessages) {
	for i := range mess {
		mess[i].CreatedAtUnix = mess[i].CreatedAt.Unix()
	}
}
func GetInitMessageFromOrder(order *structures.Order) *ChatMessages {
	return &ChatMessages{
		CreatedAt: time.Now(),
		ChatMessages: structures.ChatMessages{
			OrderUUID:  order.UUID,
			ClientUUID: order.Client.UUID,
			Message:    InitMessage,
		},
	}
}
