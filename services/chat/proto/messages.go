package proto

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/chat/models"
)

type (
	// ChatMessageToMark format for bind
	ChatMessagesToMark struct {
		MessagesUUID []string `json:"messages_uuid"`
	}
	// ChatMessagesIn format for bind
	ChatMessagesIn struct {
		Message   string                 `json:"message"`
		OrderUUID string                 `json:"order_uuid"`
		Receiver  structures.ChatMembers `json:"receiver"`
	}
	// ChatHistoryIn format for bind
	ChatHistoryIn struct {
		OrderUUID string                 `json:"order_uuid"`
		Receiver  structures.ChatMembers `json:"receiver"`
	}
	UserData struct {
		UUID   string
		Sender structures.ChatMembers
	}
	Receiver interface {
		GetReceiver() structures.ChatMembers
	}
)

func (mess *ChatHistoryIn) GetReceiver() structures.ChatMembers {
	return mess.Receiver
}

func (mess *ChatMessagesIn) GetReceiver() structures.ChatMembers {
	return mess.Receiver
}
func (mess *ChatMessagesToMark) Validate() error {
	if len(mess.MessagesUUID) == 0 {
		return errors.New("messages_uuid cannot be empty")
	}
	return nil
}
func (mess *ChatMessagesIn) Validate(sender structures.ChatMembers) error {
	if mess.OrderUUID == "" {
		return errors.New("you must provide order uuid")
	}
	if mess.Message == "" {
		return errors.New("you must provide message")
	}
	if mess.Message == models.InitMessage {
		return errors.New("invalid message")
	}
	if mess.Receiver != structures.DriverMember &&
		mess.Receiver != structures.ClientMember &&
		mess.Receiver != structures.UserCRMMember &&
		mess.Receiver != structures.TelegramBotMember {
		return errors.New("invalid receiver field")
	}
	err := mess.validateReceiver(sender)
	return err
}

func (mess *ChatHistoryIn) Validate(sender structures.ChatMembers) error {
	if mess.OrderUUID == "" {
		return errors.New("you must provide order uuid")
	}
	if mess.Receiver != structures.DriverMember &&
		mess.Receiver != structures.ClientMember &&
		mess.Receiver != structures.UserCRMMember &&
		mess.Receiver != structures.TelegramBotMember {
		return errors.New("invalid receiver field")
	}
	err := mess.validateReceiver(sender)
	return err
}

func (mess *ChatHistoryIn) validateReceiver(sender structures.ChatMembers) error {
	return validateReceiver(mess, sender)
}

func (mess *ChatMessagesIn) validateReceiver(sender structures.ChatMembers) error {
	return validateReceiver(mess, sender)
}

func validateReceiver(receiver Receiver, sender structures.ChatMembers) error {
	switch sender {
	case structures.ClientMember:
		//if receiver.GetReceiver() != structures.DriverMember {
		//	return errors.New("invalid receiver field for client")
		//}
	case structures.DriverMember:
		if receiver.GetReceiver() == structures.DriverMember {
			return errors.New("invalid receiver field for driver")
		}
	case structures.UserCRMMember:
		if receiver.GetReceiver() == structures.UserCRMMember {
			return errors.New("invalid receiver field for operator")
		}
	}
	return nil
}
