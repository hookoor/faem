package publisher

import (
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/chat/config"
)

const (
	senderChannelName     = "fcmSender"
	notifierSenderChannel = "notifierSender"
	senderChannelName2    = "rabSender"
)

// MessagesMarkedAsRead -
func (p *Publisher) MessagesMarkedAsRead(mess *structures.ChatMessagesMarkedAsRead) error {
	senderChannel, err := p.Rabbit.GetSender(senderChannelName2)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	err = p.Publish(senderChannel, rabbit.FCMExchange, rabbit.ChatMessagesMarkedAsReadKey, mess, mess.ReaderUUID)
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

// NewMessage -
func (p *Publisher) NewMessage(mess *structures.ChatMessages, receiver structures.ChatMembers, senderUUID string) error {
	senderChannel, err := p.Rabbit.GetSender(senderChannelName)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	if receiver == structures.ClientMember {
		err = p.Publish(senderChannel, rabbit.FCMExchange, rabbit.ChatMessageToClientKey, mess, senderUUID)
		if err != nil {
			return errpath.Err(err)
		}
		return nil
	}
	if receiver == structures.DriverMember {
		err = p.Publish(senderChannel, rabbit.FCMExchange, rabbit.ChatMessageToDriverKey, mess, senderUUID)
		if err != nil {
			return errpath.Err(err)
		}
		return nil
	}
	if receiver == structures.UserCRMMember {
		err = p.Publish(senderChannel, rabbit.FCMExchange, rabbit.ChatMessageToOperatorKey, mess, senderUUID)
		if err != nil {
			return errpath.Err(err)
		}
		return nil
	}

	return nil
}

func presence(channel, client string) (bool, error) {
	var connExist bool
	epoint := structures.EndPoints.Notyfire.IsPresence

	url := config.St.Notifier.Host + tool.SetURLParams(epoint.URL(), map[string]string{
		epoint.Params.Client():  client,
		epoint.Params.Channel(): channel,
	})
	err := tool.SendRequest(epoint.Method(), url, nil, nil, &connExist)
	if err != nil {
		return false, errpath.Err(err)
	}
	return connExist, nil
}

// пока не используется
func (p *Publisher) sendToNotifier(channel string, data interface{}, senderUUID string) error {
	senderChannel, err := p.Rabbit.GetSender(notifierSenderChannel)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}
	pl := structures.NotificationDesirablePayload{Tag: fcm.ChatMessageTag, Payload: data}
	notiData, err := structures.NewNotification(channel, pl)
	if err != nil {
		return errpath.Err(err)
	}

	err = p.Publish(senderChannel, rabbit.NotifierExchange, rabbit.NewKey, notiData, senderUUID)
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

func (p *Publisher) initNewMessage() error {
	senderChannel, err := p.Rabbit.GetSender(senderChannelName)
	if err != nil {
		return errors.Wrapf(err, "failed to get a sender channel")
	}

	err = senderChannel.ExchangeDeclare(
		rabbit.FCMExchange, // name // TODO: use constant or variable
		"topic",            // type // TODO: change me
		true,               // durable
		false,              // auto-deleted
		false,              // internal
		false,              // no-wait
		nil,                // arguments
	)
	return errors.Wrap(err, "failed to create an exchange")
}
