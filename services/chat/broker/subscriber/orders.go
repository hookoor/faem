package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

func (s *Subscriber) initNewOrdersReceiver() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.ChatNewOrderQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.ChatNewOrderQueue, // name
		true,                     // durable
		false,                    // delete when unused
		false,                    // exclusive
		false,                    // no-wait
		nil,                      // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,           // queue name
		rabbit.NewKey,        // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                  // queue
		rabbit.ChatNewOrderConsumer, // consumer
		true,                        // auto-ack
		false,                       // exclusive
		false,                       // no-local
		false,                       // no-wait
		nil,                         // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewOrders(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewOrders(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewUsersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleNewOrder(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle new order: %v", err)
					}
				},
			))
		}
	}
}

func (s *Subscriber) HandleNewOrder(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var order structures.Order
	if err := s.Encoder.Decode(msg.Body, &order); err != nil {
		return errors.Wrap(err, "failed to decode new order")
	}

	// Handle incoming message somehow
	if err := s.Handler.NewOrder(context.Background(), &order); err != nil {
		return errors.Wrap(err, "failed to handle new order")
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order",
	}).Info("chat receiving new order successfully")
	return nil
}
