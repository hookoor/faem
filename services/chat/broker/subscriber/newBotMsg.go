package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	channelNameNewBotMsg = "newMsgFromBot"
)

func (s *Subscriber) HandleNewBotMsg(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var botMsg *structures.MessageFromBot
	if err := s.Encoder.Decode(msg.Body, &botMsg); err != nil {
		return errors.Wrap(err, "failed to decode new order state")
	}

	// Handle incoming message somehow
	if err := s.Handler.NewMsgFromBot(ctx, botMsg); err != nil {
		return errors.Wrap(err, "failed to handle new msg from bot")
	}

	return nil
}

func (s *Subscriber) initNewBotMsg() error {
	receiverChannel, err := s.Rabbit.GetReceiver(channelNameNewBotMsg)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.ChatNewBotMsgQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}

	err = receiverChannel.QueueBind(
		queue.Name,               // queue name
		rabbit.NewIncomingMsgKey, // routing key
		rabbit.ChatExchange,      // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                   // queue
		rabbit.ChatNewBotMsgConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewBotMsg(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewBotMsg(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewUsersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleNewBotMsg(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle new bot message: %v", err)
					}
				},
			))
		}
	}
}
