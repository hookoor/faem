package subscriber

import (
	"context"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	maxNewUsersAllowed = 100
)

func (s *Subscriber) HandleNewOrderState(ctx context.Context, msg amqp.Delivery) error {
	// Decode incoming message
	var orState structures.OfferStates
	if err := s.Encoder.Decode(msg.Body, &orState); err != nil {
		return errors.Wrap(err, "failed to decode new order state")
	}

	// Handle incoming message somehow
	if err := s.Handler.OrderState(ctx, &orState); err != nil {
		return errors.Wrap(err, "failed to handle new order state")
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event": "handling new order state",
	}).Info("chat receiving new order state successfully")
	return nil
}

func (s *Subscriber) initOrderStateReceiver() error {
	receiverChannel, err := s.Rabbit.GetReceiver(rabbit.ChatOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	queue, err := receiverChannel.QueueDeclare(
		rabbit.ChatOrderStateQueue, // name
		true,                       // durable
		false,                      // delete when unused
		false,                      // exclusive
		false,                      // no-wait
		nil,                        // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to declare a queue")
	}
	neededKey := "state." + constants.OrderStateAccepted
	err = receiverChannel.QueueBind(
		queue.Name,           // queue name
		neededKey,            // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return errors.Wrap(err, "failed to bind a queue")
	}

	msgs, err := receiverChannel.Consume(
		queue.Name,                     // queue
		rabbit.ChatOrderStatesConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return errors.Wrap(err, "failed to consume from a channel")
	}

	s.wg.Add(1)
	go s.handleNewOrderState(msgs) // handle incoming messages
	return nil
}

func (s *Subscriber) handleNewOrderState(messages <-chan amqp.Delivery) {
	defer s.wg.Done()

	limit := limiter.NewConcurrencyLimiter(maxNewUsersAllowed)
	defer limit.Wait()

	for {
		select {
		case <-s.closed:
			return
		case msg := <-messages:
			// Start new goroutine to handle multiple requests at the same time
			limit.Execute(lang.Recover(
				func() {
					if err := s.HandleNewOrderState(context.Background(), msg); err != nil {
						logs.Eloger.Errorf("failed to handle new order state: %v", err)
					}
				},
			))
		}
	}
}
