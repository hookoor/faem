package server

import (
	"errors"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/chat/proto"
)

const (
	errBind = "data binding error"
)

var (
	okStruct = structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	}
)

func (r *Rest) AllUnreadMessages(c echo.Context) error {

	inputUUID, uuidType, err := userUUIDFromJWT(c)
	if err != nil {
		mess := "error getting uuid from jwt"
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "all unread messages",
				"reason": mess,
			}).
			Error(err)
		res := logs.OutputRestError(mess, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out, err := r.Handler.UnreadMessagesForOperator(c.Request().Context(), proto.UserData{
		UUID:   inputUUID,
		Sender: uuidType,
	})
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "all unread messages",
				"reason": "finding messages error",
			}).Error(err)
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

// AllUnreadMessagesByOrderUUIDs -
func (r *Rest) AllUnreadMessagesByOrderUUIDs(c echo.Context) error {

	inputUUID, uuidType, err := userUUIDFromJWT(c)

	var Orders structures.OrdersUUIDForUnreadMessages
	if err := c.Bind(&Orders); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "unread messages for orders",
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	if err != nil {
		mess := "error getting uuid from jwt"
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "unread messages for orders",
				"reason": mess,
			}).
			Error(err)
		res := logs.OutputRestError(mess, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out, err := r.Handler.UnreadMessagesForOperatorByOrderUUIDs(
		c.Request().Context(),
		proto.UserData{
			UUID:   inputUUID,
			Sender: uuidType,
		},
		Orders.OrderUUIDs)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "unread messages for orders",
				"reason": "finding messages error",
			}).Error(err)
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

func (r *Rest) ChatHistory(c echo.Context) error {
	var mess proto.ChatHistoryIn
	if err := c.Bind(&mess); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "chat history",
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	inputUUID, uuidType, err := userUUIDFromJWT(c)
	if err != nil {
		mess := "error getting uuid from jwt"
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "chat history",
				"reason": mess,
			}).
			Error(err)
		res := logs.OutputRestError(mess, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out, err := r.Handler.ChatHistory(c.Request().Context(), &mess, proto.UserData{
		UUID:   inputUUID,
		Sender: uuidType,
	})
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":     "chat history",
				"orderUUID": mess.OrderUUID,
				"reason":    "finding chat history error",
			}).Error(err)
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}
func (r *Rest) MarkMessageAsRead(c echo.Context) error {
	var mess proto.ChatMessagesToMark
	log := logs.LoggerForContext(c.Request().Context()).
		WithFields(logrus.Fields{
			"event": "new messages for mark as read",
		})
	if err := c.Bind(&mess); err != nil {
		log.WithFields(logrus.Fields{
			"reason": errBind,
		}).Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	inputUUID, uuidType, err := userUUIDFromJWT(c)
	if err != nil {
		mess := "error getting uuid from jwt"
		log.WithFields(logrus.Fields{
			"reason": mess,
		}).Error(err)
		res := logs.OutputRestError(mess, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = r.Handler.MarkMessagesAsRead(c.Request().Context(), &mess, proto.UserData{
		UUID:   inputUUID,
		Sender: uuidType,
	})
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			Error(err)
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, okStruct)
}
func (r *Rest) NewMessage(c echo.Context) error {
	var mess proto.ChatMessagesIn
	if err := c.Bind(&mess); err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "new message",
				"reason": errBind,
			}).
			Error(err)
		res := logs.OutputRestError(errBind, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	inputUUID, uuidType, err := userUUIDFromJWT(c)
	if err != nil {
		mess := "error getting uuid from jwt"
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event":  "new message",
				"reason": mess,
			}).
			Error(err)
		res := logs.OutputRestError(mess, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out, err := r.Handler.NewMessage(c.Request().Context(), &mess, proto.UserData{
		UUID:   inputUUID,
		Sender: uuidType,
	})
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			Error(err) // you may add additional fields here
		res := logs.OutputRestError("can't greet", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, out)
}

// userUUIDFromJWT returns uuid and him type(client,driver or operator)
func userUUIDFromJWT(c echo.Context) (string, structures.ChatMembers, error) {
	user := c.Get("user")
	userTok, check := user.(*jwt.Token)
	if !check {
		return "", "", errors.New("invalid user field in context")
	}
	claims := userTok.Claims.(jwt.MapClaims)
	clientUUID, check := claims["client_uuid"]
	if check {
		return clientUUID.(string), structures.ClientMember, nil
	}
	driverUUID, check := claims["driver_uuid"]
	if check {
		return driverUUID.(string), structures.DriverMember, nil
	}
	operUUID, check := claims["user_uuid"]
	if check {
		return operUUID.(string), structures.UserCRMMember, nil
	}
	return "", "", errors.New("invalid uuid field in context")
}
