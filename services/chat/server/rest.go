package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/chat/config"
	"gitlab.com/faemproject/backend/faem/services/chat/handler"
)

const (
	apiPrefix = "/api/v2"
)

type Rest struct {
	Router  *echo.Echo
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route() {
	web.UseHealthCheck(r.Router)

	r.Router.Use(middleware.CORS())
	g := r.Router.Group(apiPrefix)

	jwtSec := config.JWTSecret()
	g.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(jwtSec),
	}))
	// g.GET("/hello", r.Hello)
	g.POST("/messages/new", r.NewMessage)
	g.POST("/messages/mark/read", r.MarkMessageAsRead)
	g.POST("/chat/history", r.ChatHistory)
	g.GET("/chat/history/unread", r.AllUnreadMessages)
	g.POST("/chat/history/unread", r.AllUnreadMessagesByOrderUUIDs)
}
