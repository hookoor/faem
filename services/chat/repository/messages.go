package repository

import (
	"context"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/chat/models"
	"gitlab.com/faemproject/backend/faem/services/chat/proto"
)

func (p *Pg) GetMessagesByUUID(ctx context.Context, messUUID []string) ([]models.ChatMessages, error) {
	if len(messUUID) == 0 {
		return nil, errors.New("empty messages uuid array")
	}
	var result []models.ChatMessages
	err := p.Db.ModelContext(ctx, &result).
		Where("uuid in (?)", pg.In(messUUID)).
		Order("created_at desc").
		Select()
	return result, err
}
func (p *Pg) MarkMessagesAsRead(ctx context.Context, messUUID []string) error {
	if len(messUUID) == 0 {
		return nil
	}
	_, err := p.Db.ModelContext(ctx, &models.ChatMessages{}).
		Where("uuid in (?)", pg.In(messUUID)).
		Set("ack = true").
		Update()
	return err
}
func (p *Pg) InsertNewMessage(ctx context.Context, mess *models.ChatMessages) error {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	uu, err := uuid.NewV4()
	if err != nil {
		return errors.Wrap(err, "ошибка генерации uuid")
	}
	mess.UUID = uu.String()
	mess.CreatedAt = time.Now()
	_, err = p.Db.ModelContext(ctx, mess).
		Insert()
	if err != nil {
		return errors.Wrap(err, "ошибка сохранения нового сообщения")
	}
	return nil
}

func (p *Pg) GetAllUnreadMessagesForOperator(ctx context.Context) ([]*models.ChatMessages, error) {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	var mess []*models.ChatMessages
	err := p.Db.ModelContext(ctx, &mess).
		Where("ack is not true").
		Where("receiver = ?", structures.UserCRMMember).
		Order("created_at DESC").
		Select()
	if err != nil {
		return mess, errors.Wrap(err, "ошибка поиска непрочитанных сообщений")
	}
	return mess, nil
}

// GetAllUnreadMessagesForOperatorByOrderUUIDs -
func (p *Pg) GetAllUnreadMessagesForOperatorByOrderUUIDs(ctx context.Context, orderUUIDs []string) ([]*models.ChatMessages, error) {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	var mess []*models.ChatMessages
	err := p.Db.ModelContext(ctx, &mess).
		Where("ack is not true").
		Where("receiver = ?", structures.UserCRMMember).
		Where("order_uuid in (?)", pg.In(orderUUIDs)).
		Column("order_uuid").
		Order("created_at DESC").
		Select()
	if err != nil {
		return mess, errors.Wrap(err, "ошибка поиска непрочитанных сообщений")
	}
	return mess, nil
}

func (p *Pg) GetInitMessageByOrderUUID(ctx context.Context, orderUUID string) (*models.ChatMessages, error) {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	var mess models.ChatMessages
	err := p.Db.ModelContext(ctx, &mess).
		Where("order_uuid = ?", orderUUID).
		Where("message = ?", models.InitMessage).
		Order("created_at DESC").
		Limit(1).
		Select()
	if err != nil {
		return &mess, errors.Wrap(err, "ошибка поиска чата")
	}
	return &mess, nil
}

func (p *Pg) GetChatHistoryForMember(ctx context.Context, orderUUID string, receiver structures.ChatMembers, userData proto.UserData) ([]*models.ChatMessages, error) {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	var mess []*models.ChatMessages
	defQuery := p.Db.ModelContext(ctx, &mess).
		Where("order_uuid = ?", orderUUID).
		Order("created_at DESC")
	additionalQueryByMember(userData, receiver, defQuery)
	err := defQuery.
		Select()
	if err != nil {
		return mess, errors.Wrap(err, "ошибка поиска истории чата")
	}
	return mess, nil
}

func (p *Pg) AddDriverUUIDToChat(ctx context.Context, orderUUID, driverUUID string) (*models.ChatMessages, error) {
	// only wait for the reply for 30 seconds
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	var mess models.ChatMessages
	_, err := p.Db.ModelContext(ctx, &mess).
		Where("order_uuid = ?", orderUUID).
		Where("message = ?", models.InitMessage).
		Set("driver_uuid = ?", driverUUID).
		Update()
	if err != nil {
		return &mess, errors.Wrap(err, "ошибка обновления чата")
	}
	return &mess, nil
}

func additionalQueryByMember(userData proto.UserData, receiver structures.ChatMembers, query *orm.Query) {

	// чтобы можно было вернуть оператору переписку с клиентом, если он пришлет receiver = client
	// DEN - я это убрал потому что не можем получить переписку клиент-оператор
	//if receiver == structures.ClientMember && userData.Sender == structures.OperatorMember {
	//	userData.Sender = structures.DriverMember
	//}
	query = query.
		Where("(receiver = ? and sender = ?) OR (receiver = ? and sender = ?)", receiver, userData.Sender, userData.Sender, receiver)
}
