package handler

import (
	"context"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/chat/proto"
)

func (h *Handler) NewMsgFromBot(ctx context.Context, msg *structures.MessageFromBot) error {
	mess := proto.ChatMessagesIn{
		Message:   msg.Text,
		OrderUUID: msg.OrderUUID,
		Receiver:  structures.UserCRMMember,
	}
	var usr proto.UserData
	if msg.Source == string(structures.TelegramBotMember) {
		usr = proto.UserData{
			UUID:   msg.ClientUUID,
			Sender: structures.TelegramBotMember,
		}
	} else {
		usr = proto.UserData{
			UUID:   msg.ClientUUID,
			Sender: structures.ClientMember,
		}
	}
	_, err := h.NewMessage(ctx, &mess, usr)
	if err != nil {
		return err
	}
	return nil
}
