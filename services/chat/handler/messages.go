package handler

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/chat/models"
	"gitlab.com/faemproject/backend/faem/services/chat/proto"
)

const (
	errInvalidUser = "нет доступа к чату"
)

type (
	MessagePublisher interface {
		MessagesMarkedAsRead(mess *structures.ChatMessagesMarkedAsRead) error
		NewMessage(mess *structures.ChatMessages, receiver structures.ChatMembers, senderUUID string) error
	}
	MessageRepository interface {
		GetMessagesByUUID(ctx context.Context, messUUID []string) ([]models.ChatMessages, error)
		MarkMessagesAsRead(ctx context.Context, messUUID []string) error
		GetChatHistoryForMember(ctx context.Context, orderUUID string, receiver structures.ChatMembers, userData proto.UserData) ([]*models.ChatMessages, error)
		InsertNewMessage(ctx context.Context, mess *models.ChatMessages) error
		AddDriverUUIDToChat(ctx context.Context, orderUUID, driverUUID string) (*models.ChatMessages, error)
		GetInitMessageByOrderUUID(ctx context.Context, orderUUID string) (*models.ChatMessages, error)
		GetAllUnreadMessagesForOperator(ctx context.Context) ([]*models.ChatMessages, error)
		GetAllUnreadMessagesForOperatorByOrderUUIDs(ctx context.Context, orderUUIDs []string) ([]*models.ChatMessages, error)
	}
)

func (h *Handler) MarkMessagesAsRead(ctx context.Context, messToMark *proto.ChatMessagesToMark, usData proto.UserData) error {
	err := messToMark.Validate()
	if err != nil {
		return fmt.Errorf("validate error,%s", err)
	}
	messages, err := h.DB.GetMessagesByUUID(ctx, messToMark.MessagesUUID)
	if err != nil {
		return fmt.Errorf("getting messages by uuid error,%s", err)
	}
	var orderUUID string
	for _, uuid := range messToMark.MessagesUUID {
		check := false
		for _, mes := range messages {
			// чтобы узнать к какому заказу привязаны сообщения
			orderUUID = mes.OrderUUID
			if mes.UUID == uuid {
				check = true
				break
			}
		}
		if !check {
			return fmt.Errorf("message with uuid [%s] not found", uuid)
		}
	}
	for _, mes := range messages {
		if mes.Receiver != usData.Sender {
			return fmt.Errorf("you cannot mark as read message with uuid [%s]", mes.UUID)
		}
		if mes.Ack {
			return fmt.Errorf("message with uuid [%s] already marked as read", mes.UUID)
		}
	}
	err = h.DB.MarkMessagesAsRead(ctx, messToMark.MessagesUUID)
	if err != nil {
		return fmt.Errorf("error, %s", err)
	}
	go func() {
		err := h.Pub.MessagesMarkedAsRead(&structures.ChatMessagesMarkedAsRead{
			ReaderUUID:   usData.UUID,
			SenderType:   messages[0].Sender,
			OrderUUID:    orderUUID,
			ReaderType:   usData.Sender,
			MessagesUUID: messToMark.MessagesUUID,
		})
		if err != nil {
			logs.Eloger.Error(err)
		}
	}()
	return nil
}

func (h *Handler) NewMessage(ctx context.Context, messIn *proto.ChatMessagesIn, usData proto.UserData) (*models.ChatMessages, error) {
	if err := messIn.Validate(usData.Sender); err != nil {
		return nil, err
	}
	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"orderUUID": messIn.OrderUUID,
		"msg":       messIn.Message,
	}).Debug("new message") // just a logging sample

	mess, err := h.DB.GetInitMessageByOrderUUID(ctx, messIn.OrderUUID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get init message by order UUID")
	}
	models.FillCreatedAtUnix(mess)
	mess.Message = messIn.Message
	mess.Receiver = messIn.Receiver
	switch usData.Sender {
	case structures.ClientMember:
		mess.Sender = structures.ClientMember
		if mess.ClientUUID != usData.UUID {
			return nil, errors.New("для этого клиента недопустима отправка сообщений в данный чат")
		}
	case structures.TelegramBotMember:
		mess.Sender = structures.TelegramBotMember
		if mess.ClientUUID != usData.UUID {
			return nil, errors.New("для этого бота недопустима отправка сообщений в данный чат")
		}
	case structures.DriverMember:
		mess.Sender = structures.DriverMember
		if mess.DriverUUID != usData.UUID {
			return nil, errors.New("для этого водителя недопустима отправка сообщений в данный чат")
		}
	case structures.UserCRMMember:
		mess.Sender = structures.UserCRMMember
	}

	err = h.DB.InsertNewMessage(ctx, mess)
	if err != nil {
		return nil, errors.Wrap(err, "failed to insert new message")
	}
	mess.CreatedAtUnix = mess.CreatedAt.Unix()
	// Publish a message to the broker
	go func() {
		if err := h.Pub.NewMessage(&mess.ChatMessages, messIn.Receiver, usData.UUID); err != nil {
			logs.Eloger.Error(err) // just a sample error handling
		}
	}()
	// Return the result if all is ok
	return mess, nil
}

func (h *Handler) ChatHistory(ctx context.Context, messIn *proto.ChatHistoryIn, usData proto.UserData) ([]*models.ChatMessages, error) {
	if err := messIn.Validate(usData.Sender); err != nil {
		return nil, err
	}
	mess, err := h.DB.GetInitMessageByOrderUUID(ctx, messIn.OrderUUID)
	if err != nil {
		return nil, err
	}

	switch usData.Sender {
	case structures.ClientMember:
		if mess.ClientUUID != usData.UUID {
			return nil, errors.New(errInvalidUser)
		}
	case structures.DriverMember:
		if mess.DriverUUID != usData.UUID {
			return nil, errors.New(errInvalidUser)
		}
	}
	chatHistory, err := h.DB.GetChatHistoryForMember(ctx, messIn.OrderUUID, messIn.Receiver, usData)
	if err != nil {
		return nil, err
	}
	models.FillCreatedAtUnix(chatHistory...)
	// Return the result if all is ok
	return chatHistory, nil
}

func (h *Handler) UnreadMessagesForOperator(ctx context.Context, usData proto.UserData) ([]*models.ChatMessages, error) {
	if usData.Sender != structures.UserCRMMember {
		return nil, fmt.Errorf("access closed")
	}
	mess, err := h.DB.GetAllUnreadMessagesForOperator(ctx)
	if err != nil {
		return nil, err
	}
	models.FillCreatedAtUnix(mess...)
	// Return the result if all is ok
	return mess, nil
}

// UnreadMessagesForOperatorByOrderUUIDs -
func (h *Handler) UnreadMessagesForOperatorByOrderUUIDs(ctx context.Context, usData proto.UserData, orderUUIDs []string) (map[string]int, error) {
	if usData.Sender != structures.UserCRMMember {
		return nil, fmt.Errorf("access closed")
	}
	if len(orderUUIDs) == 0 {
		return nil, errpath.Errorf("array orderUUIDs is empty")
	}
	mess, err := h.DB.GetAllUnreadMessagesForOperatorByOrderUUIDs(ctx, orderUUIDs)
	if err != nil {
		return nil, err
	}

	orderMessCount := map[string]int{}
	for _, ms := range mess {
		val, ok := orderMessCount[ms.OrderUUID]
		if ok {
			orderMessCount[ms.OrderUUID] = val + 1
		} else {
			orderMessCount[ms.OrderUUID] = 1
		}
	}
	return orderMessCount, nil
}
