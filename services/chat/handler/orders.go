package handler

import (
	"context"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/chat/models"
)

func (h *Handler) OrderState(ctx context.Context, ordState *structures.OfferStates) error {
	if ordState.State != constants.OrderStateAccepted {
		return nil
	}
	_, err := h.DB.AddDriverUUIDToChat(ctx, ordState.OrderUUID, ordState.DriverUUID)
	if err != nil {
		logs.LoggerForContext(ctx).WithFields(logrus.Fields{
			"orderUUID":  ordState.OrderUUID,
			"driverUUID": ordState.DriverUUID,
		}).Error(err)
		return err
	}
	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"orderUUID":  ordState.OrderUUID,
		"driverUUID": ordState.DriverUUID,
	}).Info("order state successfully handled")
	return nil
}

func (h *Handler) NewOrder(ctx context.Context, order *structures.Order) error {

	mess := models.GetInitMessageFromOrder(order)
	mess.CreatedAt = time.Now()
	err := h.DB.InsertNewMessage(ctx, mess)
	if err != nil {
		logs.LoggerForContext(ctx).WithFields(logrus.Fields{
			"orderUUID": order.UUID,
			"reason":    "error insert init message to DB",
		}).Error(err)
		return err
	}
	logs.LoggerForContext(ctx).WithFields(logrus.Fields{
		"orderUUID": order.UUID,
	}).Info("new chat successfully initialized")
	return nil
}
