package handler

type Repository interface {
	MessageRepository
}

type Publisher interface {
	MessagePublisher
}

type Handler struct {
	DB  Repository
	Pub Publisher
}
