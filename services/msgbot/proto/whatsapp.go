package proto

type WAFrom struct {
	Profile struct {
		Name string `json:"name"`
	} `json:"profile"`
	WAID string `json:"wa_id"`
}

type WAContext struct {
	From                string   `json:"from"`
	GroupId             string   `json:"group_id"`
	Id                  string   `json:"id"`
	Mentions            []string `json:"mentions"`
	Forwarded           bool     `json:"forwarded"`
	FrequentlyForwarded bool     `json:"frequently_forwarded"`
}

type WAError struct {
	Code    int    `json:"code"`
	Title   string `json:"title"`
	Details string `json:"details"`
	Href    string `json:"href"`
}

type WAAudio struct {
	Id       string `json:"id"`
	File     string `json:"file"`
	Link     string `json:"link"`
	MimeType string `json:"mime_type"`
	SHA256   string `json:"sha256"`
}

type WADocument struct {
	Id       string `json:"id"`
	Caption  string `json:"caption"`
	File     string `json:"file"`
	Link     string `json:"link"`
	SHA256   string `json:"sha256"`
	MimeType string `json:"mime_type"`
}

type WAImage struct {
	Id       string `json:"id"`
	Caption  string `json:"caption"`
	File     string `json:"file"`
	Link     string `json:"link"`
	SHA256   string `json:"sha256"`
	MimeType string `json:"mime_type"`
}

type WALocation struct {
	Address   string  `json:"address"`
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Name      float64 `json:"name"`
}

type WASystem struct {
	Body string `json:"body"`
}

type WAText struct {
	Body string `json:"body"`
}

type WAVideo struct {
	Id       string `json:"id"`
	File     string `json:"file"`
	Link     string `json:"link"`
	SHA256   string `json:"sha256"`
	MimeType string `json:"mime_type"`
}

type WAVoice struct {
	Id       string `json:"id"`
	File     string `json:"file"`
	Link     string `json:"link"`
	SHA256   string `json:"sha256"`
	MimeType string `json:"mime_type"`
}

type WAContact struct {
	Addresses    []WAAddress `json:"addresses"`
	Birthday     string      `json:"birthday"`
	ContactImage string      `json:"contact_image"`
	Emails       []WAEmail   `json:"emails"`
	Ims          []WAIms     `json:"ims"`
	Name         WAName      `json:"name"`
	Org          WAOrg       `json:"org"`
	Phones       []WAPhone   `json:"phones"`
	Urls         []WAUrl     `json:"urls"`
	From         string      `json:"from"`
	Id           string      `json:"id"`
	Timestamp    string      `json:"timestamp"`
	Type         string      `json:"type"`
}

type WAContacts []WAContact

type WAAddress struct {
	City        string `json:"city"`
	Country     string `json:"country"`
	CountryCode string `json:"country_code"`
	State       string `json:"state"`
	Street      string `json:"street"`
	Type        string `json:"type"`
	Zip         string `json:"zip"`
}

type WAEmail struct {
	Email string `json:"email"`
	Type  string `json:"type"`
}

type WAIms struct {
	Service string `json:"service"`
	UserId  string `json:"user_id"`
}

type WAName struct {
	FirstName     string `json:"first_name"`
	FormattedName string `json:"formatted_name"`
	LastName      string `json:"last_name"`
}

type WAOrg struct {
	Company string `json:"company"`
}

type WAPhone struct {
	Phone string `json:"phone"`
	Type  string `json:"type"`
}

type WAUrl struct {
	Url  string `json:"url"`
	Type string `json:"type"`
}

type WAMessage struct {
	Context WAContext `json:"context"`

	From      string `json:"from"`
	GroupId   string `json:"group_id"`
	Id        string `json:"id"`
	Timestamp string `json:"timestamp"`
	Type      string `json:"type"`

	Audio    WAAudio    `json:"audio"`
	Document WADocument `json:"document"`
	Image    WAImage    `json:"image"`
	Location WALocation `json:"location"`
	System   WASystem   `json:"system"`
	Text     WAText     `json:"text"`
	Video    WAVideo    `json:"video"`
	Voice    WAVoice    `json:"voice"`
	Contacts WAContacts `json:"contacts"`

	Errors []WAError `json:"errors"`
}

type WAIncomingMsg struct {
	Contacts []WAFrom `json:"contacts"`

	Messages []WAMessage `json:"messages"`
}

type WAOutgoingMsg struct {
	To   string `json:"to"`
	Type string `json:"type"`
	Text WAText `json:"text"`
}

type WAStatus struct {
	Id          string `json:"id"`
	RecipientId string `json:"recipient_id"`
	Status      string `json:"status"`
	Timestamp   string `json:"timestamp"`
}

type WAStatuses struct {
	Status []WAStatus `json:"statuses"`
}

//{ https://developers.facebook.com/docs/whatsapp/api/webhooks/inbound
//	"contacts": [
//		{
//			"profile": {
//				"name": "sender-profile-name"
//			},
//			"wa_id": "wa-id-of-contact"
//		}
//	],
//
//	"messages": [
//		"context": {
//			"from": "sender-wa-id-of-context-message",
//			"group_id": "group-id-of-context-message",
//			"id": "message-id-of-context-message",
//			"mentions": [ "wa-id1", "wa-id2" ],
//			"forwarded": true | false,
//			"frequently_forwarded": true | false
//		},
//
//		"from": "sender-wa-id",
//		"group_id": "group-id",
//		"id": "message-id",
//		"timestamp": "message-timestamp",
//		"type": "audio | document | image | location | system | text | video | voice",
//
//		"errors": [ { ... } ],
//
//		"audio": {
//			"file": "absolute-filepath-on-coreapp",
//			"id": "media-id",
//			"link": "link-to-audio-file",
//			"mime_type": "media-mime-type",
//			"sha256": "checksum"
//		}
//
//		"document": {
//			"file": "absolute-filepath-on-coreapp",
//			"id": "media-id",
//			"link": "link-to-document-file",
//			"mime_type": "media-mime-type",
//			"sha256": "checksum",
//			"caption": "document-caption"
//		}
//
//		"image": {
//			"file": "absolute-filepath-on-coreapp",
//			"id": "media-id",
//			"link": "link-to-image-file",
//			"mime_type": "media-mime-type",
//			"sha256": "checksum",
//			"caption": "image-caption"
//		}
//
//		"location": {
//			"address": "1 Hacker Way, Menlo Park, CA, 94025",
//			"latitude": latitude,
//			"longitude": longitude,
//			"name": "location-name"
//		}
//
//		"system": {
//			"body": "system-message-content"
//		}
//
//		"text": {
//			"body": "text-message-content"
//		}
//
//		"video": {
//			"file": "absolute-filepath-on-coreapp",
//			"id": "media-id",
//			"link": "link-to-video-file",
//			"mime_type": "media-mime-type",
//			"sha256": "checksum"
//		}
//
//		"voice": {
//			"file": "absolute-filepath-on-coreapp",
//			"id": "media-id",
//			"link": "link-to-audio-file",
//			"mime_type": "media-mime-type",
//			"sha256": "checksum"
//		}
//	]
//}
