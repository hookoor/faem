package proto

var (
	Intents map[string]string
)

func DetectLocalIntent(text string) string {
	val, ok := Intents[text]
	if ok {
		return val
	}
	return ""
}

func initIntents() {
	Intents = make(map[string]string)
	Intents[Buttons.Menu.CallTaxi.T()] = Buttons.Menu.CallTaxi.D()
	Intents[Buttons.Menu.OrderFood.T()] = Buttons.Menu.OrderFood.D()
	Intents[Buttons.Menu.OrderFood.T()] = Buttons.Menu.OrderFood.D()
	Intents[Buttons.Actions.WrongDepartureAddress.T()] = Buttons.Actions.WrongDepartureAddress.D()
	Intents[Buttons.Actions.WrongArrivalAddress.T()] = Buttons.Actions.WrongArrivalAddress.D()
	Intents[Buttons.Actions.ChangeTarrif.T()] = Buttons.Actions.ChangeTarrif.D()
	Intents[Buttons.Actions.ChangePaymentType.T()] = Buttons.Actions.ChangePaymentType.D()
	Intents[Buttons.Actions.StartOrder.T()] = Buttons.Actions.StartOrder.D()
	Intents[Buttons.Actions.CancelOrder.T()] = Buttons.Actions.CancelOrder.D()
	Intents[Buttons.Actions.CreateTicket.T()] = Buttons.Actions.CreateTicket.D()
	Intents[Buttons.Actions.WrongAddress1.T()] = Buttons.Actions.WrongAddress1.D()
	Intents[Buttons.Actions.WrongAddress2.T()] = Buttons.Actions.WrongAddress2.D()
	Intents[Buttons.Actions.WrongAddress3.T()] = Buttons.Actions.WrongAddress3.D()
	Intents[Buttons.Actions.WrongAddress4.T()] = Buttons.Actions.WrongAddress4.D()
	Intents[Buttons.Actions.WrongAddress5.T()] = Buttons.Actions.WrongAddress5.D()
	Intents[Buttons.Actions.BackButton.T()] = Buttons.Actions.BackButton.D()
	Intents[Buttons.Actions.BackButton2.T()] = Buttons.Actions.BackButton.D()
	Intents[Buttons.Actions.BackButton3.T()] = Buttons.Actions.BackButton.D()
	Intents[Buttons.Actions.BackButton4.T()] = Buttons.Actions.BackButton.D()
	Intents[Buttons.Actions.CancelTicketCreation.T()] = Buttons.Actions.CancelTicketCreation.D()
	Intents[Buttons.Actions.RestoreOrder.T()] = Buttons.Actions.RestoreOrder.D()
	Intents[Buttons.Actions.YesAction.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction2.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction3.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction4.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction5.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction6.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction7.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction8.T()] = Buttons.Actions.YesAction.D()
	Intents[Buttons.Actions.YesAction9.T()] = Buttons.Actions.YesAction.D()

	Intents[Buttons.Actions.NoAction.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction2.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction3.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction4.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction5.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction6.T()] = Buttons.Actions.NoAction.D()
	Intents[Buttons.Actions.NoAction7.T()] = Buttons.Actions.NoAction.D()

	Intents[Buttons.Actions.HelpAction.T()] = Buttons.Actions.CreateTicket.D()
	Intents[Buttons.Actions.HelpAction2.T()] = Buttons.Actions.CreateTicket.D()
}
