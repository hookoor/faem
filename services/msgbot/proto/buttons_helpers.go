package proto

import (
	"fmt"
)

type TariffProto struct {
	ServiceUUID     string `json:"service_uuid"`
	ServiceImage    string `json:"service_image"`
	Name            string `json:"name"`
	Currency        string `json:"currency"`
	BonusPayment    int    `json:"bonus_payment"`
	MaxBonusPayment int    `json:"max_bonus_payment"`
	TotalPrice      int    `json:"total_price"`
}

func GetCreateTicketButton() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Consts.ButtonsDisplayLocation.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.CreateTicket.T(),
						Type: Consts.ButtonsTypes.Regular,
					},
				},
			},
		},
	}
}

//GetCancelOrderButton() отмена заказа
func GetCancelOrderButton() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Inline,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.CancelOrder.T(),
						Data: Buttons.Actions.CancelOrder.D(),
					},
				},
			},
		},
	}
}

//GetArrivalButtons возращает кнопки для статусу orders_arrival
func GetArrivalButtons() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.WrongArrivalAddress.T(),
						Type: Buttons.Type.Regular,
					},
					{
						Text: Buttons.Actions.ChangeTarrif.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.StartOrder.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
		},
	}
}

func GetFixAddressButtons() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.WrongAddress1.T(),
						Type: Buttons.Type.Regular,
					},
					{
						Text: Buttons.Actions.WrongAddress2.T(),
						Type: Buttons.Type.Regular,
					},
					{
						Text: Buttons.Actions.WrongAddress3.T(),
						Type: Buttons.Type.Regular,
					},
					{
						Text: Buttons.Actions.WrongAddress4.T(),
						Type: Buttons.Type.Regular,
					},
					{
						Text: Buttons.Actions.WrongAddress5.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.BackButton.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
		},
	}
}

func GetFixDepartureAddress() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Actions.WrongDepartureAddress.T(),
						Type: Consts.ButtonsTypes.Regular,
					},
				},
			},
		},
	}
}

func GetWellcomeButtons() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Menu.CallTaxi.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Menu.OrderFood.T(),
						Type: Buttons.Type.Regular,
					},
				},
			},
		},
	}
}

func GetContactButton() ButtonsSet {
	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons: []MsgKeyboardRows{
			{
				MsgButtons: []MsgButton{
					{
						Text: Buttons.Reply.NeedPhone.T(),
						Type: Buttons.Type.Contact,
					}}}}}
}

func ChangeServiceButtons(tarrifs []TariffProto) ButtonsSet {
	var buttons []MsgKeyboardRows
	var columns []MsgButton
	for i, _ := range tarrifs {
		columns = append(columns, MsgButton{
			Text: fmt.Sprintf("%d", i+1),
		})
	}

	buttons = append(buttons, MsgKeyboardRows{
		MsgButtons: columns,
	})

	buttons = append(buttons, MsgKeyboardRows{
		MsgButtons: []MsgButton{
			{
				Text: Buttons.Actions.BackButton.T(),
				Data: Buttons.Actions.BackButton.D(),
			},
		},
	})

	return ButtonsSet{
		DisplayLocation: Buttons.Display.Reply,
		Buttons:         buttons,
	}
}
