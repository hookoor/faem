package proto

var MsgTypes struct {
	TelegramMessage  Constant
	TelegramCallback Constant
	TelegramContact  Constant
	WhatsAppMessage  Constant
	WhatsAppContact  Constant
	Coordinates      Constant
	BrokerMessage    Constant
	Voice            Constant
}

type ButtonType struct {
	Text Constant
	Data Constant
}

//D(ata) возращает ключ кнопки
func (b *ButtonType) D() string {
	return b.Data.S()
}

//T(ext) возращает текст кнопки
func (b *ButtonType) T() string {
	return b.Text.S()
}

var Buttons struct {
	Menu struct {
		CallTaxi  ButtonType
		OrderFood ButtonType
	}
	Actions struct {
		WrongDepartureAddress   ButtonType
		WrongArrivalAddress     ButtonType
		ChangeTarrif            ButtonType
		ChangePaymentType       ButtonType
		StartOrder              ButtonType
		CancelOrder             ButtonType
		CancelPhoneVerification ButtonType
		CreateTicket            ButtonType
		WrongAddress1           ButtonType
		WrongAddress2           ButtonType
		WrongAddress3           ButtonType
		WrongAddress4           ButtonType
		WrongAddress5           ButtonType
		BackButton              ButtonType
		BackButton2             ButtonType
		BackButton3             ButtonType
		BackButton4             ButtonType
		CancelTicketCreation    ButtonType
		RestoreOrder            ButtonType
		YesAction               ButtonType
		YesAction2              ButtonType
		YesAction3              ButtonType
		YesAction4              ButtonType
		YesAction5              ButtonType
		YesAction6              ButtonType
		YesAction7              ButtonType
		YesAction8              ButtonType
		YesAction9              ButtonType
		NoAction                ButtonType
		NoAction2               ButtonType
		NoAction3               ButtonType
		NoAction4               ButtonType
		NoAction5               ButtonType
		NoAction6               ButtonType
		NoAction7               ButtonType
		HelpAction              ButtonType
		HelpAction2             ButtonType
	}
	Reply struct {
		NeedPhone ButtonType
	}
	Display struct {
		Inline Constant
		Reply  Constant
	}
	Type struct {
		Regular  Constant
		Contact  Constant
		Location Constant
	}
}

func initButtons() {
	//Action
	Buttons.Actions.WrongDepartureAddress.Text = "🚕 Адрес подачи не верный"
	Buttons.Actions.WrongDepartureAddress.Data = States.Taxi.Order.FixDeparture
	Buttons.Actions.WrongArrivalAddress.Text = "✏️ Изменить адрес назначения"
	Buttons.Actions.WrongArrivalAddress.Data = States.Taxi.Order.FixArrival

	Buttons.Actions.WrongAddress1.Text = "1"
	Buttons.Actions.WrongAddress1.Data = "1"
	Buttons.Actions.WrongAddress2.Text = "2"
	Buttons.Actions.WrongAddress2.Data = "2"
	Buttons.Actions.WrongAddress3.Text = "3"
	Buttons.Actions.WrongAddress3.Data = "3"
	Buttons.Actions.WrongAddress4.Text = "4"
	Buttons.Actions.WrongAddress4.Data = "4"
	Buttons.Actions.WrongAddress5.Text = "5"
	Buttons.Actions.WrongAddress5.Data = "5"

	Buttons.Actions.YesAction.Data = "yes_action"
	Buttons.Actions.YesAction.Text = "👍 Пожалуй, да"
	Buttons.Actions.YesAction2.Text = "да"
	Buttons.Actions.YesAction3.Text = "Да"
	Buttons.Actions.YesAction4.Text = "ок"
	Buttons.Actions.YesAction5.Text = "ОК"
	Buttons.Actions.YesAction6.Text = "Ок"
	Buttons.Actions.YesAction7.Text = "OK"
	Buttons.Actions.YesAction8.Text = "ok"
	Buttons.Actions.YesAction9.Text = "Ok"

	Buttons.Actions.NoAction.Data = "no_action"
	Buttons.Actions.NoAction.Text = "🚫 Не надо"
	Buttons.Actions.NoAction2.Text = "нет"
	Buttons.Actions.NoAction3.Text = "Нет"
	Buttons.Actions.NoAction4.Text = "НЕТ"
	Buttons.Actions.NoAction5.Text = "не"
	Buttons.Actions.NoAction6.Text = "Не"
	Buttons.Actions.NoAction7.Text = "НЕ"

	Buttons.Actions.HelpAction.Text = "Помощь"
	Buttons.Actions.HelpAction2.Text = "помощь"
	Buttons.Actions.HelpAction.Data = "help"

	Buttons.Actions.RestoreOrder.Text = "🌀 Продолжить оформление"
	Buttons.Actions.RestoreOrder.Data = "restore_order"

	Buttons.Actions.CancelTicketCreation.Text = "🔙 Не отправлять сообщение"
	Buttons.Actions.CancelTicketCreation.Data = "cancel_ticket_creation"

	Buttons.Actions.BackButton.Text = "🔙 Назад"
	Buttons.Actions.BackButton.Data = "back_button"
	Buttons.Actions.BackButton2.Text = "назад"
	Buttons.Actions.BackButton3.Text = "Назад"
	Buttons.Actions.BackButton4.Text = "НАЗАД"

	Buttons.Actions.ChangeTarrif.Text = "🚘 Изменить тариф"
	Buttons.Actions.ChangeTarrif.Data = States.Taxi.Order.ChangeService

	Buttons.Actions.ChangePaymentType.Text = "💳 Изменить cпособ оплаты"
	Buttons.Actions.ChangePaymentType.Data = States.Taxi.Order.PaymentMethod
	Buttons.Actions.StartOrder.Text = "🚕 Вызвать такси"
	Buttons.Actions.StartOrder.Data = States.Taxi.Order.ValidateBeforeStart

	Buttons.Actions.CancelOrder.Text = "❌ Отменить заказ"
	Buttons.Actions.CancelOrder.Data = States.Taxi.Order.Cancelled

	Buttons.Actions.CreateTicket.Text = "🙋 Написать в тех поддержку"
	Buttons.Actions.CreateTicket.Data = States.Taxi.CreateTicket

	//Текстовые кнопки
	Buttons.Menu.CallTaxi.Text = "🚕 Заказать Такси"
	Buttons.Menu.CallTaxi.Data = States.Taxi.Order.CreateDraft

	Buttons.Menu.OrderFood.Text = "🍕 Заказать Еду"
	Buttons.Menu.OrderFood.Data = States.Food.Order.Create

	//Reply кнопки
	Buttons.Reply.NeedPhone.Text = "Поделиться моим контактом"

	//Типы кнопок
	Buttons.Type.Regular = "regular"
	Buttons.Type.Contact = "contact"
	Buttons.Type.Location = "location"

	//Расположение кнопок
	Buttons.Display.Inline = "inline"
	Buttons.Display.Reply = "reply"

	//Message Types
	MsgTypes.TelegramMessage = "tlgrm_msg"
	MsgTypes.TelegramCallback = "tlgrm_callback"
	MsgTypes.TelegramContact = "tlgrm_contact"
	MsgTypes.BrokerMessage = "broker_msg"
	MsgTypes.Coordinates = "coordinates_msg"
	MsgTypes.Voice = "tlgrm_voice"
	MsgTypes.WhatsAppMessage = "whatsapp_msg"

}
