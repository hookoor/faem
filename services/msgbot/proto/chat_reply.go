package proto

type ChatMsg struct {
	OrderUUID string `json:"order_uuid"`
	Msg       string `json:"msg"`
}
