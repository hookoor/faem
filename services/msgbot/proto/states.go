package proto

import (
	"gitlab.com/faemproject/backend/faem/pkg/constants"
)

//States описываю все возможные типы контекста разговора, именно эти значения должны
//принимать входящий контексты и сами названия интентов, и они же используются в стейт машине
var States struct {
	NewOrder       Constant
	Welcome        Constant
	FallBackIntent Constant
	Taxi           struct { //такси
		CreateTicket         Constant //создать тикет
		SaveTicket           Constant //создать тикет
		CancelTicketCreation Constant //отмена создания тикета
		Order                struct { //этапы заказа

			CreateDraft   Constant //создание нового заказа
			Departure     Constant //откуда забрать
			Arrival       Constant //куда поедем
			FixDeparture  Constant //исправляем адрес
			FixArrival    Constant //исправляем адрес
			PaymentMethod Constant //изменить способ оплаты
			PaymentByCash Constant //оплата картой (?)
			ChangeService Constant //изменить тариф

			ValidateBeforeStart Constant //валидация заказа перед запуском
			OrderCreated        Constant //запуск заказа
			SmartDistribution   Constant //распределение
			FindingDriver       Constant //поиск авто
			OfferOffered        Constant //заказ предложен
			OfferCancelled      Constant //заказ отклонен водителем
			DriverAccepted      Constant //заказ принят
			DriverFounded       Constant //специальный статус в которые переводится того как нашли водителя

			OrderStart Constant //поиск авто
			Waiting    Constant //ожидание

			OnPlace  Constant //на месте
			OnTheWay Constant //выполнение заказа

			OrderPayment   Constant //оплата заказа
			Finished       Constant //заказ завершен
			Cancelled      Constant //заказ отменен
			CancelPromt    Constant //запрос на отмену
			DriverNotFound Constant //водитель не найден
			Unknown        Constant //заказ завершен

			NeedPhone       Constant
			SaveContact     Constant
			ValidateContact Constant
			RestoreOrder    Constant
		}
	}
	Unknown struct { //непонятные ситуации
		UnexpectedContact     Constant //когда внезапно получил контакт
		UnexpectedCoordinates Constant //когда внезапно получил контакт
	}

	Food struct {
		Order struct {
			Create Constant
		}
	}
}

func initStates() {
	//States taxi
	States.NewOrder = "new_order"
	States.Welcome = "welcome"
	States.Taxi.CancelTicketCreation = "cancel_ticket_creation"
	States.FallBackIntent = "Default Fallback Intent"
	States.Taxi.Order.CreateDraft = "taxi_order_create"
	States.Taxi.Order.Departure = "taxi_order_departure"
	States.Taxi.Order.Arrival = "taxi_order_arrival"
	States.Taxi.Order.FixDeparture = "taxi_order_fix-departure"
	States.Taxi.Order.FixArrival = "taxi_order_fix-arrival"
	States.Taxi.Order.PaymentMethod = "taxi_order_payment-method"
	States.Taxi.Order.PaymentByCash = "taxi_order_payment_cash"
	States.Taxi.Order.ChangeService = "taxi_order_change-service"
	States.Taxi.Order.NeedPhone = "taxi_order_need-phone"
	States.Taxi.Order.SaveContact = "taxi_order_save-contact"
	States.Taxi.Order.ValidateContact = "taxi_order_validate-contact"
	States.Taxi.Order.DriverFounded = "taxi_order_driver_founded"
	States.Taxi.CreateTicket = "create_ticket"
	States.Taxi.SaveTicket = "taxi_order_save_ticket"
	States.Taxi.Order.RestoreOrder = "taxi_order_restore"
	States.Taxi.Order.ValidateBeforeStart = "taxi_order_validate-before-start"
	States.Taxi.Order.OrderCreated = Constant(constants.OrderStateCreated)
	States.Taxi.Order.SmartDistribution = Constant(constants.OrderStateDistributing)
	States.Taxi.Order.OfferOffered = Constant(constants.OrderStateOffered)
	States.Taxi.Order.OfferCancelled = Constant(constants.OrderStateCancelled)
	States.Taxi.Order.CancelPromt = "taxi_order_cancell-promt"
	States.Taxi.Order.DriverAccepted = Constant(constants.OrderStateAccepted)
	States.Taxi.Order.FindingDriver = Constant(constants.OrderStateFree)
	States.Taxi.Order.OrderStart = Constant(constants.OrderStateStarted)
	States.Taxi.Order.OnTheWay = Constant(constants.OrderStateOnTheWay)
	States.Taxi.Order.OnPlace = Constant(constants.OrderStateOnPlace)
	States.Taxi.Order.Waiting = Constant(constants.OrderStateWaiting)
	States.Taxi.Order.OrderPayment = Constant(constants.OrderStatePayment)
	States.Taxi.Order.Finished = Constant(constants.OrderStateFinished)
	States.Taxi.Order.Cancelled = Constant(constants.OrderStateCancelled)
	States.Taxi.Order.DriverNotFound = Constant(constants.OrderStateDriverNotFound)

	States.Taxi.Order.Unknown = "unknown_state"

	States.Unknown.UnexpectedContact = "unexpected_contact"
	States.Unknown.UnexpectedCoordinates = "unexpected_coordinates"

	States.Food.Order.Create = "food_order_create"

	//States taxi
	States.Food.Order.Create = "food_order_create"
}
