package proto

import (
	"errors"
	"fmt"
	"math/rand"
)

var Answers struct {
	Errors    []Constant
	FallBacks []Constant
	Intents   map[Constant][]string
}

var TicketCreated = "Сообщение отправлено. Вам пришлют ответ."
var TryToArrivWhileDepartEmpty = "Произошла ошибка, давайте начнем с адреса откуда вас забрать."
var SameAddressesMessage = "Адрес назначения совпадает с адресом подачи"

var welcomeAnswer = []string{
	"И снова здарвствуйте 🤗\n\nВам доступны следующие команды:\n\n'такси' - для заказа такси\n'помощь' - написать сообщение разработчикам",
}

var fallBackAnswers = []string{
	"Я вообще не понял, о чем идет речь. Может, попробуем еще раз?\n\n%s\n\n'помощь' что бы обратиться в службу поддержки",
	"Мне непонятно. Возможно ли переформулировать о чем идет речь?\n\n%s\n\n'помощь' что бы обратиться в службу поддержки",
	"Чувствую себя идиотом в такие моменты 😬 я не понимаю вас\n\n%s\n\n'помощь' что бы обратиться в службу поддержки",
	"Человеческий язык очень сложный для меня - я не понял, о чем идет речь\n\n%s\n\n'помощь' что бы обратиться в службу поддержки",
	"Прости, что это значит? Мне непонятно\n\n%s\n\n'помощь' что бы обратиться в службу поддержки",
}

var errorsAnswers = []string{
	"Что-то пошло не так - мне очень жаль.\nПопробуем еще раз или скачайте наше приложение https://faem.page.link/wapp.\n\n%s",
	"Ненавижу ошибки а-а-а-а, но они случаются, ведь я пока учусь.\nМожно заказать через наше приложение https://faem.page.link/wapp или попробовать еще раз:\n\n%s",
	"Не ошибается только тот, кто ничего не делает (( у меня ошибка\nДумаю, стоит попробовать еще раз или закажите через наше приложение https://faem.page.link/wapp:\n\n%s",
}

var oldErrorsAnswers = []string{
	"К сожалению, произошла ошибка (( \nВсе данные я передал разработчикам - скоро мы это исправим. \n",
	"Очень жаль, но произошла ошибка. #$~$@%$%^&*()!@#$%^&*()_ \nВся информация уже у разработчиков, я прослежу, чтобы ее решили\n",
	"Оооо нет - ошибка. Больше всего на свете ненавижу ошибки.\nЯ передал разработчикам все информацию, они, наверно, уже исправляют\n",
}

var TaxiOrderCreated = []string{
	"Создаем заказ такси 🚕\n\nОтправьте мне адрес, откуда вас забрать, или пришлите свою геопозицию",
	"🚕 🚕 вези, вези...\n\nПришлите адрес, откуда вас забрать. А еще можно прислать геопозицию.",
	"Заказ такси 🚕\n\n...отличный выбор, на такси, у нас лучшие цены в городе.\n\nНа какой адрес прислать авто?",
}

var tempUnavailable = []string{
	"Мне жаль, но эта функция пока недоступна. 🧘",
	"Мы еще не успели это сделать. 🤔",
	"Это пока не работает 😒 скоро заработает",
}

var taxiOrderStarted = []string{
	"Понеслась... ищу для вас ближайшее авто 😇. \n\nДля отмены пришлите - отменить заказ",
	"Заказ создан 😉 ищу авто\n\nДля отмены пришлите - отменить заказ",
}

//var TaxiOrderChangeService = []string{
//	"Вам доступны следующие тарифы:\n",
//}

var TaxiOrderPaymentMethod = tempUnavailable

var ArrivalFixed = []string{
	"🚕 Адрес подачи: %s\n\n🚕 Адрес назначения: %s\n\nСтоимость поездки: %v₽ (%s)\n\nСпособ оплаты: %s\n",
}
var ChangeService = []string{
	"🚕 Адрес подачи: %s\n\n🚕 Адрес назначения: %s\n\n💎 %s - %v₽ (%s)\n\nДоступные тарифы:\n\n%s\n\nВыберете услугу:",
}

var DepartureFixed = []string{
	"Адрес теперь исправлен, как насчет:\n\n%s\n\nЕсли все верно пришлите адрес назначения:",
	"Новый адрес подачи:\n\n%s\n\nКуда направимся:",
}

var ValidateBeforeCreate = []string{
	"Сейчас кое-что проверю",
}

var TaxiFixDepartureAddress = []string{
	"Адрес: %s неверный?\n\nВведите адрес подачи повторно или выберите один из предлагаемых:\n\n%s",
}

var TaxiFixArrivalAddress = []string{
	"Адрес: %s неверный?\n\nВведите адрес назначения повторно или выберите один из предлагаемых:\n\n%s",
}

var FoodOrderCreate = []string{
	"Мне жаль 😔 но этот раздел еще в разработке. \nМы планируем его запустить в ближайшее время. Я напишу, как он будет готов.\n\nПока можно заказать такси.",
	"эээээ 😶 заказ еды еще не включили.\nОн запустится в начале августа, я пришлю уведомление, как он будет готов.\nА пока можно заказать такси)",
}

var NeedPhone = []string{
	"У меня нет вашего номера телефона. Пришлите его, пожалуйста, чтобы начать поиск авто",
}

var UnexpectedContact = []string{
	"Дико извиняюсь, но я не понимаю, что мне делать с этим контактом?\nВернемся к диалогу?",
	"А зачем мне этот контакт, я не понимаю... 😬",
}

var UnexpectedCoordinates = []string{
	"Я не очень понимаю, что мне делать с этими координатами...😬",
	"Коориднаты? А зачем? Не пойму, что с ними делать...",
}

var driverFounded = []string{
	"Ура, мы нашли для вас машину! 🎉🎉🎉\n\nЧерез %v минут к вам подъедет %s %s номера %s \n\nТелефон водителя: %s\n\nПожалуйста, ожидайте...",
	"Получилось! 🎉🎉🎉\n\nЧерез %v минут к вам подъедет %s %s номера %s \n\nТелефон водителя: %s\n\nПожалуйста, ожидайте...",
}
var onTheWay = []string{
	"Поехали! Следующая остановка %s.\n\nХорошей поездки.",
	"Следующая остановка %s.\n\nПоехали!",
}

var driverOnPlace = []string{
	"Вас ожидает водитель, пожалуйста, выходите",
	"Водитель на месте. Выходите",
}

var orderFinished = []string{
	"Ваш заказ завершен! Спасибо \n\nДля создания нового заказа напишите 'Заказать такси' или просто 'Привет'",
	"Приехали, будем рады видеть вас снова. \n\nДля создания нового заказа напишите 'Заказать такси' или просто 'Привет'",
}

var orderCancelled = []string{
	"Заказ отменен, будем рады видеть вас снова.\nДля создания нового заказа нажмите /start или просто напишите привет",
	"Жаль, конечно, но ваш заказ отменен 🤷\n Чтобы создать новый заказ, нажмите /start или напишите привет",
}

var driverNotFounded = []string{
	"Очень жаль, но мы не нашли водителя и отменили ваш заказ",
	"О нет! Ваш заказ отменен: мы просто не нашли водителя 😔",
}

var createTicketText = []string{
	"Пришлите текст, а я его отправлю ✉️️ разработчикам.\n\nВы получите ответ в ближайшее время.\n\n'назад' - отмены",
	"Опишите свою проблему текстом, а я его отправлю ✉️️ своим создателям и прослежу, чтобы они ответили.\n\n'назад' - отменит создание обращения",
}

var saveTicketText = []string{
	"Движемся дальше.\n\n%s\n\nДля отмены оформления заказа пришлите 'Отменить заказ'",
	"Идем дальше.\n\n%s\n\nДля отмены оформления заказа пришлите 'Отменить заказ'",
}

var arrivalState = []string{
	"🚕 Адрес подачи: %s\n🚕 Адрес назначения: %s\n\nСтоимость поездки: %v₽ (%s)\n\nСпособ оплаты: %s",
}

var departureState = []string{
	"🚕 Адрес подачи: %s\n\nКуда едем?",
}

var saveContact = []string{
	"На указанный вами номер отправленно СМС кодом верификации.\n\nВведите полученный код или кнопку `Назад`",
}

///

//фразы и словари
type PhrasesDict struct {
	dict   []string
	phrase Constant
}

func initAnswers() {
	Answers.Intents = make(map[Constant][]string)

	//ошибки
	for _, v := range errorsAnswers {
		Answers.Errors = append(Answers.Errors, Constant(v))
	}
	for _, v := range fallBackAnswers {
		Answers.FallBacks = append(Answers.FallBacks, Constant(v))
	}

	pDic := []PhrasesDict{
		{
			//создание заказа такси
			dict:   welcomeAnswer,
			phrase: States.Welcome,
		},
		{
			//создание заказа такси
			dict:   TaxiOrderCreated,
			phrase: States.Taxi.Order.CreateDraft,
		},
		{
			//исправление адреса откуда забрать
			dict:   TaxiFixDepartureAddress,
			phrase: States.Taxi.Order.FixDeparture,
		},
		{
			dict:   TaxiFixArrivalAddress,
			phrase: States.Taxi.Order.FixArrival,
		},
		{
			dict:   ArrivalFixed,
			phrase: States.Taxi.Order.Arrival,
		},
		{
			dict:   DepartureFixed,
			phrase: States.Taxi.Order.Departure,
		},
		{
			//изменить услугу
			dict:   ChangeService,
			phrase: States.Taxi.Order.ChangeService,
		},
		{
			//изменение способа оплаты
			dict:   TaxiOrderPaymentMethod,
			phrase: States.Taxi.Order.PaymentMethod,
		},
		{
			//мы ждем номер для авторизации
			dict:   NeedPhone,
			phrase: States.Taxi.Order.NeedPhone,
		},
		{
			//внезапно прислали контакт
			dict:   UnexpectedContact,
			phrase: States.Unknown.UnexpectedContact,
		},
		{
			//внезапно прислали координаты
			dict:   UnexpectedCoordinates,
			phrase: States.Unknown.UnexpectedCoordinates,
		},

		{
			//создание заказа еды
			dict:   FoodOrderCreate,
			phrase: States.Food.Order.Create,
		},
		{
			dict:   taxiOrderStarted,
			phrase: States.Taxi.Order.OrderCreated,
		},
		{
			dict:   driverFounded,
			phrase: States.Taxi.Order.OrderStart,
		},

		{
			dict:   onTheWay,
			phrase: States.Taxi.Order.OnTheWay,
		},
		{
			dict:   driverOnPlace,
			phrase: States.Taxi.Order.OnPlace,
		},
		{
			dict:   orderFinished,
			phrase: States.Taxi.Order.Finished,
		},
		{
			dict:   orderCancelled,
			phrase: States.Taxi.Order.Cancelled,
		},
		{
			dict:   driverNotFounded,
			phrase: States.Taxi.Order.DriverNotFound,
		},
		{
			dict:   createTicketText,
			phrase: States.Taxi.CreateTicket,
		},
		{
			dict:   saveTicketText,
			phrase: States.Taxi.SaveTicket,
		},
		{
			dict:   arrivalState,
			phrase: States.Taxi.Order.Arrival,
		},
		{
			dict:   departureState,
			phrase: States.Taxi.Order.Departure,
		},
		{
			dict:   saveContact,
			phrase: States.Taxi.Order.SaveContact,
		},
	}

	for _, d := range pDic {
		for _, e := range d.dict {
			Answers.Intents[d.phrase] = append(Answers.Intents[d.phrase], e)
		}
	}
}

func GetIntentText(state Constant) (string, error) {
	if val, ok := Answers.Intents[state]; ok {
		if len(Answers.Intents[state]) == 1 {
			return val[0], nil
		}
		return val[rand.Intn(len(Answers.Intents[state])-1)], nil
	}
	ans := fmt.Sprintf("intent data to founded in dictionary. State: %s", state.S())
	return "Я не знаю, как ответить (( мне очень жаль", errors.New(ans)
}

//GetErrorText возвращает случайные текст с ошибкой
func GetErrorText(currentState string) string {
	template := Answers.Errors[rand.Intn(len(errorsAnswers)-1)].S()
	result := fmt.Sprintf(template, ExpectState(currentState))
	return result
}

//GetErrorText возвращает случайные текст с ошибкой
func GetFallBackText(currentState string) string {
	template := Answers.FallBacks[rand.Intn(len(fallBackAnswers)-1)].S()
	result := fmt.Sprintf(template, ExpectState(currentState))
	return result
}

//возращает текст который уведомляет о том что мы ждем от клиента
func ExpectState(state string) string {
	switch state {
	case States.Welcome.S():
		return "Пришлите текстом: Такси или Еда или для оформления заказа"
	case States.Taxi.Order.CreateDraft.S():
		return "Давайте начнем с адреса. Откуда вас забрать?"
	case States.Taxi.Order.Departure.S():
		return "Пожалуйста, пришлите мне текстом адрес, куда поедем?"
	case States.Taxi.Order.FixDeparture.S():
		return "Веберите один из предложенных адресов или пришлите новый"
	case States.Taxi.Order.FixArrival.S():
		return "Веберите один из предложенных адресов или пришлите новый"
	case States.Taxi.Order.Arrival.S():
		return "Вы можете вызвать такси, изменить адрес назначения или тариф. Для отмены заказа вы можете создать заказ, а уже потом его отменить"
	case States.Taxi.Order.SaveContact.S():
		return "Код верификации не верный"
	case States.Taxi.Order.ChangeService.S():
		return "Попробуйте выбрать один из предложенных выше классов обслуживания"
	case States.Taxi.Order.NeedPhone.S():
		return "Я ожидаю, что вы пришлете свой номер телефона в формате +79998887766 или контакт, для регистрации"
	case States.Taxi.Order.OrderCreated.S():
		return "Ваш заказ создан. Возможно, какая-то техническая проблема - я все передам разработчикам"
	case States.Taxi.Order.FindingDriver.S(), States.Taxi.Order.SmartDistribution.S(), States.Taxi.Order.OfferOffered.S():
		return "Мы ищем для вас авто. Если хотите отменить, нажмите `❌ Отменить заказ`, в одном из предыдущих сообщений"
	default:
		return ""
	}
}
