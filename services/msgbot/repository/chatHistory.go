package repository

import (
	"context"
	"errors"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"time"
)

func (p *Pg) SaveChatHistory(ctx context.Context, chatHistory *models.ChatHistory) error {
	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
	defer cancel()
	_, err := p.Db.ModelContext(ctx, chatHistory).Insert()
	return err
}

func (p *Pg) GetChatOrders(clientMsgID string, state string, limit int, offset int) ([]models.ChatOrder, error) {
	var chatOrders []models.ChatOrder
	query := p.Db.Model(&chatOrders)
	if clientMsgID != "all" {
		query = query.Where("client_msg_id=?", clientMsgID)
	}
	if state != "all" {
		query = query.Where("state=?", state)
	}
	if limit > 0 {
		query = query.Limit(limit)
	}
	if offset > 0 {
		query = query.Offset(offset)
	}
	subquery := p.Db.Model((*models.UniqueOrder)(nil)).
		Column("order_uuid").
		ColumnExpr("count(*) AS number_of_msgs").
		Group("order_uuid")
	err := query.Column("cho.order_uuid", "cho.state", "cho.source").
		Column("cho.client_msg_id", "cho.created_at", "sq.number_of_msgs").
		Join("JOIN (?) sq ON cho.order_uuid = sq.order_uuid", subquery).
		Order("cho.created_at DESC").Select()
	if err != nil {
		return nil, err
	}
	if chatOrders == nil {
		return nil, errors.New("requested chat orders do not exist")
	}
	return chatOrders, err
}

func (p *Pg) GetChatByOrderUUID(orderUUID string) ([]models.ChatHistory, error) {
	var msgHist []models.ChatHistory
	err := p.Db.Model(&msgHist).Where("order_uuid=?", orderUUID).Order("created_at DESC").Select()
	if msgHist == nil {
		return nil, errors.New("the chat on the specified order uuid does not exist")
	}
	return msgHist, err
}

func (p *Pg) GetMsgByID(msgID string) (models.ChatHistory, error) {
	var msg models.ChatHistory
	err := p.Db.Model(&msg).Where("msg_id=?", msgID).Select()
	return msg, err
}

func (p *Pg) GetChatInfoByOrderUUID(orderUUID string) (models.ChatInfo, error) {
	var chatInfo models.ChatInfo
	err := p.Db.Model(&chatInfo).
		Where("order_uuid=?", orderUUID).
		Select()
	return chatInfo, err
}

//func (p *Pg) SaveDriverData(ctx context.Context, order structures.Order) (models.LocalOrders, error) {
//	ctx, cancel := context.WithTimeout(ctx, 30*time.Second)
//	defer cancel()
//
//	var localOrder models.LocalOrders
//
//	updatedOrder := models.OrderCRM{
//		Order:       order,
//		ServiceUUID: order.Service.UUID,
//	}
//
//	_, err := p.Db.ModelContext(ctx, &localOrder).
//		Set("order_json = ?", updatedOrder).
//		Where("order_uuid = ?", order.UUID).
//		Returning("*").
//		Update()
//
//	if err != nil {
//		return localOrder, err
//	}
//	return localOrder, nil
//}
