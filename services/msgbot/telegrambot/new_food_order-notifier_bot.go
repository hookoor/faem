//Telegram Bot Subscriber
package telegrambot

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/msgbot/handler"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strconv"
)

type (
	NotifierBotClient struct {
		Bot   *tgbotapi.BotAPI
		Token string
	}
	NotifierBotSubscriber struct {
		Bot     *tgbotapi.BotAPI
		Handler *handler.Handler
	}
)

func (b *NotifierBotClient) Init() error {
	var err error
	b.Bot, err = tgbotapi.NewBotAPI(b.Token)
	if err != nil {
		return errors.Wrap(err, "NotifierBot init: ")
	}
	return nil
}

func (b *NotifierBotClient) DeleteMsg(chatID int64, msgID int) error {
	cfg := tgbotapi.NewDeleteMessage(chatID, msgID)
	_, err := b.Bot.Send(cfg)
	return err
}

func (b *NotifierBotClient) UpdateKeyboard(chatID int64, msgID int, newKeyborad proto.ButtonsSet) error {

	var markup tgbotapi.InlineKeyboardMarkup
	if len(newKeyborad.Buttons) == 0 {
		markup = tgbotapi.InlineKeyboardMarkup{InlineKeyboard: make([][]tgbotapi.InlineKeyboardButton, 0)}
	} else {
		markup, _ = getInlineKeyboardMarkup(newKeyborad.Buttons)
	}
	cfg := tgbotapi.NewEditMessageReplyMarkup(chatID, msgID, markup)
	_, err := b.Bot.Send(cfg)
	return err
}

func (b *NotifierBotClient) UpdateMessage(chatID int64, msgId int, msg string) (proto.SendedMessage, error) {
	var tlgrMsg proto.SendedMessage
	cfg := tgbotapi.NewEditMessageText(chatID, msgId, msg)
	sendedMsg, err := b.Bot.Send(cfg)
	if err != nil {
		return tlgrMsg, errors.Wrap(err, "failed to send message")
	}
	tlgrMsg.Id = strconv.Itoa(sendedMsg.MessageID) // TODO: add nil handler
	return tlgrMsg, err
}

// SendMessage -
func (b *NotifierBotClient) SendMessage(chatID int64, msg string, keyboard ...proto.ButtonsSet) (proto.SendedMessage, error) {
	var keys proto.ButtonsSet
	var tlgrMsg proto.SendedMessage
	tlgMsgSender := tgbotapi.NewMessage(chatID, msg)
	if len(keyboard) > 0 && keyboard[0].DisplayLocation.S() != "" {
		keys = keyboard[0]
		if keys.DisplayLocation == "" {
			return tlgrMsg, errors.New("Empty display location for telegram buttons")
		}
		if keys.DisplayLocation == proto.Consts.ButtonsDisplayLocation.Inline || keys.DisplayLocation == proto.Buttons.Display.Inline {
			markup, err := getInlineKeyboardMarkup(keys.Buttons)
			if err != nil {
				return tlgrMsg, errpath.Err(err)
			}
			tlgMsgSender.ReplyMarkup = markup
		}
		if keys.DisplayLocation == proto.Consts.ButtonsDisplayLocation.Reply || keys.DisplayLocation == proto.Buttons.Display.Reply {
			markup, err := getReplyKeyboardMarkup(keys.Buttons)
			if err != nil {
				return tlgrMsg, errpath.Err(err)
			}
			tlgMsgSender.ReplyMarkup = markup
		}
	} else {
		tlgMsgSender.ReplyMarkup = tgbotapi.NewRemoveKeyboard(true)
	}

	tlgMsgSender.ParseMode = "HTML"
	sendedMsg, err := b.Bot.Send(tlgMsgSender)
	if err != nil {
		return tlgrMsg, err
	}
	tlgrMsg.Id = strconv.Itoa(sendedMsg.MessageID)
	return tlgrMsg, nil
}

// SendOperatorMessage -
func (b *NotifierBotClient) SendOperatorMessage(chatID int64, msg string) error {
	var err error
	msg = string(proto.Consts.MsgSources.Operator) + ":\n" + msg
	tlgMsgSender := tgbotapi.NewMessage(chatID, msg)

	_, err = b.Bot.Send(tlgMsgSender)
	if err != nil {
		return err
	}
	return nil
}

//Init Telegram bot subscriber
func (s *NotifierBotSubscriber) Init() error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := s.Bot.GetUpdatesChan(u)
	if err != nil {
		return errors.Wrap(err, "NotifierBot: ")
	}

	go s.handleIncomeMsg(&updates)
	return nil
}

func (s *NotifierBotSubscriber) handleIncomeMsg(updChan *tgbotapi.UpdatesChannel) {
	for update := range *updChan {
		msgJob := s.Handler.Jobs.GetJobQueue(handler.JobQueueNameMsgs, handler.JobQueueLimitMsgs)
		//создаем очередь обработки сообщений, что бы все обрабатывалось последовательно
		var id int
		if update.CallbackQuery != nil {
			id = update.CallbackQuery.Message.MessageID
		} else if update.Message != nil {
			id = update.Message.MessageID
		}

		_ = msgJob.Execute(id, func() error {
			//ctx := context.Background()
			//s.Handler.HandleIncomeTelegramMsg(ctx, &update)
			//s.Handler.TelegramMsgHadler(ctx, &update)
			//err := s.Handler.NotifierClient.SendMessage(update.Message.Chat.ID, update.Message.Text)
			//if err != nil {
			//	fmt.Printf("%+v\n", err)
			//} -410864343
			logs.Eloger.WithFields(logrus.Fields{
				"Bot":     "NotifierBot",
				"chat-id": update.Message.Chat.ID,
				"text":    update.Message.Text,
			}).Info()
			return nil
		})
	}
}
