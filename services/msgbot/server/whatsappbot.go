package server

import (
	"context"
	"github.com/labstack/echo/v4"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/msgbot/handler"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"net/http"
	"reflect"
	"time"
)

var (
	httpClient = http.Client{
		Timeout: 30 * time.Second,
	}
)

func (r *Rest) WhatsAppIncome(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "handling income message")

	var waMsg proto.WAIncomingMsg
	if err := c.Bind(&waMsg); err != nil {
		log.WithField("reason", "binding incoming arguments").Error(err)
		res := logs.OutputRestError("can't bind incoming arguments", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// int -> str      t := strconv.Itoa(123)
	// str -> int      value, _ := strconv.ParseInt("", 0, 64) // TODO: add handler

	if (!reflect.DeepEqual(waMsg, proto.WAIncomingMsg{})) {
		msgJob := r.Handler.Jobs.GetJobQueue(handler.JobQueueNameMsgs, handler.JobQueueLimitMsgs)
		//создаем очередь обработки сообщений, что бы все обрабатывалось последовательно
		rand := int(time.Now().Unix())
		_ = msgJob.Execute(rand, func() error {
			ctx := context.Background()
			r.Handler.WhatsAppMsgHandler(ctx, &waMsg)
			return nil
		})
	}
	return c.JSON(http.StatusOK, "OK")
}
