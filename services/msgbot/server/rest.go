package server

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"

	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/msgbot/handler"
)

const (
	apiPrefix = "/api/v2"
)

type Rest struct {
	Router  *echo.Echo
	Handler *handler.Handler
}

// Route defines all the application rest endpoints
func (r *Rest) Route() {
	web.UseHealthCheck(r.Router)
	r.Router.Use(middleware.CORS())
	r.Router.Use(middleware.Recover())

	g := r.Router.Group(apiPrefix)

	g.POST("/whatsapp", r.WhatsAppIncome)

	//g.GET("/hello", r.Hello)
	//g.GET("/version", r.CurrentVersion)

	//erik
	g.GET("/chat/orders", r.ChatOrders)
	g.GET("/chat", r.ChatByOrderUUID)
	g.POST("/chat", r.Reply)
}
