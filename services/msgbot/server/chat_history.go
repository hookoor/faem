package server

import (
	"encoding/json"
	"errors"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"net/http"
)

func (r *Rest) ChatOrders(c echo.Context) error {
	clientMsgID := c.QueryParam("client_msg_id")
	state := c.QueryParam("state")
	from := c.QueryParam("from")
	to := c.QueryParam("to")
	chatOrders, err := r.Handler.ChatOrdersHandler(clientMsgID, state, from, to)
	if err != nil {
		res := logs.OutputRestError("error getting chat orders", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, chatOrders)
}

func (r *Rest) ChatByOrderUUID(c echo.Context) error {
	orderUUID := c.QueryParam("order_uuid")
	if orderUUID == "" {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event": "getting chat by order uuid",
			}).Error("order uuid is empty")
		res := logs.OutputRestError("failed to parse the request", errors.New("can't provide chat by order uuid, order uuid field is empty"))
		return c.JSON(http.StatusBadRequest, res)
	}
	msgHist, err := r.Handler.ChatByOrderUUIDHandler(orderUUID)
	if err != nil {
		res := logs.OutputRestError("error getting chat by order uuid", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, msgHist)
}

func (r *Rest) Reply(c echo.Context) error {
	var supportMsg proto.ChatMsg
	err := json.NewDecoder(c.Request().Body).Decode(&supportMsg)
	if err != nil {
		logs.LoggerForContext(c.Request().Context()).
			WithFields(logrus.Fields{
				"event": "getting support msg from request body",
			}).Error(err)
		res := logs.OutputRestError("error getting support msg", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	chatInfo, sentMsgID, msgSendingTime, err := r.Handler.SendingSupportMsgHandler(supportMsg)
	if err != nil {
		res := logs.OutputRestError("error sending support msg", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = r.Handler.SavingSupportMsgHandler(chatInfo, supportMsg, sentMsgID, msgSendingTime)
	if err != nil {
		res := logs.OutputRestError("error saving support msg", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, supportMsg)
}
