package models

import (
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

type LocalUsers struct {
	tableName            struct{}               `sql:"chat_users"`
	ID                   int                    `json:"id"`
	ClientMsgID          string                 `json:"client_msg_id"`
	Source               string                 `json:"source"`
	Phone                string                 `json:"phone"`
	CrmUUID              string                 `json:"crm_uuid"`
	SourceMsgContactdata proto.MessangerContact `json:"source_msg_contactdata"` //контактные данные прямо из мессенджера
	CreatedAt            time.Time              `json:"created_at"`
	UpdatedAt            time.Time              `json:"updated_at"`
}

type LocalOrders struct {
	tableName   struct{}         `sql:"chat_orders"`
	ID          int              `json:"id"`
	OrderUUID   string           `json:"order_uuid"`
	OrderJSON   OrderCRM         `json:"order_json"`
	State       string           `json:"state"`
	ClientMsgID string           `json:"client_msg_id"` // ChatMsgId(значение от)
	ClientID    int              `json:"client_id"`
	Source      string           `json:"source"`
	ChatMsgId   string           `json:"chat_msg_id"` // id чата (переписки)
	OrderPrefs  OrderPreferences `json:"order_prefs"`
	CreatedAt   time.Time        `json:"created_at"`
	UpdatedAt   time.Time        `json:"updated_at"`
}

type Coordinates struct {
	Lat float64 `json:"lat"`
	Lon float64 `json:"lon"`
}

type OrderPreferences struct {
	Lon float64 `json:"long"`
	Lat float64 `json:"lat"`

	TelegramOrderMsgID int `json:"tlgrm_order_msg_id"` //ID сообщения с данными о заказе
	//в некоторых случаях использую эту структуру что бы передавать из брокера данные
	//в нужный нам handler
	MsgsIDs     map[string]int `json:"msgs_ids"` //мапа в которой храним IDшники сообщений определенных статусов
	Service     string         `json:"service"`  //если пользователем выбран сервис
	Coordinates Coordinates    `json:"coordinates"`
	//CreateTicketPrevStatus string              `json:"create_ticket_prev_status"` //когда создаем тикет надо сохранить предыдущий статус, что бы в него вернуться
	//CreateTicketPrevText   string              `json:"create_ticket_prev_text"`   //а еще сохраняем предыдущий текст, с той же целью
	//REFACTORED NEXT
	DetectedIntent    string              `json:"intent_detected"`     //интент который мы определили
	IntentSteps       []string            `json:"intent_steps"`        //последние 10 шагов
	FixAddressVariant int                 `json:"fix_address_variant"` //сюда запишем варианты адресов при исправлении
	Tariffs           []proto.TariffProto `json:"tariffs"`             //при смене адреса результаты тарифа пишем сюда
	JumpToState       string              `json:"jump_to_state"`
	NewState          string              `json:"new_state"`
	DepartureVariants []structures.Route  `json:"departure_variants"` //варианты адресов подачи
	ArrivalVariants   []structures.Route  `json:"arrival_variants"`   //варианты адресов назначения
	MsgReciever       string              `json:"msg_reciever"`       //получатель сообщения (может быть сама система, служба поддержки или водитель)

	Registrations Registrations `json:"registrations"` //структура связанная с регистрацией пользователя
}

type Registrations struct {
	ContactData   proto.MessangerContact `json:"contact_data"` //контактные данные пользователя
	RegRequest    ClientRegistrRequest   `json:"client_registr_request"`
	RegResponse   ClientRegistrResponse  `json:"client_registr_response"`
	TokenResponse TokenResponse          `json:"token_response"`
}

//ClientRegistrRequest отправляем для регистрации пользователя
type ClientRegistrRequest struct {
	DeviceID string `json:"device_id"`
	Phone    string `json:"phone"`
}

//ResponseStruct ответ который получаем на регистрацию
type ClientRegistrResponse struct {
	Code            int    `json:"code"`
	Msg             string `json:"message"`
	NextRequestTime int64  `json:"next_request_time"`
}

//TokenResponse ответ после валидации (токен)
type TokenResponse struct {
	Token                  string `json:"token"`
	ClientUUID             string `json:"client_uuid"`
	RefreshToken           string `json:"refresh_token"`
	RefreshTokenExpiration int64  `json:"refresh_expiration"`
}

type OrderCRM struct {
	structures.Order
	ServiceUUID string `json:"service_uuid"`
}

func NewMsgOrder(user LocalUsers, chatId string, source string) LocalOrders {
	uuid, _ := uuid.NewV4()
	var locOrder = LocalOrders{
		Source:      source,
		OrderUUID:   uuid.String(),
		State:       string(proto.States.NewOrder),
		ClientID:    user.ID,
		ClientMsgID: user.ClientMsgID,
		ChatMsgId:   chatId,
	}
	locOrder.OrderJSON = newGlobalOrder(locOrder.OrderUUID, &user)

	return locOrder
}

func newGlobalOrder(uuid string, user *LocalUsers) OrderCRM {
	var newOrder OrderCRM
	newOrder.UUID = uuid
	newOrder.Source = "msgbot"
	newOrder.CallbackPhone = user.Phone
	com := "Заказ инициирован сервисом чат-бота"
	newOrder.Comment = &com
	newOrder.Client.TelegramID = user.ClientMsgID
	newOrder.Client.MainPhone = user.Phone
	newOrder.Client.UUID = user.CrmUUID
	newOrder.CreatedAt = time.Now()
	newOrder.Source = constants.OrderSourceTelegram

	return newOrder
}

// ShortTariff short tariff data from CRM
type ShortTariff struct {
	proto.TariffProto
}
