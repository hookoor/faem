package models

import "time"

type ChatOrder struct {
	tableName    struct{}  `sql:"chat_orders,alias:cho"`
	OrderUUID    string    `sql:"order_uuid"`
	State        string    `sql:"state"`
	Source       string    `sql:"source"`
	ClientMsgID  string    `sql:"client_msg_id"`
	CreatedAt    time.Time `sql:"created_at"`
	NumberOfMsgs uint32    `sql:"number_of_msgs"`
}

type UniqueOrder struct {
	tableName    struct{} `sql:"chat_history"`
	OrderUUID    string   `sql:"order_uuid"`
	NumberOfMsgs uint32   `sql:"number_of_msgs"`
}

type ChatInfo struct {
	tableName struct{} `sql:"chat_orders"`
	State     string   `sql:"state"`
	Source    string   `sql:"source"`
	ChatID    string   `sql:"chat_msg_id"`
}
