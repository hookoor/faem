package models

import (
	"time"
)

type ChatHistory struct {
	tableName   struct{}    `sql:"chat_history"`
	Source      string      `json:"source"`
	Sender      string      `json:"sender"`
	Reciever    string      `json:"reciever"`
	OrderUUID   string      `json:"order_uuid"`
	State       string      `json:"state"`
	TypeField   string      `json:"type" sql:"type"`
	ClientUUID  string      `json:"client_uuid"`
	Text        string      `json:"text"`
	ClientMsgID string      `json:"client_msg_id"`
	ChatID      string      `json:"chat_id"`
	MsgID       string      `json:"msg_id"`
	Payload     ChatPayload `json:"payload"`
	CreatedAt   time.Time   `json:"created_at"`
}

type ChatPayload struct {
}

//source TEXT, -- ID источник заказа
//sender TEXT, -- отправить
//reciever TEXT, -- получатель
//order_uuid TEXT, -- идетификатор заказа
//state TEXT, -- статус заказа
//type TEXT, -- тип сообщения
//client_uuid TEXT, -- тип сообщения
//text TEXT, -- само сообщени
//client_msg_id TEXT, -- id сообщения
//chat_id TEXT, -- id сообщения
//payload jsonb, -- доп инфа
//created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT CURRENT_TIMESTAMP
