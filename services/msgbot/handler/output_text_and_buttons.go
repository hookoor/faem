package handler

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

func outputTextAndButtons(msg *models.ChatMsgFull) (string, proto.ButtonsSet, int) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "generate text",
		"state": msg.Order.State,
	})
	log.Debug(msg.FSM.Current())

	var templateText string
	var err error

	//берем шаблон из интента DF или справочника внутри системы
	if msg.Type == proto.MsgTypes.TelegramMessage.S() &&
		msg.Order.State != proto.States.Taxi.SaveTicket.S() &&
		msg.FSM.Current() != proto.States.Taxi.SaveTicket.S() {
		templateText = msg.DFAnswer.Answer
	} else {
		//если это колбек то берет из справочника
		templateText, err = proto.GetIntentText(proto.Constant(msg.State))
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "cant get intent text",
			}).Error(err)
		}
	}

	//смешиваем полученный ответ с данными заказа
	answerText, err := mixedOrderDataToAnswer(templateText, msg)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "using template to get answer",
			"type":   "",
		}).Error(err)
	}

	//получаем кнопки для данного сообщения
	answerButtons := getAnswerButtons(msg)

	//новое сообщение или обновляем существующее
	var msgId int
	//msgId = newOrUpdateMessage(msg)

	return answerText, answerButtons, msgId
}
