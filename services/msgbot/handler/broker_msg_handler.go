package handler

import (
	"context"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strconv"
	"time"
)

const (
	skipStateConstant = "skip it"
)

//BrokerMsgHandler специальный хендлер для обработки сообщений из брокера
func (h *Handler) BrokerMsgHandler(ctx context.Context, order *models.LocalOrders, newState proto.Constant) error {
	var err error
	var msg models.ChatMsgFull
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "handling broker message",
		"orderUUID": order.OrderUUID,
		"new state": newState,
	})

	msg.Order = *order

	msg.Source = models.SourceBroker
	msg.Type = proto.MsgTypes.BrokerMessage.S()
	msg.CreatedAt = time.Now()

	//это нужно что бы как то сохранить состояния
	msg.Order.OrderPrefs.NewState = newState.S()

	msg.FSM = h.InitOrderStateFSM()
	msg.OrderUUID = order.OrderUUID
	msg.ClientMsgID = order.ClientMsgID
	msg.State = order.State

	msg.ChatMsgID = order.ClientMsgID

	answerTextButton, err := h.handleEventTransition(&msg)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":        "failed to get answer",
			"client msg id": msg.ClientMsgID,
			"msgType":       msg.Type,
			"msgText":       msg.Text,
			"msgIntent":     msg.DFAnswer.Intent,
			"msgState":      msg.State,
		}).Error(err)
	}

	//не на все статусы надо отвечать
	if answerTextButton.textAnswer == skipStateConstant {
		return nil
	}

	//отправляем ответ
	sendedMsg, err := h.sendTelegramOrWhatsApp(msg.Order.Source, msg.ChatMsgID, &answerTextButton)

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to send msg",
			"chat id": msg.ChatMsgID,
			"msgText": answerTextButton.textAnswer,
		}).Error(err)
		return errors.Wrap(err, "Failed to send message")
	}

	err = h.Pub.NewMsg(&structures.MessageFromBot{
		Source:       string(structures.MsgBotService),
		Sender:       string(structures.MsgBotSender),
		MsgUUID:      msg.ChatMsgID + "_" + msg.MsgID,
		Receiver:     string(structures.MsgBotTelegramUserReciever),
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        sendedMsg.Id,
		Text:         answerTextButton.textAnswer,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to publish event",
			"chat id": msg.ChatMsgID,
		}).Error(err)
	}

	return nil
}

func (h *Handler) sendTelegramOrWhatsApp(source, chatID string, answerTextButton *answerType) (proto.SendedMessage, error) {
	switch source {
	case models.SourceTelegram:
		var value int64
		value, err := strconv.ParseInt(chatID, 0, 64)
		if err != nil {
			return proto.SendedMessage{}, errors.Wrap(err, "failed to parse int of ChatMsgId")
		}
		return h.Telegram.SendMessage(value, answerTextButton.textAnswer, answerTextButton.buttonsAnswer)
	case models.SourceWhatsApp:
		return h.WASendMessage(chatID, answerTextButton.textAnswer)
	default:
		return proto.SendedMessage{}, errors.New("failed to send msg, unknown msg source")
	}
}

//GetStateObjectFromText из текст возвращается объект типа proto.Constant
//это как бы сопоставление текстовых имен статусов в системе и объектов proto.Constant
func (h *Handler) GetStateObjectFromText(state string) proto.Constant {
	switch state {
	//
	case constants.OrderStateCreated:
		return proto.States.Taxi.Order.OrderCreated
	case constants.OrderStateDistributing:
		return proto.States.Taxi.Order.SmartDistribution
	case constants.OrderStateOffered:
		return proto.States.Taxi.Order.OfferOffered
	case constants.OrderStateAccepted:
		return proto.States.Taxi.Order.DriverAccepted
	case constants.OrderStateCancelled:
		return proto.States.Taxi.Order.OfferCancelled
	case constants.OrderStateFree:
		return proto.States.Taxi.Order.FindingDriver
	case constants.OrderStateStarted:
		return proto.States.Taxi.Order.OrderStart
	case constants.OrderStateOnTheWay:
		return proto.States.Taxi.Order.OnTheWay
	case constants.OrderStateOnPlace:
		return proto.States.Taxi.Order.OnPlace
	case constants.OrderStateWaiting:
		return proto.States.Taxi.Order.Waiting
	case constants.OrderStatePayment:
		return proto.States.Taxi.Order.OrderPayment
	case constants.OrderStateFinished:
		return proto.States.Taxi.Order.Finished
	}
	return proto.States.Taxi.Order.Unknown
}
