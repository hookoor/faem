// основной файл управления статусами диалога и вызова функций генерации ответа и кнопок

package handler

import (
	"github.com/looplab/fsm"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

type TaxiOrderStates struct {
	From string
	To   string
	FSM  *fsm.FSM
}

//getTextAnswer возвращает ответ и кноки для мессенджера
func (h *Handler) getTextAnswer(msg *models.ChatMsgFull) (answerType, error) {

	//если welcome то показываем приветствие
	if msg.Type == proto.MsgTypes.TelegramMessage.S() && msg.DFAnswer.Intent == proto.States.Welcome.S() {
		return answerType{
			textAnswer:    msg.DFAnswer.Answer,
			buttonsAnswer: proto.GetWellcomeButtons(),
			msgId:         0,
		}, nil
	}

	//Fall Back Intent
	if msg.DFAnswer.Intent == proto.States.FallBackIntent.S() {
		return answerType{
			textAnswer:    proto.GetErrorText(msg.State),
			buttonsAnswer: proto.GetCreateTicketButton(),
			msgId:         0,
		}, nil
	}

	//обрабатываем обращение в зависимости от его типа (сообщение или callback)
	var action string
	var err error
	switch msg.Type {
	case proto.MsgTypes.TelegramMessage.S():
		if msg.DFAnswer.Intent == "" {
			switch msg.State {
			//если интент пустой и статуст создание нового то полученный текст идет поддержку
			case proto.States.Taxi.CreateTicket.S():
				action = proto.States.Taxi.SaveTicket.S()
			}
		} else {
			action = msg.DFAnswer.Intent
		}

	//case proto.MsgTypes.TelegramCallback.S():
	//	action, err = getCallbackActionData(msg)
	//	if err != nil {
	//		txt, but, err := getErrorData(msg, err)
	//		return answerType{
	//			textAnswer:    txt,
	//			buttonsAnswer: but,
	//			msgId:         0,
	//		}, err
	//	}
	case proto.MsgTypes.Coordinates.S():
		switch msg.State {
		case proto.States.Taxi.Order.CreateDraft.S():
			action = proto.States.Taxi.Order.Departure.S()
		case proto.States.Taxi.Order.Departure.S():
			action = proto.States.Taxi.Order.Arrival.S()
		default:
			msg.State = proto.States.Unknown.UnexpectedCoordinates.S()
			answ, but, msgID := outputTextAndButtons(msg)
			return answerType{
				textAnswer:    answ,
				buttonsAnswer: but,
				msgId:         msgID,
			}, nil
		}

	case proto.MsgTypes.TelegramContact.S():
		if msg.State == proto.States.Taxi.Order.NeedPhone.S() {
			//err = h.saveClientContact(msg)
			//if err != nil {
			//	handleEventErrors(msg, err)
			//}
			action = proto.States.Taxi.Order.OrderCreated.S()
		} else {
			//если мы получаем контакт не том статусе то отвечаем
			msg.State = proto.States.Unknown.UnexpectedContact.S()
			answ, but, msgID := outputTextAndButtons(msg)
			return answerType{
				textAnswer:    answ,
				buttonsAnswer: but,
				msgId:         msgID,
			}, nil
		}
	case proto.MsgTypes.BrokerMessage.S():
		action = msg.Order.OrderPrefs.NewState

	default:
		return answerType{
			textAnswer:    "",
			buttonsAnswer: proto.ButtonsSet{},
			msgId:         0,
		}, errors.New("Unknownd message type")
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"reason":    "debbuging FSM",
		"action":    action,
		"OrderUUID": msg.OrderUUID,
	})

	//устанавливаем текущий статус в стейт машину
	msg.FSM.SetState(msg.State)

	log.WithFields(logrus.Fields{
		"Msg state": msg.State,
		"FSM State": msg.FSM.Current(),
		"DF":        msg.DFAnswer.Intent,
		"OrderUUID": msg.OrderUUID,
	}).Debug("Before")

	//переводим в новый статус и проводим различные манипуляции с данными
	if msg.FSM.Current() != action {
		err = msg.FSM.Event(action, msg)
		if err != nil {
			//перехватываем некоторые типы ошибок, например если нет номера при создании заказа
			//в этом например случае это не ошибка, а просто новый сценарий
			//answ, but, newErr := handleEventErrors(msg, err)
			//if newErr != nil {
			//	return answerType{
			//		textAnswer:    answ,
			//		buttonsAnswer: but,
			//		msgId:         0,
			//	}, newErr
			//}
		}
	}

	////не на все статусы надо отвечать, некоторые пропускаем
	//if skipState(msg.State) {
	//	return answerType{
	//		textAnswer:    skipStateConstant,
	//		buttonsAnswer: proto.ButtonsSet{},
	//		msgId:         0,
	//	}, nil
	//}

	//генерируем ответ
	answ, but, msgId := outputTextAndButtons(msg)
	return answerType{
		textAnswer:    answ,
		buttonsAnswer: but,
		msgId:         msgId,
	}, nil
}

//func newOrUpdateMessage(msg *models.ChatMsgFull) int {
//	switch msg.State {
//	//case proto.States.Taxi.CancelTicketCreation.S():
//	//	return msg.MsgID
//	//case proto.States.Taxi.CreateTicket.S():
//	//	return msg.MsgID
//	case proto.States.Taxi.Order.FixArrival.S():
//		return msg.MsgID
//	case proto.States.Taxi.Order.FixDeparture.S():
//		return msg.MsgID
//	case proto.States.Taxi.Order.ChangeService.S():
//		return msg.MsgID
//	case proto.States.Taxi.Order.Arrival.S():
//		//если мы пришли на этап из этого же сообщения
//		val, ok := msg.Order.OrderPrefs.MsgsIDs[proto.States.Taxi.Order.Arrival.S()]
//		if ok && val == msg.MsgID {
//			return msg.MsgID
//		}
//		return 0
//	default:
//		return 0
//	}
//}

////getErrorData возращает типовую ошибку
//func getErrorData(msg *models.ChatMsgFull, err error) (string, proto.ButtonsSet, error) {
//	return proto.GetErrorText(msg.State), proto.GetCreateTicketButton(), err
//}

//getCallbackActionData обрабатывает полученные коллбеки, в некоторых случаях нужно провести некоторые манипуляции
//например когда получаем один из номеров для исправления адреса то action для всех один а саму структуру расширяем
//нужным параметром
//func getCallbackActionData(msg *models.ChatMsgFull) (string, error) {
//	cbData, ok := msg.Payload.(*tgbotapi.Update)
//	if !ok {
//		return "", errors.New("unknown datatype while parsing callback data")
//	}
//	data := cbData.CallbackQuery.Data
//
//	//если мы на этапе исправления адреса
//	if (msg.State == proto.States.Taxi.Order.FixDeparture.S()) || (msg.State == proto.States.Taxi.Order.FixArrival.S()) {
//		//сначала запомним на какую кнопку нажали
//		switch data {
//		case proto.Buttons.Actions.WrongAddress1.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 1
//		case proto.Buttons.Actions.WrongAddress2.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 2
//		case proto.Buttons.Actions.WrongAddress3.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 3
//		case proto.Buttons.Actions.WrongAddress4.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 4
//		case proto.Buttons.Actions.WrongAddress5.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 5
//		case proto.Buttons.Actions.BackButton.D():
//			msg.Order.OrderPrefs.FixAddressVariant = 99
//		}
//		//теперь вернем следующий этап
//		if msg.State == proto.States.Taxi.Order.FixDeparture.S() {
//			return proto.States.Taxi.Order.Departure.S(), nil
//		} else if msg.State == proto.States.Taxi.Order.FixArrival.S() {
//			return proto.States.Taxi.Order.Arrival.S(), nil
//		}
//	}
//
//	if msg.State == proto.States.Taxi.Order.ChangeService.S() {
//		//Если на этапе смены сервиса и нажали назад
//		if data == proto.Buttons.Actions.BackButton.D() {
//			return proto.States.Taxi.Order.Arrival.S(), nil
//		}
//		msg.Order.OrderPrefs.Service = cbData.CallbackQuery.Data
//		fmt.Printf("DATA = %s SERVICE = %s\n\n", cbData.CallbackQuery.Data, msg.Order.OrderPrefs.Service)
//		return proto.States.Taxi.Order.Arrival.S(), nil
//	}
//
//	return cbData.CallbackQuery.Data, nil
//}
