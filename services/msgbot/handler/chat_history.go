package handler

import (
	"context"
	"errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strconv"
	"time"
)

func (h *Handler) ChatOrdersHandler(clientMsgID string, state string, from string, to string) ([]models.ChatOrder, error) {
	if state == "" {
		state = "all"
	}
	if clientMsgID == "" {
		clientMsgID = "all"
	}
	offset := 0
	limit := 30
	var err error
	if from != "" {
		offset, err = strconv.Atoi(from)
	}
	if to != "" {
		limit, err = strconv.Atoi(to)
	}
	if err != nil {
		logrus.WithFields(logrus.Fields{"from": from, "to": to}).Error(err)
		return nil, err
	}
	if offset > limit {
		err = errors.New("'from' parameter is bigger than 'to'")
		logrus.WithFields(logrus.Fields{"from": offset, "to": limit}).Error(err)
		return nil, err
	}
	limit = limit + 1 - offset
	if offset == 0 {
		limit--
	}
	offset--
	chatOrders, err := h.DB.GetChatOrders(clientMsgID, state, limit, offset)
	if err != nil {
		logrus.WithField("event", "getting chat orders from db").Error(err)
		return nil, err
	}
	return chatOrders, err
}

func (h *Handler) ChatByOrderUUIDHandler(orderUUID string) ([]models.ChatHistory, error) {
	msgList, err := h.DB.GetChatByOrderUUID(orderUUID)
	if err != nil {
		logrus.WithField("event", "getting chat by order uuid from db").Error(err)
		return nil, err
	}
	return msgList, err
}

func (h *Handler) SendingSupportMsgHandler(supportMsg proto.ChatMsg) (models.ChatInfo, string, time.Time, error) {
	chatInfo, err := h.DB.GetChatInfoByOrderUUID(supportMsg.OrderUUID)
	if err != nil {
		logrus.WithField("event", "getting source and chat id by order uuid from db").Error(err)
		return chatInfo, "", time.Time{}, err
	}
	answer := answerType{
		textAnswer:    supportMsg.Msg,
		buttonsAnswer: proto.ButtonsSet{},
		msgId:         0,
	}
	sentMsg, err := h.sendTelegramOrWhatsApp(chatInfo.Source, chatInfo.ChatID, &answer)
	msgSendingTime := time.Now()
	if err != nil {
		logrus.WithField("event", "sending support msg").Error(err)
		return chatInfo, sentMsg.Id, msgSendingTime, err
	}
	return chatInfo, sentMsg.Id, msgSendingTime, err
}

func (h *Handler) SavingSupportMsgHandler(chatInfo models.ChatInfo, supportMsg proto.ChatMsg, sentMsgID string, msgSendingTime time.Time) error {
	var msgType string
	switch chatInfo.Source {
	case models.SourceWhatsApp:
		msgType = proto.MsgTypes.WhatsAppMessage.S()
	case models.SourceTelegram:
		msgType = proto.MsgTypes.TelegramMessage.S()
	}
	chatData := models.ChatHistory{
		Source:      chatInfo.Source,
		Sender:      string(structures.MsgBotSupportSender),
		Reciever:    string(structures.MsgBotTelegramUserReciever),
		OrderUUID:   supportMsg.OrderUUID,
		State:       chatInfo.State,
		TypeField:   msgType,
		ClientUUID:  "",
		Text:        supportMsg.Msg,
		ClientMsgID: chatInfo.ChatID,
		ChatID:      chatInfo.ChatID,
		MsgID:       sentMsgID,
		Payload:     models.ChatPayload{},
		CreatedAt:   msgSendingTime,
	}
	err := h.DB.SaveChatHistory(context.Background(), &chatData)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"reason": "failed save support msg in db",
		}).Error(err)
	}
	return err
}
