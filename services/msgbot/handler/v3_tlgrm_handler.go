package handler

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strconv"
	"time"
)

func (h *Handler) TelegramMsgHadler(ctx context.Context, update *tgbotapi.Update) {
	var err error
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling income message 3.0",
	})

	// ignore any non-Message Updates
	if update.Message == nil && update.CallbackQuery == nil {
		log.WithFields(logrus.Fields{
			"reason": "unknown type. not message and not callback",
		}).Error("failed to response telegram message type")
		return
	}

	// если пользователь прислал голосовое сообщение,делаем из него текст
	if update.Message != nil && update.Message.Voice != nil {
		voiceText, err := h.getTextFromVoice(update.Message.Voice.FileID)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":        "failed to parse voice into text",
				"client msg id": update.Message.MessageID,
				"chat msg id":   update.Message.Chat.ID,
			}).Error(err)
		}
		update.Message.Text = voiceText
	}
	//заполняем базовую структуру и получаем текст для отправки в архив
	msg, msgBodyToChat := fillUpChatMsg(update)

	//Получаем заказ связанный с этим сообщением
	msg.Order, err = h.GetMsgOrder(ctx, structures.MessageFromBot{
		ClientMsgID: msg.ClientMsgID,
		Source:      msg.Source,
		ChatMsgID:   msg.ChatMsgID,
	})
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":        "failed to get order uuid",
			"client msg id": msg.ClientMsgID,
			"chat msg id":   msg.ChatMsgID,
		}).Error(err)
		return
	}

	msg.State = msg.Order.State
	msg.FSM = h.InitOrderStateFSM()
	msg.OrderUUID = msg.Order.OrderUUID

	//эта консутрукция нужна для того что бы в процессе отработки событий мы могли поменять получателя
	//сообщения, это может быть служба поддержки или например водитель, по дефолту - система
	//TODO реализовать логику отправки сообщения водителю
	msg.Order.OrderPrefs.MsgReciever = string(structures.MsgBotReciever)

	//получаем стуктуру с ответом
	answerTextButton, err := h.handleEventTransition(&msg)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":        "failed to get answer",
			"client msg id": msg.ClientMsgID,
			"msgType":       msg.Type,
			"msgText":       msg.Text,
			"msgIntent":     msg.DFAnswer.Intent,
			"msgState":      msg.State,
		}).Error(err)
	}
	//Отпавляем сообщение полученное от пользователя в архив чата
	err = h.Pub.NewMsg(&structures.MessageFromBot{
		Source:       string(structures.MsgBotService),
		Sender:       string(structures.MsgBotTelegramUserSender),
		Receiver:     msg.Order.OrderPrefs.MsgReciever,
		MsgUUID:      msg.ChatMsgID + "_" + msg.MsgID,
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        msg.MsgID,
		Text:         msgBodyToChat,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to publish event",
			"chat id": msg.ChatMsgID,
		}).Error(err)
	}

	chatData := models.ChatHistory{
		Source:      models.SourceTelegram,
		Sender:      string(structures.MsgBotTelegramUserSender),
		Reciever:    msg.Order.OrderPrefs.MsgReciever,
		OrderUUID:   msg.OrderUUID,
		State:       msg.State,
		TypeField:   msg.Type,
		ClientUUID:  msg.ClientUUID,
		Text:        msgBodyToChat,
		ClientMsgID: msg.ClientMsgID,
		ChatID:      msg.ChatMsgID,
		MsgID:       msg.MsgID,
		Payload:     models.ChatPayload{},
		CreatedAt:   msg.CreatedAt,
	}
	err = h.DB.SaveChatHistory(ctx, &chatData)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "failed save chat history in DB",
		}).Error(err)
	}

	//не на все статусы надо отвечать
	if answerTextButton.textAnswer == skipStateConstant {
		return
	}
	//отправляем ответное сообщение в чат
	value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
	sendedMsg, err := h.Telegram.SendMessage(value, answerTextButton.textAnswer, answerTextButton.buttonsAnswer)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to send new msg",
			"chat id": msg.ChatMsgID,
			"msgText": answerTextButton.textAnswer,
		}).Error(err)
		return
	}

	//отправляем в брокер что бы сохранить в сервисе chat историю переписки между человеком и ботом
	err = h.Pub.NewMsg(&structures.MessageFromBot{
		Source:       string(structures.MsgBotService),
		MsgUUID:      msg.ChatMsgID + "_" + msg.MsgID,
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        sendedMsg.Id,
		Text:         answerTextButton.textAnswer,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
		Sender:       string(structures.MsgBotSender),
		Receiver:     string(structures.MsgBotTelegramUserReciever),
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to publish event",
			"chat id": msg.ChatMsgID,
		}).Error(err)
	}

	chatData = models.ChatHistory{
		Source:      models.SourceTelegram,
		Sender:      string(structures.MsgBotSender),
		Reciever:    string(structures.MsgBotTelegramUserReciever),
		OrderUUID:   msg.OrderUUID,
		State:       msg.State,
		TypeField:   msg.Type,
		ClientUUID:  msg.ClientUUID,
		Text:        answerTextButton.textAnswer,
		ClientMsgID: msg.ClientMsgID,
		ChatID:      msg.ChatMsgID,
		MsgID:       sendedMsg.Id,
		Payload:     models.ChatPayload{},
		CreatedAt:   msg.CreatedAt,
	}
	err = h.DB.SaveChatHistory(ctx, &chatData)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "failed save chat history in DB",
		}).Error(err)
	}
}

//статусы на которых не нужно распозновать текст, например когда отправляем собщение в тикеты или чат
func needToResponse(state string) bool {
	skipper := []string{
		proto.States.Taxi.CreateTicket.S(),
		proto.States.Taxi.Order.SaveContact.S(),
		proto.States.Taxi.Order.NeedPhone.S(),
	}
	for _, v := range skipper {
		if state == v {
			return false
		}
	}
	return true
}

func getMsgToArchive(msgBodyToChat string, msg *models.ChatMsgFull) structures.MessageFromBot {
	return structures.MessageFromBot{
		Source:       string(structures.ClientMember),
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        msg.MsgID,
		Text:         msgBodyToChat,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
	}
}

func fillUpChatMsg(update *tgbotapi.Update) (models.ChatMsgFull, string) {
	var msg models.ChatMsgFull

	msg.CreatedAt = time.Now()
	msg.Source = models.SourceTelegram
	msg.Payload = update

	if update.Message != nil {
		msg.UserLogin = update.Message.From.UserName
		msg.Type = proto.MsgTypes.TelegramMessage.S()
		msg.Text = update.Message.Text
		msg.ChatMsgID = strconv.Itoa(int(update.Message.Chat.ID))
		msg.ClientMsgID = strconv.Itoa(update.Message.From.ID)
		msg.CreatedAtMsg = time.Unix(int64(update.Message.Date), 0)
		msg.MsgID = strconv.Itoa(update.Message.MessageID)

		if update.Message.Contact != nil {
			msg.Type = proto.MsgTypes.TelegramContact.S()
			return msg, "contact " + update.Message.Contact.PhoneNumber
		}
		if update.Message.Location != nil {
			msg.Type = proto.MsgTypes.Coordinates.S()
			s := fmt.Sprintf("coords %f %f", update.Message.Location.Latitude, update.Message.Location.Longitude)
			return msg, s
		}
		if update.Message.Voice != nil {
			msg.Type = proto.MsgTypes.Voice.S()
			return msg, "voice sended"
		}

		return msg, update.Message.Text
	}
	//если пришлю сюда значит сообщение есть CallBack
	msg.UserLogin = update.CallbackQuery.From.UserName
	msg.Type = proto.MsgTypes.TelegramCallback.S()
	msg.Text = update.CallbackQuery.Data
	msg.ChatMsgID = strconv.Itoa(int(update.CallbackQuery.Message.Chat.ID))
	msg.ClientMsgID = strconv.Itoa(update.CallbackQuery.From.ID)
	msg.CreatedAtMsg = time.Unix(int64(update.CallbackQuery.Message.Date), 0)
	msg.MsgID = strconv.Itoa(update.CallbackQuery.Message.MessageID)
	return msg, "callback " + msg.Text
}
