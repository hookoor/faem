package handler

import (
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strconv"
)

//структура для ответа
type answerType struct {
	textAnswer    string           //сам текст
	buttonsAnswer proto.ButtonsSet //инлайн кнопки
	msgId         int              //0 - отправить новое, иначе исправить id
}

const (
	defaultFallBackIntent = "Default Fallback Intent"
)

func (h *Handler) handleEventTransition(msg *models.ChatMsgFull) (answerType, error) {

	var action string
	var err error

	log := logs.Eloger.WithFields(logrus.Fields{
		"reason":    "getting text answer v3",
		"OrderUUID": msg.OrderUUID,
		"Msg.State": msg.State,
	})

	//если не сообщение из брокера
	if msg.Type != proto.MsgTypes.BrokerMessage.S() {
		//опредяем интент (что именно хотел сказать пользователь)
		err := h.detectIntent(msg)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "failed to DFAnswer",
			}).Error(err)
		}

		//если мы не поняли что это говорит нам пользователь
		if msg.Order.OrderPrefs.DetectedIntent == defaultFallBackIntent {
			log.WithFields(logrus.Fields{
				"message": msg.Text,
				"user ID": msg.ChatMsgID,
			}).Warn("fall back intent")
			return getFallBackAnswer(msg.State), nil
		}

		action, err = h.getAction(msg)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":  "failed to Get Action",
				"message": msg.Text,
				"user ID": msg.ChatMsgID,
			}).Error(err)
			return getErrorAnswer(msg.State), err
		}
	} else {
		//а если из брокера
		action = msg.Order.OrderPrefs.NewState
	}

	msg.FSM.SetState(msg.State)

	log.WithFields(logrus.Fields{
		"FSM State": msg.FSM.Current(),
		"DF":        msg.DFAnswer.Intent,
		"action":    action,
		"msg_type":  msg.Type,
	}).Debug("Before")

	if action == proto.States.Taxi.Order.Unknown.S() {
		log.WithFields(logrus.Fields{
			"State":    msg.State,
			"text":     msg.Text,
			"action":   action,
			"msg_type": msg.Type,
		}).Warn("Getting Unknown state")
	}

	if msg.FSM.Current() != action && action != proto.States.Taxi.Order.Unknown.S() {

		//эта конструкция нужна для автоматического перехода из одного статуса в другой
		for {
			err = msg.FSM.Event(action, msg)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason":    "failed to transit state",
					"OrderUUID": msg.OrderUUID,
					"state":     msg.State,
					"action":    action,
				}).Error(err)
				return getErrorAnswer(msg.State), err
			}
			buffOrder := h.Buffers.WIPOrdersFull[msg.OrderUUID]
			if buffOrder.OrderPrefs.JumpToState == "" {
				break
			}
			action = buffOrder.OrderPrefs.JumpToState
			msg.Order.OrderPrefs.IntentSteps = addIntent(action, msg.Order.OrderPrefs.IntentSteps)
			buffOrder.OrderPrefs.JumpToState = ""
			h.Buffers.WIPOrdersFull[msg.OrderUUID] = buffOrder
		}
	}

	//не на все статусы отвечаем
	if skipState(msg.State) {
		return answerType{
			textAnswer: skipStateConstant,
		}, nil
	}

	return getAnswerTextAndButton(msg)
}

//getAction принимает решение о переходе на следующий статус
func (h *Handler) getAction(msg *models.ChatMsgFull) (string, error) {

	//с начала обрабатываем особые кнопки типа координат или контактов
	switch msg.Type {
	case proto.MsgTypes.Coordinates.S():
		switch msg.State {
		//если на этапе создания заказа
		case proto.States.Taxi.Order.CreateDraft.S():
			return proto.States.Taxi.Order.Departure.S(), nil
		//если на этапе получения адреса назначения
		case proto.States.Taxi.Order.Departure.S():
			return proto.States.Taxi.Order.Arrival.S(), nil
		default:
			return proto.States.Unknown.UnexpectedCoordinates.S(), nil
		}
	case proto.MsgTypes.TelegramContact.S():
		if msg.State == proto.States.Taxi.Order.NeedPhone.S() {
			return proto.States.Taxi.Order.SaveContact.S(), nil
		} else {
			return proto.States.Unknown.UnexpectedContact.S(), nil
		}
	}

	//fmt.Printf("\n\nIntent: %s, state: %s, DF: %s\n\n", msg.Order.OrderPrefs.DetectedIntent, msg.Order.State, msg.DFAnswer.Intent)
	//если распознан интент то его возвращаем
	if msg.DFAnswer.Intent != "" {
		return msg.DFAnswer.Intent, nil
	}

	//РАЗБИРАЕМ ЛОКАЛЬНЫЕ ИНТЕНТЫ
	if msg.Order.OrderPrefs.DetectedIntent != "" {

		// Обрещение в поддержку
		if msg.Order.OrderPrefs.DetectedIntent == proto.States.Taxi.CreateTicket.S() {
			return proto.States.Taxi.CreateTicket.S(), nil
		}

		//1 - Отмена заказа
		if msg.Order.State == proto.States.Taxi.Order.CancelPromt.S() {
			//если нажимают да - отменяем
			if msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.YesAction.D() {
				return proto.States.Taxi.Order.Cancelled.S(), nil
			}
			//иначе находим предыдущий не промежуточный статус
			steps := msg.Order.OrderPrefs.IntentSteps
			if len(steps) > 0 {
				for _, v := range steps {
					if !isStateSecondary(v) {
						//и переводим в него
						return v, nil
					}
				}
			}
			//если такой не нашел то в самый первый
			return proto.States.Taxi.Order.CreateDraft.S(), nil
		}

		//2 - Кнопки выбора адреса
		//если нажали кнопки изменения адреса то разбираем какая это кнопка и идем сохранять
		if msg.Order.State == proto.States.Taxi.Order.FixDeparture.S() || msg.Order.State == proto.States.Taxi.Order.FixArrival.S() {
			//если нажали кнопку назад
			if msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.BackButton.D() {
				msg.Order.OrderPrefs.FixAddressVariant = 99
			} else {

				//если прилетела цифра
				i, err := strconv.Atoi(msg.Order.OrderPrefs.DetectedIntent)
				if err != nil {
					return msg.Order.OrderPrefs.DetectedIntent, nil
				}
				if i > 0 && i < 6 {
					msg.Order.OrderPrefs.FixAddressVariant = i
				}
			}

			if msg.State == proto.States.Taxi.Order.FixDeparture.S() {
				return proto.States.Taxi.Order.Departure.S(), nil
			} else if msg.State == proto.States.Taxi.Order.FixArrival.S() {
				return proto.States.Taxi.Order.Arrival.S(), nil
			}
		}

		//3 - смена тарифа
		//кнопка смена тарифа
		if msg.Order.State == proto.States.Taxi.Order.ChangeService.S() {
			if msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.BackButton.D() {
				return proto.States.Taxi.Order.Arrival.S(), nil
			}
			i, err := strconv.Atoi(msg.Order.OrderPrefs.DetectedIntent)
			if err != nil {
				return msg.Order.OrderPrefs.DetectedIntent, nil
			}
			if i > 0 && i < len(msg.Order.OrderPrefs.Tariffs)+1 {
				msg.Order.OrderPrefs.Service = msg.Order.OrderPrefs.Tariffs[i-1].ServiceUUID
				return proto.States.Taxi.Order.Arrival.S(), nil
			}
		}

		//4 - Создание тикета
		if msg.Order.State == proto.States.Taxi.CreateTicket.S() {
			if msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.CancelTicketCreation.D() ||
				msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.BackButton.D() {
				//иначе находим предыдущий не промежуточный статус
				steps := msg.Order.OrderPrefs.IntentSteps
				if len(steps) > 0 {
					for _, v := range steps {
						if !isStateSecondary(v) {
							//и переводим в него
							fmt.Printf("\n\nMy state is %s\n\n", v)
							return v, nil
						}
					}
				}
				//если такой не нашел то в самый первый
				return proto.States.Taxi.Order.CreateDraft.S(), nil
			}
			//на все остальные случаи - перевод в статус сохранения
			return proto.States.Taxi.SaveTicket.S(), nil
		}

		//5 - назад при регистрации номера телефона
		if msg.Order.State == proto.States.Taxi.Order.SaveContact.S() {
			if msg.Order.OrderPrefs.DetectedIntent == proto.Buttons.Actions.BackButton.D() {
				return proto.States.Taxi.Order.Arrival.S(), nil
			}
		}

		return msg.Order.OrderPrefs.DetectedIntent, nil
	}

	//теперь разбираем случае когда нужно просто сохранить полученные от пользователя данные
	switch msg.State {
	case proto.States.Taxi.Order.NeedPhone.S():
		return proto.States.Taxi.Order.SaveContact.S(), nil
	case proto.States.Taxi.CreateTicket.S():
		return proto.States.Taxi.SaveTicket.S(), nil
	case proto.States.Taxi.Order.SaveContact.S():
		return proto.States.Taxi.Order.ValidateContact.S(), nil
	case proto.States.NewOrder.S():
		return proto.States.Welcome.S(), nil
	}

	return "", errors.New("unknown action")
}

var statesToSkip = []string{
	proto.States.Taxi.Order.SmartDistribution.S(),
	proto.States.Taxi.Order.OfferOffered.S(),
	proto.States.Taxi.Order.FindingDriver.S(),
	proto.States.Taxi.Order.Waiting.S(),
	proto.States.Taxi.Order.OrderPayment.S(),
	proto.States.Taxi.Order.DriverAccepted.S(),
	proto.States.Taxi.Order.DriverFounded.S(),
	proto.States.Taxi.Order.Unknown.S(),
}

func skipState(state string) bool {
	for _, v := range statesToSkip {
		if v == state {
			return true
		}
	}
	return false
}
