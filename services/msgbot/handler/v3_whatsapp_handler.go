package handler

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"io/ioutil"
	"net/http"
	"reflect"
	"strconv"
	"time"
)

func (h *Handler) WhatsAppMsgHandler(ctx context.Context, update *proto.WAIncomingMsg) {

	var err error
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling income message",
	})

	//TODO: Сделать отправку голосовых сообщений из WBA
	//если пользователь прислал голосовое сообщение,делаем из него текст
	//if msg.Messages[0].Voice != nil {
	//	voiceText, err := h.getTextFromVoice(update.Message.Voice.FileID)
	//	if err != nil {
	//		log.WithFields(logrus.Fields{
	//			"reason":        "failed to parse voice into text",
	//			"client msg id": update.Message.MessageID,
	//			"chat msg id":   update.Message.Chat.ID,
	//		}).Error(err)
	//	}
	//	update.Message.Text = voiceText
	//}

	//заполняем базовую структуру и получаем текст для отправки в архив
	msg, msgBodyToChat := waFillUpChatMsg(*update)

	msgFromBot := structures.MessageFromBot{
		ClientMsgID: msg.ClientMsgID,
		Source:      msg.Source,
		ChatMsgID:   msg.ChatMsgID,
	}

	//Получаем заказ связанный с этим сообщением
	msg.Order, err = h.GetMsgOrder(ctx, msgFromBot)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":        "failed to get order uuid",
			"client msg id": msg.ClientMsgID,
			"chat msg id":   msg.ChatMsgID,
		}).Error(err)
		return
	}

	msg.State = msg.Order.State
	msg.FSM = h.InitOrderStateFSM()
	msg.OrderUUID = msg.Order.OrderUUID

	//эта консутрукция нужна для того что бы в процессе отработки событий мы могли поменять получателя
	//сообщения, это может быть служба поддержки или например водитель, по дефолту - система
	//TODO реализовать логику отправки сообщения водителю
	msg.Order.OrderPrefs.MsgReciever = string(structures.MsgBotReciever)

	//получаем стуктуру с ответом
	answerTextButton, err := h.handleEventTransition(&msg)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":        "failed to get answer",
			"client msg id": msg.ClientMsgID,
			"msgType":       msg.Type,
			"msgText":       msg.Text,
			"msgIntent":     msg.DFAnswer.Intent,
			"msgState":      msg.State,
		}).Error(err)
	}

	//Отпавляем сообщение полученное от пользователя в архив чата
	err = h.Pub.NewMsg(&structures.MessageFromBot{
		Source:       string(structures.MsgBotService),
		Sender:       string(structures.MsgBotWhatsAppUserSender),
		Receiver:     msg.Order.OrderPrefs.MsgReciever,
		MsgUUID:      msg.ChatMsgID + "_" + msg.MsgID,
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        msg.MsgID,
		Text:         msgBodyToChat,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to publish event",
			"chat id": msg.ChatMsgID,
		}).Error(err)
	}

	chatData := models.ChatHistory{
		Source:      models.SourceWhatsApp,
		Sender:      string(structures.MsgBotWhatsAppUserSender),
		Reciever:    msg.Order.OrderPrefs.MsgReciever,
		OrderUUID:   msg.OrderUUID,
		State:       msg.State,
		TypeField:   msg.Type,
		ClientUUID:  msg.ClientUUID,
		Text:        msgBodyToChat,
		ClientMsgID: msg.ClientMsgID,
		ChatID:      msg.ChatMsgID,
		MsgID:       msg.MsgID,
		Payload:     models.ChatPayload{},
		CreatedAt:   msg.CreatedAt,
	}
	err = h.DB.SaveChatHistory(ctx, &chatData)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "failed save chat history in DB",
		}).Error(err)
	}

	//не на все статусы надо отвечать
	if answerTextButton.textAnswer == skipStateConstant {
		return
	}

	//отправляем ответное сообщение в чат
	_, err = h.WASendMessage(update.Contacts[0].WAID, answerTextButton.textAnswer)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to send new msg",
			"chat id": msg.ChatMsgID,
			"msgText": answerTextButton.textAnswer,
		}).Error(err)
		return
	}

	//отправляем в брокер что бы сохранить в сервисе chat историю переписки между человеком и ботом
	err = h.Pub.NewMsg(&structures.MessageFromBot{
		Source:       string(structures.MsgBotService),
		MsgUUID:      msg.ChatMsgID + "_" + msg.MsgID,
		ClientMsgID:  msg.ClientMsgID,
		UserLogin:    msg.UserLogin,
		ChatMsgID:    msg.ChatMsgID,
		MsgID:        update.Messages[0].Id,
		Text:         answerTextButton.textAnswer,
		OrderUUID:    msg.OrderUUID,
		ClientUUID:   msg.ClientUUID,
		CreatedAt:    msg.CreatedAt,
		CreatedAtMsg: msg.CreatedAtMsg,
		Sender:       string(structures.MsgBotSender),
		Receiver:     string(structures.MsgBotWhatsAppUserReciever),
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":  "failed to publish event",
			"chat id": msg.ChatMsgID,
		}).Error(err)
	}

	chatData = models.ChatHistory{
		Source:      models.SourceWhatsApp,
		Sender:      string(structures.MsgBotSender),
		Reciever:    string(structures.MsgBotWhatsAppUserReciever),
		OrderUUID:   msg.OrderUUID,
		State:       msg.State,
		TypeField:   msg.Type,
		ClientUUID:  msg.ClientUUID,
		Text:        answerTextButton.textAnswer,
		ClientMsgID: msg.ClientMsgID,
		ChatID:      msg.ChatMsgID,
		MsgID:       update.Messages[0].Id,
		Payload:     models.ChatPayload{},
		CreatedAt:   msg.CreatedAt,
	}
	err = h.DB.SaveChatHistory(ctx, &chatData)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "failed save chat history in DB",
		}).Error(err)
	}
}

func waFillUpChatMsg(update proto.WAIncomingMsg) (models.ChatMsgFull, string) {
	var msg models.ChatMsgFull

	msg.CreatedAt = time.Now()
	msg.Source = models.SourceWhatsApp
	msg.Payload = update

	if update.Messages != nil {
		msg.UserLogin = update.Contacts[0].Profile.Name
		msg.ClientMsgID = update.Contacts[0].WAID // TODO: это правильно ?
		msg.Type = proto.MsgTypes.WhatsAppMessage.S()

		if update.Messages[0].GroupId == "" {
			msg.ChatMsgID = update.Contacts[0].WAID
		} else {
			msg.ChatMsgID = update.Messages[0].GroupId
		}
		msg.Text = update.Messages[0].Text.Body
		value, err := strconv.ParseInt(update.Messages[0].Timestamp, 0, 64)
		if err != nil {
			msg.CreatedAtMsg = time.Unix(0, 0)
		} else {
			msg.CreatedAtMsg = time.Unix(value, 0)
		}
		msg.MsgID = update.Messages[0].Id

		if update.Messages[0].Contacts != nil {
			fmt.Println(len(update.Messages[0].Contacts), " contacts received")
			// TODO: добавить обработку whatsapp'овских контактов к уже имеющимся telegram'овским
			msg.Type = proto.MsgTypes.WhatsAppContact.S()
			return msg, "contact " + update.Messages[0].Contacts[0].Phones[0].Phone
		}

		if !reflect.DeepEqual(update.Messages[0].Location, proto.WALocation{}) {
			fmt.Println("wa location")
			msg.Type = proto.MsgTypes.Coordinates.S()
			msg.Order.OrderPrefs.Lat = update.Messages[0].Location.Latitude
			msg.Order.OrderPrefs.Lon = update.Messages[0].Location.Longitude
			return msg, "coordinates sended"
		}

		return msg, update.Messages[0].Text.Body
	}

	// колбэков тоже не было

	return msg, "not a message"
}

func (h *Handler) WASendMessage(to string, msg string, keyboard ...proto.ButtonsSet) (proto.SendedMessage, error) {
	var sendedMessage proto.SendedMessage

	message := proto.WAOutgoingMsg{
		To:   to,
		Type: "text",
		Text: struct {
			Body string `json:"body"`
		}{
			Body: msg,
		},
	}
	//TODO сделать нормальный ответ
	err := waSendMessageRequest("POST", h.Config.Application.WhatsAppUrl, message, nil, h.Config.Application.WhatsAppToken)
	if err != nil {
		return sendedMessage, err
	}
	sendedMessage.Id = ""

	return sendedMessage, nil
}

func waSendMessageRequest(method, url string, payload, response interface{}, apiToken string) error {
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}

	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}

	req.Header.Set("Content-Type", "application/json")
	req.Header.Add("D360-Api-Key", apiToken)
	resp, err := httpClient.Do(req)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("failed to make a %s request", method))
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("%s request to %s failed with status: %d", method, url, resp.StatusCode)
		}
		return errors.Errorf("%s request to %s failed with status: %d and body: %s", method, url, resp.StatusCode, string(body))
	}

	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}

	return nil
}
