package handler

import (
	"bytes"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"io/ioutil"
	"net/http"
	"time"
)

const (
	checkIAMinterval = 30
	errorIAMinterval = 5
)

type (
	result struct {
		Result string `json:"result"`
	}

	yskOAuth struct {
		OAuthToken string `json:"yandexPassportOauthToken"`
	}

	yskIAM struct {
		IamToken  string    `json:"iamToken"`
		ExpiresAt time.Time `json:"expiresAt"`
	}
	// Config for speech synthesiser
	SpeechConfig struct {
		BaseURL  string
		FolderID string
		Token    string
	}

	// SynthClient for speech to text
	SynthClient struct {
		HTTPClient *http.Client
		Config     SpeechConfig
	}
)

func (h *Handler) getTextFromVoice(fileId string) (string, error) {
	var voice []byte
	var result string
	fileUrl, err := h.getFileDirectUrl(fileId)
	if err != nil {
		return "", err
	}
	err = getVoiceFromUrl(fileUrl, &voice)
	if err != nil {
		return "", err
	}
	result, err = h.recognizeTextFromVoice(&voice)

	return result, nil
}

func (h *Handler) getFileDirectUrl(fileId string) (string, error) {
	fileUrl, err := h.TelegramBot.GetFileDirectURL(fileId)
	if err != nil {
		return "", err
	}
	return fileUrl, err
}

func getVoiceFromUrl(fileUrl string, voice *[]byte) error {
	resp, err := http.Get(fileUrl)
	if err != nil {
		return err
	}
	*voice, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}

func (h *Handler) recognizeTextFromVoice(voice *[]byte) (string, error) {
	bearer := "Bearer " + h.Config.YandexSpeech.Token

	postUrl := h.Config.YandexSpeech.BaseURL +
		"?folderId=" + h.Config.YandexSpeech.FolderID +
		"&lang=" + h.Config.YandexSpeech.Language
	req, err := http.NewRequest("POST", postUrl, bytes.NewBuffer(*voice))
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "creating new request",
		}).Error(err)
	}
	req.Header.Add("Authorization", bearer)
	response, err := httpClient.Do(req)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "requesting text from voice",
		}).Error(err)
	}
	body, _ := ioutil.ReadAll(response.Body)
	var result result
	err = json.Unmarshal(body, &result)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "unmarshalling YSK text response",
		}).Error(err)
	}

	return result.Result, err
}

func (sc *SynthClient) SetIAM(oauth, url string) error {
	checkInterval := checkIAMinterval * time.Minute
	sc.requestToken(oauth, url, checkIAMinterval)
	go func() {
		for {
			<-time.After(checkInterval)
			sc.requestToken(oauth, url, checkIAMinterval)
		}
	}()
	return nil
}
func (sc *SynthClient) requestIAM(oauth, url string) error {

	yOauth := yskOAuth{
		OAuthToken: oauth,
	}
	body, err := json.Marshal(yOauth)

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an POST request for JWT token")
	}
	resp, err := sc.HTTPClient.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to make a GET request (SKT JWT)")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("requesting JWT to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf("requesting JWT to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}
	var token yskIAM
	err = json.NewDecoder(resp.Body).Decode(&token)
	if err != nil {
		return errors.Wrap(err, "failed to decode JWT token response")
	}
	sc.Config.Token = token.IamToken
	return nil
}

func (sc *SynthClient) requestToken(oauth string, url string, checkInterval time.Duration) {
	err := sc.requestIAM(oauth, url)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "updating Yandex Speech IAM token ",
		}).Error(err)
		checkInterval = errorIAMinterval * time.Minute
	} else {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "updating Yandex Speech IAM",
		}).Info("IAM token updated")
		checkInterval = checkIAMinterval * time.Minute
	}
}
