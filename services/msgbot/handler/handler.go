package handler

import (
	"context"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/jobqueue"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/msgbot/config"
	"gitlab.com/faemproject/backend/faem/services/msgbot/dialogflow"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

type NLP interface {
	//GetAnswer генерирует ответ на входяшее сообщение
	GetAnswer(ctx context.Context, msg *models.ChatMsgFull) (string, error)
}

const (
	JobQueueNameMsgs  = "income.msg/telegram"
	JobQueueLimitMsgs = 999
)

type Repository interface {
	MsgBotRepository
}

type Publisher interface {
	BrokerPublisher
}

type TelegramClient interface {
	TelegramInterface
}

type TelegramInterface interface {
	// Отправляем сообщение в чат, опция - клавиатура
	SendMessage(chatID int64, msg string, keyboard ...proto.ButtonsSet) (proto.SendedMessage, error)
	// Отправляем сообщение пришедшее от оператора
	SendOperatorMessage(chatID int64, msg string) error
	// Обновляем сообщение
	UpdateMessage(chatID int64, msgId int, msg string) (proto.SendedMessage, error)
	// Ставим новую клаву
	UpdateKeyboard(chatID int64, msgID int, newKeyborad proto.ButtonsSet) error
	// Удаляем сообщение целиком
	DeleteMsg(chatID int64, msgID int) error
}

type Handler struct {
	DB             Repository
	Pub            Publisher
	Telegram       TelegramClient
	TelegramBot    *tgbotapi.BotAPI
	NotifierClient TelegramClient
	NotifierBot    *tgbotapi.BotAPI
	DF             *dialogflow.DFProcessor
	Config         *config.Config
	Buffers        Buffers
	Jobs           *jobqueue.JobQueues
	//OrderStateFSM *fsm.FSM
}

type Buffers struct {
	//мапа с заказами и статусами
	WIPOrders map[string]string

	//мапа с заказами и статусами
	WIPOrdersFull map[string]models.LocalOrders

	//мапа с заказами из CRM
	CRMOrders map[string]string

	//мапа с заказами из CRM
	DriverFounded map[string]string
}

type BrokerPublisher interface {
	NewMsg(msg *structures.MessageFromBot) error
	NewDraftOrder(order *models.OrderCRM) error
	StartOrder(order *models.OrderCRM) error
	ActionOnOrder(action *structures.ActionOnOrder) error
}

func (h *Handler) InitBuffer(ctx context.Context) error {

	activeOrders, err := h.DB.GetActiveOrders(ctx)
	if err != nil {
		return err
	}
	if i := len(activeOrders); i == 0 {
		return nil
	}
	for _, v := range activeOrders {
		h.Buffers.WIPOrders[v.OrderUUID] = constants.TranslateOrderStateForClient(v.State, false)
	}
	return nil
}
