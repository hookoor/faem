package handler

import (
	"context"
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/msgbot/dialogflow"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"strings"
)

const intentHistoryLength = 12

const (
	//Параметры entities
	//Откуда
	DepartAddressHouse    = "departure-address-house"    // номер дома
	DepartAddressBuilding = "departure-address-building" // номер корпуса
	DepartAddressStreet   = "departure-address-street"   // название улицы
	DepartAddressLetter   = "departure-address-letter"   // название литера
	DepartPublicPlace     = "departure-public-place"     //название публичного места
	DepartCity            = "departure-city"             // название города

	//Куда
	ArrivalAddressHouse    = "arrival-address-house"    // номер дома
	ArrivalAddressBuilding = "arrival-address-building" // номер корпуса
	ArrivalAddressStreet   = "arrival-address-street"   // название улицы
	ArrivalAddressLetter   = "arrival-address-letter"   // название литера
	ArrivalPublicPlace     = "arrival-public-place"     // название публичного места
	ArrivalCity            = "arrival-city"             // название города

)

func (h *Handler) detectIntent(msg *models.ChatMsgFull) error {
	var err error

	//c начала пробуем определить интент по собственной библиотеке фраз
	val := proto.DetectLocalIntent(msg.Text)
	if val != "" {
		msg.Order.OrderPrefs.DetectedIntent = val
		msg.Order.OrderPrefs.IntentSteps = addIntent(val, msg.Order.OrderPrefs.IntentSteps)
		return nil
	}

	//если не поняли то идем в DialogFlow
	if (msg.Type == proto.MsgTypes.WhatsAppMessage.S() ||
		msg.Type == proto.MsgTypes.TelegramMessage.S() ||
		msg.Type == proto.MsgTypes.Voice.S()) && needToResponse(msg.State) {
		intentContext := swapContext(msg.State)

		//этот костыль нужен для того что бы на любой новый заказ мы в начале отправляли приветствие
		if msg.State == proto.States.NewOrder.S() {
			msg.Text = "приветствие"
		}

		msg.DFAnswer, err = h.DF.DetectIntentText(msg.Text, msg.ChatMsgID, intentContext)
		msg.Order.OrderPrefs.DetectedIntent = msg.DFAnswer.Intent
		msg.Order.OrderPrefs.IntentSteps = addIntent(msg.DFAnswer.Intent, msg.Order.OrderPrefs.IntentSteps)
	}

	return err
}

//swapContext подменяет контекст в некоторых случаях, это связано с особенностями работы DialogFlow
//входящий контекст должен быть подмножеством контекстов интента, а не просто одни из
func swapContext(state string) string {
	switch state {
	//case proto.States.Welcome.S():
	//	return ""
	case proto.States.Taxi.Order.FixDeparture.S():
		return proto.States.Taxi.Order.CreateDraft.S()
	case proto.States.Taxi.Order.FixArrival.S():
		return proto.States.Taxi.Order.Departure.S()
	default:
		return state
	}
}

//храним intentHistoryLength последних распознований интентов
func addIntent(intent string, history []string) []string {
	var res []string

	if len(history) == 0 {
		return append(res, intent)
	}
	res = append(res, intent)

	le := len(history)
	if le < intentHistoryLength-1 {
		return append(res, history...)
	}

	return append(res, history[:intentHistoryLength-1]...)
}

//Getting address from
func addressFromIntet(answ dialogflow.NLPResponse) (string, error) {
	var full_address, street, building, house, letter, publicPlace, city string
	switch answ.Intent {
	case proto.States.Taxi.Order.Departure.S():
		street = DepartAddressStreet
		building = DepartAddressBuilding
		house = DepartAddressHouse
		letter = DepartAddressLetter
		publicPlace = DepartPublicPlace
		city = DepartCity
	case proto.States.Taxi.Order.Arrival.S():
		street = ArrivalAddressStreet
		building = ArrivalAddressBuilding
		house = ArrivalAddressHouse
		letter = ArrivalAddressLetter
		publicPlace = ArrivalPublicPlace
		city = ArrivalCity
	case string(proto.Consts.Intents.AddressIntent):
		street = DepartAddressStreet
		building = DepartAddressBuilding
		house = DepartAddressHouse
		letter = DepartAddressLetter
		publicPlace = DepartPublicPlace
		city = DepartCity
	}

	if val, ok := answ.Entities[city]; ok && len(val) > 0 {
		full_address = full_address + val + " "
	}

	if val, ok := answ.Entities[publicPlace]; ok && len(val) > 0 {
		full_address = full_address + val + " "
	}

	if val, ok := answ.Entities[street]; ok && len(val) > 0 {
		full_address = full_address + val
		//номер дома
		if bval, ok := answ.Entities[house]; ok && len(bval) > 0 {
			cutDot(&bval)
			if bval != "" {
				full_address = full_address + ", " + bval
			}
		}
		//номер корпуса
		if bval, ok := answ.Entities[building]; ok && len(bval) > 0 {
			if bval != "" {
				full_address = full_address + " " + bval
			}
		}
		//литер
		if bval, ok := answ.Entities[letter]; ok && len(bval) > 0 {
			if bval != "" {
				full_address = full_address + " " + bval
			}
		}

	}
	if full_address == "" {
		return "", errors.New("Address is empty")
	}
	return full_address, nil
}

func cutDot(s *string) {
	ival := strings.Split(*s, ".")
	*s = ival[0]
}

//setOrderRoute получает данные из интента по адресу и возвращает заказ, имя роута и ошибку
//также функци сохраняет полученный роут в тело самого заказа
func (h *Handler) setOrderRoute(ctx context.Context, intentAnswer *dialogflow.NLPResponse, routeType proto.Constant, localOrder *models.LocalOrders) (models.LocalOrders, string, error) {
	var order models.LocalOrders
	var route string

	if intentAnswer.Intent == "" {
		return order, route, errors.New("Intent are empty")
	}
	//берет адрес из интента
	adress, err := addressFromIntet(*intentAnswer)
	if err != nil {
		return order, route, errpath.Err(err)
	}

	//ищем роут
	routes, err := h.GetCRMAdresses(adress)
	if err != nil {
		return order, route, errpath.Err(err)
	}

	if len(routes) == 0 {
		return order, route, errpath.Errorf("routes list is empty")
	}
	route = routes[0].UnrestrictedValue

	if routeType == proto.System.RouteTypes.Arrival {
		if localOrder.OrderJSON.Routes[0].UnrestrictedValue == route {
			return order, route, errors.New("Arrival and Departure adresses are the same")
		}
	}
	//Сохраняем промежуточные точки
	order, err = h.DB.SaveOrderRoute(ctx, localOrder, routeType, routes[0])
	if err != nil {
		return order, route, errpath.Err(err)
	}

	return order, route, nil
}
