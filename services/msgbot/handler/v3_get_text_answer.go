package handler

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
)

func getAnswerTextAndButton(msg *models.ChatMsgFull) (answerType, error) {

	var templateText string
	var err error

	//получаем шаблонный текст
	if msg.DFAnswer.Answer != "" {
		templateText = msg.DFAnswer.Answer
	} else {
		templateText, err = proto.GetIntentText(proto.Constant(msg.State))
		if err != nil {
			return answerType{}, errors.Wrapf(err, "can't get intent text")
		}
	}

	//замешиваем его с данными из заказа
	answerText, err := mixedOrderDataToAnswer(templateText, msg)
	if err != nil {
		return answerType{}, errors.Wrapf(err, "failed to mix template and data")
	}

	//получаем кнопки
	answerButtons := getAnswerButtons(msg)

	return answerType{
		textAnswer:    answerText,
		buttonsAnswer: answerButtons,
	}, nil
}

//getErrorData возращает типовую ошибку в зависимости от статуса
func getErrorAnswer(state string) answerType {
	return answerType{
		textAnswer:    proto.GetErrorText(state),
		buttonsAnswer: proto.GetCreateTicketButton(),
	}
}

//getErrorData возращает ошибку если мы не поняли что имееет ввиду клиент
func getFallBackAnswer(state string) answerType {
	return answerType{
		textAnswer:    proto.GetFallBackText(state),
		buttonsAnswer: proto.GetCreateTicketButton(),
	}
}
