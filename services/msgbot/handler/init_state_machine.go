package handler

import (
	"context"
	"fmt"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"github.com/looplab/fsm"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/msgbot/models"
	"gitlab.com/faemproject/backend/faem/services/msgbot/proto"
	"net/http"
	"regexp"
	"strconv"
)

var allTaxiStates = []string{
	proto.States.Welcome.S(),
	proto.States.NewOrder.S(),
	proto.States.Food.Order.Create.S(),
	proto.States.Taxi.Order.CreateDraft.S(),
	proto.States.Taxi.Order.Departure.S(),
	proto.States.Taxi.Order.FixDeparture.S(),
	proto.States.Taxi.Order.FixArrival.S(),
	proto.States.Taxi.Order.Arrival.S(),
	proto.States.Taxi.Order.PaymentMethod.S(),
	proto.States.Taxi.Order.PaymentByCash.S(),
	proto.States.Taxi.Order.ChangeService.S(),
	proto.States.Taxi.Order.NeedPhone.S(),
	proto.States.Taxi.Order.SaveContact.S(),
	proto.States.Taxi.Order.ValidateContact.S(),
	proto.States.Taxi.Order.ValidateBeforeStart.S(),
	proto.States.Taxi.Order.OrderCreated.S(),
	proto.States.Taxi.Order.FindingDriver.S(),
	proto.States.Taxi.Order.SmartDistribution.S(),
	proto.States.Taxi.Order.OfferOffered.S(),
	proto.States.Taxi.Order.DriverAccepted.S(),
	proto.States.Taxi.Order.OfferCancelled.S(),
	proto.States.Taxi.Order.OrderStart.S(),
	proto.States.Taxi.Order.DriverFounded.S(),
	proto.States.Taxi.Order.OnTheWay.S(),
	proto.States.Taxi.Order.OnPlace.S(),
	proto.States.Taxi.Order.Waiting.S(),
	proto.States.Taxi.Order.OrderPayment.S(),
	proto.States.Taxi.Order.Finished.S(),
	proto.States.Taxi.Order.Cancelled.S(),
	proto.States.Taxi.Order.DriverNotFound.S(),
	proto.States.Taxi.CancelTicketCreation.S(),
	proto.States.Taxi.CreateTicket.S(),
	proto.States.Taxi.SaveTicket.S(),
	proto.States.Taxi.Order.RestoreOrder.S(),
	proto.States.Taxi.Order.CancelPromt.S(),
}

//второстепенные статусы, в которые мы должны возвращаться если откатываемся
var secondaryStates = []string{
	proto.States.Taxi.Order.SaveContact.S(),
	proto.States.Taxi.Order.ValidateBeforeStart.S(),
	proto.States.Taxi.CreateTicket.S(),
	proto.States.Taxi.SaveTicket.S(),
	proto.States.Taxi.Order.CancelPromt.S(),
	proto.Buttons.Actions.BackButton.D(),
	proto.Buttons.Actions.NoAction.D(),
	proto.Buttons.Actions.YesAction.D(),
	proto.Buttons.Actions.CancelTicketCreation.D(),
}

func isStateSecondary(state string) bool {
	for _, v := range secondaryStates {
		if v == state {
			return true
		}
	}
	return false
}

func (h *Handler) InitOrderStateFSM() *fsm.FSM {
	return fsm.NewFSM(
		proto.States.Welcome.S(),
		fsm.Events{
			{
				Name: proto.States.NewOrder.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.NewOrder.S(),
			},
			{
				Name: proto.States.Welcome.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Welcome.S(),
			},
			//TAXIF
			{
				Name: proto.States.Taxi.Order.CancelPromt.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.CancelPromt.S(),
			},
			{
				Name: proto.States.Taxi.Order.CreateDraft.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.CreateDraft.S(),
			},
			{
				Name: proto.States.Taxi.Order.Departure.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.Departure.S(),
			},
			{
				Name: proto.States.Taxi.Order.FixDeparture.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.FixDeparture.S(),
			},
			{
				Name: proto.States.Taxi.Order.FixArrival.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.FixArrival.S(),
			},
			{
				Name: proto.States.Taxi.Order.Arrival.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.Arrival.S(),
			},
			{
				Name: proto.States.Taxi.Order.PaymentMethod.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.PaymentMethod.S(),
			},
			{
				Name: proto.States.Taxi.Order.PaymentByCash.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.PaymentByCash.S(),
			},
			{
				Name: proto.States.Taxi.Order.ChangeService.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.ChangeService.S(),
			},
			{
				Name: proto.States.Taxi.Order.ValidateBeforeStart.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.ValidateBeforeStart.S(),
			},
			{
				Name: proto.States.Taxi.Order.NeedPhone.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.NeedPhone.S(),
			},
			{
				Name: proto.States.Taxi.Order.SaveContact.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.SaveContact.S(),
			},
			{
				Name: proto.States.Taxi.Order.ValidateContact.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.ValidateContact.S(),
			},
			{
				Name: proto.States.Taxi.Order.OrderCreated.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OrderCreated.S(),
			},
			//
			{
				Name: proto.States.Taxi.Order.FindingDriver.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.FindingDriver.S(),
			},
			{
				Name: proto.States.Taxi.Order.SmartDistribution.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.SmartDistribution.S(),
			},
			{
				Name: proto.States.Taxi.Order.OfferOffered.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OfferOffered.S(),
			},
			{
				Name: proto.States.Taxi.Order.DriverAccepted.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.DriverAccepted.S(),
			},
			{
				Name: proto.States.Taxi.Order.OfferCancelled.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OfferCancelled.S(),
			},
			{
				Name: proto.States.Taxi.Order.OrderStart.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OrderStart.S(),
			},
			{
				Name: proto.States.Taxi.Order.DriverFounded.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OrderStart.S(),
			},
			{
				Name: proto.States.Taxi.Order.OnTheWay.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OnTheWay.S(),
			},
			{
				Name: proto.States.Taxi.Order.OnPlace.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OnPlace.S(),
			},
			{
				Name: proto.States.Taxi.Order.Waiting.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.Waiting.S(),
			},
			{
				Name: proto.States.Taxi.Order.OrderPayment.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.OrderPayment.S(),
			},
			{
				Name: proto.States.Taxi.Order.Finished.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.Finished.S(),
			},
			//
			{
				Name: proto.States.Taxi.Order.Cancelled.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.Cancelled.S(),
			},
			{
				Name: proto.States.Taxi.Order.DriverNotFound.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.Order.DriverNotFound.S(),
			},
			{
				Name: proto.States.Taxi.CreateTicket.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.CreateTicket.S(),
			},
			{
				Name: proto.States.Taxi.CancelTicketCreation.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.CancelTicketCreation.S(),
			},
			{
				Name: proto.States.Taxi.SaveTicket.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Taxi.SaveTicket.S(),
			},

			//
			//FOOD
			{
				Name: proto.States.Food.Order.Create.S(),
				Src:  allTaxiStates,
				Dst:  proto.States.Food.Order.Create.S(),
			},
		},
		fsm.Callbacks{
			"before_" + proto.States.Taxi.Order.OrderCreated.S(): func(e *fsm.Event) {
				var err error
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}
				ctx := context.Background()

				err = h.FillTariff(&msg.Order.OrderJSON)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to fill tariff"))
				}

				err = h.DB.SaveLocalOrder(ctx, &msg.Order)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to save updated order"))
				}

				err = h.Pub.StartOrder(&msg.Order.OrderJSON)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to start order"))
				}

				//добавляем заказ в буфер
				//h.Buffers.WIPOrdersFull[msg.OrderUUID] = msg.Order
				h.Buffers.WIPOrders[msg.OrderUUID] = msg.Order.State

			},
			"before_" + proto.States.Taxi.Order.ValidateBeforeStart.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "entring " + proto.States.Taxi.Order.OrderCreated.S() + " state",
						"reason": "failed to valide callback input data",
					}).Error(err)
				}

				ctx := context.Background()

				//проверка зарегистрирован ли пользователь
				if msg.Order.OrderJSON.Client.UUID == "" && msg.Order.OrderJSON.Client.MainPhone == "" {
					//получаем пользователя
					user, err := h.DB.GetUser(ctx, msg.ClientMsgID, msg.Source)
					if err != nil {
						e.Cancel(errors.Wrap(err, "failed to get user from DB"))
						return
					}
					//есть ли у него заполненное поле телефона
					if user.Phone == "" {
						// Номера нет, кидаем запрос на получение номера
						msg.Order.OrderPrefs.JumpToState = proto.States.Taxi.Order.NeedPhone.S()
						msg.DFAnswer.Answer = ""
						h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
						return
					}
					//если у нас есть номер заполняем его
					msg.Order.OrderJSON.Client.MainPhone = "+" + user.Phone
					msg.Order.OrderJSON.CallbackPhone = "+" + user.Phone

					msg.Order.OrderPrefs.JumpToState = proto.States.Taxi.Order.OrderCreated.S()
					h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
					return
				}
				//если номер уже есть идем дальше
				msg.Order.OrderPrefs.JumpToState = proto.States.Taxi.Order.OrderCreated.S()
				h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
			},

			"before_" + proto.States.Taxi.Order.SaveContact.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "entring " + proto.States.Taxi.Order.OrderCreated.S() + " state",
						"reason": "failed to valide callback input data",
					}).Error(err)
				}

				err = h.sendVerificationNumber(msg)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to send verification number to contact"))
					return
				}
			},
			"before_" + proto.States.Taxi.Order.ValidateContact.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "entring " + proto.States.Taxi.Order.OrderCreated.S() + " state",
						"reason": "failed to valide callback input data",
					}).Error(err)
					e.Cancel(errors.Wrap(err, "failed to get input data"))
					return
				}

				token, err := h.checkVerificationNumber(msg)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed verify code"))
					return
				}

				msg.Order.OrderPrefs.Registrations.TokenResponse = token
				msg.Order.OrderPrefs.JumpToState = proto.States.Taxi.Order.ValidateBeforeStart.S()
				h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
				ctx := context.Background()
				err = h.DB.SaveLocalOrder(ctx, &msg.Order)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to save token response data to DB"))
					return
				}

				//SAVE CONTACT HERE
				//updateMsg, _ := msg.Payload.(*tgbotapi.Update)
				err = h.DB.SaveUserContact(ctx, proto.MessangerContact{
					PhoneNumber: msg.Order.OrderPrefs.Registrations.RegRequest.Phone,
					UserID:      msg.Order.ClientMsgID,
				}, msg.Source, token.ClientUUID)
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to save token response data to DB"))
					return
				}
			},
			"before_" + proto.States.Taxi.Order.Departure.S(): func(e *fsm.Event) {
				var err error
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}

				//если полученный адрес это координаты
				if msg.Type == proto.MsgTypes.Coordinates.S() &&
					(msg.Source == models.SourceTelegram || msg.Source == models.SourceWhatsApp) {
					var route structures.Route

					err = tool.SendRequest(
						http.MethodPost,
						h.Config.Settings.ClientURL+proto.System.CRMAdresses.FindAddress.S(),
						nil,
						structures.PureCoordinates{
							Lat:  msg.Order.OrderPrefs.Lat,
							Long: msg.Order.OrderPrefs.Lon,
						},
						&route)
					if err != nil {
						e.Cancel(errors.Wrap(err, "filed to find address by coordinates"))
					}
					msg.Order.OrderJSON.Routes = append([]structures.Route{}, route)

					err = h.DB.SaveLocalOrder(context.Background(), &msg.Order)
					if err != nil {
						e.Cancel(errors.Wrap(err, "filed to save local order after saving route"))
					}
				}

				ctx := context.Background()
				//если адрес из кнопки-колбека при исправлении адреса
				if msg.Order.OrderPrefs.FixAddressVariant != 0 {
					if msg.Order.OrderPrefs.FixAddressVariant == 99 {
						return
					}
					address := msg.Order.OrderPrefs.DepartureVariants[msg.Order.OrderPrefs.FixAddressVariant-1]
					msg.Order.OrderPrefs.FixAddressVariant = 0
					msg.Order, err = h.DB.SaveOrderRoute(ctx, &msg.Order, proto.System.RouteTypes.Departure, address)
					if err != nil {
						e.Cancel(errors.Wrap(err, "filed to save new order route on callback"))
					}
					return
				}

				if msg.DFAnswer.Intent != "" {

					//получаем и пытаемся сохранить роут в заказ
					msg.Order, _, err = h.setOrderRoute(ctx, &msg.DFAnswer, proto.System.RouteTypes.Departure, &msg.Order)
					if err != nil {
						e.Cancel(err)
					}
				}

			},
			"before_" + proto.States.Taxi.Order.Arrival.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}
				ctx := context.Background()

				if msg.Order.OrderPrefs.FixAddressVariant != 0 {
					if msg.Order.OrderPrefs.FixAddressVariant == 99 {
						return
					}
					address := msg.Order.OrderPrefs.ArrivalVariants[msg.Order.OrderPrefs.FixAddressVariant-1]
					msg.Order.OrderPrefs.FixAddressVariant = 0
					msg.Order, err = h.DB.SaveOrderRoute(ctx, &msg.Order, proto.System.RouteTypes.Arrival, address)
					if err != nil {
						e.Cancel(errors.Wrap(err, "filed to save new order route on callback"))
					}
					msg.Order.OrderPrefs.FixAddressVariant = 0
					return
				}

				//если в интенте что то есть то заполняем
				if msg.DFAnswer.Intent != "" {

					//если пытаемся указать адрес назначения пока адрес отправки не задан
					if len(msg.Order.OrderJSON.Routes) == 0 {
						msg.Order.OrderPrefs.JumpToState = proto.States.Taxi.Order.Arrival.S()
						h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
						_, _ = h.sendTelegramOrWhatsApp(msg.Order.Source, msg.ChatMsgID, &answerType{textAnswer: proto.TryToArrivWhileDepartEmpty})
						//value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
						//h.Telegram.SendMessage(value, proto.TryToArrivWhileDepartEmpty)
					}

					//если новый адрес введен ручками
					newOrder, arrivalRoute, err := h.setOrderRoute(ctx, &msg.DFAnswer, proto.System.RouteTypes.Arrival, &msg.Order)
					if err != nil {
						if arrivalRoute == msg.Order.OrderJSON.Routes[0].UnrestrictedValue {
							_, _ = h.sendTelegramOrWhatsApp(msg.Order.Source, msg.ChatMsgID, &answerType{textAnswer: proto.SameAddressesMessage})
							//value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
							//h.Telegram.SendMessage(value, proto.SameAddressesMessage)
							e.Cancel(errors.Wrap(err, "arrival and departure addressed are the same"))
						} else {
							e.Cancel(errors.Wrap(err, "filed to change order route"))
						}
						return
					}
					msg.Order = newOrder

				}

				if msg.Type == proto.MsgTypes.Coordinates.S() &&
					(msg.Source == models.SourceTelegram || msg.Source == models.SourceWhatsApp) {
					var route structures.Route

					err = tool.SendRequest(
						http.MethodPost,
						h.Config.Settings.ClientURL+proto.System.CRMAdresses.FindAddress.S(),
						nil,
						structures.PureCoordinates{
							Lat:  msg.Order.OrderPrefs.Lat,
							Long: msg.Order.OrderPrefs.Lon,
						},
						&route)
					if err != nil {
						e.Cancel(errors.Wrap(err, "filed to find address by coordinates"))
						return
					}

					if len(msg.Order.OrderJSON.Routes) == 1 {
						msg.Order.OrderJSON.Routes = append(msg.Order.OrderJSON.Routes, route)
					} else if len(msg.Order.OrderJSON.Routes) == 2 {
						msg.Order.OrderJSON.Routes[1] = route
					}
				}

				//если сервис пустой, заполняем
				if msg.Order.OrderJSON.ServiceUUID == "" {
					msg.Order.OrderJSON.ServiceUUID = h.Config.Settings.Preferences.DefaultServiceUUID
					//если мы пришли со смены тарифа, то надо посмотреть заполнен ли поле выбранного тарифа и не равно ли уже выбранному
				} else if msg.Order.OrderPrefs.Service != "" && msg.Order.OrderPrefs.Service != msg.Order.OrderJSON.ServiceUUID {
					msg.Order.OrderJSON.ServiceUUID = msg.Order.OrderPrefs.Service
				}

				err = h.FillTariff(&msg.Order.OrderJSON)
				if err != nil {
					e.Cancel(errors.Wrap(err, "filed to fill tariff"))
					return
				}

				// записываем варианты тарифов
				if len(msg.Order.OrderJSON.Routes) >= 2 {
					var tariffButs []proto.TariffProto
					tariffs, err := h.GetTariffs(msg.Order.OrderJSON)
					if err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event": "failed to get tarriff",
						}).Error(err)
					}
					for _, v := range tariffs {
						tariffButs = append(tariffButs, proto.TariffProto{
							ServiceUUID:     v.ServiceUUID,
							ServiceImage:    v.ServiceImage,
							Name:            v.Name,
							Currency:        v.Currency,
							BonusPayment:    v.BonusPayment,
							MaxBonusPayment: v.MaxBonusPayment,
							TotalPrice:      v.TotalPrice,
						})
					}
					msg.Order.OrderPrefs.Tariffs = tariffButs
				}

				//очищаем кнопку - "адрес подачи не верный"
				val, ok := msg.Order.OrderPrefs.MsgsIDs[proto.States.Taxi.Order.Departure.S()]
				if ok {
					value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
					err = h.Telegram.UpdateKeyboard(value, val, proto.ButtonsSet{})
					if err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event": "clearing keyboard on entering state",
						}).Error(err)
					}
					delete(msg.Order.OrderPrefs.MsgsIDs, proto.States.Taxi.Order.Departure.S())
				}

				msg.Order.OrderPrefs.FixAddressVariant = 0

				err = h.DB.SaveLocalOrder(ctx, &msg.Order)
				if err != nil {
					e.Cancel(errors.Wrap(err, "filed to save local order"))
				}
			},

			"before_" + proto.States.Taxi.Order.Cancelled.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}

				// отправить action на отмену заказа в кролик
				err = h.Pub.ActionOnOrder(&structures.ActionOnOrder{
					OrderUUID: msg.OrderUUID,
					Action:    structures.ActionOnOrderCancelOrder,
				})
				if err != nil {
					e.Cancel(errors.Wrap(err, "failed to send cancel command"))
				}

			},
			"before_" + proto.States.Taxi.Order.FixDeparture.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}

				routes, err := h.GetCRMAdresses(msg.Order.OrderJSON.Routes[0].UnrestrictedValue)
				if err != nil {
					e.Cancel(errors.Wrap(err, "cant find adresses for departure"))
				}
				if len(routes) >= 5 {
					routes = routes[:5]
				}

				msg.Order.OrderPrefs.DepartureVariants = routes

				err = h.DB.SaveLocalOrder(context.Background(), &msg.Order)
				if err != nil {
					e.Cancel(err)
				}
			},
			"before_" + proto.States.Taxi.Order.FixArrival.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}

				if len(msg.Order.OrderJSON.Routes) < 2 {
					e.Cancel(errors.Wrap(err, "less then 2 routes"))
				}
				routes, err := h.GetCRMAdresses(msg.Order.OrderJSON.Routes[1].UnrestrictedValue)
				if err != nil {
					e.Cancel(errors.Wrap(err, "cant find adresses for departure"))
				}
				if len(routes) >= 5 {
					routes = routes[:5]
				}

				msg.Order.OrderPrefs.ArrivalVariants = routes

				err = h.DB.SaveLocalOrder(context.Background(), &msg.Order)
				if err != nil {
					e.Cancel(err)
				}
			},

			"before_" + proto.States.Taxi.SaveTicket.S(): func(e *fsm.Event) {
				msg, err := validateInput(e)
				if err != nil {
					e.Cancel(err)
				}
				//send ticket action
				//update, _ := msg.Payload.(*tgbotapi.Update)

				//сообщение о создании тикета
				_, _ = h.sendTelegramOrWhatsApp(msg.Order.Source, msg.ChatMsgID, &answerType{textAnswer: proto.TicketCreated})
				//value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
				//h.Telegram.SendMessage(value, proto.TicketCreated)

				//отмечаем получателя как суппорт систем
				msg.Order.OrderPrefs.MsgReciever = string(structures.MsgBotSupportReciever)

				//ищем статус в который нужно вернуть диалог после сохранения сообщения
				var nextState string
				steps := msg.Order.OrderPrefs.IntentSteps
				if len(steps) > 0 {
					for _, v := range steps {
						if !isStateSecondary(v) {
							nextState = v
						}
					}
				}

				//если такой не нашел то в самый первый
				if nextState == "" {
					nextState = proto.States.Taxi.Order.CreateDraft.S()
				}

				msg.Order.OrderPrefs.JumpToState = nextState
				h.Buffers.WIPOrdersFull[msg.Order.OrderUUID] = msg.Order
			},
			"leave_" + proto.States.Taxi.Order.FixArrival.S(): func(e *fsm.Event) {
				var err error
				msg, err := validateInput(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event": "validating input on enter state",
					}).Error(err)
					return
				}
				if msg.Type == proto.MsgTypes.TelegramMessage.S() {
					val, ok := msg.Order.OrderPrefs.MsgsIDs[proto.States.Taxi.Order.FixArrival.S()]
					if !ok {
						return
					}
					value, _ := strconv.ParseInt(msg.ChatMsgID, 0, 64) // TODO: add handler
					err = h.Telegram.UpdateKeyboard(value, val, proto.ButtonsSet{})
					if err != nil {
						logs.Eloger.WithFields(logrus.Fields{
							"event":  "deleting FixDeparture keyboard",
							"msgID":  msg.MsgID,
							"chatID": msg.ChatMsgID,
						}).Error(err)
					}
				}
			},
			"enter_state": func(e *fsm.Event) {
				var err error
				msg, err := validateInput(e)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event": "validating input on enter state",
					}).Error(err)
					return
				}

				//state := structures.OfferStates{
				//	OrderUUID: msg.OrderUUID,
				//	State:     e.Dst,
				//}
				msg.Order.State = e.Dst
				msg.Order.OrderPrefs.DetectedIntent = ""

				//в некоторых статусах, не надо сохранять статус заказа в БД
				err = h.DB.SaveLocalOrder(context.Background(), &msg.Order)
				//msg.Order, err = h.DB.SetOrderState(context.Background(), state)
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "setting order state",
						"reason": "db error",
					}).Error(err)
					return
				}

				msg.State = msg.Order.State

				//удалеям из буфера действующиз заказов
				if constants.InactiveOrderStates(msg.State) {
					delete(h.Buffers.WIPOrders, msg.OrderUUID)
				} else {
					h.Buffers.WIPOrders[msg.OrderUUID] = msg.Order.State
				}
			},
		},
	)
}

func (h *Handler) checkVerificationNumber(msg *models.ChatMsgFull) (models.TokenResponse, error) {

	var verResp models.TokenResponse

	type (
		VerificationCode struct {
			DeviceID  string `json:"device_id"`
			Code      int    `json:"code"`
			UTMSource string `json:"utm_source"` // метка-название промокода при регистрации для новых клиентов
		}
	)

	code, err := strconv.Atoi(msg.Text)
	if err != nil {
		return verResp, errors.New(fmt.Sprintf("Failed to get integer from code"))
	}

	verReq := VerificationCode{
		DeviceID:  msg.Order.OrderPrefs.Registrations.RegRequest.DeviceID,
		Code:      code,
		UTMSource: "",
	}

	url := fmt.Sprintf("%s/auth/verification", h.Config.Settings.ClientURL)
	err = h.RPC("POST", url, verReq, &verResp)
	if err != nil {
		return verResp, errors.Wrap(err, "failed to verify code")
	}

	return verResp, nil
}

func (h *Handler) sendVerificationNumber(msg *models.ChatMsgFull) error {

	var getMyContact proto.MessangerContact

	if msg.Source == models.SourceTelegram && msg.Type == proto.MsgTypes.TelegramContact.S() {
		fullMsg, ok := msg.Payload.(*tgbotapi.Update)
		if !ok {
			return errors.New(fmt.Sprintf("Cant transform interface to ChatMsgFull on get user data functuon"))
		}
		getMyContact.PhoneNumber = fullMsg.Message.Contact.PhoneNumber
		getMyContact.FirstName = fullMsg.Message.Contact.FirstName
		getMyContact.LastName = fullMsg.Message.Contact.LastName
	} else {
		getMyContact.PhoneNumber = msg.Text
	}

	getMyContact.PhoneNumber = getPhone(getMyContact.PhoneNumber)
	if getMyContact.PhoneNumber == "" {
		return errors.New(fmt.Sprintf("Failed to validate phone"))
	}
	//записываем во временную структуру полученные данные контакта
	getMyContact.UserID = msg.Order.ClientMsgID
	msg.Order.OrderPrefs.Registrations.ContactData = getMyContact

	//готовим и отправляем структуру для запроса на клиентское
	pload := models.ClientRegistrRequest{
		DeviceID: msg.Source + "_" + msg.Order.ClientMsgID,
		Phone:    getMyContact.PhoneNumber,
	}
	var resp models.ClientRegistrResponse

	url := fmt.Sprintf("%s/auth/new", h.Config.Settings.ClientURL)
	err := h.RPC("POST", url, pload, &resp)
	if err != nil {
		return errors.Wrap(err, "failed to make RPC request to client service")
	}
	msg.Order.OrderPrefs.Registrations.RegResponse = resp
	msg.Order.OrderPrefs.Registrations.RegRequest = pload
	//сохраняем промежуточный результат
	err = h.DB.SaveLocalOrder(context.Background(), &msg.Order)
	if err != nil {
		return errors.Wrap(err, "failed to save contact data to DB")
	}

	return nil
}

func getPhone(phone string) string {
	reg, _ := regexp.Compile("[^0-9]+")
	numsOnly := reg.ReplaceAllString(phone, "")
	if len(numsOnly) < 10 {
		return ""
	}
	if len(numsOnly) == 10 {
		return "+7" + numsOnly
	}
	if string(numsOnly[0]) == "8" || string(numsOnly[0]) == "7" {
		return "+7" + numsOnly[1:]
	}
	return ""
}

////saveClientContact УБРАТЬ?
//func (h *Handler) saveClientContact(msg *models.ChatMsgFull) error {
//	fullMsg, ok := msg.Payload.(*tgbotapi.Update)
//	if !ok {
//		return errors.New(fmt.Sprintf("Cant transform interface to ChatMsgFull on get user data function"))
//	}
//	getMyContact := proto.MessangerContact{
//		PhoneNumber: fullMsg.Message.Contact.PhoneNumber,
//		FirstName:   fullMsg.Message.Contact.FirstName,
//		LastName:    fullMsg.Message.Contact.LastName,
//		UserID:      strconv.Itoa(fullMsg.Message.Contact.UserID),
//	}
//	if getMyContact.PhoneNumber == "" {
//		return errors.New(fmt.Sprintf("Empty phone number cant register"))
//	}
//	ctx := context.Background()
//	err := h.DB.SaveUserContact(ctx, getMyContact, "telegram", "")
//	if err != nil {
//		return errors.Wrap(err, "failed to save user contact")
//	}
//	return nil
//}

func validateInput(e *fsm.Event) (*models.ChatMsgFull, error) {
	if len(e.Args) < 1 {
		er := fmt.Sprintf("Didn't get ChatMsgFull as argument on `leave_state`. From %s to %s", e.Src, e.Dst)
		return &models.ChatMsgFull{}, errors.New(er)
	}
	msg, ok := e.Args[0].(*models.ChatMsgFull)
	if !ok {
		er := fmt.Sprintf("Cant transform interface to ChatMsgFull on `leave_state`. From %s to %s", e.Src, e.Dst)
		return &models.ChatMsgFull{}, errors.New(er)
	}
	return msg, nil
}
