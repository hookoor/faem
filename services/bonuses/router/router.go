package router

import (
	// cfg "gitlab.com/faemproject/backend/faem/services/crm/config"

	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/sirupsen/logrus"
	h "gitlab.com/faemproject/backend/faem/services/bonuses/handlers"
)

const apiversion = "v1"

// Init - binding middleware and setup routers
func Init(l *logrus.Logger) *echo.Echo {

	e := echo.New()
	web.UseHealthCheck(e)

	// logrus.SetLevel(logrus.DebugLevel)
	// e.Use(logrusmiddleware.Hook())
	newLogger(l)
	e.Use(Hook())

	e.Use(middleware.Recover())
	e.Use(middleware.CORS())
	jwtSec := config.JWTSecret()
	// Metrics
	//p := prometheus.NewPrometheus("echo", nil)
	//p.Use(e)
	//e.Use(prometheus.NewMetric())
	//e.GET("/metrics", echo.WrapHandler(promhttp.Handler()))

	closedGroup := e.Group("/api/" + apiversion)
	opengroup := e.Group("/api/" + apiversion)
	closedGroup.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(jwtSec),
	}))
	closedGroup.POST("/autowriteoff", h.SaveAutoWriteOffData)
	closedGroup.POST("/promo/activate", h.ActivatePromocode)
	closedGroup.POST("/promo/create", h.CreatePromocode)
	closedGroup.POST("/promo/sync", h.Sync)
	closedGroup.GET("/promo/all", h.GetAllPromocodes)
	closedGroup.PUT("/promo/update/:uuid", h.UpdatePromocode)
	closedGroup.DELETE("/promo/delete/:uuid", h.DeletePromocode)

	closedGroup.POST("/promotions", h.CreatePromotion)
	closedGroup.PUT("/promotions/:uuid", h.UpdatePromotion)
	closedGroup.GET("/promotions", h.GetAllPromotions)
	closedGroup.DELETE("/promotions/:uuid", h.DeletePromotion)

	closedGroup.GET("/accruals/filter", h.GetAccrualsByFilter)

	opengroup.POST("/bonusesdata", h.GetBonusesDataByPhone)

	opengroup.POST("/newregpromocode", h.GetNewRegPromocode)

	{
		closedGroup.POST("/accrualbyphones", h.AccrualByPhones)
	}

	return e
}
