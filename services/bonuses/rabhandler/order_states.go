package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func initOrderStates() error {
	receiverChannelOrderState, err := rb.GetReceiver(rabbit.BonusesOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannelOrderState.QueueDeclare(
		rabbit.BonusesOrderStateQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения статусов заказов
	err = receiverChannelOrderState.QueueBind(
		q.Name,                                // queue name
		"state."+constants.OrderStateFinished, // routing key
		rabbit.OrderExchange,                  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера
	msgs, err := receiverChannelOrderState.Consume(
		q.Name,                           // queue
		rabbit.BonusesOrderStateConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go receiveOrderStates(msgs)
	return nil
}

func receiveOrderStates(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			go handleOrderState(d)
		}
	}
}

func handleOrderState(d amqp.Delivery) {
	publisher, ok := d.Headers["publisher"].(string)

	var (
		rqOrState structures.OfferStates
	)
	err := json.Unmarshal([]byte(d.Body), &rqOrState)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "handling new order state from broker",
		"orderUUID": rqOrState.OrderUUID,
		"state":     rqOrState.State,
		"offerUUID": rqOrState.OfferUUID,
		"publisher": publisher,
	})
	if !ok {
		log.WithFields(logrus.Fields{
			"reason": "error parse publisher header",
		}).Error(err)
		return
	}
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "Error unmarshalling order state",
		}).Error(err)
		return
	}
	err = models.CheckAndApplyPromotions(rqOrState.OrderUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"reason": "Error apply promotions",
		}).Error(err)
		return
	}

	err = models.ApplyReferral(rqOrState.OrderUUID)
	if err != nil {
		log.WithField("reason", "Error apply referral").Error(err)
		return
	}

	log.Info("order state handler")
}
