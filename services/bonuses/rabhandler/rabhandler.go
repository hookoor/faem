package rabhandler

import (
	"sync"
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

var (
	db *pg.DB

	rb     *rabbit.Rabbit
	wg     *sync.WaitGroup
	closed = make(chan struct{})
)

func InitVars(r *rabbit.Rabbit, conn *pg.DB, waitG *sync.WaitGroup) {
	rb = r
	db = conn
	wg = waitG
}

func StartService() error {
	err := initTransfers()
	if err != nil {
		return err
	}
	err = initOrderStates()
	if err != nil {
		return errpath.Err(err)
	}
	err = initEventNewClient()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("broker stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("broker stopped forcibly")
	}
}
