package rabhandler

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func initEventNewClient() error {
	receiverChannelOrderState, err := rb.GetReceiver(rabbit.BonusesOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannelOrderState.QueueDeclare(
		rabbit.BonusesEventNewClientQueue,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	err = receiverChannelOrderState.QueueBind(
		q.Name,
		rabbit.NewKey,
		rabbit.ClientExchange,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannelOrderState.Consume(
		q.Name,
		rabbit.BonusesEventNewClientConsumer,
		true,
		false,
		false,
		false,
		nil,
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go receiveEventNewClient(msgs)
	return nil
}

func receiveEventNewClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			go handleEventNewClient(d)
		}
	}
}

func handleEventNewClient(d amqp.Delivery) {

	publisher, ok := d.Headers["publisher"].(string)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "handling new client",
		"publisher": publisher,
	})

	if !ok {
		log.WithFields(logrus.Fields{
			"reason": "error parse publisher header",
		}).Error(errpath.Errorf("cant parse publisher"))
		return
	}

	var client structures.Client

	err := json.Unmarshal([]byte(d.Body), &client)
	if err != nil {
		log.WithField("reason", "Error unmarshalling client").Error(errpath.Err(err).Error())
		return
	}

	log.WithFields(logrus.Fields{
		"client_uuid":                client.UUID,
		"client.MainPhone":           client.MainPhone,
		"client.ReferralProgramData": client.ReferralProgramData,
	})

	cfg := config.GetCurrentReferralSystemParamsConfig()

	if client.ReferralProgramData.ReferralParentUUID != "" {
		err = models.AccrueBonuses(
			cfg.AddForRecipient,
			client.UUID,
			client.MainPhone,
			structures.ReferralMessage,
			time.Now().Add(180*24*time.Hour))
		if err != nil {
			log.WithField("reason", "Error accrue bonuses").Error(errpath.Err(err).Error())
			return
		}
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":      "handling new client",
		"clientUUID": client.UUID,
	}).Info("CRM created new client")

}
