package rabhandler

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func initTransfers() error {
	receiverChannel, err := rb.GetReceiver(rabbit.BonusesOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}
	// Declare an exchange first
	err = receiverChannel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}
	q, err := receiverChannel.QueueDeclare(
		rabbit.BonusesNewTransfersQueue, // name
		true,                            // durable
		false,                           // delete when unused
		false,                           // exclusive
		false,                           // no-wait
		nil,                             // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения статусов заказов
	err = receiverChannel.QueueBind(
		q.Name,                 // queue name
		rabbit.NewKey,          // routing key
		rabbit.BillingExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                         // queue
		rabbit.BonusesTransferConsumer, // consumer
		true,                           // auto-ack
		false,                          // exclusive
		false,                          // no-local
		false,                          // no-wait
		nil,                            // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go receiveNewTransfer(msgs)
	return nil
}

func receiveNewTransfer(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			go handleNewTransfer(d)
		}
	}
}

func handleNewTransfer(d amqp.Delivery) {
	publisher, ok := d.Headers["publisher"].(string)

	var (
		data structures.NewTransfer
	)
	err := json.Unmarshal([]byte(d.Body), &data)

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":       "handling new transfer from broker",
		"clientPhone": data.Meta.ClientPhone,
		"amount":      data.Amount,
		"accType":     data.PayerAccountType,
		"publisher":   publisher,
	})
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error unmarshalling data"}).Error(err)
		return
	}
	if !ok {
		log.WithFields(logrus.Fields{"reason": "error parse publisher header"}).Error(err)
		return
	}
	if publisher == "bonuses" ||
		data.PayerAccountType != structures.AccountTypeBonus ||
		data.PayerType != structures.UserTypeClient ||
		data.TransferType != structures.TransferTypeWithdraw {
		log.Debug("no processing required")
		return
	}
	err = models.WriteOff(data.Meta.ClientPhone, int(data.Amount))
	if err != nil {
		log.WithFields(logrus.Fields{"reason": "error write off"}).Error(err)
		return
	}
	log.Info("new transfer handler")
}
