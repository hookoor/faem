package tickers

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

const (
	checkTariffExpirationInterval = time.Minute
)

func InitExpiredBonusesTicker() {
	wg.Add(1)
	go checkExpiredBonuses()
}

func checkExpiredBonuses() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkTariffExpirationInterval):
			log := logs.Eloger.WithField("event", "check expired bonuses")

			neededAccruals, err := models.GetExpireddAccruals()
			if err != nil {
				log.WithField("reason", "can't get drivers with expired tariff").Error(err)
				continue
			}
			if len(neededAccruals) < 1 {
				log.Debug("no expired  accruals")
				continue
			}

			for i := range neededAccruals {
				falseParam := false
				neededAccruals[i].Burned += neededAccruals[i].Available
				neededAccruals[i].Active = &falseParam
				neededAccruals[i].Available = 0
			}

			if err := models.UpdateAccruals(neededAccruals); err != nil {
				log.WithField("reason", "cant update accruals").Error(err)
				continue
			}

			uuids, err := models.GeClientsUUIDByPhones(getAllClientPhones(neededAccruals))
			if err := models.UpdateAccruals(neededAccruals); err != nil {
				log.WithField("reason", "cant get uuids by phones").Error(err)
				continue
			}

			for _, acc := range neededAccruals {
				if acc.Burned <= 0 {
					continue
				}
				msg := fmt.Sprintf("Истечение срока действия бонусов по промокоду (%s)", acc.Code)
				if acc.Type == models.Promotions {
					msg = fmt.Sprintf("Истечение срока действия бонусов, начисленных за выполнение условий промоакций (%s)", acc.RuleName)
				}
				err = models.ChangeBonuses(acc.ClientPhone, msg, uuids[acc.ClientPhone], acc.Burned, structures.TransferTypeWithdraw)
				if err != nil {
					log.WithField("reason", "cant change bonuses").Error(err)
				}
			}
		}
	}
}

func getAllClientPhones(arr []models.Accruals) []string {
	var res []string
	for _, acc := range arr {
		res = append(res, acc.ClientPhone)
	}
	return res
}
