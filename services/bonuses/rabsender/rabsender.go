package rabsender

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
)

var (
	rb                  *rabbit.Rabbit
	wg                  *sync.WaitGroup
	AcNewPushMailing    = "push_mailing"
	AcChangeBonuses     = "bon_change"
	AcReferralFirstRide = "ref_first_ride"
)

func Init(r *rabbit.Rabbit, waiG *sync.WaitGroup) error {
	rb = r
	wg = waiG
	return nil
}

// SendJSONByAction выбираeм экшен и отправляем
func SendJSONByAction(action string, payload interface{}, key ...string) error {
	var err error
	switch action {
	case AcChangeBonuses:
		err = SendJSON(rabbit.BillingExchange, rabbit.NewKey, payload)
	case AcNewPushMailing:
		err = SendJSON(rabbit.FCMExchange, rabbit.MailingKey, payload)
	case AcReferralFirstRide:
		err = SendJSON(rabbit.ClientExchange, rabbit.ReferralFirstRideKey, payload)
	default:
		err = fmt.Errorf("Unknown action, cant send")
	}
	if err != nil {
		return err
	}
	return nil
}

// SendJSON сабж
func SendJSON(exchange string, key string, payload interface{}) error {
	wg.Add(1)
	defer wg.Done()

	amqpHeader := amqp.Table{}
	amqpHeader["publisher"] = "bonuses"

	pl, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	senderChannel, err := rb.GetSender("clientSender")
	if err != nil {
		return errors.Wrap(err, "failed to get a sender channel")
	}

	err = senderChannel.Publish(
		exchange, // exchange
		key,      // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         pl,
			Headers:      amqpHeader,
			DeliveryMode: amqp.Persistent,
		})
	keyExch := fmt.Sprintf("key=%s, exchange=%s", key, exchange)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "Sending to Rabbit [bonuses]",
			"value": keyExch,
		}).Error(err)
		return err
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event": "Sending to Rabbit [bonuses]",
		"value": keyExch,
	}).Debug("Sended: OK!")

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("publisher stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("publisher stopped forcibly")
	}
}
