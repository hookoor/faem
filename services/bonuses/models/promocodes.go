package models

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/bonuses/rabsender"
)

type (
	RecordsWithCount struct {
		Records interface{} `json:"records"`
		Count   int         `json:"records_count"`
	}
	Promocode struct {
		tableName    struct{}     `sql:"bon_promocodes" pg:",discard_unknown_columns"`
		UUID         string       `json:"uuid"`
		Comment      *string      `json:"comment"`
		Code         string       `json:"code"`
		Type         AccrualTypes `json:"type"`
		Synonyms     []string     `json:"synonyms" sql:",type:text[]"`
		BonusesCount int          `json:"bonuses_count"`
		ClientPhone  string       `json:"client_phone"`
		CreatorData  struct {
			Login string `json:"login"`
			UUID  string `json:"uuid"`
		} `json:"creator_data"`
		ActivationCount     int       `json:"activation_count"`
		ActivationLimit     int       `json:"activation_limit"`
		Expiration          time.Time `json:"-"`
		ExpirationUnix      int64     `json:"expiration_time_unix" sql:"-"`
		BonusesBurnTime     time.Time `json:"-"`
		BonusesBurnTimeUnix int64     `json:"bonuses_burn_time_unix" sql:"-"`
		CreatedAt           time.Time `json:"-"`
		CreatedAtUnix       int64     `json:"created_at_unix" sql:"-"`
		Deleted             bool      `json:"-"`
	}
)

func (pc *Promocode) GetComment() string {
	if pc == nil || pc.Comment == nil {
		return ""
	}
	return *pc.Comment
}

func GetAllPromocodes(pager urlvalues.Pager) ([]Promocode, int, error) {
	var res []Promocode
	query := db.Model(&res).
		Where("deleted is not true")

	count, err := query.
		Count()
	if err != nil || count == 0 {
		return res, count, err
	}
	err = query.
		Order("created_at desc").
		Apply(pager.Pagination).
		Select()
	for i := range res {
		res[i].ExpirationUnix = res[i].Expiration.Unix()
		res[i].BonusesBurnTimeUnix = res[i].BonusesBurnTime.Unix()
		res[i].CreatedAtUnix = res[i].CreatedAt.Unix()
	}
	return res, count, err
}

// GetPromocodeByCodeName -
func (pc *Promocode) GetPromocodeByCodeName(ctx context.Context, codeName string) error {
	err := db.ModelContext(ctx, pc).
		Where("code = ?", codeName).
		Where("deleted is not true").
		Select()
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}
func (pc *Promocode) Update(uuid string) *structures.ErrorWithLevel {
	pc.UUID = uuid
	if errWL := pc.validateUpdate(); errWL != nil {
		return errWL
	}
	pc.ActivationCount = 0
	_, err := db.Model(pc).
		Where("uuid = ?uuid").
		UpdateNotNull()
	return structures.Error(err)
}
func SetDeletePromocode(uuid string) error {
	_, err := db.Model(&Promocode{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}
func (pc *Promocode) Create() *structures.ErrorWithLevel {
	if errWL := pc.validate(); errWL != nil {
		return errWL
	}
	pc.UUID = structures.GenerateUUID()
	pc.ActivationCount = 0
	_, err := db.Model(pc).
		Insert()
	return structures.Error(err)
}
func (pc *Promocode) validateUpdate() *structures.ErrorWithLevel {

	if pc.BonusesCount < 0 {
		return structures.Warning(fmt.Errorf("Введите кол-во бонусов"))
	}
	if pc.Expiration.Unix() > 0 && pc.Expiration.Unix() <= time.Now().Unix() {
		return structures.Warning(fmt.Errorf("Неккоректное время истечения"))
	}
	if pc.BonusesBurnTime.Unix() > 0 && pc.BonusesBurnTime.Unix() <= time.Now().Unix() {
		return structures.Warning(fmt.Errorf("Неккоректное время сгорания бонусов"))
	}
	pc.ActivationCount = 0

	if pc.Code == "" && pc.Synonyms == nil {
		return nil
	}
	check, err := db.Model(&Promocode{}).
		Where("code = ? or synonyms && ?", pc.Code, pg.Array(append(pc.Synonyms, pc.Code))).
		Where("CURRENT_TIMESTAMP < expiration").
		Where("deleted is not true").
		Where("uuid != ?", pc.UUID).
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if check {
		return structures.Warning(fmt.Errorf("Такой промокод уже существует или нарушает уникальность синонимов. Дождитесь окончания его действия или удалите его"))
	}

	return nil
}
func (pc *Promocode) validateType() *structures.ErrorWithLevel {
	if pc.Type == "" {
		return structures.Warning(fmt.Errorf("Укажите тип промокода"))
	}
	if pc.Type != Compensation &&
		pc.Type != Coupon &&
		pc.Type != Initial {
		return structures.Warning(fmt.Errorf("Неверный тип промокода"))
	}

	return nil
}

func (pc *Promocode) validate() *structures.ErrorWithLevel {
	if pc.Code == "" {
		return structures.Warning(fmt.Errorf("Введите код"))
	}
	if pc.BonusesCount <= 0 {
		return structures.Warning(fmt.Errorf("Введите кол-во бонусов"))
	}
	if pc.Expiration.Unix() <= time.Now().Unix() {
		return structures.Warning(fmt.Errorf("Неккоректное время истечения"))
	}
	if pc.BonusesBurnTime.Unix() <= time.Now().Unix() {
		return structures.Warning(fmt.Errorf("Неккоректное время сгорания бонусов"))
	}
	if pc.BonusesBurnTime.Unix() < pc.Expiration.Unix() {
		return structures.Warning(fmt.Errorf("Бонусы не могут сгореть раньше чем закончится время действия промокода"))
	}
	if errWL := pc.validateType(); errWL != nil {
		return errWL
	}
	synAndCode := append(pc.Synonyms, pc.Code)
	check, err := db.Model(&Promocode{}).
		Where("code = ? or synonyms && ? or code in (?)", pc.Code, pg.Array(synAndCode), pg.In(synAndCode)).
		Where("CURRENT_TIMESTAMP < expiration").
		Where("deleted is not true").
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if check {
		return structures.Warning(fmt.Errorf("Такой промокод уже существует или нарушает уникальность синонимов. Дождитесь окончания его действия или удалите его"))
	}
	return nil
}

func ActivateCode(code, clientPhone, clientUUID string, clientRegTime int64, source structures.ChatMembers) *structures.ErrorWithLevel {
	var pc Promocode
	err := db.Model(&pc).
		Where("code = ? or synonyms && ?", code, pg.Array([]string{code})).
		Where("deleted is not true").
		Order("created_at desc").
		Limit(1).
		Select()
	if err != nil {
		switch err == pg.ErrNoRows {
		case true:
			return structures.Warning(fmt.Errorf("Нет такого кода :("))
		case false:
			return structures.Error(fmt.Errorf("error select code,%s", err))
		}
	}
	if pc.ClientPhone != "" && pc.ClientPhone != clientPhone {
		return structures.Warning(fmt.Errorf("Этот код недоступен для вас"))
	}
	if pc.Expiration.Unix() < time.Now().Unix() {
		return structures.Warning(fmt.Errorf("Срок действия этого кода истек"))
	}
	if pc.ActivationCount >= pc.ActivationLimit {
		return structures.Warning(fmt.Errorf("Для этого кода исчерпан лимит использований"))
	}
	newAc := Accruals{
		Source:         source,
		Code:           code,
		UUID:           structures.GenerateUUID(),
		Type:           pc.Type,
		BonusesAccrued: pc.BonusesCount,
		Available:      pc.BonusesCount,
		LifeTime:       pc.BonusesBurnTime,
		CodeUUID:       pc.UUID,
		ClientPhone:    clientPhone,
		CreatedAt:      time.Now(),
	}
	oleg := newAc.checkExistsAndSave(clientRegTime)
	if oleg != nil {
		return oleg
	}
	if clientUUID == "" {
		neededUUID, err := GeClientsUUIDByPhones([]string{clientPhone})
		if err != nil {
			return structures.Error(err)
		}
		clientUUID = neededUUID[clientPhone]
	}
	if clientUUID == "" {
		return structures.Errorf("empty client uuid")
	}

	transferType := structures.TransferTypeTopUp
	if pc.BonusesCount < 0 {
		transferType = structures.TransferTypeWithdraw
		pc.BonusesCount = -pc.BonusesCount
	}

	err = ChangeBonuses(clientPhone, fmt.Sprintf("Активация промокода (%s)", code), clientUUID, pc.BonusesCount, transferType)
	return structures.Error(err)
}
func ChangeBonuses(clientPhone, desc, clientUUID string, bonusesCount int, transType structures.BillingTransferType) error {
	bonusTransfer := structures.NewTransfer{
		IdempotencyKey:   structures.GenerateUUID(),
		TransferType:     transType,
		Description:      desc,
		PayerAccountType: structures.AccountTypeBonus,
		PayeeType:        structures.UserTypeClient,
		PayerType:        structures.UserTypeClient,
		PayerUUID:        clientUUID,
		PayeeUUID:        clientUUID,
		Amount:           float64(bonusesCount), // Сумма пополнения
		Meta: structures.TransferMetadata{ // Необязательно, в зависимости от типа контрагента заполняются следующие поля
			ClientID:    clientUUID,
			ClientPhone: clientPhone,
		},
	}
	return rabsender.SendJSONByAction(rabsender.AcChangeBonuses, bonusTransfer)
}

// ----------
// ----------
// ----------

// NewRegPromocode -
type NewRegPromocode struct {
	Promocode   string `json:"promocode"`
	ClientUUID  string `json:"client_uuid"`
	ClientPhone string `json:"phone"`
}

// GetNewRegPromocode - получение промокода для нового клиента и метка-проверка активации промокода
func GetNewRegPromocode(ctx context.Context, code NewRegPromocode) (Promocode, bool, error) {
	var prom Promocode
	err := prom.GetPromocodeByCodeName(ctx, code.Promocode)
	if err != nil {
		return prom, false, errpath.Err(err)
	}

	// был ли активирован промокод
	check, err := db.ModelContext(ctx, (*Accruals)(nil)).
		Where("code_uuid = ?", prom.UUID).
		Where("client_phone = ?", code.ClientPhone).
		Exists()
	if err != nil {
		return prom, false, errpath.Err(err)
	}
	if check {
		return prom, true, errpath.Errorf("Вы уже активировали этот код")
	}

	return prom, false, nil
}

// ----------
// ----------
// ----------
