package models

import (
	"context"
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type AutoWriteOff struct {
	tableName    struct{}               `sql:"bon_write_off" pg:",discard_unknown_columns"`
	Phone        string                 `json:"phone"`
	Condition    bool                   `json:"condition"`
	UpdatedAt    time.Time              `json:"-"`
	SwitcherType structures.ChatMembers `json:"switcher_type"`
	SwitcherUUID string                 `json:"switcher_uuid"`
}

func (ar *AutoWriteOff) Save() *structures.ErrorWithLevel {
	err := ar.validate()
	if err != nil {
		return structures.Warning(err)
	}
	ar.UpdatedAt = time.Now()
	_, err = db.Model(ar).
		OnConflict("(phone) DO UPDATE").
		Insert()
	return structures.Error(err)
}
func (ar *AutoWriteOff) validate() error {
	if ar == nil {
		return fmt.Errorf("ar is nil. contact the backend-man")
	}
	if ar.Phone == "" {
		return fmt.Errorf("phone is required field")
	}
	if ar.SwitcherType == "" || ar.SwitcherUUID == "" {
		return fmt.Errorf("switcher data is incomplete")
	}
	return nil
}

func GetAutoWFData(ctx context.Context, phone string) (result AutoWriteOff, checkExists bool, err error) {

	err = db.ModelContext(ctx, &result).
		Where("phone = ?", phone).
		Select()

	switch {
	case err == pg.ErrNoRows:
		err = nil
		return
	case err != nil:
		return
	default:
		checkExists = true
	}
	return
}
