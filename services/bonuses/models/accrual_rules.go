package models

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/antonmedv/expr"
	"github.com/go-pg/pg"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
	"gitlab.com/faemproject/backend/faem/services/bonuses/rabsender"
)

type AccrualRulesPreData struct {
	ClientOrdersNumber    int      `json:"client_orders_number"` //in the time span
	OrderServiceName      string   `json:"order_service_name"`
	OrderTotalPrice       int      `json:"order_total_price"`
	FirstPointZones       []string `json:"first_point_zones"`
	LastPointZones        []string `json:"last_point_zones"`
	OrderSource           string   `json:"order_source"`
	OrderCreationTimeUnix int64    `json:"order_creation_time_unix"`
	OrderCreationMonth    int      `json:"order_creation_month"`
	OrderCreationDay      int      `json:"order_creation_day"`
	OrderCreationHour     int      `json:"order_creation_hour"`
	OrderCreationMinute   int      `json:"order_creation_minute"`
}

type AccrualRules struct {
	tableName     struct{}  `sql:"bon_promotions" pg:",discard_unknown_columns"`
	UUID          string    `json:"uuid"`
	IfExpr        string    `json:"if_expr"`
	Name          string    `json:"name"`
	Active        *bool     `json:"active"`
	TimePeriod    int64     `json:"time_period"` //seconds
	AccrualExpr   string    `json:"accrual_expr"`
	CreatedAtUnix int64     `json:"created_at_unix" sql:"-"`
	CreatorUUID   string    `json:"creator_uuid"`
	CreatedAt     time.Time `json:"-"`
	Deleted       bool      `json:"-"`
}

const (
	orWithNumberEndpoint string = "/orders/withclienthistory/"
)

func (ar *AccrualRules) IsActive() bool {
	if ar == nil || ar.Active == nil {
		return false
	}
	return *ar.Active
}
func CheckAndApplyPromotions(orUUID string) error {
	or, err := GetOrderWithHistory(orUUID)
	if err != nil {
		return fmt.Errorf("error getting order with history,%s", err)
	}
	prData := getPreData(or)
	logrus.Warning(prData)
	bonusesNumber, activatedRules, err := prData.BonusesNumberByOrder(or)
	if err != nil {
		return fmt.Errorf("error getting bonuses number, %s", err)
	}
	if bonusesNumber <= 0 {
		return nil
	}
	msg := fmt.Sprintf("Начисление бонусов за выполнение условий промоакций (%s)", strings.Join(activatedRules, ", "))
	if or.Client.UUID == "" {
		neededUUID, err := GeClientsUUIDByPhones([]string{or.Client.MainPhone})
		if err != nil {
			return err
		}
		or.Client.UUID = neededUUID[or.Client.MainPhone]
	}
	if or.Client.UUID == "" {
		or.Client.UUID = or.Client.MainPhone //billing service allows providing phone instead uuid
	}

	_, err = CreatePromotionAccruals(bonusesNumber, or.Client.MainPhone, msg, time.Now().Add(365*24*time.Hour))
	if err != nil {
		return fmt.Errorf("error create new accruals, %s", err)
	}

	err = ChangeBonuses(or.Client.MainPhone, msg,
		or.Client.UUID,
		bonusesNumber,
		structures.TransferTypeTopUp)
	if err != nil {
		return fmt.Errorf("error change bonuses, %s", err)
	}
	tit, bod := getTitleAndBodyForClient(bonusesNumber, msg)
	return SendMessageToClients([]string{or.Client.MainPhone}, tit, bod)
}
func getTitleAndBodyForClient(bonusesNumber int, msg string) (string, string) {
	if bonusesNumber <= 0 {
		return "", ""
	}
	subStr := "начислено"
	subStr2 := "бонусов"
	remainder100 := bonusesNumber % 100
	if remainder100 < 5 || remainder100 > 20 {
		remainder10 := bonusesNumber % 10
		switch remainder10 {
		case 1:
			subStr = "начислен"
			subStr2 = "бонус"
		case 2, 3, 4:
			subStr2 = "бонуса"
		}
	}
	return fmt.Sprintf("Вам %s %v %s", subStr, bonusesNumber, subStr2), msg
}
func boolPointer(neededVal bool) *bool {
	return &neededVal
}
func SendMessageToClients(clientsPhones []string, title, message string) error {
	if len(clientsPhones) == 0 {
		return nil
	}
	return rabsender.SendJSONByAction(rabsender.AcNewPushMailing, structures.PushMailing{
		Type:          structures.MailingForFewClientType,
		TargetsPhones: clientsPhones,
		Title:         title,
		Message:       message,
	})
}

// ApplyReferral - начисление бонусов по реферальной системе
func ApplyReferral(referral string) error {

	var err error
	var order structures.Order

	url := config.St.CRM.Host + "/order/" + referral
	err = tool.SendRequest(http.MethodGet, url, nil, nil, &order)
	if err != nil {
		return errpath.Err(err)
	}

	// запрос на получение колва поездок
	var recipientTravelCount int
	err = tool.SendRequest(http.MethodGet,
		config.St.Client.Host+"/increcipientstravelcount/"+order.Client.ReferralProgramData.ParentUUID,
		nil, nil, &recipientTravelCount)
	if err != nil {
		return errpath.Err(err)
	}

	if recipientTravelCount < config.GetCurrentReferralSystemParamsConfig().BonusesForRecipientTravelCountLimit {
		err = AccrueBonuses(config.GetCurrentReferralSystemParamsConfig().DonorTravelBonusesCount,
			order.Client.ReferralProgramData.ParentUUID,
			order.Client.ReferralProgramData.ReferralParentPhone,
			structures.ReferralMessage,
			time.Now().Add(180*24*time.Hour))
		if err != nil {
			return errpath.Err(err, "error create new accruals")
		}
	}

	// бонусы за первую поездку
	client := order.Client
	clientRefData := client.ReferralProgramData
	if clientRefData.IsNewcomer {
		err = rabsender.SendJSONByAction(rabsender.AcReferralFirstRide, client.UUID)
		if err != nil {
			return errpath.Err(err)
		}

		cfg := config.GetCurrentReferralSystemParamsConfig()

		if client.ReferralProgramData.ReferralParentUUID != "" {
			err = AccrueBonusesWithMessage(
				cfg.AddForDonor,
				clientRefData.ParentUUID,
				clientRefData.ReferralParentPhone,
				structures.ReferralMessageFirstRide,
				time.Now().Add(180*24*time.Hour),
			)
			if err != nil {
				return errpath.Err(err)
			}
		}
	}

	return nil
}

func AccrueBonusesWithMessage(bonusesNumber int, clientUUID, clientPhone string, msg string, lifeTime time.Time) error {
	err := AccrueBonuses(bonusesNumber, clientUUID, clientPhone, msg, lifeTime)
	if err != nil {
		return err
	}

	tit, bod := getTitleAndBodyForClient(bonusesNumber, msg)
	err = SendMessageToClients([]string{clientPhone}, tit, bod)
	if err != nil {
		return err
	}

	return nil
}

// AccrueBonuses - начислить бонусы
func AccrueBonuses(bonusesCount int, clientUUID, clientPhone string, msg string, lifeTime time.Time) error {
	_, err := CreatePromotionAccruals(bonusesCount, clientPhone, msg, lifeTime)
	if err != nil {
		return errpath.Err(err, "error create new accruals")
	}

	err = ChangeBonuses(clientPhone,
		msg,
		clientUUID,
		bonusesCount,
		structures.TransferTypeTopUp)
	if err != nil {
		return errpath.Err(err, "error create new accruals")
	}

	return nil
}

func getPreData(or structures.OrderWithClientHistory) AccrualRulesPreData {
	var prData AccrualRulesPreData
	prData.FirstPointZones = or.StartZones
	prData.LastPointZones = or.FinishZones
	neededTime := localtime.TimeInZone(time.Now(), config.St.Application.TimeZone)
	prData.OrderCreationMonth = int(neededTime.Month())
	prData.OrderCreationDay = neededTime.Day()
	prData.OrderCreationHour = neededTime.Hour()
	prData.OrderCreationMinute = neededTime.Minute()
	prData.OrderCreationTimeUnix = or.CreatedAt.Unix()
	prData.OrderSource = or.Source
	prData.OrderTotalPrice = or.Tariff.TotalPrice
	prData.OrderServiceName = or.Service.Name
	return prData
}
func (acPD *AccrualRulesPreData) BonusesNumberByOrder(order structures.OrderWithClientHistory) (int, []string, error) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":         "bonuses number by order",
		"orderUUID":     order.UUID,
		"numberOfOrder": acPD.ClientOrdersNumber,
	})
	rules, err := GetAllRules()
	if err != nil {
		return 0, nil, fmt.Errorf("error getting all rules,%s", err)
	}
	if !checkExistsActiveRule(rules) {
		return 0, nil, nil
	}
	var res int
	var activatedRules []string
	for _, v := range rules {
		if !v.IsActive() || v.Deleted {
			continue
		}
		acPD.ClientOrdersNumber = v.getOrdersNumberInNeededTimePeriod(order)
		result, err := expr.Eval(v.IfExpr, acPD)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":   "error eval if expr",
				"ruleUUID": v.UUID,
			}).Error(err)
			continue
		}
		if result != true {
			continue
		}
		result, err = expr.Eval(v.AccrualExpr, acPD)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason":   "error eval accrual expr",
				"ruleUUID": v.UUID,
			}).Error(err)
			continue
		}
		resFL, check := result.(float64)
		if !check {
			log.WithFields(logrus.Fields{
				"reason":   "error type cast",
				"ruleUUID": v.UUID,
				"result":   result,
			}).Error(err)
			continue
		}
		activatedRules = append(activatedRules, v.Name)
		res += int(resFL)
	}
	return res, activatedRules, nil
}
func checkExistsActiveRule(rls []AccrualRules) bool {
	for _, val := range rls {
		if val.IsActive() {
			return true
		}
	}
	return false
}
func GetAllRules() ([]AccrualRules, error) {
	var rules []AccrualRules
	err := db.Model(&rules).
		Where("deleted is not true").
		Order("created_at DESC").
		Select()
	return rules, err
}
func GetOrderWithHistory(orUUID string) (structures.OrderWithClientHistory, error) {
	var res structures.OrderWithClientHistory
	url := config.St.CRM.Host + orWithNumberEndpoint + orUUID
	err := request(context.Background(), http.MethodGet, url, nil, &res)
	return res, err
}

func (ac *AccrualRules) getOrdersNumberInNeededTimePeriod(ord structures.OrderWithClientHistory) int {
	minData := time.Now().Unix() - ac.TimePeriod
	var res int
	for _, val := range ord.ClientOrderHistory {
		if val.Unix() >= minData {
			res++
		}
	}
	return res
}
func SetPromotionDeleted(uuid string) error {
	_, err := db.Model(&AccrualRules{}).
		Where("uuid = ?", uuid).
		Set("deleted = true").
		Update()
	return err
}
func (ar *AccrualRules) Update() *structures.ErrorWithLevel {
	errWL := ar.validateForUpdate()
	if errWL != nil {
		return errWL
	}
	_, err := db.Model(ar).
		Where("uuid = ?", ar.UUID).
		UpdateNotNull()
	return structures.Error(err)
}

func (ar *AccrualRules) Create() *structures.ErrorWithLevel {
	errWL := ar.validate()
	if errWL != nil {
		return errWL
	}
	ar.UUID = structures.GenerateUUID()
	_, err := db.Model(ar).
		Insert()
	return structures.Error(err)
}
func (ar *AccrualRules) validate() *structures.ErrorWithLevel {
	if ar.AccrualExpr == "" || ar.IfExpr == "" {
		return structures.Warning(fmt.Errorf("Введите оба выражения"))
	}
	if ar.TimePeriod <= 0 {
		return structures.Warning(fmt.Errorf("Неккоректный временной период"))
	}
	err := ar.checkValidExpr(false)
	if err != nil {
		return structures.Warning(err)
	}
	return ar.checkExistsIdenticalPromotion()
}

func (ar *AccrualRules) validateForUpdate() *structures.ErrorWithLevel {
	if ar.TimePeriod < 0 {
		return structures.Warning(fmt.Errorf("Неккоректный временной период"))
	}
	if ar.UUID == "" {
		return structures.Warning(fmt.Errorf("Пустой идентификатор"))
	}
	err := ar.checkValidExpr(true)
	if err != nil {
		return structures.Warning(err)
	}
	return ar.checkExistsIdenticalPromotion()
}

func (ar *AccrualRules) checkExistsIdenticalPromotion() *structures.ErrorWithLevel {
	var rule AccrualRules
	err := db.Model(&rule).
		Where("if_expr = ?", ar.IfExpr).
		Where("accrual_expr = ?", ar.AccrualExpr).
		Where("time_period = ?", ar.TimePeriod).
		Where("uuid != ?", ar.UUID).
		Where("deleted is not true").
		Select()
	if err == pg.ErrNoRows {
		return nil
	}
	if err == nil {
		var msg string
		if !rule.IsActive() {
			msg = "Промоакция с такими условиями и временным периодом уже сущестует, но не активирована. Активируйте или удалите ее"
		} else {
			msg = "Промоакция с такими условиями и временным периодом уже сущестует"
		}
		return structures.Warning(fmt.Errorf(msg))
	}
	return structures.Error(err)
}
func (ar *AccrualRules) checkValidExpr(forUpdate bool) error {
	if !forUpdate || ar.IfExpr != "" {
		_, err := expr.Eval(ar.IfExpr, AccrualRulesPreData{})
		if err != nil {
			return fmt.Errorf("Некорректное выражение условия, %s", err)
		}
	}
	if !forUpdate || ar.AccrualExpr != "" {
		_, err := expr.Eval(ar.AccrualExpr, AccrualRulesPreData{})
		if err != nil {
			return fmt.Errorf("Некорректное выражение надбавки, %s", err)
		}
	}

	return nil
}

func GetAllPromotions() ([]AccrualRules, error) {
	var rules []AccrualRules
	err := db.Model(&rules).
		Where("deleted is not true").
		Order("created_at desc").
		Select()
	for i := range rules {
		rules[i].CreatedAtUnix = rules[i].CreatedAt.Unix()
	}
	return rules, err
}
