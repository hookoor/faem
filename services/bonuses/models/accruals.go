package models

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/go-pg/pg/urlvalues"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
)

type Accruals struct {
	tableName      struct{}               `sql:"bon_promocodes_activations" pg:",discard_unknown_columns"`
	UUID           string                 `json:"uuid"`
	CodeUUID       string                 `json:"code_uuid"`
	RuleName       string                 `json:"rule_name"`
	RuleUUID       string                 `json:"rule_uuid"`
	Source         structures.ChatMembers `json:"source"`
	BonusesAccrued int                    `json:"bonuses_accrued"` //сколько бонусов было начислено
	Used           int                    `json:"used"`            //сколько бонусов использовано
	Available      int                    `json:"available"`       //сколько бонусов еще можно использовать
	Burned         int                    `json:"burned"`          //сколько бонусов сгорело
	Code           string                 `json:"code"`
	Comment        string                 `json:"comment"`
	Type           AccrualTypes           `json:"type"`
	ClientPhone    string                 `json:"client_phone"`
	Active         *bool                  `json:"active"`
	LifeTimeUnix   int64                  `json:"life_time_unix" sql:"-"`
	CreatedAtUnix  int64                  `json:"created_at_unix" sql:"-"`
	LifeTime       time.Time              `json:"-"` //когда сгорят бонусы
	CreatedAt      time.Time              `json:"-"`
}

type AccrualsFilter struct {
	ClientPhone string                 `json:"client_phone"`
	Code        string                 `json:"code"`
	Page        int                    `json:"int"`
	Limit       int                    `json:"limit"`
	Pager       urlvalues.Pager        `json:"-"`
	Source      structures.ChatMembers `json:"source"`
	CodeUUID    string                 `json:"code_uuid"`
	Active      *bool                  `json:"active"`
	Type        AccrualTypes           `json:"type"`
	OrderBy     string                 `json:"order_by"`
	MinTime     time.Time              `json:"-"`
	MinTimeUnix int64                  `json:"min_time_unix"`
	MaxTime     time.Time              `json:"-"`
	MaxTimeUnix int64                  `json:"max_time_unix"`
}
type AccrualTypes string

const (
	Initial      AccrualTypes = "initial"
	Coupon       AccrualTypes = "coupon"
	Compensation AccrualTypes = "сompensation"
	Promotions   AccrualTypes = "promotions" //начисление по промоакции
)

func (pa *Accruals) checkExistsAndSave(clientRegTime int64) *structures.ErrorWithLevel {
	check, err := db.Model(pa).
		Where("code_uuid = ?code_uuid").
		Where("client_phone = ?client_phone").
		Exists()
	if err != nil {
		return structures.Error(err)
	}
	if check {
		return structures.Warning(fmt.Errorf("Вы уже активировали этот код"))
	}
	if errWL := pa.checkInitialPromocodeActivation(clientRegTime); errWL != nil {
		return errWL
	}
	err = promocodeActivationCountInc(pa.CodeUUID)
	if err != nil {
		return structures.Error(err)
	}
	_, err = db.Model(pa).
		Insert()
	return structures.Error(err)
}
func (pa *Accruals) checkInitialPromocodeActivation(clientRegTime int64) *structures.ErrorWithLevel {
	if pa.Type != Initial {
		return nil
	}
	check, err := db.Model(pa).
		Where("type = ?", Initial).
		Where("client_phone = ?client_phone").
		Exists()
	if err != nil {
		return structures.Error(fmt.Errorf("error check initial promocode actiavation, %s", err))
	}
	if check {
		return structures.Warning(fmt.Errorf("Вы уже активировали один регистрационный промокод"))
	}
	if (time.Now().Unix()-clientRegTime)/60 > config.St.Application.RegPromocodeLifeTimeMinute {
		return structures.Warning(fmt.Errorf("Для регистрационного промокода уже поздновато("))
	}
	return nil
}
func promocodeActivationCountInc(promoUUID string) error {
	_, err := db.Model(&Promocode{}).
		Where("uuid = ?", promoUUID).
		Set("activation_count = (COALESCE(activation_count, 0) + 1)").
		Update()
	return err
}

func GetExpireddAccruals() ([]Accruals, error) {
	var res []Accruals
	err := db.Model(&res).
		Where("active").
		Where("life_time < CURRENT_TIMESTAMP").
		Order("created_at desc").
		Select()
	return res, err
}

func UpdateAccruals(in []Accruals) error {
	if len(in) < 1 {
		return nil
	}
	for i := range in {
		_, err := db.Model(&in[i]).
			Where("uuid = ?uuid").
			Update()
		if err != nil {
			return err
		}
	}

	return nil
}
func WriteOff(clientPhone string, bonNumber int) error {
	bonNumber = ABSint(bonNumber)

	var neededData []Accruals
	err := db.Model(&neededData).
		Where("client_phone = ?", clientPhone).
		Where("active").
		Order("life_time").
		Select()
	if err != nil {
		return err
	}
	if len(neededData) < 1 {
		return errors.New("empty needed data")
	}

	var recordsForUpdate []Accruals

	residue := bonNumber //остаток от того, сколько надо было списать
	for _, val := range neededData {
		residue = residue - val.setWriteBonuses(bonNumber)
		recordsForUpdate = append(recordsForUpdate, val)
		if residue <= 0 {
			break
		}
	}

	err = UpdateAccruals(recordsForUpdate)
	if err != nil {
		return fmt.Errorf("error update accruals, %s", err)
	}
	if residue > 0 {
		return fmt.Errorf("less records than necessary")
	}
	return nil
}
func ABSint(v int) int {
	if v >= 0 {
		return v
	}
	return -v
}

// setWriteBonuses списывает с начисления возможное кол-во и возвращает то, сколько списал
func (pa *Accruals) setWriteBonuses(bonNumber int) int {
	forWriteOff := bonNumber
	if bonNumber > pa.Available {
		forWriteOff = pa.Available
	}
	pa.Available -= forWriteOff
	pa.Used += forWriteOff
	if pa.Available <= 0 {
		falseParam := false
		pa.Active = &falseParam
	}
	return forWriteOff
}

func (pa *Accruals) Insert() error {
	if pa == nil {
		return nil
	}
	_, err := db.Model(pa).Insert()
	return err
}
func GeClientsUUIDByPhones(phones []string) (map[string]string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()

	resp := make(map[string]string)
	body := struct {
		Phones []string `json:"phones"`
	}{
		Phones: phones,
	}

	err := request(
		ctx,
		http.MethodGet,
		config.St.Client.Host+clientsUUIDByPhonesEndpoint,
		&body,
		&resp,
	)
	return resp, err
}
func getAllClientNumbers() ([]string, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancel()
	var resp []string
	err := request(
		ctx,
		http.MethodGet,
		config.St.Client.Host+allCLientNumbersEndpoint,
		nil,
		&resp,
	)
	return resp, err
}
func SyncAccrualWithBalances() error {
	allNumbers, err := getAllClientNumbers()
	if err != nil {
		return err
	}
	var newAccruals []Accruals
	for _, phone := range allNumbers {
		bonFloat, err := bonusesCountByPhone(context.Background(), phone)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event": "sync bonuses acc",
				"phone": phone,
			}).Error(err)
			continue
		}
		if bonFloat <= 0 {
			continue
		}
		comm := fmt.Sprintf("Суммирование всех начислений за промокоды до ввода статистики")
		newAccruals = append(newAccruals, Accruals{
			UUID:           structures.GenerateUUID(),
			Comment:        comm,
			Code:           "ALL OLD CODES",
			ClientPhone:    phone,
			BonusesAccrued: int(bonFloat),
			Available:      int(bonFloat),
			Source:         structures.ClientMember,
			Type:           Coupon,
		})
	}
	_, err = db.Model(&newAccruals).Insert()
	return err
}

func (pa *Accruals) IsActive() bool {
	if pa == nil || pa.Active == nil {
		return false
	}
	return *pa.Active
}

func CreatePromotionAccruals(bonusesNumber int, clientPhone, msg string, lifeTime time.Time) (Accruals, error) {
	newAc := Accruals{
		UUID:           structures.GenerateUUID(),
		Type:           Promotions,
		Source:         structures.ClientMember,
		BonusesAccrued: bonusesNumber,
		Available:      bonusesNumber,
		Comment:        msg,
		Active:         boolPointer(true),
		LifeTime:       lifeTime,
		ClientPhone:    clientPhone,
	}
	err := newAc.Insert()

	return newAc, err
}

func (params *AccrualsFilter) GetSuitableAccruals() ([]Accruals, int, error) {
	params.validate()
	var res []Accruals

	query := db.Model(&res).Order(params.OrderBy)

	if params.Active != nil {
		query.Where("active = ?", params.Active)
	}
	if params.ClientPhone != "" {
		query.Where("client_phone = ?", params.ClientPhone)
	}
	if params.Code != "" {
		query.Where("code = ?", params.Code)
	}
	if params.CodeUUID != "" {
		query.Where("code_uuid = ?", params.CodeUUID)
	}
	if !params.MinTime.IsZero() {
		query.Where("created_at > ?", params.MinTime)
	}
	if !params.MaxTime.IsZero() {
		query.Where("created_at < ?", params.MaxTime)
	}
	if params.Source != "" {
		query.Where("source = ?", params.Source)
	}
	if params.Type != "" {
		query.Where("type = ?", params.Type)
	}

	count, err := query.Count()
	if err != nil || count == 0 {
		return res, count, err
	}

	err = query.Apply(params.Pager.Pagination).Select()

	return res, count, err
}

func (params *AccrualsFilter) validate() {
	if params.OrderBy == "" {
		params.OrderBy = "created_at DESC"
	}
	params.Code = strings.ToLower(params.Code)
}
