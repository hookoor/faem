package models

import (
	"bytes"
	"context"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"time"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
)

var (
	db *pg.DB
)

// ConnectDB initialize connection to package var
func ConnectDB(conn *pg.DB) {
	db = conn
}

func request(ctx context.Context, method string, url string, payload, response interface{}, basicAuth ...string) error {
	r := &http.Client{
		Timeout: 15 * time.Second,
	}
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}
	req = req.WithContext(ctx)
	if len(basicAuth) > 1 {
		req.SetBasicAuth(basicAuth[0], basicAuth[1])
	}

	req.Header.Set("Content-Type", "application/json")

	resp, err := r.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to make a post request")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("post request to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf("post request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}

	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}
