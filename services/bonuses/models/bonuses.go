package models

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
	"golang.org/x/sync/errgroup"
)

const (
	balanseEndpoint             = "/client/balances"
	allCLientNumbersEndpoint    = "/clients/numbers/"
	clientsUUIDByPhonesEndpoint = "/clients/uuid/byphones"
)

func GetBonusesData(phone string) (structures.BonusesData, error) {
	var result structures.BonusesData
	var errAW, errB error
	var aWOData AutoWriteOff
	var check bool
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	errG, ctx := errgroup.WithContext(ctx)
	fillAWOData := func() error {
		aWOData, check, errAW = GetAutoWFData(ctx, phone)
		return errAW
	}
	fillBonusesData := func() error {
		result.Bonuses, errB = bonusesCountByPhone(ctx, phone)
		return errB
	}
	errG.Go(fillAWOData)
	errG.Go(fillBonusesData)
	if err := errG.Wait(); err != nil {
		return result, err
	}
	result.AutomaticWriteOff = aWOData.Condition
	//если записи еще нет - по дефолту считать что автосписание выключено (можно включить)
	if !check {
		result.AutomaticWriteOff = false
	}
	return result, nil
}

func bonusesCountByPhone(ctx context.Context, phone string) (float64, error) {
	var resp structures.BalancesInfo
	err := request(
		ctx,
		http.MethodPost,
		config.St.Billing.Host+balanseEndpoint,
		structures.ClientBalancesArgs{Phones: []string{phone}},
		&resp,
		config.St.Billing.Username,
		config.St.Billing.Password,
	)

	if err != nil {
		return 0, err
	}
	var result float64
	data, check := resp.UserAccounts[phone]
	if !check {
		return result, fmt.Errorf("response have not account with number (%s)", phone)
	}
	return data.Accounts[structures.AccountTypeBonus].Balance, nil
}
