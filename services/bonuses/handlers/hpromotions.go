package handlers

import (
	"fmt"
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func GetAllPromotions(c echo.Context) error {

	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "get all promotions",
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	res, err := models.GetAllPromotions()
	if err != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, res)
}
func DeletePromotion(c echo.Context) error {
	uuid := c.Param("uuid")
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "delete promotion",
		"uuid":     uuid,
		"userType": userType,
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetPromotionDeleted(uuid)
	if err != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		log.Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
func UpdatePromotion(c echo.Context) error {
	var data models.AccrualRules
	err := c.Bind(&data)
	uuid := c.Param("uuid")
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update promotion",
		"uuid":     uuid,
		"userType": userType,
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		err = fmt.Errorf("invalid jwt type")
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.UUID = uuid
	errWL := data.Update()
	if errWL != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func CreatePromotion(c echo.Context) error {
	var data struct {
		models.AccrualRules
	}
	err := c.Bind(&data)
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create promotions",
		"ifExpr":   data.IfExpr,
		"acExpr":   data.AccrualExpr,
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		err = fmt.Errorf("invalid user type for this action")
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.CreatorUUID = userUUID
	errWL := data.Create()
	if errWL != nil {
		msg := "model function error "
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
