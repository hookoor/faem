package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func SaveAutoWriteOffData(c echo.Context) error {
	var data models.AutoWriteOff
	var errJWT error
	data.SwitcherUUID, data.SwitcherType, errJWT = memberUUIDFromJWT(c)
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "save auto write off data",
		"userUUID": data.SwitcherUUID,
		"userType": data.SwitcherType,
	})
	if errJWT != nil {
		msg := "error parse jwt"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if data.SwitcherType == structures.ClientMember {
		data.Phone = userPhoneFromJWT(c)
	}
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	errWl := data.Save()
	if errWl != nil {
		msg := "error saving data"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
