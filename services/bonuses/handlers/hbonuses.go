package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

const (
	commentForBonuses string = "Вы можете оплатить бонусами до 30% от стоимости поездки (1 бонус = 1 рубль)"
)

func GetBonusesDataByPhone(c echo.Context) error {
	var data struct {
		Phone string `json:"phone"`
	}
	err := c.Bind(&data)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get bonuses data by phone",
		"phone": data.Phone,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	result, err := models.GetBonusesData(data.Phone)
	if err != nil {
		msg := "model function error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	result.Comment = commentForBonuses
	return c.JSON(http.StatusOK, result)
}

func Sync(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "sync bonuses",
	})
	err := models.SyncAccrualWithBalances()
	if err != nil {
		msg := "model function error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
