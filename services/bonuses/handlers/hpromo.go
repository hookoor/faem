package handlers

import (
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

type inputData struct {
	Promocode     string                 `json:"promocode"`
	Phone         string                 `json:"phone"`
	SourceType    structures.ChatMembers `json:"-"`
	ClientRegTime int64                  `json:"-"` // для проверки повторной активации регистрационного промокода
	ClientUUID    string                 `json:"-"`
}

func ActivatePromocode(c echo.Context) error {
	var data inputData
	err := c.Bind(&data)
	errFD := data.fillData(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":  "activate promocode",
		"code":   data.Promocode,
		"phone":  data.Phone,
		"clUUID": data.ClientUUID,
	})
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if errFD != nil {
		msg := "error fill input data"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.Phone, err = phoneverify.NumberVerify(data.Phone)
	if err != nil {
		msg := "Неправильный формат номера телефона"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  msg,
		})
	}
	data.Promocode = strings.ToLower(data.Promocode)

	errWL := models.ActivateCode(data.Promocode, data.Phone, data.ClientUUID, data.ClientRegTime, data.SourceType)
	if errWL != nil {
		errWL.Print(log.WithFields(logrus.Fields{
			"reason": "model function error",
		}))
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
func GetAllPromocodes(c echo.Context) error {

	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "get all promocodes",
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	pager, err := getPager(c)
	if err != nil {
		msg := "error getting pager"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	res, count, err := models.GetAllPromocodes(pager)
	if err != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: res,
		Count:   count,
	})
}
func DeletePromocode(c echo.Context) error {
	uuid := c.Param("uuid")
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "delete promocode",
		"uuid":     uuid,
		"userType": userType,
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.SetDeletePromocode(uuid)
	if err != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		log.Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
func UpdatePromocode(c echo.Context) error {
	var data models.Promocode
	err := c.Bind(&data)
	uuid := c.Param("uuid")
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "update promocode",
		"uuid":     uuid,
		"userType": userType,
		"userUUID": userUUID,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if userType != structures.UserCRMMember {
		msg := "invalid jwt type"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(msg)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if data.ClientPhone != "" {
		data.ClientPhone, err = phoneverify.NumberVerify(data.ClientPhone)
		if err != nil {
			msg := "Неправильный формат номера телефона"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
			return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
				Code: http.StatusBadRequest,
				Msg:  msg,
			})
		}
	}
	data.Code = strings.ToLower(data.Code)
	errWL := data.Update(uuid)
	if errWL != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}

func CreatePromocode(c echo.Context) error {
	var data models.Promocode
	err := c.Bind(&data)
	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "create promocode",
		"code":     data.Code,
		"phone":    data.ClientPhone,
		"userType": userType,
		"userUUID": userUUID,
	})
	if userType != structures.UserCRMMember && errJWT == nil {
		errJWT = fmt.Errorf("invalid user type")
	}
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errJWT)
		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	if data.ClientPhone != "" {
		data.ClientPhone, err = phoneverify.NumberVerify(data.ClientPhone)
		if err != nil {
			msg := "Неправильный формат номера телефона"
			log.WithFields(logrus.Fields{
				"reason": msg,
			}).Error(err)
			return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
				Code: http.StatusBadRequest,
				Msg:  msg,
			})
		}
	}

	fillCreatorAndTimeParameters(c, userUUID, &data)

	errWL := data.Create()
	if errWL != nil {
		msg := "model function error"
		log = log.WithFields(logrus.Fields{
			"reason": msg,
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", errWL.Error),
		})
	}
	return c.JSON(http.StatusOK, structures.ResponseStructOk)
}
func (inData *inputData) fillData(c echo.Context) error {
	userUUID, userType, err := memberUUIDFromJWT(c)
	if err != nil {
		return err
	}
	inData.SourceType = userType
	switch userType {
	case structures.ClientMember:
		inData.Phone = userPhoneFromJWT(c)
		inData.ClientRegTime = userRegTimeFromJWT(c)
		inData.ClientUUID = userUUID
	case structures.UserCRMMember:
		//номер уже должен быть забинжен из json-a
		inData.ClientRegTime = time.Now().Unix() //чтобы оператор мог применить рег. промокод для клиента когда угодно
	default:
		return fmt.Errorf("unknown type of token")
	}
	return nil
}
func fillCreatorAndTimeParameters(c echo.Context, userUUID string, data *models.Promocode) {
	if data == nil {
		return
	}
	data.CreatorData.UUID = userUUID
	data.CreatorData.Login = loginFromJWT(c)
	data.Expiration = time.Unix(data.ExpirationUnix, 0)
	data.BonusesBurnTime = time.Unix(data.BonusesBurnTimeUnix, 0)
	data.Code = strings.ToLower(data.Code)
}

// GetNewRegPromocode - получение и активаци промокода для новых клиентов
func GetNewRegPromocode(c echo.Context) error {

	var newClientRegistrationPromocode models.NewRegPromocode
	ctx := c.Request().Context()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get registration promocode",
	})

	err := c.Bind(&newClientRegistrationPromocode)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	log = log.WithFields(logrus.Fields{
		"code":  newClientRegistrationPromocode.Promocode,
		"phone": newClientRegistrationPromocode.ClientPhone,
	})

	promocode, isActivate, err := models.GetNewRegPromocode(ctx, newClientRegistrationPromocode)
	if err != nil {
		if isActivate {
			log.Warnln("new registration promocode is active")
			return c.JSON(http.StatusOK, "promocode is active")
		}
		msg := "GetNewRegPromocode"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(errpath.Err(err))
		res := logs.OutputRestError(msg, errpath.Err(err), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	if !isActivate {
		errWL := models.ActivateCode(
			newClientRegistrationPromocode.Promocode,
			newClientRegistrationPromocode.ClientPhone,
			newClientRegistrationPromocode.ClientUUID,
			time.Now().Unix(),
			structures.UserCRMMember,
		)
		if err != nil {
			return c.JSON(http.StatusBadRequest, errWL)
		}
	}

	log.Info("promocode ready to activate")

	return c.JSON(http.StatusOK, promocode)
}
