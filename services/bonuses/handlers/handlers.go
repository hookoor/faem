package handlers

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg/urlvalues"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	emptyUUIDMsg        = "empty uuid"
	bindingDataErrorMsg = "error binding data"
)

type RecordsWithCount struct {
	Records interface{} `json:"records"`
	Count   int         `json:"records_count"`
}

func memberUUIDFromJWT(c echo.Context) (string, structures.ChatMembers, error) {
	user := c.Get("user")
	userTok, check := user.(*jwt.Token)
	if !check {
		return "", "", fmt.Errorf("invalid user field in context")
	}
	claims := userTok.Claims.(jwt.MapClaims)
	clientUUID, check := claims["client_uuid"]
	if check {
		return clientUUID.(string), structures.ClientMember, nil
	}
	driverUUID, check := claims["driver_uuid"]
	if check {
		return driverUUID.(string), structures.DriverMember, nil
	}
	operUUID, check := claims["user_uuid"]
	if check {
		return operUUID.(string), structures.UserCRMMember, nil
	}
	return "", "", fmt.Errorf("invalid uuid field in context")
}

func userPhoneFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	phone := claims["phone"].(string)
	return phone
}
func loginFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	log, ok := claims["login"]
	if !ok {
		return ""
	}
	logStr, ok := log.(string)
	return logStr
}
func userRegTimeFromJWT(c echo.Context) int64 {
	log := logs.Eloger.WithFields(logrus.Fields{"event": "getting user reg time from jwt"})

	user := c.Get("user").(*jwt.Token)

	claims := user.Claims.(jwt.MapClaims)

	res, ok := claims["reg_time"]
	if !ok {
		log.Error("error getting reg_time claim")
		return 0
	}
	resStr, ok := res.(string)
	if !ok {
		log.Error("error convert res to string")
		return 0
	}
	resInt, err := strconv.ParseInt(resStr, 10, 64)
	if err != nil {
		log.Error("error convert string to int64")
	}
	return resInt
}

func getPager(c echo.Context) (urlvalues.Pager, error) {
	var (
		pager urlvalues.Pager
	)
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return pager, fmt.Errorf("Error parsing pager limit,%s", err)
	}
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return pager, fmt.Errorf("Error parse orders page,%s", err)
	}
	pager.Offset = (page - 1) * limit
	pager.Limit = limit
	return pager, nil
}

func getTimeParams(min, max int64) (time.Time, time.Time) {
	return time.Unix(min, 0), time.Unix(max, 0)
}

func getPagerByReadyParams(page, limit int) urlvalues.Pager {
	if page < 1 {
		page = 1
	}
	return urlvalues.Pager{
		Offset: (page - 1) * limit,
		Limit:  limit,
	}
}

// FormatExternalPhone returns a phone if its length is less or equal then E164 format without leading code
func FormatExternalPhone(phone string) string {
	phone = strings.TrimSpace(phone)
	if len(phone) < 2 {
		return ""
	}
	if strings.HasPrefix(phone, "7") || strings.HasPrefix(phone, "8") {
		phone = "+7" + phone[1:]
	}

	return phone
}
