package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
)

func GetAccrualsByFilter(c echo.Context) error {

	userUUID, userType, errJWT := memberUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":    "get accruals by filter",
		"userUUID": userUUID,
		"userType": userType,
	})
	if errJWT != nil {
		msg := "parse jwt error"
		log.WithFields(logrus.Fields{"reason": msg}).Error(errJWT)

		res := logs.OutputRestError(msg, errJWT, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var data models.AccrualsFilter
	err := c.Bind(&data)
	if err != nil {
		msg := bindingDataErrorMsg
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	data.Pager = getPagerByReadyParams(data.Page, data.Limit)
	data.ClientPhone = FormatExternalPhone(data.ClientPhone)
	if userType == structures.ClientMember {
		data.ClientPhone = userPhoneFromJWT(c)
	}
	res, count, err := data.GetSuitableAccruals()
	if err != nil {
		msg := "model function error"
		log.WithFields(logrus.Fields{"reason": msg}).Error(err)

		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s, %s", msg, err),
		})
	}
	for i := range res {
		res[i].CreatedAtUnix = res[i].CreatedAt.Unix()
		res[i].LifeTimeUnix = res[i].LifeTime.Unix()
	}
	return c.JSON(http.StatusOK, RecordsWithCount{
		Records: res,
		Count:   count,
	})
}

// AccrualByPhones -
func AccrualByPhones(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithField("event", "AccrualByPhones")

	var bind struct {
		BonusesCount int      `json:"bonuses_count"`
		LifeTime     int64    `json:"life_time_sec"`
		Phones       []string `json:"phones"`
	}
	err := c.Bind(&bind)
	if err != nil {
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	{ // validate
		if bind.BonusesCount == 0 {
			err = errpath.Errorf("BonusesCount <= 0")
			log.Errorln(err)
			return c.JSON(http.StatusBadRequest, err)
		}
		if bind.LifeTime <= 0 {
			err = errpath.Errorf("LifeTime <= 0")
			log.Errorln(err)
			return c.JSON(http.StatusBadRequest, err)
		}
		if len(bind.Phones) == 0 {
			err = errpath.Errorf("Phones count = 0")
			log.Errorln(err)
			return c.JSON(http.StatusBadRequest, err)
		}
	}

	tm := time.Unix((time.Now().Unix() + bind.LifeTime), 0)
	log.WithFields(logrus.Fields{
		"BonusesCount": bind.BonusesCount,
		"LifeTime":     fmt.Sprintf("%v + %v = %s", time.Now().Unix(), bind.LifeTime, tm.Format("2006-01-02 15:04:05")),
		"Phones":       bind.Phones,
	})

	var body struct {
		Phones []string `json:"phones"`
	}
	body.Phones = bind.Phones
	data := map[string]string{}
	err = tool.SendRequest(http.MethodGet, config.St.Client.Host+"/clients/uuid/byphones", nil, body, &data)
	if err != nil {
		log.Errorln(errpath.Err(err))
		return c.JSON(http.StatusBadRequest, errpath.Err(err))
	}

	for phone, uuid := range data {
		err = models.AccrueBonuses(bind.BonusesCount, uuid, phone, "за красивые глаза", tm)
		if err != nil {
			log.Errorln(errpath.Err(err))
			return c.JSON(http.StatusBadRequest, errpath.Err(err))
		}
	}

	log.Infoln("AccrualByPhones successfuly")

	return c.JSON(http.StatusOK, "AccrualByPhones successfuly")
}
