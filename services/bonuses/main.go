package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	xos "gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/bonuses/config"
	"gitlab.com/faemproject/backend/faem/services/bonuses/db"
	"gitlab.com/faemproject/backend/faem/services/bonuses/models"
	"gitlab.com/faemproject/backend/faem/services/bonuses/rabhandler"
	"gitlab.com/faemproject/backend/faem/services/bonuses/rabsender"
	"gitlab.com/faemproject/backend/faem/services/bonuses/router"
	"gitlab.com/faemproject/backend/faem/services/bonuses/tickers"
)

const (
	version               = "0.1.0"
	serverShutdownTimeout = 30 * time.Second
	brokerShutdownTimeout = 30 * time.Second
	tickerShutdownTimeout = 30 * time.Second
)

func main() {
	bVersion := flag.Bool("ver", false, "Output current app vesion and exit")
	bEnvVars := flag.Bool("env", false, "Output envieroinment vars and exit")
	configPath := flag.String("config", "config/bonuses.toml", "Default config filepath")

	flag.Parse()

	config.InitConfig(*configPath)

	if *bVersion {
		fmt.Println("Version: ", version)
		os.Exit(0)
	}

	if *bEnvVars {
		config.PrintVars()
		os.Exit(0)
	}

	if err := logs.SetLogLevel(config.St.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(config.St.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	eloger := logs.Eloger

	conn, err := db.Connect()
	if err != nil {
		es := fmt.Sprintf("Error connecting to DB. %s", err)
		fmt.Println(es)
		os.Exit(4)
	}
	defer db.CloseDbConnection(conn)
	fmt.Println("Connecting to Postgres successfuly", conn)
	rb := rabbit.New()
	rb.Credits.User = config.St.Broker.UserCredits
	rb.Credits.URL = config.St.Broker.UserURL
	err = rb.Init(config.St.Broker.ExchagePrefix, config.St.Broker.ExchagePostfix)
	if err != nil {
		fmt.Println(fmt.Sprintf("Error Init RabbitMQ. %s", err))
		os.Exit(5)
	}
	fmt.Println("Connecting to RabbitMQ successfully")

	defer rb.CloseRabbit()
	var wg sync.WaitGroup
	rabsender.Init(rb, &wg)
	rabhandler.InitVars(rb, conn, &wg)
	models.ConnectDB(conn)
	// err = models.CheckAndApplyPromotions("46225b8d-535d-437a-a84e-85198416020f")
	// if err != nil {
	// 	fmt.Println(fmt.Sprintf("Error Init RabbitMQ. %s", err))
	// 	os.Exit(5)
	// }
	err = rabhandler.StartService()
	if err != nil {
		fmt.Println(fmt.Sprintf("Error starting rabhandler. %s", err))
		os.Exit(5)
	}

	tickers.InitiateTickers()

	e := router.Init(eloger)
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)
	go web.Start(e, config.St.Application.Port)
	defer web.Stop(e, serverShutdownTimeout)

	// Wait for program exit
	<-xos.NotifyAboutExit()
}
