package config

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/spf13/viper"
)

// AppConfig main configuration struct
type AppConfig struct {
	Application struct {
		Port                       string
		RegPromocodeLifeTimeMinute int64
		TimeZone                   string
		LogLevel                   string
		LogFormat                  string
	}
	Database struct {
		Host     string
		User     string
		Password string
		Port     int
		Db       string
	}
	Broker struct {
		UserURL        string
		UserCredits    string
		DriverURL      string
		DriverCredist  string
		ExchagePrefix  string
		ExchagePostfix string
	}
	// open street routing machine
	OSRM struct {
		Host string
	}
	CRM struct {
		Host string
	}
	Billing struct {
		Host     string
		Username string
		Password string
	}
	Client struct {
		Host string
	}
}

// Установка дефолтныйз значений
func initDefaults() {
	// APPLICATION DAkristinahugaeva1997@mail.ruTA
	viper.SetDefault("Application.Port", structures.BonusesPort)
	viper.SetDefault("Application.TimeZone", "Europe/Moscow")
	viper.SetDefault("Application.RegPromocodeLifeTimeMinute", 1440)
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")

	// DATABASE DATA
	viper.SetDefault("Database.Host", "localhost")
	viper.SetDefault("Database.User", "postgres")
	viper.SetDefault("Database.Password", "root")
	viper.SetDefault("Database.Port", 5432)
	viper.SetDefault("Database.Db", "bonusesDebug")
	// RABBIT DATA
	viper.SetDefault("Broker.UserURL", "localhost:6004")
	viper.SetDefault("Broker.UserCredits", "user:pass")
	viper.SetDefault("Broker.ExchagePrefix", "")
	viper.SetDefault("Broker.ExchagePostfix", "")
	// open street routing machine
	viper.SetDefault("OSRM.Host", "http://localhost")

	viper.SetDefault("CRM.Host", "http://127.0.0.1:1324/api/v2")
	// Billing
	viper.SetDefault("Billing.Host", "https://billing.apis.stage.faem.pro/rpc/v1")
	viper.SetDefault("Billing.Username", "")
	viper.SetDefault("Billing.Password", "")

	viper.SetDefault("Client.Host", "http://127.0.0.1:1325/api/v2")
}
