package config

import (
	"fmt"
	"os"
	"strings"
	"sync"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var (
	// St ([St]orage) - переменная для хранения конфигурации
	St AppConfig
)

// По новому стайлу все переменные хранятся в структуре St, но что бы не пережделывать
// оставил старую структуру [Envs], и просто ей присвоил новые данные, но сами данные
// также доступны и в новой структуре

// InitConfig initialize config
func InitConfig(filepath string) {
	// инициализируем дефольные значения
	initDefaults()

	// читаем конфиг
	viper.SetConfigFile(filepath)
	err := viper.ReadInConfig()
	if err != nil {
		logs.OutputError(fmt.Sprintf("Error reading config file. %s", err))
	}

	// А теперь можно почитать переменные окружения
	bindEnvVars()

	err = viper.Unmarshal(&St)
	if err != nil {
		fmt.Println(fmt.Errorf("Error unmarshalling config file. %s", err))
	}

	logs.Eloger.WithFields(logrus.Fields{
		"TokenExpiredM": St.Application.TokenExpiredM,
	}).Infoln("configs")
	// fmt.Println(St.Broker.ClientURL)
}

func initEnvsOld() map[string]string {

	m := map[string]string{
		"host":            viper.GetString("Database.host"),
		"user":            viper.GetString("Database.user"),
		"password":        viper.GetString("Database.password"),
		"port":            viper.GetString("Database.port"),
		"db":              viper.GetString("Database.db"),
		"brokerClientURL": viper.GetString("Broker.clientURL"),
		"brokerClient":    viper.GetString("Broker.clientCredits"),
	}

	return m
}

func bindEnvVars() {
	viper.SetEnvPrefix("CLIENT")
	viper.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	viper.AutomaticEnv()
}

// PrintVars output env vars
func PrintVars() {

	fmt.Println("\nEVEIROINMENT VARS:")
	fmt.Printf("host:       %s\n", viper.GetString("Database.host"))
	fmt.Printf("user:       %s\n", viper.GetString("Database.user"))
	fmt.Printf("password:   %s\n", viper.GetString("Database.password"))
	fmt.Printf("port:       %s\n", viper.GetString("Database.port"))
	fmt.Printf("db:         %s\n", viper.GetString("Database.db"))
}

// JWTSecret phrase
func JWTSecret() string {
	secret := os.Getenv("JWT_SECRET")
	if secret == "" {
		return "InR5cCIljaldskWRtaW4iLCJ1c2Vy"
	}
	return secret
}

// -------------------------------------
// -------------------------------------
// -------------------------------------

// ReferralSystemParamsConfig -
type ReferralSystemParamsConfig struct {
	structures.ReferralSystemParams
	mx sync.Mutex
}

var (
	currentReferralSystemParams = ReferralSystemParamsConfig{
		// BlockingConfig: structures.BlockingConfig{
		// 	BlockDriverActivity:                 structures.DefaultBlockDriverActivity,
		// 	GuaranteedDriverIncomeActivityLimit: structures.DefaultGuaranteedDriverIncomeActivityLimit,
		// },
	}
)

// SetCurrentReferralSystemParamsConfig -
func SetCurrentReferralSystemParamsConfig(config structures.ReferralSystemParams) {
	currentReferralSystemParams.mx.Lock()
	defer currentReferralSystemParams.mx.Unlock()

	currentReferralSystemParams.ReferralSystemParams = config
}

// GetCurrentReferralSystemParamsConfig -
func GetCurrentReferralSystemParamsConfig() structures.ReferralSystemParams {
	currentReferralSystemParams.mx.Lock()
	defer currentReferralSystemParams.mx.Unlock()

	return currentReferralSystemParams.ReferralSystemParams
}

// -------------------------------------
// -------------------------------------
// -------------------------------------
