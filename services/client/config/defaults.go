package config

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/spf13/viper"
)

const (
	developmentEnv = "development"
)

// AppConfig main configuration struct
type AppConfig struct {
	Application struct {
		Env                              string
		Port                             string
		TokenExpiredM                    int64
		IncreasedFareUpdatePeriodSec     int64
		IncreasedFareUpMessageTimeoutSec int64
		RefreshTokenExpiredM             int64
		SessionExpM                      int64
		TimeZone                         string
		DistributingTime                 string
		LogLevel                         string
		LogFormat                        string
	}
	CRM struct {
		BaseURL string
	}
	Database struct {
		Host     string
		User     string
		Password string
		Port     int
		Db       string
	}
	SMPP struct {
		Host     string
		User     string
		Password string
		From     string
	}
	Broker struct {
		UserURL        string
		UserCredits    string
		ExClientURL    string
		ClientURL      string
		ClientCredits  string
		ExchagePrefix  string
		ExchagePostfix string
	}
	Elastic struct {
		Host  string
		Index string
		Port  string
	}
	Bonuses struct {
		Host string
	}
	FCM struct {
		ServerKey string
	}
	OSRM struct {
		// Host - !!! значение перезаписывается базой
		Host string
	}
	Notifier struct {
		Host string
	}
}

func (a *AppConfig) IsDevelopment() bool {
	return a.Application.Env == developmentEnv
}

// Установка дефолтныйз значений
func initDefaults() {
	// APPLICATION DATA
	viper.SetDefault("Application.Env", developmentEnv)
	viper.SetDefault("Application.Port", structures.ClientPort)
	viper.SetDefault("Application.TokenExpiredM", 228)
	viper.SetDefault("Application.RefreshTokenExpiredM", 30240)
	viper.SetDefault("Application.SessionExpM", 1)
	viper.SetDefault("Application.IncreasedFareUpdatePeriodSec", 20)
	viper.SetDefault("Application.IncreasedFareUpMessageTimeoutSec", 30)
	viper.SetDefault("Application.DistributingTime", 5)
	viper.SetDefault("Application.TimeZone", "Europe/Moscow")
	viper.SetDefault("Application.LogLevel", "info")
	viper.SetDefault("Application.LogFormat", "text")

	// DATABASE DATA
	viper.SetDefault("Database.Host", "78.110.156.74")
	viper.SetDefault("Database.User", "barman")
	viper.SetDefault("Database.Password", "ba4man80")
	viper.SetDefault("Database.Port", 6001)
	viper.SetDefault("Database.Db", "client")
	// RABBIT DATA
	viper.SetDefault("Broker.UserURL", "78.110.156.74:6004")
	viper.SetDefault("Broker.UserCredits", "barmen:kfclover97")
	viper.SetDefault("Broker.ClientURL", "127.0.0.1")
	viper.SetDefault("Broker.ClientCredits", "user:password")
	viper.SetDefault("Broker.ExchagePrefix", "")
	viper.SetDefault("Broker.ExchagePostfix", "")
	// SMPP
	viper.SetDefault("SMPP.Host", "default_host:port")
	viper.SetDefault("SMPP.User", "faem")
	viper.SetDefault("SMPP.Password", "secret")
	viper.SetDefault("SMPP.From", "Faem")
	// ELASTIC DATA
	viper.SetDefault("Elastic.Host", "78.110.156.74")
	viper.SetDefault("Elastic.Port", "6006")
	viper.SetDefault("Elastic.Index", "client_logs")
	// FCM
	viper.SetDefault("FCM.ServerKey", "")
	// Bonuses
	viper.SetDefault("Bonuses.Host", "http://127.0.0.1:1327/api/v1")
	// CRM
	viper.SetDefault("CRM.BaseURL", "http://127.0.0.1:1324/api/v2")
	// open street routing machine  TODO: добавить в вайпер
	viper.SetDefault("OSRM.Host", "http://osrm.faem.svc.cluster.local")

	viper.SetDefault("notifier.host", "https://notifier.apis.stage.faem.pro/api/v2")
}

// TODO: По хорошему надо также указать переменные для рэбита, а именно название очередей и пр.
// проблема в том что сейчас используются переменные из общего пакета, нужно сделать
// их дефольными, а из конфига их можно переписать
