package router

import (
	"gitlab.com/faemproject/backend/faem/pkg/prometheus"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	h "gitlab.com/faemproject/backend/faem/services/client/handlers"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

const apiversion = "v2"

// Init - binding middleware and setup routers
func Init(s *h.Server) *echo.Echo {

	e := echo.New()
	web.UseHealthCheck(e)

	// logrus.SetLevel(logrus.DebugLevel)
	// e.Use(logrusmiddleware.Hook())

	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	// Metrics
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)

	e.Use(Hook())
	jwtSec := config.JWTSecret()
	jwtGroup := e.Group("/api/" + apiversion + "/auth")
	jwtGroup.POST("/new", s.RegisterClientHandler)
	jwtGroup.POST("/verification", h.PhoneVerificationHandler)
	jwtGroup.POST("/refresh", h.RefreshTokenHandler)

	o := e.Group("/api/" + apiversion)
	open := e.Group("/api/" + apiversion)
	o.Use(middleware.JWTWithConfig(middleware.JWTConfig{
		SigningKey: []byte(jwtSec),
	}))

	open.POST("/appversion", h.CheckClientAppVersion)

	// Orders
	o.POST("/orders", h.CreateOrder)
	o.PUT("/orders/cancel/:uuid", h.CancelOrder)
	o.GET("/orders/:uuid", h.GetOrderByUUID)
	o.POST("/sms/send/", s.SendSMSToClients)
	//open.GET("/clients/numbers/", h.AllClientNumbersHandler)
	open.GET("/clients/uuid/byphones", h.GetClientsUUIDByPhones)
	o.POST("/orders/locations", h.NewClientLocation)
	o.GET("/myorders", h.OrdersByUser)
	o.GET("/balansedata", h.GetBalanseData)
	o.GET("/balancedata", h.GetBalanseData)

	o.GET("/initdata", h.GetInitData)
	o.POST("/firebasetoken", h.SaveClientFirebaseToken)
	o.POST("/order/rating", h.OrderRating)
	o.PUT("/order/update/routes/:uuid", h.OrderRoutesUpdate)
	o.PUT("/order/update/increasedfare/:uuid", h.OrderIncreasedFareUpdate)
	o.GET("/addresses/recommended/:destination", h.GetLastCustomerAddresses) // получение последних n адресов клиента
	o.POST("/buildrouteway", h.BuildRouteWay)                                // данные для построения маршрута по роутам

	// Избранные адреса
	o.POST("/addresses/favorite", h.CreateFavoriteAddresses)
	o.GET("/addresses/favorite", h.FavoriteAddressesList)
	// o.GET("/favoriteaddresses/:uuid", h.GetFavoriteAddress)
	o.PUT("/addresses/favorite/:uuid", h.UpdateFavoriteAddress)
	o.DELETE("/addresses/favorite/:uuid", h.DeleteFavoriteAddress)

	{
		o.GET("/myreferralurl", h.GetReferralURL)

		open.GET("/increcipientstravelcount/:uuid", h.GetAndIncRecipientTravelCount)
	}

	path := "/api/" + apiversion
	e.POST(path+"/buildrouteway", h.BuildRouteWay) // данные для построения маршрута по роутам

	e.GET(path+"/gettelegramid/:telegram_id", h.GetTelegramID)
	e.POST(path+"/settelegramid", h.SetTelegramID)

	// ---
	// WARN: обращаться на этот эндпоинт в crmке
	// meta: на этот эндпоинт обращаются старые версии приложения
	// TODO: удалить по возможности
	open.POST("/findaddress", h.FindAddress) // получение ближайшего адреса по координатам
	// ---
	e.GET(path+"/options", h.OptionsList)
	e.POST(path+"/testfcm", h.TestFCM)

	o.GET("/clients/income", h.GetClientIncome)

	return e
}
