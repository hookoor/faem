package handlers

import (
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"net/http"
	"strconv"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// OrderRating godoc
func OrderRating(c echo.Context) error {
	var (
		ordRat models.OrderRatingClient
	)
	cluuid := userUUIDFromJWT(c)
	if err := c.Bind(&ordRat); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "order rating",
			"reason":     "error binding data",
			"clientUUID": cluuid,
		}).Error(err)

		res := logs.OutputRestError("Error binding order rating data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	ordRat.ClientUUID = cluuid
	err := ordRat.Save()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "order rating",
			"reason":     "error saving data",
			"clientUUID": cluuid,
		}).Error(err)

		res := logs.OutputRestError("error saving data", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Update client's blacklist if required
	minDriverRating, err := models.FetchDriverMinBlacklistRating()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order rating",
			"orderUUID": ordRat.OrderUUID,
		}).Error(err)
		// continue intentionally
		minDriverRating = 0
	}
	if err = models.UpdateClientBlacklistIfNeeded(ordRat.OrderRating, minDriverRating); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order rating",
			"orderUUID": ordRat.OrderUUID,
		}).Error(err)
		// continue intentionally
	}

	var order models.OrderCApp
	err = models.GetByUUID(ordRat.OrderUUID, &order)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "Error finding order",
		}).Error(err)
		res := logs.OutputRestError("Error finding order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	// Tip if needed
	if order.GetPaymentType() == constants.OrderPaymentTypeCard && ordRat.Tip > 0 {
		TipToDriver(&order, float64(ordRat.Tip))

		notifyPush := structures.PushMailing{
			Type:          structures.MailingInformDriverType,
			TargetsUUIDs:  []string{order.Driver.UUID},
			TargetsPhones: nil,
			Title:         "Чаевые",
			Message:       "Вам оставили чаевые в размере " + strconv.Itoa(ordRat.Tip) + "р.",
		}
		err = rabhandler.SendJSONByAction(rabhandler.AcNotifyDriverAboutTip, notifyPush)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "order rating",
				"reason": "error send notify push about tip to broker",
			}).Error(err)
		}
	}

	// Notify listeners about new rating
	order.ClientRating.Value = ordRat.Value
	order.ClientRating.Comment = ordRat.Comment
	err = rabhandler.SendJSONByAction(rabhandler.AcOrderState, order.Order, rabbit.SetRatingKey)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order rating",
			"reason": "error send update order to broker",
		}).Error(err)
		res := logs.OutputRestError("Error update order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	})
}
