package handlers

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/constants"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	"gitlab.com/faemproject/backend/faem/services/client/rabsender"
)

const (
	msgTipToDriver = "Чаевые водителю"
	lblTipToDriver = "Перевозка пассажиров и багажа"
)

func TipToDriver(order *models.OrderCApp, tip float64) {
	if order.GetPaymentType() != constants.OrderPaymentTypeCard || tip <= 0 { // double check just in case
		return
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "tip to driver",
		"order_uuid": order.UUID,
	})

	if order.ClUUID == "" {
		log.Error("empty client uuid for an order with card payment")
		return
	}

	newTransfer := structures.NewTransfer{
		IdempotencyKey:   structures.GenerateUUID(),
		PayerAccountType: structures.AccountTypeCard,
		TransferType:     structures.TransferTypePayment,
		PayerUUID:        order.ClUUID,
		PayerType:        structures.UserTypeClient,
		PayeeUUID:        order.Driver.UUID,
		PayeeType:        structures.UserTypeDriver,
		Amount:           tip,
		Description:      msgTipToDriver,
		Meta: structures.TransferMetadata{
			OwnerName:   order.Source,
			OrderUUID:   order.UUID,
			DriverUUID:  order.Driver.UUID,
			ClientID:    order.ClUUID,
			ClientPhone: order.GetClientPhone(),
		},
		GatewayKey: structures.TransferKeyTip,
	}

	go func() {
		if err := rabsender.SendJSONByAction(rabsender.AcTipToDriver, newTransfer); err != nil {
			log.WithField("reason", "making a driver transfer").Error(err)
			return
		}
		log.WithField("reason", "transfer sent to RMQ").Debug("OK")

		// Generate a receipt
		GenerateReceipt(order, tip, lblTipToDriver)
	}()
}

func GenerateReceipt(order *models.OrderCApp, tip float64, label string) {
	// Prevent test receipt generation
	if config.St.IsDevelopment() {
		return
	}

	receiptData := structures.ReceiptData{
		Amount:     tip,
		OrderUUID:  order.UUID,
		ClientUUID: order.ClUUID,
		Label:      label,
		GatewayKey: structures.ReceiptKeyTip,
	}

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":     "generating receipt",
		"orderUUID": order.UUID,
	})
	if err := rabsender.SendJSONByAction(rabsender.AcMakeReceipt, receiptData, ""); err != nil {
		log.WithField("reason", "making a receipt").Error(err)
		return
	}
	log.WithField("reason", "transfer sent to RMQ").Debug("OK")
}
