package handlers

import (
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/client/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// OptionsList - список фич и сервисов
func OptionsList(c echo.Context) error {
	var (
		err      error
		reason   models.ReasonsForFront
		instResp struct {
			Feat        []models.FeatureCApp     `json:"features"`
			Serv        []models.ServiceCApp     `json:"services"`
			Reasons     []models.ReasonsForFront `json:"reasons_for_cancel"`
			TipProcents []int                    `json:"tip_percent"`
		}
	)

	instResp.Feat, err = models.FeatureList()
	if err != nil {
		msg := "error getting features list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting option list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}

	instResp.Serv, err = models.ServicesList()
	if err != nil {
		msg := "error getting services list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting option list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusNotFound)
		return c.JSON(http.StatusNotFound, res)
	}
	for _, rfc := range constants.ListClientCancelReasons() {
		for i, value := range constants.CancelReasonRU {
			if rfc == i {
				reason.ReasonName = rfc
				reason.ReasonTitle = value
				reason.ReasonImagePath = constants.CancelReasonImages[rfc]
				instResp.Reasons = append(instResp.Reasons, reason)
				break
			}

		}
	}
	//TODO: надо сделать это изменяемым параметром
	instResp.TipProcents = []int{0, 5, 10, 15, 20, 25}
	return c.JSON(http.StatusOK, instResp)
}
