package handlers

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/models"

	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// CreateOrder godoc
func CreateOrder(c echo.Context) error {

	var order, nOrder models.OrderCApp

	err := c.Bind(&order)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order creating [capp]",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Order]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	order.UUID = userUUIDFromJWT(c)
	//проверка на корректность введенного номера для перезвона
	if order.CallbackPhone != "" {
		formatNumber, err := phoneverify.NumberVerify(order.CallbackPhone)
		if err != nil {
			msg := "некорректный номер телефона для перезвона"
			logs.Eloger.WithFields(logrus.Fields{
				"event":         "order creating [capp]",
				"reason":        msg,
				"callbackPhone": order.CallbackPhone,
			}).Error(err)
			res := logs.OutputRestError(msg, err)
			return c.JSON(http.StatusBadRequest, res)
		}
		order.CallbackPhone = formatNumber
	}

	if order.SourceOfOrdersUUID == "" {
		order.SourceOfOrdersUUID = c.Request().Header.Get(constants.HeaderSourceUUID)
	}

	//Источник заказа LEGACY
	source := c.Request().Header.Get(structures.OrderSourceKey)
	if source == "" && order.SourceOfOrdersUUID == "" {
		msg := fmt.Sprintf("пустой заголовок с источником заказа - (%s)", structures.OrderSourceKey)
		logs.Eloger.WithFields(logrus.Fields{
			"event":         "order creating [capp]",
			"reason":        msg,
			"callbackPhone": order.CallbackPhone,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: msg})
	}

	var sourceIsValid bool
	for _, item := range constants.ListOrderSources() {
		if source == item {
			sourceIsValid = true
		}
	}
	// Не указан источник
	if !sourceIsValid && order.SourceOfOrdersUUID == "" {
		msg := fmt.Sprintf("такого источника заказа не существует (%s)", structures.OrderSourceKey)
		logs.Eloger.WithFields(logrus.Fields{
			"event":         "order creating [capp]",
			"reason":        msg,
			"callbackPhone": order.CallbackPhone,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{Code: 400, Msg: msg})
	}

	order.Source = source
	//Создание заказа
	nOrder, err = order.Create()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order creating [capp]",
			"reason": "Error creating order",
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}

	msg := fmt.Sprintf("New Order created, From='%s'", nOrder.Routes[0].Value)
	logs.Eloger.WithFields(logrus.Fields{
		"event":     "order creating [capp]",
		"orderUUID": nOrder.UUID,
	}).Info(msg)

	logs.OutputEvent("Order created", msg, nOrder.ID)

	//Отправка в очередь
	err = rabhandler.SendJSONByAction(rabhandler.AcOrderFromClientApp, nOrder)
	if err != nil {
		res := fmt.Sprintf("Error sending newOreder to broker. %s", err)
		logs.OutputError(res)
	}

	//Отправка в CRM
	if err := models.SendOrderToCRM(nOrder); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "order creating [capp]",
			"orderUUID": nOrder.UUID,
			"reason":    "error send order to client",
		}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError("Системная ошибка. Попробуйте позже", err))
	}
	//Перевод в нужный статус для клиента
	nOrder.StateTitle = constants.TranslateOrderStateForClient(nOrder.OrderState, nOrder.Service.ProductDelivery)

	return c.JSON(http.StatusOK, nOrder)
}

func GetOrderByUUID(c echo.Context) error {
	var (
		order models.OrderCApp
	)
	uuid := c.Param("uuid")
	if uuid == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order [client]",
			"reason": "Order UUID empty in request",
		}).Error("Order UUID empty in request")
		res := logs.OutputRestError("Order UUID empty", fmt.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}
	err := models.GetByUUID(uuid, &order)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting order [client]",
			"reason": "Error finding order",
		}).Error(err)
		res := logs.OutputRestError("Error finding order", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, order)
}

// OrdersByUser godoc
//goland:noinspection ALL
func OrdersByUser(c echo.Context) error {
	var (
		Orders []models.OrderCApp
	)
	clientUUID := userUUIDFromJWT(c)
	pager, err := getPager(c)
	if err != nil {
		pager.SetPage(1)
		pager.Limit = 25
	}
	Orders, err = models.OrdersByClientUUIDWithoutCanceled(clientUUID, pager)
	if err != nil {
		fr := fmt.Sprintf("Not found by Orders")
		res := logs.OutputRestError(fr, err, 404)
		return c.JSON(http.StatusNotFound, res)
	}
	for i := range Orders {
		Orders[i].StateTitle = constants.OrderStateRU[Orders[i].OrderState]
		Orders[i].CreatedAtUnix = Orders[i].CreatedAt.Unix()
	}
	for i := range Orders {
		var itemsForClientView []structures.TariffItem
		for _, v := range Orders[i].Tariff.Items {
			if v.Name == "Гарантированный доход водителя" {
				continue
			}
			itemsForClientView = append(itemsForClientView, v)
		}
		Orders[i].Tariff.Items = itemsForClientView
	}
	return c.JSON(http.StatusOK, Orders)
}

//func getRawOrder(or models.OrderCApp) structures.Order {
//	var rawOrder structures.Order
//	rawOrder.UUID = or.UUID
//	rawOrder.Comment = or.Comment
//	rawOrder.Routes = or.Routes
//	rawOrder.Features = or.Features
//	rawOrder.Tariff = or.Tariff
//	rawOrder.Service = or.Service
//	rawOrder.Client = or.Client
//	rawOrder.CreatedAt = or.CreatedAt
//	rawOrder.CancelTime = or.CancelTime
//	return rawOrder
//}

// CancelOrder godoc
func CancelOrder(c echo.Context) error {
	var (
		reason struct {
			Reason string `json:"reason"`
		}
	)

	uuid := c.Param("uuid")
	if uuid == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order cancel [client]",
			"reason": "Order UUID empty in request",
		}).Error("Order UUID empty in request")
		res := logs.OutputRestError("Order UUID empty", fmt.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}
	err := c.Bind(&reason)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "order cancel [client]",
			"reason": "Error binding data",
		}).Error(err)
		res := logs.OutputRestError("error binding data [Order]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	OrderState, err := models.CancelOrder(reason.Reason, uuid)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "cancel order [client]",
			"orderUUID": uuid,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  fmt.Sprintf("%s", err),
		})
	}
	if counter, err := models.IsOrderCounter(OrderState.OrderUUID); err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":     "cancel order [client]",
			"orderUUID": uuid,
		}).Error(err)
	} else if counter {
		OrderState.Flags |= structures.OfferStateFlagCounterOrder
	}
	rkey := fmt.Sprintf("%s.%s", rabbit.StateKey, OrderState.State)
	err = rabhandler.SendJSONByAction(rabhandler.AcOrderState, OrderState, rkey)

	if err != nil {
		res := fmt.Sprintf("Error sending driverState to broker. %s", err)
		logs.OutputError(res)
	}
	return c.JSON(http.StatusOK, okStruct)
}

// OrderIncreasedFareUpdate godoc
func OrderIncreasedFareUpdate(c echo.Context) error {
	var inputData struct {
		NewFare float32 `json:"increased_fare"`
	}
	cluuid := userUUIDFromJWT(c)
	uuid := c.Param("uuid")
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "order inc fare update",
		"clientUUID": cluuid,
		"orderUUID":  uuid,
	})
	if err := c.Bind(&inputData); err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		res := logs.OutputRestError("Error binding data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if uuid == "" {
		log.WithFields(logrus.Fields{
			"reason": emptyUUIDMsg,
		}).Error(emptyUUIDMsg)
		res := logs.OutputRestError(emptyUUIDMsg, fmt.Errorf("empty uuid"))
		return c.JSON(http.StatusBadRequest, res)
	}
	order, diff, errWL := models.UpdateOrderIncreasedFare(uuid, cluuid, inputData.NewFare)
	if errWL != nil {
		log.WithFields(logrus.Fields{
			"reason": "error fare update",
		})
		if errWL.Level == structures.WarningLevel {
			log.Warning(errWL.Error)
		}
		if errWL.Level == structures.ErrorLevel {
			log.Error(errWL.Error)
		}
		return c.JSON(http.StatusBadRequest, structures.ResponseStruct{
			Code: http.StatusBadRequest,
			Msg:  errWL.Error.Error(),
		})
	}
	order.OrderState = constants.ReplaceStateForClientIfNeed(order.OrderState, order.Service.ProductDelivery)
	order.StateTitle = constants.TranslateOrderStateForClient(order.OrderState, order.Service.ProductDelivery)
	if diff == 0 {
		return c.JSON(http.StatusOK, order)
	}
	err := rabhandler.SendJSONByAction(rabhandler.AcIncreasedFare, structures.WrapperOrder{
		Order:           order.Order,
		FareDifferences: diff,
	})

	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send update order to broker",
		}).Error(err)
		res := logs.OutputRestError("Error update order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, order)
}

// OrderRoutesUpdate godoc
func OrderRoutesUpdate(c echo.Context) error {
	var order models.OrderCApp
	cluuid := userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "order routes update",
		"clientUUID": cluuid,
	})
	if err := c.Bind(&order); err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		res := logs.OutputRestError("Error binding data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	uuid := c.Param("uuid")
	if uuid == "" {
		log.WithFields(logrus.Fields{
			"reason": emptyUUIDMsg,
		}).Error(emptyUUIDMsg)
		res := logs.OutputRestError(emptyUUIDMsg, fmt.Errorf(emptyUUIDMsg))
		return c.JSON(http.StatusBadRequest, res)
	}
	order.UUID = uuid
	newOrder, err := order.UpdateRoutes(cluuid)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error routes update",
		}).Error(err)
		res := logs.OutputRestError("ошибка", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = rabhandler.SendJSONByAction(rabhandler.AcNewOrderRoutesData, newOrder.Order)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send update order to broker",
		}).Error(err)
		res := logs.OutputRestError("Error update order", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	return c.JSON(http.StatusOK, newOrder)
}

// NewClientLocation godoc
func NewClientLocation(c echo.Context) error {
	var clLoc structures.ClientLocation
	clLoc.ClienUUID = userUUIDFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "new client locations",
		"clientUUID": clLoc.ClienUUID,
	})
	if err := c.Bind(&clLoc); err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingDataErrorMsg,
		}).Error(err)
		res := logs.OutputRestError("Error binding data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err := validateClientLoc(clLoc)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "validation error",
		}).Error(err)
		res := logs.OutputRestError("ошибка", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = models.SetOrderClientLocVisibility(clLoc.OrderUUID, clLoc.ClienUUID, clLoc.Visibility)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error order update",
		}).Error(err)
		res := logs.OutputRestError("ошибка", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = rabhandler.SendJSONByAction(rabhandler.AcNewClientLocation, clLoc)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send new client locations to broker",
		}).Error(err)
		res := logs.OutputRestError("Error send to broker", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	log.WithFields(logrus.Fields{
		"reason":    bindingDataErrorMsg,
		"vis":       clLoc.Visibility,
		"lat":       clLoc.Lat,
		"lon":       clLoc.Lon,
		"orderUUID": clLoc.OrderUUID,
	}).Info()
	return c.JSON(http.StatusOK, okStruct)
}

func validateClientLoc(clLoc structures.ClientLocation) error {
	if clLoc.Lat == 0 || clLoc.Lon == 0 {
		return fmt.Errorf("invalid locations")
	}
	if clLoc.OrderUUID == "" {
		return fmt.Errorf("invalid order_uuid")
	}
	return models.CheckExistsUUID(&models.OrderCApp{}, clLoc.OrderUUID)
}

func GetClientIncome(c echo.Context) error {
	clientUUID := userUUIDFromJWT(c)
	//clientUUID := c.Param("uuid")

	type item struct {
		Count   int `json:"count"`
		Balance int `json:"balance"`
	}

	var out struct {
		Activate int  `json:"activate"`
		LastDay  item `json:"last_day"`
		Total    item `json:"total"`
	}

	total, err := models.GetClientIncomeLastRecord(clientUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "getting client income by uuid [client]",
			"uuid":  clientUUID,
		}).Error(err)
		res := logs.OutputRestError("Error finding client income", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	out.Total = item{
		Count:   total.TotalCount,
		Balance: calcBalance(total.TotalCount),
	}

	now := time.Now()
	nowMinusDay := time.Now().Add(-(time.Hour * 24))
	lastDay, err := models.GetClientIncomeByPeriod(clientUUID, nowMinusDay, now)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "getting client income by uuid [client]",
			"uuid":  clientUUID,
		}).Error(err)
		res := logs.OutputRestError("Error finding client income", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	lastDayCount := 0
	for _, v := range *lastDay {
		lastDayCount += v.Count
	}

	out.LastDay = item{
		Count:   lastDayCount,
		Balance: calcBalanceFrom(total.TotalCount-lastDayCount+1, total.TotalCount),
	}

	client := new(models.ClientsApps)
	err = models.GetByUUID(clientUUID, client)
	if err != nil {
		out.Activate = 0
	} else {
		out.Activate = client.ReferralProgramData.ActivationCount
	}

	return c.JSON(http.StatusOK, out)
}

func calcBalance(tripCount int) int {
	balance := 0
	for i := 1; i <= tripCount; i++ {
		if 1 <= i && i <= 10 {
			balance += 5
		} else if 11 <= i && i <= 50 {
			balance += 4
		} else if 51 <= i && i <= 100 {
			balance += 3
		} else {
			balance += 2
		}
	}
	return balance
}

func calcBalanceFrom(from, tripCount int) int {
	balance := 0
	for i := from; i <= tripCount; i++ {
		if 1 <= i && i <= 10 {
			balance += 5
		} else if 11 <= i && i <= 50 {
			balance += 4
		} else if 51 <= i && i <= 100 {
			balance += 3
		} else {
			balance += 2
		}
	}
	return balance
}
