package handlers

import (
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/smpp"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	"gitlab.com/faemproject/backend/faem/services/client/tickers/tickersmodern"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

type Server struct {
	Sender *smpp.Sender

	Tickers *tickersmodern.Ticker
}

// NewServer Sender New constructor
func NewServer(sender *smpp.Sender, tickers *tickersmodern.Ticker) *Server {
	return &Server{
		Sender:  sender,
		Tickers: tickers,
	}
}

// SendSMSToClients отправляет сообщения клиентам
func (s *Server) SendSMSToClients(c echo.Context) error {

	var inputData struct {
		Text   string   `json:"text"`
		Phones []string `json:"phones"`
	}
	_, memType, err := memberUUIDFromJWT(c)
	if err != nil {
		res := logs.OutputRestError("Error parse jwt", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if memType != structures.UserCRMMember {
		res := logs.OutputRestError("invalid jwt type", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	err = c.Bind(&inputData)
	if err != nil {
		res := logs.OutputRestError("Error binding input data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if len(inputData.Phones) == 0 || inputData.Text == "" {
		res := logs.OutputRestError("invalid input data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	for i := range inputData.Phones {
		inputData.Phones[i], err = phoneverify.NumberVerify(inputData.Phones[i])
		if err != nil {
			res := logs.OutputRestError("number verify error", err)
			return c.JSON(http.StatusUnauthorized, res)
		}
	}

	for _, phone := range inputData.Phones {
		go s.Sender.SendSMS(phone, inputData.Text, s.Sender.From)
	}

	return c.JSON(http.StatusOK, okStruct)
}

// RegisterClientHandler запрос на валидацию нового клиента
func (s *Server) RegisterClientHandler(c echo.Context) error {
	var (
		loginData models.ClientRegistrRequest
	)
	err := c.Bind(&loginData)
	if err != nil {
		res := logs.OutputRestError("Eror binding login data", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	loginData.Phone, err = phoneverify.NumberVerify(loginData.Phone)
	if err != nil {

		return c.JSON(http.StatusUnauthorized, structures.ResponseStruct{
			Code: http.StatusUnauthorized,
			Msg:  "Неправильный формат номера телефона",
		})
	}
	code, expatation, err := models.GenerateVerificationCode(loginData)
	if err != nil {
		res := logs.OutputRestError("Error generating VerificationCode", err)
		return c.JSON(http.StatusUnauthorized, res)
	}

	resp := models.NewRegResponseStruct{
		Code:            200,
		Msg:             fmt.Sprintf("Verification code send to %s", loginData.Phone),
		NextRequestTime: expatation,
	}
	cred := fmt.Sprintf("SMS request. DeviceID = %s, Phone = %s", loginData.DeviceID, loginData.Phone)
	logs.Eloger.WithFields(logrus.Fields{
		"event": "new driver registration",
		"value": code,
	}).Info(cred)

	msg := fmt.Sprintf("Код верификации клиента: %v", code)
	go s.Sender.SendSMS(loginData.Phone, msg, s.Sender.From)

	logs.Eloger.WithFields(logrus.Fields{
		"event": "new client registration",
	}).Info("Trying to send SMS")
	msg = fmt.Sprintf("Client_ID=%s, CODE=%v", loginData.DeviceID, code)
	logs.OutputEvent("Verification code request", msg, 0)

	return c.JSON(http.StatusOK, resp)
}

// PhoneVerificationHandler ввод полученного через СМС кода
func PhoneVerificationHandler(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).WithField("event", "PhoneVerificationHandler")

	var validata models.VerificationCode
	if err := c.Bind(&validata); err != nil {
		log.Error(errpath.Err(err))
		res := logs.OutputRestError("Error binding verification data", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.WithFields(logrus.Fields{
		"DeviceID":               validata.DeviceID,
		"verification code":      validata.Code,
		"ReferralParentUUID":     validata.ReferralParentUUID,
		"registration promocode": validata.UTMSource,
	})

	serviceName := c.Request().Header.Get(string(models.RequestHeaderServiceNameKey))

	newjwt, userPhone, shbe, err := models.ValidateCode(validata, serviceName)
	if err != nil {
		log.Error(errpath.Err(err))
		return c.JSON(http.StatusUnauthorized, structures.ResponseStruct{
			Code: http.StatusUnauthorized,
			Msg:  err.Error(),
		})
	}
	log.WithFields(logrus.Fields{
		"client UUID":       newjwt.ClientUUID,
		"verification code": userPhone,
	})

	// если есть промокод нового клинта и телефона клиента нет в базе
	if validata.UTMSource != "" && !shbe {
		client, err := models.GetUserByPhone(userPhone)
		if err != nil {
			res := logs.OutputRestError("Error get client", errpath.Err(err), 401)
			return c.JSON(http.StatusUnauthorized, res)
		}

		// запрос на бонусы
		var body = struct {
			Promocode  string `json:"promocode"`
			Phone      string `json:"phone"`
			ClientUUID string `json:"client_uuid"`
		}{
			Promocode:  validata.UTMSource,
			Phone:      userPhone,
			ClientUUID: client.UUID,
		}
		err = tool.SendRequest(http.MethodPost, config.St.Bonuses.Host+"/newregpromocode", nil, body, nil)
		if err != nil {
			log.Warning(errpath.Err(err))
			// res := logs.OutputRestError("Error validating code", errpath.Err(err), 401)
			// return c.JSON(http.StatusUnauthorized, res)
		} else {
			log.WithField("meta", "have new autorization")
		}
	}

	// msg := fmt.Sprintf("Token issued for DeviceID=%s", validata.DeviceID)
	// logs.OutputClientEvent("JWT issued", msg, newjwt.ClientUUID)

	log.Infoln(errpath.Infof("verification successfuly"))

	return c.JSON(http.StatusOK, newjwt)
}

// AllClientNumbersHandler возвращает все номера телефонов клиентов из базы
func AllClientNumbersHandler(c echo.Context) error {

	data, err := models.GetAllClientNumbers()
	if err != nil {
		res := logs.OutputRestError("model func error", err, 400)
		return c.JSON(http.StatusUnauthorized, res)
	}
	return c.JSON(http.StatusOK, data)

}

// RefreshTokenHandler обновление ранее выданного токена токена
func RefreshTokenHandler(c echo.Context) error {
	var refresh models.RefreshRequest
	if err := c.Bind(&refresh); err != nil {
		msg := "error binding refresh token data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "token refresh",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}

	serviceName := c.Request().Header.Get(string(models.RequestHeaderServiceNameKey))
	if serviceName == "" {
		serviceName = string(models.RequestHeaderServiceClient)
	}

	newToken, err := models.RefreshJWTToken(refresh.RefreshToken, serviceName)
	if err != nil {
		msg := "token refresh error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "token refresh",
			"token":  refresh.RefreshToken,
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusUnauthorized)
		return c.JSON(http.StatusUnauthorized, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":      "token refresh",
		"token":      refresh.RefreshToken,
		"clientUUID": newToken.ClientUUID,
	}).Info("token refresh done successfully")
	return c.JSON(http.StatusOK, newToken)
}

// GetInitData возвращает данные
func GetInitData(c echo.Context) error {
	log := logs.LoggerForContext(c.Request().Context()).
		WithField("event", "get init data")

	userUUID := userUUIDFromJWT(c)
	user, err := models.GetUserByUUID(userUUID)
	if err != nil {
		log.WithField("userUUID", userUUID).Error(err)
		res := logs.OutputRestError("Error getting current client", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	orders, err := models.GetCurrentClientOrders(userUUID)
	if err != nil {
		logs.Eloger.WithField("userUUID", userUUID).Error(err)
		res := logs.OutputRestError("Error getting current order", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	avarageDistTime := getCurrentAverageDistTime()
	for i := range orders {
		if orders[i].OrderState == constants.OrderStateFree ||
			orders[i].OrderState == constants.OrderStateDistributing {
			orders[i].RouteWayData.RouteFromDriverToClient.Proper.Duration = int(avarageDistTime.Seconds())
		}
	}
	var returnData InitData
	returnData.OrderData = orders
	returnData.ClientUUID = userUUID
	returnData.ClientPhone = user.MainPhone
	returnData.DefaultPaymentType = user.DefaultPaymentType
	return c.JSON(http.StatusOK, returnData)
}

func getCurrentAverageDistTime() time.Duration {
	// якобы час пик
	if time.Now().Hour() > 5 && time.Now().Hour() < 7 ||
		time.Now().Hour() > 14 && time.Now().Hour() < 16 {
		return time.Duration(rand.Intn(3)+5) * time.Minute
	}
	return time.Duration(rand.Intn(2)+2) * time.Minute
}

//SaveClientFirebaseToken godoc
func GetBalanseData(c echo.Context) error {

	phone := userPhoneFromJWT(c)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "save client firebase token handler",
		"phone": phone,
	})
	bon, err := models.GetBunesesByPhone(phone)
	if err != nil {
		msg := "error getting bonuses count"
		log.WithFields(logrus.Fields{
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, bon)
}

//SaveClientFirebaseToken godoc
func SaveClientFirebaseToken(c echo.Context) error {
	var (
		clientTok models.ClientFirebaseToken
		err       error
	)
	if err = c.Bind(&clientTok); err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "save client firebase token handler",
			"reason": msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	clientTok.ClientUUID = userUUIDFromJWT(c)

	err = clientTok.Save()
	if err != nil {
		msg := "error saving token"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "save client firebase token handler",
			"clientUUID": clientTok.ClientUUID,
			"reason":     msg,
		}).Error(err)

		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	res := logs.OutputRestOK("OK")
	return c.JSON(http.StatusOK, res)
}

//GetClientsUUIDByPhones godoc
func GetClientsUUIDByPhones(c echo.Context) error {
	var phones struct {
		Phones []string `json:"phones"`
	}
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "get clients uuid by phones",
	})
	if err := c.Bind(&phones); err != nil {
		log.WithFields(logrus.Fields{"reason": bindingDataErrorMsg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(bindingDataErrorMsg, err))
	}

	res, err := models.GetClientsUUIDByPhones(phones.Phones)
	if err != nil {
		log.WithFields(logrus.Fields{"reason": modelFuncErrMsg}).Error(err)

		return c.JSON(http.StatusBadRequest, logs.OutputRestError(modelFuncErrMsg, err))
	}

	return c.JSON(http.StatusOK, res)
}
