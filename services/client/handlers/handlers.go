package handlers

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/go-pg/pg/urlvalues"
	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

const (
	lastRoute           = ""
	emptyUUIDMsg        = "empty uuid"
	bindingDataErrorMsg = "error binding data"
	modelFuncErrMsg     = "models func error"
)

var (
	okStruct = structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	}
)

type InitData struct {
	models.ClientData
	OrderData []models.OrdersForInit `json:"orders_data,omitempty"`
}

// UserIDFromJWT return user ID
func userIDFromJWT(c echo.Context) int {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userID := claims["client_id"].(float64)
	return int(userID)
}
func userPhoneFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	phone := claims["phone"].(string)
	return phone
}
func userUUIDFromJWT(c echo.Context) string {
	user := c.Get("user").(*jwt.Token)
	claims := user.Claims.(jwt.MapClaims)
	userUUID := claims["client_uuid"].(string)
	return userUUID
}

func memberUUIDFromJWT(c echo.Context) (string, structures.ChatMembers, error) {
	user := c.Get("user")
	userTok, check := user.(*jwt.Token)
	if !check {
		return "", "", fmt.Errorf("invalid user field in context")
	}
	claims := userTok.Claims.(jwt.MapClaims)
	clientUUID, check := claims["client_uuid"]
	if check {
		return clientUUID.(string), structures.ClientMember, nil
	}
	driverUUID, check := claims["driver_uuid"]
	if check {
		return driverUUID.(string), structures.DriverMember, nil
	}
	operUUID, check := claims["user_uuid"]
	if check {
		return operUUID.(string), structures.UserCRMMember, nil
	}
	return "", "", fmt.Errorf("invalid uuid field in context")
}
func TestFCM(c echo.Context) error {
	var (
		test    models.TestFCM
		check   bool
		orState models.OrderStateCApp
	)
	err := c.Bind(&test)
	if err != nil {
		msg := "Error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "testing fcm",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if test.ClientToken == "" {
		msg := "empty token"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "testing fcm",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	if test.Action == "driver_coordinates" {
		err := models.SendToClientDRVCOORTESTFCM(test.ClientToken)
		if err != nil {
			msg := "error sendind driver_coordinates"
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "testing fcm",
				"reason": msg,
			}).Error(err)
			res := logs.OutputRestError(msg, err)
			return c.JSON(http.StatusBadRequest, res)
		}
		return c.JSON(http.StatusOK, structures.ResponseStruct{
			Code: http.StatusOK,
			Msg:  "OK!",
		})
	}
	if test.Action == "chat_message" {
		err := models.SendToClientChatMessTESTFCM(test.ClientToken)
		if err != nil {
			msg := "error sendind chat_message"
			logs.Eloger.WithFields(logrus.Fields{
				"event":  "testing fcm",
				"reason": msg,
			}).Error(err)
			res := logs.OutputRestError(msg, err)
			return c.JSON(http.StatusBadRequest, res)
		}
		return c.JSON(http.StatusOK, structures.ResponseStruct{
			Code: http.StatusOK,
			Msg:  "OK!",
		})
	}
	for _, val := range constants.ListOrderStates() {
		if test.Action == val {
			check = true
			break
		}
	}
	if !check {
		msg := "invalid action"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "testing fcm",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	orState.State = test.Action
	orState.OrderUUID = "some_order_uuid"
	orState.DriverUUID = "че-нить"
	if test.Action == constants.OrderStateAccepted {
		orState.Comment = fmt.Sprint(time.Now().Unix() + 250)
	}
	err = orState.SendToClientTESTFCM(test.ClientToken)
	if err != nil {
		msg := "error sendind orderstate"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "testing fcm",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err)
		return c.JSON(http.StatusBadRequest, res)
	}
	return c.JSON(http.StatusOK, structures.ResponseStruct{
		Code: http.StatusOK,
		Msg:  "OK!",
	})

}

func getPager(c echo.Context) (urlvalues.Pager, error) {
	var (
		pager urlvalues.Pager
	)
	limit, err := strconv.Atoi(c.QueryParam("limit"))
	if err != nil {
		return pager, fmt.Errorf("Error parsing pager limit,%s", err)
	}
	page, err := strconv.Atoi(c.QueryParam("page"))
	if err != nil {
		return pager, fmt.Errorf("Error parse orders page,%s", err)
	}
	pager.Offset = (page - 1) * limit
	pager.Limit = limit
	return pager, nil
}

// ----------------------
// ----------------------
// ----------------------

// GetReferralURL -
func GetReferralURL(c echo.Context) error {
	ctx := c.Request().Context()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "GetReferralURL",
	})

	jwtPayload, err := tool.IWantJWTPayload(c.Request().Header.Get("Authorization"), 2)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "get client uuid form jwt").Errorln(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	clientUUID, exist := jwtPayload["client_uuid"]
	if !exist {
		err = errpath.Errorf("cant get client uuid form jwt")
		log.WithField("reason", "get client uuid").Errorln(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	url, err := models.GetReferralURL(ctx, clientUUID.(string))
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "GetReferralURL").Errorln(err)
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	var res struct {
		ReferralURL tool.URL `json:"referral_url"`
	}
	res.ReferralURL = url

	log.WithFields(logrus.Fields{
		"client uuid ": clientUUID,
		"referral url": url,
	}).Info("Request successful!")

	return c.JSON(http.StatusOK, res)
}

// GetAndIncRecipientTravelCount -
func GetAndIncRecipientTravelCount(c echo.Context) error {
	var err error
	clientuuid := c.Param("uuid")

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetAndIncRecipientTravelCount",
		"clientuuid": clientuuid,
	})

	rsp, err := models.GetAndIncRecipientTravelCount(clientuuid)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "models.GetAndIncRecipientTravelCount").Errorln(err.Error())
		return c.JSON(http.StatusBadRequest, err.Error())
	}

	log.Infoln("Done!")

	return c.JSON(http.StatusOK, rsp)
}

// ----------------------
// ----------------------
// ----------------------
