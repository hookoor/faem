package handlers

import (
	"fmt"
	"net/http"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/client/models"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

func CreateFavoriteAddresses(c echo.Context) error {
	var favoriteAddress models.FavoriteAddress

	clientUUID := userUUIDFromJWT(c)
	err := c.Bind(&favoriteAddress)
	if err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "create favorite address",
			"clientUUID": clientUUID,
			"reason":     msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	nFavoriteAddress, err := favoriteAddress.Create(clientUUID)
	if err != nil {
		msg := "create error"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "create favorite address",
			"clientUUID": clientUUID,
			"reason":     msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":               "create favorite address",
		"clientUUID":          clientUUID,
		"favoriteAddressUUID": nFavoriteAddress.UUID,
	}).Info("new favorite address create successfully")

	return c.JSON(http.StatusOK, nFavoriteAddress)
}

func FavoriteAddressesList(c echo.Context) error {
	var favoriteAddresses []models.FavoriteAddress
	clientUUID := userUUIDFromJWT(c)

	favoriteAddresses, err := models.FavoriteAddressesList(clientUUID)
	if err != nil {
		msg := "error getting favorite addresses list"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "getting favorite addresses list",
			"reason": msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusNotFound, res)
	}
	return c.JSON(http.StatusOK, favoriteAddresses)
}

// // GetFavoriteAddress -
// func GetFavoriteAddress(c echo.Context) error {
// 	uuid := c.Param("uuid")
// 	if uuid == "" {
// 		msg := emptyUUIDMsg
// 		logs.Eloger.WithFields(logrus.Fields{
// 			"event":  "getting favorite address by uuid",
// 			"reason": msg,
// 		}).Error()
// 		res := logs.OutputRestError(msg, fmt.Errorf(msg), http.StatusBadRequest)
// 		return c.JSON(http.StatusBadRequest, res)
// 	}
// 	var favoriteAddress models.FavoriteAddress
// 	err := models.GetByUUID(uuid, &favoriteAddress)
// 	if err != nil {
// 		msg := "error getting favorite address by uuid"
// 		logs.Eloger.WithFields(logrus.Fields{
// 			"event":               "getting favorite address by uuid",
// 			"favoriteAddressUUID": uuid,
// 			"reason":              msg,
// 		}).Error(err)
// 		res := logs.OutputRestError(msg, err, http.StatusNotFound)
// 		return c.JSON(http.StatusNotFound, res)
// 	}
// 	return c.JSON(http.StatusOK, favoriteAddress)
// }

func UpdateFavoriteAddress(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "update favorite address",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, fmt.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	clientUUID := userUUIDFromJWT(c)
	var favoriteAddress models.FavoriteAddress
	if err := c.Bind(&favoriteAddress); err != nil {
		msg := bindingDataErrorMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":               "update favorite address",
			"favoriteAddressUUID": uuid,
			"clientUUID":          clientUUID,
			"reason":              msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	uFavoriteAddress, err := favoriteAddress.Update(uuid)
	if err != nil {
		msg := "error favorite address update"
		logs.Eloger.WithFields(logrus.Fields{
			"event":               "update favorite address",
			"favoriteAddressUUID": uuid,
			"clientUUID":          clientUUID,
			"reason":              msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":               "update favorite address",
		"clientUUID":          clientUUID,
		"favoriteAddressUUID": uuid,
	}).Info("favorite address update done successfully")
	return c.JSON(http.StatusOK, uFavoriteAddress)
}

func DeleteFavoriteAddress(c echo.Context) error {
	uuid := c.Param("uuid")
	if uuid == "" {
		msg := emptyUUIDMsg
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "delete favorite address",
			"reason": msg,
		}).Error()
		res := logs.OutputRestError(msg, fmt.Errorf(msg), http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}
	var favoriteAddress models.FavoriteAddress
	favoriteAddress.UUID = uuid
	clientUUID := userUUIDFromJWT(c)
	err := favoriteAddress.SetDeleted()
	if err != nil {
		msg := "error delete favorite address"
		logs.Eloger.WithFields(logrus.Fields{
			"event":               "delete favorite address",
			"clientUUID":          clientUUID,
			"favoriteAddressUUID": uuid,
			"reason":              msg,
		}).Error(err)
		res := logs.OutputRestError(msg, err, http.StatusBadRequest)
		return c.JSON(http.StatusBadRequest, res)
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":               "delete favorite address",
		"clientUUID":          clientUUID,
		"favoriteAddressUUID": uuid,
	}).Info("favorite address delete done successfully")
	res := logs.OutputRestOK("favorite address delete")
	return c.JSON(http.StatusOK, res)
}
