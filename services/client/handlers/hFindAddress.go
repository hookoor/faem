package handlers

import (
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/phoneverify"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"

	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
)

// FindAddress - получение адреса по координатам
func FindAddress(c echo.Context) error {
	var coor tool.Coordinates

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "address search",
	})

	err := c.Bind(&coor)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "error binding data").Error(err)
		res := logs.OutputRestError("Error binding data [coordinates]", err)
		return c.JSON(http.StatusBadRequest, res)
	}
	log.WithField("coords", fmt.Sprintf("lat:%v,lon:%v", coor.Lat, coor.Long))

	var findRoute structures.Route
	var url string = config.St.CRM.BaseURL + "/findaddress"
	err = tool.SendRequest(http.MethodPost, url, nil, coor, &findRoute)
	if err != nil {
		err = errpath.Err(err)
		log.WithField("reason", "sending request error").Error(err)
		res := logs.OutputRestError("sending request error", err)
		return c.JSON(http.StatusBadRequest, res)
	}

	log.Info("address found successfully")

	return c.JSON(http.StatusOK, findRoute)
}

// GetLastCustomerAddresses - получение последних адресов клиента
func GetLastCustomerAddresses(c echo.Context) error {
	type addressWithMarker struct {
		Name    string           `json:"name"`
		Tag     string           `json:"tag"`
		Address structures.Route `json:"address"`
	}
	var result []addressWithMarker

	clientUUID := userUUIDFromJWT(c)
	destination := c.Param("destination")

	count := 7 // TODO: вынести в конфиг (кол последних адресов для клиента)

	findFavoriteAddresses, err := models.FavoriteAddressesList(clientUUID)
	if err != nil {
		msg := "error getting customer favorite addresses"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "getting the last customer addresses",
			"reason":     msg,
			"clientUUID": clientUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	for _, item := range findFavoriteAddresses {
		result = append(result, addressWithMarker{
			Name:    item.Name,
			Address: item.Address,
			Tag:     item.Tag,
		})
	}

	findRoute, err := models.LastCustomerAddressesList(clientUUID, destination, count)
	if err != nil {
		msg := "error getting last customer addresses"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "getting the last customer addresses",
			"reason":     msg,
			"clientUUID": clientUUID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	for _, item := range findRoute {
		result = append(result, addressWithMarker{Address: item})
	}

	logs.Eloger.WithFields(logrus.Fields{
		"event":      "GetLastCustomerAddresses",
		"clientUUID": clientUUID,
	}).Info("last customer addresses found successfully")

	return c.JSON(http.StatusOK, result)
}

// BuildRouteWay - возвращает данные для построения маршрута
func BuildRouteWay(c echo.Context) error {
	var (
		err         error
		routePoints []structures.Route
	)

	err = c.Bind(&routePoints)
	if err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "route construction",
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	if len(routePoints) < 2 {
		msg := fmt.Sprintf("invalid routes count required:2, got:%v", len(routePoints))
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "route construction",
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	routesForLog := fmt.Sprintf("from %s to %s", routePoints[0].Value, routePoints[1].Value)
	routeWayData, err := models.GetRouteWayData(routePoints)
	if err != nil {
		msg := "error getting route way data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "route construction",
			"routes": routesForLog,
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":  "route construction",
		"routes": routesForLog,
	}).Info("route construction done successfully")

	return c.JSON(http.StatusOK, routeWayData)
}

// SetTelegramID - записывает telegram_id в базу
func SetTelegramID(c echo.Context) error {
	var ti models.TelegramID

	err := c.Bind(&ti)
	if err != nil {
		msg := "error binding data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "bind telegram id",
			"reason": msg,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	ti.Phone, err = phoneverify.NumberVerify(ti.Phone)
	if err != nil {
		res := logs.OutputRestError("number verify error", err)
		return c.JSON(http.StatusUnauthorized, res)
	}

	msg, err := models.SaveTelegramID(ti)
	if err != nil {
		msg := "error getting route way data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "bind telegram id",
			"reason":     "save telegram id",
			"phone":      ti.Phone,
			"telegramID": ti.TelegramID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":      "bind telegram id",
		"phone":      ti.Phone,
		"telegramID": ti.TelegramID,
	}).Info("route construction done successfully")

	return c.JSON(http.StatusOK, logs.OutputRestOK(msg, 200))
}

// GetTelegramID - получает телефона по telegram_id
func GetTelegramID(c echo.Context) error {
	telegramID := c.Param("telegram_id")

	phone, err := models.GetPhoneByTelegramID(telegramID)
	if err != nil {
		msg := "error getting route way data"
		logs.Eloger.WithFields(logrus.Fields{
			"event":       "get telegram id",
			"reason":      "GetPhoneByTelegramID",
			"telegram_id": telegramID,
		}).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	return c.JSON(http.StatusOK, phone)
}
