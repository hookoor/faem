package handlers

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

func CheckClientAppVersion(c echo.Context) error {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "check app version",
	})

	clientAppVer := new(models.ClientAppVersion)
	if err := c.Bind(clientAppVer); err != nil {
		msg := "error binding data while checking client app version"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}
	log.WithField("device_id", clientAppVer.DeviceID)

	response := new(structures.ResponseStruct)
	url := config.St.CRM.BaseURL + "/clients/appversion"
	if err := tool.SendRequest(http.MethodPost, url, nil, clientAppVer.AppVersion, response); err != nil {
		msg := "error sending data to CRM while checking client app version"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	if err := clientAppVer.SaveToDB(); err != nil {
		msg := "error saving data to client_app_version"
		log.WithField("reason", msg).Error(err)
		return c.JSON(http.StatusBadRequest, logs.OutputRestError(msg, err))
	}

	return c.JSON(http.StatusOK, response)
}
