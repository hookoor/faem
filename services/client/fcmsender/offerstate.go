package fcmsender

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

type FCMOfferState struct {
	structures.OfferStates
	Title string `json:"state_title"`
}
