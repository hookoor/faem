package fcmsender

import (
	"errors"
	"fmt"

	fcm "github.com/NaySoftware/go-fcm"
	"github.com/sirupsen/logrus"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

var (
	fcmclient *fcm.FcmClient
)

const (
	defaultChannel = "fcm_client_default_channel"
	chatChannel    = "fcm_client_message_chat_channel_new"
)

//Init godoc
func Init(fclient *fcm.FcmClient) {
	fcmclient = fclient
	fcmclient.SetPriority(fcm.Priority_HIGH)
}

// // SendMessageToCLientByAction выбиракм экшен и отправляем
// func SendMessageToCLientByAction(action string, clientToken string, payload interface{}) error {
// 	var err error
// 	switch action {
// 	case OrderStateAction:
// 		err = SendMessageToCLient(payload, clientToken, 120, notifyTitle)
// 	case DriverLocationAction:
// 		err = SendJSON(rabbit.OrderExchange, key[0], payload)

// 	default:
// 		err = fmt.Errorf("Unknown action, cant send")
// 	}
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

// SendMessageToCLient отправляет клиенту с переданным clientToken сообщение с переданным payload
func SendMessageToCLient(payload interface{}, tag string, clientTokens []string, lifetime int, notifyTitle string, notifyBody string, contentAv ...bool) error {
	fcmstr := fcmpkg.FcmStruct{
		Tag:                 tag,
		NotificationMessage: getNotificationMess(notifyTitle, notifyBody),
		Payload:             payload,
	}
	if notifyTitle != "" {
		fcmclient.Message.Notification.AndroidChannelID = defaultChannel
	} else {
		fcmclient.Message.Notification.AndroidChannelID = ""
	}

	if tag == fcmpkg.ChatMessageTag || tag == fcmpkg.ChatMessagesReadTag {
		fcmclient.Message.Notification.AndroidChannelID = chatChannel
	}
	if len(clientTokens) == 0 {
		return errors.New("empty client tokens array")
	}
	fcmclient.NewFcmRegIdsMsg(clientTokens, fcmstr)
	fcmclient.Message.Notification.Title = notifyTitle
	fcmclient.Message.Notification.Body = notifyBody
	fcmclient.Message.ContentAvailable = false
	if len(contentAv) != 0 {
		fcmclient.Message.ContentAvailable = contentAv[0]
	}
	status, err := fcmclient.Send()
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":        "Send JSON to FCM",
			"clientTokens": clientTokens,
		}).Error(err)
		return fmt.Errorf("sending request error,%s", err)
	}
	if status.StatusCode != 200 {
		errMsg := "ошибка отправки сообщения"
		logs.Eloger.WithFields(logrus.Fields{
			"event":        "Send JSON to FCM",
			"clientTokens": clientTokens,
		}).Error("error sending data to client")
		status.PrintResults()
		if len(status.Results) != 0 {
			return fmt.Errorf("%s: %s, код: %v", errMsg, status.Results[0]["error"], status.StatusCode)
		}
		return fmt.Errorf("%s, код: %v", errMsg, status.StatusCode)
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event":        "Send JSON to FCM",
		"clientTokens": clientTokens,
	}).Info("Sended to FCM")
	return nil
}

func getNotificationMess(title, body string) string {
	res := title
	if body != "" {
		res = res + ". " + body
	}
	return res
}
