package rabsender

import (
	"encoding/json"
	"fmt"
	"sync"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
)

var (
	rb *rabbit.Rabbit
	wg sync.WaitGroup

	// AcOrderFromClientApp ключ нового заказа
	AcOrderFromClientApp = "new_order"
	// AcOrderState ключ статуса заказа
	AcOrderState = "orderstate"
	// AcFCMMessage новое сообщение в fcm
	AcFCMMessage             = "fcm_message"
	AcNewOrderRoutesData     = "new_order_routes"
	AcIncreasedFare          = "inc_fare"
	AcSendNewClient          = "client_change"
	AcClientActivityChanged  = "client_activity_changed"
	AcNewClientLocation      = "new_client_loc"
	AcClientKarmaChanged     = "client_karma_changed"
	AcClientBlacklistChanged = "client_blacklist_changed"
	AcTipToDriver            = "tip_to_driver"
	AcMakeReceipt            = "make_receipt"
	AcNotifyDriverAboutTip   = "notify_driver_about_tip"
	AcCentrifugo             = "send_by_centrifugo"
	AcClientUpdate           = "change_to_crm"
)

// InitRabNewOrder данные для получения новых заказов от ClientApp
func InitRabNewOrder(r *rabbit.Rabbit) error {

	// var rh RabbitHandler
	rb = r

	// err := rb.SenderChan.ExchangeDeclare(
	// 	rabbit.ClientExchNewOrder, // name
	// 	"direct",                  // type
	// 	true,                      // durable
	// 	false,                     // auto-deleted
	// 	false,                     // internal
	// 	false,                     // no-wait
	// 	nil,                       // arguments
	// )
	// if err != nil {
	// 	rsp := fmt.Errorf("Error creating exchange %s. %s", rabbit.ClientExchNewOrder, err)
	// 	return rsp
	// }

	// err = rb.SenderChan.ExchangeDeclare(
	// 	rabbit.OrderStatesExchange, // name
	// 	"topic",                    // type
	// 	true,                       // durable
	// 	false,                      // auto-deleted
	// 	false,                      // internal
	// 	false,                      // no-wait
	// 	nil,                        // arguments
	// )
	// if err != nil {
	// 	rsp := fmt.Errorf("Error creating exchange %s. %s", rabbit.OrderStatesExchange, err)
	// 	return rsp
	// }

	return nil
}

// SendJSONByAction выбираeм экшен и отправляем
func SendJSONByAction(action string, payload interface{}, key ...string) error {
	var err error
	switch action {
	case AcFCMMessage:
		err = SendJSON(rabbit.FCMExchange, key[0], payload)
	case AcOrderFromClientApp:
		err = SendJSON(rabbit.OrderExchange, rabbit.NewKey, payload)
	case AcOrderState:
		err = SendJSON(rabbit.OrderExchange, key[0], payload)
	case AcNewClientLocation:
		err = SendJSON(rabbit.ClientExchange, rabbit.ClientLocationKey, payload)
	case AcSendNewClient:
		err = SendJSON(rabbit.ClientExchange, rabbit.NewKey, payload)
	case AcNewOrderRoutesData:
		err = SendJSON(rabbit.OrderExchange, rabbit.UpdateOrderRoutesKey, payload)
	case AcIncreasedFare:
		err = SendJSON(rabbit.OrderExchange, rabbit.UpdateIncreasedFareKey, payload)
	case AcClientActivityChanged:
		err = SendJSON(rabbit.ClientExchange, rabbit.ActivityChangedKey, payload)
	case AcClientKarmaChanged:
		err = SendJSON(rabbit.ClientExchange, rabbit.KarmaChangedKey, payload)
	case AcClientBlacklistChanged:
		err = SendJSON(rabbit.ClientExchange, rabbit.BlacklistChangedKey, payload)
	case AcTipToDriver:
		err = SendJSON(rabbit.BillingExchange, rabbit.NewKey, payload)
	case AcMakeReceipt:
		err = SendJSON(rabbit.BillingExchange, rabbit.GenerateReceipt, payload)
	case AcNotifyDriverAboutTip:
		err = SendJSON(rabbit.FCMExchange, rabbit.MailingKey, payload)
	case AcCentrifugo:
		err = SendJSON(rabbit.NotifierExchange, rabbit.NewKey, payload)
	case AcClientUpdate:
		err = SendJSON(rabbit.ClientExchange, rabbit.UpdateKey, payload)
	default:
		err = fmt.Errorf("Unknown action, cant send")
	}
	if err != nil {
		return err
	}
	return nil
}

// SendJSON сабж
func SendJSON(exchange string, key string, payload interface{}) error {
	wg.Add(1)
	defer wg.Done()

	amqpHeader := amqp.Table{}
	amqpHeader["publisher"] = "client"

	pl, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	senderChannel, err := rb.GetSender("clientSender")
	if err != nil {
		return errors.Wrap(err, "failed to get a sender channel")
	}

	err = senderChannel.Publish(
		exchange, // exchange
		key,      // routing key
		false,    // mandatory
		false,    // immediate
		amqp.Publishing{
			ContentType:  "application/json",
			Body:         pl,
			Headers:      amqpHeader,
			DeliveryMode: amqp.Persistent,
		})
	keyExch := fmt.Sprintf("key=%s, exchange=%s", key, exchange)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event": "Sending to Rabbit [client]",
			"value": keyExch,
		}).Error(err)
		return err
	}
	logs.Eloger.WithFields(logrus.Fields{
		"event": "Sending to Rabbit [client]",
		"value": keyExch,
	}).Debug("Sended: OK!")

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("publisher stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("publisher stopped forcibly")
	}
}
