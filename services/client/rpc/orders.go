package rpc

import (
	"net/http"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/config"
)

const (
	EndpointForOrdersDataInStates = "/orders/data/bystates"
)

func GetOrdersDataByStates(states []string) ([]structures.OrderUUIDWithStateTransferState, error) {
	url := config.St.CRM.BaseURL + EndpointForOrdersDataInStates
	args := structures.OrdersDataByStatesArgs{States: states}
	var ordersData []structures.OrderUUIDWithStateTransferState
	err := RPC(http.MethodPost, url, args, &ordersData)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get orders data by states via rpc")
	}
	return ordersData, nil
}
