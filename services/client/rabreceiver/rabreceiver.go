package rabreceiver

import (
	"fmt"
	"sync"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/client/handlers"
)

var (
	rb     *rabbit.Rabbit
	wg     sync.WaitGroup
	server *handlers.Server
	closed = make(chan struct{})
)

func RabReceiverInit(r *rabbit.Rabbit) error {
	var err error
	rb = r

	err = InitDriverLocationToClient()
	if err != nil {
		return fmt.Errorf("Error InitDriverLocationToClient. %s", err)
	}
	err = InitSMSReceiver()
	if err != nil {
		return fmt.Errorf("Error InitSMSReceiver. %s", err)
	}

	err = InitFCMMessageReceiver()
	if err != nil {
		return fmt.Errorf("Error InitFCMMessageReceiver. %s", err)
	}

	err = InitRabOrderUpdate()
	if err != nil {
		return fmt.Errorf("Error InitRabOrderUpdate. %s", err)
	}

	err = initOrderState()
	if err != nil {
		return fmt.Errorf("Error initOrderState. %s", err)
	}

	err = initTicketsEvents()
	if err != nil {
		return fmt.Errorf("Error initTicketsEvents. %s", err)
	}

	err = initNewOptionsToClient()
	if err != nil {
		return fmt.Errorf("Error initNewOptionsToClient. %s", err)
	}

	err = InitRabCreateOrChangedClient()
	if err != nil {
		return fmt.Errorf("Error InitRabCreateOrChangedClient. %s", err)
	}

	err = initRabOrderSetRating()
	if err != nil {
		return fmt.Errorf("Error initRabOrderSetRating. %s", err)
	}

	err = initRabOrderSetRouteToClient()
	if err != nil {
		return fmt.Errorf("Error initRabOrderSetRouteToClient. %s", err)
	}

	if err = initPaymentTypeChanged(); err != nil {
		return fmt.Errorf("Error initPaymentTypeChanged. %s", err)
	}

	if err = initReceiptGenerated(); err != nil {
		return fmt.Errorf("Error initReceiptGenerated. %s", err)
	}

	err = initReferralFirstRide()
	if err != nil {
		return fmt.Errorf("Error initReferralFirstRide. %s", err)
	}

	return nil
}

func Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("subscriber stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("subscriber stopped forcibly")
	}
}
