package rabreceiver

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// InitSMSReceiver godoc
func InitSMSReceiver() error {
	receiverChannelSMS, err := rb.GetReceiver(rabbit.ClientSMSQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannelSMS.ExchangeDeclare(
		rabbit.ClientExchange, // name
		"topic",               // type
		true,                  // durable
		false,                 // auto-deleted
		false,                 // internal
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := receiverChannelSMS.QueueDeclare(
		rabbit.ClientSMSQueue, // name
		true,                  // durable
		false,                 // delete when unused
		false,                 // exclusive
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannelSMS.QueueBind(
		q.Name,                // queue name
		rabbit.SMSKey,         // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannelSMS.Consume(
		q.Name,                   // queue
		rabbit.ClientSMSConsumer, // consumer
		true,                     // auto-ack
		false,                    // exclusive
		false,                    // no-local
		false,                    // no-wait
		nil,                      // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewSMS(msgs)
	return nil
}

func handleNewSMS(sms <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-sms:
			var data structures.NewSMSData
			err := json.Unmarshal(d.Body, &data)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event": "handling new sms",
				"phone": data.Phone,
				"text":  data.Text,
			})
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error unmarshalling data",
				}).Error(err)
				continue
			}
			go server.Sender.SendSMS(data.Phone, data.Text, server.Sender.From)
			log.Info("successfully")
		}
	}
}
