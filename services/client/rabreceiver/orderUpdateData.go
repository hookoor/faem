package rabreceiver

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

// InitRabOrderUpdate данные для получения водителей из брокера
func InitRabOrderUpdate() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientOrderUpdateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientOrderUpdateQueue, // name
		true,                          // durable
		false,                         // delete when unused
		false,                         // exclusive
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.UpdateKey+".#", // routing key
		rabbit.OrderExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,                            // queue name
		rabbit.OrderUpdateFromCRMKey+".#", // routing key
		rabbit.OrderExchange,              // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                           // queue
		rabbit.ClientOrderUpdateConsumer, // consumer
		true,                             // auto-ack
		false,                            // exclusive
		false,                            // no-local
		false,                            // no-wait
		nil,                              // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderUpdate(msgs)
	return nil
}

func handleOrderUpdate(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "client" {
				continue
			}
			var needSendClientInitDataMessage bool
			tag, ok := d.Headers["tag"].(string)
			if ok && tag == rabbit.OrderUpdateByOperatorRequestTag || d.RoutingKey == rabbit.UpdateOrderProductData {
				needSendClientInitDataMessage = true
			}
			var (
				orderClient models.OrderCApp
			)
			err := json.Unmarshal(d.Body, &orderClient.Order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling update order data",
					"reason":     "Error unmarshalling order",
					"routingKey": d.RoutingKey,
					"OrderUUID":  orderClient.UUID,
				}).Error(err)
				continue
			}
			switch d.RoutingKey {
			case rabbit.UpdateOrderTariffKey:
				err = models.UpdateOrderTariff(orderClient.UUID, orderClient.Tariff)
			case rabbit.UpdateOrderProductData:
				err = models.UpdateOrderProductData(orderClient.UUID, orderClient.GetProductsData(), needSendClientInitDataMessage)
			default:
				err = orderClient.UpdateFromBroker(orderClient.UUID, needSendClientInitDataMessage)
			}
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling update order data",
					"reason":     "Error update order",
					"routingKey": d.RoutingKey,
					"OrderUUID":  orderClient.UUID,
				}).Error(err)
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling update order data",
				"OrderUUID":  orderClient.UUID,
				"routingKey": d.RoutingKey,
			}).Info("Order successfully update")
		}
	}
}
