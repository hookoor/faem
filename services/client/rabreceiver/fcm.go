package rabreceiver

import (
	"encoding/json"
	"strconv"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

const (
	bindingError                  = "error binding data"
	maxFCMTokenNumberInOneRequest = 990
)

// InitFCMMessageReceiver godoc
func InitFCMMessageReceiver() error {
	receiverChannelFCM, err := rb.GetReceiver(rabbit.ClientFCMQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = receiverChannelFCM.ExchangeDeclare(
		rabbit.FCMExchange, // name
		"topic",            // type
		true,               // durable
		false,              // auto-deleted
		false,              // internal
		false,              // no-wait
		nil,                // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := receiverChannelFCM.QueueDeclare(
		rabbit.ClientFCMQueue, // name
		true,                  // durable
		false,                 // delete when unused
		false,                 // exclusive
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannelFCM.QueueBind(
		q.Name,             // queue name
		"#",                // routing key
		rabbit.FCMExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannelFCM.Consume(
		q.Name,                   // queue
		rabbit.ClientFCMConsumer, // consumer
		true,                     // auto-ack
		false,                    // exclusive
		false,                    // no-local
		false,                    // no-wait
		nil,                      // args
	)
	if err != nil {
		return err
	}

	receiverChannelChat, err := rb.GetReceiver(rabbit.ClientChatQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	qChat, err := receiverChannelChat.QueueDeclare(
		rabbit.ClientChatQueue, // name
		true,                   // durable
		false,                  // delete when unused
		false,                  // exclusive
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannelChat.QueueBind(
		qChat.Name,                    // queue name
		rabbit.ChatMessageToClientKey, // routing key
		rabbit.FCMExchange,            // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	err = receiverChannelChat.QueueBind(
		qChat.Name,                         // queue name
		rabbit.ChatMessagesMarkedAsReadKey, // routing key
		rabbit.FCMExchange,                 // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	mess, err := receiverChannelChat.Consume(
		qChat.Name,                // queue
		rabbit.ClientChatConsumer, // consumer
		true,                      // auto-ack
		false,                     // exclusive
		false,                     // no-local
		false,                     // no-wait
		nil,                       // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go readingNewFCMMessageToClient(msgs)
	wg.Add(1)
	go readingNewChatMessageToClient(mess)
	return nil
}
func handleNewFCMMessageToClient(d amqp.Delivery) {
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handle new fcm init data message",
		"routingKey": d.RoutingKey,
	})

	switch d.RoutingKey {
	case rabbit.InitDataKey:
		var (
			data structures.InitDataStruct
		)
		err := json.Unmarshal([]byte(d.Body), &data)
		if err != nil {
			log.WithField("reason", bindingError).Error(errpath.Err(err))
			return
		}

		err = models.SendMessageToClientWithCentrifugo(
			data.ClientUUID,
			data.NotifyBody, // был nil
			fcmpkg.InitDataTag,
			data.Token,
			120,
			data.NotifyTitle,
			data.NotifyBody,
			true,
		)
		if err != nil {
			log.Error(errpath.Err(err))
			return
		}
		log.Info(errpath.Infof("Message sent successfully"))
	case rabbit.LocationKey:
		var (
			drvLoc structures.DriverLocWithToken
		)
		err := json.Unmarshal([]byte(d.Body), &drvLoc)
		if err != nil {
			log.WithField("reason", bindingError).Error(errpath.Err(err))
			return
		}

		err = models.SendMessageToClientWithCentrifugo(
			drvLoc.ClientUUID,
			drvLoc.DriverLoc,
			fcmpkg.DriverLocTag,
			drvLoc.Token,
			25,
			"",
			"",
			true,
		)
		if err != nil {
			log.Error(errpath.Err(err))
			return
		}
		log.Info(errpath.Infof("Message sent successfully"))
	case rabbit.StateKey:
		var (
			orState       structures.OrderStateWithToken
			orStateClient models.OrderStateCApp
		)
		err := json.Unmarshal([]byte(d.Body), &orState)
		if err != nil {
			log.WithField("reason", bindingError).Error(errpath.Err(err))
			return
		}
		log.WithFields(logrus.Fields{
			"ClientUUID":  orState.ClientUUID,
			"Order state": orState.OrdState,
		})

		orStateClient.OfferStates = orState.OrdState
		notTitle, notifyBody := orStateClient.GetNotifyTitleAndBody(orState.ProductDeliveryData)
		fcmPayload := fcmsender.FCMOfferState{
			OfferStates: orState.OrdState,
			Title:       constants.TranslateOrderStateForClient(orState.OrdState.State, orState.ProductDeliveryData.IsProductsDelivery),
		}
		if fcmPayload.OfferStates.Comment != "" {

			// Костыль!!! По возможности убрать техническая инфу из Comment
			// в Comment иногда приходит техническая инфа в виде числа.
			// если приходит техническая инфа, то не записывать в notifyBody
			// если приходит описание смены статуса, то записывать в notifyBody
			var isint bool = true
			if _, err := strconv.Atoi(fcmPayload.OfferStates.Comment); err != nil {
				isint = false
			}
			if !isint {
				if notifyBody == "" {
					notifyBody = fcmPayload.OfferStates.Comment
				}
			}
			if notTitle == "" {
				notTitle = fcmPayload.Title
			}
		} else {
			if notifyBody == "" {
				notifyBody = fcmPayload.Title
			}
		}

		{
			// [Зачем]: клиенту приходили избыточные пуши
			// (заказ предложен водителю, заказ не был доставлен водителю, перевод в распределение, далеко от заказа ...)
			// meta: тригерились по колбэку смены статуса
			if !orState.ProductDeliveryData.IsProductsDelivery {
				if !constants.IsStateForPushToClient(orState.OrdState.State) {
					return
				}
			}
		}

		err = models.SendMessageToClientWithCentrifugo(
			orState.ClientUUID,
			fcmPayload,
			fcmpkg.OrderStateTag,
			orState.Token,
			120,
			notTitle,
			notifyBody,
			true,
		)
		if err != nil {
			log.Error(errpath.Err(err))
			return
		}
		log.Info(errpath.Infof("Message sent successfully"))
	default:
		log.Warn(errpath.Infof("Unknown routing key"))
	}
}

func readingNewFCMMessageToClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.RoutingKey == rabbit.MailingKey {
				go handleMessageToAllClients(d)
				continue
			}
			go handleNewFCMMessageToClient(d)
		}
	}
}

func readingNewChatMessageToClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.RoutingKey == rabbit.ChatMessageToClientKey {
				go handleNewChatMessageToClient(d)
			}
			if d.RoutingKey == rabbit.ChatMessagesMarkedAsReadKey {
				go handleNewChatMessageMarkReadToClient(d)
			}
		}
	}
}

func handleNewChatMessageMarkReadToClient(d amqp.Delivery) {
	var messages structures.ChatMessagesMarkedAsRead

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handle new chat message marked as read",
		"routingKey": d.RoutingKey,
	})
	err := json.Unmarshal([]byte(d.Body), &messages)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingError,
		}).Error(err)
		return
	}
	if messages.ReaderType != structures.DriverMember ||
		messages.SenderType != structures.ClientMember {
		return
	}
	var order models.OrderCApp
	err = models.GetByUUID(messages.OrderUUID, &order)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":    "error getting client fcm token",
			"orderUUID": messages.OrderUUID,
		}).Error(err)
		return
	}
	token, err := models.GetCurrentClientFCMToken(order.ClUUID)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason":     "error getting client fcm token",
			"clientUUID": order.ClUUID,
		}).Error(err)
		return
	}

	err = models.SendMessageToClientWithCentrifugo(
		order.ClUUID,
		messages,
		fcmpkg.ChatMessagesReadTag,
		token,
		300,
		"",
		"",
	)
	if err != nil {
		err = errpath.Err(err)
		logs.Eloger.WithFields(logrus.Fields{
			"reason":     "error getting client fcm token",
			"clientUUID": order.ClUUID,
		}).Error(err)
		return
	}
}

func handleMessageToAllClients(d amqp.Delivery) {
	var (
		message structures.PushMailing
	)
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "handle new message for all clients",
		"routingKey": d.RoutingKey,
	})
	err := json.Unmarshal([]byte(d.Body), &message)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": bindingError,
		}).Error(err)
		return
	}

	uuidFeatToken := map[string]string{}
	switch message.Type {
	case structures.MailingAllClientsType:
		uuidFeatToken, err = models.GetAllClientUUIDsAndTokens()
	case structures.MailingForFewClientType:
		uuidFeatToken, err = models.GetClientTokensByPhonesOrUUIDs(message.TargetsUUIDs, message.TargetsPhones)
	default:
		return
	}
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error getting clients tokens",
		}).Error(errpath.Err(err))
		return
	}

	mailingTag := fcmpkg.MailingTag
	payload := interface{}(message.Message)
	if message.Payload != nil && message.Payload.URL != "" {
		mailingTag = fcmpkg.MailingURL
		payload = message.Payload
	}

	for key, val := range uuidFeatToken {
		go func(clientuuid, clientToken string) {
			err = models.SendMessageToClientWithCentrifugo(
				clientuuid,
				payload,
				mailingTag,
				clientToken,
				500000,
				message.Title,
				message.Message,
			)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "SendMessageToClientWithCentrifugo",
				}).Error(errpath.Err(err))
			}
		}(key, val)
	}
}

func handleNewChatMessageToClient(d amqp.Delivery) {
	var (
		message structures.ChatMessages
	)
	err := json.Unmarshal([]byte(d.Body), &message)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handle new chat message",
			"reason":     bindingError,
			"routingKey": d.RoutingKey,
		}).Error(err)
		return
	}
	if message.Receiver != structures.ClientMember {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handle new chat message",
			"reason":     "invalid receiver",
			"receiver":   message.Receiver,
			"routingKey": d.RoutingKey,
		}).Error()
		return
	}
	if message.ClientUUID == "" {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handle new chat message",
			"reason":     "empty client uuid",
			"receiver":   message.Receiver,
			"routingKey": d.RoutingKey,
		}).Error()
		return
	}
	token, err := models.GetCurrentClientFCMToken(message.ClientUUID)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handle new chat message",
			"reason":     "error getting client fcm token",
			"clientUUID": message.ClientUUID,
			"routingKey": d.RoutingKey,
		}).Error(err)
		return
	}

	err = models.SendMessageToClientWithCentrifugo(
		message.ClientUUID,
		message,
		fcmpkg.ChatMessageTag,
		token,
		300,
		"Новое сообщение",
		"Вам пришло новое сообщение от водителя",
		true,
	)
	if err != nil {
		err = errpath.Err(err)
		logs.Eloger.WithFields(logrus.Fields{
			"event":      "handle new chat message",
			"reason":     "error getting client fcm token",
			"clientUUID": message.ClientUUID,
			"routingKey": d.RoutingKey,
		}).Error(err)
		return
	}
}
