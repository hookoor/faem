package rabreceiver

import (
	"encoding/json"
	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

func initNewOptionsToClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientOptionsOrder)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для получения новых опций с CRM
	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientOptionsOrder, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения новых опций
	err = receiverChannel.QueueBind(
		q.Name,                  // queue name
		rabbit.OptionsUpdateKey, // routing key
		rabbit.OrderExchange,    // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера на получение новых опций
	c, err := receiverChannel.Consume(
		q.Name,                                  // queue
		rabbit.ClientNewOptionsToClientConsumer, // consumer
		true,                                    // auto-ack
		false,                                   // exclusive
		false,                                   // no-local
		false,                                   // no-wait
		nil,                                     // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewOptionsToClient(c)
	return nil
}

func handleNewOptionsToClient(newOptions <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-newOptions:
			if d.Headers["publisher"] == "client" {
				continue
			}
			var (
				dataFromCrm        structures.DataWithMarker
				allServicesFromCrm []structures.Service
				allFeaturesFromCrm []structures.Feature
			)

			err := json.Unmarshal([]byte(d.Body), &dataFromCrm)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling new options from crm",
					"reason": "Error unmarshalling options",
				}).Error(err)
				continue
			}

			if dataFromCrm.Marker == constants.MarkerFeatures {
				allFeaturesFromCrm = dataFromCrm.Features

				var (
					FeatureFromCrm models.FeatureCApp
					allFeatures    []models.FeatureCApp
				)

				err = models.DeleteAllFeatures()
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling new options from crm",
						"reason": "Error delete all feature",
					}).Error(err)
					continue
				}

				for _, ftr := range allFeaturesFromCrm {
					FeatureFromCrm.Feature = ftr
					allFeatures = append(allFeatures, FeatureFromCrm)
				}

				models.FillTableFeatures(allFeatures)

				logs.Eloger.WithFields(logrus.Fields{
					"event": "handling new options from crm",
				}).Info("successful save feature list")

				continue
			}
			if dataFromCrm.Marker == constants.MarkerServices {
				allServicesFromCrm = dataFromCrm.Services

				var (
					ServiceFromCrm models.ServiceCApp
					allServices    []models.ServiceCApp
				)

				err = models.DeleteAllServices()
				if err != nil {
					logs.Eloger.WithFields(logrus.Fields{
						"event":  "handling new options from crm",
						"reason": "Error delete all services",
					}).Error(err)
					continue
				}

				for _, srv := range allServicesFromCrm {
					ServiceFromCrm.Service = srv
					allServices = append(allServices, ServiceFromCrm)
				}

				models.FillTableServices(allServices)

				logs.Eloger.WithFields(logrus.Fields{
					"event": "handling new options from crm",
				}).Info("successful save service list")

				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":  "handling new options from crm",
				"reason": "Error invalid data on new options",
			}).Error(err)
		}
	}
}
