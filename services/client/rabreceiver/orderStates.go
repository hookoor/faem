package rabreceiver

import (
	"encoding/json"

	"github.com/pkg/errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

// initOrderStateDriver godo c
func initOrderState() error {

	// db = conn

	receiverChannel, err := rb.GetReceiver(rabbit.ClientOrderStateQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь для  получения статусов заказов
	orderStateQueue, err := receiverChannel.QueueDeclare(
		rabbit.ClientOrderStateQueue, // name
		true,                         // durable
		false,                        // delete when unused
		false,                        // exclusive
		false,                        // no-wait
		nil,                          // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения статусов заказов
	err = receiverChannel.QueueBind(
		orderStateQueue.Name, // queue name
		"state.*",            // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера на получение статусов заказов
	newORDST, err := receiverChannel.Consume(
		orderStateQueue.Name,                  // queue
		rabbit.ClientOrderStateDriverConsumer, // consumer
		true,                                  // auto-ack
		false,                                 // exclusive
		false,                                 // no-local
		false,                                 // no-wait
		nil,                                   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleNewOrderStates(newORDST)
	return nil
}

func handleNewOrderStates(orderStates <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-orderStates:
			var orderState models.OrderStateCApp
			err := json.Unmarshal(d.Body, &orderState.OfferStates)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling new order states",
				"state":     orderState.State,
				"orderUUID": orderState.OrderUUID,
				"offerUUID": orderState.OfferUUID,
			})
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "Error unmarshalling order state"}).Error(err)
				continue
			}
			_, err = orderState.SaveToDB()
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "Error saving order state"}).Error(err)
				continue
			}
			log.Info("successful save orderstate")

			switch orderState.State {
			case constants.OrderStateOnTheWay:
				if err := handleOnTheWay(orderState); err != nil {
					logs.Eloger.WithField("uuid", orderState.OrderUUID).WithError(err).Error()
				}
			case constants.OrderStateFinished:
				if err := handleFinished(orderState); err != nil {
					logs.Eloger.WithField("uuid", orderState.OrderUUID).WithError(err).Error()
				}
			}

			order, checkExistsOrder, err := models.CheckOrderExists(orderState.OrderUUID)
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "Error check order exists"}).Error(err)
				continue
			}
			// Change client's activity if needed
			if err = models.ChangeClientsActivityIfNeeded(orderState.OfferStates); err != nil {
				log.WithFields(logrus.Fields{"reason": "changing client activity"}).Error(err)
			}
			if !checkExistsOrder {
				continue
			}
			var ordr models.OrderCApp
			err = ordr.UpdateOrderStateFromBroker(orderState.OrderUUID, orderState.OfferStates)
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "update order state"}).Error(errpath.Err(err))
			}

			if orderState.State == constants.OrderStateOnTheWay {
				orderState.ArrivalTime, err = models.UpdateOrderArrivalTime(orderState.OrderUUID)
				if err != nil {
					log.WithFields(logrus.Fields{"reason": "Error update order arrival time"}).Error(err)
				}
			}
			err = models.UpdateOrderTariffIfNeed(orderState.OrderUUID, orderState.State)
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "Error update order tariff"}).Error(err)
			}
			if skipOrderStateSending(orderState.State, order.Service.ProductDelivery) {
				log.Info("no orderstate sending required")
				continue
			}
			storeData := order.GetProductsData().StoreData

			err = orderState.SendToClient(structures.OrderDeliveryData{
				IsProductsDelivery: order.Service.ProductDelivery,
				OwnDelivery:        order.OwnDelivery || storeData.HasOwnDelivery(),
				WithoutDelivery:    order.WithoutDelivery,
			})
			if err != nil {
				log.WithFields(logrus.Fields{"reason": "Error send to client"}).Error(err)
				continue
			}

			log.Info("successful send orderstate to client")
		}
	}
}

func handleOnTheWay(orderState models.OrderStateCApp) error {
	ord := new(models.OrderCApp)
	if err := models.GetByUUID(orderState.OrderUUID, ord); err != nil {
		return errors.Wrap(err, "fail to get order by uuid")
	}

	client := new(models.ClientsApps)
	if err := models.GetByUUID(ord.ClUUID, client); err != nil {
		return errors.Wrap(err, "fail to get client data by uuid")
	}

	token, err := models.GetCurrentClientFCMToken(client.UUID)
	if err != nil {
		return err
	}

	return models.SendMessageToClientWithCentrifugo(
		client.UUID,
		structures.PushInsurance.Payload,
		fcm.MailingURL,
		token,
		500000,
		structures.PushInsurance.Title,
		structures.PushInsurance.Message,
	)
}

func handleFinished(orderState models.OrderStateCApp) error {
	ord := new(models.OrderCApp)
	err := models.GetByUUID(orderState.OrderUUID, ord)
	if err != nil {
		return errors.Wrap(err, "fail to get order by uuid")
	}
	if !ord.FromClientApp() {
		return nil
	}

	client := new(models.ClientsApps)
	err = models.GetByUUID(ord.ClUUID, client)
	if err != nil {
		return errors.Wrap(err, "fail to get client data by uuid")
	}

	uuid := client.ReferralProgramData.ParentUUID

	if uuid == "" {
		return nil
	}

	parentTripCount, ok := ClientFriendTripCountBuffer[uuid]
	if ok {
		ClientFriendTripCountBuffer[uuid] = parentTripCount + 1
	} else {
		ClientFriendTripCountBuffer[uuid] = 1
	}

	logs.Eloger.Info("finished client incomes: ", ClientFriendTripCountBuffer)

	return nil
}

var ClientFriendTripCountBuffer = map[string]int{}

// skipOrderStateSending надо ли отправлять статус на устройство
func skipOrderStateSending(state string, isProdDelivery bool) bool {
	if !isProdDelivery {
		return false
	}
	for _, st := range skippedStatuses() {
		if state == st {
			return true
		}
	}
	return false
}
func skippedStatuses() []string {
	return []string{
		constants.OrderStateOffered,
		constants.OrderStateFree,
		constants.OrderStateDistributing,
		constants.OrderStateCreated,
		constants.OrderStateRejected,
		constants.OrderStateAccepted,
		constants.OrderStateStarted,
		constants.OrderStateOnPlace,
		constants.OrderStateWaiting,
	}
}
