package rabreceiver

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

// initRabOrderSetRouteToClient -
func initRabOrderSetRouteToClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientSetRouteToClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientSetRouteToClientQueue, // name
		true,                               // durable
		false,                              // delete when unused
		false,                              // exclusive
		false,                              // no-wait
		nil,                                // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,               // queue name
		rabbit.RouteToClient, // routing key
		rabbit.OrderExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                                // queue
		rabbit.ClientSetRouteToClientConsumer, // consumer
		true,                                  // auto-ack
		false,                                 // exclusive
		false,                                 // no-local
		false,                                 // no-wait
		nil,                                   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleOrderSetRouteToClient(msgs)
	return nil
}

func handleOrderSetRouteToClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			var (
				order models.OrderCApp
			)

			err := json.Unmarshal([]byte(d.Body), &order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order route to client",
					"reason":    "Error unmarshalling order",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}

			err = models.UpdateOrderRouteToClient(&order)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":     "handling update order route to client",
					"reason":    "Error update order route to client",
					"OrderUUID": order.UUID,
				}).Error(err)
				continue
			}
			logs.Eloger.WithFields(logrus.Fields{
				"event":     "handling update order route to client",
				"OrderUUID": order.UUID,
			}).Info("Order route to client successfully update")
		}
	}
}
