package rabreceiver

import (
	"encoding/json"
	"fmt"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

func initTicketsEvents() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientTicketsQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}
	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientTicketsQueue, // name
		true,                      // durable
		false,                     // delete when unused
		false,                     // exclusive
		false,                     // no-wait
		nil,                       // arguments
	)
	if err != nil {
		return err
	}
	err = receiverChannel.QueueBind(
		q.Name,                 // queue name
		"#",                    // routing key
		rabbit.TicketsExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	msgs, err := receiverChannel.Consume(
		q.Name,                       // queue
		rabbit.ClientTicketsConsumer, // consumer
		true,                         // auto-ack
		false,                        // exclusive
		false,                        // no-local
		false,                        // no-wait
		nil,                          // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleTickets(msgs)
	return nil
}

func handleTickets(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "client" {
				continue
			}
			if d.RoutingKey != rabbit.StateKey && d.RoutingKey != rabbit.UpdateKey {
				continue
			}
			var (
				tick structures.Ticket
			)
			err := json.Unmarshal(d.Body, &tick)
			log := logs.Eloger.WithFields(logrus.Fields{
				"event":       "handling tickets",
				"ticketUUID":  tick.UUID,
				"ticketState": tick.Status,
				"routingKey":  d.RoutingKey,
			})
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "Error unmarshalling data",
				}).Error(err)
				continue
			}
			if (d.RoutingKey == rabbit.StateKey && !tick.Status.IsFiniteState()) || tick.SourceType != structures.ClientMember {
				continue
			}
			token, err := models.GetCurrentClientFCMTokenByPhone(tick.ClientData.Phone)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "error getting token",
				}).Error(err)
				continue
			}
			var msgForAlert, body, msg string
			var lastComment structures.TicketComment
			if len(tick.Comments) != 0 {
				lastComment = tick.Comments[len(tick.Comments)-1]
				body = lastComment.Message
			}
			if d.RoutingKey == rabbit.StateKey {
				switch tick.Status {
				case structures.ResolvedStatus:
					msg = "Ваше обращение решено!"
				case structures.NotResolvedStatus:
					msg = "Ваше обращение не решено("
				}
				msgForAlert = fmt.Sprintf("%s Комментарий администратора: %s", msg, body)
			}
			if d.RoutingKey == rabbit.UpdateKey {
				if lastComment.SenderType != structures.UserCRMMember {
					continue
				}
				msg = "Новый комментарий к вашему обращению"
				msgForAlert = fmt.Sprintf("%s : %s", msg, body)
			}

			err = models.SendMessageToClientWithCentrifugo(
				tick.ClientData.UUID,
				msgForAlert,
				fcmpkg.MailingTag,
				token,
				36000,
				msg,
				body,
			)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason": "SendMessageToClientWithCentrifugo",
				}).Error(errpath.Err(err))
			}

			log.WithFields(logrus.Fields{
				"reason": "done successfully",
			}).Info("new ticket state handled")
		}
	}
}
