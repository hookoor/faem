package rabreceiver

import (
	"encoding/json"

	"github.com/go-pg/pg"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/handlers"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"
	"gitlab.com/faemproject/backend/faem/services/client/rpc"
)

var (
	db *pg.DB
	s  *pg.DB
)

// ConnectDB initialize connection to package
func ConnectDB(conn *pg.DB) {
	db = conn
}

func ConnectSMPPServer(conn *handlers.Server) {
	server = conn
}

// InitDriverLocationToClient - создает все необходимое для получения координат
// Вынесен отдельно для дальнешейго более легкого переноса на микросервис
func InitDriverLocationToClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientDriverLocationQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Объявляем очередь
	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientDriverLocationQueue, // name
		true,                             // durable
		false,                            // delete when unused
		false,                            // exclusive
		false,                            // no-wait
		nil,                              // argumentscc
	)
	if err != nil {
		return err
	}
	// Биндим очередь
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.LocationKey,    // routing key
		rabbit.DriverExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := receiverChannel.Consume(
		q.Name, // queue
		rabbit.ClientDriverLocationClientConsumer, // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleLocations(msgs)
	return nil
}

// HandleLocations сохраняет полученные координаты
func handleLocations(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			var drvLoc []models.DriverLocationDataForClient
			err := json.Unmarshal(d.Body, &drvLoc)
			if err != nil {
				logs.Eloger.Errorf("Error unmarshal location data. DriverID=%s. %s", d.ConsumerTag, err)
				continue
			}

			// Get orders with required states first
			crmOrders, err := rpc.GetOrdersDataByStates(constants.ListActiveOrderStates())
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handleLocations",
					"reason": "RPC error",
				}).Error(err)
				continue
			}
			if len(crmOrders) < 1 {
				continue
			}

			// Get CRM order uuids
			orderUUIDs := make([]string, 0, len(crmOrders))
			orderToDriverMap := make(map[string]string)
			for _, co := range crmOrders {
				orderUUIDs = append(orderUUIDs, co.UUID)
				orderToDriverMap[co.UUID] = co.DriverUUID
			}

			// Map orders to the clients (though we could fetch client data right from the CRM)
			var clientOrders []models.OrderCApp
			err = db.Model(&clientOrders).
				WhereIn("uuid IN (?)", orderUUIDs).
				Where("deleted IS NOT TRUE").
				Select()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handleLocations",
					"reason": "SQL error",
				}).Error(err)
				continue
			}
			if len(clientOrders) < 1 {
				continue
			}

			// Get client UUIDs
			clientUUIDs := make([]string, 0, len(clientOrders))
			clientToDriverMap := make(map[string]string) // clientUUID to driverUUID
			for _, co := range clientOrders {
				clientUUIDs = append(clientUUIDs, co.ClUUID)
				clientToDriverMap[co.ClUUID] = orderToDriverMap[co.UUID]
			}

			// Get client fcm tokens
			tokens, err := models.GetCurrentClientsFCMTokens(clientUUIDs)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handleLocations",
					"reason": "SQL error",
				}).Error(err)
				continue
			}
			if len(tokens) < 1 {
				continue
			}

			// Удаляем все записи с неуникальными DriverUUID для локаций
			locationMap := make(map[string]models.DriverLocationDataForClient)
			for _, loc := range drvLoc {
				if loc.DriverUUID == "" {
					continue
				}
				locationMap[loc.DriverUUID] = loc // take the most recent location
			}

			for clientUUID, driverUUID := range clientToDriverMap {
				drvLocItem, found := locationMap[driverUUID]
				if !found {
					continue
				}
				coords := structures.Coordinates{
					Lat:        drvLocItem.Latitude,
					Long:       drvLocItem.Longitude,
					DriverUUID: driverUUID,
				}

				token, found := tokens[clientUUID]
				if !found {
					continue
				}
				rabhandler.SendJSONByAction(
					rabhandler.AcFCMMessage,
					structures.DriverLocWithToken{
						ClientUUID: clientUUID,
						Token:      token,
						DriverLoc:  coords,
					},
					rabbit.LocationKey,
				)

				logs.Eloger.WithField("event", "send driver locations to client").
					Tracef("(%v, %v)", coords.Lat, coords.Long)
			}
		}
	}
}
