package rabreceiver

import (
	"encoding/json"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/client/models"

	"time"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
)

// initRabNewClient данные для получения клиентов из брокера
func InitRabCreateOrChangedClient() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientNewOrChangedClientQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientNewOrChangedClientQueue, // name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	if err != nil {
		return err
	}
	// Биндим очередь для получения клиентов
	err = receiverChannel.QueueBind(
		q.Name,                // queue name
		rabbit.UpdateKey,      // routing key
		rabbit.ClientExchange, // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                          // queue
		rabbit.ClientNewOrChangedClient, // consumer
		true,                            // auto-ack
		false,                           // exclusive
		false,                           // no-local
		false,                           // no-wait
		nil,                             // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleCreateOrChangedClient(msgs)
	return nil
}

func handleCreateOrChangedClient(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "client" {
				continue
			}
			var (
				clientApp models.ClientsApps
			)

			err := json.Unmarshal([]byte(d.Body), &clientApp.Client)
			if err != nil {

				logs.Eloger.WithFields(logrus.Fields{
					"event":  "handling create or change client",
					"reason": "Error unmarshalling client",
				}).Error(err)

				continue
			}

			clientApp.CreatedAt = time.Now()

			err = clientApp.CreateOrUpdateClient()
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "handling create or change client",
					"reason":     "Error saving new client",
					"clientUUID": clientApp.UUID,
				}).Error(err)
				continue
			}

			logs.Eloger.WithFields(logrus.Fields{
				"event":      "handling new or changed client",
				"clientUUID": clientApp.UUID,
			}).Info("ClientBackend created or update client")
		}
	}
}
