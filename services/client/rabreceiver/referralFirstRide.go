package rabreceiver

import (
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

// initReferralFirstRide -
func initReferralFirstRide() error {
	receiverChannel, err := rb.GetReceiver(rabbit.ClientReferralFirstRideQueue)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	q, err := receiverChannel.QueueDeclare(
		rabbit.ClientReferralFirstRideQueue, // name
		true,                                // durable
		false,                               // delete when unused
		false,                               // exclusive
		false,                               // no-wait
		nil,                                 // arguments
	)
	if err != nil {
		return err
	}

	err = receiverChannel.QueueBind(
		q.Name,                      // queue name
		rabbit.ReferralFirstRideKey, // routing key
		rabbit.ClientExchange,       // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}
	// Подключаем консюмера
	msgs, err := receiverChannel.Consume(
		q.Name,                                 // queue
		rabbit.ClientReferralFirstRideConsumer, // consumer
		true,                                   // auto-ack
		false,                                  // exclusive
		false,                                  // no-local
		false,                                  // no-wait
		nil,                                    // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleFirstRide(msgs)
	return nil
}

func handleFirstRide(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "handling client's first ride completion",
	})

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			var clientUUID string
			err := json.Unmarshal(d.Body, &clientUUID)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason":  "error unmarshaling client UUID",
					"payload": d.Body,
				}).Error(err)
				continue
			}

			err = models.MakeClientNotNewcomer(clientUUID)
			if err != nil {
				log.WithFields(logrus.Fields{
					"reason":      "unable to update client's newcomer status",
					"client_uuid": clientUUID,
				}).Error(err)
				continue
			}

			log.WithFields(logrus.Fields{
				"client_uuid": clientUUID,
			}).Info("Client completed his first ride")
		}
	}
}
