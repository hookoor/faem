package rabreceiver

import (
	"encoding/json"

	"github.com/korovkin/limiter"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"

	"gitlab.com/faemproject/backend/faem/pkg/lang"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

const (
	maxPaymentTypeChangedAllowed = 10
	maxReceiptGeneratedAllowed   = 10
)

func initPaymentTypeChanged() error {
	channel, err := rb.GetReceiver(rabbit.ClientBillingPaymentTypeChanged)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = channel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := channel.QueueDeclare(
		rabbit.ClientBillingPaymentTypeChanged, // name
		true,                                   // durable
		false,                                  // delete when unused
		false,                                  // exclusive
		false,                                  // no-wait
		nil,                                    // arguments
	)
	if err != nil {
		return err
	}

	err = channel.QueueBind(
		q.Name,                          // queue name
		rabbit.PaymentTypeChangedToCash, // routing key
		rabbit.BillingExchange,          // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := channel.Consume(
		q.Name, // queue
		rabbit.ClientBillingPaymentTypeChangedConsumer, // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handlePaymentTypeChanged(msgs)
	return nil
}

func handlePaymentTypeChanged(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling payment type changed"

	limit := limiter.NewConcurrencyLimiter(maxPaymentTypeChangedAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "client" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var prepay structures.PrepayOrder
					if err := json.Unmarshal(d.Body, &prepay); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					log := logs.Eloger.WithFields(logrus.Fields{
						"event":   event,
						"orderID": prepay.SubjectUUID,
					})
					if err := models.ChangeOrderPaymentTypeToCash(prepay.SubjectUUID); err != nil {
						log.Error(err)
					} else {
						log.Debug("payment type successfully changed")
					}
				},
			))
		}
	}
}

// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

func initReceiptGenerated() error {
	channel, err := rb.GetReceiver(rabbit.ClientBillingReceiptGenerated)
	if err != nil {
		return errors.Wrapf(err, "failed to get a receiver channel")
	}

	// Declare an exchange first
	err = channel.ExchangeDeclare(
		rabbit.BillingExchange, // name
		"topic",                // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrap(err, "failed to create an exchange")
	}

	q, err := channel.QueueDeclare(
		rabbit.ClientBillingReceiptGenerated, // name
		true,                                 // durable
		false,                                // delete when unused
		false,                                // exclusive
		false,                                // no-wait
		nil,                                  // arguments
	)
	if err != nil {
		return err
	}

	err = channel.QueueBind(
		q.Name,                  // queue name
		rabbit.ReceiptGenerated, // routing key
		rabbit.BillingExchange,  // exchange
		false,
		nil,
	)
	if err != nil {
		return err
	}

	msgs, err := channel.Consume(
		q.Name, // queue
		rabbit.ClientBillingReceiptGeneratedConsumer, // consumer
		true,  // auto-ack
		false, // exclusive
		false, // no-local
		false, // no-wait
		nil,   // args
	)
	if err != nil {
		return err
	}

	wg.Add(1)
	go handleReceiptGenerated(msgs)
	return nil
}

func handleReceiptGenerated(msgs <-chan amqp.Delivery) {
	defer wg.Done()

	const event = "handling receipt generated"

	limit := limiter.NewConcurrencyLimiter(maxReceiptGeneratedAllowed)
	defer limit.Wait()

	for {
		select {
		case <-closed:
			return
		case d := <-msgs:
			if d.Headers["publisher"] == "client" {
				continue
			}

			limit.Execute(lang.Recover(
				func() {
					var receipt structures.ReceiptHook
					if err := json.Unmarshal(d.Body, &receipt); err != nil {
						logs.Eloger.WithField("event", event).Error(err)
						return
					}

					log := logs.Eloger.WithFields(logrus.Fields{
						"event":   event,
						"orderID": receipt.OrderUUID,
					})
					if err := models.FillGeneratedReceipt(receipt); err != nil {
						log.Error(err)
					} else {
						log.Debug("generated receipt successfully handled")
					}
				},
			))
		}
	}
}
