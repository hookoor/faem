package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	xos "gitlab.com/faemproject/backend/faem/pkg/os"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/smpp"
	"gitlab.com/faemproject/backend/faem/pkg/web"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/db"
	"gitlab.com/faemproject/backend/faem/services/client/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/client/handlers"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	"gitlab.com/faemproject/backend/faem/services/client/rabreceiver"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"
	"gitlab.com/faemproject/backend/faem/services/client/router"
	"gitlab.com/faemproject/backend/faem/services/client/tickers"
	"gitlab.com/faemproject/backend/faem/services/client/tickers/tickersmodern"
)

const (
	version               = "0.0.1"
	serverShutdownTimeout = 30 * time.Second
	brokerShutdownTimeout = 30 * time.Second
)

func main() {
	bVersion := flag.Bool("ver", false, "Output current app vesion and exit")
	bEnvVars := flag.Bool("env", false, "Output envieroinment vars and exit")
	configPath := flag.String("config", "config/client.toml", "Default config filepath")

	flag.Parse()

	config.InitConfig(*configPath)

	if *bVersion {
		fmt.Println("Version: ", version)
		os.Exit(0)
	}

	if *bEnvVars {
		config.PrintVars()
		os.Exit(0)
	}

	if err := logs.SetLogLevel(config.St.Application.LogLevel); err != nil {
		log.Fatalf("Failed to set log level: %v", err)
	}
	if err := logs.SetLogFormat(config.St.Application.LogFormat); err != nil {
		log.Fatalf("Failed to set log format: %v", err)
	}
	logger := logs.Eloger

	conn, err := db.Connect()
	if err != nil {
		es := fmt.Sprintf("Error connecting to DB. %s", err)
		fmt.Println(es)
		os.Exit(4)
	}
	defer db.CloseDbConnection(conn)
	// logs.OutputInfo("Connecting to Postgres successfuly")
	fmt.Println("Connecting to Postgres successfuly", conn)

	models.ConnectDB(conn)
	rabreceiver.ConnectDB(conn)

	// подключение к кролику
	r := rabbit.New()
	r.Credits.URL = config.St.Broker.UserURL
	r.Credits.User = config.St.Broker.UserCredits
	err = r.Init(config.St.Broker.ExchagePrefix, config.St.Broker.ExchagePostfix)
	if err != nil {
		fmt.Println("Rabbit init error", err)
		os.Exit(4)
	}
	//подключение к сервису ShortMessagePeer2Peer
	smsSender, err := smpp.NewSmsSender(config.St.SMPP.Host, config.St.SMPP.User, config.St.SMPP.Password, config.St.SMPP.From)
	if err != nil {
		logs.Eloger.WithFields(logrus.Fields{
			"event":  "init",
			"reason": "Error connecting to SMPP server",
		}).Error(err)
	}

	// трассировка подключений
	go func() {
		for c := range smsSender.ConnStatus {
			logs.Eloger.WithFields(logrus.Fields{
				"value": c.Status(),
			}).Info("SMPP connection status")
		}
	}()

	// асинхронная проверка статусов отправки СМС
	go func() {
		for {
			mes := <-smsSender.SenderStatus
			switch mes.State {
			case "Ok":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Info("SMS sended!")
			case "Warn":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Warn("Sender attempt")
			case "Error":
				logs.Eloger.WithFields(logrus.Fields{
					"event": "SMS sender",
					"value": mes.Msg,
				}).Warn("Sms not send")
			}
			if mes.State == "OK" {
			}
		}
	}()

	srv := handlers.NewServer(
		smsSender,
		tickersmodern.Init(conn),
	)
	// logrus.Warnf("Connection [%s] | Credits[%s] | Chan [%s]", r.Connection, r.Credits, r.SenderChan)
	defer r.CloseRabbit()
	e := router.Init(srv)
	rabreceiver.ConnectSMPPServer(srv)
	err = rabhandler.InitRabNewOrder(r)
	if err != nil {
		es := fmt.Sprintf("Error InitRabNewOrder. %s", err)
		fmt.Println(es)
		os.Exit(4)
	}
	defer rabhandler.Wait(brokerShutdownTimeout) // wait publisher first

	err = rabreceiver.RabReceiverInit(r)
	if err != nil {
		es := fmt.Sprintf("Error RabReceiverInit. %s", err)
		fmt.Println(es)
		os.Exit(4)
	}
	defer rabreceiver.Wait(brokerShutdownTimeout)

	tickers.InitiateTickers()

	models.ConnectRabbit(r)
	fcmCl := fcm.NewClient(config.St.FCM.ServerKey)
	fcmsender.Init(fcmCl)
	// Start an http server and remember to shut it down
	router.InitLogger(logger)
	go web.Start(e, config.St.Application.Port)
	defer web.Stop(e, serverShutdownTimeout)

	// Wait for program exit
	<-xos.NotifyAboutExit()
}
