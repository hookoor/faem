package models

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/gofrs/uuid"
)

// ServiceCApp - order service structure
type ServiceCApp struct {
	tableName struct{} `sql:"client_services" pg:",discard_unknown_columns"`
	ormStruct
	structures.Service
}

// Create - save structure to database
func (sr *ServiceCApp) Create() (ServiceCApp, error) {
	nService, err := sr.validCreation()
	if err != nil {
		return nService, fmt.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nService, err
	}

	nService.UUID = uu.String()
	_, err = db.Model(&nService).Returning("*").Insert()
	if err != nil {
		return nService, fmt.Errorf("DB error. %v ", err)
	}
	return nService, nil
}

func (sr *ServiceCApp) validCreation() (ServiceCApp, error) {
	var newService ServiceCApp
	if sr.Name == "" {
		return newService, fmt.Errorf("Field name is required")
	}
	if sr.PriceCoeff == 0 {
		return newService, fmt.Errorf("Field priceCoeff is required")
	}
	newService.Name = sr.Name
	newService.PriceCoeff = sr.PriceCoeff
	newService.Freight = sr.Freight
	newService.Comment = sr.Comment
	newService.Tag = sr.Tag
	return newService, nil
}

// SaveToDB - сохраняет сервис в базу
func (sr ServiceCApp) SaveToDB() (ServiceCApp, error) {
	sr.CreatedAt = time.Now()
	_, err := db.Model(&sr).Returning("*").Insert()
	return sr, err
}

// ServiceByID - return Service structure based on ID
func ServiceByID(id int) (ServiceCApp, error) {
	var service ServiceCApp
	service.ID = id
	err := db.Model(&service).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return service, err
	}
	return service, nil
}

// GetDefaultService - return Service structure based on ID
func GetDefaultService() (ServiceCApp, error) {
	var service ServiceCApp
	err := db.Model(&service).
		Where("deleted is not true AND \"default\" = true").
		Select()
	if err != nil {
		return service, err
	}
	return service, nil
}

// ServicesList - return all Services
func ServicesList() ([]ServiceCApp, error) {
	var services []ServiceCApp
	err := db.Model(&services).
		Where("deleted is not true ").
		Select()
	if err != nil {
		return services, err
	}
	return services, nil
}

// Update godoc
func (sr *ServiceCApp) Update(uuid string) (ServiceCApp, error) {

	uService, err := sr.validUpdate(uuid)
	if err != nil {
		return uService, fmt.Errorf("Data validation error. %v", err)
	}

	err = UpdateByPK(&uService)
	if err != nil {
		return uService, fmt.Errorf("DB error. %v", err)
	}
	return uService, nil
}

//
func (sr *ServiceCApp) validUpdate(uuid string) (ServiceCApp, error) {

	flag := false
	var service ServiceCApp

	if sr.Name != "" {
		service.Name = sr.Name
		flag = true
	}
	if sr.Freight != nil {
		service.Freight = sr.Freight
		flag = true
	}
	if sr.Comment != "" {
		service.Comment = sr.Comment
		flag = true
	}
	if sr.PriceCoeff != 0 {
		service.PriceCoeff = sr.PriceCoeff
		flag = true
	}
	if !flag {
		return service, fmt.Errorf("Required fields are empty")
	}
	service.UpdatedAt = time.Now()
	service.UUID = uuid
	err := CheckExistsUUID(&service, uuid)
	if err != nil {
		return service, err
	}
	service.Deleted = false
	return service, nil
}

// DeleteAllServices - удаляет все записи в таблице services
func DeleteAllServices() error {
	_, err := db.Model(&ServiceCApp{}).Where("id<>?", 0).Delete()
	if err != nil {
		return err
	}
	return nil
}

// FillTableServices - заполняет записями таблицу services
func FillTableServices(serviceArr []ServiceCApp) error {
	_, err := db.Model(&serviceArr).Insert()
	if err != nil {
		return err
	}
	return nil
}
