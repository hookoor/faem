package models

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/rabsender"
)

// TODO: move to the remote config
const (
	clientMaxRating               = 5
	clientSignificantRatingCount  = 50
	clientSignificantRatingPeriod = time.Hour * 24

	driverMinBlacklistRating = 2
)

type (
	OrderRatingClient struct {
		tableName struct{} `sql:"client_order_rating"`
		ID        int
		Tip       int `json:"tip"`
		structures.OrderRating
		CreatedAt time.Time
	}
)

func (orRat *OrderRatingClient) Save() error {
	err := orRat.validate()
	if err != nil {
		return fmt.Errorf("validate error: %s", err)
	}
	_, err = db.Model(orRat).Insert()
	if err != nil {
		return fmt.Errorf("insert error: %s", err)
	}
	return err
}

func (orRat *OrderRatingClient) validate() error {
	var order OrderCApp
	err := db.Model(&order).Where("uuid = ? AND cl_uuid = ?", orRat.OrderUUID, orRat.ClientUUID).Select()
	if err != nil {
		return fmt.Errorf("error find order ,%s", err)
	}
	if orRat.Value > 5 || orRat.Value < 0 {
		return fmt.Errorf("invalid value")
	}
	orRat.DriverUUID = order.Driver.UUID
	if orRat.Tip < 0 {
		return fmt.Errorf("invalid tip")
	}
	return nil
}

func UpdateClientKarma(clientUUID string) (float64, error) {
	if clientUUID == "" {
		return 0, errors.New("empty clientUUID provided")
	}

	significantRatingCount, err := fetchClientSignificantRatingCount()
	if err != nil {
		return 0, errors.Wrap(err, "failed to fetch client significant karma count")
	}

	var ratings []OrderCApp
	if err = db.Model(&ratings).
		Column("driver_rating", "created_at").
		Where("cl_uuid = ?", clientUUID).
		Where("(driver_rating ->> 'value')::integer <> 0").
		Order("created_at DESC").
		Limit(significantRatingCount).
		Select(); err != nil {
		return 0, errors.Wrap(err, "failed to fetch client existing karma count")
	}

	// Total existing karma counts may be less then significant count, append some records to match total
	for len(ratings) < significantRatingCount {
		ratings = append(ratings, OrderCApp{
			Order: structures.Order{
				DriverRating: structures.OrderRatingUnit{Value: clientMaxRating},
			},
			CreatedAt: time.Now().Add(-2 * clientSignificantRatingPeriod), // make a little bit less important by subtracting two periods
		})
	}

	// Count the average
	num, denom := 0., 0.
	for _, rating := range ratings {
		daysDiff := time.Now().Sub(rating.CreatedAt) / clientSignificantRatingPeriod
		if daysDiff < 1 {
			daysDiff = 1
		}
		weight := 1. / float64(daysDiff)
		num += weight * float64(rating.Order.DriverRating.Value)
		denom += weight
	}

	// Update the client
	karma := num / denom
	_, err = db.Model((*ClientsApps)(nil)).
		Where("uuid = ?", clientUUID).
		Set("karma = GREATEST(?, 0)", karma).
		Update()
	if err != nil {
		return 0, errors.Wrap(err, "failed to update client karma")
	}

	// Notify listeners about client's karma changing
	go func() {
		notification := structures.UserKarma{
			UserUUID: clientUUID,
			Karma:    karma,
		}
		if err := rabsender.SendJSONByAction(rabsender.AcClientKarmaChanged, notification); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "notifying about client karma changing",
				"clientUUID": clientUUID,
			}).Error(err)
		}
	}()

	return karma, nil
}

func fetchClientSignificantRatingCount() (int, error) {
	return clientSignificantRatingCount, nil
}

func FetchDriverMinBlacklistRating() (int, error) {
	return driverMinBlacklistRating, nil
}

func UpdateClientBlacklistIfNeeded(orderRating structures.OrderRating, minDriverRating int) error {
	if orderRating.Value > minDriverRating || orderRating.Value == 0 || orderRating.DriverUUID == "" {
		return nil
	}

	var blacklist []string
	_, err := db.Model((*ClientsApps)(nil)).
		Where("uuid = ?", orderRating.ClientUUID).
		Set("blacklist = array_append(blacklist, ?)", orderRating.DriverUUID).
		Returning("blacklist").
		Update(&blacklist)
	if err != nil {
		return errors.Wrap(err, "failed to update client blacklist")
	}

	// Notify crm service about the blacklist changing
	go func() {
		notification := structures.UserBlacklist{
			UserUUID:  orderRating.ClientUUID,
			Blacklist: blacklist,
		}
		if err := rabsender.SendJSONByAction(rabsender.AcClientBlacklistChanged, notification); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "notifying about client blacklist changing",
				"clientUUID": orderRating.ClientUUID,
			}).Error(err)
		}
	}()
	return nil
}
