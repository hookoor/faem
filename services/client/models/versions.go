package models

import (
	"time"

	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

var (
	ErrEmptyDeviceID   = errors.New("empty device_id field")
	ErrEmptyOS         = errors.New("empty os field")
	ErrEmptyAppVersion = errors.New("empty app version field")
)

type ClientAppVersion struct {
	tableName struct{} `sql:"client_app_versions"`

	LastActivity time.Time `sql:"last_activity"`
	structures.AppVersion
}

func (clver ClientAppVersion) validate() error {
	switch "" {
	case clver.DeviceID:
		return ErrEmptyDeviceID
	case clver.OS:
		return ErrEmptyOS
	case clver.Version:
		return ErrEmptyAppVersion
	}

	return nil
}

func (clver ClientAppVersion) SaveToDB() error {
	if err := clver.validate(); err != nil {
		// Legacy проверка, убрать, когда устаревших приложений станет мало (оставить только return err без if else)
		if err == ErrEmptyDeviceID {
			clver.DeviceID = "outdated app (16.03.2021)"
		} else {
			return err
		}
	}
	clver.LastActivity = time.Now()

	if res, err := db.Model(&clver).WherePK().UpdateNotNull(); err != nil {
		return err
	} else if res.RowsAffected() == 0 {
		if _, err := db.Model(&clver).Insert(); err != nil {
			return err
		}
	}

	return nil
}
