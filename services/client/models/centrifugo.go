package models

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/fcmsender"
	"gitlab.com/faemproject/backend/faem/services/client/rabsender"
)

const notifierSenderChannel = "notifierSender"

// SendMessageToClientWithCentrifugo - пытается отослать по центрифуге затем по fcm
func SendMessageToClientWithCentrifugo(clientuuid string, payload interface{}, tag string, clientToken string, lifetime int, notifyTitle string, notifyBody string, contentAv ...bool) error {

	log := logs.Eloger.WithFields(logrus.Fields{
		"event":              "SendMessageToClientWithCentrifugo",
		"clientuuid":         clientuuid,
		"tag":                tag,
		"NotifyTitleAndBody": fmt.Sprintf("notifyTitle = %s; notifyBody = %s", notifyTitle, notifyBody),
		"clientToken":        clientToken,
	})

	skiperr := func() error {
		centrifugoChannel := "client/" + clientuuid

		connExist, err := CentrifugoPresence(centrifugoChannel, clientuuid)
		if err != nil {
			errpath.Err(err)
		}

		if connExist {
			pl := structures.NotificationDesirablePayload{
				Tag:     tag,
				Title:   notifyTitle,
				Message: notifyBody,
				Payload: payload,
			}
			data, err := structures.NewNotification(centrifugoChannel, pl)
			if err != nil {
				return errpath.Err(err)
			}

			err = rabsender.SendJSONByAction(rabsender.AcCentrifugo, data)
			if err != nil {
				return errpath.Err(err)
			}

			log.Info("SendMessageByCentrifugo")

			return nil
		}

		return errpath.Errorf("Centrifugo connect not exist")
	}()

	if skiperr != nil {
		log.Warn(errpath.Err(skiperr, "sending by centrifugo failed"))

		err := fcmsender.SendMessageToCLient(payload, tag, []string{clientToken}, lifetime, notifyTitle, notifyBody, contentAv...)
		if err != nil {
			return errpath.Err(err)
		}
	}

	return nil
}

// CentrifugoPresence -
func CentrifugoPresence(channel, client string) (bool, error) {
	var connExist bool
	epoint := structures.EndPoints.Notyfire.IsPresence

	url := config.St.Notifier.Host + tool.SetURLParams(epoint.URL(), map[string]string{
		epoint.Params.Client():  client,
		epoint.Params.Channel(): channel,
	})
	err := tool.SendRequest(epoint.Method(), url, nil, nil, &connExist)
	if err != nil {
		return false, errpath.Err(err)
	}
	return connExist, nil
}

// -----------------
// -----------------
// -----------------
