package models

import (
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

// FavoriteAddress -
type (
	FavoriteAddress struct {
		tableName   struct{}         `sql:"client_favorite_addresses"`
		UUID        string           `json:"uuid"`
		Name        string           `json:"name"`
		Tag         string           `json:"tag"`
		Description string           `json:"description"`
		ClientUUID  string           `json:"client_uuid"`
		Address     structures.Route `json:"address"`
		ormStruct
	}
)

// Create - save favorite address to database
func (fa *FavoriteAddress) Create(clientUUID string) (FavoriteAddress, error) {
	nFavoriteAddress, err := fa.validCreation()
	if err != nil {
		return nFavoriteAddress, fmt.Errorf("data validation error (create). %v ", err)
	}
	nFavoriteAddress.ClientUUID = clientUUID

	uu, err := uuid.NewV4()
	if err != nil {
		return nFavoriteAddress, err
	}
	nFavoriteAddress.UUID = uu.String()

	_, err = db.Model(&nFavoriteAddress).Returning("*").Insert()
	if err != nil {
		return nFavoriteAddress, fmt.Errorf("DB error. %v ", err)
	}
	return nFavoriteAddress, nil
}

func (fa *FavoriteAddress) validCreation() (FavoriteAddress, error) {
	var newFavoriteAddress FavoriteAddress
	if fa.Name == "" {
		return newFavoriteAddress, fmt.Errorf("field name is required")
	}
	// if fa.Address == "" {
	// 	return newFavoriteAddress, fmt.Errorf("Field address is required")
	// }
	newFavoriteAddress = *fa
	return newFavoriteAddress, nil
}

// FavoriteAddressList - return all favorite address
func FavoriteAddressesList(clientUUID string) ([]FavoriteAddress, error) {
	var (
		favoriteAddresses []FavoriteAddress
	)
	err := db.Model(&favoriteAddresses).
		Where("client_uuid = ?", clientUUID).
		Where("deleted is not true").
		Select()
	if err != nil {
		return nil, err
	}

	return favoriteAddresses, nil
}

// Update - save and return updated favorite address
func (fa *FavoriteAddress) Update(uuid string) (FavoriteAddress, error) {

	uFavoriteAddress, err := fa.validUpdate(uuid)
	if err != nil {
		return *fa, fmt.Errorf("data validation error (update). %v", err)
	}

	_, err = db.Model(&uFavoriteAddress).Where("uuid=?", uuid).Returning("*").UpdateNotNull()
	if err != nil {
		return *fa, err
	}

	return uFavoriteAddress, nil
}

func (fa *FavoriteAddress) validUpdate(uuid string) (FavoriteAddress, error) {
	var newFavoriteAddress FavoriteAddress
	if fa.Name == "" {
		return newFavoriteAddress, fmt.Errorf("field name is required")
	}
	if uuid == "" {
		return newFavoriteAddress, fmt.Errorf("field client_uuid is required")
	}
	// if fa.Address == "" {
	// 	return newFavoriteAddress, fmt.Errorf("Field address is required")
	// }

	newFavoriteAddress.UpdatedAt = time.Now()
	newFavoriteAddress.UUID = uuid
	err := CheckExistsUUID(&newFavoriteAddress, uuid)
	if err != nil {
		return newFavoriteAddress, err
	}

	return *fa, nil
}

// SetDeleted - set delete favorite address
func (fa *FavoriteAddress) SetDeleted() error {
	var (
		favoriteAddress FavoriteAddress
	)
	err := CheckExistsUUID(&favoriteAddress, fa.UUID)
	if err != nil {
		return err
	}
	_, err = db.Model(fa).Where("uuid=?uuid").Set("deleted = true").Update()
	if err != nil {
		return err
	}
	return nil
}
