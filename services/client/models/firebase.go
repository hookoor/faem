package models

import (
	"fmt"
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
)

type (
	//ClientFirebaseToken firebase ключи клиентов
	ClientFirebaseToken struct {
		tableName  struct{}  `sql:"client_firebase_tokens"`
		ID         int       `json:"id" sql:",pk"`
		ClientUUID string    `json:"client_uuid" sql:",unique"`
		Token      string    `json:"token"`
		UpdatedAt  time.Time `json:"updated_at" `
	}
)

// Save сохраняет в базу токен. Если для клиента уже есть токен - обновляет его
func (clFT ClientFirebaseToken) Save() error {
	clFT.UpdatedAt = time.Now()

	_, err := db.Model(&clFT).
		OnConflict("(client_uuid) DO UPDATE").
		Insert()
	return err
}

//GetClientTokensByPhonesOrUUIDs возвращает токены клиентов по uuid или телефонам, если uuid пустые
func GetClientTokensByPhonesOrUUIDs(clientUUIDs, clientPhones []string) (map[string]string, error) {
	var clFT []ClientFirebaseToken
	var err error
	if len(clientUUIDs) != 0 {
		err = db.Model(&clFT).WhereIn("client_uuid in (?)", clientUUIDs).Select()
	} else if len(clientPhones) == 0 {
		return nil, fmt.Errorf("empty arguments")
	} else {
		//сопоставляем номера телефона с uuid и ищем по uuid токены в таблице client_firebase_tokens
		joinQ := "inner join (select token,client_uuid from client_firebase_tokens) as cl_ft on cl_apps.uuid = cl_ft.client_uuid"
		queryText := fmt.Sprintf("select cl_ft.token from  (select uuid,main_phone from client_apps where main_phone in (?)) as cl_apps %s", joinQ)
		_, err = db.Query(&clFT, queryText, pg.In(clientPhones))
	}

	uuidFeatToken := map[string]string{}
	for _, tok := range clFT {
		uuidFeatToken[tok.ClientUUID] = tok.Token
	}
	return uuidFeatToken, err
}

//GetAllClientUUIDsAndTokens возвращает все имеющиеся токены клиентов
func GetAllClientUUIDsAndTokens() (map[string]string, error) {
	var clFT []ClientFirebaseToken

	err := db.Model(&clFT).Select()
	if err != nil {
		return nil, errpath.Err(err)
	}

	uuidFeatToken := map[string]string{}
	for _, tok := range clFT {
		uuidFeatToken[tok.ClientUUID] = tok.Token
	}

	return uuidFeatToken, nil
}

//GetCurrentClientFCMToken возвращает актуальный fcm токен клиента
func GetCurrentClientFCMToken(clientUUID string) (string, error) {
	var (
		clFT ClientFirebaseToken
	)
	err := db.Model(&clFT).Where("client_uuid = ?", clientUUID).Select()
	return clFT.Token, err
}

//GetCurrentClientFCMTokenByPhone возвращает актуальный fcm токен клиента
func GetCurrentClientFCMTokenByPhone(clientPhone string) (string, error) {
	client, err := GetUserByPhone(clientPhone)
	if err != nil {
		return "", fmt.Errorf("error getting client by phone,%s", err)
	}
	return GetCurrentClientFCMToken(client.UUID)
}
func GetCurrentClientsFCMTokens(clientUUIDs []string) (map[string]string, error) {
	if len(clientUUIDs) < 1 {
		return nil, nil
	}

	var clFT []ClientFirebaseToken
	err := db.Model(&clFT).WhereIn("client_uuid IN (?)", clientUUIDs).Select()
	if err != nil {
		return nil, err
	}

	result := make(map[string]string)
	for _, token := range clFT {
		result[token.ClientUUID] = token.Token
	}
	return result, nil
}
