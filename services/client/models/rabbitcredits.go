package models

import (
	"fmt"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/config"
)

type (

	// BrokerData возращает структуру с настройками подключения
	ClientData struct {
		ClientUUID         string `json:"client_uuid"`
		ClientPhone        string `json:"client_phone"`
		DefaultPaymentType string `json:"default_payment_type"`
		ReferralCode       string `json:"referral_code"`
	}
	//ExchangeType godoc
	ExchangeType struct {
		ExcName    string `json:"name"`
		RoutingKey string `json:"routing_key"`
		Mandatory  bool   `json:"mandatory"`
		Immediate  bool   `json:"immediate"`
	}
	OfferForDriver struct {
		structures.OfferTaximetr
		State struct {
			Value   string `json:"value"`
			Message string `json:"message"`
		} `json:"order_state"`
	}
	// ConsumerType структура для подключения к очереди
	ConsumerType struct {
		Queue     string `json:"queue"`
		Consumer  string `json:"consumer"`
		AutoAck   bool   `json:"auto_ack"`
		Exclusive bool   `json:"exclusive"`
		NoLocal   bool   `json:"no_local"`
		NoWait    bool   `json:"no_wait"`
		Args      string `json:"args"`
	}
)

// func getClientQueue(userUUID string) (ConsumerType, error) {
// 	var clApp ClientsApps
// 	clApp.UUID = userUUID

// 	err := db.Model(&clApp).
// 		Where("deleted is not true AND UUID = ?", userUUID).
// 		First()

// 	if err != nil {
// 		return ConsumerType{}, err
// 	}
// 	var ct ConsumerType
// 	ct.AutoAck = true
// 	qdrv := fmt.Sprintf("orderstates.%s", clApp.BrokerQueue)
// 	ct.Queue = qdrv
// 	ct.Consumer = "client." + clApp.BrokerQueue

// 	q, _ := rb.ReciverChan.QueueDeclare(
// 		qdrv,  // name
// 		false, // durable
// 		false, // delete when unused
// 		false, // exclusive
// 		false, // no-wait
// 		nil,   // arguments
// 	)
// 	// key := fmt.Sprintf("orderoffer.%v", driverID)
// 	_ = rb.ReciverChan.QueueBind(
// 		q.Name,                     // queue name
// 		clApp.BrokerQueue,          // routing key
// 		rabbit.OrderStatesExchange, // exchange
// 		false,
// 		nil)

// 	return ct, err
// }

func getClientConnectionString() string {

	clientConnString := fmt.Sprintf("amqp://%s@%s", config.St.Broker.ClientCredits, config.St.Broker.ClientURL)

	return clientConnString
}
