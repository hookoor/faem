package models

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/rabsender"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"

	"github.com/go-pg/pg"
	"github.com/gofrs/uuid"
	"github.com/sirupsen/logrus"
)

const (
	bonusesDataEndpoint = "/bonusesdata"
)

type (
	// ClientsApps structure. The same table structure will be createad in DB
	ClientsApps struct {
		structures.Client
		tableName   struct{}  `sql:"client_apps"`
		ID          int       `json:"id" sql:",pk"`
		Deleted     bool      `json:"-" sql:"default:false"`
		BrokerQueue string    `json:"broker_queue" description:"Comment"`
		CreatedAt   time.Time `sql:"default:now()" json:"created_at"`
		UpdatedAt   time.Time `json:"updated_at" `
	}
)

func GetAllClientNumbers() ([]string, error) {
	var neededData []string
	err := db.Model(&ClientsApps{}).
		ColumnExpr("DISTINCT ON (main_phone) main_phone").
		Where("deleted is not true").
		Where("main_phone is not null").
		Order("main_phone").
		Select(&neededData)
	return neededData, err
}

// ValidateCode валидирует код
func ValidateCode(vdata VerificationCode, serviceName string) (TokenResponse, string, bool, error) {

	var (
		response TokenResponse
		rg       RegisterSession
		// clientState ClientStatesDrv
		err error
	)

	if serviceName == "" {
		serviceName = string(RequestHeaderServiceClient)
	}

	// ЗАПЛАТКА ДЛЯ ВЕРИФИКАЦИИ
	if vdata.Code != GodModeInst.AppsVerificationCode.Value {
		err = db.Model(&rg).
			Where("device_id = ? AND code = ? AND CURRENT_TIMESTAMP < expiration AND used_at IS NULL", vdata.DeviceID, vdata.Code).
			Returning("*").
			Order("created_at DESC").
			Limit(1).
			Select()
		if err != nil {
			if err == pg.ErrNoRows {
				return response, "", false, fmt.Errorf("Код неверный")
			}
			return response, "", false, fmt.Errorf("error finding code, %s", err)
		}
	} else {
		err = db.Model(&rg).
			Where("device_id = ? AND used_at IS NULL", vdata.DeviceID).
			Returning("*").
			Order("created_at DESC").
			Limit(1).
			Select()
		if err != nil {
			return response, "", false, fmt.Errorf("Not found registration with this device ID, %s", err)
		}
	}

	rg.UsedAt = time.Now()
	err = db.Update(&rg)
	if err != nil {
		return TokenResponse{}, rg.Phone, false, err
	}
	// Обнуляем сессиию одноразого пароля
	// Если на этот номер или устройство есть аккаунты их надо заблокировать, а также их refresh токены
	// blockClientsApp(rg.Phone, rg.DeviceID)

	shbe, err := ExistUserByPhone(rg.Phone, vdata.DeviceID)
	if err != nil {
		return TokenResponse{}, rg.Phone, false, err
	}

	// Создаем новый клиенткий профиль
	clientApp, err := findOrCreateClientApp(rg.Phone, rg.DeviceID, vdata.ReferralParentUUID)
	if err != nil {
		return TokenResponse{}, rg.Phone, false, err
	}
	// var newState ClientStatesD

	// _ = db.Model(&clientState).
	// 	Where("client_uuid = ?", clientApp.UUID).
	// 	Returning("*").
	// 	Limit(1).
	// 	Select()
	// if clientState.State == variables.ClientsStates["Moderation"] || clientState.State == variables.ClientsStates["Available"] || clientState.State == variables.ClientsStates["Online"] {
	// 	_, err = SetClientStatus(clientApp.UUID, variables.ClientsStates["Available"], "")
	// 	if err != nil {
	// 		logs.OutputError(fmt.Sprintf("Error saving client status. %s", err))
	// 		// return ClientsApps{}, err
	// 	}
	// } else {
	// 	_, err = SetClientStatus(clientApp.UUID, variables.ClientsStates["Moderation"], "новая регистрация через приложение")
	// 	if err != nil {
	// 		logs.OutputError(fmt.Sprintf("Error saving client state. %s", err))
	// 		// return ClientsApps{}, err
	// 	}
	// }

	// Генерируем и создаем
	response, err = generateJWT(clientApp.UUID, serviceName, rg.DeviceID, rg.Phone, clientApp.CreatedAt.Unix())
	if err != nil {
		return response, rg.Phone, false, err
	}

	return response, rg.Phone, shbe, nil
}

// // blockClientsApp блокирует уже созданные аккаунты с полученным номером телефона или ID устройства
// func blockClientsApp(phone string, deviceID string) {
// 	var drApps []ClientsApps
// 	_ = db.Model(&drApps).
// 		Where("phone = ? OR device_id = ?", phone, deviceID).
// 		Select()
// 	reason := fmt.Sprintf("заблокировано по приничине новой регистрации device_id=%s phone=%s", phone, deviceID)
// 	for _, drv := range drApps {
// 		_, err := SetClientStatus(drv.UUID, "blocked", reason)
// 		if err != nil {
// 			logs.OutputError(err.Error())
// 		}

// 		_ = expireClientToken(drv.UUID)
// 	}
// }

// findOrCreateClientApp возвращает уже созданный или создаен новый экземлпяр ClientsApps
func findOrCreateClientApp(phone string, deviceID string, referral string) (ClientsApps, error) {
	var (
		client ClientsApps
	)
	client.MainPhone = phone
	client.DeviceID = deviceID

	check, err := db.Model(&client).Where("main_phone = ? ", phone).Exists()
	if err != nil {
		return ClientsApps{}, fmt.Errorf("error check user existing,%s", err)
	}
	if !check {
		client.MainPhone = phone
		client.DeviceID = deviceID
		if client.DefaultPaymentType == "" {
			client.DefaultPaymentType = constants.OrderPaymentTypeCash
		}

		newUUID, err := uuid.NewV4()
		if err != nil {
			return ClientsApps{}, err
		}
		client.UUID = newUUID.String()

		{ // заполнение реферальных данных
			refUUID, err := uuid.NewV4()
			if err != nil {
				return ClientsApps{}, errpath.Err(err)
			}
			client.ReferralProgramData.ReferralCode = refUUID.String()
			client.ReferralProgramData.Enable = true
			client.ReferralProgramData.ActivationCount = 0
			client.ReferralProgramData.IsNewcomer = true

			if referral != "" {
				var refClient ClientsApps
				var rowExist bool = true

				err = db.Model(&refClient).Where("referral_program_data ->> 'referral_code' = ?", referral).Select()
				if err != nil {
					if err != pg.ErrNoRows {
						return ClientsApps{}, errpath.Err(err)
					}
					rowExist = false
				}

				if rowExist {
					if refClient.ReferralProgramData.ActivationCount < config.GetCurrentReferralSystemParamsConfig().ActivationLimit {
						client.ReferralProgramData.ReferralParentUUID = referral
						client.ReferralProgramData.ParentUUID = refClient.UUID
						client.ReferralProgramData.ReferralParentPhone = refClient.MainPhone

						_, err = db.Model((*ClientsApps)(nil)).
							Where("referral_program_data ->> 'referral_code' = ?", referral).
							Set(`referral_program_data = referral_program_data || CONCAT('{"activation_count":', COALESCE(referral_program_data ->> 'activation_count','0')::int + 1, '}')::jsonb`).
							Update()
						if err != nil {
							return ClientsApps{}, errpath.Err(err)
						}
					}
				}
			}
		}

		_, err = db.Model(&client).Returning("*").Insert()
		if err != nil {
			return ClientsApps{}, err
		}

		// Reward a new client to promote him in the distribution process
		initialActivity, err := GetInitialClientActivity()
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "get initial activity",
				"ClientUUID": client.UUID,
			}).Error(err)
		} else {
			activityItem := ClientActivityItem{
				ID:             uuid.Must(uuid.NewV4()).String(),
				ClientUUID:     client.UUID,
				Event:          "reward a new client",
				ActivityChange: initialActivity,
			}
			client.Activity, err = AddActivityForClient(activityItem)
			if err != nil {
				logs.Eloger.WithFields(logrus.Fields{
					"event":      "reward a new client with initial activity",
					"ClientUUID": client.UUID,
				}).Error(err)
			}
		}

		// Set the initial rating
		client.Karma, err = UpdateClientKarma(client.UUID)
		if err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "reward a new client with initial rating",
				"clientUUID": client.UUID,
			}).Error(err)
		}

		err = rabhandler.SendJSONByAction(rabhandler.AcSendNewClient, client)
		if err != nil {
			res := fmt.Sprintf("Error sending newOreder to broker. %s", err)
			logs.OutputError(res)
		}
		return client, nil
	}
	err = db.Model(&client).Where("main_phone = ? ", phone).Limit(1).Select()
	if err != nil {
		return ClientsApps{}, fmt.Errorf("error find user,%s", err)
	}
	return client, nil
}

// GetUserByPhone возвращает клиента по телефону
func GetUserByPhone(phone string) (ClientsApps, error) {
	var (
		user ClientsApps
	)
	err := db.Model(&user).Where("main_phone = ?", phone).Where("deleted is not true").Select()
	return user, err
}

// ExistUserByPhone - был ли когда-либо клиент с таким телефоном
func ExistUserByPhone(phone string, deviceID string) (bool, error) {
	var exist bool = false

	var client ClientsApps
	err := db.Model(&client).Where("main_phone = ?", phone).Select()
	if err != nil && err != pg.ErrNoRows {
		return exist, errpath.Err(err)
	}
	if client.UUID != "" {
		exist = true
	} else {
		return exist, nil
	}
	if client.DeviceID != deviceID { // если пользователь зашел с другим deviceID, ему нужно поменять deviceID в базе
		client.DeviceID = deviceID

		_, err = db.Model(&client).Where("uuid = ?", client.UUID).Update()
		if err != nil {
			return exist, errpath.Err(err)
		}

		err = rabhandler.SendJSONByAction(rabsender.AcClientUpdate, client, "")
		if err != nil {
			return exist, errpath.Err(err)
		}
	}

	return exist, nil
}

// GetUserByUUID возвращает клиента по uuid
func GetUserByUUID(userUUID string) (ClientsApps, error) {
	var (
		user ClientsApps
	)
	err := db.Model(&user).Where("uuid = ?", userUUID).Select()

	return user, err
}

// CreateOrUpdateClient - save or update client from CRM
func (ca ClientsApps) CreateOrUpdateClient() error {
	_, err := db.Model(&ca).
		OnConflict("(main_phone) DO UPDATE").
		Insert()
	if err != nil {
		return err
	}
	return nil
}

type TelegramID struct {
	Phone      string `json:"phone"`
	TelegramID string `json:"telegram_id"`
}

//SendInitDataMessageToClient godoc
func SendInitDataMessageToClient(clUUID, title, body string) error {

	token, err := GetCurrentClientFCMToken(clUUID)
	if err != nil {
		return errpath.Err(fmt.Errorf("error getting current client FCM token,%s", err))
	}
	rabhandler.SendJSONByAction(
		rabhandler.AcFCMMessage,
		structures.InitDataStruct{
			ClientUUID:  clUUID,
			Token:       token,
			NotifyBody:  body,
			NotifyTitle: title,
		},
		rabbit.InitDataKey)
	return nil
}
func delaySendingIncFareMessage(clientUUID, orderUUID string, prodDel bool) {
	//если заказ по доставке продуктов, то не отсылать сообщение о повыщении цены
	if prodDel {
		return
	}
	sleepTime := config.St.Application.IncreasedFareUpMessageTimeoutSec
	log := logs.Eloger.WithFields(logrus.Fields{
		"event":      "delay sending inc fare message",
		"clientUUID": clientUUID,
		"orderUUID":  orderUUID,
		"sleepTime":  sleepTime,
	})
	sleep := func() {
		time.Sleep(time.Duration(sleepTime) * time.Second)
	}

	for {
		sleep()
		state, err := GetCurrentOrderState(orderUUID)
		if err != nil {
			log.WithFields(logrus.Fields{
				"reason": "error getting current state",
			}).Error(err)
			return
		}
		//if offered - wait again, distributing - send message, other state - do nothing
		if state == constants.OrderStateOffered {
			continue
		}
		if constants.InDistributingStates(state) {
			break
		}
		return
	}
	err := SendPriceIncreaseMessageToClient(clientUUID)
	if err != nil {
		log.WithFields(logrus.Fields{
			"reason": "error send price increase message",
		}).Error(err)
	}
	return
}

//SendPriceIncreaseMessageToClient godoc
func SendPriceIncreaseMessageToClient(clUUID string) error {

	token, err := GetCurrentClientFCMToken(clUUID)
	if err != nil {
		return errpath.Err(fmt.Errorf("error getting current client FCM token, %s", err))
	}

	title := "Рекомендуем вам повысить стоимость поездки"
	message := "Это привлечет внимание водителей"
	err = SendMessageToClientWithCentrifugo(
		clUUID,
		message, // был nil
		fcmpkg.MailingTag,
		token,
		20,
		title,
		message,
	)
	if err != nil {
		return errpath.Err(err)
	}
	return nil
}

// SaveTelegramID - привязка или регистрация через telegram id
func SaveTelegramID(ti TelegramID) (string, error) {
	var c = ClientsApps{
		Client: structures.Client{
			TelegramID: ti.TelegramID,
			MainPhone:  ti.Phone,
		},
	}

	exist, err := db.Model(&c).Where("main_phone = ?main_phone").Exists()
	if err != nil {
		return "", fmt.Errorf("save telegram id error [%s]", err)
	}
	if exist {
		_, err := db.Model(&c).Set("telegram_id = ?telegram_id").Where("main_phone = ?main_phone").Update()
		if err != nil {
			return "", fmt.Errorf("save telegram id error [%s]", err)
		}
		return "привязка прошла успешно", nil
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return "", fmt.Errorf("creating uuid error [%s]", err)
	}
	c.UUID = uu.String()
	c.Comment = "registration from telegram"
	_, err = db.Model(&c).Insert()
	if err != nil {
		return "", fmt.Errorf("save telegram id error [%s]", err)
	}
	return "регистрация нового номера", nil

}

// GetPhoneByTelegramID - получить номер телефона по telegram id
func GetPhoneByTelegramID(telegramID string) (string, error) {
	var c ClientsApps

	err := db.Model(&c).Where("telegram_id = ? ", telegramID).Select()
	if err != nil {
		return "", fmt.Errorf("error find telegram_id,%s", err)
	}

	return c.MainPhone, nil
}

// GetClientsUUIDByPhones - return map[phone]uuid
func GetClientsUUIDByPhones(phones []string) (map[string]string, error) {
	var clients []ClientsApps
	res := make(map[string]string)

	err := db.Model(&clients).WhereIn("main_phone in (?)", phones).
		Column("main_phone", "uuid").Select()

	for _, cl := range clients {
		res[cl.MainPhone] = cl.UUID
	}

	return res, err
}

// GetBunesesByPhone godoc
func GetBunesesByPhone(phone string) (structures.BonusesData, error) {
	var resp structures.BonusesData
	req := struct {
		Phone string `json:"phone"`
	}{
		Phone: phone,
	}
	err := request(
		http.MethodPost,
		config.St.Bonuses.Host+bonusesDataEndpoint,
		req,
		&resp,
		nil,
	)

	return resp, err
}
