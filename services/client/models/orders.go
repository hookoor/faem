package models

import (
	"bytes"
	"encoding/json"
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"io/ioutil"
	"net/http"
	"sort"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/config"

	"time"

	"github.com/go-pg/pg"
	"github.com/go-pg/pg/urlvalues"
	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

// // Criteria godoc
// type Criteria struct {
// 	MinDate     time.Time `json:"min_date"`
// 	MaxDate     time.Time `json:"max_date"`
// 	OrderState  string    `json:"orderstate"`
// 	ClientPhone string    `json:"client_phone"`
// 	OwnerUUID   string    `json:"owner_uuid"`
// 	FeatureUUID []string  `json:"feature_uuid"`
// 	ServiceUUID string    `json:"service_uuid"`
// }OrderClient
const (
	getTariffEndpoint             = "/orders/tariff"
	driverLastLocationEndpoint    = "/drivers/getlocations"
	getOrderCurrentStateEndpoint  = "/orders/currentstate/"
	getCurrentOrdersDataEndpoint  = "/orders/current/client/"
	getCurrentOrderTariffEndpoint = "/ordertariff/"
	orderCreationInCRMEndpoint    = "/orders/from/client"
	getProductDataEndpoint        = "/products/fororder"
)

type ClientIncome struct {
	tableName  struct{}  `sql:"client_income"`
	Id         int       `json:"-" sql:"id"`
	ClientUUID string    `json:"client_uuid"`
	Count      int       `json:"count"`
	TotalCount int       `json:"total_count"`
	Created    time.Time `json:"created"`
}

func GetClientIncomeByPeriod(uuid string, min, max time.Time) (*[]ClientIncome, error) {
	clientIncome := new([]ClientIncome)
	err := db.Model(clientIncome).
		Where("client_uuid = ?", uuid).
		Where("created >= ? AND created <= ?", min, max).
		Select()
	return clientIncome, err
}

func GetClientIncomeLastRecord(uuid string) (ClientIncome, error) {
	clientIncome := new(ClientIncome)
	err := db.Model(clientIncome).
		Where("client_uuid = ?", uuid).
		Last()
	return *clientIncome, err
}

func SaveClientIncomeRecords(rows *[]ClientIncome) error {
	_, err := db.Model(rows).Insert()
	return err
}

// OrderCApp main orders CRM struct
type OrderCApp struct {
	tableName struct{} `sql:"client_orders" pg:",discard_unknown_columns"`
	structures.Order
	ID                 int      `json:"id" sql:",pk"`
	ClUUID             string   `json:"client_uuid"`
	SrUUID             string   `json:"service_uuid"`
	CallbackPhone      string   `json:"callback_phone"`
	FeaturesUUIDS      []string `json:"features_uuids"`
	StartUserUUID      string   `json:"-"`
	SourceOfOrdersUUID string   `json:"source_uuid" sql:"-"` //
	// LastUserUUID    string            `json:"-"`
	// FinishUserUUID  string            `json:"-"`
	AppointmentTime             time.Time `json:"-" description:"Время назначения авто на заказ"`
	CompleteTime                time.Time `json:"-" description:"Врем завершения заказа"`
	PickupTime                  time.Time `json:"-" description:"Прибытие авто"`
	LastIncreasedFareUpdateTime time.Time `json:"-"`
	// OwUUID          string            `json:"owner_uuid"`
	CreatedAt     time.Time `sql:"default:now()" json:"created_at" description:"Дата создания"`
	CreatedAtUnix int64     `sql:"-" json:"created_at_unix"`
	UpdatedAt     time.Time `json:"updated_at" `
	Deleted       bool      `json:"-" sql:"default:false"`
	Visibility    bool      `json:"visibility"`
	OrderState    string    `json:"state"`
	StateTitle    string    `json:"state_title" sql:"-"`
	// OrderStateUUID  string    `json:"order_state_uuid" `
}

//OrdersForInit - кроме заказа содержит его актуальный статус
type OrdersForInit struct {
	OrderCApp
	// State             string                  `json:"state"`
	FreeTime          int                     `json:"free_time,omitempty"`
	DriverArrivalTime int64                   `json:"driver_arrival_time,omitempty" sql:"-"`
	DriverLocation    *structures.Coordinates `json:"driver_location,omitempty"`
}

// OrderStateCApp - структура для хранения статуса
type OrderStateCApp struct {
	tableName struct{}  `sql:"client_orderstates"`
	ID        int       `sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"created_at"`
	structures.OfferStates
}

//SaveToDB сохраняет статус заказа в базу
func (orST OrderStateCApp) SaveToDB() (OrderStateCApp, error) {
	orST.CreatedAt = time.Now()
	_, err := db.Model(&orST).Returning("*").Insert()
	return orST, err
}

// Create - creating order
func (or OrderCApp) Create() (OrderCApp, error) {

	//TODO брать сервисы из црм,удалить таблицу
	//
	var service ServiceCApp
	err := db.Model(&service).Where("deleted is not true AND uuid = ?", or.SrUUID).Select()
	if err != nil {
		return or, fmt.Errorf("нет такого сервиса [%s]", err)
	}
	or.Service = service.Service
	err = or.validateCreation()
	if err != nil {
		return OrderCApp{}, err
	}
	uu, err := uuid.NewV4()
	if err != nil {
		return OrderCApp{}, err
	}
	uus := uu.String()

	if or.PaymentType == "" {
		var client ClientsApps
		err := db.Model(&client).
			Where("deleted is not true AND uuid = ?", or.UUID).
			Column("default_payment_type").
			Select()
		if err != nil {
			return or, errpath.Err(err)
		}
		or.PaymentType = client.DefaultPaymentType
	}

	// назначается время на принятие заказа
	dur, _ := time.ParseDuration(config.St.Application.DistributingTime)
	or.CancelTime = time.Now().Add(dur)
	fmt.Printf("\n\n DUR=%s. time=%s", dur, or.CancelTime)

	// сохраняем в БД
	nOrder, err := or.saveNewOrder(uus, or.UUID)
	if err != nil {
		return OrderCApp{}, err
	}
	// запускается горутина в которой отправится пуш о необходимости повысить цену заказа
	go delaySendingIncFareMessage(or.UUID, nOrder.UUID, or.Service.ProductDelivery)
	// возвращается заказ из базы
	return nOrder, nil
}

//Update обновляет заказ по uuid
func (or *OrderCApp) Update(uuid string) error {
	_, err := db.Model(or).
		Where("uuid = ?", uuid).
		Returning("*").
		UpdateNotNull()
	return err
}

func GetCurrentOrderState(orderUUID string) (string, error) {
	var result string
	err := request(http.MethodGet, config.St.CRM.BaseURL+getOrderCurrentStateEndpoint+orderUUID, nil, &result, nil)
	if err != nil {
		return result, err
	}
	return result, err
}
func GetOrderWithCurrentState(orderUUID string) (OrderCApp, error) {
	var order OrderCApp
	err := GetByUUID(orderUUID, &order)
	if err != nil {
		return order, fmt.Errorf("error finding orders, %s", err)
	}
	order.OrderState, err = GetCurrentOrderState(orderUUID)
	if err != nil {
		return order, fmt.Errorf("ошибка поиска статуса заказа, %s", err)
	}
	return order, nil
}
func (or *OrderCApp) validateRoutes(clientUUID string) error {
	var oldOrder OrderCApp
	err := GetByUUID(or.UUID, &oldOrder)
	if err != nil {
		return fmt.Errorf("ошибка поиска заказа, %s", err)
	}
	currentOrState, err := GetCurrentOrderState(or.UUID)
	if err != nil {
		return fmt.Errorf("ошибка поиска статуса заказа, %s", err)
	}
	if currentOrState == constants.OrderStatePayment ||
		currentOrState == constants.OrderStateFinished {
		return fmt.Errorf(`нельзя обновлять заказ в статусе "%s"`, constants.OrderStateRU[currentOrState])
	}
	if oldOrder.ClUUID != clientUUID {
		return fmt.Errorf("вы не можете обновлять этот заказ")
	}
	if len(or.Routes) <= 1 {
		return fmt.Errorf("необходимы 2 или более точек назначения")
	}
	if or.Routes[0] != oldOrder.Routes[0] {
		return fmt.Errorf("нельзя менять первую точку назначения")
	}
	return nil
}
func (or OrderCApp) UpdateRoutes(clientUUID string) (OrderCApp, error) {
	err := or.validateRoutes(clientUUID)
	if err != nil {
		return or, err
	}
	var oldOrder OrderCApp
	err = GetByUUID(or.UUID, &oldOrder)
	if err != nil {
		return or, fmt.Errorf("error getting order by uuid,%s", err)
	}
	// Забиваем старые данные
	newRoutes := or.Routes
	or = oldOrder
	or.Routes = newRoutes

	err = or.fillTariffAndRouteWayData()
	if err != nil {
		return or, fmt.Errorf("ошибка генерации нового тарифа,%s", err)
	}
	_, err = db.Model(&or).
		Where("uuid = ?", or.UUID).
		Set("routes = ?routes").
		Set("tariff = ?tariff").
		Set("route_way_data = ?route_way_data").
		Returning("*").
		Update()
	return or, err
}

// UpdateOrderIncreasedFare returns the order and differences between the new and old increased tariff
func UpdateOrderIncreasedFare(orderUUID, clientUUID string, newFare float32) (OrderCApp, float32, *structures.ErrorWithLevel) {

	order, err := GetOrderWithCurrentState(orderUUID)
	if err != nil {
		return order, 0, structures.Error(err)
	}
	fareDiff := newFare - order.IncreasedFare
	err = order.validateNewIncreasedFare(clientUUID)
	if err != nil {
		return order, 0, structures.Warning(err)
	}
	if order.IncreasedFare == newFare {
		return order, fareDiff, nil
	}
	order.IncreasedFare = newFare
	err = order.fillTariffAndRouteWayData()
	if err != nil {
		return order, 0, structures.Error(fmt.Errorf("error fill tariff"))
	}
	_, err = db.Model(&order).Where("uuid = ?uuid").
		Set("tariff = ?tariff").
		Set("increased_fare = ?increased_fare").
		Set("last_increased_fare_update_time = now()").
		Returning("*").Update()
	return order, fareDiff, structures.Error(err)
}

func (or *OrderCApp) validateNewIncreasedFare(clientUUID string) error {
	if or.ClUUID != clientUUID {
		return fmt.Errorf("order does not belong to this customer")
	}
	previousUpdateTime := time.Now().Unix() - or.LastIncreasedFareUpdateTime.Unix()
	if previousUpdateTime < config.St.Application.IncreasedFareUpdatePeriodSec {
		return fmt.Errorf("Следующее изменение стоимости будет доступно через %v секунд", config.St.Application.IncreasedFareUpdatePeriodSec-previousUpdateTime)
	}
	if or.OrderState == constants.OrderStateOffered {
		return fmt.Errorf("Мы предлагаем ваш заказ водителю, поэтому в данный момент изменить его стоимость нельзя")
	}

	if !constants.InDistributingStates(or.OrderState) {
		return fmt.Errorf("Этому заказу уже нельзя менять стоимость")
	}

	return nil
}

//UpdateFromBroker обновляет заказ по uuid если он существует
func (or *OrderCApp) UpdateFromBroker(uuid string, needSendInitData bool) error {
	exist, err := db.Model(or).Where("uuid = ?", uuid).Exists()
	if err != nil {
		return fmt.Errorf("error checking existing order,%s", err)
	}
	if !exist {
		return nil
	}
	err = or.Update(uuid)
	if err != nil {
		return fmt.Errorf("error update order, %s", err)
	}
	if !needSendInitData {
		return nil
	}
	title := "Данные вашего заказа были обновлены"
	body := "Проверьте новые данные на предмет корректности"
	err = SendInitDataMessageToClient(or.ClUUID, title, body)
	if err != nil {
		return fmt.Errorf("error send init data message,%s", err)
	}
	return nil
}

// UpdateOrderStateFromBroker - обновляет статус заказа по uuid если он существует
func (or *OrderCApp) UpdateOrderStateFromBroker(uuid string, state structures.OfferStates) error {
	exist, err := db.Model(or).Where("uuid = ?", uuid).Exists()
	if err != nil {
		return fmt.Errorf("error checking existing order,%s", err)
	}
	if !exist {
		return nil
	}
	_, err = db.Model(or).
		Where("uuid = ?", uuid).
		Set("order_state = ?", state.State).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}

func UpdateOrderTariffIfNeed(orderUUID, newOrderState string) error {
	// водительское может одновременно прислать эти статусы. Поэтому пусть тариф обновляется при каждом из них
	if newOrderState != constants.OrderStatePayment &&
		newOrderState != constants.OrderStateFinished &&
		newOrderState != constants.OrderStateOnTheWay {
		return nil
	}

	currentTariff, err := getOrderTariffFromCRM(orderUUID)
	if err != nil {
		return err
	}
	return UpdateOrderTariff(orderUUID, currentTariff)
}
func getOrderTariffFromCRM(orderUUID string) (structures.Tariff, error) {
	var result structures.Tariff
	neededURL := config.St.CRM.BaseURL + getCurrentOrderTariffEndpoint + orderUUID
	err := request(http.MethodGet, neededURL, nil, &result, nil)
	return result, err
}
func UpdateOrderTariff(orderUUID string, tariff structures.Tariff) error {
	// if tariff.TotalPrice <= 0 {
	// 	return fmt.Errorf("zero tariff total price")
	// }
	var order OrderCApp
	_, err := db.Model(&order).
		Where("uuid = ?", orderUUID).
		Set("tariff = ?", tariff).
		Returning("*").
		Update()
	if err != nil {
		return err
	}
	if !constants.InActiveStates(order.OrderState) {
		return nil
	}

	//без оповещения
	err = SendInitDataMessageToClient(
		order.Client.UUID,
		"",
		"",
	)
	if err != nil {
		return fmt.Errorf("error send init message to client,%s", err)
	}
	return nil
}
func UpdateOrderProductData(orderUUID string, prodData structures.ProductsDataInOrder, sendInitData bool) error {
	var order OrderCApp
	_, err := db.Model(&order).
		Where("uuid = ?", orderUUID).
		Set("products_data = ?", prodData).
		Returning("*").
		Update()
	if err != nil || !sendInitData {
		return err
	}
	title := "Номер вашего заказа - " + prodData.OrderNumberInStore
	body := ""
	err = SendInitDataMessageToClient(order.ClUUID, title, body)
	if err != nil {
		return fmt.Errorf("error send init data message,%s", err)
	}
	return err
}

//OrdersByClientUUID возвращает все заказы клиента
//func OrdersByClientUUID(clientuuid string, pager urlvalues.Pager) ([]OrderCApp, error) {
//	var (
//		orders []OrderCApp
//	)
//	err := db.Model(&orders).
//		Where("cl_uuid = ?", clientuuid).
//		Where("deleted is not true", clientuuid).
//		Order("created_at desc").
//		Apply(pager.Pagination).
//		Select()
//	return orders, err
//}

// OrdersByClientUUIDWithoutCanceled - возвращает все заказы клиента кроме отмененных
func OrdersByClientUUIDWithoutCanceled(clientuuid string, pager urlvalues.Pager) ([]OrderCApp, error) {
	var (
		orders []OrderCApp
	)
	err := db.Model(&orders).
		Where("cl_uuid = ?", clientuuid).
		Where("deleted is not true", clientuuid).
		Where("order_state <> ?", constants.OrderStateCancelled).
		Order("created_at desc").
		Apply(pager.Pagination).
		Select()
	return orders, err
}

// Сохранение заказа в бд
func (or *OrderCApp) saveNewOrder(uuid string, userUUID string) (OrderCApp, error) {
	var (
		orderState OrderStateCApp
		err        error
		nOrder     OrderCApp
		isOptional = true
	)

	// nOrder.Features = or.Features
	nOrder.UUID = uuid
	nOrder.Comment = or.Comment
	nOrder.Routes = or.Routes
	nOrder.FeaturesUUIDS = or.FeaturesUUIDS
	nOrder.SrUUID = or.SrUUID
	nOrder.ClUUID = userUUID
	nOrder.Tariff = or.Tariff
	nOrder.Service = or.Service
	nOrder.IncreasedFare = or.IncreasedFare
	if or.IncreasedFare < 0 {
		nOrder.IsOptional = &isOptional
	}
	nOrder.CallbackPhone = or.CallbackPhone
	nOrder.WithoutDelivery = or.WithoutDelivery
	nOrder.OwnDelivery = or.OwnDelivery
	nOrder.Client = or.Client
	nOrder.ProductsData = or.ProductsData
	nOrder.ProductsInput = or.ProductsInput
	nOrder.CancelTime = or.CancelTime
	nOrder.OrderState = constants.OrderStateCreated
	// nOrder.fillingСoordinates()
	nOrder.Source = or.Source
	nOrder.PaymentType = or.PaymentType
	nOrder.SourceOfOrders = or.SourceOfOrders
	nOrder.SourceOfOrdersUUID = or.SourceOfOrdersUUID

	features, _ := FeatureList()
	for _, fuuid := range or.FeaturesUUIDS {
		for _, inst := range features {
			if fuuid == inst.UUID {
				nOrder.Features = append(nOrder.Features, inst.Feature)
			}
		}
	}
	user, err := GetUserByUUID(userUUID)
	nOrder.Client = user.Client

	if isFuelOrder(or.Service.Tag) {
		err = nOrder.fillTariffWithFuel()
		if err != nil {
			return nOrder, err
		}
	} else {
		err = nOrder.fillTariffAndRouteWayData()
		if err != nil {
			return nOrder, err
		}
	}

	_, err = db.Model(&nOrder).Returning("*").Insert()
	if err != nil {
		return OrderCApp{}, err
	}
	orderState.State = constants.OrderStateCreated
	orderState.OrderUUID = nOrder.UUID
	orderState.Comment = "init state"
	_, err = orderState.SaveToDB()
	if err != nil {
		return OrderCApp{}, fmt.Errorf("error save init offer state,%s", err)
	}
	return nOrder, nil
}

func (or *OrderCApp) validateCreation() error {
	routsLen := len(or.Routes)
	orderCanHaveOneRoute := or.Service.ProductDelivery && or.WithoutDelivery
	if ((!orderCanHaveOneRoute && routsLen < 2) || or.Routes[routsLen-1].UnrestrictedValue == "") && !or.WithoutDelivery && !isFuelOrder(or.Service.Tag) {
		return fmt.Errorf("error getting price. Less then 2 routes or last route is empty")
	}

	if or.SrUUID == "" {
		return fmt.Errorf("ServiceUUID is required field")
	}
	if !or.Service.ProductDelivery || len(or.ProductsInput) == 0 {
		or.Service.ProductDelivery = false
		return nil
	}
	or.Service.ProductDelivery = true
	err := or.fillProductData()
	if err != nil {
		return err
	}
	if or.ProductsData == nil || len(or.ProductsData.Pruducts) == 0 {
		return fmt.Errorf("empty products data. something went wrong")
	}
	workScheduleData, err := or.ProductsData.StoreData.GetWorkScheduleForDay(int(time.Now().Weekday()))
	if err != nil {
		return err
	}
	if workScheduleData.DayOff {
		return fmt.Errorf("У заведения сегодня выходной")
	}
	store := or.Order.GetProductsData().StoreData
	if !store.IsAvailable() {
		return fmt.Errorf("Заведение не обслуживает клиентов")
	}
	for _, prod := range or.Order.GetProductsData().Pruducts {
		if !prod.IsAvailable() {
			return fmt.Errorf("Товар '%s' сейчас недоступен", prod.Name)
		}
	}
	actualMinuteNumber := getMinuteInDay()
	if (actualMinuteNumber < workScheduleData.WorkBeginning ||
		actualMinuteNumber > workScheduleData.WorkEnding) &&
		!workScheduleData.IsWorkRoundTheClock() {
		return fmt.Errorf("Заведение не работает")
	}
	return nil
}

func isFuelOrder(serviceTags []string) bool {
	for _, el := range serviceTags {
		if el == "fuel" {
			return true
		}
	}
	return false
}

func getMinuteInDay() int {
	return CurrentTime().Minute() + (60 * CurrentTime().Hour())
}

// SetDeleted godoc
func (or *OrderCApp) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var frient OrderCApp
	err := CheckExistsUUID(&frient, or.UUID)
	if err != nil {
		return err
	}
	or.Deleted = true
	_, err = db.Model(or).Where("uuid=?uuid").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}
func GetClientActualOrdersData(clUUID string) ([]structures.OrderWithDataForClient, error) {
	var result []structures.OrderWithDataForClient
	url := config.St.CRM.BaseURL + getCurrentOrdersDataEndpoint + clUUID
	err := request(http.MethodGet, url, nil, &result, nil)
	return result, err
}

func GetProductData(inData []structures.ProductInputData) (*structures.ProductsDataInOrder, error) {
	var result structures.ProductsDataInOrder
	url := config.St.CRM.BaseURL + getProductDataEndpoint
	err := request(http.MethodPost, url, inData, &result, nil)
	if err != nil {
		return nil, err
	}
	return &result, err
}

// GetCurrentClientOrders возвращает текущие заказы клиента
func GetCurrentClientOrders(clientUUID string) ([]OrdersForInit, error) {
	var (
		orderUUIDs   []string
		orders       []OrderCApp
		actualOrders []OrdersForInit
		actualOrder  OrdersForInit
	)
	// excludedStates := [3]string{
	// 	variables.OrderStateFinished,
	// 	variables.OrderStateCancelled,
	// 	variables.OrderStateDriverNotFound,
	// }

	ordersData, err := GetClientActualOrdersData(clientUUID)
	if err != nil {
		return nil, fmt.Errorf("Error getting actual orders, %s", err)
	}
	if len(ordersData) == 0 {
		return nil, nil
	}
	for _, order := range ordersData {
		orderUUIDs = append(orderUUIDs, order.UUID)
	}
	err = db.Model(&orders).
		WhereIn("uuid in (?)", orderUUIDs).
		Order("created_at DESC").
		Limit(10).
		Select()
	if err != nil {
		return nil, fmt.Errorf("Error finding order, %s", err)
	}
	// orderStatesDis := db.Model((*OrderStateCApp)(nil)).
	// 	ColumnExpr("max(created_at) created_at, (order_uuid)").
	// 	Where("order_uuid in (?)", pg.In(orderUUIDs)).
	// 	Group("order_uuid")

	// err = db.Model().
	// 	TableExpr("(?) q", orderStatesDis).
	// 	Column("os.*").
	// 	Join("join client_orderstates os on os.order_uuid = q.order_uuid and os.created_at = q.created_at").
	// 	Where("os.state not in (?)", pg.In(excludedStates)).
	// 	Select(&orStates)
	// if err != nil {
	// 	return nil, fmt.Errorf("Error finding order state, %s", err)
	// }
	// if len(orStates) == 0 {
	// 	return nil, nil
	// }

	// ordForArrivalTime, err := selectOrdersForArrivalTime(orStates)
	// if err != nil {
	// 	fmt.Printf("Error selectOrdersForArrivalTime [%s]", err)
	// }

	var driversUUID []string
	for _, data := range ordersData {
		if data.State == constants.OrderStateStarted || data.State == constants.OrderStateOnPlace {
			driversUUID = append(driversUUID, data.DriverUUID)
		}
	}

	driverLocations, err := getDriversLocations(driversUUID)
	if err != nil {
		logrus.Errorf("не удалось получить локации водителей [%s]", err)
	}

	for _, orData := range ordersData {
		for _, order := range orders {
			if order.UUID == orData.UUID {
				actualOrder.OrderCApp = order
				if orData.State == constants.OrderStateOnPlace || orData.State == constants.OrderStateWaiting {
					// seconden := int(time.Now().Unix() - orderstate.StartState.Unix())
					// freeBuff, err := strconv.Atoi(orderstate.Comment)
					// if err != nil {
					// 	return nil, fmt.Errorf("Error convert string to int, [%s]", err)
					// }
					actualOrder.FreeTime = int(orData.RemainingWaitingTime)
				}
				if orData.State == constants.OrderStateOnTheWay {
					actualOrder.DriverArrivalTime = orData.DriverArrivalTime
				}
				actualOrder.OrderState = constants.ReplaceStateForClientIfNeed(orData.State, order.Service.ProductDelivery)
				actualOrder.StateTitle = constants.TranslateOrderStateForClient(actualOrder.OrderState, order.Service.ProductDelivery)
				// for _, item := range ordForArrivalTime {
				// 	if item.OrderUUID == order.UUID {
				// 		intArrival, err := strconv.ParseInt(item.Comment, 10, 64)
				// 		if err != nil {
				// 			fmt.Printf("Ошибка парсинга комента (%s) для driver_arrival_time", item.Comment)
				// 		}

				// 		break
				// 	}
				// }
				for _, item := range driverLocations {
					if item.DriverUUID == order.Driver.UUID {
						item.DriverUUID = "" // зануляю дабы не выводить в json
						actualOrder.DriverLocation = &item
						break
					}
				}
				actualOrder.CreatedAtUnix = actualOrder.CreatedAt.Unix()
				actualOrders = append(actualOrders, actualOrder)
			}
		}
	}
	sort.SliceStable(actualOrders, func(i, j int) bool {
		return actualOrders[i].CreatedAt.Unix() < actualOrders[j].CreatedAt.Unix()
	})
	return actualOrders, nil
}

//CancelOrder отменяет заказ
func CancelOrder(reason string, uuid string) (OrderStateCApp, error) {
	var (
		orderState OrderStateCApp
		check      bool
	)
	currState, err := GetCurrentOrderState(uuid)
	if err != nil {
		return orderState, err
	}
	for _, state := range constants.ListActiveOrderStates() {
		if state == currState {
			check = true
			break
		}
	}
	if !check {
		return OrderStateCApp{}, fmt.Errorf("Вы не можете отменить неактивный заказ")
	}
	orderState.State = constants.OrderStateCancelled
	for _, val := range constants.ListClientCancelReasons() {
		if reason == val {
			check = true
			break
		}
	}
	if !check {
		return OrderStateCApp{}, fmt.Errorf("invalid cancel reason")
	}
	orderState.Comment = reason
	orderState.OrderUUID = uuid
	err = orderState.NewOrderState()
	if err != nil {
		return OrderStateCApp{}, err

	}
	return orderState, nil
}
func GetDriverLastCoordinates(driverUUID string) (structures.DriverCoordinates, error) {
	var result []structures.DriverCoordinates
	url := config.St.CRM.BaseURL + driverLastLocationEndpoint
	err := request(http.MethodPost, url, []string{driverUUID}, &result, nil)
	if err != nil {
		return structures.DriverCoordinates{}, err
	}
	if len(result) == 0 {
		return structures.DriverCoordinates{}, fmt.Errorf("empty result")
	}
	return result[0], nil
}

func SendOrderToCRM(order OrderCApp) error {
	url := config.St.CRM.BaseURL + orderCreationInCRMEndpoint
	err := request(http.MethodPost, url, &order, nil, nil)
	return err
}

func UpdateOrderArrivalTime(orderUUID string) (int64, error) {
	var order OrderCApp
	var routes []structures.Route
	err := GetByUUID(orderUUID, &order)
	if err != nil {
		return -1, err
	}
	lat, lon := order.Routes[0].Lat, order.Routes[0].Lon
	if order.Driver.UUID != "" {
		coords, err := GetDriverLastCoordinates(order.Driver.UUID)
		if err != nil {
			return -1, err
		}
		lat, lon = float32(coords.Lat), float32(coords.Long)
	}

	routes = append(routes, structures.Route{Lat: lat, Lon: lon})
	routes = append(routes, order.Routes[len(order.Routes)-1])
	// вычисляем данные osrm от точки, в которой сейчас находится водитель до конечной точки заказа
	routeWayData, err := GetRouteWayData(routes)
	if err != nil {
		return -1, err
	}
	arrTime := time.Now().Unix() + int64(float32(routeWayData.Geometry.Proper.Duration)*structures.OrderDurationCoeff)
	_, err = db.Model(&order).Set("arrival_time = ?", arrTime).
		Where("uuid = ?", orderUUID).
		Update()
	return arrTime, err
}

func CheckOrderExists(orderUUID string) (OrderCApp, bool, error) {
	var res OrderCApp
	check := true
	err := db.Model(&res).
		Where("uuid = ?", orderUUID).
		Select()
	if err == pg.ErrNoRows {
		check = false
		err = nil
	}
	return res, check, err
}
func (or *OrderCApp) fillTariffAndRouteWayData() error {
	var tariff structures.Tariff
	or.CreatedAtUnix = or.CreatedAt.Unix()
	err := request(http.MethodPost, config.St.CRM.BaseURL+getTariffEndpoint, or, &tariff, nil)
	if err != nil {
		return err
	}
	or.Tariff = tariff
	or.RouteWayData, err = GetRouteWayData(or.Routes)
	if err != nil {
		return fmt.Errorf("error getting route way data,%s", err)
	}
	return nil
}

func (or *OrderCApp) fillTariffWithFuel() error {

	or.Tariff.Name = "Fuel"
	or.Tariff.TariffCalcType = structures.CalcWithDist

	return nil
}
func (or *OrderCApp) fillProductData() (err error) {
	if !or.Service.ProductDelivery {
		return
	}
	or.ProductsData, err = GetProductData(or.ProductsInput)
	return
}

func request(method, url string, payload, response interface{}, headers map[string]string) error {
	r := &http.Client{
		Timeout: 15 * time.Second,
	}
	body, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "failed to marshal a payload")
	}
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return errors.Wrap(err, "failed to create an http request")
	}

	req.Header.Set("Content-Type", "application/json")
	for key, val := range headers {
		req.Header.Set(key, val)
	}

	resp, err := r.Do(req)
	if err != nil {
		return errors.Wrap(err, "failed to make a post request")
	}
	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			return errors.Errorf("post request to %s failed with status: %d", url, resp.StatusCode)
		}
		return errors.Errorf("post request to %s failed with status: %d and body: %s", url, resp.StatusCode, string(body))
	}

	if response != nil {
		return json.NewDecoder(resp.Body).Decode(response)
	}
	return nil
}

func UpdateOrderRating(order *OrderCApp) error {
	_, err := db.Model(order).
		Set("driver_rating = ?driver_rating").
		Set("client_rating = ?client_rating").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}
	return nil
}

//
func UpdateOrderRouteToClient(order *OrderCApp) error {
	_, err := db.Model(order).
		Set("route_way_data = ?route_way_data").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return err
	}
	return nil
}

func SetOrderClientLocVisibility(orderUUID string, clientUUID string, value bool) error {
	check, err := db.Model(&OrderCApp{}).
		Where("uuid = ?", orderUUID).
		Where("cl_uuid = ?", clientUUID).
		Exists()
	if err != nil {
		return fmt.Errorf("checking order exists error,%s", err)
	}
	if !check {
		return errors.New("заказ не принадлежит этому пользователю")
	}
	currentState, err := GetCurrentOrderState(orderUUID)
	if err != nil {
		return fmt.Errorf("checking order state error,%s", err)
	}
	check = false
	for _, activeState := range constants.ListActiveOrderStates() {
		if currentState == activeState {
			check = true
			break
		}
	}
	if !check {
		return fmt.Errorf(`нельзя отправлять локации для заказа в статусе "%s"`, constants.OrderStateRU[currentState])
	}
	_, err = db.Model(&OrderCApp{}).
		Where("uuid = ?", orderUUID).
		Set("visibility = ?", value).
		Update()

	return err
}

// selectOrdersForArrivalTime - выбирает все заказы у которых текущий статус "order_start"
// и возвращат их со статусом "offer_accepted" для взятия из comments значения для arrivalTime
func selectOrdersForArrivalTime(orStates []OrderStateCApp) ([]OrderStateCApp, error) {
	if len(orStates) < 1 {
		return nil, nil
	}

	var orderUUIDForArrival []string
	var statesForArrival []OrderStateCApp

	for _, orderstate := range orStates {
		if orderstate.State == constants.OrderStateStarted {
			orderUUIDForArrival = append(orderUUIDForArrival, orderstate.OrderUUID)
		}
	}
	if len(orderUUIDForArrival) == 0 {
		return statesForArrival, nil
	}
	err := db.Model(&statesForArrival).
		Where("order_uuid in (?)", pg.In(orderUUIDForArrival)).
		Where("state = ?", constants.OrderStateAccepted).
		Select()
	if err != nil {
		return nil, fmt.Errorf("Error finding order state [%s]", err)
	}

	if len(statesForArrival) == 0 {
		return nil, fmt.Errorf("return array(statesForArrival) is empty")
	}
	return statesForArrival, nil
}

// getDriversLocations - делает запрос в crm (/drivers/getlocations)
// для пролучения координат водителей
func getDriversLocations(driversUUID []string) ([]structures.Coordinates, error) {
	client := &http.Client{}
	if len(driversUUID) == 0 {
		return nil, nil
	}
	body, err := json.Marshal(driversUUID)
	if err != nil {
		return nil, fmt.Errorf("ошибка маршалинга [%s]", err)
	}

	endPoint := "/drivers/getlocations"
	req, err := http.NewRequest(
		"POST",
		config.St.CRM.BaseURL+endPoint,
		bytes.NewReader([]byte(body)),
	)
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-Type", "application/json")
	// req.Header.Add("Authorization", "Bearer "+token)

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	var drvLocs []structures.Coordinates
	err = json.NewDecoder(resp.Body).Decode(&drvLocs)
	if err != nil {
		return nil, fmt.Errorf("ошибка декодирования [%s]", err)
	}

	return drvLocs, nil
}

func IsOrderCounter(orderUUID string) (bool, error) {
	var marker bool
	err := tool.SendRequest(
		http.MethodGet,
		fmt.Sprintf("%s/orders/counterordermarker/%s", config.St.CRM.BaseURL, orderUUID),
		nil, nil, &marker,
	)

	return marker, err
}
