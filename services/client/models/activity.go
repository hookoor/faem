package models

import (
	"fmt"
	"time"

	"github.com/gofrs/uuid"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/services/client/rabsender"
)

// TODO: move to the remote config: db/consul/redis/etc.
const (
	initialClientActivity       = 10
	clientActivityOrderFinished = 1

	vipRequiredOrderCount    = 30
	vipRequiredOrderInterval = time.Hour * 24 * 30
)

func GetInitialClientActivity() (int, error) {
	return initialClientActivity, nil
}

func GetClientActivityForFinishedOrder() (int, error) {
	return clientActivityOrderFinished, nil
}

func GetClientOrderCountForVIP() (int, error) {
	return vipRequiredOrderCount, nil
}

func GetClientOrderIntervalForVIP() (time.Duration, error) {
	return vipRequiredOrderInterval, nil
}

type ClientActivityItem struct {
	ID             string `json:"id"`
	ClientUUID     string `json:"client_uuid"`
	OrderUUID      string `json:"order_uuid"`
	Event          string `json:"event"`
	ActivityChange int    `json:"activity"`
}

type CrmActivityHistoryItem struct {
	UserUUID       string `json:"user_uuid"`
	OrderUUID      string `json:"order_uuid"`
	Event          string `json:"event"`
	ActivityChange int    `json:"activity_change"`
	Activity       int    `json:"activity"`
}

func ChangeClientsActivityIfNeeded(orderState structures.OfferStates) error {
	switch orderState.State {
	case constants.OrderStateAccepted:
		clientUUID, err := clientUUIDFromOrderUUID(orderState.OrderUUID)
		if err != nil {
			return err
		}

		// Clear client promotion after the offer been accepted by a driver
		err = ClearClientPromotionBooster(clientUUID)
		return errors.Wrap(err, "failed to clear client's promotion")
	case constants.OrderStateFinished:
		clientUUID, err := clientUUIDFromOrderUUID(orderState.OrderUUID)
		if err != nil {
			return err
		}

		// Check if need to set VIP status for the client
		vipRequiredCount, err := GetClientOrderCountForVIP()
		if err != nil {
			return errors.Wrap(err, "failed to get required order count for VIP")
		}
		vipRequiredInterval, err := GetClientOrderIntervalForVIP()
		if err != nil {
			return errors.Wrap(err, "failed to get interval to detect VIP status")
		}
		successfulOrderCount, err := GetClientSuccessfulOrdersForPeriod(clientUUID, vipRequiredInterval)
		if err != nil {
			return errors.Wrap(err, "failed to get client successful orders for a period")
		}
		isVip := successfulOrderCount >= vipRequiredCount
		if err = SetClientPromotionVIP(clientUUID, isVip); err != nil {
			return errors.Wrap(err, "failed to set client's VIP status")
		}

		// Finally add the activity
		activity, err := GetClientActivityForFinishedOrder()
		if err != nil {
			return errors.Wrap(err, "failed to get driver activity for a finished order")
		}
		_, err = AddActivityForClient(activityItemFromOrderState(clientUUID, orderState, activity, "order finished"))
		return err
	case constants.OrderStateCancelled:
		// If an order was cancelled by the fault of a driver, reward the client
		if orderState.Comment != constants.CancelReasonDriver {
			return nil
		}

		clientUUID, err := clientUUIDFromOrderUUID(orderState.OrderUUID)
		if err != nil {
			return err
		}
		return PromoteClientBooster(clientUUID)
	}
	return nil
}

func AddActivityForClient(activity ClientActivityItem) (int, error) {
	var newActivity int
	_, err := db.Model((*ClientsApps)(nil)).
		Where("uuid = ?", activity.ClientUUID).
		Set("activity = GREATEST(activity + ?, 0)", activity.ActivityChange). // negative is not allowed
		Returning("activity").
		Update(&newActivity)
	if err != nil {
		return 0, errors.Wrapf(
			err, "failed to add activity %d for a client with uuid %s", activity.ActivityChange, activity.ClientUUID,
		)
	}

	// Notify listeners about client's activity changing
	go func() {
		if err := rabsender.SendJSONByAction(
			rabsender.AcClientActivityChanged, crmActivityHistoryItem(activity, newActivity),
		); err != nil {
			logs.Eloger.WithFields(logrus.Fields{
				"event":      "notifying about client activity changing",
				"clientUUID": activity.ClientUUID,
				"orderUUID":  activity.OrderUUID,
			}).Error(err)
		}
	}()

	return newActivity, nil
}

func clientUUIDFromOrderUUID(orderUUID string) (string, error) {
	var order OrderCApp
	if err := GetByUUID(orderUUID, &order); err != nil {
		return "", errors.Wrap(err, "failed to get an order by UUID from the db")
	}
	if order.ClUUID == "" {
		return "", errors.Errorf("empty client UUID associated with an order with UUID %s", orderUUID)
	}
	return order.ClUUID, nil
}

func activityItemFromOrderState(
	clientUUID string, orderState structures.OfferStates, amount int, event string,
) ClientActivityItem {
	return ClientActivityItem{
		ID:             uuid.Must(uuid.NewV4()).String(),
		ClientUUID:     clientUUID,
		OrderUUID:      orderState.OrderUUID,
		Event:          event,
		ActivityChange: amount,
	}
}

func crmActivityHistoryItem(item ClientActivityItem, newActivity int) CrmActivityHistoryItem {
	return CrmActivityHistoryItem{
		UserUUID:       item.ClientUUID,
		OrderUUID:      item.OrderUUID,
		Event:          item.Event,
		ActivityChange: item.ActivityChange,
		Activity:       newActivity,
	}
}

func PromoteClientBooster(clientUUID string) error {
	return setClientPromotionBooster(clientUUID, "true")
}

func ClearClientPromotionBooster(clientUUID string) error {
	return setClientPromotionBooster(clientUUID, "false")
}

func setClientPromotionBooster(clientUUID, booster string) error {
	_, err := db.Model((*ClientsApps)(nil)).
		Where("uuid = ?", clientUUID).
		Set(fmt.Sprintf("promotion = jsonb_set(COALESCE(promotion, '{}'), '{booster}', '%s')", booster)).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to change client's promotion")
	}
	return nil
}

func GetClientSuccessfulOrdersForPeriod(clientUUID string, period time.Duration) (int, error) {
	count, err := db.Model((*OrderCApp)(nil)).
		Join("INNER JOIN client_orderstates AS s ON order_c_app.uuid = s.order_uuid AND s.state = ?", constants.OrderStateFinished).
		Where("order_c_app.created_at > ?", time.Now().Add(-period)).
		Where("order_c_app.cl_uuid = ?", clientUUID).
		Count()
	return count, errors.Wrap(err, "failed to count the records in the db")
}

func SetClientPromotionVIP(clientUUID string, isVIP bool) error {
	vip := "false"
	if isVIP {
		vip = "true"
	}
	_, err := db.Model((*ClientsApps)(nil)).
		Where("uuid = ?", clientUUID).
		Set(fmt.Sprintf("promotion = jsonb_set(COALESCE(promotion, '{}'), '{is_vip}', '%s')", vip)).
		Update()
	if err != nil {
		return errors.Wrap(err, "failed to change client's promotion")
	}
	return nil
}
