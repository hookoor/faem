package models

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/structures"

	"github.com/gofrs/uuid"
)

// FeatureCApp - order frient structure
type FeatureCApp struct {
	tableName struct{} `sql:"client_features" pg:",discard_unknown_columns"`
	ormStruct
	structures.Feature
}

// Create - save structure to database
func (fr *FeatureCApp) Create() (FeatureCApp, error) {
	nFeature, err := fr.validCreation()
	if err != nil {
		return nFeature, fmt.Errorf("Data validation error. %v ", err)
	}

	uu, err := uuid.NewV4()
	if err != nil {
		return nFeature, err
	}

	nFeature.UUID = uu.String()
	_, err = db.Model(&nFeature).Returning("*").Insert()
	if err != nil {
		return nFeature, fmt.Errorf("DB error. %v ", err)
	}
	return nFeature, nil
}

func (fr *FeatureCApp) validCreation() (FeatureCApp, error) {
	var newFeature FeatureCApp
	if fr.Name == "" {
		return newFeature, fmt.Errorf("Field name is required")
	}
	newFeature.Name = fr.Name
	newFeature.Price = fr.Price
	newFeature.Comment = fr.Comment

	return newFeature, nil
}

// SaveToDB - сохраняет фичу в базу
func (fr FeatureCApp) SaveToDB() (FeatureCApp, error) {
	fr.CreatedAt = time.Now()
	_, err := db.Model(&fr).Returning("*").Insert()
	return fr, err
}

// FeatureList - return all Feature
func FeatureList() ([]FeatureCApp, error) {
	var frients []FeatureCApp
	err := db.Model(&frients).
		Where("deleted is not true").
		Order("updated_at DESC NULLS LAST").
		Order("created_at DESC NULLS LAST").
		Select()
	if err != nil {
		return frients, err
	}
	return frients, nil
}

// Update godoc
func (fr *FeatureCApp) Update(uuid string) (FeatureCApp, error) {

	uFeature, err := fr.validUpdate(uuid)
	if err != nil {
		return uFeature, fmt.Errorf("Data validation error. %v", err)
	}

	err = UpdateByPK(&uFeature)
	if err != nil {
		return uFeature, fmt.Errorf("DB error. %v", err)
	}
	return uFeature, nil
}

//
func (fr *FeatureCApp) validUpdate(uuid string) (FeatureCApp, error) {

	flag := false
	var frient FeatureCApp

	if fr.Name != "" {
		frient.Name = fr.Name
		flag = true
	}
	if fr.Comment != "" {
		frient.Comment = fr.Comment
		flag = true
	}
	if fr.Price != 0 {
		frient.Price = fr.Price
		flag = true
	}

	if !flag {
		return frient, fmt.Errorf("Required fielfr are empty")
	}
	frient.UpdatedAt = time.Now()
	frient.UUID = uuid
	err := CheckExistsUUID(&frient, uuid)
	if err != nil {
		return frient, err
	}
	frient.Deleted = false
	return frient, nil
}

// SetDeleted godoc
func (fr *FeatureCApp) SetDeleted() error {
	// TODO: вынести в общие методы и сделать его универсальным
	var frient FeatureCApp
	err := CheckExistsUUID(&frient, fr.UUID)
	if err != nil {
		return err
	}

	_, err = db.Model(fr).Where("uuid=?uuid").Set("deleted = true").Update()
	if err != nil {
		return err
	}

	return nil
}

// // FeatureByRegion godoc
// func FeatureByRegion(regionid int) ([]FeatureCApp, error) {
// 	var frients []FeatureCApp
// 	err := db.Model(&frients).
// 		Where("region_id = ? AND deleted is not true", regionid).
// 		Select()
// 	if err != nil {
// 		return frients, err
// 	}
// 	return frients, nil
// }

// DeleteAllFeatures - удаляет все записи в таблице features
func DeleteAllFeatures() error {
	_, err := db.Model(&FeatureCApp{}).Where("id<>?", 0).Delete()
	if err != nil {
		return err
	}
	return nil
}

// FillTableFeatures - заполняет записями таблицу features
func FillTableFeatures(featureArr []FeatureCApp) error {
	_, err := db.Model(&featureArr).Insert()
	if err != nil {
		return err
	}
	return nil
}
