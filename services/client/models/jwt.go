package models

import (
	"strconv"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/config"

	"fmt"
	"math/rand"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gofrs/uuid"
)

// const tokenExpiredTime = 120
// const refreshTokenExpiredMinutes = 30240
// const sessionExpirationMinutes = 2

type ServiceNameForHeader string

const RequestHeaderServiceNameKey ServiceNameForHeader = "ServiceName"
const RequestHeaderServiceClient ServiceNameForHeader = "faem_client"
const RequestHeaderServiceFood ServiceNameForHeader = "faem_food"
const RequestHeaderServiceFoodWeb ServiceNameForHeader = "faem_food_web"

// RegisterSession сессии для регистрации пользователей.
// здесь храним ИД сессии, телефон, серийник и время когда он будет актуальным
type RegisterSession struct {
	tableName  struct{}  `sql:"client_reg_sessions"`
	UUID       string    `json:"uuid" sql:",pk" description:"Session ID"`
	DeviceID   string    `json:"device_id"`
	Phone      string    `json:"phone"`
	Code       int       `json:"code"`
	Expiration time.Time `json:"expiration_time"`
	UsedAt     time.Time `json:"used_time"`
	CreatedAt  time.Time `sql:"default:now()" json:"created_at"`
}

type (
	// ClientRegistrRequest структура запроса на регистрацию
	ClientRegistrRequest struct {
		DeviceID string `json:"device_id"`
		Phone    string `json:"phone"`
	}
	// VerificationCode структура для получения кода
	VerificationCode struct {
		DeviceID           string `json:"device_id"`
		Code               int    `json:"code"`
		UTMSource          string `json:"utm_source"`           // метка-название промокода при регистрации для новых клиентов
		ReferralParentUUID string `json:"referral_parent_code"` // uuid клиента который предоставил реферальную ссылку
	}
	// TokenResponse структура ответа с токеным
	TokenResponse struct {
		Token                  string `json:"token"`
		ClientUUID             string `json:"client_uuid"`
		RefreshToken           string `json:"refresh_token"`
		RefreshTokenExpiration int64  `json:"refresh_expiration"`
		ServiceName            string `json:"service_name"`
	}
	// RefreshRequest структура запроса refrash механизма
	RefreshRequest struct {
		RefreshToken string `json:"refresh"`
	}
)

// JWTSessions godoc
type JWTSessions struct {
	tableName           struct{}  `sql:"client_jwt_sessions"`
	ID                  int       `json:"id" sql:",pk"`
	ClientID            int       `json:"client_id"`
	ClientUUID          string    `json:"client_uuid"`
	ServiceName         string    `json:"service_name"`
	RefreshToken        string    `json:"refresh_token" description:"Refresh token"`
	RefreshTokenUsed    time.Time `json:"refresh_token_used" description:"Refresh token used date"`
	RefreshTokenExpired time.Time `json:"refrash_expired" description:"Token expiration date"`
	CreatedAt           time.Time `sql:"default:now()" json:"created_at" `
}

// GenerateVerificationCode generates code for verification telephone number
func GenerateVerificationCode(rq ClientRegistrRequest) (int, int64, error) {

	var sess RegisterSession

	if rq.DeviceID == "" || rq.Phone == "" {
		return 0, 0, fmt.Errorf("Phone and Device ID are required fields. RTFM")
	}

	// TODO: Сделать валидацию на номер телефона
	c, err := db.Model(&sess).
		Where("device_id = ? AND phone = ? AND CURRENT_TIMESTAMP < expiration AND used_at IS NULL", rq.DeviceID, rq.Phone).
		Count()
	if err != nil {
		return 0, 0, err
	}
	if c != 0 {
		return 0, 0, fmt.Errorf("СМС отправлено, попробуйте запросить новый код авторизации позже")
	}

	sessionID, err := uuid.NewV4()
	if err != nil {
		return 0, 0, err
	}
	sess.UUID = sessionID.String()

	sess.DeviceID = rq.DeviceID
	sess.Phone = rq.Phone
	sess.Code = rangeIn(1000, 9999)
	dur := time.Minute * time.Duration(config.St.Application.SessionExpM)
	sess.Expiration = time.Now().Add(dur)

	_, err = db.Model(&sess).Returning("*").Insert()
	if err != nil {
		return 0, 0, err
	}

	return sess.Code, sess.Expiration.Unix(), nil
}

func rangeIn(low, hi int) int {
	return low + rand.Intn(hi-low)
}

func expireClientToken(clientUUID, serviceName string) error {

	var sessOld JWTSessions

	_, err := db.Model(&sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("client_uuid = ?", clientUUID).
		Where("service_name = ?", serviceName).
		Update()
	if err != nil {
		return err
	}

	return nil
}

func generateJWT(clientUUID, serviceName string, deviceID, phone string, regTime int64) (TokenResponse, error) {

	var tokenResp TokenResponse

	err := expireClientToken(clientUUID, serviceName)
	if err != nil {
		return tokenResp, err
	}

	jwtSess, err := newRefreshToken(clientUUID, serviceName)
	if err != nil {
		return tokenResp, err
	}

	tokenResp.RefreshToken = jwtSess.RefreshToken
	tokenResp.RefreshTokenExpiration = jwtSess.RefreshTokenExpired.UTC().Unix()
	tokenResp.ClientUUID = clientUUID
	tokenResp.ServiceName = jwtSess.ServiceName

	tokenResp.Token, err = newJWT(clientUUID, deviceID, phone, serviceName, regTime)
	if err != nil {
		return tokenResp, err
	}

	return tokenResp, nil
}

// NewRefrashToken returns refresh token and expiration time
func newRefreshToken(clientUUID, serviceName string) (JWTSessions, error) {

	var sessNew JWTSessions

	newToken, err := uuid.NewV4()
	if err != nil {
		return sessNew, err
	}

	sessNew.RefreshToken = newToken.String()
	dur := time.Minute * time.Duration(config.St.Application.RefreshTokenExpiredM)
	sessNew.RefreshTokenExpired = time.Now().Add(dur)
	sessNew.ClientUUID = clientUUID
	sessNew.ServiceName = serviceName

	err = sessNew.saveTokenData()
	if err != nil {
		return sessNew, err
	}

	return sessNew, nil
}

// GenerateJWT generates new token
func newJWT(clientUUID, deviceID string, phone string, serviceName string, regTime int64) (string, error) {

	jwtSec := config.JWTSecret()
	mySigningKey := []byte(jwtSec)

	claims := structures.ClientTokenClaim{
		ClientUUID:  clientUUID,
		RegTime:     strconv.FormatInt(regTime, 10),
		DeviceID:    deviceID,
		ServiceName: serviceName,
		Phone:       phone,
	}
	claims.IssuedAt = time.Now().Unix()

	dur := time.Minute * time.Duration(config.St.Application.TokenExpiredM)
	claims.ExpiresAt = time.Now().Add(dur).Unix()

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	ss, err := token.SignedString(mySigningKey)
	if err != nil {
		return "", err
	}

	return ss, nil
}

// saveTokenData expired existing and create new token for user
func (jwts *JWTSessions) saveTokenData() error {

	_, err := db.Model(jwts).Returning("*").Insert()
	if err != nil {
		return errpath.Err(err, "Error saving session data")
	}

	return nil
}

// RefreshJWTToken godoc
func RefreshJWTToken(token, serviceName string) (TokenResponse, error) {

	var newToken TokenResponse

	clientApp, err := expireRefreshToken(token)
	if err != nil {
		return newToken, err
	}

	jwtSess, err := newRefreshToken(clientApp.UUID, serviceName)

	newToken.RefreshTokenExpiration = jwtSess.RefreshTokenExpired.UTC().UnixNano()
	newToken.RefreshToken = jwtSess.RefreshToken
	newToken.ClientUUID = clientApp.UUID

	newToken.Token, err = newJWT(clientApp.UUID, clientApp.DeviceID, clientApp.MainPhone, serviceName, clientApp.CreatedAt.Unix())
	if err != nil {
		return newToken, err
	}

	return newToken, nil
}

// Expire token and return Client ID
func expireRefreshToken(token string) (ClientsApps, error) {

	var clientApp ClientsApps
	var sessOld JWTSessions

	_, err := db.Model(&sessOld).
		Set("refresh_token_used = ?", time.Now()).
		Where("refresh_token = ? AND CURRENT_TIMESTAMP < refresh_token_expired AND refresh_token_used is NULL", token).
		Returning("*").
		Update(&sessOld)
	if err != nil {
		return clientApp, fmt.Errorf("Refresh token not valid,%s", err)
	}

	err = db.Model(&clientApp).
		Where("deleted is not true AND UUID = ?", sessOld.ClientUUID).
		First()
	if err != nil {
		return clientApp, err
	}
	return clientApp, nil
}
