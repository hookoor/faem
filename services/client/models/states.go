package models

import (
	"fmt"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/constants"
	fcmpkg "gitlab.com/faemproject/backend/faem/pkg/fcm"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/services/client/fcmsender"
	rabhandler "gitlab.com/faemproject/backend/faem/services/client/rabsender"
)

type (
	TestFCM struct {
		Action      string `json:"action"`
		ClientToken string `json:"token"`
	}
)

//NewOrderState создает новый статус заказа
func (ordState OrderStateCApp) NewOrderState() error {
	ordState.StartState = time.Now()
	err := ordState.saveNewState()
	if err != nil {
		return fmt.Errorf("Error saving new orderstate,%s", err)
	}

	return nil
}

//NewOrderState сохраняет новый статус заказа
func (ordState *OrderStateCApp) saveNewState() error {
	ordState.CreatedAt = time.Now()
	_, err := db.Model(ordState).Insert()
	return err
}

//GetRelatedClient возвращает клиента, связанного с заказом
func (ordState *OrderStateCApp) GetRelatedOrderAndClient() (OrderCApp, ClientsApps, error) {
	var (
		order  OrderCApp
		client ClientsApps
	)
	err := db.Model(&order).Where("uuid = ?", ordState.OrderUUID).Select()
	if err != nil {
		return order, client, fmt.Errorf("error finding order,%s", err)
	}
	err = db.Model(&client).Where("uuid = ?", order.ClUUID).Select()
	if err != nil {
		return order, client, fmt.Errorf("error finding client,%s", err)
	}
	return order, client, nil
}

//SendToClient отправляет статус заказа клиенту если нужно
func (ordState *OrderStateCApp) SendToClient(delData structures.OrderDeliveryData) error {

	order, client, err := ordState.GetRelatedOrderAndClient()
	if err != nil {
		return fmt.Errorf("error getting related client, %s", err)
	}
	if ordState.State == constants.OrderStateTransToStore {
		messBody := "Курьер заведения скоро его доставит"
		if order.WithoutDelivery {
			messBody = "Вы сможете забрать его в заведении"
		}
		err = SendInitDataMessageToClient(
			client.UUID,
			"Заказ принят!",
			messBody,
		)
		if err != nil {
			return fmt.Errorf("error send init message to client,%s", err)
		}
		return nil
	}

	token, err := GetCurrentClientFCMToken(client.UUID)
	if err != nil {
		return errpath.Err(fmt.Errorf("error getting current client FCM token,%s", err))
	}
	offState := ordState.OfferStates
	offState.State = constants.ReplaceStateForClientIfNeed(offState.State, delData.IsProductsDelivery)

	return rabhandler.SendJSONByAction(
		rabhandler.AcFCMMessage,
		structures.OrderStateWithToken{
			ClientUUID:          client.UUID,
			Token:               token,
			ProductDeliveryData: delData,
			OrdState:            offState,
		},
		rabbit.StateKey)
}

//SendToClientTESTFCM отправляет статус заказа клиенту (для теста fcm)
func (ordState *OrderStateCApp) SendToClientTESTFCM(clientToken string) error {
	notTitle, notBody := ordState.GetNotifyTitleAndBody(structures.OrderDeliveryData{})
	fcmPayload := fcmsender.FCMOfferState{
		OfferStates: ordState.OfferStates,
		Title:       constants.TranslateOrderStateForClient(ordState.State, false),
	}
	err := fcmsender.SendMessageToCLient(
		fcmPayload,
		fcmpkg.OrderStateTag,
		[]string{clientToken},
		120,
		notTitle,
		notBody,
		true,
	)
	return err
}

//SendToClientDRVCOORTESTFCM отправляет координаты водилы клиенту (для теста fcm)
func SendToClientDRVCOORTESTFCM(clientToken string) error {
	var (
		coords structures.Coordinates
	)
	coords.Lat = 43.036274
	coords.Long = 44.655212
	coords.DriverUUID = "some_driver_uuid"

	err := fcmsender.SendMessageToCLient(
		coords,
		fcmpkg.DriverLocTag,
		[]string{clientToken},
		25,
		"",
		"",
	)
	return err
}

//SendToClientChatMessTESTFCM отправляет сообщение клиенту (для теста fcm)
func SendToClientChatMessTESTFCM(clientToken string) error {
	var (
		mess structures.ChatMessages
	)
	mess.UUID = "some mess uuid"
	mess.Message = "some mess body"
	mess.DriverUUID = "some_driver_uuid"
	mess.OrderUUID = "some order uuid "
	mess.Sender = structures.DriverMember
	mess.Receiver = structures.ClientMember
	mess.CreatedAtUnix = time.Now().Unix()
	mess.ClientUUID = "some_client_uuid"
	err := fcmsender.SendMessageToCLient(
		mess,
		fcmpkg.ChatMessageTag,
		[]string{clientToken},
		300,
		"Новое сообщение",
		"",
		true,
	)
	return err
}

// GetNitifyTitle возвращает подходящий заголовок push-уведомления. Если статус не нужно выводить
// в шторку уведомлений, статус должен быть пустым
func (ordState *OrderStateCApp) GetNotifyTitleAndBody(delData structures.OrderDeliveryData) (string, string) {

	switch ordState.State {
	case constants.OrderStateAccepted:
		return "Водитель найден", ""
	case constants.OrderStateOnPlace:
		if delData.IsProductsDelivery {
			return "", ""
		}
		return "Водитель ожидает", ""
	case constants.OrderStateCooking:
		return "Заказ уже готовится", "Совсем скоро он к вам отправится"
	case constants.OrderStateFinished:
		if delData.IsProductsDelivery {
			return "Заказ завершен", ""
		}
		return "Оцените поездку", ""
	case constants.OrderStateOnTheWay:
		if delData.IsProductsDelivery {
			if delData.WithoutDelivery {
				return "Курьер забрал ваш заказ", "Он свяжется с вами по прибытии на адрес доставки"
			}
			return "Курьер забрал ваш заказ", "Он уже направляется к вам"
		}
	case constants.OrderStatePayment:
		if delData.IsProductsDelivery {
			if delData.WithoutDelivery {
				return "Заказ готов к выдаче", "Осталось всего лишь его забрать!"
			}
			if delData.OwnDelivery {
				return "", ""
			}
			return "Водитель подъехал", "Осталось всего лишь получить свой заказ!"
		}
	case constants.OrderStateCancelled:
		return "Ваш заказ был отменен", fmt.Sprintf("Причина отмены: %s", constants.CancelReasonRU[ordState.Comment])
	}
	return "", ""
}
