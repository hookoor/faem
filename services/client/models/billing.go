package models

import (
	"fmt"
	"gitlab.com/faemproject/backend/faem/pkg/constants"

	"github.com/pkg/errors"

	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	changeOrderPayTypeNotifyTitle = "Способ оплаты вашего заказа изменен"
	changeOrderPayTypeNotifyBody  = "Новый способ оплаты - наличные"
)

func ChangeOrderPaymentTypeToCash(orderUUID string) error {
	var order OrderCApp
	_, err := db.Model(&order).
		Set("payment_type = ?", constants.OrderPaymentTypeCash).
		Set(fmt.Sprintf("tariff = jsonb_set(COALESCE(tariff, '{}'), '{payment_type}', '\"%s\"')", constants.OrderPaymentTypeRU[constants.OrderPaymentTypeCash])).
		Where("uuid = ?", orderUUID).
		Returning("*").
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment type", orderUUID)
	}
	return SendInitDataMessageToClient(
		order.ClUUID,
		changeOrderPayTypeNotifyTitle,
		changeOrderPayTypeNotifyBody)
}

func FillGeneratedReceipt(receipt structures.ReceiptHook) error {
	var order OrderCApp
	if err := GetByUUID(receipt.OrderUUID, &order); err != nil {
		return errors.Wrapf(err, "failed to get order by uuid [%s]", receipt.OrderUUID)
	}

	meta := order.PaymentMeta
	if !meta.HasPrimaryData() {
		meta.ReceiptURL = receipt.Url
		meta.QrCodeURL = receipt.QrCodeUrl
	} else {
		meta.AdditionalData = append(meta.AdditionalData, structures.OrderAdditionalPaymentMetadata{
			ReceiptURL: receipt.Url,
			QrCodeURL:  receipt.QrCodeUrl,
		})
	}
	order.PaymentMeta = meta // update the field

	_, err := db.Model(&order).
		Set("payment_meta = ?payment_meta").
		Where("uuid = ?uuid").
		Update()
	if err != nil {
		return errors.Wrapf(err, "failed to update order's [%s] payment meta", receipt.OrderUUID)
	}
	return nil
}
