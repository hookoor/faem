package models

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-pg/pg"
	"gitlab.com/faemproject/backend/faem/pkg/localtime"
	"gitlab.com/faemproject/backend/faem/pkg/rabbit"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
)

var (
	db *pg.DB
	rb *rabbit.Rabbit

	// GodModeInst -
	GodModeInst structures.GodMode
)

func init() {
	GodModeInst.Init()
}

type ormStruct struct {
	Deleted   bool      `json:"-" sql:"default:false"`
	ID        int       `json:"-" sql:",pk"`
	CreatedAt time.Time `sql:"default:now()" json:"-" description:"Дата создания"`
	UpdatedAt time.Time `json:"-" `
}

// NewRegResponseStruct REST response structs
type NewRegResponseStruct struct {
	Code            int    `json:"code"`
	Msg             string `json:"message"`
	NextRequestTime int64  `json:"next_request_time"`
}

// ReasonsForFront причины для отмены заказа
type ReasonsForFront struct {
	ReasonName      string `json:"reason_name" `
	ReasonTitle     string `json:"reason_title" `
	ReasonImagePath string `json:"reason_image"`
}

// DriverLocationDataForClient - координаты водителей
type DriverLocationDataForClient struct {
	// tableName  struct{}  `sql:"crm_drv_locations"`
	ID         int       `sql:",pk"`
	DriverID   int64     `json:"driver_id"`
	DriverUUID string    `json:"driver_uuid" sql:",unique" `
	Latitude   float64   `json:"lat"`
	Longitude  float64   `json:"lng"`
	Satelites  int       `json:"sats"`
	Timestamp  int64     `json:"time"`
	CreatedAt  time.Time `sql:"default:now()" json:"-"`
}

// ConnectDB initialize connection to package var
func ConnectDB(conn *pg.DB) {
	db = conn
}

// CheckExistsID return error if ID in mdl model not found
func CheckExistsID(mdl interface{}, id int) error {
	exs, _ := db.Model(mdl).Column("id").Where("id = ? AND deleted is not true", id).Exists()

	if !exs {
		return fmt.Errorf("Запись ID=%v не найдена или удалена", id)
	}
	return nil
}

// CheckExistsUUID return error if UUID in mdl model not found
func CheckExistsUUID(mdl interface{}, uuid string) error {
	exs, err := db.Model(mdl).Column("uuid").Where("uuid = ? AND deleted is not true", uuid).Exists()
	if err != nil {
		return fmt.Errorf("ошибка проверки существования записи с uuid [%s]", uuid)
	}
	if !exs {
		return fmt.Errorf("Запись UUID=%v не найдена или удалена", uuid)
	}
	return nil
}

// UpdateByPK update model by PK
func UpdateByPK(mdl interface{}) error {
	_, err := db.Model(mdl).Where("uuid=?uuid").Returning("*").UpdateNotNull()
	if err != nil {
		return err
	}
	return nil
}

// GetByUUID returning object by interface and UUID
func GetByUUID(uuid string, mdl interface{}) error {
	err := db.Model(mdl).
		Where("deleted is not true AND uuid = ?", uuid).
		Select()
	if err != nil {
		return err
	}
	return nil
}

// GetByID returning object by interface and ID
func GetByID(id int, mdl interface{}) error {
	err := db.Model(mdl).
		Where("deleted is not true AND id = ?", id).
		Select()
	if err != nil {
		return err
	}
	return nil
}

// // BeforeUpdate godoc
// func (os *ormStruct) BeforeUpdate(db orm.DB) error {
// 	os.UpdatedAt = time.Now()
// 	return nil
// }

// ConnectRabbit initialize connection to package var
func ConnectRabbit(rab *rabbit.Rabbit) {
	rb = rab
}

// GetItinerary - получение данных с помощью OSRM по координатам
func GetItinerary(coordinates []structures.Coordinates) (structures.OSRMData, error) {
	var (
		reqString string
		res       structures.OSRMData
	)

	client := &http.Client{}
	reqString += config.St.OSRM.Host
	reqString += "/route/v1/driving/"

	for i, inst := range coordinates {
		reqString += strconv.FormatFloat(float64(inst.Long), 'f', 6, 64) + "," + strconv.FormatFloat(float64(inst.Lat), 'f', 6, 64)
		if i != len(coordinates)-1 {
			reqString += ";"
		}
	}

	reqString += "?geometries=geojson"

	req, err := http.NewRequest(
		"GET",
		reqString,
		nil,
	)
	if err != nil {
		fmt.Println(err)
		return structures.OSRMData{}, err
	}
	//req.Header.Add("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return structures.OSRMData{}, err
	}
	defer resp.Body.Close()
	//io.Copy(os.Stdout, resp.Body)
	decoder := json.NewDecoder(resp.Body)

	err = decoder.Decode(&res)
	if err != nil {
		return structures.OSRMData{}, fmt.Errorf("ошибка декодинга ответа с OSRM,%s", err)
	}
	if res.Code != "Ok" {
		return structures.OSRMData{}, errpath.Errorf("статус не ОК")
	}

	return res, nil
}
func CurrentTime() time.Time {
	return localtime.TimeInZone(time.Now(), config.St.Application.TimeZone)
}

// GetRouteWayData - преобразовывает osrm данные в нужный формат
func GetRouteWayData(routePoints []structures.Route) (structures.RouteWayData, error) {
	var (
		routeWayData      structures.RouteWayData
		routeToDriverJson structures.RouteToDriverJson
		coords            []structures.Coordinates
		step              int
	)

	for _, inst := range routePoints {
		coords = append(coords, structures.Coordinates{Lat: float64(inst.Lat), Long: float64(inst.Lon)})
	}
	if len(routePoints) <= 1 {
		return structures.RouteWayData{}, nil
	}
	osrmData, err := GetItinerary(coords)
	if err != nil {
		return structures.RouteWayData{}, errpath.Err(err, "ошибка расчета дистанции")
	}

	routeWayData.Geometry.Way.Coor = osrmData.Routes[0].Geometry.Coor
	routeWayData.Geometry.Way.Type = osrmData.Routes[0].Geometry.Type
	routeWayData.Geometry.Type = "Feature"
	routeWayData.Geometry.Proper.Duration = int(osrmData.Routes[0].Duration)
	routeWayData.Geometry.Proper.Distance = osrmData.Routes[0].Distance

	for k, inst := range osrmData.Routes[0].Geometry.Coor {
		if osrmData.Waypoints[step].Location[0] == inst[0] {
			if step < len(routePoints)-1 {
				routeToDriverJson.Type = "Feature"
				routeToDriverJson.Proper.Duration = int(osrmData.Routes[0].Legs[step].Duration)
				routeToDriverJson.Proper.Distance = osrmData.Routes[0].Legs[step].Distance
				routeWayData.Routes = append(routeWayData.Routes, routeToDriverJson)
			}
			if step > 0 {
				routeWayData.Routes[step-1].Way.Coor = append(routeWayData.Routes[step-1].Way.Coor, osrmData.Routes[0].Geometry.Coor[k])
			}
			step++
		}
		if step == len(routePoints) {
			break
		}
		routeWayData.Routes[step-1].Way.Type = "LineString"
		routeWayData.Routes[step-1].Way.Coor = append(routeWayData.Routes[step-1].Way.Coor, osrmData.Routes[0].Geometry.Coor[k])
	}

	return routeWayData, nil
}

// ----------------------
// ----------------------
// ----------------------

// GetReferralURL -
func GetReferralURL(ctx context.Context, clientUUID string) (tool.URL, error) {
	var tmp string

	var client ClientsApps
	err := db.ModelContext(ctx, &client).
		Where("uuid = ?", clientUUID).
		Where("deleted is not true").
		Select()
	if err != nil {
		return tool.URL(tmp), errpath.Err(err)
	}

	tmp = fmt.Sprintf(config.GetCurrentReferralSystemParamsConfig().ReferralURLTemplate, client.ReferralProgramData.ReferralCode)

	return tool.URL(tmp), nil
}

// GetAndIncRecipientTravelCount -
func GetAndIncRecipientTravelCount(clientuuid string) (int, error) {
	var err error
	var res int

	var client ClientsApps
	_, err = db.Model(&client).
		Where("uuid = ?", clientuuid).
		Set(`referral_program_data = 
			JSONB_SET(COALESCE(referral_program_data, '{}'), 
				'{recipients_travel_count}', 
				((referral_program_data ->> 'recipients_travel_count')::INT + 1)::TEXT::JSONB)`).
		Returning("referral_program_data").
		Update()
	if err != nil {
		return 0, errpath.Err(err)
	}

	res = client.ReferralProgramData.RecipientsTravelCount

	return res, nil
}

// ----------------------
// ----------------------
// ----------------------

func MakeClientNotNewcomer(uuid string) error {
	var err error

	var client ClientsApps
	_, err = db.Model(&client).
		Where("uuid = ?", uuid).
		Set(`referral_program_data = 
			JSONB_SET(COALESCE(referral_program_data, '{}'), 
				'{is_newcomer}', 
				'false'::JSONB)`).
		Update()
	if err != nil {
		return errpath.Err(err)
	}

	return nil
}
