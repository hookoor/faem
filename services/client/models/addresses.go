package models

import (
	"gitlab.com/faemproject/backend/faem/pkg/structures"
)

const (
	CustomerAddressSource = "source"
	CustomerAddressTarget = "target"
)

func LastCustomerAddressesList(clientUUID string, destination string, count int) ([]structures.Route, error) {
	var lastOrdersList []OrderCApp
	var res []structures.Route

	err := db.Model(&lastOrdersList).
		Where("cl_uuid = ?", clientUUID).
		Order("created_at DESC").
		Column("routes").
		Limit(count).
		Select()
	if err != nil {
		return nil, err
	}

	// if target == CustomerAddressTarget {
	// 	for _, inst := range lastOrdersList {
	// 		res = append(res, inst.Routes[len(inst.Routes)-1])
	// 	}
	// }
	if destination == CustomerAddressSource {
		for _, inst := range lastOrdersList {
			res = append(res, inst.Routes[0])
		}
	} else {
		for _, inst := range lastOrdersList {
			res = append(res, inst.Routes[len(inst.Routes)-1])
		}
	}
	res = sortRoutesByUnique(res)
	return res, nil
}

func sortRoutesByUnique(old []structures.Route) (new []structures.Route) {
	alreadyAdded := func(str string) bool {
		for _, item := range new {
			if item.UnrestrictedValue == str {
				return true
			}
		}
		return false
	}
	for _, item := range old {
		if alreadyAdded(item.UnrestrictedValue) {
			continue
		}
		new = append(new, item)
	}
	return
}

// // GetMatches возвращает наиболее релевантные адреса для переданной строки
// func GetMatches(str string) []structures.Route {
// 	var Mathes []structures.Route
// var (
// 	a      []float64
// 	m      int
// )
// distances := make(map[float64][]structures.Route)
// distances2 := make(map[float64][]structures.Route)

// for _, value := range AllRoutes {
// 	Mathes = append(Mathes, value)

// if strings.HasPrefix(strings.ToLower(value.Value), strings.ToLower(str)) {
// 	Mathes = append(Mathes, value)
// 	m++
// 	if m >= 9 {
// 		break
// 	}
// }
// //в coeff записывается отношение длины присылаемой и сравниваемой строки. Нужно для того чтобы длинные строки были более релевантными, вроде работает
// coeff := float64(len(str)) / float64(len(value.Value))
// distance := float64(Distance(str, value.Value)) * coeff
// distances[distance] = append(distances[distance], value)
// }
// for i := range distances {
// 	a = append(a, i)
// }
// sort.Float64s(a)

// for _, value := range a {

// 	// distances2[value] = distances[value]
// 	for _, value2 := range distances[value] {

// 		m++
// 		if m >= 9 {
// 			break
// 		}
// 		Mathes = append(Mathes, value2)
// 	}
// 	if m >= 9 {
// 		break
// 	}
// }

// 	return Mathes
// }

// var aaa int = 0

// // Distance вычисление дистанции между строками (по Левенштейну)
// func Distance(s1, s2 string) int {
// 	min := func(values ...int) int {
// 		m := values[0]
// 		for _, v := range values {
// 			if v < m {
// 				m = v
// 			}
// 		}
// 		return m
// 	}
// 	r1, r2 := []rune(s1), []rune(s2)
// 	n, m := len(r1), len(r2)
// 	if n > m {
// 		r1, r2 = r2, r1
// 		n, m = m, n
// 	}
// 	currentRow := make([]int, n+1)
// 	previousRow := make([]int, n+1)
// 	for i := range currentRow {
// 		currentRow[i] = i
// 	}
// 	for i := 1; i <= m; i++ {
// 		for j := range currentRow {
// 			previousRow[j] = currentRow[j]
// 			if j == 0 {
// 				currentRow[j] = i
// 				continue
// 			} else {
// 				currentRow[j] = 0
// 			}
// 			add, del, change := previousRow[j]+1, currentRow[j-1]+1, previousRow[j-1]
// 			if r1[j-1] != r2[i-1] {
// 				change++
// 			}
// 			currentRow[j] = min(add, del, change)

// 		}
// 	}
// 	return currentRow[n]
// }
