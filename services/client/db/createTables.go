package db

import (
	"log"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
)

// MyCreateTable - функция создания таблицы по струтуре TestStruct
func MyCreateTable(db *pg.DB, table interface{}) error {
	// структура-настройка при создании таблицы
	opts := &orm.CreateTableOptions{
		// проверка на создание такой же таблицы
		IfNotExists: true,
	}

	// создаем таблицу
	createErr := db.CreateTable(table, opts)
	if createErr != nil {
		log.Printf("Ошибка при создании таблицы, [%v] \n", createErr)
		return createErr
	}

	log.Printf("Таблица создана успешно \n")

	return nil
}

// // CreateTables - функция создания все таблиц
// func CreateTables() error {
// 	// model.CreateTableWasher()
// 	MyCreateTable(myConn, &models.OrderStateCApp{})
// 	MyCreateTable(myConn, &models.FeatureCApp{})
// 	MyCreateTable(myConn, &models.ServiceCApp{})
// 	MyCreateTable(myConn, &models.OrderCApp{})

// 	return nil
// }
