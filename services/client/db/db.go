package db

import (
	"fmt"

	pg "github.com/go-pg/pg"
	"github.com/go-pg/pg/orm"
	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

var (
	// CRMConn -
	CRMConn    *pg.DB
	myConn     *pg.DB
	modelsList []interface{}
)

func initModelList() {
	modelsList = append(modelsList, (*models.ClientsApps)(nil))

}

type dbLogger struct{}

func (d dbLogger) BeforeQuery(q *pg.QueryEvent) {}

func (d dbLogger) AfterQuery(q *pg.QueryEvent) {
	logs.Eloger.Trace(q.FormattedQuery())
}

// Connect return DB connection
func Connect() (*pg.DB, error) {

	var conn *pg.DB

	addr := fmt.Sprintf("%s:%v", config.St.Database.Host, config.St.Database.Port)

	conn = pg.Connect(&pg.Options{
		Addr:     addr,
		User:     config.St.Database.User,
		Password: config.St.Database.Password,
		Database: config.St.Database.Db,
	})
	var n int

	conn.AddQueryHook(dbLogger{})
	_, err := conn.QueryOne(pg.Scan(&n), "SELECT 1")
	if err != nil {
		return conn, fmt.Errorf("Error conecting to DB. Host: %s, user: %s, db: %s", config.St.Database.Host, config.St.Database.User, config.St.Database.Db)
	}

	if err = createSchema(conn); err != nil {
		return conn, fmt.Errorf("Error creating DB schemas. %v", err)
	}

	myConn = conn
	return conn, nil
}

// CloseDbConnection closing connection for defer in main
func CloseDbConnection(db *pg.DB) {
	db.Close()
}

func createSchema(db *pg.DB) error {
	logrus.Info("Creatind tables if not exist...")
	initModelList()
	for _, m := range modelsList {
		err := db.CreateTable(m, &orm.CreateTableOptions{
			IfNotExists:   true,
			FKConstraints: false,
		})
		if err != nil {
			return err
		}
	}
	return nil
}
