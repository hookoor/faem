package tickers

import (
	"net/http"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
)

const (
	checkrefreshConfigl = time.Minute
)

// InitRefreshConfig -
func InitRefreshConfig() {
	wg.Add(1)
	go refreshConfig()
}

func refreshConfig() {
	defer wg.Done()

	log := logs.Eloger.WithFields(logrus.Fields{
		"event": "ticker refreshConfig",
	})

	for {
		select {
		case <-closed:
			return
		case <-time.After(checkrefreshConfigl):

			err := refreshReferralSystemParams()
			if err != nil {
				log.WithField("reason", "refrashReferralSystemParams").Error(errpath.Err(err).Error())
			}

			// ...refresh something else

			log.Info("config refresh successfully")
		}
	}
}

func refreshReferralSystemParams() error {

	var bindReferralSystemParams structures.ReferralSystemParams
	url := config.St.CRM.BaseURL + "/referralsystemparams"
	err := tool.SendRequest(http.MethodGet, url, nil, nil, &bindReferralSystemParams)
	if err != nil {
		return errpath.Err(err)
	}

	config.SetCurrentReferralSystemParamsConfig(bindReferralSystemParams)

	return nil
}
