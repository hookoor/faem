package tickers

import (
	"github.com/pkg/errors"
	"gitlab.com/faemproject/backend/faem/services/client/models"
	"gitlab.com/faemproject/backend/faem/services/client/rabreceiver"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

const (
	buffClear = time.Minute * 20
)

// InitRefreshConfig -
func InitClientIncome() {
	wg.Add(1)
	go clientIncome()
}

func clientIncome() {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(buffClear):

			err := clientIncomeHandler()
			if err != nil {
				logs.Eloger.WithError(err).Error("clients income ticker")
			} else {
				logs.Eloger.Info("clients income handled successfully")
			}
		}
	}
}

func clientIncomeHandler() error {
	var rows []models.ClientIncome

	for clientUUID, friendTripCount := range rabreceiver.ClientFriendTripCountBuffer {
		var totalCount int

		clIncData, err := models.GetClientIncomeLastRecord(clientUUID)
		if err != nil {
			if err.Error() != "pg: no rows in result set" {
				return errors.Wrap(err, "fail to get client last income")
			}
			totalCount = 0
		} else {
			totalCount = clIncData.TotalCount
		}

		row := models.ClientIncome{
			ClientUUID: clientUUID,
			Count:      friendTripCount,
			TotalCount: totalCount + friendTripCount,
		}
		rows = append(rows, row)
	}

	logs.Eloger.Info("client incomes for save: ", rows)
	err := models.SaveClientIncomeRecords(&rows)
	if err != nil {
		return errors.Wrap(err, "fail to save client income data")
	}
	logs.Eloger.Info("client incomes saved")

	// clear buffer
	rabreceiver.ClientFriendTripCountBuffer = map[string]int{}

	return nil
}
