package tickersmodern

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
	"gitlab.com/faemproject/backend/faem/pkg/structures"
	"gitlab.com/faemproject/backend/faem/pkg/structures/errpath"
	"gitlab.com/faemproject/backend/faem/pkg/structures/tool"
	"gitlab.com/faemproject/backend/faem/services/client/config"
	"gitlab.com/faemproject/backend/faem/services/client/models"
)

// InitRefreshGodModeParamsTicker -
func (ticker *Ticker) InitRefreshGodModeParamsTicker() {
	wg.Add(1)
	go ticker.refreshGodModeParams(context.Background())
}

func (ticker *Ticker) refreshGodModeParams(ctx context.Context) {
	defer wg.Done()

	for {
		select {
		case <-closed:
			return
		case <-time.After(structures.RefreshGodModeParamsPeriod):

			log := logs.Eloger.WithField("event", "refreshGodModeParams")

			var resp structures.GodMode
			err := tool.SendRequest(
				http.MethodGet,
				config.St.CRM.BaseURL+"/godmode",
				nil,
				nil,
				&resp)
			if err != nil {
				log.Error(errpath.Err(err))
			}

			if models.GodModeInst != resp {
				models.GodModeInst = resp

				if resp.OSRM.Address != "" {
					config.St.OSRM.Host = resp.OSRM.Address
				}
			}

		}
	}
}
