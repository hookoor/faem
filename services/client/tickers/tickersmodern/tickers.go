package tickersmodern

import (
	"sync"
	"time"

	"github.com/go-pg/pg"

	"gitlab.com/faemproject/backend/faem/pkg/logs"
)

var (
	wg     sync.WaitGroup
	closed = make(chan struct{})
)

// Ticker -
type Ticker struct {
	dbConn *pg.DB
}

// Init -
func Init(conn *pg.DB) *Ticker {
	ticker := &Ticker{
		dbConn: conn,
	}

	ticker.InitRefreshGodModeParamsTicker()

	return ticker
}

// Wait -
func (*Ticker) Wait(shutdownTimeout time.Duration) {
	// try to shutdown the listener gracefully
	stoppedGracefully := make(chan struct{}, 1)
	go func() {
		// Notify subscribers about exit, wait for their work to be finished
		close(closed)
		wg.Wait()
		stoppedGracefully <- struct{}{}
	}()

	// wait for a graceful shutdown and then stop forcibly
	select {
	case <-stoppedGracefully:
		logs.Eloger.Info("tickers stopped gracefully")
	case <-time.After(shutdownTimeout):
		logs.Eloger.Info("tickers stopped forcibly")
	}
}
