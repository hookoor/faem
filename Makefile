.PHONY: deps
deps:
	@go mod vendor
	@go mod tidy

.PHONY: build-client
build-client: deps
	go build -o ./bin/client ./services/client

.PHONY: build-crm
build-crm: deps
	go build -o ./bin/crm ./services/crm

.PHONY: build-driver
build-driver: deps
	go build -o ./bin/driver ./services/driver

.PHONY: build-all
build-all: deps \
	build-client build-crm build-driver

.PHONY: image-client
image-client: deps
	docker build -t localhost/faem/client:dirty -f docker/client/Dockerfile .

.PHONY: image-crm
image-crm: deps
	docker build -t localhost/faem/crm:dirty -f docker/crm/Dockerfile .

.PHONY: image-driver
image-driver: deps
	docker build -t localhost/faem/driver:dirty -f docker/driver/Dockerfile .
