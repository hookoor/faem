# secrets

Документ описывает создание секретов для приложений в Kubernetes.

## Client

```bash
export CLIENT_POSTGRES_USER=client_user
export CLIENT_POSTGRES_PASSWORD=put-password-here
export CLIENT_RABBIT_USER_CREDENTIALS="client_backend:put-password-here"
export CLIENT_FCM_SERVERKEY="client_fcm-password-here"
export CLIENT_SMPP_USER="itsrv23_feam"
export CLIENT_SMPP_PASSWORD="put-password-here"


cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: client-settings
  labels:
    app.kubernetes.io/name: client
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${CLIENT_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${CLIENT_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${CLIENT_RABBIT_USER_CREDENTIALS}" | base64)
  smpp-user: $(echo -n "${CLIENT_SMPP_USER}" | base64)
  smpp-password: $(echo -n "${CLIENT_SMPP_PASSWORD}" | base64)
  client-fcm-key: $(echo -n "${CLIENT_FCM_SERVERKEY}" | base64)
EOF
```

## CRM

```bash
export CRM_POSTGRES_USER=crm_user
export CRM_POSTGRES_PASSWORD=put-password-here
export CRM_RABBIT_USER_CREDENTIALS="crm_backend:put-password-here"

cat << EOF | kubectl apply -f -
   apiVersion: v1
   kind: Secret
   metadata:
     name: crm-settings
     labels:
       app.kubernetes.io/name: crm
       app.kubernetes.io/managed-by: kubectl
   data:
     postgres-user: $(echo -n "${CRM_POSTGRES_USER}" | base64)
     postgres-password: $(echo -n "${CRM_POSTGRES_PASSWORD}" | base64)
     rabbitmq-user-credentials: $(echo -n "${CRM_RABBIT_USER_CREDENTIALS}" | base64)
   EOF
```

## Driver

```bash
export DRIVER_POSTGRES_USER=driver_user
export DRIVER_POSTGRES_PASSWORD=put-password-here
export DRIVER_RABBIT_USER_CREDENTIALS="drv_backend:put-password-here"
export DRIVER_RABBIT_DRIVER_CREDENTIALS="driver_client:put-password-here"
export DRIVER_SMPP_USER="itsrv23_feam"
export DRIVER_SMPP_PASSWORD="put-password-here"
export DRIVER_FCM_SERVERKEY="fcm-key-here"


cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: driver-settings
  labels:
    app.kubernetes.io/name: driver
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${DRIVER_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${DRIVER_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${DRIVER_RABBIT_USER_CREDENTIALS}" | base64)
  rabbitmq-driver-credentials: $(echo -n "${DRIVER_RABBIT_DRIVER_CREDENTIALS}" | base64)
  smpp-user: $(echo -n "${DRIVER_SMPP_USER}" | base64)
  smpp-password: $(echo -n "${DRIVER_SMPP_PASSWORD}" | base64)
  driver-fcm-key: $(echo -n "${DRIVER_FCM_SERVERKEY}" | base64)
EOF
```

## VOIP

```bash
export VOIP_POSTGRES_USER=voip_user
export VOIP_POSTGRES_PASSWORD=put-password-here
export VOIP_RABBIT_USER_CREDENTIALS="voip_backend:put-password-here"
export VOIP_TTS_OAUTH="voip_tts_oauth"
export VOIP_TTS_FOLDERID="folderID"
export VOIP_ASTER_PASSWORD="aster-password"


cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: voip-settings
  labels:
    app.kubernetes.io/name: voip
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${VOIP_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${VOIP_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${VOIP_RABBIT_USER_CREDENTIALS}" | base64)
  voip-tts-outh-key: $(echo -n "${VOIP_TTS_OAUTH}" | base64)
  voip-tts-folderid: $(echo -n "${VOIP_TTS_FOLDERID}" | base64)
  voip-aster-password: $(echo -n "${VOIP_ASTER_PASSWORD}" | base64)
VOIP_TTS_OAUTH
EOF
```

## CHAT

```bash
export CHAT_POSTGRES_USER=voip_user
export CHAT_POSTGRES_PASSWORD=put-password-here
export CHAT_RABBIT_USER_CREDENTIALS="chat_backend:put-password-here"
export CHAT_SECRET=voip_user

cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: chat-settings
  labels:
    app.kubernetes.io/name: chat
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${CHAT_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${CHAT_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${CHAT_RABBIT_USER_CREDENTIALS}" | base64)
  application-secret: $(echo -n "${CHAT_SECRET}" | base64)
EOF
```

## LIMESIDECAR

```bash
export LIMESIDECAR_POSTGRES_USER=voip_user
export LIMESIDECAR_POSTGRES_PASSWORD=put-password-here
export LIMESIDECAR_RABBIT_USER_CREDENTIALS="voip_backend:put-password-here"
export LIMESIDECAR_CLIENTJWT=voip_user
export LIMESIDECAR_EXTERNALPG_USER=voip_user
export LIMESIDECAR_EXTERNALPG_PASS=voip_user


cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: limesidecar-settings
  labels:
    app.kubernetes.io/name: limesidecar
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${LIMESIDECAR_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${LIMESIDECAR_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${LIMESIDECAR_RABBIT_USER_CREDENTIALS}" | base64)
  client-jwt: $(echo -n "${LIMESIDECAR_CLIENTJWT}" | base64)
  external-postgres-user: $(echo -n "${LIMESIDECAR_EXTERNALPG_USER}" | base64)
  external-postgres-password: $(echo -n "${LIMESIDECAR_EXTERNALPG_PASS}" | base64)
EOF
```

## BILLING

```bash
export BILLING_POSTGRES_USER=billing_user
export BILLING_POSTGRES_PASSWORD=put-password-here
export BILLING_BROKER_USERCREDITS="voip_backend:put-password-here"
export BILLING_AUTH_GATE_PASSWORD="pass_here"
export BILLING_RPC_PASSWORD="pass_here"

cat << EOF | kubectl apply -f -
apiVersion: v1
kind: Secret
metadata:
  name: billing-settings
  labels:
    app.kubernetes.io/name: billing
    app.kubernetes.io/managed-by: kubectl
data:
  postgres-user: $(echo -n "${BILLING_POSTGRES_USER}" | base64)
  postgres-password: $(echo -n "${BILLING_POSTGRES_PASSWORD}" | base64)
  rabbitmq-user-credentials: $(echo -n "${BILLING_BROKER_USERCREDITS}" | base64)
  gate-password: $(echo -n "${BILLING_AUTH_GATE_PASSWORD}" | base64)
  rpc-password: $(echo -n "${BILLING_RPC_PASSWORD}" | base64)

EOF
```