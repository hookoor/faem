# Настройки RabbitMQ

В настояший момент очереди и обменники создаются самими приложениями. Мы создаем только пользователей и раздаем им права.

```
export RABBIT_CRM_USER=crm
export RABBIT_CRM_PASSWORD=crmpassword

export RABBIT_DRIVER_USER=driver
export RABBIT_DRIVER_PASSWORD=driverpassword

export RABBIT_DRVCLIENT=driverclient
export RABBIT_DRVCLIENT_PASSWORD=driverpswclient

export RABBIT_CLIENT_USER=client
export RABBIT_CLIENT_PASSWORD=clientpassword

export RABBIT_CLIENTIOS=clientios
export RABBIT_CLIENTISO_PASSWORD=clientiospassword


// UserCredits - аккаунт CRM бэкенда 
rabbitmqctl add_user $RABBIT_CRM_USER $RABBIT_CRM_PASSWORD
rabbitmqctl set_permissions -p / $RABBIT_CRM_USER ".*" ".*" ".*"


// UserCredits в Driver - аккаунт Driver бэкенда
rabbitmqctl add_user $RABBIT_DRIVER_USER $RABBIT_DRIVER_PASSWORD
rabbitmqctl set_permissions -p / $RABBIT_DRIVER_USER ".*" ".*" ".*"


// UserCredits в Client - аккаунт клиентского приложения
rabbitmqctl add_user $RABBIT_CLIENT_USER $RABBIT_CLIENT_PASSWORD
rabbitmqctl set_permissions -p / $RABBIT_CLIENT_USER ".*" ".*" ".*"


// DriverCredits в Driver - кредитсы для водительского приложения
rabbitmqctl add_user $RABBIT_DRVCLIENT $RABBIT_DRVCLIENT_PASSWORD
rabbitmqctl set_permissions -p / $RABBIT_DRVCLIENT "^$" ".*" ".*"


// ClientCredits в Client - кредитсы для клиентского приложения
rabbitmqctl add_user $RABBIT_CLIENTIOS $RABBIT_CLIENTISO_PASSWORD
rabbitmqctl set_permissions -p / $RABBIT_CLIENTIOS "^$" ".*" ".*"
```

**TODO**: Создание очередений и обменников здесь в скрипте