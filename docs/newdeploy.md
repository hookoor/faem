## Деплой нового сервиса. Хронология
На примере деплоинга сервиса voip

### Добавление БД

1. Создал нобую БД и дал доступ админу к новой БД
2. Создать пользователя как в доке `postgres.md`
2. Накатил миграции


### Исправление gitlab-ci.yml
Для начала я расширил инструкции для фазы билда:

```
docker-build:voip:
  extends: .docker-build
  variables:
    IMAGE_NAME: voip
```

Фазы пуша:

```
docker-push:voip:
  extends: .docker-push
  dependencies:
    - docker-build:voip
  variables:
    IMAGE_NAME: voip
    IMAGE_TAG: build.${CI_PIPELINE_ID}

```

Расширил параметры запуска helm чартов:
```
    --set voip.image.repository=${CI_REGISTRY_IMAGE}/voip
    --set voip.image.tag=${IMAGE_TAG}
```

а также на фазе деплоя:

```
    --set voip.image.repository=${CI_REGISTRY_IMAGE}/driver
    --set voip.image.tag=${IMAGE_TAG}
```

### Исправление values.yaml

Добавляем конфигурации сервиса `environments/stage01/values.yaml`

```
voip:
  ingress:
    enabled: true
    hosts:
      - host: voip.apis.stage.faem.pro
        paths: ["/"]
    tls:
      - secretName: voip-tls
        hosts:
          - voip.apis.stage.faem.pro

  configEnvs:
    CLIENT_DATABASE_HOST: 35.228.139.234
    CLIENT_DATABASE_PORT: 5432
    CLIENT_DATABASE_DB: voip
    CLIENT_BROKER_USERURL: rabbitmq.faem.svc.cluster.local:5672
    CLIENT_BROKER_EXCHAGEPREFIX: ""
    CLIENT_BROKER_EXCHAGEPOSTFIX: ""

  secretRefs:
    - env: VOIP_DATABASE_USER
      secretName: voip-settings
      secretKey: postgres-user
    - env: VOIP_DATABASE_PASSWORD
      secretName: voip-settings
      secretKey: postgres-password
    - env: VOIP_BROKER_USERCREDITS
      secretName: voip-settings
      secretKey: rabbitmq-user-credentials
```

### Деплоймент конфиг
1. Расширил faem-backend/values.yaml
2. Сделать копию сервиса в `faem-backend/templates/voip`
3. Инкрементировал версию helm chart
4. Добавил блок VOIP в `faem-backend/templates/_helpers.tpl`
5. Скопировал docker файл для VOIP (папка `docker`)
6. Создал дефолтный пустой конфиг в папке `config/voip.toml`
7. Обновить terrform DNS
