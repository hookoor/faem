## Развертывание БД

После того как БД создано нужно установить POSTGIS плагин для CRM базы. Сам PostGis уже есть предустановлен, нужно только запустить команду

```
CREATE EXTENSION postgis;

```

Далее создаем пользователей, роли и раздаем им права. Ниже представлены SQL инструкции, единственная особенность в том что их надо выполнять при подключении к нужной базе. 

Подключившись к $CRM_DATABASE:

```
CREATE USER $CRM_USER WITH PASSWORD $CRM_PASSWORD;
CREATE ROLE "crm_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "crm_apiservice" TO $CRM_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO crm_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO crm_apiservice;
GRANT ALL PRIVILEGES ON DATABASE $CRM_DATABASE to $CRM_USER;
```


После подключения к $DRIVER_DATABASE

```
CREATE USER $DRIVER_USER WITH PASSWORD $DRIVER_PASSWORD;
CREATE ROLE "driver_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "driver_apiservice" TO $DRIVER_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO driver_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO driver_apiservice;
GRANT ALL PRIVILEGES ON DATABASE $DRIVER_DATABASE to $DRIVER_USER;

```

После подключения к $CLIENT_DATABASE

```
CREATE USER $CLIENT_USER WITH PASSWORD $CLIENT_PASSWORD;
CREATE ROLE "client_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "client_apiservice" TO $CLIENT_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO client_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO client_apiservice;
GRANT ALL PRIVILEGES ON DATABASE $CLIENT_DATABASE to $CLIENT_USER;

```

После подключения к $VOIP_DATABASE

```
CREATE USER $VOIP_USER WITH PASSWORD $VOIP_PASSWORD;
CREATE ROLE "voip_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "voip_apiservice" TO $VOIP_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO voip_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO voip_apiservice;
GRANT ALL PRIVILEGES ON DATABASE $VOIP_DATABASE to $VOIP_USER;

```

После подключения к $LIMESIDECAR

```
CREATE USER $LIMESIDECAR WITH PASSWORD $LIMESC_PWD;
CREATE ROLE "limesc_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "limesc_apiservice" TO $LIMESIDECAR;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO limesc_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO limesc_apiservice;
GRANT ALL PRIVILEGES ON DATABASE limesidecar to $LIMESIDECAR;

```

После подключения к $CHAT

```
CREATE USER $CHAT_USER WITH PASSWORD $CHAT_PWD;
CREATE ROLE "chat_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "chat_apiservice" TO $CHAT_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO chat_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO chat_apiservice;
GRANT ALL PRIVILEGES ON DATABASE chat to $CHAT_USER;

```

После подключения к $BILLING

```
CREATE USER $BILLING_USER WITH PASSWORD $BILLING_PWD;
CREATE ROLE "billing_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "billing_apiservice" TO $BILLING_USER;
GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO billing_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO billing_apiservice;
GRANT ALL PRIVILEGES ON DATABASE billing to $BILLING_USER;

```


## Миграция

Для накатывания миграций используем CLI (golang-migrate/migrate)[https://github.com/golang-migrate/migrate]

Пример использования:
```
./bin/migrate.darwin -source file://crm/sql -database postgres://postgres:password@127.0.0.1/crm?sslmode=disable up

```

Пример создания миграции:
```
bin/migrations/migrate.darwin -source file://cmd/migrations/voip -database postgres://postgres:root@127.0.0.1/voip?sslmode=disable create -ext SQL -dir cmd/migrations/voip initVoipDB
```

Что бы все работало, я добавляю в конец доступ к группе api_services в миграции

```
GRANT INSERT, SELECT, UPDATE, DELETE ON TABLE public.operator_groups TO crm_apiservice;
GRANT SELECT, USAGE ON SEQUENCE public.operator_groups_id_seq TO crm_apiservice;
```

## Миграция

Seed данные лежат в отдельной репе https://gitlab.com/faemproject/databases-data


## Подключение репортера
CREATE USER crm_reporter WITH PASSWORD 'put-pass-here';
CREATE ROLE "faem_reporter" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "faem_reporter" TO crm_reporter;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO faem_reporter;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO faem_reporter;

## После подключения к $ANALYTIC
CREATE USER analytic WITH PASSWORD 'put-pass-here';
CREATE ROLE "analytic_apiservice" NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
GRANT "analytic_apiservice" TO analytic;
GRANT SELECT ON ALL TABLES IN SCHEMA public TO analytic_apiservice;
GRANT  ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO analytic_apiservice;