# Configuration

This directory holds documented default configuration files for services. These files should be used as a reference 
and/or as a default configuration

When deploying services to environments, custom configuration files based on these configs should be used. Alternatively,
default configuration file can be used as a base, and configuration parameters should be overridden using environment
variables.
