module gitlab.com/faemproject/backend/faem

require (
	cloud.google.com/go v0.81.0
	cloud.google.com/go/bigquery v1.8.0
	cloud.google.com/go/storage v1.15.0
	github.com/NaySoftware/go-fcm v0.0.0-20190516140123-808e978ddcd2
	github.com/antonmedv/expr v1.1.4
	github.com/casbin/casbin/v2 v2.29.2
	github.com/centrifugal/gocent v2.1.0+incompatible
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fiorix/go-smpp v0.0.0-20210403173735-2894b96e70ba
	github.com/go-pg/pg v8.0.7+incompatible
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/gofrs/uuid v4.0.0+incompatible
	github.com/golang/protobuf v1.5.2
	github.com/hashicorp/go-version v1.2.1
	github.com/korovkin/limiter v0.0.0-20190919045942-dac5a6b2a536
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/echo/v4 v4.2.2
	github.com/labstack/gommon v0.3.0
	github.com/looplab/fsm v0.1.0
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/nguyenthenguyen/docx v0.0.0-20181031033525-8cb697a41e43
	github.com/oliamb/cutter v0.2.2
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.10.0
	github.com/prometheus/common v0.18.0
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.7.1
	github.com/streadway/amqp v1.0.0
	github.com/stretchr/testify v1.7.0
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	github.com/ttacon/libphonenumber v1.2.1
	gitlab.com/faemproject/backend/core/shared v1.228.420 // перезаписывается ниже, тут не нужно менять
	go.uber.org/atomic v1.7.0
	go.uber.org/multierr v1.7.0
	golang.org/x/crypto v0.0.0-20200820211705-5c72a883971a
	golang.org/x/net v0.0.0-20210316092652-d523dce5a7f4
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/text v0.3.6
	google.golang.org/api v0.45.0
	google.golang.org/genproto v0.0.0-20210420162539-3c870d7478d2
)

replace gitlab.com/faemproject/backend/core/shared => gitlab.com/faemproject/backend/core/shared.git v0.9.21-casbin-test

// replace gitlab.com/faemproject/backend/core/shared => ../shared

go 1.16
